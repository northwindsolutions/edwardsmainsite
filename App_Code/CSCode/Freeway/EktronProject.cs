#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  23 Oct 2009
Details : Modified the existing functionality of one file submittion in a Freeway Project
          To multiple files subbmittion in a Freeway Project.          
---------------------------------
  
*/
#endregion

#region using
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
using Ektron.Cms;
using DS = Ektron.Cms.LocalizationJobDataSet;
using System.Globalization;

#endregion

/// <summary>
/// Summary description for EktronProject
/// </summary>
public class EktronProject : LB.FreewayLib.Freeway.VojoProject
{
	#region Private Members

	private long _tempJobId;
	private DataRow _row = null;
	private DataSet _retrievedFile = null;
	private List<long> _contentIds = null;
	private long _currentImportJobId = -1;
	private int _sourceLangId = -1;

	#endregion

	#region Constructor

    /// <summary>
    /// Creates instance of the EktronProject by projectid
    /// </summary>
    /// <param name="projectId"></param>
	public EktronProject(string projectId)
		: base(projectId)
	{
		return;
	}

    /// <summary>
    /// Creates instance of the EktronProject by projectsummary
    /// </summary>
    /// <param name="projectSummary"></param>
	public EktronProject(LB.FreewayLib.VojoService.ProjectSummary projectSummary)
		: base(projectSummary)
	{
		RetreiveJobId();

		FreewayServer = SubmittedWhere;

		return;
	}

    
    /// <summary>
    /// LB Mumbai 26 Oct 2009
    /// Constructor overloading
    /// Returns EktronProject class object
    /// assigns the project id and Job id
    /// <param name="projectId"></param>
    /// <param name="jobId"></param>
    /// </summary>
	public EktronProject(string projectId, long jobId)
	{
        Id = projectId;
		_tempJobId = jobId;

		return;
	}

    /// <summary>
    /// Creates instance of the EktronProject by jobId
    /// </summary>
    /// <param name="jobId"></param>
    public EktronProject(long jobId)
    {
        _tempJobId = jobId;

        return;
    }

    /// <summary>
    /// Creates instance of the EktronProject by project row
    /// </summary>
    /// <param name="row"></param>
	public EktronProject(DataRow row)
		: base(row != null ? row["ProjectId"].ToString() : string.Empty)
	{
		_row = row;

		FreewayServer = SubmittedWhere;

		return;
	}

    /// <summary>
    /// Creates instance of the EktronProject - Default Constructor
    /// </summary>
	public EktronProject()
	{
		return;
	}

	#endregion

	#region Public Members

    /// <summary>
    /// Manages the visibility of the project data
    /// </summary>
	public bool Visible
	{
		get { return _row != null && (bool)_row["Visible"]; }
		set
		{
			if (_row == null || Visible == value || string.IsNullOrEmpty(Id))
				return;

			_row["Visible"] = value;
			//FreewayDB.SetProjectVisible(int.Parse(Id), Visible);
            FreewayDB.SetProjectVisible(Id, Visible);


			return;
		}
	}

    /// <summary>
    /// Manages the JobId information in the system
    /// </summary>
	public long JobId
	{
		get
		{
			if (_row == null)
				return 0;

			return (long)_row["JobId"];
		}
		set
		{
			if (_row == null)
				return;

			_row["JobId"] = value;

			return;
		}
	}



    /// <summary>
    /// Manages the all JobIds information available for each project in the system
    /// </summary>
    public string JobsAvailable
    {
        get
        {
            if (_row == null)
                return null;

            return ((string)_row["JobsAvailable"]).Remove(0, 1);
        }
        set
        {
            if (_row == null)
                return;

            _row["AvailableJobs"] = value;

            return;
        }
    }

    /// <summary>
    /// Manages the all XLIFF files information available for each project in the system
    /// </summary>
    public string XliffFilesAvailable
    {
        get
        {
            string returnval = string.Empty;
            if (_row == null)
                returnval = null;
            if(_row["XliffFileIds"].ToString().Length > 0)
                returnval = ((string)_row["XliffFileIds"]).Remove(0, 1);
            return returnval;
        }
        set
        {
            if (_row == null)
                return;

            _row["XliffFileIds"] = value;

            return;
        }
    }
    

    /// <summary>
    /// Manages the SubmittedBy information in the system
    /// </summary>
	public string SubmittedBy
	{
		get
		{
			if (_row == null)
				return "Unknown";

			return _row["SubmittedBy"].ToString();
		}
		set
		{
			if (_row == null)
				return;

			_row["SubmittedBy"] = value;

			return;
		}
	}

    /// <summary>
    /// Manages the SubmittedWhen information in the system
    /// </summary>
	public string SubmittedWhen
	{
		get
		{
			if (_row == null)
				return "Unknown";

			return (_row["SubmittedWhen"].ToString().Length > 0)?((DateTime)_row["SubmittedWhen"]).ToString("g"):string.Empty;
		}
		set
		{
			if (_row == null)
				return;

			_row["SubmittedWhen"] = value;

			return;
		}
	}

    /// <summary>
    /// Manages the CreatedBy information in the system
    /// </summary>
    public string CreatedBy
    {
        get
        {
            if (_row == null)
                return "Unknown";

            return _row["CreatedBy"].ToString();
        }
        set
        {
            if (_row == null)
                return;

            _row["CreatedBy"] = value;

            return;
        }
    }

    /// <summary>
    /// Manages the CreatedWhen information in the system
    /// </summary>
    public string CreatedWhen
    {
        get
        {
            if (_row == null)
                return "Unknown";

            return (_row["CreatedWhen"].ToString().Length > 0) ? ((DateTime)_row["CreatedWhen"]).ToString("g") : string.Empty;
        }
        set
        {
            if (_row == null)
                return;

            _row["CreatedWhen"] = value;

            return;
        }
    }

    /// <summary>
    /// Manages the RetrievedWhen information in the system
    /// </summary>
	public string RetrievedWhen
	{
		get
		{
			GetRetrieveFiles();

			if (_retrievedFile == null || _retrievedFile.Tables[0].Rows.Count == 0)
				return "Never";

			DateTime value = DateTime.MinValue;

			foreach (DataRow row in _retrievedFile.Tables[0].Rows)
				if ((DateTime)row["RetrieveDate"] > value)
					value = (DateTime)row["RetrieveDate"];

			return value.ToString("g");
		}
	}

    /// <summary>
    /// Manages the SourceLangId information in the system
    /// </summary>
	public int SourceLangId
	{
		get
		{
			if (_sourceLangId == -1)
			{
				LocalizationAPI locApi = new LocalizationAPI();

				LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(_tempJobId);

				for (int job = 0; job < jobs.Rows.Count; job++)
				{
					LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

					LocalizationJobDataSet.LocalizationJobFileDataTable jobFiles = locApi.GetFilesByJob(jobRow.JobID);

					if (jobFiles.Rows.Count == 0)
						continue;

					for (int file = 0; file < jobFiles.Rows.Count; file++)
					{
						LocalizationJobDataSet.LocalizationJobFileRow fileRow = jobFiles[file];

						_sourceLangId = fileRow.SourceLanguage;

						break;
					}

					if (_sourceLangId != -1)
						break;
				}

				if (_sourceLangId == -1)
					_sourceLangId = 1033;
			}

			return _sourceLangId;
		}
	}

    /// <summary>
    /// Manages the ContentIds information in the system
    /// </summary>
	public long[] ContentIds
	{
		get
		{
			if (_contentIds == null)
			{
				_contentIds = new List<long>();

				LocalizationAPI locApi = new LocalizationAPI();


                string[] jobcollection = JobsAvailable.Split(',');

                for (int i = 0; i < jobcollection.Length; i++)
                {
                    long jobid = Convert.ToInt64(jobcollection[i]);
                    LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(jobid);

                    for (int job = 0; job < jobs.Rows.Count; job++)
                    {
                        LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

                        LocalizationJobDataSet.LocalizationSkeletonDataTable contentItems = locApi.GetContentItemsByJob(jobRow.JobID);

                        if (contentItems.Rows.Count == 0)
                            continue;

                        for (int item = 0; item < contentItems.Rows.Count; item++)
                        {
                            LocalizationJobDataSet.LocalizationSkeletonRow contentItem = contentItems[item];

                            if (!_contentIds.Contains(contentItem.ItemID))
                                _contentIds.Add(contentItem.ItemID);
                        }
                    }
                    _contentIds.Sort();
                }
			}

			return _contentIds.ToArray();
		}
	}

    /// <summary>
    /// Manages the ContentIds with job ids information in the system
    /// </summary>
    public System.Collections.Hashtable ContentswithJobs
    {
        get
        {
            System.Collections.Hashtable _ContentIdwithJobs = null;
            _ContentIdwithJobs = new System.Collections.Hashtable();
            System.Text.StringBuilder _Content;
            LocalizationAPI locApi = new LocalizationAPI();

            string[] jobcollection = JobsAvailable.Split(',');

            for (int i = 0; i < jobcollection.Length; i++)
            {
                _Content = new System.Text.StringBuilder();
                LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(Convert.ToInt64(jobcollection[i]));
                #region for (int job = 0; job < jobs.Rows.Count; job++)
                for (int job = 0; job < jobs.Rows.Count; job++)
                {
                    LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

                    LocalizationJobDataSet.LocalizationSkeletonDataTable contentItems = locApi.GetContentItemsByJob(jobRow.JobID);

                    if (contentItems.Rows.Count == 0)
                        continue;
                    System.Text.StringBuilder contentids = new System.Text.StringBuilder();

                    #region for (int item = 0; item < contentItems.Rows.Count; item++)
                    for (int item = 0; item < contentItems.Rows.Count; item++)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        LocalizationJobDataSet.LocalizationSkeletonRow contentItem = contentItems[item];

                        //if (!_ContentIdwithJobs.Contains(contentItem.ItemID))
                        //    _ContentIdwithJobs.Add(contentItem.ItemID, jobid);

                        long id = contentItem.ItemID;

                        ContentAPI contentApi = new ContentAPI();
                        if (contentItem.ItemType == 4)
                        break;

                        ContentData content = contentApi.GetContentById(id, ContentAPI.ContentResultType.Published);

                        //if (content == null)
                        //    throw new Exception("Content id - " + id.ToString() + " is not available and throwing invalid Content id exception.");

                        ////if (_contentIds[_contentIds.Count - 1] != id)

                        ////if(item != contentItems.Rows.Count-1)  
                        ////    sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcn.gif"));
                        ////else
                        //    sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcln.gif"));

                        //sb.Append(string.Format(@"<a href=""../content.aspx?action=View&folder_id=0&id={0}&LangType={2}&callerpage=content.aspx&origurl=action%3dViewContentByCategory%26id%3d0"">{0} - {1}</a><br />", content.Id.ToString(), content.Title, SourceLangId));

                        //if (sb.ToString().Trim().Length == 0)
                        //{
                        //    sb.Append("<span>" + "No Contents are available." + "</span>");
                        //}
                        _Content.Append(content.Id.ToString() + " - " + content.Title);
                        _Content.Append("*");

                    }
                    #endregion
                }
                #endregion
                if (!_ContentIdwithJobs.Contains(jobcollection[i].ToString()))
                    _ContentIdwithJobs.Add(jobcollection[i].ToString(), _Content.ToString());
            }


            return _ContentIdwithJobs;
        }
    }


    /// <summary>
    /// Manages the SubmittedWhere information in the system. 
    /// Project can be submitted on Demo server or Live server
    /// </summary>
	public LB.FreewayLib.Freeway.FreewayServers SubmittedWhere
	{
		get
		{
			if (_row == null)
				return LB.FreewayLib.Freeway.FreewayServers.Demo;

			try
			{
				return (LB.FreewayLib.Freeway.FreewayServers)Enum.Parse(typeof(LB.FreewayLib.Freeway.FreewayServers), _row["SubmittedWhere"].ToString());
			}
			catch { }

			return LB.FreewayLib.Freeway.FreewayServers.Demo;
		}
	}

    /// <summary>
    /// Identifies the status of the Job in Ektron site
    /// </summary>
	public bool IsJobRunning
	{
		get
		{
			if (_currentImportJobId == -1)
				return false;

			LocalizationAPI locApi = new LocalizationAPI();

			LocalizationJobDataSet.LocalizationJobRow jobRow = locApi.GetJobByID(_currentImportJobId);

			if (jobRow != null && !DS.LocalizationJobRow.IsJobDone((LocalizationJobDataSet.LocalizationJobRow.States)jobRow.State))
				return true;

			_currentImportJobId = -1;

			return false;
		}
	}

 

    /// <summary>
    /// Submits the project with or without Quote in Freeway
    /// </summary>
	public void Submit(bool submitForQuote)
	{
		LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();

		if (vojoIdentity == null)
			return;

		LocalizationAPI locApi = new LocalizationAPI();

		LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(_tempJobId);

		for (int job = 0; job < jobs.Rows.Count; job++)
		{
			LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

			LocalizationJobDataSet.LocalizationJobFileDataTable jobFiles = locApi.GetFilesByJob(jobRow.JobID);

			if (jobFiles.Rows.Count == 0)
				continue;

			for (int file = 0; file < jobFiles.Rows.Count; file++)
			{
				LocalizationJobDataSet.LocalizationJobFileRow fileRow = jobFiles[file];

				try
				{
					CultureInfo sourceCulture = new CultureInfo(fileRow.SourceLanguage);

					LB.FreewayLib.VojoService.Language[] targetLangs = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(sourceCulture.Name, vojoIdentity);

					if (targetLangs.Length == 0)
						continue;

					CultureInfo targetCulture = new CultureInfo(fileRow.TargetLanguage);

					if (!IsLanguageSupported(targetLangs, targetCulture))
						continue;

					AddFileToProject(fileRow);
				}
				catch { }
			}
		}

		if (Files.Count > 0)
			Submit(vojoIdentity, submitForQuote);

		return;
	}

    ///// <summary>
    ///// LB Mumbai 26 Oct 2009
    ///// Adds file to the project after retrieving the file from job collection
    ///// </summary>
    ///// <param name="files"></param>
    //public void AddFile(ListItemCollection files)
    //{
    //    // Freeway Vojo Identity
    //    LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();

    //    // If Freeway Vojo Identity is nill, interupt the functionality 
    //    //and return to the calling function
    //    if (vojoIdentity == null)
    //        return;

    //    LocalizationAPI locApi = new LocalizationAPI();

    //    LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(_tempJobId);
    //    bool fileSubInFreeway = false;
    //    for (int job = 0; job < jobs.Rows.Count; job++)
    //    {
    //        LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

    //        LocalizationJobDataSet.LocalizationJobFileDataTable jobFiles = locApi.GetFilesByJob(jobRow.JobID);

    //        if (jobFiles.Rows.Count == 0)
    //            continue;

    //        for (int file = 0; file < jobFiles.Rows.Count; file++)
    //        {
    //            LocalizationJobDataSet.LocalizationJobFileRow fileRow = jobFiles[file];

    //            try
    //            {
    //                ListItem listItem = files.FindByValue(fileRow.FileID.ToString());

    //                if (listItem != null && !listItem.Selected)
    //                    continue;

    //                CultureInfo sourceCulture = new CultureInfo(fileRow.SourceLanguage);

    //                LB.FreewayLib.VojoService.Language[] targetLangs = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(sourceCulture.Name, vojoIdentity);

    //                if (targetLangs.Length == 0)
    //                    continue;

    //                CultureInfo targetCulture = new CultureInfo(fileRow.TargetLanguage);

    //                if (!IsLanguageSupported(targetLangs, targetCulture))
    //                    continue;

    //                // Adds multiple files in Freeway Project
    //                AddMultipleFiles(fileRow);
    //                // File add functionality succeeded
    //                fileSubInFreeway = true;
    //            }
    //            catch (Exception exception)
    //            {
    //                // File add functionality failed
    //                fileSubInFreeway = false;
    //            }                
    //        }            
    //    }
    //    // If File add functionality succeeded
    //    // Make the entery in Freeway DB
    //    if (fileSubInFreeway)
    //        DoFileSubmitted();
    //}

    /// <summary>
    /// LB Mumbai 26 Oct 2009
    /// Adds file to the project after retrieving the file from job collection
    /// </summary>
    /// <param name="files"></param>
    public string AddFile(ListItemCollection files)
    {
        string msgreturn = string.Empty;
        // Freeway Vojo Identity
        LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();

        // If Freeway Vojo Identity is nill, interupt the functionality 
        //and return to the calling function
        if (vojoIdentity == null)
            return "vojoIdentity is null";

        LocalizationAPI locApi = new LocalizationAPI();

        LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(_tempJobId);
        bool fileSubInFreeway = false;

        //Inserts the XLIFF files id's in Ektron Freeway Database
        string strcommaseparatedxlifffileids = string.Empty;

        for (int job = 0; job < jobs.Rows.Count; job++)
        {
            LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

            LocalizationJobDataSet.LocalizationJobFileDataTable jobFiles = locApi.GetFilesByJob(jobRow.JobID);

            if (jobFiles.Rows.Count == 0)
                continue;



            for (int file = 0; file < jobFiles.Rows.Count; file++)
            {
                LocalizationJobDataSet.LocalizationJobFileRow fileRow = jobFiles[file];

                try
                {
                    ListItem listItem = files.FindByValue(fileRow.FileID.ToString());

                    if (listItem != null && !listItem.Selected)
                        continue;

                    CultureInfo sourceCulture = new CultureInfo(fileRow.SourceLanguage);

                  //LB.FreewayLib.VojoService.Language[] targetLangs = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(sourceCulture.Name, vojoIdentity);
                  LB.FreewayLib.VojoService.Language[] targetLangs = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(new LanguageIDMappingUtil().getEquivalentFreewayLangID( sourceCulture.Name), vojoIdentity);

                    if (targetLangs.Length == 0)
                        continue;

                    CultureInfo targetCulture = new CultureInfo(fileRow.TargetLanguage);

                    if (!IsLanguageSupported(targetLangs, targetCulture))
                        continue;

                    // Adds multiple files in Freeway Project
                    msgreturn = AddMultipleFiles(fileRow);

                    if (msgreturn == "File added to the project successfully.")
                    {
                        //Inserts the XLIFF files id's in Ektron Freeway Database
                        strcommaseparatedxlifffileids += "," + fileRow.FileName + "*" + _tempJobId;

                        // File add functionality succeeded
                        fileSubInFreeway = true;
                    }

                }
                catch (Exception exception)
                {
                    if (exception.Message.Contains("Freeway.Exceptions.Vojo"))
                        throw new Exception(exception.Message);
                    // File add functionality failed
                    fileSubInFreeway = false;
                }
            }
        }

        // If File add functionality succeeded
        // Make the entery in Freeway DB
        if (fileSubInFreeway)
        {
            //Remove the ',' charcatcer from strcommaseparatedxlifffileids field located at 0th location
            strcommaseparatedxlifffileids = strcommaseparatedxlifffileids.Remove(0, 1);
            //Inserts the XLIFF files id's in Ektron Freeway Database
            DoFileSubmitted(strcommaseparatedxlifffileids);
        }
        return msgreturn;
    }

    
    /// <summary>
    /// LB Mumabi 26 Oct 2009
    /// Adds multiple file to the project in Freeway Project
    /// </summary>
    /// <param name="fileRow"></param>
    public string AddMultipleFiles(LocalizationJobDataSet.LocalizationJobFileRow fileRow)
    {
        CultureInfo sourceCulture = new CultureInfo(fileRow.SourceLanguage);
        CultureInfo targetCulture = new CultureInfo(fileRow.TargetLanguage);

        LocalizationAPI locApi = new LocalizationAPI();

        string fileName = locApi.GetLocalizationUrl() + "/" + fileRow.FileUrl;

        string fullFilename = HttpContext.Current.Request.MapPath(fileName);

        if (System.IO.File.Exists(fullFilename))
        {
            try
            {
                byte[] data = System.IO.File.ReadAllBytes(fullFilename);

                LB.FreewayLib.Freeway.VojoFile file = new LB.FreewayLib.Freeway.VojoFile(fileRow.FileName,new LanguageIDMappingUtil().getEquivalentFreewayLangID( sourceCulture.Name), new LanguageIDMappingUtil().getEquivalentFreewayLangID(targetCulture.Name), data);

                file.AddMeta("FileID", fileRow.FileID.ToString());

                Files.Add(file);
                
                LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();

                if (vojoIdentity == null)
                    return "vojoIdentity is null";
        	
		        // Calls the VojoProject to add the file in Freeway Project
                    AddFile(vojoIdentity, true);
                
            }
            catch(Exception ex) { throw new Exception(ex.Message); }
        }

        return "File added to the project successfully."; 
    }
    /// <summary>
    /// LB Mumbai 26 Oct 2009
    /// Submit the Project in Freeway by taking files to submit and 
    /// submit  the project with/ without quite
    /// </summary>
    /// <param name="files"></param>
    /// <param name="submitForQuote"></param>
	//public void Submit(ListItemCollection files, bool submitForQuote)
    public void Submit(bool submitProject, bool submitForQuote)
	{
        LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();

        if (vojoIdentity == null)
            return;

        // Calls the VojoProject to submit the Project in Freeway 
		Submit(vojoIdentity, submitForQuote,false);
        // Update the Freeway database for successful Project submission
        DoProjectSubmitted();
		return;
    }

    /// <summary>
    /// LB Mumbai 26 Oct 2009
    /// Creates the Project in Freeway
    /// If project is already available then assign the Project Id
    /// Else create the project and assign the retrieved Project ID
    /// </summary>
    /// <param name="_projectid"></param>
    public string CreateProject()
    {
        // Create Project in Freeway
        LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
        var server = vojoIdentity.FreewayServer.ToString();
        var user = vojoIdentity.Username.ToString();
        var password = vojoIdentity.Password.ToString();
        Id = CreateProject(vojoIdentity);

        //if (Id.Contains("WV"))
        //{
        //    string strId = Id.Substring(3);
        //    int intId = 0;
        //    int.TryParse(strId, out intId);
        //    Id = Convert.ToString(intId);
        //}

        //Add Project entry in Ektron Freeway Database 
        DoEktronFreewayDBProjectCreation();

        return Id;
    }
    
    /// <summary>
    /// Functionality developed to add multiple files to the same project
    /// It only adds the file to the project
    /// Does not create or submit the project
    /// </summary>
    /// <param name="fileRow"></param>
    private void AddFileToProject(LocalizationJobDataSet.LocalizationJobFileRow fileRow)
	{
		CultureInfo sourceCulture = new CultureInfo(fileRow.SourceLanguage);
		CultureInfo targetCulture = new CultureInfo(fileRow.TargetLanguage);

		LocalizationAPI locApi = new LocalizationAPI();

		string filename = locApi.GetLocalizationUrl() + "/" + fileRow.FileUrl;

		string fullFilename = HttpContext.Current.Request.MapPath(filename);

		if (System.IO.File.Exists(fullFilename))
		{
			try
			{
				byte[] data = System.IO.File.ReadAllBytes(fullFilename);

				LB.FreewayLib.Freeway.VojoFile file = new LB.FreewayLib.Freeway.VojoFile(fileRow.FileName,new LanguageIDMappingUtil().getEquivalentFreewayLangID( sourceCulture.Name),new LanguageIDMappingUtil().getEquivalentFreewayLangID( targetCulture.Name), data);

				file.AddMeta("FileID", fileRow.FileID.ToString());

				Files.Add(file);
			}
			catch { }
		}

		return;
	}

    /// <summary>
    /// Retrieves the translated file from Freeway
    /// by file id and Freeway authentication
    /// </summary>
    /// <param name="file"></param>
    /// <param name="vojoIdentity"></param>
    /// <returns></returns>
	public override bool RetrieveFile(LB.FreewayLib.Freeway.VojoFile file, LB.FreewayLib.Freeway.VojoIdentity vojoIdentity)
	{
		bool value = base.RetrieveFile(file, vojoIdentity);

		if (value)
		{
			UserAPI userApi = new UserAPI();

			FreewayDB.UpdateFileRetrieval(Id, file.Id, userApi.UserObject(userApi.UserId).Username);

			// will force a reload of the data
			_retrievedFile = null;
		}

		return value;
	}

    /// <summary>
    /// Retrieves the collection of translated files from Freeway
    /// by file id's and Freeway authentication
    /// </summary>
    /// <param name="files"></param>
    /// <param name="vojoIdentity"></param>
    /// <returns></returns>
	public bool RetrieveFile(ListItemCollection files, LB.FreewayLib.Freeway.VojoIdentity vojoIdentity)
	{
		bool value = false;

		foreach (LB.FreewayLib.Freeway.VojoFile file in Files)
		{
			ListItem listItem = files.FindByValue(file.Id);

			if (listItem != null && !listItem.Selected)
				continue;

			value |= RetrieveFile(file, vojoIdentity);
		}

		return value;
	}

    /// <summary>
    /// Get the last retrived file date and time information
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
	public DateTime GetLastRetrieve(LB.FreewayLib.Freeway.VojoFile file)
	{
		GetRetrieveFiles();

		DateTime value = DateTime.MinValue;

		foreach (DataRow row in _retrievedFile.Tables[0].Rows)
			if (row["FileId"].ToString().Equals(file.Id) && (DateTime)row["RetrieveDate"] > value)
				value = (DateTime)row["RetrieveDate"];

		return value;
	}

    /// <summary>
    /// Get the history of retrived files date and time information
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
	public DateTime[] GetAllRetrieveDate(LB.FreewayLib.Freeway.VojoFile file)
	{
		GetRetrieveFiles();

		List<DateTime> dates = new List<DateTime>();

		foreach (DataRow row in _retrievedFile.Tables[0].Rows)
			if (file == null || row["FileId"].ToString().Equals(file.Id))
				dates.Add((DateTime)row["RetrieveDate"]);

		dates.Sort();
		dates.Reverse();

		return dates.ToArray();
	}

    /// <summary>
    /// Functionality to retrieve the translated XLIFF files from Freeway
    /// </summary>
    /// <param name="files"></param>
    /// <param name="startImportThread"></param>
    /// <returns></returns>
	public bool ImportFiles(ListItemCollection files, bool startImportThread)
	{
        // Check for import can be started
		if (!CanWeStartImport())
			return false;

		LocalizationAPI locApi = new LocalizationAPI();

		string sourceLabgId = string.Empty;

		if (files != null)
		{
            // Retriving the Trnslated XLIFF files from Freeway
			foreach (ListItem item in files)
			{
				if (!item.Selected)
					continue;

				LB.FreewayLib.Freeway.VojoFile file = null;

				foreach (LB.FreewayLib.Freeway.VojoFile fileItem in Files)
				{
					sourceLabgId = fileItem.SourceLang;

					if (fileItem.Id.Equals(item.Value))
					{
						file = fileItem;
						break;
					}
				}

				if (file == null)
					continue;

				string destinationFilename = System.IO.Path.Combine(locApi.GetTranslationUploadDirectory(), file.Filename);

                if (!RetrieveFile(file, FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity()))
					continue;

				try
				{
					System.IO.File.WriteAllBytes(destinationFilename, file.Data);
				}
				catch { }
			}
		}
		else
		{
			// always retrieve all files from the project if none were specify
			foreach (LB.FreewayLib.Freeway.VojoFile file in Files)
			{
				string destinationFilename = System.IO.Path.Combine(locApi.GetTranslationUploadDirectory(), file.Filename);

                if (!RetrieveFile(file, FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity()))
					continue;

				try
				{
					System.IO.File.WriteAllBytes(destinationFilename, file.Data);
				}
				catch { }
			}
		}

		if (!string.IsNullOrEmpty(sourceLabgId))
			locApi.ContentLanguage = new CultureInfo(sourceLabgId).LCID;

		if (startImportThread)
			_currentImportJobId = locApi.StartImportTranslation();
		else
			_currentImportJobId = -1;

		return true;
	}

    /// <summary>
    /// Functionality to retrieve selected XLIFF files from Treenode 
    /// those are translated XLIFF files from Freeway
    /// </summary>
    /// <param name="files"></param>
    /// <param name="startImportThread"></param>
    /// <returns></returns>
    public bool ImportFiles(TreeView files, bool startImportThread, bool fromtreenode)
    {
        // Check for import can be started
        if (!CanWeStartImport())
            return false;

        LocalizationAPI locApi = new LocalizationAPI();

        string sourceLabgId = string.Empty;

        if (files != null)
        {
            // Retriving the Trnslated XLIFF files from Freeway
            foreach (TreeNode item in files.CheckedNodes)
            {
                if ((item.ShowCheckBox == false) && (!item.Checked == false))
                    continue;

                LB.FreewayLib.Freeway.VojoFile file = null;

                foreach (LB.FreewayLib.Freeway.VojoFile fileItem in Files)
                {
                    sourceLabgId = new LanguageIDMappingUtil().getEquivalentEktronLangID(fileItem.SourceLang);

                    if (fileItem.Id.Equals(item.Value))
                    {
                        file = fileItem;
                        break;
                    }
                }

                if (file == null)
                    continue;

                string destinationFilename = System.IO.Path.Combine(locApi.GetTranslationUploadDirectory(), file.Filename);

                if (!RetrieveFile(file, FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity()))
                    continue;

                try
                {
                    System.IO.File.WriteAllBytes(destinationFilename, file.Data);
                }
                catch { }
            }
        }
        else
        {
            // always retrieve all files from the project if none were specify
            foreach (LB.FreewayLib.Freeway.VojoFile file in Files)
            {
                string destinationFilename = System.IO.Path.Combine(locApi.GetTranslationUploadDirectory(), file.Filename);

                if (!RetrieveFile(file, FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity()))
                    continue;

                try
                {
                    System.IO.File.WriteAllBytes(destinationFilename, file.Data);
                }
                catch { }
            }
        }

        if (!string.IsNullOrEmpty(sourceLabgId))
            locApi.ContentLanguage = new CultureInfo(sourceLabgId).LCID;

        if (startImportThread)
            _currentImportJobId = locApi.StartImportTranslation();
        else
            _currentImportJobId = -1;

        return true;
    }

	#endregion

	#region Protected Members

    /// <summary>
    /// On submission of the project Freeway
    /// the information will be logged into the Ektron Freeway Project table
    /// </summary>
	protected override void DoProjectSubmitted()
	{
		base.DoProjectSubmitted();

		if (!string.IsNullOrEmpty(Id))
		{
			UserAPI userApi = new UserAPI();
           // FreewayDB.SubmitProjectId(int.Parse(Id), userApi.UserObject(userApi.UserId).Username, FreewayServer.ToString());
			FreewayDB.SubmitProjectId(Id, userApi.UserObject(userApi.UserId).Username, FreewayServer.ToString());
		}

		return;
	}

    ///// <summary>
    ///// LB Mumbai 26 Oct 2009
    ///// Update the Project row in  Freeway DB against the Project Submitted in Freeway
    ///// </summary>
    //protected void DoFileSubmitted()
    //{
    //    if (!string.IsNullOrEmpty(Id))
    //    {
    //        UserAPI userApi = new UserAPI();

    //        FreewayDB.CreateJobId(int.Parse(Id),_tempJobId, userApi.UserObject(userApi.UserId).Username);
    //    }

    //    return;
    //}

    /// <summary>
    /// LB Mumbai 26 Oct 2009
    /// Update the Project row in  Freeway DB against the Project Submitted in Freeway
    /// along with the xlifffiles comma separated values
    /// </summary>
    protected void DoFileSubmitted(string strcommaseparatedxlifffileids)
    {
        if (!string.IsNullOrEmpty(Id))
        {
            UserAPI userApi = new UserAPI();
          //  FreewayDB.CreateJobId(int.Parse(Id), _tempJobId, userApi.UserObject(userApi.UserId).Username, strcommaseparatedxlifffileids);
            FreewayDB.CreateJobId(Id, _tempJobId, userApi.UserObject(userApi.UserId).Username, strcommaseparatedxlifffileids);
        }

        return;
    }



    /// <summary>
    /// LB Mumbai 26 Oct 2009
    /// Update the Project row in  Freeway DB against the Project Submitted in Freeway
    /// </summary>
    protected void DoEktronFreewayDBProjectCreation()
    {
        if (!string.IsNullOrEmpty(Id))
        {
            UserAPI userApi = new UserAPI();

           
            //if (Id.Contains("WV"))
            //{
            //    string strId = Id.Substring(3);
            //    int intId = 0;
            //    int.TryParse(strId, out intId);
            //    Id = Convert.ToString(intId);
            //}

            //FreewayDB.CreateJobId(int.Parse(Id), _tempJobId, userApi.UserObject(userApi.UserId).Username, string.Empty);
            FreewayDB.CreateJobId(Id, _tempJobId, userApi.UserObject(userApi.UserId).Username, string.Empty);
        }

        return;
    }

	#endregion

	#region Private Members

    /// <summary>
    /// Get the Retrieved Files of Project informnation from Ektron data repository
    /// </summary>
	private void RetreiveJobId()
	{
		try
		{
            //if (Id.Contains("WV"))
            //{
            //    string strId = Id.Substring(3);
            //    int intId = 0;
            //    int.TryParse(strId, out intId);
            //    Id = Convert.ToString(intId);
            //}
			//_row = FreewayDB.GetProjectRowFromProjectId(int.Parse(Id));
            _row = FreewayDB.GetProjectRowFromProjectId(Id);
		}
        catch (Exception ex) { throw ex; }

		return;
	}

    /// <summary>
    /// Checks for the language support for 
    /// Ektron target languages selected against Freeway target languages
    /// </summary>
    /// <param name="targetLanguages"></param>
    /// <param name="targetCulture"></param>
    /// <returns></returns>
	private static bool IsLanguageSupported(LB.FreewayLib.VojoService.Language[] targetLanguages, CultureInfo targetCulture)
	{
		foreach (LB.FreewayLib.VojoService.Language language in targetLanguages)
			if (string.Compare(new LanguageIDMappingUtil().getEquivalentEktronLangID(language.ID), targetCulture.Name, true) == 0)
				return true;

		return false;
	}

    /// <summary>
    /// Get the retrieved files information from Ektron File management repository
    /// </summary>
	private void GetRetrieveFiles()
	{
		if (_retrievedFile != null || string.IsNullOrEmpty(Id))
			return;

		_retrievedFile = FreewayDB.GetFileRetrievalPerProject(Id);

		return;
	}


    /// <summary>
    /// Check for file import functionality can be performed
    /// </summary>
    /// <returns></returns>
	private bool CanWeStartImport()
	{
		if (_currentImportJobId == -1)
			return true;

		LocalizationAPI locApi = new LocalizationAPI();

		LocalizationJobDataSet.LocalizationJobRow jobRow = locApi.GetJobByID(_currentImportJobId);

		if (jobRow != null && !DS.LocalizationJobRow.IsJobDone((LocalizationJobDataSet.LocalizationJobRow.States)jobRow.State))
			throw new Exception("This project is curently importing...");

#if false
		// we will check if there's any file that has been process in the upload folder
		int tryCount = 60;

		System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(locApi.GetTranslationUploadDirectory());

		while (tryCount-- > 0)
		{
			if (directory.GetFiles().Length == 0)
				return true;

			System.Threading.Thread.Sleep(500);
		}

		return false;
#else
		return true;
#endif
	}

	#endregion
}
