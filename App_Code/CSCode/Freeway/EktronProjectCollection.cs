#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  22 Oct 2009
Details : Modified the existing functionality of one file submittion in a Freeway Project
          To multiple files subbmittion in a Freeway Project.
          
---------------------------------
  
*/
#endregion

#region using
using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

#endregion

/// <summary>
/// Summary description for EktronProjectCollection
/// </summary>
public class EktronProjectCollection : List<EktronProject>
{
	#region Private Members

	private string _submittedByFilter = string.Empty;

	#endregion

	#region Constructor
    /// <summary>
    ///Returns the EktronProject collection instance
    /// By either creating the new and retrieving the updated collection
    /// </summary>
    /// <param name="vojoIdentity"></param>
    /// <param name="ektronProjectOnly"></param>
    /// <param name="submittedByFilter"></param>
	public EktronProjectCollection(LB.FreewayLib.Freeway.VojoIdentity vojoIdentity, bool ektronProjectOnly, string submittedByFilter)
	{
		_submittedByFilter = submittedByFilter;

		try
		{
			LB.FreewayLib.VojoService.ProjectSummary[] projectSummaries = LB.FreewayLib.Freeway.VojoServer.GetProjectSummary(200, vojoIdentity);

			foreach (LB.FreewayLib.VojoService.ProjectSummary projectSummary in projectSummaries)
			{
				EktronProject ektronProject = new EktronProject(projectSummary);

                if (ektronProjectOnly && ektronProject.Visible && (_submittedByFilter.Length == 0 || ektronProject.CreatedBy.Equals(_submittedByFilter)))
                   Add(ektronProject);
			}
		}
		catch(Exception ex)
        {
            throw ex;
        }

		return;
    }


    /// <summary>
    /// LB Mumbai 27 Oct 2009
    /// Retrieves the project summary and adds to the list based on 
    /// the project is Draft
    /// </summary>
    /// <param name="vojoIdentity"></param>
    /// <param name="submittedByFilter"></param>
    public EktronProjectCollection(LB.FreewayLib.Freeway.VojoIdentity vojoIdentity)
    {
       try
        {
            LB.FreewayLib.VojoService.ProjectSummary[] projectSummaries = LB.FreewayLib.Freeway.VojoServer.GetProjectSummary(200, vojoIdentity);

            foreach (LB.FreewayLib.VojoService.ProjectSummary projectSummary in projectSummaries)
            {
                EktronProject ektronProject = new EktronProject(projectSummary);

                if (ektronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Draft && ektronProject.Visible)
                    Add(ektronProject);
            }
        }
        catch (Exception exception) { throw exception; }

        return;
    }

    /// <summary>
    /// LB Mumbai 27 Oct 2009
    /// Retrieves the project summary and adds to the list based on 
    /// the project is Draft
    /// </summary>
    /// <param name="vojoIdentity"></param>
    /// <param name="submittedByFilter"></param>
    public EktronProjectCollection(LB.FreewayLib.Freeway.VojoIdentity vojoIdentity,string projectid)
    {
        try
        {
            LB.FreewayLib.VojoService.ProjectSummary[] projectSummaries = LB.FreewayLib.Freeway.VojoServer.GetProjectSummary(200, vojoIdentity);

            foreach (LB.FreewayLib.VojoService.ProjectSummary projectSummary in projectSummaries)
            {
                EktronProject ektronProject = new EktronProject(projectSummary);

                if (ektronProject.Id == projectid)
                    Add(ektronProject);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return;
    }


   /* /// <summary>
    /// LB Mumbai 27 Oct 2009
    /// Retrieves the project summary and adds to the list based on 
    /// the project is Draft
    /// </summary>
    /// <param name="vojoIdentity"></param>
    /// <param name="submittedByFilter"></param>
    public EktronProjectCollection(LB.FreewayLib.Freeway.VojoIdentity vojoIdentity)
    {
        try
        {
            LB.FreewayLib.VojoService.ProjectSummary[] projectSummaries = LB.FreewayLib.Freeway.VojoServer.GetProjectSummary(200, vojoIdentity);

            foreach (LB.FreewayLib.VojoService.ProjectSummary projectSummary in projectSummaries)
            {
                EktronProject ektronProject = new EktronProject(projectSummary);

                if (ektronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Draft && ektronProject.Visible)
                    Add(ektronProject);
            }
        }
        catch (Exception exception) { throw exception; }

        return;
    }*/

	#endregion

	#region Public Members
    /// <summary>
    /// Manages the EktronProjects information through out the user session 
    /// </summary>
	public static EktronProjectCollection CurrentProjects
	{
		get
		{
			if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["EktronProjects"] == null)
				return null;

			return HttpContext.Current.Session["EktronProjects"] as EktronProjectCollection;
		}
		set
		{
			if (HttpContext.Current != null && HttpContext.Current.Session != null)
				HttpContext.Current.Session["EktronProjects"] = value;

			return;
		}
	}

    /// <summary>
    /// Returns the EktronProject collection to the calling function
    /// By either creating the new and retrieving the updated collection
    /// </summary>
    /// <param name="submittedBy"></param>
    /// <param name="createNew"></param>
    /// <returns></returns>
    public static EktronProjectCollection GetProjects(string submittedBy, bool createNew)
    {
        
        if (createNew || CurrentProjects == null || !CurrentProjects.SubmittedByFilter.Equals(submittedBy))
            EktronProjectCollection.CurrentProjects = new EktronProjectCollection(FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity(), true, submittedBy);
        else
            EktronProjectCollection.CurrentProjects.Update();

        return EktronProjectCollection.CurrentProjects;
    }

    /// <summary>
    /// LB Mumbai 27 Oct 2009
    /// Retrievs the available Projects from the EktronProjectCollection class 
    /// for filling Send To Freeway dropdown list
    /// </summary>
    /// <returns></returns>
    public static EktronProjectCollection GetProjects()
    {
        EktronProjectCollection.CurrentProjects = new EktronProjectCollection(FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
        return EktronProjectCollection.CurrentProjects;
    }

    /// <summary>
    /// LB Mumbai 27 Oct 2009
    /// Retrievs the available Projects from the EktronProjectCollection class
    /// for retrieveing Sumbitted by and Submitted when information
    /// </summary>
    /// <returns></returns>
    public static EktronProjectCollection GetProjects(bool Sumbittedbywheninfo, string projectid)
    {
        EktronProjectCollection.CurrentProjects = new EktronProjectCollection(FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity(), projectid);
        return EktronProjectCollection.CurrentProjects;
    }
   
    /// <summary>
    /// Create the collection of projects by comparing the Freeway projects 
    /// against Project maiantained under Ektron data repository as well based on the submit by clause and
    /// Returns the project symmary collection to the calling function
    /// </summary>
    public void Update()
	{
        LB.FreewayLib.VojoService.ProjectSummary[] projectSummaries = LB.FreewayLib.Freeway.VojoServer.GetProjectSummary(200, FreewayConfiguration.IsExecutiveUser ?
            FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

		foreach (LB.FreewayLib.VojoService.ProjectSummary projectSummary in projectSummaries)
		{
			if (!Contains(projectSummary.ID))
			{
				EktronProject ektronProject = new EktronProject(projectSummary);

				if (ektronProject.Visible && (_submittedByFilter.Length == 0 || ektronProject.SubmittedBy.Equals(_submittedByFilter)))
					Add(ektronProject);
			}
		}

		return;
	}

    /// <summary>
    /// Reset the Ektron session for EktronProjects 
    /// </summary>
	public static void Reset()
	{
		HttpContext.Current.Session["EktronProjects"] = null;

		return;
	}

    /// <summary>
    /// Check for the project is already maiantained under Ektron data repository
    /// </summary>
    /// <param name="projectId"></param>
    /// <returns></returns>
	public EktronProject this[string projectId]
	{
		get
		{
			foreach (EktronProject project in this)
				if (project.Id == projectId)
					return project;

			return null;
		}
	}

    /// <summary>
    /// Check for Project is maintained under Ektron data repository
    /// </summary>
    /// <param name="projectId"></param>
    /// <returns></returns>
	public bool Contains(string projectId)
	{
		foreach (EktronProject project in this)
			if (project.Id.Equals(projectId))
				return true;

		return false;
	}

    /// <summary>
    /// Returns the Submit by username
    /// </summary>
	public string SubmittedByFilter
	{
		get { return _submittedByFilter; }
	}

	#endregion
}
