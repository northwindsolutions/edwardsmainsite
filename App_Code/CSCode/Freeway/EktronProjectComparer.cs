#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details : Complare the projects and find they are identical or not
---------------------------------
Modified By :    
Modified Date :  
Details : 
          
---------------------------------
  
*/
#endregion

#region using
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;

using System.Reflection;
#endregion

/// <summary>
/// Summary description for EktronProjectComparer
/// </summary>
public class EktronProjectComparer : IComparer<EktronProject>
{
	#region Private Members

	private string _compareField = "Id";
	private int _assendingFactor = 1;

	#endregion

	#region Constructor

    /// <summary>
    /// Create the default instance of the EktronProjectComparer class
    /// </summary>
	public EktronProjectComparer()
	{
		return;
	}

    /// <summary>
    /// Create the instance of the project by compareField and assending parameter
    /// </summary>
    /// <param name="compareField"></param>
    /// <param name="assending"></param>
	public EktronProjectComparer(string compareField, bool assending)
	{
		_compareField = compareField;
		_assendingFactor = assending ? 1 : -1;

		return;
	}

	#endregion

	#region IComparer<EktronProject> Members

    /// <summary>
    /// Checks the projects are identical by compareField and assending
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
	public int Compare(EktronProject x, EktronProject y)
	{
		if (x == null)
		{
			if (y == null)
			{
				// If x is null and y is null, they're equal. 
				return 0;
			}
			else
			{
				// If x is null and y is not null, y
				// is greater. 
				return -1 * _assendingFactor;
			}
		}
		else
		{
			// If x is not null...
			if (y == null)
			// ...and y is null, x is greater.
			{
				return 1 * _assendingFactor;
			}
			else
			{
				PropertyInfo propertyInfo = typeof(EktronProject).GetProperty(_compareField);

				if (propertyInfo == null)
					return 0;

				object xValue = propertyInfo.GetValue(x, null);
				object yValue = propertyInfo.GetValue(y, null);

				if (xValue == null || yValue == null)
					return (xValue == null ? (yValue == null ? 0 : -1) : 1) * _assendingFactor;

				IComparable xComparable = xValue as IComparable;

				if (xComparable != null)
					return xComparable.CompareTo(yValue) * _assendingFactor;

				// can't compare so we assume the are identical
				return 0;
			}
		}
	}

	#endregion
}
