﻿using System;
using System.Data;
using System.Configuration;

using System.Xml.Linq;
using System.Diagnostics;

/// <summary>
/// Summary description for EventLog
/// </summary>
public class EventLogs
{
    EventLog EL = new EventLog();
    bool enableventlog = false;

    public EventLogs()
    {
        //checks if event log is enabled
        if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
            enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

        if (enableventlog)
        {
            if (!EventLog.SourceExists("EktronFreewayLog"))
            {
                //Creating new Log, (it will appear as a tree node in windows event log)       
                EventLog.CreateEventSource("EktronFreewayLog", "EktronFreewayLog");
                EL = new EventLog();
                EL.Source = "EktronFreewayLog";
            }
        }
    }
    public void AddException(string Message)
    {
        //checks if event log is enabled
        if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
            enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

        //Log a exception to the eventLog 
        if (enableventlog)
        {
            EL.Source = "EktronFreewayLog";
            EL.WriteEntry(Message, EventLogEntryType.Error);

            //System.Windows.Forms.MessageBox.Show("One or more error occured during the install.Please consult the Windows Event Log for More details (EktronFreewayLog).", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }
    }
}
