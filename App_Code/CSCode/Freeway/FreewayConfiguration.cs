#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  22 Oct 2009
Details : Performs the Freeway functionality for CreateFreewayIdentity
          
---------------------------------
  
*/
#endregion

#region using
using System;
using System.Data;
using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
using Ektron.Cms;
using LB.FreewayLib.VojoService;
#endregion

/// <summary>
/// Summary description for FreewayConfiguration CreateFreewayIdentity, 
/// </summary>
public static class FreewayConfiguration
{
	#region Constructor

	static FreewayConfiguration()
	{
	}

	#endregion

	#region Public Members

	public const string LocalizationContentIdSessionName = "LocalizationContentIdSessionName";
	public const string FreewayContentSessionName = "FreewayContentSessionName";
	
	/// <summary>
    	/// Identifies whether the user has executive rights in Freeway
    	/// <returns></returns>
	public static bool IsExecutiveUser
	{
		get
		{
			string roleName = ConfigurationManager.AppSettings["FreewayExecutiveRoleName"];

			if (string.IsNullOrEmpty(roleName))
				roleName = "Freeway Executive Role";

			UserAPI userApi = new UserAPI();

			return userApi.GetRolePermissionSystem(roleName, userApi.UserId);
		}
	}

	/// <summary>
    	/// Based on the condition of the Global User Settings 
    	/// VojoIdentity with Executive permissions will be created and send to the calling function
    	/// by passsing FreewayUserName, Password, FreewayServer(Demo / Live),
    	/// SubmitWhere and UseSsl parameters
    	/// </summary>
    	/// <returns></returns
	public static LB.FreewayLib.Freeway.VojoIdentity CreateExecutiveFreewayIdentity()
	{
		DataRow genericUser = FreewayDB.GetUserRow(-1, true);

		if (genericUser != null && (bool)genericUser["ForceGlobalSettings"])
			return new LB.FreewayLib.Freeway.VojoIdentity(genericUser["FreewayUsername"].ToString(),
				genericUser["FreewayPassword"].ToString(), (LB.FreewayLib.Freeway.FreewayServers)Enum.Parse(typeof(LB.FreewayLib.Freeway.FreewayServers),
                genericUser["SubmitWhere"].ToString()), GetProxy(), (bool)genericUser["UseSsl"]);

		DataRow freewayUser = FreewayDB.GetUserRow((long)new UserAPI().UserId, true);

		if (freewayUser == null)
			return null;

		return new LB.FreewayLib.Freeway.VojoIdentity(freewayUser["FreewayUsername"].ToString(),
			freewayUser["FreewayPassword"].ToString(), (LB.FreewayLib.Freeway.FreewayServers)Enum.Parse(typeof(LB.FreewayLib.Freeway.FreewayServers),
            freewayUser["SubmitWhere"].ToString()), GetProxy(), (bool)freewayUser["UseSsl"]);
	}


    /// <summary>
    /// Based on the condition of the Global User Settings 
    /// VojoIdentity will be created and send to the calling function
    /// by passsing FreewayUserName, Password, FreewayServer(Demo / Live),
    /// SubmitWhere and UseSsl parameters
    /// </summary>
    /// <returns></returns>
	public static LB.FreewayLib.Freeway.VojoIdentity CreateFreewayIdentityOld()
	{

        DataRow freewayUser = FreewayDB.GetUserRow((long)new UserAPI().UserId, true);
        if (freewayUser == null)
            return null;
        else
        {
            DataRow genericUser = FreewayDB.GetUserRowFreewayServerWise(-1, true, freewayUser["CurrentEnvironment"].ToString ());

            if (genericUser != null && (bool)genericUser["ForceGlobalSettings"])
            {
                return new LB.FreewayLib.Freeway.VojoIdentity(genericUser["FreewayUsername"].ToString(),
                    genericUser["FreewayPassword"].ToString(), (LB.FreewayLib.Freeway.FreewayServers)Enum.Parse(typeof(LB.FreewayLib.Freeway.FreewayServers),
                    genericUser["SubmitWhere"].ToString()), GetProxy(), (bool)genericUser["UseSsl"]);
            }
            else
            {
                return new LB.FreewayLib.Freeway.VojoIdentity(freewayUser["FreewayUsername"].ToString(),
                    freewayUser["FreewayPassword"].ToString(), (LB.FreewayLib.Freeway.FreewayServers)Enum.Parse(typeof(LB.FreewayLib.Freeway.FreewayServers),
                    freewayUser["SubmitWhere"].ToString()), GetProxy(), (bool)freewayUser["UseSsl"]);
            }

        }
	}

    //Modifications for Edwards Vaccum for Global User Setting
    public static LB.FreewayLib.Freeway.VojoIdentity CreateFreewayIdentity()
    {
        bool iGbl = true;
        DataRow freewayUser = FreewayDB.GetUserRow((long)new UserAPI().UserId, true);

        DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(iGbl, (long)new UserAPI().UserId);
        string server = Enum.GetName(typeof(LB.FreewayLib.Freeway.FreewayServers), LB.FreewayLib.Freeway.FreewayServers.Demo);

        if (freewayGlobalUser != null)
        {
            int iVal = -1;
            for (int i = 0; i <= freewayGlobalUser.Table.Rows.Count - 1; i++)
            {
                iVal++;
                iGbl = Convert.ToBoolean(freewayGlobalUser.Table.Rows[i]["ForceGlobalSettings"]);
                if (iGbl == true)
                {
                    break;
                }
            }
            return CheckAndReturnMode(freewayUser, freewayGlobalUser, server, iVal);   
        }
        else
        {
            if (freewayUser != null)
            {
                int iVal = -1;
                for (int i = 0; i <= freewayUser.Table.Rows.Count - 1; i++)
                {
                    iVal++;
                    iGbl = Convert.ToBoolean(freewayUser.Table.Rows[i]["ForceGlobalSettings"]);
                    if (iGbl == true)
                    {
                        break;
                    }
                }
                return CheckAndReturnMode(freewayUser, freewayGlobalUser, server, iVal);
            }
            else
            {
                return null;
            }
        }
    }

    public static LB.FreewayLib.Freeway.VojoIdentity CreateFreewayIdentity1()
    {
        bool iGbl = true;
        DataRow freewayUser = FreewayDB.GetUserRow((long)new UserAPI().UserId, true);

        DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(iGbl, (long)new UserAPI().UserId);
        string server = Enum.GetName(typeof(LB.FreewayLib.Freeway.FreewayServers), LB.FreewayLib.Freeway.FreewayServers.Demo);

        if (freewayUser != null)
        {
            int iVal = -1;
            for (int i = 0; i <= freewayUser.Table.Rows.Count - 1; i++)
            {
                iVal++;
                iGbl = Convert.ToBoolean(freewayUser.Table.Rows[i]["ForceGlobalSettings"]);
                if (iGbl == true)
                {
                    break;
                    // return CheckAndReturnMode(freewayUser, freewayGlobalUser, server, i);
                }
            }
            return CheckAndReturnMode(freewayUser, freewayGlobalUser, server, iVal);        
        }
        else
        {
            return null;
        }
                
    }

    private static LB.FreewayLib.Freeway.VojoIdentity CheckAndReturnMode(DataRow freewayUser, DataRow freewayGlobalUser, string server,int row)
    {
        if (freewayGlobalUser == null)
        {
            if (freewayUser == null)
                return null;
            else
            {
                if (Convert.ToString(freewayUser.Table.Rows[row]["FreewayServerUrl"]).ToLower().Contains(server.ToString().ToLower()))
                {
                    return new LB.FreewayLib.Freeway.VojoIdentity(Convert.ToString(freewayUser.Table.Rows[row]["FreewayUsername"]),
                            Convert.ToString(freewayUser.Table.Rows[row]["FreewayPassword"]), LB.FreewayLib.Freeway.FreewayServers.Demo, Convert.ToString(freewayUser.Table.Rows[row]["FreewayServerUrl"]), GetProxy());
                }
                else
                {
                    return new LB.FreewayLib.Freeway.VojoIdentity(Convert.ToString(freewayUser.Table.Rows[row]["FreewayUsername"]),
                                Convert.ToString(freewayUser.Table.Rows[row]["FreewayPassword"]), LB.FreewayLib.Freeway.FreewayServers.Live, Convert.ToString(freewayUser.Table.Rows[row]["FreewayServerUrl"]), GetProxy());
                }
            }
        }
        else
        {
            if (Convert.ToString(freewayGlobalUser.Table.Rows[row]["FreewayServerUrl"]).ToLower().Contains(server.ToString().ToLower()))
            {
                return new LB.FreewayLib.Freeway.VojoIdentity(freewayGlobalUser["FreewayUsername"].ToString(),
                        freewayGlobalUser["FreewayPassword"].ToString(), LB.FreewayLib.Freeway.FreewayServers.Demo, freewayGlobalUser["FreewayServerUrl"].ToString(), GetProxy());
            }
            else
            {
                return new LB.FreewayLib.Freeway.VojoIdentity(freewayGlobalUser["FreewayUsername"].ToString(),
                            freewayGlobalUser["FreewayPassword"].ToString(), LB.FreewayLib.Freeway.FreewayServers.Live, freewayGlobalUser["FreewayServerUrl"].ToString(), GetProxy());
            }
        }
    }


    


    public static LB.FreewayLib.Freeway.VojoIdentity CreateFreewayIdentityOld07012015()
    {
        bool iGbl = true;
        DataRow freewayUser = FreewayDB.GetUserRow((long)new UserAPI().UserId, true);

        if (freewayUser != null)
        {
            iGbl = Convert.ToBoolean(freewayUser["ForceGlobalSettings"]);
        }
       
        DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(iGbl, (long)new UserAPI().UserId);
        string server = Enum.GetName(typeof(LB.FreewayLib.Freeway.FreewayServers), LB.FreewayLib.Freeway.FreewayServers.Demo);
        if (freewayGlobalUser == null) 
        {
            if (freewayUser == null)
                return null;
            else
            {
                if (freewayUser["FreewayServerUrl"].ToString().ToLower().Contains(server.ToString().ToLower()))
                {
                    return new LB.FreewayLib.Freeway.VojoIdentity(freewayUser["FreewayUsername"].ToString(),
                            freewayUser["FreewayPassword"].ToString(), LB.FreewayLib.Freeway.FreewayServers.Demo, freewayUser["FreewayServerUrl"].ToString(), GetProxy());
                }
                else
                {
                    return new LB.FreewayLib.Freeway.VojoIdentity(freewayGlobalUser["FreewayUsername"].ToString(),
                                freewayGlobalUser["FreewayPassword"].ToString(), LB.FreewayLib.Freeway.FreewayServers.Live, freewayGlobalUser["FreewayServerUrl"].ToString(), GetProxy());
                }
            }
        }
        else
        {
            if (freewayUser["FreewayServerUrl"].ToString().ToLower().Contains(server.ToString().ToLower()))
            {
                return new LB.FreewayLib.Freeway.VojoIdentity(freewayGlobalUser["FreewayUsername"].ToString(),
                        freewayGlobalUser["FreewayPassword"].ToString(), LB.FreewayLib.Freeway.FreewayServers.Demo, freewayGlobalUser["FreewayServerUrl"].ToString(), GetProxy());
            }
            else
            {
                return new LB.FreewayLib.Freeway.VojoIdentity(freewayGlobalUser["FreewayUsername"].ToString(),
                            freewayGlobalUser["FreewayPassword"].ToString(), LB.FreewayLib.Freeway.FreewayServers.Live, freewayGlobalUser["FreewayServerUrl"].ToString(), GetProxy());
            }
        }
    }

    public static System.Net.IWebProxy GetProxy()
    {
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["UseProxy"]))
            {
                System.Net.WebProxy proxy = new System.Net.WebProxy(Convert.ToString(ConfigurationManager.AppSettings["ProxyServer"]), Convert.ToInt32(ConfigurationManager.AppSettings["ProxyPort"]));

                if (Convert.ToString(ConfigurationManager.AppSettings["ProxyUsername"]).Length > 0)
                    proxy.Credentials = new System.Net.NetworkCredential(Convert.ToString(ConfigurationManager.AppSettings["ProxyUsername"]), Convert.ToString(ConfigurationManager.AppSettings["ProxyPassword"]));

                return proxy;
            }
            else
                return null;
        }
        catch (Exception ex)
        {

        }

        return null;
    }

    private static LB.FreewayLib.Freeway.FreewayServers _freewayServer;

    /// <summary>
    /// Returns FreewayServer information, currently it is Demo server.
    /// </summary>
	public static LB.FreewayLib.Freeway.FreewayServers FreewayServer
	{
		get { return LB.FreewayLib.Freeway.FreewayServers.Live; }
        set { _freewayServer = value; }
	}

    /// <summary>
    /// Returns Source Languages from Freeway
    /// </summary>
    public static Language[] SourceLanguages
    {
        get
        {
            LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
            Language[] vojoLanguages = LB.FreewayLib.Freeway.VojoServer.GetSourceLanguages(vojoIdentity);

            return vojoLanguages;
        }
    }


    /// <summary>
    /// Returns Target Languages based on the selected Source Language from Freeway
    /// </summary>
    public static Language[] TargetLanguages(string SelectedSourceLanguage)
    {
        LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
        Language[] vojoLanguages = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(SelectedSourceLanguage, vojoIdentity);
       return vojoLanguages;
    }

    /// <summary>
    /// Returns Tasks from Freeway
    /// </summary>
    public static LB.FreewayLib.Freeway.VojoTaskCollection Tasks
    {
        get 
        {
            LB.FreewayLib.Freeway.VojoIdentity vojoIdentity =  FreewayConfiguration.CreateFreewayIdentity();
            return (LB.FreewayLib.Freeway.VojoTasks.GetTasks(vojoIdentity)).AvailableTasks; 
        }
    }

    /// <summary>
    /// Returns Components from Freeway
    /// </summary>
    public static LB.FreewayLib.Freeway.VojoComponentCollection Components
    {
        get
        {
            LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
            return (LB.FreewayLib.Freeway.VojoTasks.GetTasks(vojoIdentity)).AvailableComponents;
        }
    }

    /// <summary>
    /// Returns Subjects from Freeway
    /// </summary>
    public static LB.FreewayLib.Freeway.VojoSubjectCollection Subjects
    {
        get
        {
            LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
            return (LB.FreewayLib.Freeway.VojoTasks.GetTasks(vojoIdentity)).AvailableSubjects;
        }
    }


    /// <summary>
    /// Returns SubTasks from Freeway based on the Task selected
    /// </summary>
    public static LB.FreewayLib.Freeway.VojoSubTaskCollection SubTasks(LB.FreewayLib.Freeway.VojoTask SelectedTask)
    {
            LB.FreewayLib.Freeway.VojoIdentity vojoIdentity =  FreewayConfiguration.CreateFreewayIdentity();
            return (new LB.FreewayLib.Freeway.VojoTask(SelectedTask,vojoIdentity)).SubTasks;
    }

    /// <summary>
    /// Returns Uoms from Freeway based on the Task selected
    /// </summary>
    public static LB.FreewayLib.Freeway.VojoUOMCollection Uoms(LB.FreewayLib.Freeway.VojoTask SelectedTask)
    {
            LB.FreewayLib.Freeway.VojoIdentity vojoIdentity =  FreewayConfiguration.CreateFreewayIdentity();
            return (new LB.FreewayLib.Freeway.VojoTask(SelectedTask, vojoIdentity)).Uoms;
    }

    /// <summary>
    /// Add the task to the specified project
    /// </summary>
    public static int AddTask(string projectid, string sourcelanguageid, string targetlanguageid, string componentid, string subjectsid, string taskid, string subtaskid, double volume, string uomid)
    {

	LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
	Vojo vojotask = LB.FreewayLib.Freeway.VojoServer.GetVojo(vojoIdentity);
        return vojotask.AddTaskToProject(vojoIdentity.GetTicket(), projectid,new LanguageIDMappingUtil().getEquivalentFreewayLangID( sourcelanguageid), new LanguageIDMappingUtil().getEquivalentFreewayLangID(targetlanguageid), componentid, subjectsid, taskid, subtaskid, volume, uomid);

        /*LB.FreewayLib.Freeway.VojoIdentity vojoIdentity =  FreewayConfiguration.CreateFreewayIdentity();
        Vojo vojotask = new Vojo();
        return vojotask.AddTaskToProject(vojoIdentity.GetTicket(), projectid, sourcelanguageid, targetlanguageid, componentid, subjectsid, taskid, subtaskid, volume, uomid);
	*/    
}

    /// <summary>
    /// Get the tasks of the specified project
    /// </summary>
    public static projectdetailLine[] GetTasks(string projectid)
    {
        LB.FreewayLib.Freeway.VojoIdentity vojoIdentity =  FreewayConfiguration.CreateFreewayIdentity();
        Vojo vojotask = LB.FreewayLib.Freeway.VojoServer.GetVojo(vojoIdentity);
        return       vojotask.GetProjectInformation(vojoIdentity.GetTicket(), projectid, true, "English").Tasks;
        
    }

    /// <summary>
    /// Get the tasks of the specified project
    /// </summary>
    public static FileStatusList GetFileStatus(string projectid)
    {
  	LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
        Vojo vojoFileStatus = LB.FreewayLib.Freeway.VojoServer.GetVojo(vojoIdentity);
        return vojoFileStatus.GetFileStatus(vojoIdentity.GetTicket(), projectid, string.Empty, string.Empty, string.Empty, string.Empty);

    }


  /*  public static FileStatus[] GetFileStatus1(string projectid)
    {
  	LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
	return LB.FreewayLib.Freeway.VojoServer.GetFileStatus(projectid, vojoIdentity);
        
    }
*/

    /// <summary>
    /// Checks the current version of the Ektron site 
    /// and returns the boolean value
    /// </summary>
	public static bool Version75OrGreater
	{
		get
		{
			try
			{
				double version = double.Parse(ConfigurationManager.AppSettings["ek_cmsversion"]);

				return version >= 7.5;
			}
			catch { }

			return false;
		}
	}

	#endregion
}
