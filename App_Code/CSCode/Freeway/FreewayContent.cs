#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  26 May 2010
Details :   Performs the Ektron content data related functionality 
---------------------------------
*/
#endregion

#region
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
using Ektron.Cms;
using System.Collections.Specialized;
using System.Web;
#endregion

/// <summary>
/// Summary description for FreewayContent
/// </summary>
public class FreewayContent
{
	#region Private Declaration

	private List<ContentData> _contents = new List<ContentData>();

	#endregion

	#region Constructor
    /// <summary>
    /// Returns the default instance of the FreewayContent class
    /// </summary>
	public FreewayContent()
	{
		return;
	}

	#endregion

	#region Public Members

    /// <summary>
    /// Adds the content from the Ektron localization session based on the source language selected
    /// </summary>
	public void LoadFromCurrentSession()
	{
		List<int> contentIds = HttpContext.Current.Session[FreewayConfiguration.LocalizationContentIdSessionName] as List<int>;

		if (contentIds != null)
		{
			ContentAPI contentApi = new ContentAPI();

			foreach (int contentId in contentIds)
			{
				LanguageData[] languages = contentApi.DisplayAddViewLanguage(contentId);

				ContentData contentData = contentApi.GetContentById(contentId, ContentAPI.ContentResultType.Published);

				if (contentData != null && !Contains(contentData))
					_contents.Add(contentData);
			}
		}

		return;
	}

    /// <summary>
    /// Adds the content from the query string passed for Ektron localization  
    /// based on the source language selected
    /// </summary>
    /// <param name="queryString"></param>
	public void LoadFromQueryString(NameValueCollection queryString)
	{
		string stringIds = queryString["CIDs"];

		if (!string.IsNullOrEmpty(stringIds))
		{
			string[] ids = stringIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

			ContentAPI contentApi = new ContentAPI();

			foreach (string id in ids)
			{
				int contentId;

				try
				{
					contentId = int.Parse(id);
				}
				catch
				{
					continue;
				}

				LanguageData[] languages = contentApi.DisplayAddViewLanguage(contentId);

				ContentData contentData = contentApi.GetContentById(contentId, ContentAPI.ContentResultType.Published);

				if (contentData != null && !Contains(contentData))
					_contents.Add(contentData);
			}
		}

		return;
	}

    /// <summary>
    /// Compares the contentData from the Ektron data repository
    /// and if found matching returns the boolean true 
    /// </summary>
    /// <param name="contentData"></param>
    /// <returns></returns>
	public bool Contains(ContentData contentData)
	{
		foreach (ContentData contentDataItem in _contents)
			if (contentDataItem.Id == contentData.Id)
				return true;

		return false;
	}

    /// <summary>
    /// Returns the content data list
    /// </summary>
	public List<ContentData> Contents
	{
		get { return _contents; }
	}

	#endregion
}
