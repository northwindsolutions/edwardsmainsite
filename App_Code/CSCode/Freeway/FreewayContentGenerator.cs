#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  26 May 2010
Details :   Creates the Freeway table showing Send to Freeway link
            and Project Id with status in Send To Freeway history page.  
---------------------------------
  
*/
#endregion
#region using
using System;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using DS = Ektron.Cms.LocalizationJobDataSet;
using System.Text;
using System.Globalization;
#endregion
/// <summary>
/// Summary description for FreewayContentGenerator
/// </summary>
public class FreewayContentGenerator
{
    #region constructor
    /// <summary>
    /// Returns the default instance of the FreewayContentGenerator class
    /// </summary>
    public FreewayContentGenerator()
	{
    }
    #endregion
    /// <summary>
    /// Generates the Freeway table showing Send to Freeway link
    /// and Project Id with status
    /// </summary>
    /// <param name="jobRow"></param>
    /// <returns>String</returns>
    public static string CreateLocalizationJobContent(DS.LocalizationJobRow jobRow)
	{
		StringBuilder sb = new StringBuilder();
        
		switch (jobRow.JobType)
		{
            case (int)DS.LocalizationJobRow.Types.ExportMenu:
            case (int)DS.LocalizationJobRow.Types.ExportFolder:
            case (int)DS.LocalizationJobRow.Types.Export:
            case (int)DS.LocalizationJobRow.Types.ExportContent:
            case (int)DS.LocalizationJobRow.Types.ExportTaxonomy:

            //case (int)DS.LocalizationJobRow.Types.CompressFiles:
            //case (int)DS.LocalizationJobRow.Types.Export:
            //case (int)DS.LocalizationJobRow.Types.ExportContent:
            //case (int)DS.LocalizationJobRow.Types.ExportFolder:
            //case (int)DS.LocalizationJobRow.Types.ExportMenu:
            //case (int)DS.LocalizationJobRow.Types.ExportTaxonomy:
            //case (int)DS.LocalizationJobRow.Types.ExtractText:
            //case (int)DS.LocalizationJobRow.Types.Import:
            //case (int)DS.LocalizationJobRow.Types.MergeText:
            //case (int)DS.LocalizationJobRow.Types.UncompressFiles:
				{
					if (jobRow.State != (int)DS.LocalizationJobRow.States.Complete)
						return string.Empty;                    

                        EktronProject ektronProject = new EktronProject(FreewayDB.GetProjectRowFromJobId(jobRow.JobID));
                        sb.Append(@"<div style=""border:solid 1px;width:145px;"">");
                        sb.Append(@"<b>Freeway</b><br />");
                        if (!string.IsNullOrEmpty(ektronProject.Id))
                        {
                            try
                            {
                                ektronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
                                    FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

                                sb.Append(String.Format(@"Project ID: <a id=""__gotoFreeway"" href=""{2}"" target=""_blank"">{0}</a><br />Status: {1}",
                                    ektronProject.Id, ektronProject.Status, LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl(ektronProject.Id,
                                    ektronProject.FreewayServer, FreewayConfiguration.CreateFreewayIdentity().UseSsl)));

                                if (ektronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Completed)
                                    sb.Append(String.Format(@" <a href=""RetrieveJobFromFreeway.aspx?jobid={0}&backurl={1}"">Retrieve...</a><br>",
                                        HttpUtility.UrlEncode(jobRow.JobID.ToString()), HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString())));
                            }
                            catch
                            {
                                sb.Append(String.Format(@"Project ID: <a id=""__gotoFreeway"" href=""{2}"" target=""_blank"">{0}</a><br />Status: {1}",
                                    ektronProject.Id, "Unknown", LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl(ektronProject.Id,
                                    ektronProject.FreewayServer, FreewayConfiguration.CreateFreewayIdentity().UseSsl)));
                            }
                        }
                        else
                        {
                             LocalizationAPI m_objLocalizationApi = new LocalizationAPI();                            
                             LocalizationJobDataSet.LocalizationJobDataTable jobsOfJobs = m_objLocalizationApi.GetJobs(jobRow.JobID);
                             DataRow[] dr = jobsOfJobs.Select("JobType=7");
                             if (dr.Length > 0)
                            sb.Append(String.Format(@" <a href=""SendCompletedJobToFreeway.aspx?jobid={0}&backurl={1}"">Add Content To Freeway Project</a><br>",
                                HttpUtility.UrlEncode(jobRow.JobID.ToString()), HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString())));
                            else
                            sb.Append(String.Format(@"<font style='color:black;text-decoration:none;'>XLIFF Files not generated</font>.<br>",
                        HttpUtility.UrlEncode(jobRow.JobID.ToString()), HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString())));
                        
                        }
                        sb.Append("</div>");
                  
					break;
				}
		}

		return sb.ToString();
	}

   
    

    private static void LocalizationAPI()
    {
        throw new Exception("The method or operation is not implemented.");
    }
}
