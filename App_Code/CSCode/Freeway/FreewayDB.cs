
#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  22 Oct 2009
Details : Modified the existing functionality of one file submittion in a Freeway Project
          To multiple files subbmittion in a Freeway Project.          
---------------------------------
  
*/
#endregion
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using System.Data.SqlClient;

/// <summary>
/// Summary description for FreewayDB
/// </summary>
public static class FreewayDB
{
    #region Private Members

    private const string GetProjectIdFromJobIdSelectCommand = "SELECT * FROM FreewayProjectTable WHERE JobId = @JobId";
    private const string UpdateVisible4ProjectIdCommand = "UPDATE FreewayProjectTable SET Visible = @Visible WHERE ProjectId = @ProjectId";
    private const string SelectProjectIdFromJobIdCommand = "SELECT * FROM FreewayProjectTable WHERE JobId = @JobId";
    private const string GetProjectStatusFromJobIdSelectCommand = "SELECT * FROM FreewayProjectTable WHERE JobId = @JobId";

    //private const string SelectProjectIdFromProjectIdCommand = "    SELECT ProjectId,jobid,createdby,createdwhen,submittedby,submittedwhen,submittedwhere,visible, " +
    //                                                            "   (SELECT ',' + Convert(varchar,jobid)    " +
    //                                                            "   FROM FreewayProjectTable    " +
    //                                                            "   where ProjectId = @ProjectId    " +
    //                                                            "   ORDER BY projectid  " +
    //                                                            "   FOR XML PATH('')) AS JobsAvailable  " +
    //                                                            "   FROM FreewayProjectTable    " +
    //                                                            "   where ProjectId = @ProjectId    ";

    private const string SelectProjectIdFromProjectIdCommand = "    SELECT ProjectId,jobid,createdby,createdwhen,submittedby,submittedwhen,submittedwhere,visible, " +
                                                                "   (SELECT ',' + Convert(varchar,jobid)    " +
                                                                "   FROM FreewayProjectTable    " +
                                                                "   where ProjectId = @ProjectId    " +
                                                                "   ORDER BY projectid  " +
                                                                "   FOR XML PATH('')) AS JobsAvailable , " +
                                                                "   (SELECT ',' + Convert(varchar(Max),XliffFileIds)    " +
                                                                "   FROM FreewayProjectTable    " +
                                                                "   where ProjectId = @ProjectId    " +
                                                                "   ORDER BY projectid  " +
                                                                "   FOR XML PATH('')) AS XliffFileIds  " +
                                                                "   FROM FreewayProjectTable    " +
                                                                "   where ProjectId = @ProjectId    ";

    // LB Mumbai 22 Oct 2009
    // Need to use Sql Stored Procedures rather that Inline Sql Statements 
    // as prone to SQL Injections
    // But still following the existing coding standards
    // New sql statements added to Insert the project on creation, Create the JOb ID on New Job creation
    // and update the Project row on project submission in Freeway
    private const string CreateProjectIdInsertCommand = "INSERT INTO FreewayProjectTable (ProjectId, JobId,CreatedBy,CreatedWhen, SubmittedBy, SubmittedWhen, SubmittedWhere, Visible) VALUES (@ProjectId, @JobId,@CreatedBy,@CreatedWhen, @SubmittedBy, @SubmittedWhen, @SubmittedWhere, @Visible)";
    private const string SubmitProjectIdUpdateCommand = "UPDATE FreewayProjectTable SET SubmittedBy = @SubmittedBy , SubmittedWhen = @SubmittedWhen , SubmittedWhere = @SubmittedWhere , Visible = @Visible WHERE ProjectId = @ProjectId";
    private const string CreateJobIdInsertCommand = "INSERT INTO FreewayProjectTable (ProjectId, JobId, CreatedBy, CreatedWhen,Visible) VALUES (@ProjectId, @JobId, @SubmittedBy, @SubmittedWhen, @Visible)";

    private const string UpdateProjectStatusUpdateCommand = "UPDATE FreewayProjectTable SET CurrentProjectStatus = @CurrentProjectStatus WHERE ProjectId = @ProjectId";
    //private const string InsertEktronUserIdCommand = "INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, SubmitWhere, UseSsl, CanSubmit, ForceGlobalSettings) VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword, @SubmitWhere, @UseSsl, @CanSubmit, @ForceGlobalSettings)";
    private const string UpdateEktronUserIdCommand = "UPDATE FreewayUsers SET FreewayUsername = @FreewayUsername, FreewayPassword = @FreewayPassword, SubmitWhere = @SubmitWhere, UseSsl = @UseSsl, CanSubmit = @CanSubmit, ForceGlobalSettings = @ForceGlobalSettings WHERE EktronUserId = @EktronUserId";
    private const string InsertFileRetrieveCommand = "INSERT INTO FreewayFileProjectRetrieval (ProjectId, FileId, RetrieveDate, RetrieveBy) VALUES (@ProjectId, @FileId, @RetrieveDate, @RetrieveBy)";
    private const string SelectFileRetrieveCommand = "SELECT * FROM FreewayFileProjectRetrieval WHERE ProjectId = @ProjectId";
    private const string SelectAllEktronUserIdCommand = "SELECT * FROM FreewayUsers where ForceGlobalSettings = 0";    
    //Added by Supriya  for the modifications of Version 2.0
    private const string InsertEktronUserIdCommand = "INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, SubmitWhere, UseSsl, CanSubmit, ForceGlobalSettings,CurrentEnvironment) VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword, @SubmitWhere, @UseSsl, @CanSubmit, @ForceGlobalSettings,@CurrentEnvironment)";
    private const string SelectAllEktronUserFreewayServerWiseCommand = "SELECT * FROM FreewayUsers where SubmitWhere=@SubmitWhere";
    private const string SelectEktronUsersFreewayServerWiseCommand = "SELECT * FROM FreewayUsers WHERE EktronUserId = @EktronUserId AND SubmitWhere=@SubmitWhere";
    private const string UpdateEktronUserFreewayServerWiseCommand = "UPDATE FreewayUsers SET FreewayUsername = @FreewayUsername, FreewayPassword = @FreewayPassword, UseSsl = @UseSsl, CanSubmit = @CanSubmit, ForceGlobalSettings = @ForceGlobalSettings,CurrentEnvironment=@CurrentEnvironment WHERE EktronUserId = @EktronUserId AND SubmitWhere = @SubmitWhere";
    private const string UpdateCurrentServerCommand = "UPDATE FreewayUsers SET CurrentEnvironment=@CurrentEnvironment WHERE EktronUserId = @EktronUserId ";
    private const string SelectEktronUserIdCommandEnv = "SELECT * FROM FreewayUsers WHERE EktronUserId = @EktronUserId AND SubmitWhere=CurrentEnvironment";
    private const string SelectEktronUserIdCommand = "SELECT * FROM FreewayUsers WHERE EktronUserId = @EktronUserId";
   

    #endregion

    #region Constructor

    static FreewayDB()
    {
        return;
    }

    #endregion

    #region Public Members
    /// <summary>
    /// Retruns the sql connection by retrieving the connection string from web.config
    /// </summary>
    /// <returns></returns>
    public static SqlConnection GetConnection()
    {
        //return new SqlConnection(ConfigurationManager.ConnectionStrings["FreewayConnectionString"].ConnectionString);
        return new SqlConnection(ConfigurationManager.ConnectionStrings["Ektron.DbConnection"].ConnectionString);
    }
    /// <summary>
    /// Retruns Project Id from Ektron data repository based on the jobid passed
    /// </summary>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public static int GetProjectIdFromJobId(long jobId)
    {
        SqlCommand command = new SqlCommand(GetProjectIdFromJobIdSelectCommand);

        command.Parameters.AddWithValue("@JobId", jobId);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0)
                    return 0;

                return (int)dataSet.Tables[0].Rows[0]["ProjectId"];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return 0;
    }
    /// <summary>
    /// Retruns Project data from Ektron data repository based on the projectid passed
    /// </summary>
    /// <param name="projectId"></param>
    /// <returns></returns>
    public static DataRow GetProjectRowFromProjectId(int projectId)
    {
        SqlCommand command = new SqlCommand(SelectProjectIdFromProjectIdCommand);

        command.Parameters.AddWithValue("@ProjectId", projectId);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                    return null;

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    public static DataRow GetProjectRowFromProjectId(string projectId)
    {
        SqlCommand command = new SqlCommand(SelectProjectIdFromProjectIdCommand);

        command.Parameters.AddWithValue("@ProjectId", projectId);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                    return null;

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    /// <summary>
    /// Retruns Project data from Ektron data repository based on the jobid passed
    /// </summary>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public static DataRow GetProjectRowFromJobId(long jobId)
    {
        SqlCommand command = new SqlCommand(SelectProjectIdFromJobIdCommand);

        command.Parameters.AddWithValue("@JobId", jobId);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                    return null;

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }
    /// <summary>
    /// Set the visibility status of the project in Ektron data repository.
    /// </summary>
    /// <param name="projectId"></param>
    /// <param name="visible"></param>
    /// <returns></returns>
    public static bool SetProjectVisible(int projectId, bool visible)
    {
        SqlCommand command = new SqlCommand(UpdateVisible4ProjectIdCommand);

        command.Parameters.AddWithValue("@ProjectId", projectId);
        command.Parameters.AddWithValue("@Visible", visible);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                return command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return false;
    }


    public static bool SetProjectVisible(string projectId, bool visible)
    {
        SqlCommand command = new SqlCommand(UpdateVisible4ProjectIdCommand);

        command.Parameters.AddWithValue("@ProjectId", projectId);
        command.Parameters.AddWithValue("@Visible", visible);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                return command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return false;
    }

    /// <summary>
    /// LB Mumbai 22 Oct 2009
    /// Inserts the record in Freeway DB on Project creation in Freeway
    /// </summary>
    /// <param name="projectId"></param>
    /// <param name="jobId"></param>
    /// <param name="submittedBy"></param>
    /// <param name="submittedWhere"></param>
    public static void CreateProjectId(int projectId, long jobId, string createby, string submittedWhere)
    {
        SqlCommand command = new SqlCommand(CreateProjectIdInsertCommand);

        command.Parameters.AddWithValue("ProjectId", projectId);
        command.Parameters.AddWithValue("JobId", DBNull.Value);
        command.Parameters.AddWithValue("CreatedBy", createby);
        command.Parameters.AddWithValue("CreatedWhen", DateTime.Now);
        command.Parameters.AddWithValue("SubmittedBy", DBNull.Value);
        command.Parameters.AddWithValue("SubmittedWhen", DBNull.Value);
        command.Parameters.AddWithValue("SubmittedWhere", submittedWhere);
        command.Parameters.AddWithValue("Visible", true);

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }

    /// <summary>
    /// LB Mumbai 22 Oct 2009
    /// Update the record in Freeway DB on Project submission in Freeway
    /// </summary>
    /// <param name="projectId"></param>
    /// <param name="submittedBy"></param>
    /// <param name="submittedWhere"></param>
    public static void SubmitProjectId(int projectId, string submittedBy, string submittedWhere)
    {
        SqlCommand command = new SqlCommand(SubmitProjectIdUpdateCommand);

        command.Parameters.AddWithValue("ProjectId", projectId);
        command.Parameters.AddWithValue("SubmittedBy", submittedBy);
        command.Parameters.AddWithValue("SubmittedWhen", DateTime.Now);
        command.Parameters.AddWithValue("SubmittedWhere", submittedWhere);
        command.Parameters.AddWithValue("Visible", true);

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }



     public static void SubmitProjectId(string projectId, string submittedBy, string submittedWhere)
    {
        SqlCommand command = new SqlCommand(SubmitProjectIdUpdateCommand);

        command.Parameters.AddWithValue("ProjectId", projectId);
        command.Parameters.AddWithValue("SubmittedBy", submittedBy);
        command.Parameters.AddWithValue("SubmittedWhen", DateTime.Now);
        command.Parameters.AddWithValue("SubmittedWhere", submittedWhere);
        command.Parameters.AddWithValue("Visible", true);

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }

    ///// <summary>
    ///// LB Mumbai 22 Oct 2009
    ///// Insert the record in Freeway DB on adding the new File in Freeway Project
    ///// </summary>
    ///// <param name="projectId"></param>
    ///// <param name="jobId"></param>
    ///// <param name="submittedBy"></param>
    //public static void CreateJobId(int projectId, long jobId, string submittedBy)
    //{

    //    string CreateJobIdInsertCommandtext = "If Exists (Select ProjectId From FreewayProjectTable WHERE ProjectId = @ProjectId and JobId = " + Convert.ToString(Int64.MinValue) +
    //                                                    ") Update FreewayProjectTable set JobId=@JobId where ProjectId=@ProjectId and JobId = " + Convert.ToString(Int64.MinValue) +
    //                                                    " else INSERT INTO FreewayProjectTable (ProjectId, JobId, CreatedBy, CreatedWhen,Visible) VALUES (@ProjectId, @JobId, @SubmittedBy, @SubmittedWhen, @Visible)";

    //    SqlCommand command = new SqlCommand(CreateJobIdInsertCommandtext);

    //    command.Parameters.AddWithValue("ProjectId", projectId);
    //    command.Parameters.AddWithValue("JobId", jobId != 0 ? jobId : long.MinValue);
    //    command.Parameters.AddWithValue("SubmittedBy", submittedBy);
    //    command.Parameters.AddWithValue("SubmittedWhen", DateTime.Now);
    //    command.Parameters.AddWithValue("Visible", true);


    //    command.Connection = GetConnection();

    //    using (command.Connection)
    //    {
    //        command.Connection.Open();

    //        try
    //        {
    //            command.ExecuteNonQuery();
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine(ex.Message);
    //        }
    //        finally
    //        {
    //            command.Connection.Close();
    //        }
    //    }

    //    return;
    //}

    /// <summary>
    /// LB Mumbai 22 Oct 2009
    /// Insert the record in Freeway DB on adding the new File in Freeway Project
    /// </summary>
    /// <param name="projectId"></param>
    /// <param name="jobId"></param>
    /// <param name="submittedBy"></param>
    public static void CreateJobId(int projectId, long jobId, string submittedBy, string strcommaseparatedxlifffileids)
    {

        string CreateJobIdInsertCommandtext = "If Exists (Select ProjectId From FreewayProjectTable WHERE ProjectId = @ProjectId and JobId = " + Convert.ToString(Int64.MinValue) +
                                                        ") Update FreewayProjectTable set JobId=@JobId,XliffFileIds = @XliffFileIds  where ProjectId=@ProjectId and JobId = " + Convert.ToString(Int64.MinValue) +
                                                        " else INSERT INTO FreewayProjectTable (ProjectId, JobId,XliffFileIds, CreatedBy, CreatedWhen,Visible) VALUES (@ProjectId, @JobId, @XliffFileIds, @SubmittedBy, @SubmittedWhen, @Visible)";

        SqlCommand command = new SqlCommand(CreateJobIdInsertCommandtext);

        command.Parameters.AddWithValue("ProjectId", projectId);
        command.Parameters.AddWithValue("JobId", jobId != 0 ? jobId : long.MinValue);
        command.Parameters.AddWithValue("XliffFileIds", strcommaseparatedxlifffileids.Length != 0 ? strcommaseparatedxlifffileids : string.Empty);
        command.Parameters.AddWithValue("SubmittedBy", submittedBy);
        command.Parameters.AddWithValue("SubmittedWhen", DateTime.Now);
        command.Parameters.AddWithValue("Visible", true);


        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                command.Connection.Close();
            }
        }

        return;
    }


    public static void CreateJobId(string projectId, long jobId, string submittedBy, string strcommaseparatedxlifffileids)
    {

        string CreateJobIdInsertCommandtext = "If Exists (Select ProjectId From FreewayProjectTable WHERE ProjectId = @ProjectId and JobId = " + Convert.ToString(Int64.MinValue) +
                                                        ") Update FreewayProjectTable set JobId=@JobId,XliffFileIds = @XliffFileIds  where ProjectId=@ProjectId and JobId = " + Convert.ToString(Int64.MinValue) +
                                                        " else INSERT INTO FreewayProjectTable (ProjectId, JobId,XliffFileIds, CreatedBy, CreatedWhen,Visible) VALUES (@ProjectId, @JobId, @XliffFileIds, @SubmittedBy, @SubmittedWhen, @Visible)";

        SqlCommand command = new SqlCommand(CreateJobIdInsertCommandtext);

        command.Parameters.AddWithValue("ProjectId", projectId);
        command.Parameters.AddWithValue("JobId", jobId != 0 ? jobId : long.MinValue);
        command.Parameters.AddWithValue("XliffFileIds", strcommaseparatedxlifffileids.Length != 0 ? strcommaseparatedxlifffileids : string.Empty);
        command.Parameters.AddWithValue("SubmittedBy", submittedBy);
        command.Parameters.AddWithValue("SubmittedWhen", DateTime.Now);
        command.Parameters.AddWithValue("Visible", true);


        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                command.Connection.Close();
            }
        }

        return;
    }

    /// <summary>
    /// Get freeway user details from ektronUserId and create new user if not found
    /// </summary>
    /// <param name="ektronUserId"></param>
    /// <param name="createIfNotFound"></param>
    /// <returns></returns>
    public static DataRow GetUserRow(long ektronUserId, bool createIfNotFound)
    {
        SqlCommand command = new SqlCommand(SelectEktronUserIdCommand);

        command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                {
                    if (createIfNotFound && ektronUserId != -1)
                    {
                        //Need to check if this needs to be actually commented
                        //CreateUserRow(ektronUserId);

                        return GetUserRow(ektronUserId, false);
                    }

                    return null;
                }

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    public static DataRow GetGlobalUserRow(bool isGlobal,long ektronUserId)
    {
        //Modifcation for Edwards Vaccum for Global User Mode
        //string selectGlobalUser = "Select top 1 * from FreewayUsers where ForceGlobalSettings = @IsGLobal and EktronUserId = @ektronUserId ";
        string selectGlobalUser = "Select top 1 * from FreewayUsers where ForceGlobalSettings = @IsGLobal ";
        SqlCommand command = new SqlCommand(selectGlobalUser);
        command.Parameters.AddWithValue("@IsGLobal", isGlobal);
        command.Parameters.AddWithValue("@ektronUserId", ektronUserId);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    public static DataRow GetGlobalUserRow(bool isGlobal)
    {
        string selectGlobalUser = "Select top 1 * from FreewayUsers where ForceGlobalSettings = @IsGLobal";
        SqlCommand command = new SqlCommand(selectGlobalUser);
        command.Parameters.AddWithValue("@IsGLobal", isGlobal);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    /// <summary>
    /// Get freeway user details freeway server wise  from ektronUserId and create new user if not found 
    /// </summary>
    /// <param name="ektronUserId"></param>
    /// <param name="createIfNotFound"></param>
    /// <returns></returns>
    public static DataRow GetUserRowFreewayServerWise(long ektronUserId, bool createIfNotFound, string SubmitWhere)
    {
        SqlCommand command = new SqlCommand(SelectEktronUserIdCommand);

        command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
       //This code has been commented because SubmitWhere is no loinger required
        // command.Parameters.AddWithValue("@SubmitWhere", SubmitWhere);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                {
                    if (createIfNotFound && ektronUserId != -1)
                    {
                        CreateUserRow(ektronUserId);

                        return GetUserRow(ektronUserId, false);
                    }

                    return null;
                }

                return dataSet.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    //Created by supriya To get All the users freeway server wise
    public static DataTable GetAllUsersFreewayServerWise(string SubmitWhere)
    {
        SqlCommand command = new SqlCommand(SelectAllEktronUserFreewayServerWiseCommand);
        command.Parameters.AddWithValue("@SubmitWhere", SubmitWhere);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

    public static DataTable GetAllUserRow()
    {
        SqlCommand command = new SqlCommand(SelectAllEktronUserIdCommand);

        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                if (da.Fill(dataSet) == 0 || dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }

   

    /// <summary>
    /// Create Freeway user mapping settings in Ektron and 
    /// maintains in Ektron data repository 
    /// </summary>
    /// <param name="ektronUserId"></param>
    private static void CreateUserRow(long ektronUserId)
    {
        SqlCommand command = new SqlCommand(InsertEktronUserIdCommand);

        command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
        command.Parameters.AddWithValue("@FreewayUsername", string.Empty);
        command.Parameters.AddWithValue("@FreewayPassword", string.Empty);
        command.Parameters.AddWithValue("@SubmitWhere", "Demo");
        command.Parameters.AddWithValue("@UseSsl", false);
        command.Parameters.AddWithValue("@CanSubmit", true);
        command.Parameters.AddWithValue("@ForceGlobalSettings", false);
        command.Parameters.AddWithValue("@CurrentEnvironment", string.Empty);

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }

    public static void DeleteUserRow(long ektronUserId, string freewayUserName,bool isGlobal)
    {
        //Modification for edwards Vaccum for Global Mode
        //string DeleteEktronUserIdCommand = "Delete from FreewayUsers where EktronUserId =@EktronUserId and FreewayUsername = @FreewayUsername and ForceGlobalSettings = @ForceGlobalSettings";
        string DeleteEktronUserIdCommand = "Delete from FreewayUsers where ForceGlobalSettings = @ForceGlobalSettings";

        SqlCommand command = new SqlCommand(DeleteEktronUserIdCommand);

        //command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
        //command.Parameters.AddWithValue("@FreewayUsername", freewayUserName);
        command.Parameters.AddWithValue("@ForceGlobalSettings", isGlobal);        
        //command.Parameters.AddWithValue("@SubmitWhere", "Demo");
        //command.Parameters.AddWithValue("@CurrentEnvironment", "Demo");

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }



    public static void CreateUserRow(long ektronUserId, string freewayUserName, string freewayPassword,string freewayServerUrl, bool isGlobal, string origionalFreewayUserName, string OrigionalFreewayPassword)
    {

        //string CreateInsertEktronUserIdCommand = "if exists(select EktronUserId from FreewayUsers where EktronUserId =@EktronUserId and FreewayUsername=@OrigionalfreewayUsername and FreewayPassword=@OrigionalfreewayPassword) " +
        //   "update FreewayUsers set FreewayUsername=@FreewayUsername,FreewayPassword=@FreewayPassword, ForceGlobalSettings = @ForceGlobalSettings,FreewayServerUrl = @FreewayServerUrl where EktronUserId = @EktronUserId " +
        //   "else INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, ForceGlobalSettings,FreewayServerUrl) " +
        //  

        string CreateInsertEktronUserIdCommand = "if exists(select EktronUserId from FreewayUsers where EktronUserId =@EktronUserId and ForceGlobalSettings = @ForceGlobalSettings) " +
           "update FreewayUsers set FreewayUsername=@FreewayUsername,FreewayPassword=@FreewayPassword, ForceGlobalSettings = @ForceGlobalSettings,FreewayServerUrl = @FreewayServerUrl where EktronUserId = @EktronUserId and ForceGlobalSettings = @ForceGlobalSettings " +
           "else INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, ForceGlobalSettings,FreewayServerUrl) " +
           "VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword,@ForceGlobalSettings,@FreewayServerUrl)";

        SqlCommand command = new SqlCommand(CreateInsertEktronUserIdCommand);

        command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
        command.Parameters.AddWithValue("@FreewayUsername", freewayUserName);
        command.Parameters.AddWithValue("@FreewayPassword", freewayPassword);
        command.Parameters.AddWithValue("@ForceGlobalSettings", isGlobal);
        command.Parameters.AddWithValue("@OrigionalfreewayUsername", origionalFreewayUserName);
        command.Parameters.AddWithValue("@OrigionalfreewayPassword", OrigionalFreewayPassword);
        command.Parameters.AddWithValue("@FreewayServerUrl", freewayServerUrl);

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }
    public static void CreateUserRowForGlobal(long ektronUserId, string freewayUserName, string freewayPassword,string freewayServerUrl, bool isGlobal)
    {
        //string CreateInsertEktronUserIdCommand = "if exists(select EktronUserId from FreewayUsers where EktronUserId =@EktronUserId and ForceGlobalSettings = 1) " +
        //  "update FreewayUsers set FreewayUsername=@FreewayUsername,FreewayPassword=@FreewayPassword, ForceGlobalSettings = @ForceGlobalSettings,FreewayServerUrl = @FreewayServerUrl where EktronUserId = @EktronUserId " +
        //  "else INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, ForceGlobalSettings,FreewayServerUrl) " +
        //  "VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword,@ForceGlobalSettings,@FreewayServerUrl)";

        string CreateInsertEktronUserIdCommand = "if exists(select EktronUserId from FreewayUsers where EktronUserId =@EktronUserId and ForceGlobalSettings = 1) " +
       "update FreewayUsers set FreewayUsername=@FreewayUsername,FreewayPassword=@FreewayPassword,FreewayServerUrl = @FreewayServerUrl where EktronUserId = @EktronUserId " +
       "else INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, ForceGlobalSettings,FreewayServerUrl) " +
       "VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword,@ForceGlobalSettings,@FreewayServerUrl)";

        SqlCommand command = new SqlCommand(CreateInsertEktronUserIdCommand);

        command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
        command.Parameters.AddWithValue("@FreewayUsername", freewayUserName);
        command.Parameters.AddWithValue("@FreewayPassword", freewayPassword);
        command.Parameters.AddWithValue("@ForceGlobalSettings", isGlobal);
        command.Parameters.AddWithValue("@FreewayServerUrl", freewayServerUrl);
        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }
   
    public static void RemoveForceGlobalSetting(bool globalSetting)
    {
        string UpdateGlobalCommand = "update FreewayUsers set ForceGlobalSettings = @ForceGlobalSettings where ForceGlobalSettings = 1";
        SqlCommand command = new SqlCommand(UpdateGlobalCommand);
        command.Parameters.AddWithValue("@ForceGlobalSettings", globalSetting);
        command.Connection = GetConnection();
        using (command.Connection)
        {
            command.Connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }
        return;
    }


    //public static void CreateUserRowForGlobal(long ektronUserId, string freewayUserName, string freewayPassword,string freewayServerUrl, bool isGlobal)
    //{

    //    //string CreateInsertEktronUserIdCommand = "if exists(select EktronUserId from FreewayUsers where EktronUserId =@EktronUserId and FreewayUsername=@FreewayUsername and FreewayPassword=@FreewayPassword) " +
    //    //   "update FreewayUsers set FreewayUsername=@FreewayUsername,FreewayPassword=@FreewayPassword, ForceGlobalSettings = @ForceGlobalSettings,FreewayServerUrl = @FreewayServerUrl where EktronUserId = @EktronUserId " +
    //    //   "else INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, ForceGlobalSettings,FreewayServerUrl) " +
    //    //   "VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword,@ForceGlobalSettings,@FreewayServerUrl)";

    //    string CreateInsertEktronUserIdCommand = "if exists(select EktronUserId from FreewayUsers where EktronUserId =@EktronUserId) " +
    //      "update FreewayUsers set FreewayUsername=@FreewayUsername,FreewayPassword=@FreewayPassword, ForceGlobalSettings = @ForceGlobalSettings,FreewayServerUrl = @FreewayServerUrl where EktronUserId = @EktronUserId " +
    //      "else INSERT INTO FreewayUsers (EktronUserId, FreewayUsername, FreewayPassword, ForceGlobalSettings,FreewayServerUrl) " +
    //      "VALUES (@EktronUserId, @FreewayUsername, @FreewayPassword,@ForceGlobalSettings,@FreewayServerUrl)";

    //    SqlCommand command = new SqlCommand(CreateInsertEktronUserIdCommand);

    //    command.Parameters.AddWithValue("@EktronUserId", ektronUserId);
    //    command.Parameters.AddWithValue("@FreewayUsername", freewayUserName);
    //    command.Parameters.AddWithValue("@FreewayPassword", freewayPassword);
    //    command.Parameters.AddWithValue("@ForceGlobalSettings", isGlobal);
    //    command.Parameters.AddWithValue("@FreewayServerUrl", freewayServerUrl);
    //    command.Connection = GetConnection();

    //    using (command.Connection)
    //    {
    //        command.Connection.Open();

    //        try
    //        {
    //            command.ExecuteNonQuery();
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine(ex.Message);
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    return;
    //}

   
    /// <summary>
    /// Updates Freeway User mapping as per Freeway server in Ektron data repository 
    /// </summary>
    /// <param name="userRow"></param>
    /// <returns></returns>
    public static bool UpdateUserRowFreewayServerWise(DataRow userRow)
    {
        SqlCommand command = new SqlCommand(UpdateEktronUserFreewayServerWiseCommand);

        command.Parameters.AddWithValue("@EktronUserId", userRow["EktronUserId"]);
        command.Parameters.AddWithValue("@FreewayUsername", userRow["FreewayUsername"]);
        command.Parameters.AddWithValue("@FreewayPassword", userRow["FreewayPassword"]);
        command.Parameters.AddWithValue("@SubmitWhere", userRow["SubmitWhere"]);
        command.Parameters.AddWithValue("@UseSsl", userRow["UseSsl"]);
        command.Parameters.AddWithValue("@CanSubmit", userRow["CanSubmit"]);
        command.Parameters.AddWithValue("@ForceGlobalSettings", userRow["ForceGlobalSettings"]);
        command.Parameters.AddWithValue("@CurrentEnvironment", userRow["CurrentEnvironment"]);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                //Modified by supriya as commented code was giving error.
                command.Connection.Open();
                int isUpdated = command.ExecuteNonQuery();
                if (isUpdated == 0)
                {
                    command.CommandText = InsertEktronUserIdCommand;
                    command.ExecuteNonQuery();
                }
                if (Convert.ToInt64( userRow["EktronUserId"] )!= -1)
                {
                    SqlCommand command1 = new SqlCommand(UpdateCurrentServerCommand);
                    command1.Parameters.AddWithValue("@EktronUserId", userRow["EktronUserId"]);
                    command1.Parameters.AddWithValue("@CurrentEnvironment", userRow["CurrentEnvironment"]);
                    command1.Connection = GetConnection();
                    command1.Connection.Open();
                    isUpdated = command1.ExecuteNonQuery();
                }
                //DataSet dataSet = new DataSet();
                //SqlDataAdapter da = new SqlDataAdapter();

                //// specify and execute the SQL Query.
                ////da.SelectCommand = command;
                //da.UpdateCommand = command;
                //da.Update(userRow);
                //return da.Update(new DataRow[] { userRow }) > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }
            finally
            {
            }

        return true;
    }

    /// <summary>
    /// Updates Freeway User mapping settings in Ektron data repository 
    /// </summary>
    /// <param name="userRow"></param>
    /// <returns></returns>
    public static bool UpdateUserRow(DataRow userRow)
    {
        SqlCommand command = new SqlCommand(UpdateEktronUserIdCommand);

        command.Parameters.AddWithValue("@EktronUserId", userRow["EktronUserId"]);
        command.Parameters.AddWithValue("@FreewayUsername", userRow["FreewayUsername"]);
        command.Parameters.AddWithValue("@FreewayPassword", userRow["FreewayPassword"]);
        command.Parameters.AddWithValue("@SubmitWhere", userRow["SubmitWhere"]);
        command.Parameters.AddWithValue("@UseSsl", userRow["UseSsl"]);
        command.Parameters.AddWithValue("@CanSubmit", userRow["CanSubmit"]);
        command.Parameters.AddWithValue("@ForceGlobalSettings", userRow["ForceGlobalSettings"]);
        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                //Modified by supriya as commented code was giving error.
                command.Connection.Open();
                int isUpdated = command.ExecuteNonQuery();
                if (isUpdated == 0)
                {
                    command.CommandText = InsertEktronUserIdCommand;
                    command.ExecuteNonQuery();
                }
                //DataSet dataSet = new DataSet();
                //SqlDataAdapter da = new SqlDataAdapter();

                //// specify and execute the SQL Query.
                ////da.SelectCommand = command;
                //da.UpdateCommand = command;
                //da.Update(userRow);
                //return da.Update(new DataRow[] { userRow }) > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }
            finally
            {
            }

        return true;
    }
    /// <summary>
    /// Retrieves the history of files data retrieval available for the project 
    /// in Ektron data repository based on the projectid
    /// </summary>
    /// <param name="projectId"></param>
    /// <returns></returns>
    public static DataSet GetFileRetrievalPerProject(string projectId)
    {
        SqlCommand command = new SqlCommand(SelectFileRetrieveCommand);

        command.Connection = GetConnection();

        using (command.Connection)
            try
            {
                //command.Parameters.AddWithValue("ProjectId", int.Parse(projectId));
                command.Parameters.AddWithValue("ProjectId", projectId);

                command.Connection.Open();

                DataSet dataSet = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                // specify and execute the SQL Query.
                da.SelectCommand = command;
                da.Fill(dataSet);

                return dataSet;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }

        return null;
    }
    /// <summary>
    /// Update the file retrieval information in Ektron data repository.
    /// </summary>
    /// <param name="projectId"></param>
    /// <param name="fileId"></param>
    /// <param name="retrieveBy"></param>
    public static void UpdateFileRetrieval(string projectId, string fileId, string retrieveBy)
    {
        SqlCommand command = new SqlCommand(InsertFileRetrieveCommand);

        command.Connection = GetConnection();

        using (command.Connection)
        {
            command.Connection.Open();

            //command.Parameters.AddWithValue("ProjectId", int.Parse(projectId));
            command.Parameters.AddWithValue("ProjectId", projectId);
            //command.Parameters.AddWithValue("FileId", int.Parse(fileId));
            command.Parameters.AddWithValue("FileId", fileId);
            command.Parameters.AddWithValue("RetrieveDate", DateTime.Now);
            command.Parameters.AddWithValue("RetrieveBy", retrieveBy);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        return;
    }

    #endregion
}
