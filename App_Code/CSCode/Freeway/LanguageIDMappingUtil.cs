﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web;

/// <summary>
/// Summary description for LanguageIDMappingUtil
/// </summary>
public class LanguageIDMappingUtil
{
	public LanguageIDMappingUtil()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string getEquivalentFreewayLangID(string ektronLangID)
    {

        string langId = ektronLangID;

        if (WebConfigurationManager.AppSettings[ektronLangID] != null)
        {
            langId = WebConfigurationManager.AppSettings[ektronLangID];
        }

        return langId;
    }

    public string getEquivalentEktronLangID(string freewayLangID)
    {
        string langId = freewayLangID;

        string[] keyArray = WebConfigurationManager.AppSettings.AllKeys;

        if (keyArray!=null && keyArray.Length > 0)
        {
            foreach (string key in keyArray)
            {
                if (WebConfigurationManager.AppSettings[key].ToLower().Equals(freewayLangID.ToLower()))
                {
                    langId = key;
                }
            }
        }

        return langId;
    }
}