#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  26 May 2010
Details :        
---------------------------------
  
*/
#endregion
#region using
using System;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
#endregion
/// <summary>
/// Summary description for EktronProject
/// </summary>
public class ProjectDetails
{
    #region Private Members
    private string _projectDescription;
	private string _poReference;
	private string _specialInstruction;
    #endregion
    #region Constructo
    public ProjectDetails()
	{
    }
    #endregion
    #region Public properties
    public string ProjectDescription
	{
		get { return _projectDescription; }
		set { _projectDescription = value; }
	}

	public string POReference
	{
		get { return _poReference; }
		set { _poReference = value; }
	}

	public string SpecialInstruction
	{
		get { return _specialInstruction; }
		set { _specialInstruction = value; }
	}
    #endregion
}
