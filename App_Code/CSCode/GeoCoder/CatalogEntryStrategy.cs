﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ektron.Cms.Commerce;
using Ektron.Cms.Extensibility;
using Ektron.Cms.API;
using Ektron.Cms;
using System.Diagnostics;

namespace Cms.Extensions.GoogleGeoCoder
{
    public class CatalogEntryStrategy : Ektron.Cms.Extensibility.Commerce.CatalogEntryStrategy
    {
        #region Member Variables
        private string Latitude = string.Empty;
        private string Longitude = string.Empty;
        #endregion
        public override void OnAfterAdd(EntryData entryData, CmsEventArgs eventArgs)
        {
            FillMapCoordinates(entryData);
        }
        public override void OnAfterUpdate(EntryData entryData, CmsEventArgs eventArgs)
        {
            FillMapCoordinates(entryData);
        }
        public void FillMapCoordinates(EntryData entryData)
        {
            Ektron.Cms.API.Content.Content contentApi = new Ektron.Cms.API.Content.Content();
            Ektron.Cms.API.Metadata metaDataApi = new Metadata();
            try
            {
                metaDataApi.RequestInformationRef.ContentLanguage = entryData.LanguageId;
                CustomAttributeList list = metaDataApi.GetContentMetadataList(entryData.Id);
                CustomAttribute address = null;
                CustomAttribute mdLatitude = null;
                CustomAttribute mdLongitude = null;
                foreach (CustomAttribute item in list.ToArray())
                {
                    if ((item.Name.ToLower() == "mapaddress"))
                    {
                        address = item;
                    }
                    else if ((item.Name.ToLower() == "maplatitude"))
                    {
                        mdLatitude = item;
                    }
                    else if ((item.Name.ToLower() == "maplongitude"))
                    {
                        mdLongitude = item;
                    }
                }
                if (((address != null && !string.IsNullOrEmpty(address.Value.ToString().Trim()))))
                {
                    // Make sure we have meta tags
                    Helper.GetResults(address.Value.ToString(), out Longitude, out Latitude);
                    if ((!string.IsNullOrEmpty(this.Latitude) & !string.IsNullOrEmpty(this.Longitude)))
                    {
                        //set the latitude and longitude metadata
                        if ((mdLatitude != null))
                        {
                            long latitudeId = 0;
                            Int64.TryParse(mdLatitude.Id.ToString(), out latitudeId);
                            contentApi.UpdateContentMetaData(entryData.Id, latitudeId, Latitude);

                        }
                        if (mdLongitude != null)
                        {
                            long longitudeId = 0;
                            Int64.TryParse(mdLongitude.Id.ToString(), out longitudeId);
                            contentApi.UpdateContentMetaData(entryData.Id, longitudeId, Longitude);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToEventLog("Error: " + ex.ToString(), EventLogEntryType.Error);
            }
        }
    }

}
