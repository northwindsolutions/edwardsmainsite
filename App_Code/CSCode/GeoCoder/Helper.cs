﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Web;
using System.Net;
using System.Xml;
using System.Text.RegularExpressions;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Extensibility;
using System.Runtime.InteropServices;

namespace Cms.Extensions.GoogleGeoCoder
{
    public class Helper
    {
        #region variables
        protected static string GOOGLEMAPHOST = "http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=true";
        protected static Regex ns = new Regex("\\bxmlns\\s*=\\s*[\"'][^\"']*[\"']");
        #endregion

        public static void GetResults(string Address, out  string Longitude, out  string Latitude)
        {
            Latitude = string.Empty;
            Longitude = string.Empty;
            WebClient client = new WebClient();
            string pageHtml = string.Empty;
            string Status = string.Empty;
            string GoogleUrl = string.Format(GOOGLEMAPHOST, System.Web.HttpUtility.UrlEncode(Address));
            try
            {
                Byte[] pageData = client.DownloadData(GoogleUrl);
                pageHtml = System.Text.Encoding.ASCII.GetString(pageData);
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                //pageHtml = pageHtml.Replace("xmlns=""http://earth.google.com/kml/2.0""", "") ' Remove the namespace
                pageHtml = ns.Replace(pageHtml, string.Empty);
                // Removes all the xmlns
                xmlDoc.LoadXml(pageHtml.ToLower());
                // Load the XML from google, their element names are not all lower case
                //Get the status code from the response XML
                Status = xmlDoc.SelectSingleNode("geocoderesponse/status").InnerText.ToString(); 
                // ok is good, 602 did not get a good address
                if ((Status.ToLower() == "ok"))
                {
                    Longitude = xmlDoc.SelectSingleNode("geocoderesponse/result/geometry/location/lng").InnerText.ToString();
                    Latitude = xmlDoc.SelectSingleNode("geocoderesponse/result/geometry/location/lat").InnerText.ToString();
                }
                else
                {
                    WriteToEventLog(" Google Status: " + Status.ToString() + ", Google result:" + pageHtml.ToLower(), EventLogEntryType.Information);
                }
            }
            catch (XmlException ex)
            {
                WriteToEventLog("Response received: " + pageHtml + ex.Message.ToString(), EventLogEntryType.Error);
            }
            catch (WebException ex)
            {
                WriteToEventLog("Check your internet connection : " + ex.Message.ToString(), EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                // In case google is down
                WriteToEventLog("Error[GetResults]: " + ex.ToString(), EventLogEntryType.Error);
            }
            finally
            {
            }
        }
        public static void GetResults(string Address, out  string Longitude, out  string Latitude, out string Woeid)
        {
            Woeid = string.Empty;
            Latitude = string.Empty;
            Longitude = string.Empty;
            WebClient client = new WebClient();
            string pageHtml = string.Empty;
            string Status = string.Empty;
            string GoogleUrl = string.Format(GOOGLEMAPHOST, System.Web.HttpUtility.UrlEncode(Address));
            try
            {
                Byte[] pageData = client.DownloadData(GoogleUrl);
                pageHtml = System.Text.Encoding.ASCII.GetString(pageData);
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                pageHtml = ns.Replace(pageHtml, string.Empty);
                xmlDoc.LoadXml(pageHtml.ToLower());
                Status = xmlDoc.SelectSingleNode("/kml/response/status/code").InnerText.ToString();
                // 200 is good, 602 did not get a good address
                if ((Status == "200"))
                {
                    string[] coordinates = xmlDoc.SelectSingleNode("/kml/response/placemark/point/coordinates").InnerText.ToString().Split(',');
                    if (coordinates.Length > 1)
                    {
                        // Returns Latitude, Longitude and Altitude as a comma seperated results, surprise they don't return direction as well
                        Longitude = coordinates[0];
                        Latitude = coordinates[1];
                        pageData = client.DownloadData("http://query.yahooapis.com/v1/public/yql?q=select+woeid+from+geo.places+where+text='" + Latitude + "," + Longitude + "'&format=xml");
                        pageHtml = System.Text.Encoding.ASCII.GetString(pageData);
                        pageHtml = ns.Replace(pageHtml, string.Empty);
                        xmlDoc = new System.Xml.XmlDocument();
                        xmlDoc.LoadXml(pageHtml.ToLower());
                        if (xmlDoc != null)
                        {
                            Woeid = xmlDoc.SelectSingleNode("query/results/place/woeid").InnerText;                            
                        }
                    }

                    else
                    {
                        WriteToEventLog(" Google Status: " + Status.ToString() + ", Google result:" + pageHtml.ToLower(), EventLogEntryType.Information);
                    }
                }
            }
            catch (XmlException ex)
            {
                WriteToEventLog("Response received: " + pageHtml + ex.Message.ToString(), EventLogEntryType.Error);
            }
            catch (WebException ex)
            {
                WriteToEventLog("Check your internet connection : " + ex.Message.ToString(), EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                // In case google is down
                WriteToEventLog("Error[GetResults]: " + ex.ToString(), EventLogEntryType.Error);
            }
            finally
            {
            }
        }
        /// <summary>
        /// Writes the entry (message) to the Application Event Log.
        /// </summary>
        /// <param name="Entry">Message to log into event log</param>
        /// <param name="EventType">Log entry type [Error | Warning | Information]</param>
        /// <remarks></remarks>
        public static void WriteToEventLog(string Entry, EventLogEntryType EventType)
        // ERROR: Optional parameters aren't supported in C# EventLogEntryType EventType)
        {
            string AppName = "CMS400";
            EventLog ev = new EventLog("Application");
            //Read web.config file
            //These are in try catch because we don't want to take changes on the exception here
            System.Configuration.AppSettingsReader objConfigSettings = new System.Configuration.AppSettingsReader();

            ///Read web.confg
            ev.Source = AppName;
            try
            {
                if (!EventLog.SourceExists(AppName, "."))
                {
                    EventLog.CreateEventSource(AppName, "Application");
                }
            }
            catch (System.Security.SecurityException ex)
            {
                ev.WriteEntry(ex.ToString());
            }
            //throw error that user does not have a permission.
            try
            {
                ev.WriteEntry(Entry, EventType);
            }
            catch (Exception Ex)
            {
                ev.WriteEntry(Ex.ToString());
            }

        }

    }

}

