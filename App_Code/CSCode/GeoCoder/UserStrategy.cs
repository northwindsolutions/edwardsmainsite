﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Extensibility;
using Ektron.Cms.Instrumentation;

namespace Cms.Extensions.GoogleGeoCoder
{
    public class UserStrategy : Ektron.Cms.Extensibility.UserStrategy
    {
        #region Member Variables
        private string Latitude = string.Empty;
        private string Longitude = string.Empty;
        #endregion

        public override void OnAfterUpdateUser(Ektron.Cms.UserData userData, CmsEventArgs eventArgs)
        {
            ObtainMapCoordinates(userData);
        }

        public override void OnAfterAddUser(Ektron.Cms.UserData userData, CmsEventArgs eventArgs)
        {
            ObtainMapCoordinates(userData);
        }



        public void ObtainMapCoordinates(Ektron.Cms.UserData uData)
        {
            Ektron.Cms.API.User.User userApi = new Ektron.Cms.API.User.User(); 
            try
            {
                //int iLatitudeIndex = -1;
                //int iLongitudeIndex = -1;
                if ((uData != null))
                {
                    if ((!string.IsNullOrEmpty(uData.Address)))
                    {
                        //get the coordinates from google
                        Helper.GetResults(uData.Address,  out Longitude, out Latitude);
                        if ((!string.IsNullOrEmpty(this.Latitude) & !string.IsNullOrEmpty(this.Longitude)))
                        {
                            userApi.UpdateMapCoordinate(uData.Id, Convert.ToDouble(Latitude), Convert.ToDouble(Longitude));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToEventLog("Error: " + ex.ToString(), EventLogEntryType.Error);
            }
           
        }


    }
}
