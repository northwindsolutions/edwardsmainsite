﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using Edwards.Portal;
using Edwards.Utility;
using Ektron.Cms;

/// <summary>
/// Summary description for Videos
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class Videos : System.Web.Services.WebService {

    int pageValue = 6;

    public Videos () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetPrimaryData(long id)
    {
        List<ContentData> lstContentData = new List<Ektron.Cms.ContentData>();
        lstContentData = ContentUtility.GetListBySmartformIdAndFolder(Constants.SmartFormIds.Videos, id);
        return GetBuiltHtml(lstContentData);
    }

    /// <returns>string</returns>
    [WebMethod]
    public string GetPaginationData(int pageNumber, long id)
    {
        string strHtml = "";
        List<ContentData> lstContentData = new List<Ektron.Cms.ContentData>();
        lstContentData = ContentUtility.GetListBySmartformIdAndFolder(Constants.SmartFormIds.Videos, id);
        if (lstContentData.Count >= 6)
        {
            strHtml = GetBuiltHtml(lstContentData, pageNumber);
        }

        return strHtml;
    }

    private string GetBuiltHtml(List<ContentData> contentDataList)
    {
        var mainBuilder = new StringBuilder();
        if (contentDataList != null && contentDataList.Count > 0)
        {
            
            pageValue = contentDataList.Count < 6 ? contentDataList.Count : pageValue;
            for (int index = 0; index < pageValue; index++)
            {
                if (index <= contentDataList.Count)
                {
                    XDocument xDoc = XDocument.Parse(contentDataList[index].Html);
                    if (contentDataList[index] != null)
                    {
                            Edwards.Models.Videos video = BindVideosContent(xDoc).First();
                            if (index % 2 == 0)
                            {
                                mainBuilder.Append("<div class=\"one-half\">");
                                mainBuilder.Append("<div class=\"videos-outer\">");
                                mainBuilder.Append(video.ImageUrl != null ? "<div class=\"img_div\"><img src=\"" + video.ImageUrl + "\" alt=\"\"/></div>" : string.Empty);
                                mainBuilder.Append("<div class=\"videos-text\">");
                                mainBuilder.Append(video.Title != null ? "<h4><strong>" + video.Title + "</strong></h4>" : string.Empty);
                                mainBuilder.Append(video.Teaser != null ? "<p>" + video.Teaser + "</p>" : string.Empty);
                                mainBuilder.Append(video.DownloadUrl != null ? "<p><a target=\"_blank\" href=\"" + video.DownloadUrl + "\">"+ video.Text +"<span class=\"icon\"></span></a></p>" : string.Empty);
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                            }
                            else
                            {
                                mainBuilder.Append("<div class=\"one-half omega\">");
                                mainBuilder.Append("<div class=\"videos-outer\">");
                                mainBuilder.Append(video.ImageUrl != null ? "<div class=\"img_div\"><img src=\"" + video.ImageUrl + "\" alt=\"\"/></div>" : string.Empty);
                                mainBuilder.Append("<div class=\"videos-text\">");
                                mainBuilder.Append(video.Title != null ? "<h4><strong>" + video.Title + "</strong></h4>" : string.Empty);
                                mainBuilder.Append(video.Teaser != null ? "<p>" + video.Teaser + "</p>" : string.Empty);
                                mainBuilder.Append(video.DownloadUrl != null ? "<p><a target=\"_blank\" href=\"" + video.DownloadUrl + "\">" + video.Text + "<span class=\"icon\"></span></a></p>" : string.Empty);
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                            }
                    }
                }
                else
                {
                    break;
                }
            }
           // mainBuilder.AppendFormat("<div id=\"container\">{0}</div>", subBuilder);

            return mainBuilder.ToString();
        }
        return string.Empty;
    }

    protected List<Edwards.Models.Videos> BindVideosContent(XDocument xDoc)
    {
        List<Edwards.Models.Videos> lstVideos = new List<Edwards.Models.Videos>();
        lstVideos = (from element in xDoc.Descendants("Video")
                     select new Edwards.Models.Videos
                     {
                         ImageUrl = element.getXmlElementAttribValue("ImageUrl", "img", "src"),
                         Title = element.Element("Title").Value,
                         Teaser = element.Element("Teaser").Value,
                         Text = HttpContext.GetGlobalResourceObject("EdwardsPortal", "Download").ToString(), //element.Element("DownloadUrlLink").Value,
                         DownloadUrl = element.getXmlElementAttribValue("DownloadUrlLink", "a", "href")
                     }).ToList();
        return lstVideos;
    }

    private string GetBuiltHtml(List<ContentData> contentDataList, int pageNumber)
    {
        var mainBuilder = new StringBuilder();
        int currentIndex = pageNumber * pageValue;
        int totalCount = contentDataList.Count;
        int totalItems = totalCount - currentIndex % 6 == 0 ? 6 : totalCount - currentIndex;
        if (contentDataList.Count > currentIndex)
        {
            contentDataList = contentDataList.Skip(currentIndex).Take(totalItems).ToList();
            for (int index = 0; index < contentDataList.Count; index++)
            {
                if (index <= contentDataList.Count)
                {
                    XDocument xDoc = XDocument.Parse(contentDataList[index].Html);
                    if (contentDataList[index] != null)
                    {
                        Edwards.Models.Videos video = BindVideosContent(xDoc).First();
                            if (index % 2 == 0)
                            {
                                mainBuilder.Append("<div class=\"one-half\">");
                                mainBuilder.Append("<div class=\"videos-outer\">");
                                mainBuilder.Append(video.ImageUrl != null ? "<div class=\"img_div\"><img src=\"" + video.ImageUrl + "\" alt=\"\"/></div>" : string.Empty);
                                mainBuilder.Append("<div class=\"videos-text\">");
                                mainBuilder.Append(video.Title != null ? "<h4><strong>" + video.Title + "</strong></h4>" : string.Empty);
                                mainBuilder.Append(video.Teaser != null ? "<p>" + video.Teaser + "</p>" : string.Empty);
                                mainBuilder.Append(video.DownloadUrl != null ? "<p><a target=\"_blank\" href=\"" + video.DownloadUrl + "\">" + video.Text + "<span class=\"icon\"></span></a></p>" : string.Empty);
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                            }
                            else
                            {
                                mainBuilder.Append("<div class=\"one-half omega\">");
                                mainBuilder.Append("<div class=\"videos-outer\">");
                                mainBuilder.Append(video.ImageUrl != null ? "<div class=\"img_div\"><img src=\"" + video.ImageUrl + "\" alt=\"\"/></div>" : string.Empty);
                                mainBuilder.Append("<div class=\"videos-text\">");
                                mainBuilder.Append(video.Title != null ? "<h4><strong>" + video.Title + "</strong></h4>" : string.Empty);
                                mainBuilder.Append(video.Teaser != null ? "<p>" + video.Teaser + "</p>" : string.Empty);
                                mainBuilder.Append(video.DownloadUrl != null ? "<p><a target=\"_blank\" href=\"" + video.DownloadUrl + "\">" + video.Text + "<span class=\"icon\"></span></a></p>" : string.Empty);
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                                mainBuilder.Append("</div>");
                            }
                    }
                }
                else
                {
                    break;
                }
            }

            return mainBuilder.ToString();
        }
        return string.Empty;
    }
    
}
