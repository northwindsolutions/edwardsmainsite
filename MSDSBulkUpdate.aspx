﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MSDSBulkUpdate.aspx.cs" Inherits="MSDSBulkUpdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button runat="server" ID="importBtn" Text="Import" OnClick="ImportClick"/>
    </div>
    </form>
    <br/>
    <asp:Label ID="lblMessage" runat="server" />
    <br />
    <br/>
    <asp:Label ID="lblItemList" runat="server"/>
</body>
</html>
