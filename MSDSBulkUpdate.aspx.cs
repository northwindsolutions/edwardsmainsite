﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Content;
using Ektron.Cms.API;
using Ektron.Cms.Framework.Content;

public partial class MSDSBulkUpdate : System.Web.UI.Page
{
    private const long EnglishFolderId = 148;
    private const long ChineseFolderId = 151;
    private const long FrenchFolderId = 150;
    private const long GermanFolderId = 149;
    private const long ItalianFolderId = 152;
    private const long JapaneseFolderId = 153;
    private const long KoreanFolderId = 154;
    private const long PortugueseFolderId = 155;
    private int? pageSize = 5000000;
    private StringBuilder errList = new StringBuilder();
    private StringBuilder buffList = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ImportClick(object sender, EventArgs e)
    {
        // folder ID where I want the files to go

        long x = 0;
        long folderId = PortugueseFolderId;

        string pdfFolderPath = @"C:\Uploads";
        string[] filePaths = Directory.GetFiles(pdfFolderPath, "*.pdf");

        foreach (string pdfFile in filePaths)
        {
            x++;
            // Get filename of pdf file
            string pdfFileName = Path.GetFileName(pdfFile);

            if (string.IsNullOrEmpty(pdfFileName))
            {
                lblMessage.Text = "Error: pdfFileName is null or empty";
                return;
            }

            // create the content item title in the form "YYYY T - CCCC" where Y is year, T is term and C is program code.
            string contentTitle = pdfFileName.Replace(".pdf", "");

            // initialize framework.
            Library libraryApi = new Library();
            LibraryManager lmgr = new LibraryManager();

            // upload the file to Ektron
            Ektron.Cms.LibraryConfigData libSettingData = libraryApi.GetLibrarySettings(folderId);
            string strFilename = Server.MapPath(libSettingData.FileDirectory) + pdfFileName;

            // determine if the item already exists.
            LibraryCriteria criteria = new LibraryCriteria();

            criteria.AddFilter(Ektron.Cms.Common.LibraryProperty.ParentId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, folderId);
            criteria.PagingInfo = new PagingInfo
            {
                RecordsPerPage = pageSize.Value,
            };

            // get library item with the same title as item in ektron
            LibraryData libraryData = lmgr.GetList(criteria).FirstOrDefault(z => z.Title == contentTitle);

            if (libraryData != null)
            {
                libraryData.Title = libraryData.Title;

                byte[] newFile = File.ReadAllBytes(pdfFile);

                if (newFile.Length > 0)
                {
                    //libraryData.File = newFile;
                    //libraryData.FileName = strFilename;
                    libraryData.DisplayLastEditDate = String.Format("{0:G}", DateTime.Now);
                }

                //lmgr.Update(libraryData);

                buffList.Append("Library Item Updated with ID = " + libraryData.Id.ToString() + " filename = " + contentTitle + "<br/>");
            }
            else
            {
                errList.Append("<strong style=\"color: red\">Error:</strong> " + contentTitle + " is null and doesn't exist <br />");
            }
        }

        lblMessage.Text = errList.ToString();
        lblItemList.Text = buffList.ToString();
    }

}