$directoriesWithWritePermission = @("AssetManagement", "assets", "imagecache", "logs", "privateassets", "uploadedfiles", "uploadedImages")
$filesWithWritePermission = @("workarea\collectiontopiclist.xml")

$errors = $FALSE



Write-host ""
Write-host "Setting permissions of new build." -foregroundcolor yellow -backgroundcolor black

$InheritanceFlag = @([System.Security.AccessControl.InheritanceFlags]::ContainerInherit,[System.Security.AccessControl.InheritanceFlags]::ObjectInherit)
$PropagationFlag = [System.Security.AccessControl.PropagationFlags]::None
Try{
        Write-host "Applying read-only permissions to root directory... " -nonewline
        $Acl = Get-Acl
        $colRights = [System.Security.AccessControl.FileSystemRights]"ReadAndExecute, ListDirectory, Read"
        $NSro = New-Object  system.security.accesscontrol.filesystemaccessrule("Network Service",$colRights,$InheritanceFlag,$PropagationFlag,"Allow")
        $Acl.SetAccessRule($NSro)
        Set-Acl ".\" $Acl
        Write-host "done." -foregroundcolor green
}
Catch{
        Write-host "Unable to update permissions of root directory. An error occurred." -foregroundcolor red -backgroundcolor black
        $errors = $TRUE
}

$NSrw = New-Object  system.security.accesscontrol.filesystemaccessrule("Network Service", "FullControl",$InheritanceFlag,$PropagationFlag,"Allow")

foreach($directory in $directoriesWithWritePermission)
{
        if(-not(test-path $directory)){
                Write-host "Directory ""$directory"" does not exist." -foregroundcolor red -backgroundcolor black
                $errors = $TRUE
        }
        else{
                Try{
                        Write-host "Applying write permissions to directory ""$directory""... " -nonewline
                        $Acl = Get-Acl $directory
                        $Acl.SetAccessRule($NSrw)
                        Set-Acl $directory $Acl
                        Write-host "done." -foregroundcolor green
                }
                Catch{
                        Write-host "Unable to update permissions of directory ""$directory"". An error occurred." -foregroundcolor red -backgroundcolor black
                        $errors = $TRUE
                }
        }
}

foreach($file in $filesWithWritePermission)
{
        if(-not(test-path $file)){
                Write-host "File ""$file"" does not exist." -foregroundcolor red -backgroundcolor black
                $errors = $TRUE
        }
        else{
                Try{
                        Write-host "Applying write permissions to file ""$file""... " -nonewline
                        $Acl = Get-Acl $file
                        $Acl.SetAccessRule($NSrw)
                        Set-Acl $file $Acl
                        Write-host "done." -foregroundcolor green
                }
                Catch{
                        Write-host "Unable to update permissions of file ""$file"". An error occurred." -foregroundcolor red -backgroundcolor black
                        $errors = $TRUE
                }
        }
}


Write-host ""
if(-not($errors)){
        Write-host "Setup completed successfully." -foregroundcolor yellow -backgroundcolor black
        }
else{
        Write-host "Setup completed with errors." -foregroundcolor red -backgroundcolor black
}       
Write-host "Press ENTER to close. " -foregroundcolor yellow -backgroundcolor black
Read-Host 