﻿//Dependencies
//Ektron.Namespace.js

if ("undefined" !== typeof ($ektron)) {

    Ektron.Namespace.Register('Ektron.Workarea.Blog.AddEditComment');
    
    Ektron.Workarea.Blog.AddEditComment.SubmitForm = function () {
        var failCount = (0 !== $ektron('.ektron-ui-invalid').length), 
            emailFail = (0 === $ektron('.email input').val().length ),
            commentFail = (0 === $ektron('.comment textarea').val().length ); 
        if (failCount || emailFail || commentFail ) { alert('Required fields missing.'); }
        else { document.forms[0].submit(); }

    }

}
