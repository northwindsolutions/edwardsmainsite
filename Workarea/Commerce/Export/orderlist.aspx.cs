﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Commerce;
using Ektron.Cms.Common;
using Ektron.Cms;

public partial class Commerce_Export_orderlist : Ektron.Cms.Workarea.Page
{
    #region properties

    protected EkMessageHelper m_refMsg;
    protected ContentAPI m_refContentApi = new ContentAPI();

    #endregion 

    #region Constructors

    /// <summary>
    /// Constructor logic goes here 
    /// </summary>
    public Commerce_Export_orderlist()
    {
        m_refMsg = m_refContentApi.EkMsgRef;
    }

    #endregion 

    #region protected methods

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        try
        {
            bool IsRunning = CommerceExportHelper.ExportChecker.ThreadRunning;
            if (IsRunning)
            {
                ExportStatus.Text = this.GetMessage("lbl commerce export message");
                Response.AddHeader("Refresh", "5");
            }
            else
            {
                if (CommerceExportHelper.ExportChecker.StreamedOutput != null)
                {
                    if (CommerceExportHelper.ExportChecker.ProviderType == "CSVExportProvider")
                    {
                        Response.AddHeader("Content-Disposition", "attachment;filename=orderlist.csv");
                        Response.ContentType = "text/csv";
                    }
                    else if (CommerceExportHelper.ExportChecker.ProviderType == "XLSExportProvider")
                    {
                        Response.AddHeader("Content-Disposition", "attachment;filename=orderlist.xls");
                        Response.ContentType = "application/ms-excel";
                    }

                    Response.BinaryWrite(CommerceExportHelper.ExportChecker.StreamedOutput);
                    Response.Flush();
                    Response.End();
                }
            }
        }
		catch (System.Threading.ThreadAbortException tx)
        {
            //Threadaborts are ok...ASP.Net aborts thread when unloading application
        }
        catch (Exception ex)
        {
            EkException.LogException(ex);
        }
    }

    #endregion 


    #region private methods

    private string GetMessage(string MessageTitle)
    {
        return m_refMsg.GetMessage(MessageTitle);
    }

    #endregion 
}