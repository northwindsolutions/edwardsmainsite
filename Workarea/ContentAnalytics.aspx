﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentAnalytics.aspx.cs" Inherits="ContentAnalytics" %>
<%@ Register Src="controls/analytics/Global.ascx" TagName="Global" TagPrefix="uc3" %>
<%@ Register Src="controls/analytics/Page.ascx" TagName="Page" TagPrefix="uc4" %>
<%@ Register Src="controls/analytics/Referring_url.ascx" TagName="Referring_url" TagPrefix="uc5" %>
<%@ Register Src="controls/analytics/ContentReports.ascx" TagName="ContentReports" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Content Analytics Page</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <asp:literal id="StyleSheetJS" runat="server" />
</head>
<body>
    <form id="form1" name="form1" runat="server">
        <div id="dhtmltooltip">
        </div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
            </div>
        </div>
        <div class="ektronPageContainer ektronPageInfo">
            <table class="ektronForm" style="margin-top:0px; position:relative; z-index:1;">
                <tr>
                    <td class="label" title="Quick View">
                        <asp:Literal ID="lblQuickView" runat="server" EnableViewState="False" /></td>
                    <td class="readOnlyValue">
                        <asp:LinkButton ToolTip="View Day" ID="ctlDay" runat="server" EnableViewState="False"
                            Font-Bold="False" OnClick="ctlDay_Click">[Day]</asp:LinkButton>
                        <asp:LinkButton ToolTip="View Week" ID="ctlWeek" runat="server" EnableViewState="False"
                            OnClick="ctlWeek_Click">[Week]</asp:LinkButton>
                        <asp:LinkButton ToolTip="View Month" ID="ctlMonth" runat="server" EnableViewState="False"
                            OnClick="ctlMonth_Click">[Month]</asp:LinkButton>
                        <asp:LinkButton ToolTip="View Year" ID="ctlYear" runat="server" EnableViewState="False"
                            OnClick="ctlYear_Click">[Year]</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Literal ID="lblJumpTo" runat="server" EnableViewState="False" /></td>
                    <td class="readOnlyValue">
                        <asp:LinkButton ToolTip="Jump to Previous Day" ID="linkPrevious" runat="server" OnClick="linkPrevious_Click">[Previous Day]</asp:LinkButton>
                        <asp:LinkButton ToolTip="Jump to Next Day" ID="linkNext" runat="server" OnClick="linkNext_Click">[Next Day]</asp:LinkButton>
                        <asp:LinkButton ToolTip="Jump to Today" ID="linkToday" runat="server" OnClick="linkToday_Click">[Today]</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="lblstartdate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblstartdatepicker" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="stripe">
                    <td class="label">
                        <asp:Label ID="lblenddate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblenddatepicker" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Button ToolTip="Run Custom Range" ID="Button1" runat="server" Text="Run Custom Range"
                OnClick="Button1_Click" />
            <div id="td_ecp_search" runat="server">
            </div>
            <hr />
            <br />
            <div class="ektronPageContainer ektronPageTabbed" style="margin:62px 0 0 0;position:inherit; top:0;">
                <div class="tabContainerWrapper">
                    <ektronUI:Tabs ID="tabcontrol" runat="server">
                        <ektronUI:Tab ID="TabSiteStatistics" runat="server" Text="Site Statistics">
                            <ContentTemplate>
                                <div id="dvSiteStatistics">
                                    <uc3:Global ID="Global1" runat="server" Visible="false" ReportType="1" />
                                </div>
                            </ContentTemplate>
                        </ektronUI:Tab>
                        <ektronUI:Tab ID="TabSiteActivity" runat="server" Text="Site Activity">
                            <ContentTemplate>
                                <div id="dvSiteActivity">
                                    <uc3:Global ID="Global2" runat="server" Visible="false" ReportType="2"  />
                                </div>
                            </ContentTemplate>
                        </ektronUI:Tab>
                        <ektronUI:Tab ID="TabContent" runat="server" Text="Content" OnClick="TabContent_Click">
                            <ContentTemplate>
                                <div id="dvContent">
                            <uc2:ContentReports ID="ContentReports1" runat="server" Visible="false" />
                        </div>
                            </ContentTemplate>
                        </ektronUI:Tab>
                        <ektronUI:Tab ID="TabTemplate" runat="server" Text="Template" OnClick="TabTemplate_Click">
                            <ContentTemplate>
                                <div id="dvTemplates">
                            <uc4:Page ID="Page1" runat="server" Visible="false" />
                        </div>
                            </ContentTemplate>
                        </ektronUI:Tab>
                        <ektronUI:Tab ID="TabReferrers" runat="server" Text="Referrers" OnClick="TabReferrers_Click">
                            <ContentTemplate>
                                <div id="dvReferrers">
                            <uc5:Referring_url ID="Referring_url1" runat="server" Visible="false" />
                        </div> 
                            </ContentTemplate>
                        </ektronUI:Tab>
                    </ektronUI:Tabs>                
                </div>    
            </div>
        </div>
        <input type="hidden" id="start_date" name="start_date" />
        <input type="hidden" id="end_date" name="end_date" />
    </form>
</body>
</html>
