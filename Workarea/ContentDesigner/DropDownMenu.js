if (!activeDropDown)var activeDropDown=null; if (!onClickHandlerAttached)var onClickHandlerAttached= false; function DropDownMenu(menuButton,menuElement){ this.MenuButton=menuButton; this.MenuElement=menuElement; this.MenuEventHandler=null
; var theDropDown=this ; if (!onClickHandlerAttached){if (document.all){document.attachEvent("\x6f\x6eclick",theDropDown.BodyClickHandler); }else {document.addEventListener('click',theDropDown.BodyClickHandler, true); }}}DropDownMenu.prototype.BodyClickHandler= function (e){if (!e){var e=window.event; }if (null!=activeDropDown){activeDropDown.SetVisible( false); }}
; DropDownMenu.prototype.Enable= function (isEnabled){if (this.IsValid( )){ this.MenuButton.disabled=isEnabled? false : true; }}
; DropDownMenu.prototype.IsValid= function ( ){return (null!=this.MenuButton) && (null!=this.MenuElement); }
; DropDownMenu.prototype.IsVisible= function ( ){return this.IsValid( ) && 'visibl\x65'==this.MenuElement.style.visibility; }
; DropDownMenu.prototype.Toggle= function ( ){ this.SetVisible(!this.IsVisible( )); }
; DropDownMenu.prototype.GetCoords= function (node){var coords=new Array(0,0); if (node.offsetParent){while (node.offsetParent){coords[0]+=node.offsetLeft; coords[1]+=node.offsetTop; node=node.offsetParent; if (node==document.body){coords[0]-=node.offsetLeft; coords[1]-=node.offsetTop; }}}return coords; }
; DropDownMenu.prototype.SetVisible= function (visible){if (this.IsValid( )){if (visible){if (activeDropDown){activeDropDown.SetVisible( false); }var coords=this.GetCoords(this.MenuButton); this.MenuElement.style.left=coords[0]+"\x70x"; this.MenuElement.style.top=(coords[1]+this.MenuButton.offsetHeight)+"p\x78"; this.MenuElement.style.zIndex=51200; this.MenuElement.style.overflow="\x68idden"; this.MenuElement.style.position="\x61bsolute"; this.MenuElement.style.visibility="\x76isible"; }else { this.MenuElement.style.visibility='hid\x64\x65n'; }activeDropDown=visible?this :null
; if (null!=this.MenuEventHandler && null!=this.MenuEventHandler.OnShowMenu){ this.MenuEventHandler.OnShowMenu(visible); }}}
; DropDownMenu.prototype.SetMenuInnerHtml= function (innerHtml){ this.MenuElement.innerHTML=innerHtml; }
;
