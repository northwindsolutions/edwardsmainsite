﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page language="c#" CodeFile="collectionselectorpopup.aspx.cs" Inherits="Ektron.ContentDesigner.Dialogs.CollectionSelectorPopup" AutoEventWireup="false" %>
<%@ Register TagPrefix="ek" TagName="FieldDialogButtons" Src="ucFieldDialogButtons.ascx" %>
<%@ Register TagPrefix="radTS" Namespace="Telerik.WebControls" Assembly="RadTabStrip.NET2" %>
<%@ Register TagPrefix="ek" TagName="CollectionTree" Src="ucCollectionTree.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title id="Title" runat="server">Select Content</title>
    <style type="text/css">
        div#CBResults 
        {
        	height: 10em !important;
        }
        div#Ektron_Dialog_Tabs_BodyContainer, .Ektron_Dialog_Tabs_BodyContainer, .Ektron_DialogTabBodyContainer
        {
        	margin: 0 !important;
        }
        .Invisible
        {
			display: none;
        }
        span.folder
        {
        	cursor: hand; 
        	cursor: pointer;
        }
        .CBfoldercontainer span.folderselected, div.treecontainer .selected
        {
	        background-color: #9cf !important;
        }
    </style>
</head>
<body class="dialog">
<form id="Form1" runat="server">
   <div class="Ektron_DialogTabstrip_Container">
    <radTS:RadTabStrip id="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1" SelectedIndex="0" ReorderTabRows="true" 
		 SkinID="TabstripDialog">
        <Tabs>
            <radTS:Tab ID="Collection" Text="Collection" Value="Collection" />
        </Tabs>
    </radTS:RadTabStrip>  
    </div>
    <div id="TreeViewDiv" class="Ektron_Dialog_Tabs_BodyContainer CBWidget">
        <radTS:RadMultiPage Height="290" id="RadMultiPage1" runat="server" SelectedIndex="0" AutoScrollBars="true">
	        <radTS:PageView id="PageViewCollection" runat="server" > 
	            <ek:CollectionTree ID="collectiontree" runat="server" />
	        </radTS:PageView>
        </radTS:RadMultiPage>
        <asp:TextBox ID="tbData" ToolTip="Data" CssClass="HiddenTBData" runat="server" style="display:none;"></asp:TextBox>
        <asp:TextBox ID="tbFolderPath" ToolTip="Folder Path" CssClass="HiddenTBFolderPath" runat="server" style="display:none;"></asp:TextBox>
    	<input type="hidden" id="hdnAppPath" name="hdnAppPath" value="" />
        <input type="hidden" id="hdnLangType" name="hdnLangType" value="" />
        <input type="hidden" id="hdnFolderId" name="hdnFolderId" value="0"/>
    </div>
    <div class="Ektron_Dialogs_LineContainer">
        <div class="Ektron_TopSpaceSmall"></div>
        <div class="Ektron_StandardLine"></div>
    </div>		 
    <ek:FieldDialogButtons ID="FieldDialogButtons" OnOK="return insertField();" runat="server" />    
</form> 
<script language="javascript" type="text/javascript">
<!--
    Ektron.ready(function(){
        initField();
        window.focus();
        if($ektron("div#RadTabStrip1 a").is(":visible")) {
            $ektron("div#RadTabStrip1 a").eq(0).focus();
        }
        BindOnRadWindowKeyDown();
    });
    
	var ResourceText = 
	{
		sWarnNoSelection: "<asp:literal id="sWarnNoSelection" runat="server"/>"
	,	sWarnMultiSelection: "<asp:literal id="sWarnMultiSelection" runat="server"/>"
	,	sWarnNoResult: "<asp:literal id="sWarnNoResult" runat="server"/>"
	,   TreeRoot: "<asp:literal id="sTreeRoot" runat="server"/>"
	,   CollectionItems: "<asp:literal id="collectionItems" runat="server"/>"
	,   TaxonomyItems: "<asp:literal id="taxonomyItems" runat="server"/>"
	,   sChildren: "<asp:literal id="sChildren" runat="server"/>"
	,   sItems: "<asp:literal id="sItems" runat="server"/>"
	,   idType: {
         "collection": "<asp:literal id="sCollection" runat="server"/>"
        }  
	};
    var webserviceURL = "";
    var m_filterBy = "content";
    var m_selectorType = "content";
    var m_idType = "content:htmlcontent";
    var m_defaultFolder = 0;
    var m_folderNavigation = "descendant";
    var m_objFormField = null;
    var m_waPath = "<%= new Ektron.Cms.SiteAPI().ApplicationPath %>";
    var m_langType;
    var m_resourceTitle = "";
    
    function initField()
    {
        m_objFormField = new EkFormFields();
        var appPath;
        var idValue;
//		var bSearchByFolder = true;
//		var bSearchByTaxonomy = true;
//		var bSearchByWords = true;
		var sRoot = ResourceText.TreeRoot;
		var searchTerm;

        var args = GetDialogArguments();
        if (args)
        {
            appPath = args.appPath;
            m_langType = args.langType;
			m_idType = args.idType;
			idValue = args.idValue + "";
//            bSearchByFolder = args.searchByFolder;
//            bSearchByTaxonomy = args.searchByTaxonomy;
//            bSearchByWords = args.searchByWords;
            m_filterBy = (args.filterBy || "content:htmlcontent ");
            m_selectorType = args.selectorType;
            m_defaultFolder = args.defaultFolder;
            m_folderNavigation = (args.folderNavigation || m_folderNavigation);
            var startFolderTitle = args.startFolderTitle;
            var rootFolder = 0;
            if (startFolderTitle && startFolderTitle.length > 0 && m_folderNavigation != "ancestor" && m_selectorType != "startingfolder")
            {
                sRoot = args.startFolderTitle;
                rootFolder = m_defaultFolder;
            }
            searchTerm = args.searchTerm;
            m_resourceTitle = getResourceFieldResourceTitle(args.display);
        }
	    document.getElementById("hdnAppPath").value = appPath;
	    document.getElementById("hdnLangType").value = m_langType;
	    
		webserviceURL = (appPath ? appPath : m_waPath + "ContentDesigner/");
		webserviceURL += "dialogs/contentfoldertree.ashx";
		if (m_langType)
		{
			webserviceURL += "?LangType=" + m_langType;
		}
		
        var oTabCtl = <%= RadTabStrip1.ClientID %>;
        var tabCollection = oTabCtl.FindTabById("<%= RadTabStrip1.ClientID %>_Collection");
		tabCollection.SelectParents();

    }	
    
    function insertField()
    {
        var retObj = null;
        var el = null;
        var sIdType = m_idType;
        var idValue;

                el = $ektron("div.collectionTree").find("li.selected");
                if (el.length === 0)
                {
                    alert(ResourceText.sWarnNoSelection);
                }
                else
                {
                    idValue = parseInt(el.attr("data-ektron-id"), 10);
                    var sResTitle = $ektron.trim($ektron("span.textWrapper", el).eq(0).text());
                    retObj = 
                    {
                        resourceId: idValue
                    ,   resourceTitle: sResTitle
                    ,   sDisplay: m_objFormField.getResourceFieldDisplayValue(ResourceText, m_waPath, sIdType, idValue, sResTitle, m_langType)
                    ,   idType: sIdType
                    };
                }
      
        if (retObj)
        {
            retObj.selectorType = m_selectorType;
            CloseDlg(retObj);	
        }
        return false;
    }
    
    function getResourceFieldResourceTitle(fullDisplay)
    {
        //For example, resource «Smart Form:564»
        if (!fullDisplay) return "";
        var lend = fullDisplay.indexOf("«");
        if (lend > -1)
        {
            return $ektron.trim(fullDisplay.substr(0, lend));
        }
        else
        {
            return fullDisplay;
        }
    }
    
// -->
</script>
</body> 
</html> 
