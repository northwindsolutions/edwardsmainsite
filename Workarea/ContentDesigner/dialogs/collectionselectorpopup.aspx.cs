using System;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Newtonsoft.Json;
using System.Collections.Generic;
using Ektron.Cms.Workarea.Framework;

namespace Ektron.ContentDesigner.Dialogs
{
    /// <summary>
    /// Summary description for CollectionSelectorPopup.
    /// </summary>
	public partial class CollectionSelectorPopup : WorkareaDialogPage
    {
        private void Page_Load(object sender, EventArgs e)
        {
			this.RegisterWorkareaCssLink();
			this.RegisterDialogCssLink();
			Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
            Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronStringJS);

			Ektron.Cms.API.JS.RegisterJSInclude(this, "../RadWindow.js", "RadWindowJS");
            Ektron.Cms.API.JS.RegisterJSInclude(this, "../ekformfields.js", "ekformfieldsJS"); 
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "InitializeRadWindow", "InitializeRadWindow();", true);

            // Register JS
            //Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronJsonJS);
            //Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronJQueryClueTipJS);
            //Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronScrollToJS);

            //// Insert CSS Links
            Ektron.Cms.API.Css.RegisterCss(this, "CBStyle.css", "CBWidgetCSS"); //cbstyle will include the other req'd stylesheets


            string selectorType = Request.QueryString["SelectorType"].ToString().ToLower();
            string idType = Request.QueryString["idType"].ToString().ToLower();

            this.Title.Text = this.GetMessage("lbl select collection");
            this.sWarnNoSelection.Text = this.GetMessage("warning no selection");
            this.sWarnMultiSelection.Text = this.GetMessage("warning multiple selection");
            this.sWarnNoResult.Text = this.GetMessage("generic no results found");
            string sCollectionItems = this.GetMessage("lbl collection items");
            this.collectionItems.Text = sCollectionItems;
            this.sCollection.Text = this.GetMessage("lbl collection");
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///        Required method for Designer support - do not modify
        ///        the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

    }
}