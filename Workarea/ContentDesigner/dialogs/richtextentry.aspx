﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page language="c#" CodeFile="richtextentry.aspx.cs" Inherits="Ektron.ContentDesigner.Dialogs.RichTextEntry" AutoEventWireup="false" %>
<%@ Register tagprefix="ektron" tagname="AlohaEditor" src="../../controls/Editor/Aloha.ascx" %>
<%@ Register src="../../controls/Editor/ContentDesignerWithValidator.ascx" tagname="ContentDesignerEditor" tagprefix="ektron" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title id="Title" runat="server">Edit Rich Area Field</title>
<style>
    div.ektronPageHeader div.ektronTitlebar
    {
    	color: #414141;
    }
    div.Ektron_Dialog_Tabs_BodyContainer
    {
    	width: 98%;
    	height: 95%;
    }
    div.ektronPageContainer
    {
    	min-height:430px;
    }
    div#pleaseWait
    {
        width: 128px;
        height: 128px;
        margin: -64px 0 0 -64px;
        background-color: #fff;
        background-image: url("../../images/ui/loading_big.gif");
        background-repeat: no-repeat;
        text-indent: -10000px;
        border: none;
        padding: 0;
        top: 50%;
        left: 50%;
        position: fixed;
    }
</style>
</head>
<body class="dialog">
<form id="Form1" runat="server">
    <asp:Literal ID="StyleSheetJS" runat="server"></asp:Literal>
    <div class="ektronPageHeader">
        <div class="ektronTitlebar" id="divTitleBar" runat="server"></div>
        <div class="ektronToolbar" id="divToolBar" runat="server"></div>
    </div>
    <div class="ektronPageContainer ektronPageGrid">
    <div class="Ektron_DialogTabstrip_Container">
    </div>
    <div class="Ektron_Dialogs_LineContainer">
        <div class="Ektron_TopSpaceSmall"></div>
        <div class="Ektron_StandardLine"></div>
    </div>	
    <div class="Ektron_Dialog_Tabs_BodyContainer">
        <ektron:AlohaEditor ID="AlohaEditor" runat="server" Visible="true" ToolbarConfig="Content" />
        <ektron:ContentDesignerEditor ID="CDEditor" runat="server" Visible="true" Toolbars="Standard" />
    </div>    
    </div>
    <div class="ektronWindow" id="pleaseWait">
        <h3><asp:Literal ID="LoadingImg" runat="server" /></h3>
    </div>
</form> 
<script language="javascript" type="text/javascript">
<!--
    Ektron.ready(function ()
    {
        initModel();
        initField();
        window.focus();
        BindOnRadWindowKeyDown();
    });
    
    var m_strInitialContent = "";
    var m_oEditor = null;
    var m_selectedEditor = "<asp:Literal ID='selectedEditor' runat='server'></asp:Literal>";
    var jsContentLanguage="<asp:literal id='jsContentLanguage' runat='server'/>";    
    var jsId="<asp:literal id='jsId' runat='server'/>";
    var jsDefaultContentLanguage="<asp:literal id='jsDefaultContentLanguage' runat='server'/>";

	function initField()
	{
	    var args = GetDialogArguments();
	    if (args)
	    {
	        m_strInitialContent = args.content;
	        m_oEditor = args.EditorObj;
	        m_contentType = args.contentType;
	        m_fnGetFilteredContent = args.getFilteredContent;
	        sDefaultId = args.fieldId;
	    }

        if ("Aloha" == m_selectedEditor && typeof Aloha != "undefined")
        {
            $ektron('#pleaseWait').modalShow();
            Aloha.ready(function ()
            { 
                if (m_strInitialContent.length > 0)
                {
                    $ektron("div.aloha-editable").html(m_strInitialContent);
                    $ektron("div.ektron-aloha").siblings("input:hidden").val(m_strInitialContent);
                }
                $ektron("div.ektron-aloha").width("98%").css("padding", "5px");
                $ektron("div.aloha-sidebar-handle").show();
                $ektron('#pleaseWait').modalHide();
            });
        }

        if ("ContentDesigner" == m_selectedEditor && "object" == typeof Ektron && Ektron.ContentDesigner && Ektron.ContentDesigner.instances)
        {
            $ektron('#pleaseWait').modalShow();
            var objContentDesigner = Ektron.ContentDesigner.instances["CDEditor"];
            if (objContentDesigner != null && m_strInitialContent != "")
            {
                setTimeout(function ()
                {
                    $ektron('#pleaseWait').modalHide();
                    objContentDesigner.setContent("designpage", m_strInitialContent);
                }, 10000);
            }
            else
            {
                $ektron('#pleaseWait').modalHide();
            }
        }
    }

    function insertField()
    {
        var content = "",
            editor,
			editorDynamicId = "",
           oBody = "";
        switch (m_selectedEditor)
        {
            case "Aloha":
                if (typeof Aloha != "undefined")
                {
                    editorDynamicId = $ektron("div.aloha-editable").attr("id");
                    editor = Aloha.getEditableById(editorDynamicId);
                    oBody = $ektron("<div />").html(editor.getContents()).get(0);
                    content = filterRichContent(oBody);
                }
                break;
            case "ContentDesigner":
            default:
                var objContentDesigner = Ektron.ContentDesigner.instances["CDEditor"];
                if (objContentDesigner != null)
                {
                    content = objContentDesigner.getContent("designpage");
                }
                break;
        }
	    CloseDlg(content);
	}

    // function extract from MozillaPasteHelperDlg.ascx
	function filterRichContent(oBody)
	{
	    var content = Ektron.Xml.serializeXhtml(oBody.childNodes);
	    if ("function" == typeof m_fnGetFilteredContent)
	    {
	        content = m_fnGetFilteredContent(content);
	    }

	    // ektron start - #52915, #52383: Clean up Markup with HTML Tidy

	    // #66716 - preserve the space that will be removed by the HTML tidy
	    //content = content.replace(/> /g, ">&#160;").replace(/–/g, "&#8211;").replace(/—/g, "&#8212;");
	    // #73038 -update overrides issue addressed by #66716
	    content = content.replace(/–/g, "&#8211;").replace(/—/g, "&#8212;");
	    // #71843: if there is only non-P tags in the content, tidy put the clean XHTML in the HEAD.
        // revert 73843 for 73038 as it is not needed in Ver87SP2
        //if(-1 == content.indexOf("</p>")) {
        //    content = '<p>' + content + '</p>';
	    //}

	    $ektron.ajax(
		{
		    type: "POST",
		    async: true,
		    url: m_oEditor.ekParameters.srcPath + "ekajaxtidy.aspx",
		    data: { html: content },
		    dataType: "html",
		    success: function (data)
		    {
		        data = $ektron.trim(data);
		        if (data.indexOf("ekAjaxTidyError") > -1)
		        {
		            var matchResult = data.match(/<body[^>]*>([\w\W]*?)<\/body>/);
		            if (matchResult.length >= 2)
		            {
		                errorMessage = matchResult[1];
		            }
		            else
		            {
		                errorMessage = data;
		            }
		        }
		        else
		        {
		            //Only for Aloha update textarea to script
		            if ("Aloha" == m_selectedEditor && typeof Aloha != "undefined") {
		                content = data.replace(/(<textarea[^>]*>)([\s\S]*?)(<\/textarea[^>]*>)/ig, "<script type=\"text/javascript\">$2<\/script>");
		            }
		            else {
		                content = data;
		            }
		        }
		    },
		    error: function (xhr)
		    {
		        errorMessage = "Ajax Error: " + xhr.status + ": " + xhr.statusText;
		    }
		});
	    // ektron end
	    return content;
	}

	function initModel()
	{
	    // PLEASE WAIT MODAL
	    $ektron("#pleaseWait").modal(
            {
                trigger: '',
                modal: true,
                toTop: true,
                onShow: function (hash)
                {
                    hash.o.fadeIn();
                    hash.w.fadeIn();
                },
                onHide: function (hash)
                {
                    hash.w.fadeOut("fast");
                    hash.o.fadeOut("fast", function ()
                    {
                        if (hash.o)
                        {
                            hash.o.remove();
                        }
                    }
			        );
                }
            }
        );
    }

//-->
</script>
</body>
</html>
