using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Workarea.Framework;

namespace Ektron.ContentDesigner.Dialogs
{
    /// <summary>
    /// Summary description for Aloha.
    /// </summary>
    public partial class RichTextEntry : WorkareaDialogPage
    {
        protected ContentAPI _ContentApi;
        protected EkMessageHelper _MessageHelper;
        protected StyleHelper styleHelper = new StyleHelper();
        protected ICmsContextService _cmsContextService = ServiceFactory.CreateCmsContextService();
        private string m_SelectedEditControl = string.Empty;

        private void Page_Load(object sender, System.EventArgs e)
        {
            _ContentApi = new ContentAPI();
            _MessageHelper = _ContentApi.EkMsgRef;
            if (Request.RawUrl.ToLower().Contains("<script"))
            {
                Utilities.ShowError(_MessageHelper.GetMessage("invalid querstring"));
                return;
            } 
            this.RegisterWorkareaCssLink();
			this.RegisterDialogCssLink();
			Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
			Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronStringJS);
			Ektron.Cms.API.JS.RegisterJSInclude(this, Ektron.Cms.API.JS.ManagedScript.EktronXmlJS);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronModalJS);
			Ektron.Cms.API.JS.RegisterJSInclude(this, "../ekxbrowser.js", "ekxbrowserJS");
			Ektron.Cms.API.JS.RegisterJSInclude(this, "../ekutil.js", "ekutilJS");
			Ektron.Cms.API.JS.RegisterJSInclude(this, "../RadWindow.js", "RadWindowJS");
			Ektron.Cms.API.JS.RegisterJSInclude(this, "../ekformfields.js", "ekformfieldsJS");
			this.ClientScript.RegisterClientScriptBlock(this.GetType(), "InitializeRadWindow", "InitializeRadWindow();", true);
            StyleSheetJS.Text = this.styleHelper.GetClientScript();

            this.Title.Text = _MessageHelper.GetMessage("edit rich area field");

            long folderId = 0;
            if(!string.IsNullOrEmpty(Request.QueryString["folderid"]))
            { 
                long.TryParse(Request.QueryString["folderid"], out folderId);
            }
            long contentId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["contentid"]))
            {
                long.TryParse(Request.QueryString["contentid"], out contentId);
            }

            jsContentLanguage.Text = Convert.ToString(_ContentApi.ContentLanguage);
            jsDefaultContentLanguage.Text = Convert.ToString(_ContentApi.DefaultContentLanguage);
            jsId.Text = Convert.ToString(contentId);
            LoadingImg.Text = _MessageHelper.GetMessage("one moment msg");
            EditorSetup(folderId);

            this.RegisterResources();
		}

        protected override void OnPreRender(EventArgs e)
        {
            divTitleBar.InnerHtml = ".";
            divToolBar.InnerHtml = this.ToolBar();
            base.OnPreRender(e);
        }

        private string ToolBar()
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder();
            string onlineHelpKey = "EditRichAreaField";
            string updateLinkCaption = _MessageHelper.GetMessage("btn save");
            string backLinkCaption = _MessageHelper.GetMessage("generic cancel");
            result.Append("<table><tbody><tr>");
            result.Append(this.styleHelper.GetButtonEventsWCaption(_cmsContextService.WorkareaPath + "/images/UI/Icons/back-arrow.gif", "richtextentry.aspx", backLinkCaption, backLinkCaption, "onclick=\"CloseDlg(); return false;\"", StyleHelper.BackButtonCssClass, true));

            result.Append(this.styleHelper.GetButtonEventsWCaption(_cmsContextService.UIPath + "/images/silk/icons/edit.png", "richtextentry.aspx?action=update", updateLinkCaption, updateLinkCaption, "onclick=\"return insertField();\"", StyleHelper.EditButtonCssClass, true));

            
            result.Append("</tr></tbody></table>");
            return result.ToString();
        }

        private void EditorSetup(long folderId)
        {
            string content_stylesheet = this.ContentApi.GetStyleSheetByFolderID(folderId);
            m_SelectedEditControl = Utilities.GetEditorPreference(Request);
            selectedEditor.Text = m_SelectedEditControl;
            switch (m_SelectedEditControl)
            {
                case "Aloha":
                    CDEditor.Visible = false;
                    AlohaEditor.Visible = true;
                    AlohaEditor.FolderId = folderId;
                    AlohaEditor.Stylesheet = _cmsContextService.SitePath + "/" + content_stylesheet;
                    break;
                case"ContentDesigner":
                default:
                    AlohaEditor.Visible = false;
                    CDEditor.Visible = true;
                    CDEditor.FolderId = folderId;
                    CDEditor.Stylesheet = _cmsContextService.SitePath + "/" + content_stylesheet;
                    PermissionData cPerms = _ContentApi.LoadPermissions(folderId, "folder", 0);
                    CDEditor.SetPermissions(cPerms);
                    CDEditor.AllowFonts = true;
                    CDEditor.Width = new Unit(780, UnitType.Pixel);
                    break;
            }
        }

        private void RegisterResources()
        {
            Package richAreaFieldPackage = new Package()
            {
                Components = new List<Ektron.Cms.Framework.UI.Component>()
                    {
                        Packages.Ektron.CssFrameworkBase,
                        Packages.EktronCoreJS,
                        Packages.Ektron.Workarea.Core
                    }
            };

            richAreaFieldPackage.Register(this);
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///        Required method for Designer support - do not modify
        ///        the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

    }
}
