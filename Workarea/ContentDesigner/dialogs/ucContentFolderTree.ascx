<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucContentFolderTree.ascx.cs" Inherits="Ektron.ContentDesigner.Dialogs.ContentFolderTree" %>
<ektronUI:CssBlock ID="folderTreeTab" runat="server">
    <CssTemplate>
        .left, .right {   
            overflow:auto;
            height:98%;
        }  
        .left  { float:left; width: 25%; border:1px solid black; } 
        .right { float:right; width:73%; }  
        .ektron-ui-gridview table tbody tr.selected { background-color: #CCCCCC; }
        .item { cursor: pointer; }
        .pnlSearchArea { text-align: center; padding-bottom: 1em;}
        .pnlSearchArea span.searchBox input { width: 20em; }
        .ContentType {display:none;}
        .ektron-ui-tree ul.ektron-ui-tree-root { width:97%; }
        div.folderTree li.ektron-ui-tree-branch div.hitLocation div.selectionLocation { white-space:nowrap; }
    </CssTemplate>
</ektronUI:CssBlock>
<div id="left" runat="server" class="left">
    <div class="CBfoldercontainer">
        <ektronUI:FolderTree runat="server" ID="folderTree" RootId="0" SelectionMode="SingleForAll" UseInternalAdmin="true" CssClass="folderTree" 
            OnPreSerializeData="folderTree_OnPreSerializeData" AutoPostBackOnSelectionChanged="true" PageSize="100" OnClick="folderTree_OnClick" >
        </ektronUI:FolderTree>
    </div>
</div>
<div id="right" runat="server" class="right">
    <ektronUI:Message ID="searchServerMsg" runat="server" DisplayMode="Error" Visible="false"></ektronUI:Message>
    <asp:Panel runat="server" ID="pnlSearchArea" CssClass="pnlSearchArea">
        <asp:Label ID="lblSearch" runat="server"></asp:Label>
        <ektronUI:TextField runat="server" ID="txtSearchBox" CssClass="searchBox" ></ektronUI:TextField>
        <ektronUI:Button runat="server" ID="btnSearch" Text="Search" OnClick="DoSearchInFolder" />
    </asp:Panel>
    <ektronUI:GridView ID="contentList" runat="server" AutoGenerateColumns="false" EnableEktronUITheme="true" AllowPaging="true" Caption=" " 
         OnEktronUIPageChanged="OnEktronUIPageChanged"  >
        <Columns>
            <asp:TemplateField ItemStyle-Width="10%">
                <ItemTemplate>
                    <asp:literal ID="ItemIcon" runat="server" Text='<%#GetContentIcon(Eval("ContType"), Eval("SubType"), DataBinder.Eval(Container, "DataItem.AssetData.Icon")) %>'></asp:literal>
                    <asp:Label ID="ContentType" runat="server" Text='<%# GetContentTypeString(Eval("ContType")) %>' CssClass="ContentType"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ContentTitle" runat="server" Text='<%# Eval("Title") %>' CssClass="ContentTitle item"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <Columns>
            <asp:TemplateField ItemStyle-Width="18%">
                <ItemTemplate>
                    <asp:Label ID="ContentId" runat="server" Text='<%# Eval("Id") %>' CssClass="ContentId item"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </ektronUI:GridView>
    <input type="hidden" id="selectedContent" class="selectedContent" runat="server" />
    <input type="hidden" id="selectedFolderPath" class="selectedFolderPath" runat="server" />
    <input type="hidden" id="InFolder" class="InFolder" runat="server" />
</div>


<script language="javascript" type="text/javascript">
<!--
    Ektron.ready(function () {
        $ektron(".ContentTitle, .ContentId").click(function () {
            SelectThisContent(this);
        });
        // re-select the content in grid 
        var idValue = $ektron(".selectedContent").val();
        if (typeof idValue != "undefined" && idValue.length > 0) {
            var eElem = $ektron("span:textEquals('" + idValue + "')");
            var eRow = eElem.closest("tr").addClass("selected");
        }
        // make the selected tree node in view
        var eSelectedNode = $ektron("li.selected:visible");
        if (eSelectedNode.length > 0) {
            scroll(eSelectedNode, $ektron("div.left"))
        }
    });

    $ektron.expr[':'].textEquals = function(a, i, m) {
        return $(a).text().match("^" + m[3] + "$");
    };

    function SelectThisContent(element) {
        //highlight the row
        var eRow = $ektron(element).closest("tr");
        if (!eRow.hasClass("selected")) {
            $ektron("tr.selected").removeClass("selected"); 
            eRow.addClass("selected");
            //add it to the hidden literal
            var id = $ektron(".ContentId", eRow).text();
            $ektron("#selectedContent").text(id).val(id);
        }
    }

    function scroll(eElement, eParent) {
        //http://praveenlobo.com/techblog/how-to-scroll-elements-smoothly-in-javascript-jquery-without-plugins/
        eParent.animate({ scrollTop: eParent.scrollTop() + eElement.offset().top - eParent.offset().top }, { duration: 'slow', easing: 'swing' });
        $ektron('html,body').animate({ scrollTop: eParent.offset().top - ($ektron(window).height() / 3) }, { duration: 1000, easing: 'swing' });
    }
//-->
</script>