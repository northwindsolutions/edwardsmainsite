using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Ektron.Cms.Framework.UI.Tree;
using Ektron.Cms.Framework.UI.Controls.EktronUI;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Expressions;
using Ektron.Cms.Workarea.Framework;
using Ektron.Cms.Common;

namespace Ektron.ContentDesigner.Dialogs
{
	public partial class ContentFolderTree : System.Web.UI.UserControl
	{
		public string Filter = "";
		protected SiteAPI m_refSiteApi = new SiteAPI();
		protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
        private string _info;
        private string _filterBy = string.Empty;
        private long _defaultFolder;
        private Ektron.Cms.Content.ContentCriteria criteria = new Ektron.Cms.Content.ContentCriteria();
        protected string idValue = string.Empty;
        private bool _showTree = true;
        private bool _showGrid = true;
        private int _recordsPerPage = 10;
        
        public bool ShowTree 
        {
            get { return _showTree; }
            set { _showTree = value; }
        }
        public bool ShowGrid
        {
            get { return _showGrid; }
            set { _showGrid = value; }
        }
        private string ProcessingSearch
        {
            get { return ViewState["ProcessingSearch"] as string; }
            set { ViewState["ProcessingSearch"] = value; }
        }
        private string GridFolderId
        {
            get { return ViewState["GridFolderId"] as string; }
            set { ViewState["GridFolderId"] = value; }
        }
        private long CurrentFolderId
        {
            get
            {
                if (!Page.IsPostBack)
                {
                    if (_defaultFolder > 0)
                    {
                        return _defaultFolder;
                    }
                    return 0;
                }
                else if (null == folderTree.CurrentNodeId || "" == folderTree.CurrentNodeId)
                {
                    if (GridFolderId != null)
                    {
                        return Int64.Parse(GridFolderId);
                    }
                    else if (!string.IsNullOrEmpty(InFolder.Value) && InFolder.Value != "0")
                    {
                        return Int64.Parse(InFolder.Value);
                    }
                    return 0;
                }
                return Int64.Parse(folderTree.CurrentNodeId);
            }
        }
        private Ektron.Cms.PagingInfo GridPagingInfo
        {
            get 
            {
                if (ViewState["GridPagingInfo"] == null)
                {
                    PagingInfo info = new PagingInfo();
                    info.RecordsPerPage = _recordsPerPage;
                    ViewState["GridPagingInfo"] = info;
                }
                return ViewState["GridPagingInfo"] as PagingInfo; 
            }
            set { ViewState["GridPagingInfo"] = value; }
        }
        private Ektron.Cms.PagingInfo SearchPagingInfo
        {
            get
            {
                if (ViewState["SearchPagingInfo"] == null)
                {
                    PagingInfo info = new PagingInfo();
                    info.RecordsPerPage = _recordsPerPage;
                    ViewState["SearchPagingInfo"] = info;
                }
                return ViewState["SearchPagingInfo"] as PagingInfo;
            }
            set { ViewState["SearchPagingInfo"] = value; }
        }

        protected void Page_Load(Object sender, EventArgs e)
		{
            ContentAPI capi = new ContentAPI();
            m_refMsg = m_refSiteApi.EkMsgRef;
            RegisterResources();
            string SelectorType = "content";
            if (!string.IsNullOrEmpty(Request.QueryString["SelectorType"]))
            {
                SelectorType = Request.QueryString["SelectorType"];
            }
            if ("folder" == SelectorType || "startingfolder" == SelectorType)
            {
                ShowGrid = false;
            }
            string FolderNavigation = "descendant";
            if (!string.IsNullOrEmpty(Request.QueryString["folderNavigation"]) && Request.QueryString["folderNavigation"] != "null")
            {
                FolderNavigation = Request.QueryString["folderNavigation"];
            }
            _filterBy = "content:htmlcontent ";
            if (!string.IsNullOrEmpty(Request.QueryString["filter"]))
            {
                _filterBy = Request.QueryString["filter"];
            }
            if (!Page.IsPostBack && !string.IsNullOrEmpty(Request.QueryString["idValue"]))
            {
                idValue = Request.QueryString["idValue"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["folder"]))
            {
                if ("startingfolder" == SelectorType)
                {
                    _defaultFolder = Int64.Parse(Request.QueryString["folder"]);
                }
                else
                {
                    folderTree.RootId = Request.QueryString["folder"];
                }
            }

            searchServerMsg.Visible = false;
            lblSearch.Text = m_refMsg.GetMessage("lbl Search within this folder");
            if (ShowTree)
            {
                // folder tree tab
                string folderPath = "0";
                if (ShowGrid)
                {
                    // Content Selector: show both tree and grid
                    selectedContent.Value = idValue;
                    if (!string.IsNullOrEmpty(idValue))
                    {
                        // folder path by the selected content
                        folderPath = updateFolderPath(SelectorType, Int64.Parse(idValue));
                    }
                    else
                    {
                        // current folder path by the selected folder node
                        folderPath = updateFolderPath("folder", CurrentFolderId);
                    }
                    InFolder.Value = CurrentFolderId.ToString();

                    if ("descendant" == FolderNavigation || "ancestor" == FolderNavigation)
                    {
                        // show folder tree from root
                        selectedFolderPath.Value = folderPath;
                    }
                    else if ("children" == FolderNavigation)
                    {
                        selectedFolderPath.Value = folderTree.RootId;
                        _defaultFolder = Int64.Parse(folderTree.RootId);
                        folderTree.Enabled = false; // only allow contents in this folder 
                    }
                    else
                    {
                        //only show content in the selected starting folder
                        folderTree.RootId = _defaultFolder.ToString();
                        selectedFolderPath.Value = CurrentFolderId.ToString();
                    }
                }
                else
                {
                    // Folder Selector: show only folder tree
                    right.Visible = false;
                    left.Attributes.Add("style", "width:98%");
                    pnlSearchArea.Visible = false;
                    contentList.Visible = false;

                    if (idValue.Length > 0)
                    {
                        folderPath = updateFolderPath(SelectorType, Int64.Parse(idValue));
                        selectedFolderPath.Value = folderPath;
                        InFolder.Value = idValue.ToString();
                    }
                    else
                    {
                        selectedFolderPath.Value = "0";
                        InFolder.Value = "0";
                    }
                }
            }
            else
            {
                // Search tab: show search result grid
                left.Visible = false;
                right.Attributes.Add("style", "width:98%");
                folderTree.Visible = false;
                lblSearch.Text = m_refMsg.GetMessage("lbl Search all content");
            }
		}
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (ShowTree && GridFolderId != CurrentFolderId.ToString())
            {
                ProcessingSearch = "false";
            }

            if (ShowTree && ShowGrid && ProcessingSearch != "true")
            {
                ProcessingSearch = "false";
                if (!Page.IsPostBack)
                {
                    if (selectedFolderPath.Value != "0")
                    {
                        GetFolderContent(_defaultFolder);
                    }
                    else
                    {
                        GetFolderContent(Int64.Parse(folderTree.RootId));
                    }
                }
                else
                {
                    if (GridFolderId != CurrentFolderId.ToString())
                    {
                        // need to reset paging info
                        GridPagingInfo = new PagingInfo();
                        GridPagingInfo.RecordsPerPage = _recordsPerPage;
                        GridFolderId = CurrentFolderId.ToString();
                        _defaultFolder = CurrentFolderId;
                    }
                    GetFolderContent(CurrentFolderId);
                }
            }
        }
        protected void folderTree_OnPreSerializeData(Object sender, EventArgs e)
        {
            if (!Page.IsPostBack && ShowTree)
            {
                foreach (var node in folderTree.ItemsFlatList)
                {
                    if (_defaultFolder.ToString() == node.Id)
                    {
                        node.Selected = true;
                        if (node.Id != folderTree.RootId)
                        {
                            ExpandAncestors(node, folderTree.ItemsFlatList);
                        }
                    }
                    node.ClickCausesPostback = true;
                }
            }
        }

        protected void OnEktronUIPageChanged(Object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            if (ShowTree) //folder tab grid
            {
                GridPagingInfo = e.PagingInfo;
                if ("true" == ProcessingSearch)
                {
                    ProcessingSearch = "true";
                    DoSearch(CurrentFolderId, selectedFolderPath.Value, false);
                }
                else
                {
                GridFolderId = _defaultFolder.ToString();
                InFolder.Value = CurrentFolderId.ToString();
                    ProcessingSearch = "false";
                }
            }
            else
            {
                SearchPagingInfo = e.PagingInfo;
                ProcessingSearch = "true";
                DoSearch(0, "", true);
            }
        }
        private string updateFolderPath(string resourceType, long idValue)
        {
        	string strFolderPath = "";
            long folderid;
            ContentAPI capi = new ContentAPI();
            if (resourceType == "content" && idValue > -1)
			{
                folderid = capi.GetFolderIdForContentId(idValue);
                _defaultFolder = folderid;
                strFolderPath = folderid.ToString();
				while (folderid != 0)
				{
					folderid = capi.GetParentIdByFolderId(folderid);
					if (folderid > 0) strFolderPath += "/" + folderid.ToString();
				}
                strFolderPath = strFolderPath.Replace("/0/", string.Empty);
			}
            else if ("folder" == resourceType && idValue > -1)
            {
                folderid = idValue;
                _defaultFolder = idValue;
                strFolderPath = idValue.ToString();
                while (folderid != 0)
                {
                    folderid = capi.GetParentIdByFolderId(folderid);
                    if (folderid > 0) strFolderPath += "/" + folderid.ToString();
                }
            }
			return strFolderPath;
		}

        protected void folderTree_OnClick(Object sender, EventArgs e)
        {
            ProcessingSearch = "false";
        }

        public void ExpandAncestors<T>(T node, IList<T> flatList, bool select = false) where T : class, ITreeNode, new()
        {
            var parentNode = GetParent<T>(node, flatList);
            if (parentNode != null)
            {
                parentNode.Expanded = true;
                parentNode.Selected = parentNode.Selected || select;
                ExpandAncestors<T>(parentNode, flatList);
            }
        }

        public T GetParent<T>(T node, IList<T> flatList) where T : class, ITreeNode, new()
        {
            if (node != null && !string.IsNullOrEmpty(node.ParentId) && node.ParentId != node.Id && flatList != null)
            {
                var parentId = node.ParentId;
                try
                {
                    var parentNode = (
                        from f in flatList
                        where f.Id == parentId
                        select f
                        ).First();
                    return parentNode;
                }
                catch { return null; }
            }
            return null;
        }
        private void GetFolderContent(long objectID)
        {
            Ektron.Cms.API.Folder folder = new Folder();
            FolderData fdata = folder.GetFolder(objectID);

            if (fdata.FolderType == 9)
            {
                // catalog
                this.getcatalogcontent(objectID);
            }
            else
            {
                if (objectID > -1)
                {
                    Ektron.Cms.Framework.Core.Content.Content contentapi = new Ektron.Cms.Framework.Core.Content.Content();
                    criteria.OrderByField = Ektron.Cms.Common.ContentProperty.Title;
                    criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                    criteria.PagingInfo = GridPagingInfo;

                    criteria.AddFilter(ContentProperty.LanguageId, CriteriaFilterOperator.EqualTo, contentapi.ContentLanguage);
                    criteria.RequiredFilterGroup.AddFilter(ContentProperty.FolderId, CriteriaFilterOperator.EqualTo, objectID);
                    criteria.RequiredFilterGroup.AddFilter(ContentProperty.SubType, CriteriaFilterOperator.NotEqualTo, EkEnumeration.CMSContentSubtype.PageBuilderData);
                    criteria.RequiredFilterGroup.AddFilter(ContentProperty.SubType, CriteriaFilterOperator.NotEqualTo, EkEnumeration.CMSContentSubtype.PageBuilderMasterData);
                    
                    List<Ektron.Cms.ContentData> cData = contentapi.GetList(criteria);

                    List<Ektron.Cms.ContentData> items = new List<Ektron.Cms.ContentData>();
                    if (cData != null && cData.Count > 0)
                    {
                        Regex rgxSF = new Regex(@"\bcontent:smartform\b");
                        foreach (Ektron.Cms.ContentData t in cData)
                        {
                            string contentType = string.Empty;
                            if (t.XmlConfiguration.Id > 0)
                            {
                                contentType = "content:smartform";
                            }
                            else
                            {
                                contentType = getContentTypeString(t.ContType);
                            }
                            Regex rgx = new Regex(@"\b" + contentType + @"\b");
                            if (rgx.IsMatch(_filterBy) && (0 == t.XmlConfiguration.Id || (t.XmlConfiguration.Id > 0 && rgxSF.IsMatch(_filterBy))) )
                            {
                                    items.Add(t);
                            }
                        }

                        if (items.Count > 0)
                        {
                            contentList.EktronUIPagingInfo = GridPagingInfo;
                            contentList.DataSource = items;
                            contentList.DataBind();
                            contentList.Visible = true;
                        }
                    }
                }
            }
        }
        private void getcatalogcontent(long objectID)
        {
            List<Ektron.Cms.ContentData> items = new List<Ektron.Cms.ContentData>();
            if (objectID > -1)
            {
                Ektron.Cms.Commerce.CatalogEntryApi catapi = new Ektron.Cms.Commerce.CatalogEntryApi();
                Ektron.Cms.Common.Criteria<Ektron.Cms.Commerce.EntryProperty> catcriteria = new Ektron.Cms.Common.Criteria<Ektron.Cms.Commerce.EntryProperty>();
                catcriteria.AddFilter(Ektron.Cms.Commerce.EntryProperty.CatalogId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, objectID);
                catcriteria.AddFilter(Ektron.Cms.Commerce.EntryProperty.IsArchived, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, false);
                catcriteria.OrderByField = Ektron.Cms.Commerce.EntryProperty.Title;
                catcriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                catcriteria.PagingInfo = GridPagingInfo;
                List<Ektron.Cms.Commerce.EntryData> eData = catapi.GetList(catcriteria);

                List<Ektron.Cms.ContentData> list = new List<Ektron.Cms.ContentData>();
                if (eData != null && eData.Count > 0)
                {
                    foreach (Ektron.Cms.Commerce.EntryData e in eData)
                    {
                        Ektron.Cms.ContentData t = new Ektron.Cms.ContentData();
                        t.ContType = 1; // content:smartform";
                        t.SubType = EkEnumeration.CMSContentSubtype.Content;
                        t.AssetData.Icon = "";
                        t.Title = e.Title;
                        t.Id = e.Id;
                        list.Add(t); 
                    }

                    if (list.Count > 0)
                    {
                        contentList.EktronUIPagingInfo = GridPagingInfo;
                        contentList.DataSource = list;
                        contentList.DataBind();
                        contentList.Visible = true;
                    }
                }
            }
        }
        private string getIdType(string contentType, string mimeType)
        {
            string sIdType;
            switch (contentType)
            {
                case "forms":
                case "Forms":
                    sIdType = "content:htmlform";
                    break;
                case "asset":
                case "Asset":
                case "content:mso":
                case "content:asset":
                case "101":
                case "102":
                case "106":
                case "103":
                    sIdType = "content:asset";
                    if (mimeType.Contains("excel"))
                    {
                        sIdType = "content:mso:xls";
                    }
                    else if (mimeType.Contains("msword"))
                    {
                        sIdType = "content:mso:doc";
                    }
                    else if (mimeType.Contains("ms-powerpoint"))
                    {
                        sIdType = "content:mso:ppt";
                    }
                    else if (mimeType.Contains("application/"))
                    {
                        sIdType += ":" + mimeType.Replace("application/", "");
                    }
                    else if (mimeType.Contains("image"))
                    {
                        sIdType += ":" + mimeType.Replace("/", ":");
                    }
                    break;
                case "multimedia":
                case "Multimedia":
                case "content:multimedia":
                    sIdType = "content:multimedia";
                    break;
                case "product":
                case "Product":
                case "content:product":
                    sIdType = "content:product";
                    break;
                case "content":
                case "Content":
                case "content:htmlcontent":
                default:
                    if (mimeType.Contains("xml"))
                    {
                        sIdType = "content:smartform";
                    }
                    else
                    {
                        sIdType = "content:htmlcontent";
                    }
                    break;
            }
            return sIdType;
        }
        protected string GetContentTypeString(object contentType)
        {
            return getContentTypeString(int.Parse(contentType.ToString()));
        }
        private string getContentTypeString(int contentType)
        {
            string sContentType;
            switch (contentType)
            {
                case 2:
                    sContentType = "content:htmlform";
                    break;
                case 101:
                    sContentType = "content:mso";
                    break;
                case 102:
                case 106:
                case 103:
                case 8:
                    sContentType = "content:asset";
                    break;
                case 104:
                    sContentType = "content:multimedia";
                    break;
                case 3333: //CatalogEntry 
                    sContentType = "content:product";
                    break;
                case 1:
                    sContentType = "content:htmlcontent";
                    break;
                case 7: //Ektron.Cms.Common.EkEnumeration.CMSContentType.LibraryItem
                    sContentType = "LibraryItem"; // not supported
                    break;
                case 3:  //Archive_Content
                case 4:  //Archive_Forms
                case 9:  //Archive_Assets    
                case 12: //Archive_Media
                case 99: //NonLibraryContent    
                case 111: //DiscussionTopic  
                default:
                    sContentType = "not supported types";
                    break;
            }
            return sContentType;
        }
        private int getContentTypeIdx(string contentType)
        {
            int intContentType;
            switch (contentType.ToLower())
            {
                case "forms":
                    intContentType = 2;
                    break;
                case "assets":
                    intContentType = 8;
                    break;
                case "101":
                    intContentType = 101;
                    break;
                case "102":
                case "103":
                    intContentType = 102;
                    break;
                case "106":
                    intContentType = 106;
                    break;
                case "multimedia":
                    intContentType = 104;
                    break;
                case "product":
                    intContentType = 3333;
                    break;
                case "content":
                default:
                    intContentType = 1;
                    break;
            }
            return intContentType;
        }
        protected string GetContentIcon(object ContentTypeID, object ContentSubType, object ImageUrl)
        {
            int contentTypeID = int.Parse(ContentTypeID.ToString());
            int contentSubType = System.Convert.ToInt16(ContentSubType);
            ContentAPI capi = new ContentAPI();
            Microsoft.VisualBasic.Collection item = new Microsoft.VisualBasic.Collection();
            item.Add(contentSubType, "ContentSubType", null, null);
            item.Add(ImageUrl.ToString(), "ImageUrl", null, null);
            return Ektron.Cms.Common.EkFunctions.getContentTypeIconAspx(contentTypeID, item, capi.ApplicationPath);
        }
        protected void DoSearchInFolder(object sender, EventArgs e)
		{
            ProcessingSearch = "true";
            if (ShowTree)
            {
                GridPagingInfo = new PagingInfo(); // reset paging everything the search button is clicked.
                GridPagingInfo.RecordsPerPage = _recordsPerPage;
                DoSearch(CurrentFolderId, selectedFolderPath.Value, false);
            }
            else
            {
            SearchPagingInfo = new PagingInfo(); // reset paging everything the search button is clicked.
            SearchPagingInfo.RecordsPerPage = _recordsPerPage;
                DoSearch(0, "", true);
            }
        }
        private void DoSearch(long folderId, string folderPath, bool onSearchTab)
        {
            ContentData content = new ContentData();
            List<Ektron.Cms.ContentData> items = new List<Ektron.Cms.ContentData>();
            if (!String.IsNullOrWhiteSpace(txtSearchBox.Text))
            {
                ContentAPI capi = new ContentAPI();

                Ektron.Cms.Framework.Search.SearchManager mgr = new Ektron.Cms.Framework.Search.SearchManager();
                KeywordSearchCriteria c = new KeywordSearchCriteria();
                c.OrderBy = new List<OrderData>() { new OrderData(SearchContentProperty.Title, OrderDirection.Ascending) };
                c.PagingInfo = (onSearchTab? SearchPagingInfo : GridPagingInfo);
                c.ReturnProperties = new HashSet<PropertyExpression>(Ektron.Cms.Search.Compatibility.PropertyMappings.ContentSearchProperties);

                c.QueryText = txtSearchBox.Text;

                Expression tree = SearchContentProperty.Language.EqualTo(capi.ContentLanguage);
                tree &= SearchType.IsNonUserContent() & SearchContentProperty.ContentSubType == 0
                        & SearchContentProperty.ContentType != 7
                        & SearchContentProperty.ContentType != 1111;

                if (false == onSearchTab)
                {
                    //tree &= SearchContentProperty.FolderIdPath.Contains(folderPath); //recursive to the child folder
                    tree &= SearchContentProperty.FolderId.EqualTo(folderId); //only on this folder
                }

                c.ExpressionTree = tree;
                bool bResetPagingInfo = false;
                try
                {
                    SearchResponseData response = mgr.Search(c);
                    //Ektron.Cms.Instrumentation.Log.WriteWarning("count = " + response.PagingInfo.TotalRecords.ToString());

                    if (response.Results.Count > 0)
                    {
                    string contentids = "";
                    foreach (Ektron.Cms.Search.SearchResultData item in response.Results)
                    {
                        contentids += "," + item[SearchContentProperty.Id].ToString();
                    }


                        Ektron.Cms.Common.ContentRequest req = new Ektron.Cms.Common.ContentRequest();
                        req.ContentType = EkEnumeration.CMSContentType.AllTypes;
                        req.GetHtml = false;
                        req.Ids = contentids;
                        req.MaxNumber = 100;
                        req.RetrieveSummary = false;
                        Ektron.Cms.Common.ContentResult res = capi.LoadContentByIds(ref req, null);

                    for (int i = 0; i < res.Item.Length; i++)
                    {
                        ContentData my = new ContentData();
                        my.Id = res.Item[i].Id;
                        my.Title = res.Item[i].Title;
                        string mimeType = (res.Item[i].XMLCollectionID > 0) ? "application/xml" : "text/html";
                        string icon = "";
                        if (res.Item[i].AssetInfo != null)
                        {
                            icon = res.Item[i].AssetInfo.Icon;
                            mimeType = res.Item[i].AssetInfo.MimeType;
                        }
                        my.ContType = getContentTypeIdx(res.Item[i].ContentType.ToString());
                        items.Add(my);
                    }

                    //databind
                    contentList.Visible = false;
                    if (items.Count > 0)
                    {
                        contentList.EktronUIPagingInfo = (onSearchTab ? SearchPagingInfo : GridPagingInfo); 
                        items = items.OrderBy(x => x.Title).ToList();
                        contentList.DataSource = items;
                        contentList.DataBind();
                        contentList.Visible = true;
                    }
                    }
                    else
                    {
                        searchServerMsg.Text = m_refMsg.GetMessage("generic no search results found");
                        searchServerMsg.DisplayMode = Message.DisplayModes.Information;
                        searchServerMsg.Visible = true;
                    }
                }
                catch (SettingsNotFoundException)
                {
                    bResetPagingInfo = true;
                    searchServerMsg.Text = m_refMsg.GetMessage("msg SettingsNotFoundException error");
                    searchServerMsg.Visible = true;
                }
                catch (ArgumentNullException)
                {
                    bResetPagingInfo = true;
                    // temporary needed before Search has implement the SettingsNotFoundException in v8.61+
                    searchServerMsg.Text = m_refMsg.GetMessage("msg SettingsNotFoundException error");
                    searchServerMsg.Visible = true;
                }
                catch (Exception ex)
                {
                    bResetPagingInfo = true;
                    searchServerMsg.Text = string.Format(m_refMsg.GetMessage("msg errors"), ex.Message);
                    searchServerMsg.Visible = true;
                }
                finally
                {
                    if (bResetPagingInfo)
                    {
                        if (onSearchTab)
                        {
                            SearchPagingInfo = new PagingInfo();
                            SearchPagingInfo.RecordsPerPage = _recordsPerPage;
                        }
                        else
                        {
                            GridPagingInfo = new PagingInfo();
                            GridPagingInfo.RecordsPerPage = _recordsPerPage;
                        }
                    }
                }
            }
        }
        private void RegisterResources()
        {
            // Register JS
            JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        }


    }

}
