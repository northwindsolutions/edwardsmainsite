<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTaxonomyTree.ascx.cs" Inherits="Ektron.ContentDesigner.Dialogs.TaxonomyTree" %>
<ektronUI:CssBlock ID="TaxTabSTyle" runat="server">
    <CssTemplate>
    .left, .right {   
        overflow:auto;
        height:85%;
    }  
    .left  { float:left; width: 25%; border:1px solid black; } 
    .right { float:right; width:73%; /*border:1px solid black;*/}  
    .ektron-ui-taxonomyTree {height: 88%; overflow:auto;}
    .ItemPathDiv {position: inherit;}
    .TaxTree { height: 100%; }
    div.TaxTree li.ektron-ui-tree-branch div.hitLocation div.selectionLocation {white-space:nowrap;}
    </CssTemplate>
</ektronUI:CssBlock>
<div id="left" runat="server" class="left">
<ektronUI:TaxonomyTree ID="TaxTree" runat="server" AutoPostBackOnSelectionChanged="true" SelectionMode="SingleForAll" UseInternalAdmin="true" CssClass="TaxTree"
    OnPreSerializeData="TaxTree_OnPreSerializeData" RootId="0" ShowRoot="false" PageDepth="1" PageSize="100">
</ektronUI:TaxonomyTree>
</div>
<div id="right" runat="server" class="right">
    <ektronUI:GridView ID="TaxonomyContentList" runat="server" AutoGenerateColumns="false" EnableEktronUITheme="true" AllowPaging="true" Caption="Taxonomy Items" 
         OnEktronUIPageChanged="OnEktronUIPageChanged"  >
        <Columns>
            <asp:TemplateField ItemStyle-Width="10%">
                <ItemTemplate>
                    <asp:literal ID="ItemIcon" runat="server" Text='<%#GetContentIcon(Eval("ContType"), Eval("SubType"), DataBinder.Eval(Container, "DataItem.AssetData.Icon")) %>'></asp:literal>
                    <asp:Label ID="ContentType" runat="server" Text='<%# GetContentTypeString(Eval("ContType")) %>' CssClass="ContentType"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ContentTitle" runat="server" Text='<%# Eval("Title") %>' CssClass="ContentTitle item"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <Columns>
            <asp:TemplateField ItemStyle-Width="18%">
                <ItemTemplate>
                    <asp:Label ID="ContentId" runat="server" Text='<%# Eval("Id") %>' CssClass="ContentId item"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </ektronUI:GridView>
</div>
<div id="ItemPathDiv" runat="server" class="ItemPathDiv">
    <div class="Ektron_TopSpaceSmall"></div>
    <asp:Label id="lblCurrentPath" ToolTip="Current Path" runat="server" CssClass="label">Current Path:</asp:Label>
    <div id="taxPath" class="taxPath" runat="server"></div>
</div>
<asp:TextBox ID="tbTaxonomyPath" ToolTip="Taxonomy Path" CssClass="HiddenTBTaxonomyPath" runat="server" style="display:none;"></asp:TextBox>
<script type="text/javascript">
<!--
    Ektron.ready(function () {
        $ektron("div.ektron-ui-taxonomyTree").find("span.textWrapper").click(function () {
            var eCurrent = $ektron(this),
                currentNodePath = eCurrent.closest("li.ektron-ui-tree-branch").attr("data-ektron-path");
            LoadTaxonomyPathName(currentNodePath);
        });
        
        var eSelectedNode = $ektron("div.ektron-ui-taxonomyTree").find("li.selected");
        if (eSelectedNode.length > 0) {
            LoadTaxonomyPathName(eSelectedNode.eq(0).attr("data-ektron-path"));
        }
    });
    function LoadTaxonomyPathName(nodePath) {
        var path, aryNode, i,
                pathName = "";
        if (nodePath.length > 0) {
            aryNode = nodePath.split('^');
            for (i = 0; i < aryNode.length; i++) {
                if (aryNode[i] > 0) {
                    path = $ektron.trim($ektron("li[data-ektron-id='" + aryNode[i] + "']").find("span.textWrapper").eq(0).text());
                    if (pathName.length > 0) {
                        pathName = pathName + " > " + path;
                    }
                    else {
                        pathName = path;
                    }
                }
            }
            $ektron("div.taxPath").html("<i>" + pathName + "</i>");
        } 
    }
//-->
</script>