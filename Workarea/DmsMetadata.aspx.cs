using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
using Microsoft.VisualBasic;
using Ektron.Cms.Framework.Settings.UrlAliasing;


public partial class Workarea_DmsMetadata : Ektron.Cms.Workarea.Page
{
    public Workarea_DmsMetadata()
    {
        m_refMsg = m_ContentApi.EkMsgRef;

    }

    protected bool TaxonomyRoleExists = false;
    protected Ektron.Cms.ContentAPI m_ContentApi = new Ektron.Cms.ContentAPI();
    protected EkMessageHelper m_refMsg;
    protected string m_idString = "";
    protected StyleHelper m_refStyle = new StyleHelper();
    protected string AppImgPath = "";
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected long m_folderId = -1;
    protected string action = string.Empty;
    protected Collection page_meta_data;
    protected string m_intTaxFolderId = "0"; // note: this is used by an include file!

    private bool isFromMultiUpload
    {
        get
        {
            bool ret = false;
            if (Request.QueryString["displayUrlAlias"] != null || Request.Form["isFromMultiUpload"] == "true")
            {
                ret = true;
            }
            return ret;
        }
    }
    private System.Collections.Hashtable htImagesAssets;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!uxAliasScriptManager.IsInAsyncPostBack)
        {
            long contentId = -1;
            Utilities.ValidateUserLogin();

            AppImgPath = (string)m_refContentApi.AppImgPath;
            ltrStyleSheetJs.Text = (new StyleHelper()).GetClientScript();
            EnhancedMetadataScript.Text = Ektron.Cms.CustomFields.GetEnhancedMetadataScript();
            EnhancedMetadataArea.Text = Ektron.Cms.CustomFields.GetEnhancedMetadataArea();
            RegisterResources();
            if (String.IsNullOrEmpty(Request.QueryString["idString"]))
            {
                m_idString = "";
            }
            else
            {
                m_idString = Request.QueryString["idString"];
            }

            if (!String.IsNullOrEmpty(Request.QueryString["folderId"]))
            {
                m_folderId = Convert.ToInt64(Request.QueryString["folderId"]);
                ltrTaxFolderId.Text = m_folderId.ToString();
                m_intTaxFolderId = m_folderId.ToString();
            }
            if ((m_ContentApi.EkContentRef.IsAllowed(m_folderId, 0, "folder", "add", m_ContentApi.RequestInformationRef.UserId) == false) || m_ContentApi.RequestInformationRef.UserId == 0)
            {
                Response.Redirect(m_ContentApi.ApplicationPath + "reterror.aspx?info=" + Server.UrlEncode(m_refMsg.GetMessage("msg login cms user")), false);
                return;
            }

            ltrActionPage.Text = "DMSMetadata.aspx?idString=" + EkFunctions.HtmlEncode(m_idString) + "&folderId=" + m_folderId;
            if (!string.IsNullOrEmpty(Request.QueryString["TaxonomyId"]))
            {
                ltrActionPage.Text += "&TaxonomyId=" + Request.QueryString["TaxonomyId"];
                jsTaxRedirectID.Text = Request.QueryString["TaxonomyId"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["action"]))
            {
                if (Request.QueryString["action"] == "Submit")
                {
                    PublishContent();
                    if (Request.QueryString["close"] == "true")
                    {
                        string uniqueKey = Convert.ToString(m_ContentApi.UserId) + Convert.ToString(m_ContentApi.UniqueId) + "RejectedFiles";
                        if (HttpContext.Current != null && HttpContext.Current.Session[uniqueKey] != null && HttpContext.Current.Session[uniqueKey].ToString().Length > 0)
                        {
                            string failedUpload = Convert.ToString(HttpContext.Current.Session[uniqueKey]);
                            failedUpload = failedUpload.Replace("\\", "\\\\");
                            failedUpload = failedUpload.Replace("'", "\\'");
                            this.jsInvalidFileTypeMsg.Text = m_refMsg.GetMessage("lbl error message for multiupload") + " " + failedUpload;
                            HttpContext.Current.Session.Remove(uniqueKey);
                        }
                        else
                        {
                            this.jsInvalidFileTypeMsg.Text = "";
                        }
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_closeTBScript", "CloseThickBoxandReload();", true);
                    }
                    else if (Request.QueryString["closeWindow"] == "true")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_refreshTop", "top.location.href = top.location.href;", true);
                        Response.Redirect("close.aspx");
                    }
                    else
                    {
                        Response.Redirect((string)("content.aspx?action=ViewContentByCategory&id=" + m_folderId));
                    }
                }
            }

            if (!String.IsNullOrEmpty(Request.QueryString["action"]))
            {
                if (Request.QueryString["action"] == "Cancel")
                {
                    if (Request.QueryString["close"] == "true")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_closeTBScript", "if(parent != null && typeof parent.ektb_remove == \'function\'){parent.ektb_remove();}", true);
                    }
                    else if (Request.QueryString["closeWindow"] == "true")
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_refreshTop", "top.location.href = top.location.href;", true);
                        Response.Redirect("close.aspx");
                    }
                    else
                    {
                        Response.Redirect((string)("content.aspx?action=ViewContentByCategory&id=" + m_folderId));
                    }
                }
            }


            if (String.IsNullOrEmpty(Request.QueryString["contentId"]))
            {
                contentId = -1;
            }
            else
            {
                try
                {
                    contentId = Convert.ToInt64(Request.QueryString["contentId"]); // should always be an integer unles an error is returned from the ajax page
                }
                catch
                {
                    contentId = -1;
                }
            }

            if (contentId != -1 && m_folderId != -1)
            {
                myMetadata.Text = CaptureMetadata(contentId, m_folderId).ToString();
                myTaxonomy.Text = CaptureTaxonomy(contentId, m_folderId);
                DisplayUrlAlias(contentId, m_folderId);
            }
            else
            {
                myMetadata.Text = "<p class=" + "\"" + "nodata" + "\"" + ">No Metadata Available.</p>";
                myTaxonomy.Text = "<p class=" + "\"" + "nodata" + "\"" + ">No Taxonomy Data Available.</p>";
            }
            if (string.IsNullOrEmpty(myMetadata.Text) && jsURLRequired.Text == "false" && myTaxonomy.Text.ToLower() == "<div id=\"emptytree\"></div>")
            {
                // nothing to dispaly close the window
                if (Request.QueryString["close"] == "true")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_closeTBScript", "CloseThickBoxandReload();", true);
                }
                else if (Request.QueryString["closeWindow"] == "true")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_refreshTop", "top.location.href = top.location.href;", true);
                    Response.Redirect("close.aspx");
                }
                else
                {
                    Response.Redirect((string)("content.aspx?action=ViewContentByCategory&id=" + m_folderId));
                }
            }
            ToolBar();
        }
    }

    private void DisplayUrlAlias(long contentId, long folderId)
    {
        bool required = false;
        ContentData cont = m_ContentApi.GetContentById(contentId);
        FolderData fdTmp = this.m_ContentApi.EkContentRef.GetFolderById(folderId);
        AliasSettings m_urlAliasSettings = ObjectFactory.GetAliasSettingsManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation()).Get();

        if (m_urlAliasSettings.IsAliasingEnabled && m_urlAliasSettings.IsManualAliasingEnabled)
        {
            if (fdTmp.AliasRequired)
            {
                required = true;
            }
        }
        if (required && !isFromMultiUpload)
        {
            if (Ektron.ASM.AssetConfig.ConfigManager.IsImageAsset(cont.AssetData.FileExtension))
                required = false;
        }
        if (required)
        {
            if (!isFromMultiUpload)
            {
                jsURLRequired.Text = "true";
                ltrShowUrlAlias.Text = "<li><a title=\"Set URL Alias\" id=\"urlAnchor\" onclick=\"dmsMetadataShowHideCategory('urlalias');return false;\" href=\"#\">URL Alias</a></li>";
                uxAliasTabContent.Visible = true;               
            }
            else
            {
                jsURLRequired.Text = "false";
                ltrShowUrlAlias.Text = "<input type='hidden' id='isFromMultiUpload' name='isFromMultiUpload' value='true' />";
            }
        }
        else
        {
            jsURLRequired.Text = "false";
            ltrShowUrlAlias.Text = "";
        }
    }

    /// <summary>
    /// Process New URL Aliasing
    /// </summary>
    /// <param name="content_edit_data"> contentData </param>
    private void ProcessAlias(long contid)
    {
        IAliasManager aliasManager = ObjectFactory.GetAliasManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
        AliasData oldalias = new AliasData();
        if (contid > 0)
        {
            oldalias = aliasManager.GetAlias(contid, m_refContentApi.RequestInformationRef.ContentLanguage, EkEnumeration.TargetType.Content);
        }

        if (!string.IsNullOrEmpty(oldalias.Alias))
        {
            action = "update";
        }

        // Alias Data
        string aliasName = "";
        string aliasextension = "";
        string fullAlias = "";
        if (Request.Form["uxAliasTabContent$uxAliasAddName"] != null) { aliasName = Request.Form["uxAliasTabContent$uxAliasAddName"].ToString(); }
        if (Request.Form["uxAliasTabContent$uxExtensionDropDownList"] != null) { aliasextension = Request.Form["uxAliasTabContent$uxExtensionDropDownList"].ToString(); }
        fullAlias = aliasName + aliasextension;
        if (!String.IsNullOrEmpty(aliasName))
        {
            //Add
            AliasData alias = new AliasData();
            alias.Type = EkEnumeration.AliasRuleType.Manual;
            alias.TargetType = EkEnumeration.TargetType.Content;
            alias.LanguageId = m_refContentApi.RequestInformationRef.ContentLanguage;
            alias.IsEnabled = true;
            alias.IsDefault = true;
            alias.Alias = fullAlias;
            alias.QueryStringAction = EkEnumeration.QueryStringActionType.None;
            alias.TargetId = contid;
            alias = aliasManager.Add(alias);
        }
        else
        {
            //Edit
            if (this.action == "update")
            {
                uxAliasTabContent.UpdateManualAliasActivationAndDefaults();
            }
        }
    }

    private StringBuilder CaptureMetadata(long contentId, long folderId)
    {
        StringBuilder metadataOutput = new StringBuilder();
        ContentAPI myContentAPI = new ContentAPI();
        ContentData myContentEditData = new ContentData();
        ContentMetaData[] myContentMetadata;
        string myType = "update";
        int myCounter = 0;
        Ektron.Cms.Site.EkSite myEkSite = new Ektron.Cms.Site.EkSite();

        int ContentLanguage = EkConstants.CONTENT_LANGUAGES_UNDEFINED;

        if (Page.Request.QueryString["LangType"] != null)
        {
            if (Page.Request.QueryString["LangType"] != "")
            {
                ContentLanguage = Convert.ToInt32(Page.Request.QueryString["LangType"]);
                myContentAPI.SetCookieValue("LastValidLanguageID", ContentLanguage.ToString());
            }
            else
            {
                if (myContentAPI.GetCookieValue("LastValidLanguageID") != "")
                {
                    ContentLanguage = Convert.ToInt32(myContentAPI.GetCookieValue("LastValidLanguageID"));
                }
            }
        }
        else
        {
            if (myContentAPI.GetCookieValue("LastValidLanguageID") != "")
            {
                ContentLanguage = Convert.ToInt32(myContentAPI.GetCookieValue("LastValidLanguageID"));
            }
        }

        if (ContentLanguage == EkConstants.CONTENT_LANGUAGES_UNDEFINED)
        {
            myContentAPI.ContentLanguage = EkConstants.ALL_CONTENT_LANGUAGES;
        }
        else
        {
            myContentAPI.ContentLanguage = ContentLanguage;
        }

        if (contentId != -1)
        {
            myEkSite = myContentAPI.EkSiteRef;
            myContentEditData = myContentAPI.GetContentById(contentId, 0);
            myContentMetadata = myContentEditData.MetaData;

            if (myContentMetadata.Length > 0)
            {
                metadataOutput = Ektron.Cms.CustomFields.WriteFilteredMetadataForEdit(myContentMetadata, false, myType, folderId, ref myCounter, myEkSite.GetPermissions(folderId, 0, "folder"));
                if (metadataOutput.Length > 0)
                {
                    ltrShowMetadata.Text = "<li><a id=\"metadataAnchor\" href=\"#\" onclick=\"dmsMetadataShowHideCategory(\'metadata\');return false;\" title=\"View Metadata\" class=\"selected\">" + myContentAPI.EkMsgRef.GetMessage("metadata text") + "</a></li>";
                }
            }
        }

        return metadataOutput;
    }

    private void SetTaxonomy(long contentid, long ifolderid)
    {
        TaxonomyBaseData[] taxonomy_cat_arr = null;
        TaxonomyRequest taxonomy_request = new TaxonomyRequest();
        TaxonomyBaseData[] taxonomy_data_arr = null;
        var ekContentRef = m_refContentApi.EkContentRef;

        taxonomy_request.TaxonomyId = ifolderid;
        taxonomy_request.TaxonomyLanguage = m_refContentApi.ContentLanguage;
        taxonomy_data_arr = ekContentRef.GetAllFolderTaxonomy(ifolderid);
        ltrTaxonomyOverrideId.Text = "0";
        if (!String.IsNullOrEmpty(Request.QueryString["TaxonomyOverrideId"]))
        {
            ltrTaxonomyOverrideId.Text = Request.QueryString["TaxonomyOverrideId"].ToString();
        }
        long taxonomyId = 0;
        if (!String.IsNullOrEmpty(Request.QueryString["TaxonomyId"]))
        {
            taxonomyId = Convert.ToInt64(Request.QueryString["TaxonomyId"]);
        }

        if (taxonomyId > 0)
        {
            ltrTaxonomyTreeIdList.Text = taxonomyId.ToString();
            if (ltrTaxonomyTreeIdList.Text.Trim().Length > 0)
            {
                taxonomy_cat_arr = ekContentRef.GetTaxonomyRecursiveToParent(taxonomyId, m_refContentApi.ContentLanguage, 0);
                if ((taxonomy_cat_arr != null) && taxonomy_cat_arr.Length > 0)
                {
                    foreach (TaxonomyBaseData taxonomy_cat in taxonomy_cat_arr)
                    {
                        if (ltrTaxonomyTreeParentIdList.Text == "")
                        {
                            ltrTaxonomyTreeParentIdList.Text = Convert.ToString(taxonomy_cat.TaxonomyId);
                        }
                        else
                        {
                            ltrTaxonomyTreeParentIdList.Text = ltrTaxonomyTreeParentIdList.Text + "," + Convert.ToString(taxonomy_cat.TaxonomyId);
                        }
                    }
                }
            }
        }

        this.ApplyAssignedTaxonomies(contentid, ekContentRef);

        ltrTaxFolderId.Text = ifolderid.ToString();
        m_intTaxFolderId = ifolderid.ToString();
    }

    private void ApplyAssignedTaxonomies(long contentid, EkContent ekContentRef)
    {
        // Get taxonomies already assigned to object, pass to client code to check or disable taxonomy check-boxes:
        if (contentid > 0)
        {
            var taxonomy_cat_arr = this.m_refContentApi.ReadAllAssignedCategory(contentid);
            if ((taxonomy_cat_arr != null) && taxonomy_cat_arr.Length > 0)
            {
                var fetchedIds = new Dictionary<long, bool>();
                foreach (TaxonomyBaseData taxonomy_cat in taxonomy_cat_arr)
                {
                    var taxonomyNodeId = taxonomy_cat.TaxonomyId;
                    if (string.IsNullOrEmpty(this.ltrTaxonomyTreeIdList.Text))
                    {
                        this.ltrTaxonomyTreeIdList.Text = Convert.ToString(taxonomyNodeId);
                    }
                    else
                    {
                        this.ltrTaxonomyTreeIdList.Text = this.ltrTaxonomyTreeIdList.Text + "," + Convert.ToString(taxonomyNodeId);
                    }

                    if (!fetchedIds.ContainsKey(taxonomyNodeId))
                    {
                        fetchedIds.Add(taxonomyNodeId, true);
                        var ancestors = ekContentRef.GetTaxonomyRecursiveToParent(
                            taxonomyNodeId, ekContentRef.RequestInformation.ContentLanguage, 0);

                        if (ancestors != null && ancestors.Length > 0)
                        {
                            foreach (var ancestor in ancestors)
                            {
                                if (!fetchedIds.ContainsKey(ancestor.Id))
                                {
                                    fetchedIds.Add(ancestor.Id, true);
                                    if (string.IsNullOrEmpty(this.ltrTaxonomyTreeParentIdList.Text))
                                    {
                                        this.ltrTaxonomyTreeParentIdList.Text = ancestor.Id.ToString();
                                    }
                                    else
                                    {
                                        this.ltrTaxonomyTreeParentIdList.Text += "," + ancestor.Id.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private string CaptureTaxonomy(long contentId, long folderId)
    {
        StringBuilder taxonomyOutput = new StringBuilder();

        Folder myFolderApi = new Folder();
        FolderData myFolderData;
        ContentAPI myContentApi = new ContentAPI();
        TaxonomyBaseData[] myTaxonomyBaseData;
        List<long> myTaxonomyIds = new List<long>();
        System.Text.StringBuilder Js;
        Js = new System.Text.StringBuilder();
        long iTmpCaller = myContentApi.RequestInformationRef.CallerId;
        long iTmpuserID = myContentApi.RequestInformationRef.UserId;

        int ContentLanguage = EkConstants.CONTENT_LANGUAGES_UNDEFINED;

        if (!(Page.Request.QueryString["LangType"] == null))
        {
            if (Page.Request.QueryString["LangType"] != "")
            {
                ContentLanguage = Convert.ToInt32(Page.Request.QueryString["LangType"]);
                myContentApi.SetCookieValue("LastValidLanguageID", ContentLanguage.ToString());
            }
            else
            {
                if (myContentApi.GetCookieValue("LastValidLanguageID") != "")
                {
                    ContentLanguage = Convert.ToInt32(myContentApi.GetCookieValue("LastValidLanguageID"));
                }
            }
        }
        else
        {
            if (myContentApi.GetCookieValue("LastValidLanguageID") != "")
            {
                ContentLanguage = Convert.ToInt32(myContentApi.GetCookieValue("LastValidLanguageID"));
            }
        }

        if (ContentLanguage == EkConstants.CONTENT_LANGUAGES_UNDEFINED)
        {
            myContentApi.ContentLanguage = EkConstants.ALL_CONTENT_LANGUAGES;
        }
        else
        {
            myContentApi.ContentLanguage = ContentLanguage;
        }

        myContentApi.RequestInformationRef.CallerId = Ektron.Cms.Common.EkConstants.InternalAdmin;
        myContentApi.RequestInformationRef.UserId = Ektron.Cms.Common.EkConstants.InternalAdmin;

        myFolderData = myContentApi.GetFolderById(folderId, true);
        myTaxonomyBaseData = myFolderData.FolderTaxonomy;
        myContentApi.RequestInformationRef.CallerId = iTmpCaller;
        myContentApi.RequestInformationRef.UserId = iTmpuserID;

        if (!String.IsNullOrEmpty(Request.QueryString["TaxonomyId"]))
        {
            jsTaxRedirectID.Text = taxonomyselectedtree.Value = Request.QueryString["TaxonomyId"].ToString();

        }
        SetTaxonomy(contentId, folderId);

        Js.Append("function ValidateTax(){").Append(Environment.NewLine);
        if (myTaxonomyBaseData.Length > 0 && (myTaxonomyBaseData != null))
        {
            if (myFolderData.CategoryRequired == true)
            {
                Js.Append("      document.getElementById(\"taxonomyselectedtree\").value=\"\";").Append(Environment.NewLine);
                Js.Append("      for(var i=0;i<taxonomytreearr.length;i++){").Append(Environment.NewLine);
                Js.Append("         if(document.getElementById(\"taxonomyselectedtree\").value==\"\"){").Append(Environment.NewLine);
                Js.Append("             document.getElementById(\"taxonomyselectedtree\").value=taxonomytreearr[i];").Append(Environment.NewLine);
                Js.Append("         }else{").Append(Environment.NewLine);
                Js.Append("             document.getElementById(\"taxonomyselectedtree\").value=document.getElementById(\"taxonomyselectedtree\").value+\",\"+taxonomytreearr[i];").Append(Environment.NewLine);
                Js.Append("         }").Append(Environment.NewLine);
                Js.Append("       } ").Append(Environment.NewLine);
                Js.Append("      if (Trim(document.getElementById(\'taxonomyselectedtree\').value) == \'\') { ").Append(Environment.NewLine);
                Js.Append("         alert(\'" + m_refMsg.GetMessage("js tax cat req") + "\'); ").Append(Environment.NewLine);
                Js.Append("         return false; ").Append(Environment.NewLine);
                Js.Append("      } ").Append(Environment.NewLine);
                Js.Append("      return true; }").Append(Environment.NewLine);
            }
            else
            {
                Js.Append("      return true;}").Append(Environment.NewLine);
            }
            ltrTaxJS.Text = Js.ToString();

            ltrShowTaxonomy.Text = "<li><a id=\"taxonomyAnchor\" href=\"#\" onclick=\"dmsMetadataShowHideCategory(\'taxonomy\');return false;\" title=\"View Taxonomy\">" + m_refMsg.GetMessage("viewtaxonomytabtitle") + "</a></li>";
            string addTaxonomy = "<div id=" + "\"" + "TreeOutput" + "\"" + "></div>";
            return addTaxonomy;
        }
        else
        {
            Js.Append("      return true;}").Append(Environment.NewLine);
            ltrTaxJS.Text = Js.ToString();
            string addTaxonomyEmpty = "<div id=\"EmptyTree\"></div>";
            return addTaxonomyEmpty;
        }
    }
    private bool PublishContent()
    {
        ContentAPI contApi = new ContentAPI();
        EkContent contObj;
        int ContentLanguage = EkConstants.CONTENT_LANGUAGES_UNDEFINED;

        if (!(Page.Request.QueryString["LangType"] == null))
        {
            if (Page.Request.QueryString["LangType"] != "")
            {
                ContentLanguage = Convert.ToInt32(Page.Request.QueryString["LangType"]);
                contApi.SetCookieValue("LastValidLanguageID", ContentLanguage.ToString());
            }
            else
            {
                if (contApi.GetCookieValue("LastValidLanguageID") != "")
                {
                    ContentLanguage = Convert.ToInt32(contApi.GetCookieValue("LastValidLanguageID"));
                }
            }
        }
        else
        {
            if (contApi.GetCookieValue("LastValidLanguageID") != "")
            {
                ContentLanguage = Convert.ToInt32(contApi.GetCookieValue("LastValidLanguageID"));
            }
        }

        if (ContentLanguage == EkConstants.CONTENT_LANGUAGES_UNDEFINED)
        {
            contApi.ContentLanguage = EkConstants.ALL_CONTENT_LANGUAGES;
        }
        else
        {
            contApi.ContentLanguage = ContentLanguage;
        }

        object[] acMetaInfo = new object[4];
        string MetaSelect;
        string MetaSeparator;
        string MetaTextString = "";
        int ValidCounter = 0;
        int i = 0;
        bool hasMeta = false;
        if (Page.Request.Form["frm_validcounter"] != "")
        {
            ValidCounter = System.Convert.ToInt32(Page.Request.Form["frm_validcounter"]);
            hasMeta = true;
        }
        else
        {
            ValidCounter = 1;
        }

        string TaxonomyTreeIdList = "";
        TaxonomyTreeIdList = Page.Request.Form[taxonomyselectedtree.UniqueID];
        if ((!string.IsNullOrEmpty(TaxonomyTreeIdList)) && TaxonomyTreeIdList.Trim().EndsWith(","))
        {
            ltrTaxonomyTreeIdList.Text = TaxonomyTreeIdList.Substring(0, TaxonomyTreeIdList.Length - 1);
        }

        contObj = contApi.EkContentRef;
        string[] ids;
        string contId = "";
        ids = m_idString.Split(",".ToCharArray());
        htImagesAssets = new System.Collections.Hashtable();
        foreach (string tempLoopVar_contId in ids)
        {
            contId = tempLoopVar_contId;
            if (contId != "")
            {

                EditUrlAlias(long.Parse(contId), this.m_folderId);
                page_meta_data = new Collection();
                if (hasMeta == true)
                {
                    for (i = 1; i <= ValidCounter; i++)
                    {
                        acMetaInfo[1] = Page.Request.Form["frm_meta_type_id_" + i];
                        acMetaInfo[2] = contId;
                        MetaSeparator = Page.Request.Form["MetaSeparator_" + i];
                        MetaSelect = Page.Request.Form["MetaSelect_" + i];
                        if (Convert.ToInt32(MetaSelect) != 0)
                        {
                            MetaTextString = Strings.Replace(Page.Request.Form["frm_text_" + i], ", ", MetaSeparator.ToString(), 1, -1, 0);
                            if (MetaTextString.StartsWith(MetaSeparator))
                            {
                                MetaTextString = MetaTextString.Substring(MetaTextString.Length - (MetaTextString.Length - 1), (MetaTextString.Length - 1));
                            }
                            MetaTextString = CleanString(MetaTextString);
                            acMetaInfo[3] = MetaTextString;
                        }
                        else
                        {
                            MetaTextString = Strings.Replace(Page.Request.Form["frm_text_" + i], ";", MetaSeparator.ToString(), 1, -1, 0);
                            if (MetaTextString == null)
                                MetaTextString = "";
                            MetaTextString = CleanString(MetaTextString);
                            acMetaInfo[3] = MetaTextString;
                        }
                        page_meta_data.Add(acMetaInfo, i.ToString(), null, null);
                        acMetaInfo = new object[4];
                    }
                }
                if (!(string.IsNullOrEmpty(TaxonomyTreeIdList)))
                {

                    TaxonomyContentRequest request = new TaxonomyContentRequest();

                    request.ContentId = long.Parse(contId);
                    request.TaxonomyList = TaxonomyTreeIdList;
                    request.FolderID = m_folderId;
                    contObj.AddTaxonomyItem(request);
                }

                if (page_meta_data.Count > 0 && hasMeta == true)
                {
                    m_ContentApi.EkContentRef.UpdateMetaData(page_meta_data);
                }

            }
        }

        foreach (string tempLoopVar_contId in ids)
        {
            contId = tempLoopVar_contId;
            if (contId != "")
            {
                try
                {
                    string Status;
                    Status = (string)(contApi.GetContentStatusById(long.Parse(contId)));
                    long lcontId = long.Parse(contId);
                    if (htImagesAssets[lcontId] != null)// if item is exists in the hash table, the url alias is required.
                    {//process url alias required, publish image assets, checkin others
                        bool bIsPublish = (bool)htImagesAssets[lcontId];

                        if (Status == "O")
                        {
                            if (bIsPublish)
                                contApi.PublishContentById(lcontId, m_folderId, ContentLanguage, "true", -1, "");
                            else
                                contApi.EkContentRef.CheckIn(lcontId, "");
                        }
                        else if (Status == "I")
                        {
                            if (bIsPublish)
                                contApi.EkContentRef.SubmitForPublicationv2_0(lcontId, m_folderId, string.Empty);
                        }
                    }
                    else
                    {// normal process
                        if (Status == "O") // this will check in and publish
                        {
                            contApi.PublishContentById(lcontId, m_folderId, ContentLanguage, "true", -1, "");
                        }
                        else if (Status == "I") // this is just a publish
                        {
                            contApi.EkContentRef.SubmitForPublicationv2_0(lcontId, m_folderId, string.Empty);
                        }
                    }
                }
                catch (Exception)
                {
                    // I wrapped it in this try block because there is a current problem on the server where the content is already being put
                    // into published state if there are multiple pieces of content, the metadata still updates and is put in the right state.
                }
            }
        }
        return true;
    }
    public string CleanString(string inStr)
    {
        string returnValue;
        string outStr = string.Empty;
        int l;
        for (l = 1; l <= inStr.Length; l++)
        {
            if (Strings.Asc(inStr.Substring(l - 1, 1)) > 31)
            {
                outStr = outStr + inStr.Substring(l - 1, 1);
            }
        }
        returnValue = outStr;
        return returnValue;
    }

    void EditUrlAlias(long contId, long folderId)
    {
        bool required = false;
        //ContentData cont = m_ContentApi.EkContentRef.gec(contId);
        FolderData fdTmp = this.m_ContentApi.EkContentRef.GetFolderById(folderId);
        AliasSettings m_urlAliasSettings = ObjectFactory.GetAliasSettingsManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation()).Get();

        if (m_urlAliasSettings.IsAliasingEnabled && m_urlAliasSettings.IsManualAliasingEnabled)
        {
            if (fdTmp.AliasRequired)
            {
                required = true;
            }
        }
        if (!required)
            return;


        if (isFromMultiUpload)
        {
            ContentData cData = m_ContentApi.GetContentById(contId);
            if (Ektron.ASM.AssetConfig.ConfigManager.IsImageAsset(cData.AssetData.FileExtension))
                htImagesAssets.Add(contId, true);
            else
                htImagesAssets.Add(contId, false);
            return;
        }
        else
        {
            ProcessAlias(contId);
        }
    }

    private void ToolBar()
    {
        string closeWin = "";
        System.Text.StringBuilder result;
        result = new System.Text.StringBuilder();
        List<string> lstTitleBar = new List<string>();

        if (ltrShowTaxonomy.Text != "")
        {
            //divTitleBar.InnerHtml = m_refStyle.GetTitleBar((string)(m_refMsg.GetMessage("dms taxonomy title")));
            lstTitleBar.Add("Taxonomy");
        }
        if (ltrShowMetadata.Text != "")
        {
            //divTitleBar.InnerHtml = m_refStyle.GetTitleBar((string)(m_refMsg.GetMessage("dms metadata title")));
            lstTitleBar.Add("Metadata");
        }
        if (ltrShowUrlAlias.Text != "")
        {
            lstTitleBar.Add("Url Alias");
        }

        if (lstTitleBar.Count > 0)
        {
            string titlebarFormat = "Add {0} for files";
            divTitleBar.InnerHtml = m_refStyle.GetTitleBar(string.Format(titlebarFormat, string.Join(",", lstTitleBar.ToArray())));
        }
        else
        {
            divTitleBar.InnerHtml = m_refStyle.GetTitleBar("");
        }

        if (Request.QueryString["close"] == "true")
        {
            closeWin = "close";
        }
        result.Append("<table ><tr>");
        result.Append(m_refStyle.GetButtonEventsWCaption((string)(m_refContentApi.AppPath + "images/UI/Icons/cancel.png"), "#", (string)(m_refMsg.GetMessage("generic Cancel")), (string)(m_refMsg.GetMessage("generic Cancel")), "onclick=\"return CancelForm(\'form1\', \'\', \'" + closeWin + "\');\"", StyleHelper.CancelButtonCssClass, true));
        result.Append(m_refStyle.GetButtonEventsWCaption((string)(m_refContentApi.AppPath + "images/ui/icons/contentpublish.png"), "#", (string)(m_refMsg.GetMessage("generic Publish")), (string)(m_refMsg.GetMessage("generic Publish")), "onclick=\"return SubmitForm(\'form1\', \'\', \'" + closeWin + "\');\"", StyleHelper.PublishButtonCssClass, true));
        result.Append(StyleHelper.ActionBarDivider);
        result.Append("<td>");
        result.Append(m_refStyle.GetHelpButton("addmetadataforfiles", ""));
        result.Append("</td>");
        result.Append("</tr></table>");
        divToolBar.InnerHtml = result.ToString();
    }

    private void RegisterResources()
    {
        JS.RegisterJS(this, m_refContentApi.AppPath + "java/workareahelper.js", "EktronWorkareaHelperJS");
        Packages.Ektron.Workarea.Core.Register(this);
    }
}
