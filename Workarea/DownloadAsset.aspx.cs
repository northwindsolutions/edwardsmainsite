using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Data;
using System.Web.Caching;
using System.Xml.Linq;
using System.Web.UI;
using System.Diagnostics;
using System.Web.Security;
using System;
using System.Text;
using Microsoft.VisualBasic;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.Profile;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Web;
using System.IO;
using System.Net;
using Ektron.Cms;
using Ektron.ASM.AssetConfig;
using Ektron.ASM.FileHandler;
using Ektron.Storage;
using Ektron.Cms.Content;
	public partial class DownloadAsset : Ektron.Cms.Workarea.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Ektron.Cms.ContentAPI content_api = new Ektron.Cms.ContentAPI();
			try
			{
				Ektron.Cms.ContentData content_data = null;
				long asset_id = 0;
				bool valid_attempt = false;
				int LangbackUp = 0;
				if (! (Request.QueryString["id"] == null))
				{
					asset_id = Convert.ToInt64(Request.QueryString["id"]);
				}
				LangbackUp = content_api.ContentLanguage;
				if (Request.QueryString["LangType"] != null && content_api.ContentLanguage == -1)
				{
					content_api.ContentLanguage = Convert.ToInt32(Request.QueryString["LangType"]);
				}
				if (content_api.ContentLanguage == -1)
				{
					content_api.ContentLanguage = int.Parse(content_api.GetCookieValue("SiteLanguage"));
				}
				long iTmpCaller = content_api.RequestInformationRef.CallerId;
				if (asset_id > 0)
				{
					content_api.RequestInformationRef.CallerId = Ektron.Cms.Common.EkConstants.InternalAdmin;
					content_api.RequestInformationRef.UserId = Ektron.Cms.Common.EkConstants.InternalAdmin;
					try
					{
						content_data = content_api.GetContentById(asset_id, 0);
					}
					catch
					{
					}
					finally
					{
						content_api.RequestInformationRef.CallerId = iTmpCaller;
						content_api.RequestInformationRef.UserId = iTmpCaller;
					}
					if (content_data != null)
					{
						content_api.ContentLanguage = content_data.LanguageId;
						content_data = null;
					}
					content_data = content_api.ShowContentById(asset_id, content_api.CmsPreview, System.Convert.ToBoolean(! content_api.CmsPreview));
					content_api.ContentLanguage = LangbackUp;
				}
				if ((content_data != null) && (content_data.AssetData != null) && (content_data.AssetData.Version.Length > 0))
				{
                    string filepath;
                    AssetData _assetData = new AssetData();
                    

                    if (content_api.CmsPreview)
                    {
                        _assetData.AssetDataForIDVersion(content_data.AssetData.Id, content_data.AssetData.Version);
                        filepath = DocumentManagerData.Instance.StorageLocation + _assetData.Storage + ConfigManager.pathChar + _assetData.Name;
                    }
                    else
                    {
                        _assetData.AssetDataFromAssetID(content_data.AssetData.Id);
                        filepath = Page.Server.MapPath((string)(content_api.EkContentRef.GetViewUrl(Convert.ToInt32(content_data.Type), content_data.AssetData.Id).Replace(Page.Request.Url.Scheme + "://" + Page.Request.Url.Authority, "").Replace(":443", "").Replace(":80", "")));
                    }
					if (filepath != null)
					{
                        string filename = Path.GetFileName(filepath);
                        string ext = "";
                        ext = Path.GetExtension(filepath);
                        if (StorageClient.Context.File.Exists(filepath))
                        {
                            valid_attempt = true;
                            if (ext.Contains("pdf") || ext.Contains("pps"))
                            {
                                Stream stream = Ektron.Storage.StorageClient.Context.File.DownloadStream(filepath);
                                var memoryStream = new MemoryStream();
                                stream.CopyTo(memoryStream);
                                byte[] Buffer = memoryStream.ToArray();
								if (Buffer.Length > 0)
								{
									valid_attempt = true;
									Response.Clear();
									Response.ContentType = (string) ((ext.Contains("pdf")) ? "application/pdf" : "application/vnd.ms-powerpoint");
                                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + (Request.Browser.Browser == "IE" ? (Server.UrlPathEncode(System.IO.Path.GetFileNameWithoutExtension(_assetData.Handle))) : (System.IO.Path.GetFileNameWithoutExtension(_assetData.Handle))) + ext + "\"");
                                    Response.AddHeader("Content-Length", Buffer.Length.ToString());
									Response.BinaryWrite(Buffer);
								}
							}
							else
							{
                                //if (ext.Contains("txt") || ext.Contains("nxb"))
                                //{
                                //    filepath = DocumentManagerData.Instance.StorageLocation + _assetData.Storage + ConfigManager.pathChar + _assetData.Name;
                                //}
                                Response.Clear();
                                Response.ContentType = content_data.AssetData.MimeType;
                                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + (Request.Browser.Browser == "IE" ? (Server.UrlPathEncode(_assetData.Handle)) : _assetData.Handle) + "\"");
                                try
                                {
                                    byte[] content;
                                    Stream sourceFile = StorageClient.Context.File.DownloadStream(filepath);
                                    long fileSize = sourceFile.Length;
                                    Response.AddHeader("Content-Length", fileSize.ToString());
                                    if (fileSize == 0)
                                    {
                                        fileSize = 1;
                                        content = new byte[1];
                                        content[0] = 0x20;
                                    }
                                    else
                                    {
                                        content = new byte[(int)fileSize];
                                        sourceFile.Read(content, 0, (int)fileSize);
                                        sourceFile.Close();
                                    }
                                    Response.OutputStream.Write(content, 0, (int)fileSize);
                                }
                                catch { }
                            }
                            Response.Flush();
                            try
                            {
                                Response.End();
                            }
                            catch
                            {
                            }
                        }
                        //try to get it from other servers
                        else
                        {
                            if (!LoadBalanceRequestFile.isLoadBalanceFileRequest(HttpContext.Current.Request.Url.AbsoluteUri))
                            {
                                byte[] filedata = LoadBalanceRequestFile.GetFileFromNetwork(HttpContext.Current, content_api.EkContentRef.GetViewUrl(Convert.ToInt32(content_data.Type), content_data.AssetData.Id));
                                if (filedata != null) valid_attempt = true;

                                Response.Clear();
                                Response.ContentType = LoadBalanceRequestFile.getMimeType(ext.Replace(".", ""));
                                if (ext.Contains("pdf") || ext.Contains("pps"))
                                {
                                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + (Request.Browser.Browser == "IE" ? (Server.UrlPathEncode(System.IO.Path.GetFileNameWithoutExtension(_assetData.Handle))) : (System.IO.Path.GetFileNameWithoutExtension(_assetData.Handle))) + ext + "\"");
                                }
                                else
                                {
                                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + (Request.Browser.Browser == "IE" ? (Server.UrlPathEncode(_assetData.Handle)) : _assetData.Handle) + "\"");
                                }
                                Response.BinaryWrite(filedata);
                                Response.Flush();
                                try { Response.End(); }
                                catch { }
                            }
                        }
					}
				}
				if (! valid_attempt)
				{
                    Response.StatusCode = 403;
					notification_message.Text = "File does not exist or you do not have permission to view this file";
					notification_message.ToolTip = "Error Message - " + notification_message.Text;
					// Register CSS
					Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
					Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.AllIE);
                    Login.Visible = System.Convert.ToBoolean(!content_api.IsLoggedIn);
					content_api.RequestInformationRef.RedirectFromLoginKeyName = Request.Url.PathAndQuery.ToString();
					Login.RedirectFromLoginPage();
					Login.Fill();
				}
				
			}
			catch (Exception)
			{
                Response.StatusCode = 403;
                notification_message.Text = "File does not exist or you do not have permission to view this file";
				// Register CSS
				Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
				Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.AllIE);
                Login.Visible = System.Convert.ToBoolean(!content_api.IsLoggedIn);
				content_api.RequestInformationRef.RedirectFromLoginKeyName = Request.Url.PathAndQuery.ToString();
				Login.RedirectFromLoginPage();
				Login.Fill();
			}
			
		}
	}
