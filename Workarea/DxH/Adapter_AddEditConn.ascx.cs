﻿namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.DxH.Client;
    using Ektron.Cms.Framework.UI;
    using Ektron.DxH.Common.Connectors;
    using Ektron.DxH.Common.Objects;
    using Ektron.Cms.Core;
    using Ektron.Cms.Common;

    public partial class Adapter_AddEditConn : System.Web.UI.UserControl
    {
        private ContextBusClient cbClient = null;
        private ConnectionManagerClient ConnMgr;
        private List<Ektron.DxH.Common.Connectors.ConnectionParam> param=null;
        private List<Control> editCtrls = null;
        private string labelPrefix = "ltrTF_";
        private string controlPrefix = "TF_";
        private bool IsEdit { get { return !string.IsNullOrEmpty(ConnectionName); } }

        public string AdapterName { get; set; }
        private string AdapterDiaplayName
        {
            
            get;
            set;
        }
        public string ConnectionName { get; set; }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            try
            {
                Packages.Ektron.Namespace.Register(this);
                Packages.Ektron.CssFrameworkBase.Register(this);
                Packages.Ektron.Workarea.Core.Register(this);
                cbClient = new ContextBusClient();
                ConnMgr = new ConnectionManagerClient();
                if (!IsEdit)
                {
                    param = ConnMgr.GetConnectionParameterList(AdapterName).ToList();
                }
                else
                {
                    param = ConnMgr.LoadConnection(ConnectionName, AdapterName).ToList();
                }

                

                ConnectorFlyWeight ada_obj = cbClient.GetRegisteredAdapterList().Find(x => x.Id == AdapterName);
                AdapterDiaplayName = ada_obj.DisplayName;
                SetUIText();
                editCtrls = new List<Control>();
                buildChildControls();

                if (AdapterName == "Ektron" && !Page.IsPostBack && !IsEdit)
                {
                    PrePopulteEktronConn();
                }
            }
            catch (Exception ex)
            {
                GeneralExceptionHandling(ex);
            }
        }

        protected string GetSavePostBack()
        {
            return Page.ClientScript.GetPostBackEventReference(btnSave, string.Empty);
        }
        
        protected void btnTestRetry_Click(object sender, EventArgs e)
        {
            VerifyConnectionInfo();
        }
        protected void btnTest_Click(object sender, EventArgs e)
        {
            VerifyConnectionInfo();
        }

        protected void VerifyConnectionInfo()
        {
            try
            {
                if (!isConnNameUnique(tfConnName.Text) && !IsEdit)
                {
                    //Name is not unique
                    populateValuesFromPage();
                    msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    msgBox.Text = GetLocalResourceObject("strNameConflict").ToString();
                    btnTest.Visible = false;
                    btnTestRetry.Visible = true;
                }
                else
                {

                    if (TestConnection())
                    {
                        msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                        msgBox.Text = GetLocalResourceObject("strTestSucess").ToString();
                    }
                    else
                    {
                        msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                        msgBox.Text = GetLocalResourceObject("strTestFail").ToString();
                        btnTest.Visible = false;
                        btnTestRetry.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                msgBox.Text = GetLocalResourceObject("strTestFail").ToString();
                btnTest.Visible = false;
                btnTestRetry.Visible = true;
            }
            msgBox.Visible = true;

        }
        protected void btnSaveConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                populateValuesFromPage();

                // DXH Connection
                CreateOrUpdateConnection();

                pnlEditArea.Visible = false;
                msgConfirm.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                msgConfirm.Visible = true;
                ltrPageHeadign.Text = string.Format(GetLocalResourceObject("strSaveOKConfirmHeader").ToString(), AdapterDiaplayName);
                msgConfirm.Text = string.Format(GetLocalResourceObject("strSaveOK").ToString(), AdapterDiaplayName, tfConnName.Text);
                pnlConfirmArea.Visible = true;
                btnSave.Visible = false;
                btnCancel.Text = GetLocalResourceObject("btnClose").ToString();
                btnSaveConfirm.Visible = false;
                //redirect
                //btnCancel.OnClientClick = "closeDialog(); window.location='ViewDXHConnections.aspx'; return false;";
                Response.Redirect("ViewDXHConnections.aspx", true);
            }
            catch (Exception ex)
            {
                msgBox.Visible = true;
                msgBox.Text = GetLocalResourceObject("strErrSaving").ToString();
                btnSave.Visible = false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isConnNameUnique(tfConnName.Text) && !IsEdit)
                {
                    //Name is not unique
                    msgBox.Visible = true;
                    msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    msgBox.Text = GetLocalResourceObject("strNameConflict").ToString();

                    btnSave.Visible = false;
                    btnSaveConfirm.Visible = false;
                    btnTestRetry.Visible = true;
                    btnTest.Visible = false;

                }
                else
                {
                    if (TestConnection())
                    {
                        // DXH Connection
                        CreateOrUpdateConnection();

                        pnlEditArea.Visible = false;
                        msgConfirm.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                        msgConfirm.Visible = true;
                        if (!IsEdit)
                        {
                            ltrPageHeadign.Text = string.Format(GetLocalResourceObject("strSaveOKConfirmHeader").ToString(), AdapterDiaplayName);
                            msgConfirm.Text = string.Format(GetLocalResourceObject("strSaveOK").ToString(), AdapterDiaplayName, tfConnName.Text);
                        }
                        else
                        {
                            ltrPageHeadign.Text = string.Format(GetLocalResourceObject("strSaveOKConfirmHeader_Edit").ToString(), AdapterDiaplayName);
                            msgConfirm.Text = string.Format(GetLocalResourceObject("strSaveOK_Edit").ToString(), AdapterDiaplayName, tfConnName.Text);
                        }
                        pnlConfirmArea.Visible = true;
                        btnSave.Visible = false;
                        btnCancel.Text = GetLocalResourceObject("btnClose").ToString();
                    }
                    else
                    {
                        msgBox.Visible = true;
                        msgBox.Text = string.Format(GetLocalResourceObject("strSaveFailConfirm").ToString(), AdapterDiaplayName, tfConnName.Text);
                        btnSave.Visible = false;
                        btnSaveConfirm.Visible = true;
                        btnTestRetry.Visible = true;
                        btnTest.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                GeneralExceptionHandling(ex);
            }
        }

        /// <summary>
        /// Creates Or Update a DXH connection.
        /// </summary>
        private void CreateOrUpdateConnection()
        {
            var connectionName = tfConnName.Text;
            ConnMgr.Create(new Connection()
            {
                Name = connectionName,
                ConnectorName = AdapterName,
                Parameters = param
            });

            var settingsId = EkEnumeration.SiteSetting.DxHInboundConnection.GetHashCode();

            // If it is an Ektron Connection, Then a Inbound Connection to settings table.
            if (AdapterName.ToLower() == Ektron.DxH.Common.Constants.Adapter.Ektron.ToLower())
            {
                // Check for Existing Data.
                var settings = new SiteSetting();
                var dxhSetting = settings.GetItem(settingsId);
                if ((dxhSetting != null) && (dxhSetting.Value != ""))
                {
                    // Updates Existing InBound Connection Setings.
                    dxhSetting.Id = settingsId;
                    dxhSetting.SiteId = 0;
                    dxhSetting.Value = connectionName;
                    settings.Update(dxhSetting);
                }
                else
                {
                    // Add a New InBound Connection Setings.
                    var settingData = new SiteSettingData();
                    settingData.Id = settingsId;
                    settingData.SiteId = 0;
                    settingData.Value = connectionName;
                    settings.Add(settingData);
                }
            }
        }

        private void populateValuesFromPage()
        {
            Control c = null;
            string CtrlID = "";
            foreach (Ektron.DxH.Common.Connectors.ConnectionParam p in param)
            {
                CtrlID = controlPrefix + p.Id;
                c = editCtrls.Single(x => x.ID == CtrlID);
                p.Value = c.GetType().GetProperty("Value").GetValue(c, null) as string;
            }
        }
        private bool TestConnection()
        {
            populateValuesFromPage();
            bool result = false;
            try
            {
                result = ConnMgr.TestConnection(param, AdapterName);
            }
            catch 
            {
                result = false;
            }

            // If Test fails, display the password field, else hide the password field.
            if (!result)
            {
                editCtrls.FindAll(x => x.GetType().Equals(typeof(Ektron.Cms.Framework.UI.Controls.EktronUI.PasswordField)) || x.GetType().Equals(LoadControl("Controls/RequiredPwd.ascx").GetType())).ForEach(x => x.Visible = true);
                string scriptString = @"$ektron('span.asterisk').css('display', 'none');";
                ScriptManager.RegisterStartupScript(btnTest, this.GetType(), "removePasswordText", scriptString.ToString(), true);
            }
            else
            {
                editCtrls.FindAll(x => x.GetType().Equals(typeof(Ektron.Cms.Framework.UI.Controls.EktronUI.PasswordField)) || x.GetType().Equals(LoadControl("Controls/RequiredPwd.ascx").GetType())).ForEach(x => x.Visible = false);
            }
            return result;
        }
        private bool isConnNameUnique(string ConnName)
        {
            bool ret = true;
            try
            {
                List<Ektron.DxH.Common.Connectors.ConnectionParam> parms= ConnMgr.LoadConnection(ConnName, AdapterName).ToList();
                if (parms != null && parms.Count > 0)
                {
                    ret = false;
                }
            }
            catch (Exception ex)
            {
            }
            return ret;
        }
        private void buildChildControls()
        {
            Table tbl = new Table();
            tbl.ID = "tblEditBody";

            foreach (Ektron.DxH.Common.Connectors.ConnectionParam p in param)
            {
                TableRow tr = new TableRow();

                TableCell tdLeft = new TableCell();
                TableCell tdRight = new TableCell();

                tdLeft.CssClass = "editTDLeft";
                tdRight.CssClass = "editTDRight";

                tdLeft.Style.Add("font-weight", "bold");

                Literal ltr = new Literal();
                ltr.ID = labelPrefix + p.Id;
                ltr.Text = p.DisplayName + ":";
                if (p.IsRequired)
                {
                    ltr.Text += " <span class=\"ektron-ui-required\">*</span>";
                }
                Control c = null;
                if (p.IsRequired && p.IsPassword)
                {
                    c = LoadControl("Controls/RequiredPwd.ascx");
                }
                else if (p.IsRequired)
                {
                    c = LoadControl("Controls/RequiredField.ascx");
                }
                else if (p.IsPassword)
                {
                    c = LoadControl(typeof(Ektron.Cms.Framework.UI.Controls.EktronUI.PasswordField), null);
                }
                else
                {
                    c = LoadControl(typeof(Ektron.Cms.Framework.UI.Controls.EktronUI.TextField), null);
                }
                c.ID = controlPrefix + p.Id;
                c.GetType().GetProperty("CssClass").SetValue(c, "editCtrls", null);
                if (IsEdit&& !IsPostBack)
                {
                    c.GetType().GetProperty("Value").SetValue(c, p.Value, null);
                    pnlConnNameArea.Visible = false;
                    tfConnName.Visible=false;
                    tfConnName.Text=ltrConnNameReadOnly.Text = ConnectionName;
                    ltrConnName.Visible = false;
                }
                if (p.IsPassword && this.Page.IsPostBack)
                {
                    c.Visible = false;
                }
                editCtrls.Add(ltr);
                editCtrls.Add(c);

                tdLeft.Controls.Add(ltr);

                if (p.IsPassword && this.Page.IsPostBack)
                {
                    Literal ltPWDMask = new Literal();
                    ltPWDMask.ID = "ltPWDMask_" + p.Id;
                    ltPWDMask.Text = "<span class='asterisk'>";
                    ltPWDMask.Text += new string('*', 10);
                    ltPWDMask.Text += "</span>";
                    tdRight.Controls.Add(ltPWDMask);
                }
                tdRight.Controls.Add(c);

                string Parm_instr_reskey = string.Format("strDesc_1_{0}_{1}", AdapterName, p.Id.ToLower());
                if (GetLocalResourceObject(Parm_instr_reskey) != null)
                {
                    if(!string.IsNullOrEmpty(GetLocalResourceObject(Parm_instr_reskey).ToString()))
                    {
                        TableRow trInst = new TableRow();
                        string instText = GetLocalResourceObject(Parm_instr_reskey).ToString();
                        TableCell td2Col = new TableCell();
                        td2Col.ColumnSpan = 2;
                        Literal ltr_Para_Inst = new Literal();
                        ltr_Para_Inst.ID = "Ltr_" + Parm_instr_reskey;
                        ltr_Para_Inst.Text = instText;
                        td2Col.Controls.Add(ltr_Para_Inst);
                        trInst.Cells.Add(td2Col);
                        tbl.Rows.Add(trInst);
                    }
                }
                tr.Cells.Add(tdLeft);
                tr.Cells.Add(tdRight);

                tbl.Rows.Add(tr);
            }
            this.pnlControlHolder.Controls.Add(tbl);
        }
        private void SetUIText()
        {
            string purdue = isStartCharVowel(AdapterDiaplayName) ? "An" : "A";
            string Title = IsEdit ? string.Format(GetLocalResourceObject("strHeadingEdit").ToString(), AdapterDiaplayName, ConnectionName) : string.Format(GetLocalResourceObject("strHeadingAdd").ToString(), AdapterDiaplayName);
            string[] pNames=param.ConvertAll<string>(x=>x.DisplayName).ToArray();
            string Instruction = "";
            string pNameJoin = string.Join(", ", pNames);
            int idxName = pNameJoin.LastIndexOf(",");

            if (idxName > 0)
            {
                pNameJoin = pNameJoin.Remove(idxName, 1);
                pNameJoin = pNameJoin.Insert(idxName, " and");
            }

            if (IsEdit)
            {
                Instruction = string.Format(GetLocalResourceObject("strInstructionEdit").ToString(), pNameJoin, AdapterDiaplayName);
            }
            else
            {
                if (GetLocalResourceObject("strInstruction_" + AdapterName) == null)
                {
                    Instruction = string.Format(GetLocalResourceObject("strInstruction_All").ToString(), pNameJoin, AdapterDiaplayName);
                }
                else
                {
                    Instruction = string.Format(GetLocalResourceObject("strInstruction_" + AdapterName).ToString(), pNameJoin, AdapterDiaplayName);
                }
            }

            string ConnNameInstrnction = string.Format(GetLocalResourceObject("strConnNameInst").ToString(), AdapterDiaplayName);

            string descKey = "strDesc_" + AdapterName;
            if (GetLocalResourceObject(descKey) != null)
                ltrDescText.Text = GetLocalResourceObject(descKey).ToString();
            else
            {
                string sAll = GetLocalResourceObject("strDesc_All").ToString();
                sAll = string.Format(sAll, AdapterDiaplayName, purdue);
                ltrDescText.Text = sAll;
            }
            ltrPageHeadign.Text = Title;
            ltrInstruction.Text = Instruction;
            ltrConnName.Text = ConnNameInstrnction;
            diagAddEditConn.Title = Title;
        }
        private bool isStartCharVowel(string s)
        {
            bool ret = false;
            switch (s.ToLower().ToArray()[0])
            {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    ret = true;
                    break;
            }
            return ret;
        }
        private void GeneralExceptionHandling(Exception ex)
        {
            pnlEditArea.Visible = false;
            pnlExceptions.Visible = true;
            diagAddEditConn.Title = ltrPageHeadign.Text = GetLocalResourceObject("strExceptionHeading").ToString();
            
            if (ex != null)
            {
                if (ex.Message != null)
                    ltrExcText.Text = ex.Message;
                if (ex.StackTrace != null)
                    ltrExcText.Text += "<br/>" + ex.StackTrace;
            }

            btnSave.Visible = false;
        }
        private void PrePopulteEktronConn()
        {
            Ektron.Cms.ContentAPI m_refContentApi = new Ektron.Cms.ContentAPI();

            Control c = null;
            string CtrlID = "";
            foreach (Ektron.DxH.Common.Connectors.ConnectionParam p in param)
            {
                CtrlID = controlPrefix + p.Id;
                c = editCtrls.Single(x => x.ID == CtrlID);

                switch (p.Id)
                {
                    case "username":
                        c.GetType().GetProperty("Value").SetValue(c, m_refContentApi.RequestInformationRef.LoggedInUsername, null);
                        break;
                    case "siteurl":
                        if (Request.Url.Port == 80)
                            c.GetType().GetProperty("Value").SetValue(c, Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host + m_refContentApi.RequestInformationRef.SitePath, null);
                        else
                            c.GetType().GetProperty("Value").SetValue(c, Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host + ":" + Request.Url.Port + m_refContentApi.RequestInformationRef.SitePath, null);
                        break;
                }
            }
        }

        Control FindChildRecursive(Control control, string id)
        {
            if (control == null) return null;
            //try to find the control at the current level            
            Control ctrl = control.FindControl(id); 
            if (ctrl == null)
            {
                //search the children                
                foreach (Control child in control.Controls) 
                {
                    ctrl = FindChildRecursive(child, id); 
                    if (ctrl != null) 
                        break; 
                }
            } return ctrl;
        }
    }
}