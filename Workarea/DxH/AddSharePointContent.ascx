﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddSharePointContent.ascx.cs"
    Inherits="Ektron.Workarea.DxH.AddSharePointContent" %>
<asp:PlaceHolder ID="uxAddSharePointContentDialog" runat="server">
    <div class="uxAddSharePointContentDialog" runat="server" style="width: 640">
        <asp:Literal runat="server" ID="ltrAddSharePointContentMsg" Text="<% $Resources: ltrAddSharePointContentMsg %>" />
        <div class="ektronTopSpace">
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbSharePointConnection" ClientIDMode="Static" Text="<%$Resources: lbSharePointConnection %>"
                        runat="server" Font-Bold="true" />
                    <span class="ektron-ui-required">*</span>
                </td>
                <td>
                    <asp:DropDownList runat="server" DataTextField="ConnectionName" DataValueField="ConnectionName"
                        OnSelectedIndexChanged="sharePointConnectionList_OnSelectedIndexChanged" EnableViewState="true"
                        ID="sharePointConnectionList" AppendDataBoundItems="true" AutoPostBack="true">
                        <asp:ListItem Text="<%$ Resources: SelectConnectionName  %>" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
</asp:PlaceHolder>
<ektronUI:Dialog ID="uxErrorDialog" runat="server" Modal="True" Resizable="true"
    Visible="false" EnableViewState="False" AutoOpen="False" Draggable="True" Enabled="False"
    Stack="True">
    <ContentTemplate>
        <div id="connectionError" class="ektron-ui-clearfix">
            <asp:Image ID="aspDXHLogo" runat="server" ToolTip="<% $Resources: dxhlogo %>" Visible="true"
                ImageUrl="~/Workarea/DxH/images/dxh_logo_60x60.png" />
            <h2 class="dxhErrorModal"><asp:Literal ID="ltrConnectionError" runat="server" Visible="true"></asp:Literal></h2>
        </div>
        <ektronUI:Message ID="msgConnectionError" runat="server" DisplayMode="Error" />
        <asp:Literal ID="ltrTryAgain" runat="server" Text="<%$Resources:ltrTryAgain %>"></asp:Literal>
    </ContentTemplate>
    <Buttons>
        <ektronUI:DialogButton runat="server" ID="btnClose" CloseDialog="True" CausesValidation="True"
            CommandArgument="" CommandName="" Text="<%$Resources: close %>" ToolTip="<%$Resources: close %>"
            NavigateUrl="" OnClientClick="" PostBackUrl=""></ektronUI:DialogButton>
    </Buttons>
</ektronUI:Dialog>
