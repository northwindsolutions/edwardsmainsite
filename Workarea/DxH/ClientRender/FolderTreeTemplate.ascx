﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderTreeTemplate.ascx.cs" Inherits="Tree.DXH.TreeResources.Template.ClientRender.FolderTreeTemplate" %>
<div id="<%= ClientID %>" <%= CssManager %> >

    <asp:PlaceHolder ID="placeHolder" runat="server" />

    <ul id="<%= ClientID %>_TreeRootElement" class="ektron-ui-tree-root ektron-ui-clearfix" ></ul>

    <script id="<%= ClientID %>_ItemTemplate" type="text/x-jquery-tmpl">
        {{if Visible}}
            <li data-ektron-id="${Id}" data-ektron-parentid="${ParentId}" 
                data-ektron-rootid="${RootId}" data-ektron-path="${Path}" 
                data-ektron-level="${Level}" data-ektron-selectionmode="${SelectionModeString}"

                {{if CommandName != null && CommandName.length > 0}} data-ektron-CommandName="${CommandName}" data-ektron-CommandArgument="${CommandArgument}" {{/if}}
                {{if ClickCausesPostback}} data-ektron-ClickCausesPostback="${ClickCausesPostback}" {{/if}}

                {{tmpl "#<%= ClientID %>_CustomValuesTemplate"}}

                {{if CanHaveChildren }}
                    {{if HasChildren || ChildrenNotLoaded }}
                        {{if Expanded }}
                            class="ektron-ui-tree-branch ektron-ui-tree-folder{{if Type}} ektron-ui-tree-type-${Type.toLowerCase()}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType.toLowerCase()}{{/if}}{{if Selected }} selected{{/if}} collapsible"
                        {{else}}
                            class="ektron-ui-tree-branch ektron-ui-tree-folder{{if Type}} ektron-ui-tree-type-${Type.toLowerCase()}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType.toLowerCase()}{{/if}}{{if Selected }} selected{{/if}} expandable"
                        {{/if}}
                    {{else}}
                        class="ektron-ui-tree-branch ektron-ui-tree-folder{{if Type}} ektron-ui-tree-type-${Type.toLowerCase()}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType.toLowerCase()}{{/if}}{{if Selected }} selected{{/if}}"
                    {{/if}}
                {{else}}
                    class="ektron-ui-tree-leaf ektron-ui-tree-folder{{if Type}} ektron-ui-tree-type-${Type.toLowerCase()}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType.toLowerCase()}{{/if}}{{if Selected }} selected{{/if}}"
                {{/if}}
            >
                <div class="hitLocation" >
                    <div class="selectionLocation {{if SelectionModeString}}${SelectionModeString.toLowerCase()}{{else}}nullselectionmode{{/if}}" >
                        <span class="ui-icon"> </span>
                        {{if NavigateUrl }} 
                            {{tmpl "#<%= ClientID %>_NavigateUrlTemplate"}}
                        {{else}}
                            <span class="textWrapper{{if OnClientClick || Action }} clickable{{/if}}"
                                {{if OnClientClick}} onclick="${OnClientClick}; return false;" {{/if}}
                                {{if Action }} data-ektron-action="${Action}" {{/if}}
                                > ${Text} </span>
                        {{/if}}
                    </div>
                </div>
                {{if HasChildren}}
                    {{tmpl "#<%= ClientID %>_ChildrenTemplate"}}
                {{/if}}
            </li>
        {{/if}}
    </script>

    <script id="<%= ClientID %>_NavigateUrlTemplate" type="text/x-jquery-tmpl">
        <a href="${NavigateUrl}"{{if Action }} data-ektron-action="${Action}"{{/if}} class="textWrapper{{if OnClientClick || Action }} clickable{{/if}}"
            {{if OnClientClick }} 
                onclick="${OnClientClick}; return false;" 
            {{/if}}
        >
            ${Text}
        </a>
    </script>

    <script id="<%= ClientID %>_ChildrenTemplate" type="text/x-jquery-tmpl">
        {{if Items != null || ChildrenNotLoaded}}
            <ul data-ektron-id="${Id}_childContainer" >
                {{if Items != null}}
                    {{tmpl(Items) "#<%= ClientID %>_ItemTemplate"}}
                {{/if}}
            </ul>
        {{/if}}
    </script>

    <script id="<%= ClientID %>_CustomValuesTemplate" type="text/x-jquery-tmpl">
        {{if Custom != null }} 
            {{each(i, c) Custom}}
                data-ektron-custom-${i}="${c}" 
            {{/each}}
        {{/if}}
    </script>
</div>