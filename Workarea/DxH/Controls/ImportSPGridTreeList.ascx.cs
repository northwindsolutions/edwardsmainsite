﻿namespace Ektron.Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.DxH;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Settings.DxH;
    using Ektron.DxH.Client;
    using Ektron.DxH.Client.Sharepoint;
    using Ektron.DxH.Common;
    using Ektron.DxH.Common.Connectors;
    using Ektron.DxH.Common.Objects;
    using Ektron.Workarea.DxH;
    using Ektron.Cms.Framework.Context;
    using Ektron.Newtonsoft.Json;
using Ektron.DxH.Tasks;
    using System.ComponentModel;
    using Ektron.Cms.Framework.UI.Tree;

    [Serializable]
    public class DataListObject
    {
        public string Name { get; set; }
        public string List { get; set; }
        public string Id { get; set; }
        public string ObjectDefinitionId { get; set; }
        public string IdHash { get; set; }
    }

    public class ImportSPGridTreeListErrorEventArgs : EventArgs
    {
        public Exception ex { get; set; }
    }
    
    public partial class ImportSPGridTreeList : System.Web.UI.UserControl
    {
        public event EventHandler<ImportSPGridTreeListErrorEventArgs> OnError;

        public Ektron.DxH.Client.Sharepoint.SharepointClient _sharepointClient;
        public ContextBusClient _contextBusClient;
        public bool isChild
        {
            get 
            {
                var rtnVal = ViewState["IsChildFolder"];
                if(rtnVal == null)
                {
                    return false;
                }

                return (bool)rtnVal;
            }
            set
            {
                ViewState["IsChildFolder"] = value;
            }
        }
        private const char DEFINITION_CHILD_DELIMITER = '\u0007';
        private string dxhPath;
        private string selectedConnectionName;

        protected bool SelectedItemsGrid_SelectItemField_IsRemoveButtonVisible { get; set; }

        
       

        JavaScriptSerializer _jsonSerializer;
        private JavaScriptSerializer jsonSerializer
        {
            get
            {
                if (_jsonSerializer == null)
                    _jsonSerializer = new JavaScriptSerializer();
                return _jsonSerializer;
            }
        }

        protected bool SelectItemsGrid_SelectItemField_IsCheckboxVisible { get; set; }


        public bool ItemsHaveBeenSelected 
        {
            get
            {
                bool? selections = ViewState["ItemsHaveBeenSelected"] as bool?;
                if (selections == null)
                {
                    return false;
                }
                else
                {
                    return (bool)selections;
                }
            }
            set
            {
                ViewState["ItemsHaveBeenSelected"] = value;
            }
        }

        private string AdapterName
        {
            get { return "SharePoint"; }
        }
        public Ektron.DxH.Client.Sharepoint.SharepointClient SharepointClient
        {
            get
            {
                if (this._sharepointClient == null)
                {
                    this._sharepointClient = new SharepointClient();
                    this._contextBusClient = new Ektron.DxH.Client.ContextBusClient();

                    // login:
                    this._contextBusClient.Login(this.SelectedConnectionName, "SharePoint");
                    _sharepointClient.Login(this.SelectedConnectionName);
                }
                return this._sharepointClient;
            }
        }
        private string DxhPath
        {
            get
            {
                if (this.dxhPath == null)
                {
                    this.dxhPath = CmsContextService.Current.WorkareaPath + "/DxH";
                }
                return this.dxhPath;
            }
        }

        protected string SelectItemsGridHeader
        {

            get
            {
                return !string.IsNullOrEmpty(this.SelectedFolderList) ? string.Format(GetLocalResourceObject("SelectItemsGridHeader").ToString(), this.SelectedFolderList, this.SelectedConnectionName) : "Items";
            }
        }

        protected string SelectItemsGridHeaderText
        {
            get
            {
                return string.Format(GetLocalResourceObject("ltrTitleDesc").ToString(), this.SelectedConnectionName);
            }
        }
        
        public string SelectItemsGridIdHash
        {
            get
            {
                if (ViewState["SelectItemsGridIdHash"] != null)
                {
                    return (string)ViewState["SelectItemsGridIdHash"];
                }
                return String.Empty;
            }
            set
            {
                ViewState["SelectItemsGridIdHash"] = value;
            }
        }
        public string SelectItemsGridId
        {
            get
            {
                if (ViewState["SelectItemsGridId"] != null)
                {
                    return (string)ViewState["SelectItemsGridId"];
                }
                return String.Empty;
            }
            set
            {
                ViewState["SelectItemsGridId"] = value;
            }
        }
        protected string SelectedList
        {
            get
            {
                if (ViewState["SelectedList"] != null)
                {
                    return (string)ViewState["SelectedList"];
                }
                return String.Empty;
            }
            set
            {
                ViewState["SelectedList"] = value;
            }
        }
        protected string SelectedOjectDefinitionId
        {
            get
            {
                if (ViewState["SelectedOjectDefinitionId"] != null)
                {
                    return (string)ViewState["SelectedOjectDefinitionId"];
                }
                return String.Empty;
            }
            set
            {
                ViewState["SelectedOjectDefinitionId"] = value;
            }
        }
        public int RecordsPerPage
        {
            get
            {
                return 10;
            }
        }
        public int SelectItemsGridCurrentPage
        {
            get
            {
                if (ViewState["uxSelectItemsGridCurrentPage"] != null)
                {
                    return (int)ViewState["uxSelectItemsGridCurrentPage"];
                }
                return 1;
            }
            set
            {
                ViewState["uxSelectItemsGridCurrentPage"] = value;
            }
        }
        
        /// <summary>
        /// Value indicates whether or not the user has selected to import content by SP Lists or individual SP Definitions
        /// </summary>
        public string ByListOrItem
        {
            get
            {
                return (Page.FindControl("ItemOrList") as HiddenField != null ? (Page.FindControl("ItemOrList") as HiddenField).Value : string.Empty);
            }
            
        }


        public bool ByList()
        {
            return (ByListOrItem.ToLower().Trim() == "bylist" ? true : false);
        }

        public List<GridViewDirtyField> SelectListDataDirtyFields
        {
            get
            {
                var selectListDataDirtyFields = Page.FindControl("uxSelectListGridDataDirtyFields") as HiddenField;

                return selectListDataDirtyFields != null ?
                    JsonConvert.DeserializeObject<List<GridViewDirtyField>>(selectListDataDirtyFields.Value) : null;
            }
            set {
                var selectListDataDirtyFields = Page.FindControl("uxSelectListGridDataDirtyFields") as HiddenField;
                if (selectListDataDirtyFields == null)
                    throw new NullReferenceException("Select List Grid Data Dirty Fields, which are used to remember user list selection.");
                selectListDataDirtyFields.Value = JsonConvert.SerializeObject(value);
            }
        }

        public List<GridViewDirtyField> SelectItemsGridDirtyFields
        {
            get
            {
                var selectItemsGridDirtyFields = Page.FindControl("uxSelectItemsGridDataDirtyFields") as HiddenField;

                return selectItemsGridDirtyFields != null ?
                    JsonConvert.DeserializeObject<List<GridViewDirtyField>>(selectItemsGridDirtyFields.Value) : null;
            }
            set
            {
                var selectItemsGridDirtyFields = Page.FindControl("uxSelectItemsGridDataDirtyFields") as HiddenField;
                if (selectItemsGridDirtyFields == null)
                    throw new NullReferenceException("Select Items Grid Data Dirty Fields, which are used to remember user item selection.");
                selectItemsGridDirtyFields.Value = JsonConvert.SerializeObject(value);
            }
        }

       

        public List<DataListObject> SelectedItemsGridData 
        {

            get {

                var selectedItemsGridData = Page.FindControl("uxSelectedItemsGridData") as HiddenField;


                return selectedItemsGridData != null ?
                            JsonConvert.DeserializeObject<List<DataListObject>>(selectedItemsGridData.Value) :
                                null;
            }
            set
            {
                var selectedItemsGridData = Page.FindControl("uxSelectedItemsGridData") as HiddenField;
                if (selectedItemsGridData == null)
                    throw new NullReferenceException("Selected Items Grid Data");
                selectedItemsGridData.Value = JsonConvert.SerializeObject(value);
            }
        }
        protected string SelectListGridHeaderText
        {
            get
            {
                return string.Format(GetLocalResourceObject("ltrListTitle").ToString(), this.SelectedConnectionName);
            }
        }
        public string SelectedConnectionName
        {
            get
            {
                if (string.IsNullOrEmpty(selectedConnectionName))
                {
                    selectedConnectionName = !String.IsNullOrEmpty((string)ViewState["ConnName"]) ? (string)ViewState["ConnName"] : String.Empty;
                    if (string.IsNullOrEmpty(selectedConnectionName))
                    {
                        var hiddenConnectionControl = Page.FindControl("uxConnectionName") as HiddenField;
                        if (hiddenConnectionControl != null)
                        {
                            selectedConnectionName = hiddenConnectionControl.Value;
                        }
                    }
                    
                }
                return selectedConnectionName;
            }
            set 
            {
                selectedConnectionName = value;
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        private void RaiseOnError(ImportSPGridTreeListErrorEventArgs e)
        {
            if (this.OnError != null)
            {
                this.OnError(this, e);
            }
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnDataBinding(EventArgs e)
        {
            if (string.IsNullOrEmpty(this.ByListOrItem) || this.ByListOrItem.ToLower().Trim() == "bylist")
            {
                DataBindSelectListGrid();
                aspItemListTreeWrapper.Visible = false;
                aspItemListGridWrapper.CssClass = ItemListGridWrapperCssClass();
                uxSelectListGrid.Visible = true;        

                
            }
            else
            {
                DataBindFolderTree(null, null);
                DataBindSelectItemsGrid(true);
            }
          
            base.OnDataBinding(e);
        }

   

        private void OnTreeCallback(object sender, TreeAjaxCallbackEventArgs e)
        {
            try
            {
                e.AbortDefaultProcessing = true; // we will create the tree-nodes locally

                string topFolderId = null, childId = null, rawId = e.EventArgument;

                string connectionNameIdentifier = "ConnectionName:";
                string loadChildren = "loadChildren:";
                string levelDelimiter = "_level_";
                string connDelimiter = "_ConDelim_";

                if (!rawId.Contains(connectionNameIdentifier))
                    throw new ArgumentException("Connection Name was not present on the tree call back.");

                string connectionName = rawId.Substring(rawId.IndexOf(connectionNameIdentifier) + connectionNameIdentifier.Length, rawId.IndexOf(connDelimiter) - connectionNameIdentifier.Length);


                if (rawId.Contains(loadChildren)) { rawId = rawId.Substring(rawId.IndexOf(loadChildren) + loadChildren.Length); }

                if (rawId.Contains(levelDelimiter)) { rawId = rawId.Substring(0, rawId.IndexOf(levelDelimiter)); }

                var IDs = rawId.Split(DEFINITION_CHILD_DELIMITER);
                if (IDs.Length >= 1) { topFolderId = DecodeDelimiters(IDs[0] ?? ""); }
                if (IDs.Length >= 2)
                {
                    childId = (IDs[1] ?? "").Split('_')[0];
                    childId = DecodeDelimiters(childId);
                }

                this.SelectedConnectionName = connectionName;
                var objDef = this.SharepointClient.GetObjectDefinition(topFolderId, connectionName);
                if (objDef == null) { return; }

                var folders = (String.IsNullOrEmpty(childId))
                    ? this.SharepointClient.GetListFolders(objDef, connectionName)
                    : this.SharepointClient.GetSubFolders(objDef, childId, connectionName);

                var nodes = new Ektron.Cms.Framework.UI.Tree.TreeNodeCollection();
                if (folders != null && folders.Results.Count > 0)
                {
                    folders.Results.ForEach(delegate(ObjectInstance objectInstance)
                    {
                        var sharepointNodeInfo = GetSharepointNodeInfo(objectInstance);
                        if (IsListObject(objDef.Id))
                        {
                            sharepointNodeInfo.FileUrl = sharepointNodeInfo.FileDirRef + "/" + sharepointNodeInfo.Title ?? sharepointNodeInfo.Name;
                        }
                        sharepointNodeInfo.Name = (sharepointNodeInfo.Title ?? sharepointNodeInfo.Name) ?? "(no title)";
                        var node = MakeTreeNode(sharepointNodeInfo, objDef.Id, objDef.DisplayName, connectionName + "_" + this.AdapterName);
                        nodes.Add(node);
                    });
                }
                uxTree.Items = nodes;
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ImportSPGridTreeListErrorEventArgs() { ex = ex });
            }
        }
        
        public void DataBindAll()
        {
            if (string.IsNullOrEmpty(this.ByListOrItem) || this.ByListOrItem.ToLower().Trim() == "bylist")
            {
                DataBindSelectedItemsGrid();
                DataBindSelectListGrid();
            }
            else
            {
                DataBindFolderTree(null, null);
                DataBindSelectedItemsGrid();
                DataBindSelectItemsGrid();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            uxTree.TemplatePath = this.DxhPath + "/ClientRender/FolderTreeTemplate.ascx";
            uxTree.Callback += OnTreeCallback;
            ViewState["ConnName"] = Request.QueryString["ConnName"];
            if (string.IsNullOrEmpty(this.ByListOrItem) || this.ByListOrItem.ToLower().Trim() == "bylist")
            {
                uxSelectByListField.CheckBoxFieldColumnStyle.Visible = true;
                uxSelectByItemField.RadioButtonFieldColumnStyle.Visible = false;
                this.uxTree.Visible = false;
                this.uxTreeHeader.Visible = false;
                this.uxTreeOverlayBindings.Visible = false;

				this.ltrAddSharePointContentMsg.Text = string.Format(GetLocalResourceObject("ltrAddSharePointContentMsgByList").ToString(), this.SelectedConnectionName ?? string.Empty);
                
                
            }
            else
            {
                uxSelectByItemField.RadioButtonFieldColumnStyle.Visible = true;
                uxSelectByListField.CheckBoxFieldColumnStyle.Visible = false;
                this.uxTreeHeader.Visible = true;
                this.uxTreeOverlayBindings.Visible = true;
                uxSelectListGrid.Visible = true;
				this.ltrAddSharePointContentMsg.Text = GetLocalResourceObject("ltrAddSharePointContentMsgByItem").ToString();
            }





            base.OnLoad(e);
        }  

        private SharepointNodeInfo GetSharepointNodeInfo(ObjectInstance objectInstance)
        {
            var nodeInfo = new SharepointNodeInfo();
            if (objectInstance == null) { return nodeInfo; }
            objectInstance.Fields.ForEach(delegate(Field field)
            {
                if (field.DisplayName == "ID")
                {
                    nodeInfo.Id = field.Value.ToString();
                }
                if (field.Id == "FileLeafRef")
                {
                    nodeInfo.Name = field.Value.ToString();
                }
                if (field.Id == "Title" && field.Value != null)
                {
                    nodeInfo.Title = field.Value.ToString();
                }
                if (field.Id == "FileDirRef")
                {
                    nodeInfo.FileDirRef = field.Value.ToString();
                }
                if (field.Id == "FileUrl")
                {
                    nodeInfo.FileUrl = field.Value.ToString();
                }
                if (field.Id == "FolderChildCount")
                {
                    nodeInfo.FolderChildCount = Convert.ToInt16(field.Value);
                }
            });
            return nodeInfo;
        }
        protected void uxSelectListGrid_uxSelectByListField_OnCheckedChanged(object sender, GridViewCheckedChangedEventArgs e)
        {
            DataBindSelectedItemsGrid(true);
            DataBindSelectListGrid();
            //uxAddSPContent.Enabled = e.DirtyFields.FindAll(c => c.DirtyState == "checked").ToList<GridViewDirtyField>().Count > 0 ? true : false;
        }
        protected void uxSelectListGrid_uxSelectByItemField_SelectedIndexChanged(object sender, GridViewSelectedIndexChangedEventArgs e)
        {
            this.SelectItemsGridId = e.DirtyField.ValueFields["Id"];
            this.SelectedList = e.DirtyField.ValueFields["List"];
            this.SelectedOjectDefinitionId = e.DirtyField.ValueFields["ObjectDefinitionId"];
            DataBindSelectListGrid();
            DataBindSelectItemsGrid(true);
            DataBindSelectedItemsGrid(true);
            
        }
        public void DataBindSelectListGrid()
        {
            List<DataListObject> selectListGridData = ViewState["SelectListGridData"] as List<DataListObject>;
            Ektron.Cms.PagingInfo selectListGridPagingInfo = ViewState["SelectListGridPagingInfo"] as Ektron.Cms.PagingInfo;

            

            if (selectListGridData == null || selectListGridPagingInfo == null)
            {
                //set grid data object and save to viewstate
                List<FlyweightObjectDefinition> flyweights = this.SharepointClient.GetObjectDefinitionNameList(this.SelectedConnectionName);
                selectListGridData = new List<DataListObject>();
                flyweights.ForEach(delegate(FlyweightObjectDefinition flyweightobjectDefinition)
                {
                    DataListObject item = new DataListObject();
                    item.Id = flyweightobjectDefinition.Id;
                    item.IdHash = flyweightobjectDefinition.Id.GetHashCode() + "-" + flyweightobjectDefinition.DisplayName.GetHashCode();
                    item.List = flyweightobjectDefinition.DisplayName;
                    item.Name = String.Empty;
                    item.ObjectDefinitionId = flyweightobjectDefinition.Id;
                    selectListGridData.Add(item);
                });

                if (this.SelectListDataDirtyFields != null)
                {
                    //clean up
                    uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.Clear();
                    uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.AddRange(this.SelectListDataDirtyFields);
                    uxSelectListGrid.EktronUIColumns["uxSelectByItemField"].DirtyFields.Clear();
                    uxSelectListGrid.EktronUIColumns["uxSelectByItemField"].DirtyFields.AddRange(this.SelectListDataDirtyFields);
                }

                ViewState["SelectListGridData"] = selectListGridData;
                selectListGridPagingInfo = new Ektron.Cms.PagingInfo();
                selectListGridPagingInfo.CurrentPage = 1;
                selectListGridPagingInfo.TotalPages = flyweights.Count;
                selectListGridPagingInfo.TotalRecords = flyweights.Count;
                selectListGridPagingInfo.RecordsPerPage = 100000;
                ViewState["SelectListGridPagingInfo"] = selectListGridPagingInfo;
                uxSelectListGrid.EktronUIPagingInfo = selectListGridPagingInfo;

                

                uxSelectListGrid.DataSource = selectListGridData;
                uxSelectListGrid.DataBind();
            }
            else
            {
                if (this.SelectListDataDirtyFields != null)
                {
                    //clean up
                    uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.Clear();
                    uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.AddRange(this.SelectListDataDirtyFields);
                    uxSelectListGrid.EktronUIColumns["uxSelectByItemField"].DirtyFields.Clear();
                    uxSelectListGrid.EktronUIColumns["uxSelectByItemField"].DirtyFields.AddRange(this.SelectListDataDirtyFields);
                }
                uxSelectListGrid.DataSource = selectListGridData;
                uxSelectListGrid.DataBind();
            }


        }

        public void Clear()
        {
            uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.Clear();
            uxSelectListGrid.EktronUIColumns["uxSelectByItemField"].DirtyFields.Clear();
            ViewState["SelectedItemsGridData"] = null;
            uxSelectItemsGrid.Controls.Clear();
            uxSelectedItemsGrid.Controls.Clear();
        }

        public void DataBindSelectedItemsGrid(bool forceBind = false)
        {
            //page level information on the ImportSharepointContent page.

            try
            {
                List<DataListObject> selectedItemsGridData = ViewState["SelectedItemsGridData"] as List<DataListObject> != null ? ViewState["SelectedItemsGridData"] as List<DataListObject> :
                    this.SelectedItemsGridData != null ?
                       this.SelectedItemsGridData : null;

                if (selectedItemsGridData == null || forceBind)
                {
                    selectedItemsGridData = new List<DataListObject>();
                    Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField selectedItems = null;

                    switch (this.ByListOrItem.ToLower().Trim())
                    {
                        case "bylist":
                            selectedItems = uxSelectListGrid.EktronUIColumns["uxSelectByListField"] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                            selectedItems.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                            {
                                if (dirtyField.DirtyState == "checked")
                                {
                                    DataListObject checkedItem = new DataListObject();
                                    checkedItem.Id = dirtyField.ValueFields["Id"].ToString();
                                    checkedItem.List = dirtyField.ValueFields["List"];
                                    checkedItem.Name = GetLocalResourceObject("AllItemsAndDocuments").ToString();
                                    checkedItem.ObjectDefinitionId = dirtyField.ValueFields["ObjectDefinitionId"];
                                    checkedItem.IdHash = dirtyField.KeyFields["IdHash"];
                                    selectedItemsGridData.Add(checkedItem);
                                }
                            });
                            this.SelectListDataDirtyFields = selectedItems.DirtyFields;
                            break;

                        case "byitem":
                            selectedItems = uxSelectItemsGrid.EktronUIColumns["uxSelectItemField"] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                            selectedItems.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                            {
                                if (dirtyField.DirtyState == "checked")
                                {
                                    DataListObject checkedItem = new DataListObject();
                                    checkedItem.Id = dirtyField.ValueFields["Id"].ToString();
                                    checkedItem.List = dirtyField.ValueFields["List"];
                                    checkedItem.Name = dirtyField.ValueFields["Name"];
                                    checkedItem.ObjectDefinitionId = dirtyField.ValueFields["ObjectDefinitionId"];
                                    checkedItem.IdHash = dirtyField.KeyFields["IdHash"];
                                    selectedItemsGridData.Add(checkedItem);
                                }
                            });
                            this.SelectItemsGridDirtyFields = selectedItems.DirtyFields;
                            break;
                    }
                }

                if (selectedItemsGridData.Count == 0)
                {
                    //uxAddSPContent.Enabled = false;
                    this.SelectedItemsGrid_SelectItemField_IsRemoveButtonVisible = false;
                    DataListObject item = new DataListObject();
                    item.Id = String.Empty;
                    item.IdHash = String.Empty;
                    item.List = String.Empty;
                    item.Name = GetLocalResourceObject("NoItemsDocsSelectedForImport").ToString();
                    item.ObjectDefinitionId = String.Empty;
                    selectedItemsGridData.Add(item);
                    this.ItemsHaveBeenSelected = false;
                }
                else if ((selectedItemsGridData.Count == 1) & (selectedItemsGridData[0].Name.Equals(GetLocalResourceObject("NoItemsDocsSelectedForImport").ToString())))
                {
                    this.ItemsHaveBeenSelected = false;
                }

                else
                {
                    this.SelectedItemsGrid_SelectItemField_IsRemoveButtonVisible = true;
                    this.ItemsHaveBeenSelected = true;
                }

                this.SelectedItemsGridData = selectedItemsGridData;

                ViewState["SelectedItemsGridData"] = selectedItemsGridData;

                PagingInfo selectedItemsGridPagingInfo = new Ektron.Cms.PagingInfo();
                selectedItemsGridPagingInfo.CurrentPage = 1;
                selectedItemsGridPagingInfo.TotalPages = 1;
                selectedItemsGridPagingInfo.TotalRecords = selectedItemsGridData.Count;
                selectedItemsGridPagingInfo.RecordsPerPage = selectedItemsGridData.Count;

                uxSelectedItemsGrid.EktronUIPagingInfo = selectedItemsGridPagingInfo;

                if (!ByList())
                {
                    uxSelectedItemsGrid.DataSource = selectedItemsGridData;
                    uxSelectedItemsGrid.DataBind();
                }
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ImportSPGridTreeListErrorEventArgs() { ex = ex });
            }
        }
        public void DataBindSelectItemsGrid(bool forceBind = false)
        {
            try
            {
                List<DataListObject> selectItemsGridData = ViewState["SelectItemsGridData"] as List<DataListObject>;
                Ektron.Cms.PagingInfo selectItemsGridPagingInfo = ViewState["SelectItemsGridPagingInfo"] as Ektron.Cms.PagingInfo;


                if (selectItemsGridData == null || selectItemsGridPagingInfo == null || forceBind)
                {

                    ObjectDefinition selection = null;
                    if (String.IsNullOrEmpty(this.SelectedOjectDefinitionId))
                    {
                        if (SelectedFolderNode != null)
                            this.SelectedOjectDefinitionId = SelectedFolderNode;
                        else
                        {

                            this.SelectedOjectDefinitionId = this.SharepointClient.GetObjectDefinitionNameList(this.SelectedConnectionName).FirstOrDefault().Id;
                            ViewState["firstObjDef"] = this.SelectedOjectDefinitionId;

                        }
                    }

                    selection = this.SharepointClient.GetObjectDefinition(this.SelectedOjectDefinitionId, this.SelectedConnectionName);

                    Ektron.DxH.Common.Objects.PagingInformation dxhPagingInfo = new PagingInformation();

                    if (selectItemsGridPagingInfo == null)
                    {
                        Ektron.Cms.PagingInfo pagingInfo = new PagingInfo();
                        pagingInfo = ViewState["SelectListGridPagingInfo"] as Ektron.Cms.PagingInfo;
                        dxhPagingInfo.RecordsPerPage = this.RecordsPerPage;
                        if (pagingInfo != null)
                            dxhPagingInfo.CurrentPage = pagingInfo.CurrentPage;
                        else
                            dxhPagingInfo.CurrentPage = 0;
                    }
                    else
                    {
                        dxhPagingInfo.RecordsPerPage = selectItemsGridPagingInfo.RecordsPerPage;
                        dxhPagingInfo.CurrentPage = this.SelectItemsGridCurrentPage;
                    }

                    ObjectInstanceList Instances = null;

                    if (!this.isChild)
                        Instances = this.SharepointClient.GetListFolderItems(selection, dxhPagingInfo, SelectedConnectionName);
                    else
                        Instances = this.SharepointClient.GetSubFoldersItems(selection, dxhPagingInfo, this.TreeCurrentNode.ParentId.Split('|').LastOrDefault().ToString(), SelectedConnectionName);

                    selectItemsGridData = new List<DataListObject>();

                    if (Instances.Results.Count > 0)
                    {
                        Instances.Results.ForEach(delegate(ObjectInstance objectInstance)
                        {
                            string fieldId = String.Empty;
                            string name = String.Empty;
                            objectInstance.Fields.ForEach(delegate(Field field)
                            {
                                if (field.DisplayName.ToLower() == "id")
                                {
                                    fieldId = selection.DisplayName.GetHashCode() + "-" + field.Value.ToString();
                                }
                                if (field.DisplayName.ToLower() == "name")
                                {
                                    name = field.Value.ToString();
                                }
                                if (field.DisplayName.ToLower() == "title" && String.IsNullOrEmpty(name))
                                {
                                    if (field.Value != null)
                                    {
                                        name = field.Value.ToString();
                                    }
                                    if (String.IsNullOrEmpty(name))
                                    {
                                        name = "(no title)";
                                    }
                                }
                            });
                            DataListObject item = new DataListObject();
                            item.Id = fieldId;
                            item.IdHash = fieldId + "-" + this.SelectedList.GetHashCode();
                            item.Name = String.IsNullOrEmpty(name) ? "(no title)" : name;
                            item.List = this.SelectedList;
                            item.ObjectDefinitionId = this.SelectedOjectDefinitionId;
                            if (String.IsNullOrEmpty(item.List))
                            {
                                item.List = item.ObjectDefinitionId.Split('|').LastOrDefault();
                            }
                            selectItemsGridData.Add(item);
                        });

                    }
                    else
                    {
                        this.SelectItemsGrid_SelectItemField_IsCheckboxVisible = false;
                        DataListObject item = new DataListObject();
                        item.Id = String.Empty;
                        item.IdHash = String.Empty;
                        item.List = String.Empty;
                        item.Name = GetLocalResourceObject("NoContentAvailable").ToString();
                        item.ObjectDefinitionId = String.Empty;
                        selectItemsGridData.Add(item);
                    }
                    ViewState["SelectItemsGridData"] = selectItemsGridData;



                    //set grid paging info and save to viewstate
                    selectItemsGridPagingInfo = new Ektron.Cms.PagingInfo();
                    selectItemsGridPagingInfo.CurrentPage = this.SelectItemsGridCurrentPage;
                    selectItemsGridPagingInfo.TotalPages = Instances.Paging.TotalPages;
                    selectItemsGridPagingInfo.TotalRecords = Instances.Paging.TotalRecords;
                    selectItemsGridPagingInfo.RecordsPerPage = Instances.Paging.RecordsPerPage;
                    ViewState["SelectItemsGridPagingInfo"] = selectItemsGridPagingInfo;
                }

                if (selectItemsGridData.Count > 0 && selectItemsGridData.Any(item => !string.IsNullOrEmpty(item.Id)))
                {
                    this.SelectItemsGrid_SelectItemField_IsCheckboxVisible = true;
                }
                else
                {
                    this.SelectItemsGrid_SelectItemField_IsCheckboxVisible = false;
                }

                uxSelectItemsGrid.Visible = true;
                uxSelectItemsGrid.EktronUIPagingInfo = selectItemsGridPagingInfo;

                if (this.SelectItemsGridDirtyFields != null)
                {
                    uxSelectItemsGrid.EktronUIColumns["uxSelectItemField"].DirtyFields.Clear();
                    uxSelectItemsGrid.EktronUIColumns["uxSelectItemField"].DirtyFields.AddRange(this.SelectItemsGridDirtyFields);
                }

                uxSelectItemsGrid.DataSource = selectItemsGridData;
                uxSelectItemsGrid.DataBind();
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ImportSPGridTreeListErrorEventArgs() { ex = ex });
            }
        }
        protected string SelectedFolderNode
        {
            get { return ((this.Page.IsPostBack && ViewState != null) ? ViewState["SelectedFolderNode"] : null) as string; }
            set { if (ViewState != null) { ViewState["SelectedFolderNode"] = value; } }
        }

        protected string SelectedFolderList
        {
            get
            {
                if (!string.IsNullOrEmpty(this.SelectedFolderNode))
                {
                    return this.SelectedFolderNode.Split('|').LastOrDefault() ?? string.Empty;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        protected void uxSelectItemsGrid_uxSelectItemField_SelectedIndexChanged(object sender, GridViewCheckedChangedEventArgs e)
        {
            DataBindSelectedItemsGrid(true);
            DataBindSelectItemsGrid(false);
            if (!uxTree.Visible)
            {
                DataBindSelectListGrid();
            }
        }
       
        protected void SelectedItemsGrid_RemoveItem(object sender, CommandEventArgs e)
        {
            try
            {
                DataListObject itemToRemove = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<DataListObject>((string)e.CommandArgument) as DataListObject;
                if (itemToRemove != null)
                {
                    switch (this.ByListOrItem.ToLower().Trim())
                    {
                        // By List
                        case "bylist":
                            uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.ForEach(delegate(GridViewDirtyField existingDirtyField)
                            {
                                if (existingDirtyField.ValueFields["Id"] == itemToRemove.Id)
                                {
                                    uxSelectListGrid.EktronUIColumns["uxSelectByListField"].DirtyFields.Remove(existingDirtyField);
                                }
                            });
                            DataBindSelectedItemsGrid(true);
                            DataBindSelectListGrid();
                            break;
                        // By Item
                        case "byitem":
                            uxSelectItemsGrid.EktronUIColumns["uxSelectItemField"].DirtyFields.ForEach(delegate(GridViewDirtyField existingDirtyField)
                            {
                                if (existingDirtyField.ValueFields["Id"] == itemToRemove.Id)
                                {
                                    uxSelectItemsGrid.EktronUIColumns["uxSelectItemField"].DirtyFields.Remove(existingDirtyField);
                                }
                            });
                            DataBindSelectedItemsGrid(true);
                            DataBindSelectItemsGrid();
                            
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ImportSPGridTreeListErrorEventArgs() { ex = ex });
            }
        }


        public void OnSelectionHandler(Object sender, TreeSelectionEventArgs argument)
        {
            try
            {
                string currentNode = uxTree.CurrentNodeId;
                if (String.IsNullOrEmpty(currentNode)) { return; }

                var IDs = currentNode.Split(DEFINITION_CHILD_DELIMITER);
                string topFolderId = null;
                if (IDs.Length >= 1) { topFolderId = DecodeDelimiters(IDs[0] ?? ""); }
                if (!String.IsNullOrEmpty(topFolderId)) { SelectedFolderNode = topFolderId; }

                string childId = null;
                if (IDs.Length >= 2) { childId = DecodeDelimiters(IDs[1] ?? ""); }
                this.isChild = !String.IsNullOrEmpty(childId);

                if (isChild && String.IsNullOrEmpty(uxTree.CurrentNode.ParentId))
                {
                    uxTree.CurrentNode.ParentId = childId;
                }

                this.TreeCurrentNode = uxTree.CurrentNode;
                this.SelectedOjectDefinitionId = topFolderId;
                DataBindSelectItemsGrid(true);
                DataBindSelectedItemsGrid(true);
                this.SelectedOjectDefinitionId = string.Empty;
                uxSelectListGrid.Visible = false;
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ImportSPGridTreeListErrorEventArgs() { ex = ex });
            }
        }
        private class SharepointNodeInfo
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Title { get; set; }
            public string FileDirRef { get; set; }
            public string FileUrl { get; set; }
            public int? FolderChildCount { get; set; }
        }
        private bool IsDocumentObject(string id)
        {
            if (string.IsNullOrEmpty(id)) { return false; }
            var fragments = id.Split('|');
            if (fragments.Length >= 2)
            {
                return fragments[1].IndexOf("Document") == 0;
            }
            return false;
        }

        private bool IsListObject(string id)
        {
            if (string.IsNullOrEmpty(id)) { return false; }
            var fragments = id.Split('|');
            if (fragments.Length >= 2)
            {
                return fragments[1].IndexOf("List") == 0;
            }
            return false;
        }
        private string EncodeDelimiters(string text)
        {
            if (string.IsNullOrEmpty(text)) { return text; }
            return ((text.Replace('|', '\u0011')).Replace('^', '\u0012')).Replace('_', '\u0013');
        }

        private string DecodeDelimiters(string text)
        {
            if (string.IsNullOrEmpty(text)) { return text; }
            return ((text.Replace('\u0011', '|')).Replace('\u0012', '^')).Replace('\u0013', '_');
        }

        protected string ItemListGridWrapperCssClass()
        {
            string cssClass = String.Empty;
            cssClass = aspItemListTreeWrapper.Visible ? "itemList-select-grid select-grid-bumpRight" : "itemList-select-grid";
            return cssClass;
        }
        protected void uxSelectItemsGrid_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.SelectItemsGridCurrentPage = e.PagingInfo.CurrentPage;
                if (ByList())
                {
                    DataBindSelectListGrid();
                }
                DataBindSelectItemsGrid(true);
                DataBindSelectedItemsGrid();
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }
        protected string GetSelectedItemsGrid_RemoveItem_CommandArument(DataListObject item)
        {
            return this.jsonSerializer.Serialize(item);
        }
     
        
       
        private Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode MakeTreeNode(SharepointNodeInfo sharepointNodeInfo, string objectDefId, string objectDefName, string rootId)
        {
            var id = EncodeDelimiters(objectDefId + (string.IsNullOrEmpty(sharepointNodeInfo.FileUrl) ? null : DEFINITION_CHILD_DELIMITER + sharepointNodeInfo.FileUrl));
            var hasChildren = (sharepointNodeInfo.FolderChildCount != null) ? sharepointNodeInfo.FolderChildCount > 0 : true;
            return MakeTreeNode(id, sharepointNodeInfo.Name, 2, hasChildren, rootId);
        }
        private Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode MakeTreeNode(string id, string name, int level, bool hasChildren, string rootId)
        {
            var node = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode();

            node.Id = id;
            node.Text = name;
            node.RootId = rootId;
            node.Type = "folder";
            node.SubType = "";
            node.Level = level;
            node.SelectionMode = Cms.Framework.UI.Tree.TreeSelectionMode.SingleForAll;
            node.CanHaveChildren = true;
            node.HasChildren = hasChildren;
            node.Action = hasChildren ? "LoadChildren" : null;

            return node;
        }
        protected void SetErrorDialogParam()
        {
            /*****TODO*****
             * Fix below
             */
            string addSharePointContentTitle = string.Format(GetLocalResourceObject("uxAddSharePointContentDialog").ToString(), "Test");
            //string addSharePointContentTitle = string.Format(GetLocalResourceObject("uxAddSharePointContentDialog").ToString(), this.ctrlListOrItem.FolderData.Name.ToString());
            uxErrorDialog.Title = addSharePointContentTitle;
        }
        public void DataBindFolderTree(ObjectInstanceList objInstanceList, List<ObjectDefinition> objDefList)
        {
            try
            {
                var list = new Ektron.Cms.Framework.UI.Tree.TreeNodeCollection();
                var rootId = this.SelectedConnectionName + "_" + this.AdapterName;

                List<FlyweightObjectDefinition> flyweights = this.SharepointClient.GetObjectDefinitionNameList(SelectedConnectionName);
                foreach (var item in flyweights)
                {
                    var hasChildren = true; // hard-coded for now, need support from API to correct this
                    var folderItem = MakeTreeNode(item.Id, item.DisplayName, 0, hasChildren, rootId);
                    list.Add(folderItem);
                }

                uxTree.Visible = true;
                aspItemListTreeWrapper.Visible = true;
                uxTreeCallBackOverride.Visible = true;
                uxTreeHeader.InnerHtml = string.Format(GetLocalResourceObject("uxTreeHeader").ToString(), this.SelectedConnectionName);
                uxTreeHeader.Attributes.Add("title", string.Format(GetLocalResourceObject("uxTreeHeaderTitleAttr").ToString(), this.SelectedConnectionName));
                aspItemListGridWrapper.CssClass = ItemListGridWrapperCssClass();

                uxTree.DataSource = list;
                uxTree.DataBind();
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ImportSPGridTreeListErrorEventArgs() { ex = ex });
            }
        }





        public Cms.Framework.UI.Tree.ITreeNode TreeCurrentNode 
        {
            get
            {
                return ((ITreeNode)ViewState["TreeCurrentNode"]);
            }
            set
            {
                ViewState["TreeCurrentNode"] = value;
            }
        }
    }
}