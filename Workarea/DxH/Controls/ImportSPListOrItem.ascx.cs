﻿namespace Ektron.Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.DxH;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Settings.DxH;
    using Ektron.DxH.Client;
    using Ektron.DxH.Client.Sharepoint;
    using Ektron.DxH.Common;
    using Ektron.DxH.Common.Connectors;
    using Ektron.DxH.Common.Objects;
    using Ektron.Workarea.DxH;
    using System.Web;


    public partial class ImportSPListOrItem : System.Web.UI.UserControl
    {
        #region Member Variables


        private FolderData _folderData;
        public ContentAPI _contentApi;
        private EkMessageHelper messageHelper;
        //private int _sharePointListOrItem = 0;

        #endregion

        #region properties

        private long FolderId
        {
            get
            {
                return !(Request.QueryString["id"] == null) ? Convert.ToInt64(Request.QueryString["id"]) : 0;
            }
        }
        public ContentAPI contentAPI
        {
            get
            {
                if (this._contentApi == null)
                {
                    _contentApi = new ContentAPI();
                }
                return this._contentApi;
            }
        }



        public FolderData FolderData
        {
            get
            {
                if (this._folderData == null)
                {
                    this._folderData = this.contentAPI.GetFolderById(this.FolderId);
                }
                return _folderData;
            }
        }

        #endregion

        #region Events
        protected void aspSharePointContentOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            (Page.FindControl("ItemOrList") as HiddenField).Value = (sender as RadioButtonList).SelectedItem.Value;
            this.EnableOrDisableNextButton();
        }
        protected void radioDeleteContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            (Page.FindControl("DeleteContent") as HiddenField).Value = (sender as RadioButtonList).SelectedItem.Value;
            this.EnableOrDisableNextButton();
        }

        protected void selectlanguageContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            (Page.FindControl("LanguageID") as HiddenField).Value = (sender as DropDownList).SelectedItem.Value;
            this.EnableOrDisableNextButton();
        }

        #endregion
        protected override void OnLoad(EventArgs e)
        {
            (Page.FindControl("ItemOrList") as HiddenField).Value = this.aspSharePointContentOption.SelectedValue;
            (Page.FindControl("DeleteContent") as HiddenField).Value = this.radioDeleteContent.SelectedValue;
            (Page.FindControl("LanguageID") as HiddenField).Value = this.selectlanguageContent.SelectedValue;

            var uxConnectionName = Page.FindControl("uxConnectionName") as HiddenField;
            if (uxConnectionName == null)
                throw new NullReferenceException("Could not find the connection name hidden field");

            if (!IsPostBack)
            {
                Ektron.Cms.UserAPI _uapi = new UserAPI();
                Ektron.Cms.BusinessObjects.Localization.LocaleManager localemanger = new Cms.BusinessObjects.Localization.LocaleManager(_uapi.RequestInformationRef);
                Ektron.Cms.Common.Criteria<Ektron.Cms.Localization.LocaleProperty> criteria = new Ektron.Cms.Common.Criteria<Ektron.Cms.Localization.LocaleProperty>();
                criteria.AddFilter(Ektron.Cms.Localization.LocaleProperty.Enabled, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, true);
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByField = Cms.Localization.LocaleProperty.EnglishName;
                List<Ektron.Cms.Localization.LocaleData> localData = localemanger.GetList(criteria);
                if (localData != null)
                {
                    selectlanguageContent.DataSource = localData;
                    selectlanguageContent.DataTextField = "EnglishName";
                    selectlanguageContent.DataValueField = "Id";
                    selectlanguageContent.SelectedIndex = localData.FindIndex(p => p.Id == _uapi.RequestInformationRef.ContentLanguage);
                    selectlanguageContent.DataBind();
                }
            }

            this.ltrOptionQuestion.Text = string.Format(GetLocalResourceObject("ltrOptionQuestion").ToString(), uxConnectionName.Value);
            aspSharePointContentOption.SelectedIndexChanged += new EventHandler(aspSharePointContentOption_SelectedIndexChanged);
        }

        private void EnableOrDisableNextButton()
        {
            var nextButton = Page.FindControl("uxSharePointWizard$StepNavigationTemplateContainerID$uxNextButton") as Ektron.Cms.Framework.UI.Controls.EktronUI.Button;
            if (nextButton != null)
            {
                if (!string.IsNullOrEmpty(this.aspSharePointContentOption.SelectedValue) && !string.IsNullOrEmpty(this.radioDeleteContent.SelectedValue))
                    nextButton.Enabled = true;
                else
                    nextButton.Enabled = false;
            }
        }


    }
}