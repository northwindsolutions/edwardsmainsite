﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MappingGridView.ascx.cs"
    Inherits="UserControls_MappingGridView" %>
<ektronUI:GridView ID="uxgridview" runat="Server" AutoGenerateColumns="false" EktronUIEnabled="true">
    <Columns>
        <asp:TemplateField HeaderText="Folder Metadata">
            <ItemTemplate>
                <asp:Literal runat="server" ID="Type" Text='<%# Eval("Name") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <ektronUI:CheckBoxField ID="uxDeleteField">
            <CheckBoxFieldHeaderStyle HeaderText="SharePoint Metadata" />
            <CheckBoxFieldColumnStyle />
            <KeyFields>
                <ektronUI:KeyField DataField="Required" />
            </KeyFields>
        </ektronUI:CheckBoxField>
    </Columns>
</ektronUI:GridView>
<ektronUI:Message ID="uxWarning" runat="server" DisplayMode="Warning" Text="This Folder Requires Metadata. SharePoint Content will be imported as 'Checked In' and cannot be published until the required metadata is supplied."
    Visible="false">
</ektronUI:Message>
<asp:HiddenField ID="uxGridviewDirtyHidden" runat="server" Value="" />
