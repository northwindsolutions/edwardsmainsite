﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.Organization;

public partial class UserControls_MappingGridView : UserControl
{
    #region Properties

    public string FolderName { get; set; }
    public FolderManager folderManager = new FolderManager();

    private long FolderId
    {
        get
        {
            return !(Request.QueryString["id"] == null) ? Convert.ToInt64(Request.QueryString["id"]) : 0;
        }
    }

    #endregion

    #region protected Methods

    protected void Page_Load(object sender, EventArgs e)
    {
        var listMetadata = folderManager.GetAssignedMetadata(this.FolderId);

        //uxgridview.EktronUIPagingInfo = criteria.PagingInfo;
        uxgridview.EktronUIPagingInfo = new Ektron.Cms.PagingInfo();
        uxgridview.DataSource = listMetadata;
        uxgridview.DataBind();
    }

    /// <summary>
    /// Get The Current Folder Name.
    /// </summary>
    /// <returns></returns>
    private String GetFolderName()
    {
        return folderManager.GetItem(this.FolderId).Name + " Metadata";
    }

    #endregion
}