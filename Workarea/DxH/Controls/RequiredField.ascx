﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequiredField.ascx.cs" Inherits="Workarea.DxH.Controls.RequiredField" %>
<ektronUI:TextField runat="server" ID="tfRequired">
    <ValidationRules>
        <ektronUI:RequiredRule />
    </ValidationRules>
</ektronUI:TextField>