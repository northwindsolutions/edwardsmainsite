﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SharePointTreeTemplate.ascx.cs" Inherits="SharePointTreeTemplate" %>
<%--<div id="<%= ClientID %>" <%= CssManager %> >

    <asp:PlaceHolder ID="placeHolder" runat="server" />

    <ul id="<%= ClientID %>_TreeRootElement" class="ektron-ui-tree-root" ></ul>

    <input type="hidden" id="<%= ClientID %>_controlUniqueId" class="controlUniqueId" value="<%= ControlUniqueId %>" />

    <script id="<%= ClientID %>_ItemTemplate" type="text/x-jquery-tmpl">
        <li data-ektron-id="${Id}" data-ektron-parentid="${ParentId}" 
            data-ektron-rootid="${RootId}" data-ektron-path="${Path}" 
            data-ektron-nodetype="${Type}" data-ektron-nodesubtype="${SubType}" 
            data-ektron-depth="${Depth}" data-ektron-selectionmode="${SelectionModeString}"
            
            {{if CommandName != null && CommandName.length > 0}} data-ektron-CommandName="${CommandName}" data-ektron-CommandArgument="${CommandArgument}" {{/if}}
            {{if ClickCausesPostback}} data-ektron-ClickCausesPostback="${ClickCausesPostback}" {{/if}}

            {{tmpl "#<%= ClientID %>_CustomValuesTemplate"}}

            {{if CanHaveChildren }}
                {{if HasChildren || ChildrenNotLoaded }}
                    {{if Expanded }}
                        class="ektron-ui-tree-branch collapsible{{if Type}} ektron-ui-tree-type-${Type}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType}{{/if}}"
                    {{else}}
                        class="ektron-ui-tree-branch expandable{{if Type}} ektron-ui-tree-type-${Type}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType}{{/if}}"
                    {{/if}}
                {{else}}
                    class="ektron-ui-tree-branch{{if Type}} ektron-ui-tree-type-${Type}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType}{{/if}}"
                {{/if}}
            {{else}}
                class="ektron-ui-tree-leaf{{if Type}} ektron-ui-tree-type-${Type}{{/if}}{{if SubType}} ektron-ui-tree-subtype-${SubType}{{/if}}"
            {{/if}}
        >
            {{tmpl "#<%= ClientID %>_SelectionTemplate"}}

            <span class="ui-icon file ${Type}"> </span>

            {{if NavigateUrl }} 
                {{tmpl "#<%= ClientID %>_NavigateUrlTemplate"}}
            {{else}}
                {{if OnClientClick || Action }} 
                    <span 
                        {{if OnClientClick}} onclick="${OnClientClick}; return false;" {{/if}}
                        class="clickable" 
                        {{if Action }} data-ektron-action="${Action}" {{/if}}
                        data-ektron-id="${Id}" data-ektron-path="${Path}" data-ektron-parentid="${ParentId}" > ${Text} </span>
                {{else}}
                    ${Text}
                {{/if}}
            {{/if}}

            {{if HasChildren}}
                {{tmpl "#<%= ClientID %>_ChildrenTemplate"}}
            {{/if}}
        </li>
    </script>

    <script id="<%= ClientID %>_NavigateUrlTemplate" type="text/x-jquery-tmpl">
        <a href="${NavigateUrl}" data-ektron-action="${Action}" class="node-type-${Type}"
            data-ektron-id="${Id}" data-ektron-path="${Path}" data-ektron-parentid="${ParentId}" 
            {{if OnClientClick }} 
                onclick="${OnClientClick}; return false;" 
            {{/if}}
        >
            ${Text}
        </a>
    </script>

    <script id="<%= ClientID %>_ChildrenTemplate" type="text/x-jquery-tmpl">
        {{if Items != null || ChildrenNotLoaded}}
            <ul data-ektron-id="${Id}_childContainer" 
                {{if !Expanded }}
                    class="ektron-ui-tree-branch expandable"
                {{/if}}
            >
                {{if Items != null}}
                    {{tmpl(Items) "#<%= ClientID %>_ItemTemplate"}}
                {{/if}}
            </ul>
        {{/if}}
    </script>

    <script id="<%= ClientID %>_SelectionTemplate" type="text/x-jquery-tmpl">
        {{if (SelectionModeString == "MultiForAll") || (SelectionModeString == "MultiForItems" && Type == "item")}}
            <input type="checkbox" name="<%= ClientID + "_select_" %>${Id}" data-ektron-path="${Path}" value="select_${Path}" class="ektron-ui-tree-select" title="" data-ektron-selectionmode="${SelectionModeString}" 
                {{if Selected }}
                    checked="checked"
                {{/if}}
                />
        {{else}}
            {{if (SelectionModeString == "SingleForAll") || (SelectionModeString == "SingleForItems" && Type == "item")}}
                <input type="radio" name="<%= ClientID + "_select_" %>${Id}" data-ektron-path="${Path}" value="select_${Path}" class="ektron-ui-tree-select" title="" data-ektron-selectionmode="${SelectionModeString}" />
            {{/if}}
        {{/if}}
    </script>

    <script id="<%= ClientID %>_CustomValuesTemplate" type="text/x-jquery-tmpl">
        {{if Custom != null }} 
            {{each(i, c) Custom}}
                data-ektron-custom-${i}="${c}" 
            {{/each}}
        {{/if}}
    </script>

    <ektronUI:JavaScriptBlock ExecutionMode="OnEktronReady" ID="uxSharePointCheckBoxScript" runat="server">
        <ScriptTemplate>
            $ektron(".ektron-ui-tree-sharePoint .ektron-ui-tree-select").click(function(){
                var checkbox = $ektron(this);
                var childCheckboxes = checkbox.siblings("ul").find(".ektron-ui-tree-select");
                if (checkbox.prop("checked")){
                    childCheckboxes.prop("disabled", true).prop("checked", true);
                }
                else
                {
                    childCheckboxes.prop("disabled", false).prop("checked", false);
                }
            });
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
</div>--%>