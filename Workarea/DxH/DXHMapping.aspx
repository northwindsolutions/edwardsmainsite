﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DXHMapping.aspx.cs" Inherits="Workarea.DxH.DXHMapping" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$Resources: PageTitle%>" /></title>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:JavaScriptBlock ID="uxJavaScriptBlock" runat="server">
        <ScriptTemplate>
           $ektron(".ektronformfields").on("change", function()
            {
                updateRequiredMappingValue(this);
                $ektron(this).siblings("span").css("visibility", "hidden");	
                if ("" == this.value)
                {
                    $ektron(this).siblings("span").css("visibility", "visible");
                }
            });
      <%--      if (0 == $ektron(".optMappingRows:visible").length)
            {
                AddOptionalRow();
            }--%>

            $ektron("label.preInfieldLabel").each(function()
            {
                $ektron(this).inFieldLabels().addClass("inFieldLabel").removeClass("preInfieldLabel");
            });
            $ektron("input").attr("autocomplete","off");
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
        <script type="text/javascript">
            function updateRequiredMappingValue(elem) {
                var jsonObject = Ektron.JSON.parse($ektron("#hdnJsonFieldValues").val());
                for (var i = 0; i < jsonObject.length; i++) {
                    if (elem.id == jsonObject[i].Id) {
                        jsonObject[i].ValueName = elem.value;
                        jsonObject[i].ValueDisplayName = elem.options[elem.selectedIndex].text;
                        $ektron("#hdnJsonFieldValues").attr("value", Ektron.JSON.stringify(jsonObject)).prop("value", Ektron.JSON.stringify(jsonObject));
                        break;
                    }
                }
            }
            function updateOptionalMappingValue(elem) {
                var rowIndex = $ektron(elem).closest("tr").attr("id"),
					optFieldId = $ektron("#optDropDown_" + rowIndex).val(),
					mapFieldName = $ektron("#mapDropDown_" + rowIndex).val();
					mapFieldDisplayName = elem.options[elem.selectedIndex].text;
                if (optFieldId.length > 0) {
                	UpdateOptionalMappingValues(optFieldId, mapFieldName, mapFieldDisplayName);
                    var unique_values = {};
                    $(".optDropDown:visible[value!='']").each(function () {
                        if (!unique_values[$(this).val()]) {
                            unique_values[$(this).val()] = true;
                            var rowIndex1 = $ektron(this).closest("tr").attr("id");
                            $ektron("#optDropDown_" + rowIndex1).addClass("unique");
                            $ektron("#optDropDown_" + rowIndex1).siblings("span").addClass("ektron-ui-hidden");
                            $ektron("#mapDropDown_" + rowIndex1).removeAttr("disabled");
                        }
                        else {
                            $ektron("#optDropDown_" + rowIndex).removeClass("unique");
                            $ektron("#optDropDown_" + rowIndex).siblings("span").removeClass("ektron-ui-hidden");
                            $ektron("#mapDropDown_" + rowIndex).attr("disabled", "disabled");
                        }
                    });
                    
                    if ($ektron(elem).hasClass("optDropDown") || $ektron(elem).hasClass("optMappingRows")) {
                        $ektron(elem).closest("table").children("caption").hide();
                        if ($ektron("span.warning:visible").length > 0) {
                            $ektron(elem).closest("table").children("caption").show();
                        }
                    }
                }
            }
            function UpdateOptionalMappingValues(optFieldId, mapFieldName, mapFieldDisplayName) {
                var jsonObject = Ektron.JSON.parse($ektron("#hdnJsonOptFieldValues").val());
                // serialize the hidden json object
                for (var i = 0; i < jsonObject.length; i++) {
                	if (jsonObject[i].Id === optFieldId) {
                		jsonObject[i].ValueName = mapFieldName;
                		jsonObject[i].ValueDisplayName = mapFieldDisplayName;
                    	break;
                    }
                }
                $ektron("#hdnJsonOptFieldValues").attr("value", Ektron.JSON.stringify(jsonObject)).prop("value", Ektron.JSON.stringify(jsonObject));
                return true;
            }

            function AddOptionalRow() {
                var optCounter = $ektron(".optDropDown").length;
                var eNewRow = $ektron(".repeater").clone();
                if (eNewRow.length > 0) {
                    eNewRow.removeClass("ektron-ui-hidden").removeClass("repeater").addClass("optMappingRows").attr("id", optCounter);
                    eNewRow.find("[id='optDropDown_']").prop("id", "optDropDown_" + optCounter).attr("id", "optDropDown_" + optCounter);
                    eNewRow.find("[id='mapDropDown_']").prop("id", "mapDropDown_" + optCounter).attr("id", "mapDropDown_" + optCounter);
                    $ektron("#tblOptionalFields").append(eNewRow.get(0));
                    $ektron(".optDropDown").on("change", function () {
                        updateOptionalMappingValue(this);
                    });
                    $ektron(".mapDropDown").on("change", function () {
                        updateOptionalMappingValue(this);
                    });
                }
            }
            function DeleteOptionalRow(elem) {
                var rowIndex = $ektron(elem).closest("tr").attr("id");
                if ($ektron("tr.optMappingRows:visible").length > 1) {
                    var optFieldId = $ektron("#optDropDown_" + rowIndex).val();
                    if (optFieldId.length > 0) {
                        var jsonObject = Ektron.JSON.parse($ektron("#hdnJsonOptFieldValues").val());
                        for (var i = 0; i < jsonObject.length; i++) {
                            if (optFieldId == jsonObject[i].Id) {
                                jsonObject[i].ValueName = "";
                                jsonObject[i].ValueDisplayName = "";
                                $ektron("#hdnJsonOptFieldValues").attr("value", Ektron.JSON.stringify(jsonObject)).prop("value", Ektron.JSON.stringify(jsonObject));
                                break;
                            }
                        }
                    }
                    $ektron("tr#" + rowIndex).addClass("ektron-ui-hidden"); // made invisible instead of remove() to prevent duplicate tr#{index}.
                    $ektron("tr.optMappingRows:visible").each(function () {
                        updateOptionalMappingValue(this);
                    });
                }
            }

            function TestMappingClicked() {
                if (!$('#spanWaitTest').hasClass('TesTLinkdisabled')) {
                    $('#spanWaitTest').addClass('TesTLinkdisabled');
                    $ektron('#spanWaitTest').toggle();
                    window.location = '<asp:Literal runat="server" ID="ltrTestMappingURL" /> ';
                    return true;
                } else {
                    return false;
                }

            }

        </script>
<div>
    <div id="general">
        <asp:Image CssClass="pageLogo" AlternateText="<%$Resources: LogoText%>" ID="dxhLogo" runat="server" ImageUrl="images/dxh_logo_60x60.png" ToolTip="<%$Resources: LogoText%>" /><asp:Label ID="PageHeader" runat="server" CssClass="ektron-ui-text-xlarge pageHeader" Text="<%$ Resources: PageHeader %>"></asp:Label><br />
        <asp:Label ID="DxhDescription" Text="<%$Resources: MappingDescription%>" runat="server"></asp:Label>
    </div>
    <div>
    <asp:TextBox ID="hdnJsonFieldValues" runat="server" CssClass="ektron-ui-hidden hdnFieldValues" ></asp:TextBox>
    <asp:TextBox ID="hdnJsonOptFieldValues" runat="server" CssClass="ektron-ui-hidden hdnJsonOptFieldValues" ></asp:TextBox>

    <ektronUI:Message runat="server" ID="msgExceptionGeneral" Text="<%$Resources: strGeneralException%>" Visible="false" EnableViewState="false" />

    <asp:MultiView ID="pageViews" runat="server" ActiveViewIndex="0">
        <asp:View ID="vConnection" runat="server">
            <div class="msgRequired"><asp:Literal ID="msgRequired" runat="server" Text="<%$Resources: RequiredAsterisk %>"></asp:Literal>&#160;<span class="ektron-ui-required">*</span></div>
            <ektronUI:Message DisplayMode="Error" runat="server" ID="errConnectionMsg" Visible="false" EnableViewState="false" />
            <ektronUI:Message runat="server" ID="msgProgress" DisplayMode="Working" Visible="true" CssClass="connectionNameWorkingMessage ektron-ui-hidden" Text="<%$Resources: IdentifyingDXHObjectType %>" />
            <asp:Panel runat="server" id="pnlMapInfo">
                <table class="dataVTable" >
                <tbody>
                <tr>
                    <td colspan="2"><asp:Label ID="lblMappingInstruction" Text="<%$Resources: MappingInstruction%>" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <th class="label"><asp:Label ID="lblMappingName" runat="server" Text="<%$ Resources: MappingName %>" AssociatedControlID="txtMappingName"></asp:Label>&#160;<span class="ektron-ui-required">*</span></th>
                    <td>
                    <ektronUI:TextField ID="txtMappingName" runat="server">
                        <ValidationRules><ektronUI:RequiredRule /></ValidationRules>
                    </ektronUI:TextField>
                    </td>
                </tr> 
                <tr>
                    <th class="label"><asp:Label ID="lblConnectionName" runat="server" Text="<%$ Resources: DXHConnectionName %>" AssociatedControlID="ddlConnectionName"></asp:Label>&#160;<span class="ektron-ui-required">*</span></th>
                    <td>
                    <asp:DropDownList ID="ddlConnectionName" runat="server" DataTextField="ConnectionName" DataValueField="ConnectionName" Width="350px"
                        OnSelectedIndexChanged="ddlConnectionName_OnSelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true">
                        <asp:ListItem Text="<%$ Resources: SelectConnectionName  %>" Value="" />
                    </asp:DropDownList>
                    </td>
                </tr>
                <asp:Panel ID="liObjectType" runat="server" Visible="false">
                <tr>
                    <td colspan="2"><asp:Literal ID="lblObjectTypeInstruction" runat="server" Text="<%$Resources: ObjectTypeInstruction %>"></asp:Literal></td>
                </tr>
                <tr>
                    <th><asp:Label ID="lblObjectType" runat="server" Text="<%$ Resources: DXHObjectType %>" AssociatedControlID="ddlObjectType"></asp:Label>&#160;<span class="ektron-ui-required">*</span></th>
                    <td>
                    <asp:DropDownList ID="ddlObjectType" runat="server" DataTextField="DisplayName" DataValueField="Id" 
                        OnSelectedIndexChanged="ddlObjectType_OnSelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true">
                        <asp:ListItem Text="<%$ Resources: SelectObjectType %>" Value="" />
                    </asp:DropDownList>
                    </td>
                </tr>
                </asp:Panel>
                </tbody>
            </table>
            </asp:Panel>
        </asp:View>
        <asp:View ID="vRequiredFields" runat="server">
            <div class="msgRequired"><asp:Literal ID="msgRequired2" runat="server" Text="<%$Resources: RequiredAsterisk %>"></asp:Literal>&#160;<span class="ektron-ui-required">*</span></div>
            <ektronUI:Message DisplayMode="Error" runat="server" ID="errRequiredFldsMsg" Visible="false" EnableViewState="false" />
            <div><asp:Literal ID="lblRequiredFieldsInstruction" runat="server" Text="<%$Resources: RequiredFieldsInstruction %>"></asp:Literal></div>
            <asp:Table id="tblRequiredFields" runat="server" class="dataHTable" >
                <asp:TableHeaderRow CssClass="ektron-ui-even">
                    <asp:TableHeaderCell ID="lblRequiredFields" runat="server" Text="<%$ Resources: RequiredFields %>"></asp:TableHeaderCell>
                    <asp:TableHeaderCell ID="lblAvailableFields" runat="server" Text="<%$ Resources: AvailableFields %>"></asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </asp:View>
        <asp:View ID="vOptionalFields" runat="server">
            <ektronUI:Message DisplayMode="Error" runat="server" ID="errOptionalFldsMsg" Visible="false" EnableViewState="false" />
            <div class="instructionMsg">
                <p><asp:Literal ID="lblOptionalFieldsInstruction" runat="server" Text="<%$Resources: OptionalFieldsInstruction %>"></asp:Literal></p>
                <p><asp:Literal ID="lblOptionalFieldsNote" runat="server" Text="<%$Resources: OptionalFieldsNote %>"></asp:Literal></p>
            </div>
            <asp:Table id="tblOptionalFields" runat="server" class="dataHTable" Caption="<%$Resources: FieldMappedError %>">
                <asp:TableHeaderRow ID="rHeaderOptionalFields" runat="server">
                    <asp:TableCell ID="colDelete" runat="server" CssClass="colNarrow"></asp:TableCell>
                    <asp:TableHeaderCell ID="lblAvailableOptFields" runat="server" Text="<%$ Resources: AvailableFields %>" CssClass="ektron-ui-even"></asp:TableHeaderCell>
                    <asp:TableHeaderCell ID="lblOptionalFields" runat="server" Text="<%$ Resources: Available3rdPartyFields %>" CssClass="ektron-ui-even"></asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
            <div class="addOptionalRow"><a id="linkAddRow" runat="server" onclick="AddOptionalRow();" title="<%$Resources: AddAnotherOptionalField%>" ><img id="imgAddRow" runat="server" src="" alt="<%$Resources: AddAnotherOptionalField%>" />&#160;<asp:Literal ID="lblAddRow" runat="server" Text="<%$Resources: AddAnotherOptionalField%>" /></a></div>
        </asp:View>
        <asp:View ID="vCompleted" runat="server">
            <ektronUI:Message DisplayMode="Success" runat="server" ID="msgComplete" Visible="true" />
            <div><asp:Literal ID="lblCompleteMappingMsg" runat="server" Text="<%$Resources: CompletePageDescription %>"></asp:Literal></div>
            <div class="instructionMsg"><asp:Literal ID="lblCompleteValidationMsg" runat="server" ></asp:Literal></div>
        </asp:View>
    </asp:MultiView>
    <asp:Panel runat="server" ID="pnlButtonArea">
        <ektronUI:Button runat="server" ID="btnPrevious" ToolTip="<%$Resources:btnPrevious %>" Text="<%$Resources:btnPrevious %>" OnClick="btnPrevious_Click" Visible="false" CssClass="buttonLeft" />
        <span class="buttonRight">
        <ektronUI:Button runat="server" ID="btnTestMapping" Visible="false" Text="<%$Resources:btnTestMapping %>" DisplayMode="Anchor"  OnClientClick="TestMappingClicked();" ></ektronUI:Button><span id="spanWaitTest" style="display:none; color:Red;">Please wait...</span>
        <ektronUI:Button runat="server" ID="btnNext" ToolTip="<%$Resources:btnNext %>" Text="<%$Resources:btnNext %>" OnClick="btnNext_Click" Enabled="false" CssClass="buttonNext"/>
        <ektronUI:Button runat="server" ID="btnCancel" ToolTip="<%$Resources:btnCancel %>" Text="<%$Resources:btnCancel %>" OnClientClick="parent.closeDialog(); return false;" />
        </span>
    </asp:Panel>
    <asp:panel runat="server" ID="pnlOK" Visible="false">
        <ektronUI:Button runat="server" ID="btnOK" OnClientClick="parent.closeDialog(); return false;" Text="<% $Resources: btnOK %>" ToolTip="<% $Resources: btnOK %>" CssClass="buttonRight"></ektronUI:Button>
    </asp:panel>
    </div>

</div>
    </form>
</body>
</html>
