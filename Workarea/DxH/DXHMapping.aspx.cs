﻿namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Framework.Content;
    using Ektron.Cms.Framework.Settings.DxH;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Settings.DxH;
    using Ektron.DxH.Client;
    using Ektron.DxH.Common.Contracts;
    using Ektron.DxH.Common.Objects;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.DxH.Tasks;
    using System.Web;
    using Ektron.DxH.Common.Connectors;

    [Serializable]
    public class DXHMappingValue
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string FieldType { get; set; }
        public string ValueName { get; set; }
        public string ValueDisplayName { get; set; }
    }
    [Serializable]
    public class ConnectionAdapterTable
    {
        public string ConnectioinName { get; set; }
        public string Adapter { get; set; }
    }

    public partial class DXHMapping : Ektron.Cms.Workarea.Page
    {
        private long formId = 0;
        private int lang = (new Ektron.Cms.CommonApi()).ContentLanguage;
        private string adapterName = string.Empty;
        private string connectionName = string.Empty;
        private string objectType = string.Empty;
        private int ektronFieldCount = 0;
        private FormFieldList currentFieldLst;
        private DropDownList ddlFormFields;
        private List<FieldDefinition> optFields = new List<FieldDefinition>();
        private ObjectDefinition thirdPartyMeta;
        private ObjectDefinition ektronMeta;
        private ContextBusClient contextbus;
        private ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        private SiteAPI _ekSite = null;
        private ConnectionManagerClient connMgr;


        private List<DXHMappingValue> MappingValues
        {
            get { return ViewState["DxhMappingValues"] as List<DXHMappingValue>; }
            set { ViewState["DxhMappingValues"] = value; }
        }
        private List<DXHMappingValue> OptMappingValues
        {
            get { return ViewState["DxhOptMappingValues"] as List<DXHMappingValue>; }
            set { ViewState["DxhOptMappingValues"] = value; }
        }
        private List<ConnectionAdapterTable> ConnectionNames
        {
            get { return ViewState["ConnectionNames"] as List<ConnectionAdapterTable>; }
            set { ViewState["ConnectionNames"] = value; }
        }
        private string ThirdPartyMeta
        {
            get { return ViewState["ThirdPartyMeta"] as string; }
            set { ViewState["ThirdPartyMeta"] = value; }
        }
        private string EktronMeta
        {
            get { return ViewState["EktronMeta"] as string; }
            set { ViewState["EktronMeta"] = value; }
        }
        private string MappedData
        {
            get { return ViewState["MappedData"] as string; }
            set { ViewState["MappedData"] = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ddlFormFields = new DropDownList();
            contextbus = new ContextBusClient();
            connMgr = new ConnectionManagerClient();
            _ekSite = new SiteAPI();
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Package contextbusControlPackage = new Package()
            {
                Components = new List<Component>()
                {
                    Packages.Ektron.CssFrameworkBase,
                    Css.Create(cmsContextService.WorkareaPath + "/csslib/ektron.workarea.dxh.css"),
                    Packages.Ektron.JSON,
                    Packages.jQuery.Plugins.InfieldLabels
                }
            };

            //In JavaScript:
            //GET -- var jsonObject = $ektron("#myhiddenfieldId").val(Ektron.JSON.parse(string));
            //SET -- $ektron("#myhiddenfieldId").attr("value", Ektron.JSON.stringify(string)).prop("value", Ektron.JSON.stringify(string));
            contextbusControlPackage.Register(this);

            string reqFieldData = HttpContext.Current.Request.Form["hdnJsonFieldValues"];
            if (!String.IsNullOrEmpty(reqFieldData))
            {
                List<DXHMappingValue> clientDataObject = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<DXHMappingValue>>(reqFieldData);
                if (clientDataObject != null)
                {
                    this.MappingValues = clientDataObject;
                }
            }
            string optFieldData = HttpContext.Current.Request.Form["hdnJsonOptFieldValues"];
            if (!String.IsNullOrEmpty(optFieldData))
            {
                List<DXHMappingValue> clientDataObject = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<DXHMappingValue>>(optFieldData);
                if (clientDataObject != null)
                {
                    this.OptMappingValues = clientDataObject;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.ValidateUserLogin();

            if (!(Request.QueryString["formid"] == null))
            {
                formId = Convert.ToInt64(Request.QueryString["formid"]);
            }
            if (0 == formId)
            {
                errConnectionMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errConnectionMsg.Text = GetLocalResourceObject("errMsg").ToString();
                errConnectionMsg.Visible = true;
                btnNext.Enabled = false;
            }
            if (!(Request.QueryString["lang"] == null))
            {
                int contentLanguage = Convert.ToInt32(Request.QueryString["lang"]);
                if (contentLanguage > 0)
                {
                    lang = contentLanguage;
                }
            }

            if (!IsPostBack)
            {
                GetEktronMetadata();
            }

            DxHUtils dxhUtils = new DxHUtils();
            List<DxHConnectionData> connectionLst = dxhUtils.GetDxHConnectionList();
            connectionLst = (from con in connectionLst
                             where (con.AdapterName != "DxH" && con.AdapterName != "Ektron" && con.AdapterName != "SharePoint")
                             select con).ToList();

            if (connectionLst != null && connectionLst.Count > 0)
            {
                if (!IsPostBack)
                {
                    ddlConnectionName.DataSource = connectionLst;
                    ddlConnectionName.DataBind();
                    ddlConnectionName.Attributes.Add("onchange", "$ektron('#" + msgProgress.ClientID + "').show();");
                }

                ConnectionNames = new List<ConnectionAdapterTable>();
                foreach (DxHConnectionData c in connectionLst)
                {
                    ConnectionAdapterTable connection = new ConnectionAdapterTable();
                    connection.ConnectioinName = c.ConnectionName;
                    connection.Adapter = c.AdapterName;
                    ConnectionNames.Add(connection);
                }
            }
            else
            {
                errConnectionMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errConnectionMsg.Text = GetLocalResourceObject("errThirdPartyAdapterMsg").ToString();
                errConnectionMsg.Visible = true;
            }


            if (null == EktronMeta)
            {
                errConnectionMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errConnectionMsg.Text = GetLocalResourceObject("errEktronAdapterMsg").ToString();

                DxhDescription.Visible = false;
                pnlButtonArea.Visible = false;
                pnlOK.Visible = true;

                if (_ekSite.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.DxHConnectionAdmin))
                {
                    string addInBoundLink = "<a href='#' onclick='parent.window.location.href=\"" + _ekSite.AppPath + "DxH/ViewDXHConnections.aspx\"'>" + GetLocalResourceObject("strViewAllConnections").ToString() + "</a>";
                    string createConnection = string.Format(GetLocalResourceObject("createConnection").ToString(), addInBoundLink);
                    errConnectionMsg.Text += createConnection;
                }
                else
                {
                    errConnectionMsg.Text += GetLocalResourceObject("contactAdmin").ToString();
                }
                pnlMapInfo.Visible = false;
                errConnectionMsg.Visible = true;
            }

            connectionName = ddlConnectionName.SelectedValue.ToString();
            if (!string.IsNullOrEmpty(connectionName) && ConnectionNames != null)
            {
                adapterName = ConnectionNames.Find(cn => cn.ConnectioinName == connectionName).Adapter;
            }

            if (string.IsNullOrEmpty(txtMappingName.Text))
            {
                PageHeader.Text = GetLocalResourceObject("EditMapHeader").ToString();
                DxHMappingData mappedData = DxHUtils.GetMapping(Ektron.Cms.Common.EkEnumeration.CMSObjectTypes.Content, formId, lang);
                if (mappedData != null && mappedData.Adapter != null)
                {
                    MappedData = DxHUtils.SerializeObject<DxHMappingData>(mappedData);
                    txtMappingName.Text = mappedData.Title;
                    adapterName = mappedData.Adapter;
                    ddlConnectionName.SelectedValue = connectionName = mappedData.Connection;

                    LoadObjectTypeDropDown(adapterName, connectionName);
                    ddlObjectType.SelectedValue = mappedData.TargetObjectDefinitionId;
                    if (!string.IsNullOrEmpty(adapterName) && !string.IsNullOrEmpty(connectionName) && !string.IsNullOrEmpty(ddlObjectType.SelectedValue))
                    {
                        liObjectType.Visible = true;
                        btnNext.Enabled = true;
                    }
                }
            }
            objectType = ddlObjectType.SelectedValue.ToString();
        }

        protected void ddlConnectionName_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlObjectType.Items.Clear();
            liObjectType.Visible = false;
            if (!string.IsNullOrEmpty(adapterName) && EktronMeta != null)
            {
                LoadObjectTypeDropDown(adapterName, connectionName);
            }
        }

        protected void ddlObjectType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            btnNext.Enabled = false;
            if (!errConnectionMsg.Visible && !string.IsNullOrEmpty(objectType) && EktronMeta != null)
            {
                btnNext.Enabled = true;
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                pageViews.ActiveViewIndex = pageViews.ActiveViewIndex + 1;
                if (1 == pageViews.ActiveViewIndex)
                {
                    CreateMappingTable();
                    if (this.MappingValues != null && 0 == this.MappingValues.Count)
                    {
                        // skip required fields mapping if there is not required fields in the meta but need to retrieve its optional fields
                        pageViews.ActiveViewIndex = pageViews.ActiveViewIndex + 1;
                    }
                }
                else if (null == this.OptMappingValues && 2 == pageViews.ActiveViewIndex)
                {
                    // skip optional fields mapping if there is not optional fields in the meta
                    pageViews.ActiveViewIndex = pageViews.ActiveViewIndex + 1;
                }
                switch (pageViews.ActiveViewIndex)
                {
                    case 1:
                        // show required fields mapping
                        btnPrevious.Visible = true;
                        btnNext.Enabled = true;
                        btnNext.CausesValidation = true;
                        btnNext.ValidationGroup = "valGroup1";
                        PageHeader.Text = string.Format(GetLocalResourceObject("RequiredFieldHeader").ToString(), adapterName);
                        DxhDescription.Text = string.Format(GetLocalResourceObject("RequiredFieldDescription").ToString(), objectType, adapterName);
                        lblRequiredFieldsInstruction.Text = string.Format(GetLocalResourceObject("RequiredFieldsInstruction").ToString(), objectType);
                        lblRequiredFields.Text = string.Format(GetLocalResourceObject("RequiredFields").ToString(), adapterName);
                        break;
                    case 2:
                        // show optional fields mapping
                        btnPrevious.Visible = true;
                        //btnPrevious.OnClientClick = "GetOptionalMappingValues();";
                        btnNext.Enabled = true;
                        btnNext.Text = btnNext.ToolTip = GetLocalResourceObject("btnMapAction").ToString();
                        //btnNext.OnClientClick = "GetOptionalMappingValues();";
                        btnNext.CausesValidation = false;
                        PageHeader.Text = string.Format(GetLocalResourceObject("OptionalFieldHeader").ToString(), adapterName);
                        DxhDescription.Text = string.Format(GetLocalResourceObject("OptionalFieldDescription").ToString(), objectType, adapterName);
                        lblOptionalFieldsInstruction.Text = string.Format(GetLocalResourceObject("OptionalFieldsInstruction").ToString(), objectType);
                        lblOptionalFieldsNote.Text = string.Format(GetLocalResourceObject("OptionalFieldsNote").ToString(), adapterName);
                        lblOptionalFields.Text = string.Format(GetLocalResourceObject("Available3rdPartyFields").ToString(), adapterName);
                        imgAddRow.Src = cmsContextService.WorkareaPath + "/images/ui/icons/add.png";
                        imgAddRow.Alt = string.Format(GetLocalResourceObject("AddAnotherOptionalField").ToString(), adapterName);
                        lblAddRow.Text = string.Format(GetLocalResourceObject("AddAnotherOptionalField").ToString(), adapterName);
                        linkAddRow.Title = string.Format(GetLocalResourceObject("AddAnotherOptionalField").ToString(), adapterName);
                        CreateOptionalTable();
                        break;
                    case 3:
                    default:
                        // show completed mapping message
                        string err = string.Empty;
                        btnNext.Visible = false;
                        btnCancel.Text = btnCancel.ToolTip = GetLocalResourceObject("btnClose").ToString();
                        btnPrevious.Visible = false;
                        try
                        {
                            SaveMapping();
                        }
                        catch (Exception ex)
                        {
                            err = ex.Message;
                            msgComplete.Text = err;
                            msgComplete.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                            lblCompleteMappingMsg.Visible = false;
                        }
                        if (string.IsNullOrEmpty(err))
                        {
                            DxhDescription.Visible = false;
                            PageHeader.Text = GetLocalResourceObject("CompletePageHeader").ToString();
                            ContentManager contentMgr = new ContentManager();
                            ContentData cd = contentMgr.GetItem(formId);
                            string formName = cd.Title;
                            lblCompleteMappingMsg.Visible = true;
                            lblCompleteMappingMsg.Text = string.Format(GetLocalResourceObject("CompletePageDescription").ToString(), formName, adapterName, objectType);
                            msgComplete.Text = string.Format(GetLocalResourceObject("CompleteMapping").ToString(), formName, adapterName, objectType);
                            lblCompleteValidationMsg.Text = string.Format(GetLocalResourceObject("CompleteValidationMsg").ToString(), adapterName);
                            btnTestMapping.Visible = true; ltrTestMappingURL.Text = string.Format("DXHMappingTest.aspx?objectId={0}&lang={1}", formId.ToString(), lang.ToString());
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                pageViews.Visible = false;
                pnlButtonArea.Visible = false;
                msgExceptionGeneral.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                msgExceptionGeneral.Text = GetLocalResourceObject("strGeneralException").ToString();
            }
        }

        private void SetTextForSteps()
        {
            switch (pageViews.ActiveViewIndex)
            {
                case 0:
                    break;
                case 1:
                    PageHeader.Text = string.Format(GetLocalResourceObject("RequiredFieldHeader").ToString(), adapterName);
                    DxhDescription.Text = string.Format(GetLocalResourceObject("RequiredFieldDescription").ToString(), objectType, adapterName);
                    lblRequiredFieldsInstruction.Text = string.Format(GetLocalResourceObject("RequiredFieldsInstruction").ToString(), objectType);
                    lblRequiredFields.Text = string.Format(GetLocalResourceObject("RequiredFields").ToString(), adapterName);
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                pageViews.ActiveViewIndex = pageViews.ActiveViewIndex - 1;
                if (1 == pageViews.ActiveViewIndex && this.MappingValues != null && 0 == this.MappingValues.Count)
                {
                    pageViews.ActiveViewIndex = pageViews.ActiveViewIndex - 1;
                }
                btnNext.Enabled = true;
                switch (pageViews.ActiveViewIndex)
                {
                    case 0:
                        // show connection step
                        btnPrevious.Visible = false;
                        break;
                    case 1:
                        // show required fields mapping step
                        ReloadMappingValues();
                        CreateMappingTable();
                        SetTextForSteps();
                        break;
                    case 2:
                        // show optional fields mapping step
                        ReloadOptMappingValues();
                        CreateOptionalTable();
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                pageViews.Visible = false;
                pnlButtonArea.Visible = false;
                msgExceptionGeneral.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                msgExceptionGeneral.Text = GetLocalResourceObject("strGeneralException").ToString();
            }
        }

        //get list of items from an adapter\connection
        protected List<FlyweightObjectDefinition> GetFlyweights(string adapterName, string connectionName)
        {
            try
            {
                LoginConnection(adapterName, connectionName);
                List<FlyweightObjectDefinition> metaObjectList = contextbus.GetObjectDefinitionNameList(adapterName);
                //LogoutConnection(adapterName);
                if (metaObjectList != null && metaObjectList.Count > 0)
                {
                    return metaObjectList;
                }
            }
            catch (Exception ex)
            {
                errConnectionMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errConnectionMsg.Text = ex.Message;
                errConnectionMsg.Visible = true;
            }
            return null;
        }

        private void LoginConnection(string adapterName, string connectionName)
        {
            try
            {
                contextbus.Login(connectionName, adapterName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        private void LogoutConnection(string adapterName)
        {
            contextbus.Logout(adapterName);
        }

        private bool IsValidConnectionName(string adapterName, string connectionName)
        {
            bool dxhSupported = false;
            try
            {
                List<ConnectionParam> spParams = connMgr.LoadConnection(connectionName, adapterName).ToList();
                try
                {
                    if (connMgr.TestConnection(spParams, adapterName))
                    {
                        dxhSupported = true;
                    }
                }
                catch (Exception)
                { dxhSupported = false; }
            }
            catch (Exception)
            { dxhSupported = false; }
            return dxhSupported;
        }

        private void CreateMappingTable()
        {
            bool newMapping = false;
            DxHMappingData mappedData = null;
            List<FieldDefinition> reqFields = null;
            if (MappedData != null)
            {
                mappedData = DxHUtils.DeserializeObject<DxHMappingData>(MappedData);
            }

            if (mappedData != null && objectType == mappedData.TargetObjectDefinitionId)
            {
                // edit same mapping
                thirdPartyMeta = DxHUtils.DeserializeObject<ObjectDefinition>(mappedData.TargetObjectDefinition);
                TaskManagerClient taskClient = new TaskManagerClient();
                MappingTask taskData = (MappingTask)taskClient.GetTask(mappedData.MappingTaskId);

                reqFields = taskData.TargetObject.Fields.FindAll(field => field.IsRequired == true);
                reqFields = reqFields.OrderBy(x => x.DisplayName).ToList();
                if (reqFields.Count > 0)
                {
                    this.MappingValues = new List<DXHMappingValue>();
                    foreach (FieldDefinition fld in reqFields)
                    {
                        DXHMappingValue item = new DXHMappingValue();
                        item.Id = fld.Id;
                        item.DisplayName = fld.DisplayName;
                        item.FieldType = fld.DataType.Type;
                        item.ValueName = taskData.TargetFields.Single(m => m.Id == fld.Id).Mapping.Value.ToString();
                        this.MappingValues.Add(item);
                    }
                }
                optFields = taskData.TargetObject.Fields.FindAll(field => field.IsRequired == false);
                if (objectType.ToLower() == "hubspot.contact")
                    optFields = optFields.Where(p => !p.DisplayName.Equals("UserToken")).ToList();
                optFields = optFields.OrderBy(x => x.DisplayName).ToList();
                if (optFields.Count > 0)
                {
                    this.OptMappingValues = new List<DXHMappingValue>();
                    CreateOptionalMappingValues(optFields, taskData.TargetFields.FindAll(m => m.Mapping.Value != null));
                }
            }
            else
            {
                try
                {
                    // add mapping or edit new objectType for mapping
                    LoginConnection(adapterName, connectionName);
                    thirdPartyMeta = contextbus.GetObjectDefinition(adapterName, objectType);

                    if (thirdPartyMeta != null)
                    {
                        if (null == this.MappingValues)
                        {
                            this.MappingValues = new List<DXHMappingValue>();
                            newMapping = true;
                        }
                        else
                        {
                            ReloadMappingValues();
                        }

                        reqFields = thirdPartyMeta.Fields.FindAll(field => field.IsRequired == true);
                        reqFields = reqFields.OrderBy(x => x.DisplayName).ToList();
                        optFields = thirdPartyMeta.Fields.FindAll(field => field.IsRequired == false);
                        if (thirdPartyMeta.Id.ToLower() == "hubspot.contact")
                            optFields = optFields.Where(p => !p.DisplayName.Equals("UserToken")).ToList();
                        optFields = optFields.OrderBy(x => x.DisplayName).ToList();
                        if (optFields.Count > 0)
                        {
                            this.OptMappingValues = new List<DXHMappingValue>();
                            CreateOptionalMappingValues(optFields, null);
                        }
                    }
                }
                catch (Exception ex)
                {
                    errRequiredFldsMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    errRequiredFldsMsg.Text = ex.Message;
                    errRequiredFldsMsg.Visible = true;
                    tblRequiredFields.Visible = false;
                    lblRequiredFieldsInstruction.Visible = false;
                    btnNext.Enabled = false;
                }
            }

            if (thirdPartyMeta != null)
            {
                ThirdPartyMeta = DxHUtils.SerializeObject<ObjectDefinition>(thirdPartyMeta);

                tblRequiredFields.Visible = true;
                lblRequiredFieldsInstruction.Visible = true;

                bool oddRow = true;
                foreach (FieldDefinition field in reqFields)
                {
                    string fieldDisplayName = field.DisplayName;
                    string fieldId = field.Id;
                    string fieldValue = string.Empty;

                    if (true == newMapping)
                    {
                        DXHMappingValue item = new DXHMappingValue();
                        item.Id = fieldId;
                        item.DisplayName = fieldDisplayName;
                        item.FieldType = field.DataType.Type;
                        this.MappingValues.Add(item);
                    }
                    else if (this.MappingValues.Count > 0 && this.MappingValues.FindAll(m => m.ValueName != null).Count > 0)
                    {
                        fieldValue = this.MappingValues.Single(m => m.Id == fieldId).ValueName;
                    }

                    TableRow tr = new TableRow();
                    TableCell tc1 = new TableCell();
                    Literal label = new Literal();
                    label.Text = fieldDisplayName + "&#160;<span class=\"ektron-ui-required\">*</span>";
                    tc1.Controls.Add(label);
                    tr.Cells.Add(tc1);

                    TableCell tc2 = new TableCell();
                    DropDownList dropdownlst = CreateEktronFormDropDown();
                    dropdownlst.ID = fieldId;
                    dropdownlst.CssClass = "ektronformfields";
                    if (!string.IsNullOrEmpty(fieldValue))
                    {
                        dropdownlst.SelectedValue = fieldValue;
                    }
                    tc2.Controls.Add(dropdownlst);
                    RequiredFieldValidator ddValidator = new RequiredFieldValidator();
                    ddValidator.ControlToValidate = fieldId;
                    ddValidator.ValidationGroup = "valGroup1";
                    ddValidator.ErrorMessage = GetLocalResourceObject("RequiredAsterisk").ToString();
                    ddValidator.CssClass = "warning ektron-ui-required";
                    tc2.Controls.Add(ddValidator);
                    tr.Cells.Add(tc2);

                    if (oddRow)
                    {
                        tr.CssClass = "";
                        oddRow = false;
                    }
                    else
                    {
                        tr.CssClass = "ektron-ui-odd";
                        oddRow = true;
                    }
                    tblRequiredFields.Rows.Add(tr);
                }
                hdnJsonFieldValues.Text = Ektron.Newtonsoft.Json.JsonConvert.SerializeObject(this.MappingValues);
            }
            else if (!errRequiredFldsMsg.Visible)
            {
                errRequiredFldsMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errRequiredFldsMsg.Text = string.Format(GetLocalResourceObject("NoObjectDefinitonFound").ToString(), objectType, adapterName);
                errRequiredFldsMsg.Visible = true;
                tblRequiredFields.Visible = false;
                lblRequiredFieldsInstruction.Visible = false;
                btnNext.Enabled = false;
            }
        }

        private void CreateOptionalTable()
        {
            if (this.OptMappingValues != null)
            {
                TableRow tr = new TableRow();
                TableCell tc0 = new TableCell();
                Image img = new Image();
                img.AlternateText = GetLocalResourceObject("DeleteOptionalField").ToString();
                img.ImageUrl = cmsContextService.WorkareaPath + "/images/ui/icons/remove.png";
                img.CssClass = "deleteoptionalfield";
                img.Attributes.Add("onclick", "DeleteOptionalRow(this);");
                tc0.Controls.Add(img);
                tc0.CssClass = "colNarrow";
                tr.Cells.Add(tc0);

                TableCell tc1 = new TableCell();
                DropDownList ddlOptionalFields = new DropDownList();
                ddlOptionalFields.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectOptionalField").ToString(), string.Empty));
                ddlOptionalFields.AppendDataBoundItems = true;
                ddlOptionalFields.DataTextField = "DisplayName";
                ddlOptionalFields.DataValueField = "Id";
                ddlOptionalFields.DataSource = this.OptMappingValues;
                ddlOptionalFields.DataBind();
                ddlOptionalFields.ID = "optDropDown_";
                ddlOptionalFields.CssClass = "optDropDown unique";
                tc1.Controls.Add(ddlOptionalFields);
                Label ddValidator = new Label();
                ddValidator.Text = GetLocalResourceObject("Duplicate").ToString();
                ddValidator.CssClass = "ektron-ui-required ektron-ui-hidden warning";
                tc1.Controls.Add(ddValidator);
                tc1.CssClass = "colWide";

                TableCell tc2 = new TableCell();
                DropDownList dropdownlst = CreateEktronFormDropDown();
                dropdownlst.ID = "mapDropDown_";
                dropdownlst.CssClass = "mapDropDown ektronformfields";
                tc2.Controls.Add(dropdownlst);
                tr.Cells.Add(tc2);
                tr.Cells.Add(tc1);
                tr.CssClass = "repeater ektron-ui-hidden";
                tblOptionalFields.Rows.Add(tr);

                // reload existing rows
                ReloadOptMappingValues();
                List<DXHMappingValue> existing = this.OptMappingValues.FindAll(o => !string.IsNullOrEmpty(o.ValueName));
                if (existing.Count > 0)
                {
                    int counter = 1;
                    foreach (DXHMappingValue map in existing)
                    {
                        tr = new TableRow();
                        tc0 = new TableCell();
                        img = new Image();
                        img.AlternateText = GetLocalResourceObject("DeleteOptionalField").ToString();
                        img.ImageUrl = cmsContextService.WorkareaPath + "/images/ui/icons/remove.png";
                        img.CssClass = "deleteoptionalfield";
                        img.Attributes.Add("onclick", "DeleteOptionalRow(this);");
                        tc0.Controls.Add(img);
                        tc0.CssClass = "colNarrow";
                        tr.Cells.Add(tc0);

                        tc1 = new TableCell();
                        ddlOptionalFields = new DropDownList();
                        ddlOptionalFields.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectOptionalField").ToString(), string.Empty));
                        ddlOptionalFields.AppendDataBoundItems = true;
                        ddlOptionalFields.DataTextField = "DisplayName";
                        ddlOptionalFields.DataValueField = "Id";
                        ddlOptionalFields.DataSource = this.OptMappingValues;
                        ddlOptionalFields.DataBind();
                        ddlOptionalFields.ID = "optDropDown_" + counter;
                        ddlOptionalFields.CssClass = "optDropDown unique";
                        ddlOptionalFields.SelectedValue = this.OptMappingValues.Single(m => m.Id == map.Id).Id;
                        tc1.Controls.Add(ddlOptionalFields);
                        ddValidator = new Label();
                        ddValidator.Text = GetLocalResourceObject("Duplicate").ToString();
                        ddValidator.CssClass = "ektron-ui-required ektron-ui-hidden warning";
                        tc1.Controls.Add(ddValidator);
                        tc1.CssClass = "colWide";


                        tc2 = new TableCell();
                        dropdownlst = CreateEktronFormDropDown();
                        dropdownlst.ID = "mapDropDown_" + counter;
                        dropdownlst.CssClass = "mapDropDown ektronformfields";
                        dropdownlst.SelectedValue = this.OptMappingValues.Single(m => m.Id == map.Id).ValueName;
                        tc2.Controls.Add(dropdownlst);
                        tr.Cells.Add(tc2);
                        tr.Cells.Add(tc1);
                        tr.ID = counter.ToString();
                        tblOptionalFields.Rows.Add(tr);
                        tr.CssClass = "optMappingRows";
                        counter++;
                    }
                }

                tblOptionalFields.Caption = string.Format(GetLocalResourceObject("FieldMappedError").ToString(), adapterName);
                hdnJsonOptFieldValues.Text = Ektron.Newtonsoft.Json.JsonConvert.SerializeObject(this.OptMappingValues);
            }
        }

        private DropDownList CreateEktronFormDropDown()
        {
            DropDownList ddlFormFields = new DropDownList();
            if (null == currentFieldLst && false == errRequiredFldsMsg.Visible)
            {
                FormManager formMgr = new FormManager();
                currentFieldLst = formMgr.GetFormFieldList(formId);
            }

            btnNext.Enabled = true;
            if (currentFieldLst != null)
            {
                ektronFieldCount = currentFieldLst.Fields.Length;
                ddlFormFields.AppendDataBoundItems = true;
                ddlFormFields.DataTextField = "DisplayName";
                ddlFormFields.DataValueField = "FieldName";
                ddlFormFields.DataSource = currentFieldLst.Fields;
                ddlFormFields.DataBind();
                ReorderAlphabetized(ddlFormFields);
                ddlFormFields.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectFieldName").ToString(), string.Empty));
            }
            else
            {
                errRequiredFldsMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errRequiredFldsMsg.Text = GetLocalResourceObject("errMsg").ToString();
                errRequiredFldsMsg.Visible = true;
                btnNext.Enabled = false;
            }

            return ddlFormFields;
        }

        private void CreateOptionalMappingValues(List<FieldDefinition> optFields, List<MappableField> targetFields)
        {
            foreach (FieldDefinition field in optFields)
            {
                DXHMappingValue item = new DXHMappingValue();
                item.Id = field.Id;
                item.DisplayName = field.DisplayName;
                item.FieldType = field.DataType.Type;
                if (targetFields != null && targetFields.Count > 0)
                {
                    foreach (MappableField m in targetFields)
                    {
                        if (m.Id == field.Id)
                        {
                            item.ValueName = (m.Mapping.Value != null ? m.Mapping.Value.ToString() : null);
                            break;
                        }
                    }
                }
                this.OptMappingValues.Add(item);
            }
        }

        private void ReloadMappingValues()
        {
            string reqFieldData = Request.Form["hdnJsonFieldValues"];
            if (!String.IsNullOrEmpty(reqFieldData))
            {
                List<DXHMappingValue> clientDataObject = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<DXHMappingValue>>(reqFieldData);
                if (clientDataObject != null)
                {
                    this.MappingValues = clientDataObject;
                }
            }
        }

        private void ReloadOptMappingValues()
        {
            string optFieldData = Request.Form["hdnJsonOptFieldValues"];
            if (!String.IsNullOrEmpty(optFieldData))
            {
                List<DXHMappingValue> clientDataObject = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<DXHMappingValue>>(optFieldData);
                if (clientDataObject != null)
                {
                    this.OptMappingValues = clientDataObject;
                }
            }
        }

        private void SaveMapping()
        {
            ReloadMappingValues();
            ReloadOptMappingValues();
            if (this.OptMappingValues != null)
            {
                this.OptMappingValues = this.OptMappingValues.FindAll(item => !string.IsNullOrEmpty(item.ValueName));
            }

            List<DXHMappingValue> allFieldsToBeMapped = new List<DXHMappingValue>();
            if (this.MappingValues != null)
            {
                allFieldsToBeMapped.AddRange(this.MappingValues);
            }
            if (this.OptMappingValues != null)
            {
                allFieldsToBeMapped.AddRange(this.OptMappingValues);
            }

            TaskManagerClient taskClient = new TaskManagerClient();
            ektronMeta = DxHUtils.DeserializeObject<ObjectDefinition>(EktronMeta);
            thirdPartyMeta = DxHUtils.DeserializeObject<ObjectDefinition>(ThirdPartyMeta);
            if (thirdPartyMeta.Id.ToLower() == "hubspot.contact")
            {
                ektronMeta.Fields.Add(new FieldDefinition("DxHHubSpotUserToken", "DxHHubSpotUserToken", new FieldType(typeof(string)), false, false));
                allFieldsToBeMapped.Add(new DXHMappingValue()
                {
                    DisplayName = "UserToken",
                    FieldType = "System.String",
                    Id = "usertoken",
                    ValueDisplayName = "DxHHubSpotUserToken",
                    ValueName = "DxHHubSpotUserToken"
                });
            }
            MappingTask map = taskClient.CreateMappingTask(ektronMeta, DxHUtils.DeserializeObject<ObjectDefinition>(ThirdPartyMeta));
            map.SourceObject = ektronMeta;
            map.TargetObject = thirdPartyMeta;
            foreach (MappableField mf in map.TargetFields)
            {
                DXHMappingValue m = allFieldsToBeMapped.Find(mv => mv.Id == mf.Id);
                if (m != null)
                {
                    mf.Mapping.MappingType = MappingType.FieldMapping;
                    mf.Mapping.Value = map.SourceObject.Fields.Single(ek => ek.Id == m.ValueName).Id;
                }
            }
            //List<Ektron.DxH.Common.Contracts.ValidationMessage> vm = taskClient.ValidateTask(map); // for debug only
            map.TaskID = taskClient.SaveTask(map);
            DxHUtils.SaveMapping(adapterName, connectionName, map, null, Ektron.Cms.Common.EkEnumeration.CMSObjectTypes.Content, formId, txtMappingName.Text);

            string ektConnectionName = GetEktronConnectionName();
            DxHUtils.SetLeadIdForUser("Ektron", ektConnectionName, adapterName, connectionName, map, formId, lang);
        }

        private static void ReorderAlphabetized(DropDownList ddl)
        {
            List<ListItem> listCopy = new List<ListItem>();
            foreach (ListItem item in ddl.Items)
                listCopy.Add(item);
            ddl.Items.Clear();
            foreach (ListItem item in listCopy.OrderBy(item => item.Text))
                ddl.Items.Add(item);
        }

        private string GetEktronConnectionName()
        {
            // connect to Ektron Inbound connection to find out the ObjectDefinition of this Ektron Html form

            DxHUtils dxhUtils = new DxHUtils();
            Connection connection = dxhUtils.GetDxhConnection("", "Ektron");

            string ektConnection = null;
            if (connection != null)
            {
                try
                {
                    List<ConnectionParam> spParams = connMgr.LoadConnection(connection.Name, "Ektron").ToList();
                    string ektDomain = "";
                    if (Request.Url.Port == 80)
                        ektDomain = Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host + _ekSite.SitePath;
                    else
                        ektDomain = Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host + ":" + Request.Url.Port + _ekSite.SitePath;
                    foreach (ConnectionParam p in spParams)
                    {
                        if ("siteurl" == p.Id.ToLower() && ektDomain == p.Value)
                        {
                            ektConnection = connection.Name;
                            break;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }

            return ektConnection;
        }
        private void GetEktronMetadata()
        {
            string ektConnection = GetEktronConnectionName();
            if (!string.IsNullOrEmpty(ektConnection))
            {
                try
                {
                    LoginConnection("Ektron", ektConnection);
                    List<FlyweightObjectDefinition> objectLst = GetFlyweights("Ektron", ektConnection);

                    string copylang = Request.QueryString["copy_lang"];
                    if (!string.IsNullOrEmpty(copylang) && Convert.ToInt32(copylang) > 0)
                        lang = Convert.ToInt32(copylang);

                    foreach (FlyweightObjectDefinition f in objectLst)
                    {
                        if (f.Id == "Form|" + formId + "|" + lang)
                        {
                            ektronMeta = contextbus.GetObjectDefinition("Ektron", f.Id);
                            break;
                        }
                    }
                    EktronMeta = DxHUtils.SerializeObject<ObjectDefinition>(ektronMeta);
                }
                catch
                {
                    errConnectionMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    errConnectionMsg.Text = GetLocalResourceObject("errEktronAdapterMsg").ToString();
                    errConnectionMsg.Visible = true;
                }
            }
        }

        private void LoadObjectTypeDropDown(string adapterName, string connectionName)
        {
            if (IsValidConnectionName(adapterName, connectionName))
            {
                List<FlyweightObjectDefinition> objectLst = GetFlyweights(adapterName, connectionName);
                if (objectLst != null)
                {
                    if (ddlObjectType.Items.FindByText(GetLocalResourceObject("SelectObjectType").ToString()) == null)
                    {
                        ddlObjectType.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectObjectType").ToString(), string.Empty));
                    }

                    ddlObjectType.AppendDataBoundItems = true;
                    ddlObjectType.DataSource = objectLst;
                    ddlObjectType.DataBind();

                    lblObjectTypeInstruction.Text = string.Format(GetLocalResourceObject("ObjectTypeInstruction").ToString(), adapterName);
                    lblObjectType.Text = string.Format(GetLocalResourceObject("DXHObjectType").ToString(), adapterName);
                    liObjectType.Visible = true;
                }
            }
            else
            {
                errConnectionMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errConnectionMsg.Text = string.Format(GetLocalResourceObject("InvalidConnectionNameSelected").ToString(), connectionName);
                errConnectionMsg.Visible = true;
            }
        }
    }
}