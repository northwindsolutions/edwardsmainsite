﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DXHMappingTest.aspx.cs" Inherits="Workarea.DxH.DXHMappingTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .PageWarpper
        {
            margin-left: auto;
            margin-right: auto;
            width: 550px;
        }
        .InputArea
        {
            border: solid 1px rgb(218, 218, 218);
            padding: 15px;
            margin-bottom: 15px;
        }
        .editTDLeft
        {
            padding-right: 20px;
            vertical-align: middle;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .tdRight
        {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="PageWarpper">
        <asp:UpdatePanel runat="server" ID="uplWarpper">
            <ContentTemplate>
                <div class="contentDiv">
                    <div style="padding-right: 15px; margin-bottom: 15px;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 64px;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;
                                    padding-left: 5px;">
                                    <asp:Literal ID="ltrPageHeadign" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Literal runat="server" ID="ltrPageInstr" />
                                </td>
                            </tr>
                        </table>
                        <asp:UpdateProgress runat="server" ID="updProgress">
                            <ProgressTemplate>
                                <ektronUI:Message runat="server" ID="updWorking" DisplayMode="Working" Text="<%$Resources:str_PleaseWait %>">
                                </ektronUI:Message>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <ektronUI:Message runat="server" ID="msgBox" DisplayMode="Error" Visible="false"
                        EnableViewState="false">
                    </ektronUI:Message>
                    <asp:Panel runat="server" ID="pnlControlHolder" CssClass="InputArea" />
                    <asp:Panel class="btnArea" runat="server" ID="pnlBtnArea">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: left;">
                                    
                                    <ektronUI:Button runat="server" ID="btnEditMapping" Text="<%$Resources:btnEditMapping %>" Visible="true"  DisplayMode="Anchor" />
                                </td>
                                <td style="text-align: right">
                                <ektronUI:Button runat="server" ID="btnTest" Text="<%$Resources:btnTest %>" OnClick="btnTestClick"
                                        OnClientClick="TextBtnClicked()" />
                                    <ektronUI:Button runat="server" ID="btnCancelNoConfirm" Text="<%$Resources:btnClose %>" Visible="false" OnClientClick="parent.closeDialog(); return false;" />
                                    <ektronUI:Button runat="server" ID="btnCancel" Text="<%$Resources:btnClose %>" OnClientClick="return showComfirmArea();" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <div class="confirmDiv" style="display: none;">
                     <table style="width: 100%;">
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 64px;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;
                                    padding-left: 5px;">
                                    <asp:Literal ID="ltr_CloseConfirmHeading" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Literal runat="server" ID="ltr_CloseConfirmBody" />
                                </td>
                            </tr>
                        </table>
                    <ektronUI:Message runat="server" ID="msgConfirm" Text="ConfirmText" DisplayMode="Warning" Visible="false">
                    </ektronUI:Message>
                    <table style="width: 100%; margin-top:15px;">
                        <tr>
                            <td style="width: 50%; text-align: left;">
                                <ektronUI:Button runat="server" ID="btnConfirm" Text="<%$Resources:str_EndTest %>" OnClientClick="parent.closeDialog(); return false;" />
                            </td>
                            <td style="text-align: right">
                                <ektronUI:Button runat="server" ID="btnConfirmCancel" Text="<%$Resources:btnCancel %>"
                                    OnClientClick="return showComfirmArea();" />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
		<input type="hidden" class="hubspotusertoken" id="hubspotusertoken" runat="server" />
    </div>
    <script type="text/javascript">
        function showComfirmArea() {
            $('.confirmDiv').toggle();
            $('.contentDiv').toggle();
            return false;
        }
        function TextBtnClicked() {
            $ektron("<%= btnTest.Selector %>").button("disable");
        }
		$(document).ready(
            function () {
                $(".hubspotusertoken").val(GetCookie("hubspotutk"));
            });

        function GetCookie(name) {
            var arg = name + "=";
            var alen = arg.length;
            var clen = document.cookie.length;
            var i = 0;
            while (i < clen) {
                var j = i + alen;
                if (document.cookie.substring(i, j) == arg) {
                    return getCookieVal(j);
                }
                i = document.cookie.indexOf(" ", i) + 1;
                if (i == 0) break;
            }
            return null;
        }

        function getCookieVal(offset) {
            var endstr = document.cookie.indexOf(";", offset);
            if (endstr == -1) { endstr = document.cookie.length; }
            return unescape(document.cookie.substring(offset, endstr));
        }            
    </script>
    </form>
</body>
</html>
