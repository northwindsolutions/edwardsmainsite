﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeleteDxhMap.aspx.cs" Inherits="Workarea.DxH.DeleteDxhMap" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$Resources: PageTitle%>" /></title>
    <script type="text/javascript">
        function CompleteDeleteAction(action)
        { 
            if ("function" == typeof self.parent.CompleteDeleteAction)
            {
                self.parent.CompleteDeleteAction(action);
            }
        }
        function CompleteDeleteAction(action, jsContentLanguage, jsFormId) {
            if ("function" == typeof self.parent.CompleteDeleteAction) {
                self.parent.CompleteDeleteAction(action, jsContentLanguage, jsFormId);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <div id="general">
        <asp:Image CssClass="pageLogo" ID="dxhLogo" runat="server" ImageUrl="~/Workarea/DxH/images/dxh_logo_60x60.png" AlternateText="<%$Resources:DxhLogoText %>" ToolTip="<%$Resources:DxhLogoText %>" /><asp:Label ID="PageHeader" runat="server" CssClass="ektron-ui-text-xlarge pageHeader"></asp:Label><br />
        <asp:Label ID="DxhDescription" runat="server"></asp:Label>
    </div>
    <div class="instructionMsg">
        <p><asp:Literal ID="lblDeleteMsg1" runat="server"></asp:Literal></p>
        <%--<p class="importantMsg"><asp:Literal ID="lblDeleteMsg2" runat="server"></asp:Literal></p>--%>

        <asp:Panel runat="server" ID="pnlExceptions" Visible="false">
        <ektronUI:Message DisplayMode="Error" runat="server" ID="errMsg" Visible="false" EnableViewState="false" />
                    <a runat="server" id="ancShowDetail" href="#" class="details" title='<%$Resources:strExceptionShowDetail %>'> <asp:Literal runat="server" ID="ltrExMore" Text="<%$Resources:strExceptionShowDetail %>"/></a>
                    <a runat="server" id="ancHideDetail" href="#" class="details ektron-ui-hidden" title='<%$Resources:strExceptionHideDetail %>'> <asp:Literal runat="server" ID="Literal2" Text="<%$Resources:strExceptionHideDetail %>"/></a>
                            <p class="details ektron-ui-hidden">
                                <asp:Literal runat="server" ID="ltrExcText" />        
                            </p>
                            <ektronUI:JavaScriptControlGroup ID="uxControlGroup" AssociatedTriggerIDs="<%# ancShowDetail.ClientID+','+ ancHideDetail.ClientID %>"
                                AssociatedTargetCssClasses="details"
                                AssociatedEvents="click" runat="server">
                                <Modules>
                                    <ektronUI:ToggleModule ID="ToggleModule1" runat="server" />
                                </Modules>
                            </ektronUI:JavaScriptControlGroup>
          </asp:Panel>

       
    </div>
    <asp:Panel runat="server" ID="pnlButtonArea">
        <span class="buttonRight dlgButton">
        <ektronUI:Button runat="server" ID="btnDlgDelete" ToolTip="<%$Resources:btnDelete %>" Text="<%$Resources:btnDelete %>" OnClick="btnDelete_Click" CssClass="buttonNext"/>
        <ektronUI:Button runat="server" ID="btnDlgCancel" ToolTip="<%$Resources:btnCancel %>" Text="<%$Resources:btnCancel %>" OnClientClick="CompleteDeleteAction('cancel'); return false;" />
        </span>
    </asp:Panel>
    <asp:Literal ID="litScript" runat="server" />
    </form>
</body>
</html>
