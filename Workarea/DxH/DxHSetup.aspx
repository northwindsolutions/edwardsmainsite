﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DxHSetup.aspx.cs"
    Inherits="Workarea.DxH.DxHSetup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .action-bar-rollover-indicator
        {
            display: none;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PopUpWindow(url, hWind, nWidth, nHeight, nScroll, nResize) {
            var cToolBar = "toolbar=0,location=0,directories=0,status=" + nResize + ",menubar=0,scrollbars=" + nScroll + ",resizable=" + nResize + ",width=" + nWidth + ",height=" + nHeight;
            var popupwin = window.open(url, hWind, cToolBar);
            return popupwin;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:JavaScriptBlock ID="uxOpenDialogScript" runat="server" ExecutionMode="OnParse">
            <ScriptTemplate>
                function openDelConfirmDialog(){ 
                $ektron("<%= diagDelConfirm.Selector %>").dialog("open");
            } 
            function closeDelConfirmDialog(){ 
                $ektron("<%= diagDelConfirm.Selector %>").dialog("close");
            }
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>

        <div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="divTitleBar" runat="server">
                    <span id="WorkareaTitleBar">
                        <asp:Literal ID="ltrPageTitle" runat="server" Text="<%$Resources: PageTitle%>" /></span>
                </div>
                <div class="ektronToolbar" id="divToolBar">
                    <table>
                        <tr>
                            <td runat="server" visible="false" id="tdCancelButton" style="padding-right: 5px;">
                                <a class="primary cancelButton" href="ViewDXHConnections.aspx" runat="server" id="achBack"></a>
                            </td>
 <%--                           <td>
                                <ektronUI:Button runat="server" ID="btnConnectDXH" Text="<%$Resources: btnConnDXH%>"
                                    DisplayMode="Button" ValidationGroup="ConnDXH" OnClick="btnConnectDXH_Click" />
                            </td>--%>
                            <td>
                                <div class="actionbarDivider">
                                </div>
                            </td>
                            <td>
                                <asp:Literal runat="server" ID="litHelp" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="ektronPageContainer" style="padding-left: 15px; padding-right: 15px;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left; vertical-align: middle; width: 80px;">
                            <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                        </td>
                        <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;">
                            <asp:Literal ID="ltrPageHeadign" runat="server" Text="<%$Resources: PageTitle%>" />
                        </td>
                    </tr>
                </table>
                <div>
                    <asp:Literal ID="ltrInstruction" runat="server" Text="<%$Resources: strInstructions%>" />
                </div>
                <div style="font-weight: bold; text-align: left; font-size: x-small; width: 430px; margin-bottom: 15px;">
                    <asp:Label runat="server" Text="<% $Resources: Required %>" ID="aspRequired"></asp:Label>
                    <span class="ektron-ui-required">*</span>
                </div>
                <div id="divInstruction">
                    <asp:UpdateProgress runat="server" ID="UProgress">
                        <ProgressTemplate>
                            <ektronUI:Message runat="server" ID="msgProgress" DisplayMode="Working" Text="<%$Resources: strTestConnProgressText%>" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="uPnlTestBtn" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ektronUI:Message DisplayMode="Error" runat="server" ID="msgError" Visible="false"
                                EnableViewState="false" />
                            <table style="width: 530px;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Label ID="lbTblURL" runat="server" Text="<%$Resources: tbURLLabel%>" Font-Bold="true" />
                                        <span class="ektron-ui-required">*</span>
                                        <ektronUI:TextField runat="server" ID="tbURL" ValidationGroup="ConnDXH">
                            <ValidationRules>
                                <%--<ektronUI:RequiredRule ErrorMessage="Required Field" ClientValidationEnabled="true"/>--%>
                                <%--<ektronUI:UrlRule ClientValidationEnabled="true"/>--%>
                                <ektronUI:CustomRule JavascriptFunctionName="ValidateTextField" />
                            </ValidationRules>
                                        </ektronUI:TextField>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <div style="margin-top: 20px; margin-bottom: 15px;">
                                            <%--<div style="margin-top:1em ">--%>
                                            <ektronUI:Button runat="server" ID="btnConnectDxH2" Text="<%$Resources: btnConnDXH1%>"
                                                DisplayMode="Button" ValidationGroup="ConnDXH" OnClick="btnConnectDXH_Click" />
                                            <%--</div>--%>
                                            <ektronUI:Button runat="server" ID="btnTestConn" Text="<%$Resources: btnTestConn%>"
                                                DisplayMode="Button" ValidationGroup="ConnDXH" OnClick="btnTestConn_Click" />
                                            <ektronUI:Button runat="server" ID="btnDelete" Text="<%$Resources: btnDeleteConn%>"
                                                DisplayMode="Button" OnClientClick="openDelConfirmDialog();" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnConnectDxH2" />
                            <asp:AsyncPostBackTrigger ControlID="btnTestConn" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <br />
                </div>
                <ektronUI:Dialog runat="server" ID="diagSaveResult" Modal="true" Width="550" Resizable="false"
                    EnableViewState="false" Title="<%$Resources: PageTitle%>">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 17%;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources: PageTitle%>" />
                                </td>
                            </tr>
                        </table>
                        <div class="ektronTopSpace">
                            <ektronUI:Message runat="server" ID="msgSaveConirm" DisplayMode="Error" Text="<%$Resources: strTestFail%>" />
                        </div>
                        <asp:Literal runat="server" ID="ltrSaveConfirm" Text="<%$Resources: strSaveConfirm%>" />
                    </ContentTemplate>
                    <Buttons>
                        <ektronUI:DialogButton Text="<%$Resources: btnSave%>" ID="btnSaveConfirm" OnClick="btnSaveConfirm_Click"></ektronUI:DialogButton>
                        <ektronUI:DialogButton CloseDialog="true" ID="btnCancelConfirm" Text="<%$Resources: btnCancel%>"></ektronUI:DialogButton>
                    </Buttons>
                </ektronUI:Dialog>
                <ektronUI:Dialog runat="server" ID="diagSaveOK" Modal="true" Width="550" Resizable="false"
                    EnableViewState="false" Title="<%$Resources: strSaveSucessTitle%>">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 17%;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: strSaveSucessTitle%>" />
                                </td>
                            </tr>
                        </table>
                        <div class="ektronTopSpace">
                            <ektronUI:Message runat="server" ID="msgDiagSaveOK" DisplayMode="Success" Text="<%$Resources: strSaveSucess%>" />
                        </div>
                    </ContentTemplate>
                    <Buttons>
                        <ektronUI:DialogButton Text="<%$Resources: btnOK%>" ID="btnSaveOK"></ektronUI:DialogButton>
                    </Buttons>
                </ektronUI:Dialog>

                <%--confirm delete dialog--%>
                <ektronUI:Dialog runat="server" ID="diagDelConfirm" AutoOpen="false" Title="<%$Resources: strDelConfirm_Dlg_Title%>"
                    Width="640" Resizable="false" Modal="true">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 17%;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em; padding-left: 5px;">
                                    <asp:Literal ID="ltrDeleteHeader" runat="server" Text="<%$Resources: strDelConfirm_message%>" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Buttons>
                        <ektronUI:DialogButton runat="server" ID="btnDeleteConfirm" OnClick="btnDeleteConfim_Click"
                            ValidationGroup="NoValiGrp" Text="<%$Resources: btnDelete_confirm%>" CloseDialog="false" />
                        <ektronUI:DialogButton runat="server" ID="DialogButton1" CloseDialog="true" Text="Cancel" />
                    </Buttons>
                </ektronUI:Dialog>

                <%-- Delete Success dialog--%>
                <ektronUI:Dialog runat="server" ID="diagDelSuccess" AutoOpen="false" Title="<%$Resources: strDelConfirm_Dlg_Title%>"
                    Width="640" Resizable="false" Modal="true">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 17%;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em; padding-left: 5px;">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$Resources: strDelSuccessMessage%>" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                   <Buttons>
                        <ektronUI:DialogButton Text="<%$Resources: btnOK%>" ID="DialogButton2"></ektronUI:DialogButton>
                    </Buttons>
                </ektronUI:Dialog>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function ValidateTextField(Fieldval, FieldName) {

            if ($.trim(Fieldval) == '') {
                $ektron('#<%=msgError.ClientID %>').css('display', 'none');
                return false;
            }
            else
                return true;
        }
    </script>
</body>
</html>
