﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorLogView.aspx.cs" Inherits="Workarea.DxH.ErrorLogView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function PopUpWindow(url, hWind, nWidth, nHeight, nScroll, nResize) {
            var cToolBar = "toolbar=0,location=0,directories=0,status=" + nResize + ",menubar=0,scrollbars=" + nScroll + ",resizable=" + nResize + ",width=" + nWidth + ",height=" + nHeight;
            var popupwin = window.open(url, hWind, cToolBar);
            return popupwin;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:JavaScriptBlock runat="server" ID="uxJavaScriptBlock">
            <ScriptTemplate>
                ToggleFilterUI = function() 
                { 
                    var filterCriteria = $ektron('.criterias'); 
                    if(filterCriteria.is(":hidden"))
                    {
                        filterCriteria.show();
                        $ektron("img.showHideArrow").attr("src","../images/UI/Icons/arrowHeadDown.png");
                        $ektron("img.showHideArrow").attr("alt", "Hide Filter Results");
                        $ektron("img.showHideArrow").attr("title", "Hide Filter Results");
                    }
                    else
                    {
                        filterCriteria.hide();
                        $ektron("img.showHideArrow").attr("src","../images/UI/Icons/arrowHeadLeft.png");
                        $ektron("img.showHideArrow").attr("alt", "Show Filter Results");
                        $ektron("img.showHideArrow").attr("title", "Show Filter Results");
                    }
                }

                DisplayFromTo = function()
                {
                    if($ektron("#aspDateRangeDropDown")[0].value == "Custom range...")
                    $ektron(".dateRangeWrapper").show();
                    else
                    $ektron(".dateRangeWrapper").hide();
                }

                $ektron("#<%= fromTimepickerInfieldLabel.ClientID%>").addClass("inFieldLabel").inFieldLabels();
                $ektron("#<%= toTimepickerInfieldLabel.ClientID%>").addClass("inFieldLabel").inFieldLabels();
                $ektron(".timepicker").timepicker({
                    ampm: true,
                    timeFormat: "<%= System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern.Replace("tt", "TT").ToString() %>",
                    separator: "<%= System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat .TimeSeparator %>",
                    showSecond: true
                });

                // insert icons for severity

                var severityItems = $ektron("#<%= aspSeverityCheckBoxList.ClientID %> label");
                var infoIconMarkUp = '<img alt="information" class="iconSpacing" title="Information" src="../FrameworkUI/images/silk/icons/information.png"/>';
                var errorIconMarkUp = '<img alt="error" class="iconSpacing" title="Error" src="../FrameworkUI/images/silk/icons/exclamation.png"/>';
                var warningIconMarkUp = '<img alt="warning" class="iconSpacing" title="Warning" src="../FrameworkUI/images/silk/icons/error.png"/>';

                for(var i = 0; i < severityItems.length; i++)
                {
                    switch (severityItems[i].innerHTML)
                    {
                        case "Info":
                            $ektron(severityItems[i]).prepend(infoIconMarkUp);
                        break;
                        case "Error":
                            $ektron(severityItems[i]).prepend(errorIconMarkUp);
                        break;
                        case "Warning":
                            $ektron(severityItems[i]).prepend(warningIconMarkUp);
                        break;
                    }
                }
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
    <div class="ektronPageHeader">
        <div class="ektronTitlebar" id="divTitleBar" runat="server">
            <span id="WorkareaTitleBar">
                <asp:Literal ID="ltrPageTitle" runat="server" Text="<%$Resources: PageTitle%>" /></span>
        </div>
        <div class="ektronToolbar" id="htmToolBar" runat="server">
            <table>
                <tr>
                    <td>
                        <a style="cursor: default" class="primary backButton" href="ViewDXHConnections.aspx">
                        </a>
                    </td>
                    <td style="padding-left: 5px;">
                        <ektronUI:Button runat="server" Text="<%$Resources: exportLog %>" ToolTip="<% $Resources: exportLog%>"
                            ID="exportLog" OnClick="exportLog_Click"></ektronUI:Button>
                    </td>
                    <td><div class="actionbarDivider"></div></td>
                    <td><asp:Literal runat="server" ID="litHelp"/></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ektronPageContainer ektronPageInfo">
        <div class="errorFilterCriteria">
            <div style="padding-top: 0.5em; padding-left: 0.5em">
                <span><strong><a onclick="ToggleFilterUI();" href="#">
                    <asp:Label runat="server" ID="aspFilterResults" Text="<%$Resources:aspFilterResults  %>"
                        ToolTip="<%$Resources:aspFilterResults  %>"></asp:Label></a></strong></span><span><a
                            onclick="ToggleFilterUI();" href="#"><img class="showHideArrow" src="../images/UI/Icons/arrowHeadLeft.png"
                                alt="Show Filter Results" title="Show Filter Results" /></a></span></div>
            <div class="errorFilterCriteria criterias">
                <div class="severity" style="position:absolute;width:100%">
                    <div class="filterLabel">
                        <strong>
                            <asp:Label runat="server" ID="aspSeverity" Text="<%$Resources: aspSeverity %>" ToolTip="<%$Resources: aspSeverity %>"></asp:Label></strong>
                    </div>
                    
                    <asp:CheckBoxList runat="server" ID="aspSeverityCheckBoxList">
                    </asp:CheckBoxList>
                </div>
                <div class="dateLogged" style="position:absolute;top:28px;left:220px">
                    <div class="filterLabel">
                        <strong>
                            <asp:Label runat="server" ID="aspDateLogged" Text="<%$Resources: aspDateLogged %>"
                                ToolTip="<%$Resources: aspDateLogged %>"></asp:Label></strong>
                    </div>
                    <div>
                        <asp:DropDownList Width="20em" runat="server" ID="aspDateRangeDropDown">
                            <asp:ListItem Text="<%$Resources: Anytime(default) %>"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources: Lasthour %>"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources: Last12hours %>"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources: Last24hours %>"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources: Last7days %>"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources: Last30days %>"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources: Customrange... %>"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="dateRangeWrapper">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="aspFrom" Text="<%$Resources: aspFrom %>" ToolTip="<%$Resources: aspFrom %>"></asp:Label>
                                </td>
                                <td>
                                    <div class="fromDatePicker">
                                        <ektronUI:Datepicker runat="server" CssClass="customRangeDatepicker" ID="uxFromDatePicker"></ektronUI:Datepicker>
                                        <span class="ektron-ui-timepicker">
                                            <asp:Label AssociatedControlID="fromTimepicker" ID="fromTimepickerInfieldLabel" runat="server" />
                                            <asp:TextBox ID="fromTimepicker" CssClass="timepicker fromTimepicker inputWidth"
                                                runat="server" />
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="aspTo" Text="<%$Resources: aspTo %>" ToolTip="<%$Resources: aspTo %>"></asp:Label>
                                </td>
                                <td>
                                    <div class="toDatePicker">
                                        <ektronUI:Datepicker runat="server" CssClass="customRangeDatepicker" ID="uxToDatePicker"></ektronUI:Datepicker>
                                        <span class="ektron-ui-timepicker">
                                            <asp:Label AssociatedControlID="toTimepicker" ID="toTimepickerInfieldLabel" runat="server" />
                                            <asp:TextBox ID="toTimepicker" CssClass="timepicker fromTimepicker inputWidth" runat="server" />
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="source" style="position:absolute;top:28px;left:520px">
                    <div class="filterLabel">
                        <strong>
                            <asp:Label runat="server" ID="aspSource" Text="<%$Resources: aspSource %>" ToolTip="<%$Resources: aspSource %>"></asp:Label></strong>
                    </div>
                    <div class="scrollSource">
                        <asp:CheckBoxList runat="server" ID="aspSourceList">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="eventIDErrorLog" style="margin:0px 800px">
                    <div class="filterLabel">
                        <strong>
                            <asp:Label runat="server" ID="aspEventID" Text="<%$Resources: aspEventID %>" ToolTip="<%$Resources: aspEventID %>"></asp:Label></strong>
                    </div>
                    <div class="scrollEventID">
                        <asp:CheckBoxList runat="server" ID="aspEventIDList">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="filterButtons" style="position:absolute;top:210px">
                    <ektronUI:Button runat="server" ID="uxFilterResults" Text="<%$Resources: uxFilterResults %>"
                        ToolTip="<%$Resources: uxFilterResults %>" OnClick="uxFilterResults_Click"></ektronUI:Button>
                    <ektronUI:Button runat="server" ID="uxClear" OnClick="uxClear_Click" Text="<%$Resources:uxClear %>"
                        ToolTip="<%$Resources:uxClear%>"></ektronUI:Button>
                </div>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <div class="ektronTopSpace">
            <ektronUI:GridView runat="server" ID="EkGrid" AutoGenerateColumns="false" EnableEktronUITheme="true" OnEktronUISortChanged="EkGrid_EktronUIThemeSortChanged" OnEktronUIPageChanged="EkGrid_EktronUIThemePageChanged"  Width="99%" >
                <Columns>

                        <asp:TemplateField HeaderText="<%$Resources: gv_header_Severity%>"  ItemStyle-Width="10px" SortExpression="Severity">
                            <ItemTemplate>
                            <div style="width:17px;height:17px; overflow :hidden;">
                                <image class='<%#((Ektron.DxH.Common.Exceptions.ExceptionLogItem)Container.DataItem).Severity %>'   src='<%=ResolveUrl("~/WorkArea/FrameworkUI/images/sprites/interactionCues.png") %>'></image>
                            </div>
                            
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="LogRowID" HeaderText="<%$Resources: gv_header_LogRowID%>"  ItemStyle-Width="15px" SortExpression="LogID"/>
                        
                        <asp:TemplateField ItemStyle-Width="70px" SortExpression="Date" HeaderText="<%$Resources: gv_header_Date %>">
                        
                            <ItemTemplate>
                            
                                <%# 
                                   Ektron.Cms.Common.EkFunctions.FormatDisplayDate (TimeZoneInfo.ConvertTimeFromUtc(((Ektron.DxH.Common.Exceptions.ExceptionLogItem)Container.DataItem).Date, TimeZoneInfo.Local),LangId) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Source" SortExpression="Source" HeaderText="<%$Resources: gv_header_Source%>" ItemStyle-Width="50px"  />
                        <asp:BoundField DataField="EventID" SortExpression="EventID" HeaderText="<%$Resources: gv_header_EventID%>"  ItemStyle-Width="10px" />
                        
                        <asp:TemplateField HeaderText="<%$Resources: gv_header_Message%>">
                        <ItemTemplate>
                                <%#Eval("Message").ToString().Length > 150 ? Eval("Message").ToString().Substring(0, 150)  : Eval("Message")%>
                                
                                <span  class='<%#Eval("LogRowID") + "_details  ektron-ui-hidden" %>' id='<%#Eval("LogRowID") + "_more_detail" %>'>
                                   <%#Eval("Message").ToString().Length > 150 ? Eval("Message").ToString().Substring(150)  : ""%>
                                </span>
                                <%#Eval("Message").ToString().Length > 150 ? " <span style='display:inline;'>" : " <span style='display:none;'>"%>
                                <a style="color:Blue; text-decoration:underline"  class='<%#Eval("LogRowID") + "_details" %>' href="#"  onclick="$('.<%#Eval("LogRowID") + "_details" %>').toggle()">
                                    <asp:Literal runat="server" ID="ltrExMore" Text="<%$Resources:strMoreDetail %>" />
                                </a>
                                
                                <a style="color:Blue; text-decoration:underline" class='<%#Eval("LogRowID") + "_details  ektron-ui-hidden" %>' href="#" onclick="$('.<%#Eval("LogRowID") + "_details" %>').toggle()" >
                                    <asp:Literal runat="server" ID="ltrExLess" Text="<%$Resources:strLessDetail %>" />
                                </a>
                                </span>
                                

                            </ItemTemplate>
                        </asp:TemplateField>
                        
                </Columns>
        </ektronUI:GridView>
        <ektronUI:Message runat="server" ID="msgNoRecords" DisplayMode="Information" Text="<%$Resources:msgNoRecords  %>" EnableViewState="false" Visible="false"></ektronUI:Message>
        <div style="text-align:right; width:100%;">
           *<asp:Literal runat="server" ID="ltrTimeZoneText" />
        </div>
        
        </div>
    </div>
    </form>
</body>
</html>
