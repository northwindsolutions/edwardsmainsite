﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImportSharepointContent.aspx.cs"
    Inherits="Ektron.Workarea.DxH.ImportSharepointContent" %>

<%@ Register Src="Controls/ObjectDefMappingUtility.ascx" TagName="MappingUtility"
    TagPrefix="EktronUC" %>
<%@ Register Src="Controls/ImportSPListOrItem.ascx" TagName="ucImportSPListOrItem"
    TagPrefix="EktronUC" %>
<%@ Register Src="Controls/ImportSPGridTreeList.ascx" TagName="ucImportSPGridTreeList"
    TagPrefix="EktronUC" %>
<%@ Register Src="AddSharePointContent.ascx" TagName="ConnectionList" TagPrefix="EktronUC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="uxScriptManager" runat="server" AllowCustomErrorsRedirect="true">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="uxUpdateProgress" runat="server" DisplayAfter="0" ClientIDMode="AutoID">
        <ProgressTemplate>
            <div class="ui-widget-overlay">
                <div id="xhr-message" class="ektronOverlayPleaseWait xhr-message">
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="ektronPageHeader dxhPageHeader">
        <div class="ektronTitlebar" id="divTitleBar" runat="server">
            <span id="WorkareaTitleBar">
                <asp:Literal ID="ltrPageTitle" runat="server" /></span>
        </div>
    </div>
    <div class="ektronPageContainer dxhPageContainer">
        <div class="helpBar ektron-ui-clearfix">
            <asp:Literal runat="server" ID="ltrBackButton" />
            <asp:Literal runat="server" ID="ltrHelp" />
        </div>
        <asp:UpdatePanel ChildrenAsTriggers="true" ClientIDMode="AutoID" UpdateMode="Always"
            runat="server" ID="uxUpdatePanel">
            <ContentTemplate>
                <asp:Wizard ID="uxSharePointWizard" runat="server" ActiveStepIndex="0" DisplaySideBar="false"
                    OnActiveStepChanged="uxSharePointWizard_ActiveStepChanged" Width="100%">
                    <HeaderTemplate>
                        <div class="DxHHeader">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Workarea/DxH/images/dxh_logo_60x60.png" />
                                    </td>
                                    <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;
                                        padding-left: 1em;">
                                        <asp:Literal runat="server" Text="Title" ID="ltrTitle" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </HeaderTemplate>
                    <WizardSteps>
                        <asp:WizardStep ID="WizardStep1" runat="server" EnableTheming="false" StepType="Start"
                            Title="Select Connection">
                            <div class="stepContent">
                                <EktronUC:ConnectionList ID="uxConnectionList" runat="server" />
                            </div>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep2" runat="server" EnableTheming="False" StepType="Step"
                            Title="Item Or List">
                            <div class="stepContent">
                                <EktronUC:ucImportSPListOrItem runat="server" ID="uxImportSPListOrItem"></EktronUC:ucImportSPListOrItem>
                            </div>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep3" runat="server" EnableTheming="False" Title="Selection">
                            <div class="stepContent">
                                <EktronUC:ucImportSPGridTreeList runat="server" ID="uxImportSPGridTreeList"></EktronUC:ucImportSPGridTreeList>
                            </div>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep4" runat="server" EnableTheming="False" Title="Mapping">
                            <div class="stepContent">
                                <div>
                                    <p>
                                        <asp:Literal runat="server" ID="ltrMapText" /></p>
                                </div>
                                <asp:UpdatePanel ID="uxSelectedItemsListPanel" runat="server" ChildrenAsTriggers="true"
                                    UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="uxSelectedItemsListWrapper">
                                            <asp:Repeater ID="uxSelectedItemsRepeater" runat="server">
                                                <HeaderTemplate>
                                                    <ul>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li class="tabsLink"><a href='<%# DataBinder.Eval(Container.DataItem, "IdHash", "#{0}") %>'
                                                        class="EmptyAnchor"></a>
                                                        <asp:LinkButton ID="sharePointItemLinkButton" runat="server" CommandArgument='<%# Eval("ObjectDefinitionId") %>'
                                                            OnCommand="SharepointItemCommand" Text='<%# Eval("List") %>' />
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="uxSelectedItemsRepeaterDivs" runat="server">
                                                <ItemTemplate>
                                                    <div id='<%# DataBinder.Eval(Container.DataItem, "IdHash") %>'>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="mappingUtilContainer">
                                            <asp:UpdatePanel runat="server" ID="mappingUtilityPanel">
                                                <ContentTemplate>
                                                    <EktronUC:MappingUtility ID="theMappingUtil" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <ektronUI:JavaScriptBlock ID="uxJavascriptBlock" ExecutionMode="OnEktronReady" runat="server">
                                            <ScriptTemplate>
                                                var theCurrentTabId = $ektron('#uxCurrentTabId'), 
                                                    theMappingUtil = $ektron('.mappingUtilContainer');

                                                $ektron(".uxSelectedItemsListWrapper").tabs( { 
                                                            select: function(event, ui){ 
                                                                theCurrentTabId.val(ui.index);
                                                            }, 
                                                            selected: theCurrentTabId.val(), 
                                                        }).addClass('ui-tabs-vertical ui-helper-clearfix');
                                                $ektron(".uxSelectedItemsListWrapper li").removeClass('ui-corner-top').addClass('ui-corner-left');
                                                $ektron(".tabsLink").on('click',function(){ 
                                                    var children = $ektron(this).children('a');
                                                    children[0].click(); 
                                                    children[1].click(); 
                                                    }); 
                                                    theMappingUtil.appendTo('.uxSelectedItemsListWrapper div:not(.ui-tabs-hide)');
                                            </ScriptTemplate>
                                        </ektronUI:JavaScriptBlock>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep5" runat="server" StepType="Finish" Title="Finish">
                            <div class="stepContent">
                                <h3>
                                    Click "Done" to schedule the selected Sharepoint&reg; content for import.</h3>
                                <br />
                                <p>
                                    To see progress after import begins, refresh your browser window. Any import errors
                                    will be captured in the DxH Connection log.</p>
                            </div>
                        </asp:WizardStep>
                    </WizardSteps>
                    <StartNavigationTemplate>
                        <div class="stepNavigation ektron-ui-clearfix">
                            <div class="buttonForward">
                                <ektronUI:Button runat="server" CausesValidation="false" DisplayMode="Button" CommandName="MoveNext"
                                    Enabled="false" ID="uxStartButton" Text="Start" OnClick="uxSharePointWizard_StartButtonClick"
                                    CssClass="dxhPrimary" />
                                <ektronUI:Button ID="uxStartCancelButton" runat="server" CausesValidation="false"
                                    DisplayMode="Button" Text="Cancel" OnClick="uxButton_Cancel" />
                            </div>
                        </div>
                    </StartNavigationTemplate>
                    <StepNavigationTemplate>
                        <div class="stepNavigation ektron-ui-clearfix">
                            <div class="buttonBack">
                                <ektronUI:Button runat="server" CausesValidation="false" DisplayMode="Button" CommandName="MovePrevious"
                                    ID="uxBackButton" Text="Back" OnClick="uxBackButton_Click" />
                            </div>
                            <div class="buttonForward">
                                <ektronUI:Button runat="server" CausesValidation="false" DisplayMode="Button" CommandName="MoveNext"
                                    ID="uxNextButton" Text="Next" OnClick="uxNextButton_Click" CssClass="dxhPrimary" />
                                <ektronUI:Button ID="uxCancelButton" runat="server" CausesValidation="false" DisplayMode="Button"
                                    Text="Cancel" OnClick="uxButton_Cancel" Enabled="true" />
                            </div>
                        </div>
                    </StepNavigationTemplate>
                    <FinishNavigationTemplate>
                        <div class="stepNavigation">
                            <div class="buttonBack">
                                <ektronUI:Button runat="server" CausesValidation="false" DisplayMode="Button" CommandName="MovePrevious"
                                    ID="uxFinishBackButton" Text="Back" OnClick="uxBackButton_Click" />
                            </div>
                            <div class="buttonForward">
                                <ektronUI:Button runat="server" CausesValidation="false" DisplayMode="Button" CommandName="MoveComplete"
                                    ID="uxFinishButton" Text="Done" OnClick="uxFinishButton_Click" CssClass="dxhPrimary" />
                                <ektronUI:Button ID="uxFinishCancelButton" runat="server" CausesValidation="false"
                                    DisplayMode="Button" Text="Cancel" OnClick="uxButton_Cancel" />
                            </div>
                        </div>
                    </FinishNavigationTemplate>
                </asp:Wizard>
                <ektronUI:Dialog Modal="true" runat="server" ID="uxErrorModal" AutoOpen="false" Draggable="false">
                    <ContentTemplate>
                        <div id="connectionError" class="ektron-ui-clearfix">
                            <asp:Image ID="dxhErrorImage" runat="server" ToolTip="<% $Resources: dxhlogo %>" Visible="false"
                                ImageUrl="~/Workarea/DxH/images/dxh_logo_60x60.png" />
                            <h2 class="dxhErrorModal">
                                <asp:Literal ID="ltrDxHError" runat="server" Visible="true"></asp:Literal></h2>
                        </div>
                        <ektronUI:Message DisplayMode="Error" ID="uxErrorMessage" runat="server" Visible="true" />
                    </ContentTemplate>
                    <Buttons>
                        <ektronUI:DialogButton CloseDialog="true" OnClick="Error_Acknowledgement" ID="uxErrorButton"
                            Text="<%$ Resources:close %>" runat="server" />
                    </Buttons>
                </ektronUI:Dialog>
                <ektronUI:JavaScriptBlock runat="server" ClientIDMode="AutoID" ExecutionMode="OnEktronReady"
                    ID="uxPopUpWindowCheck">
                    <ScriptTemplate>
                        if("undefined" === typeof(PopUpWindow)) { PopUpWindow = function(url, hWind, nWidth,
                        nHeight, nScroll, nResize) { var cToolBar = "toolbar=0,location=0,directories=0,status="
                        + nResize + ",menubar=0,scrollbars=" + nScroll + ",resizable=" + nResize + ",width="
                        + nWidth + ",height=" + nHeight; var popupwin = window.open(url, hWind, cToolBar);
                        return popupwin; }; }
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <asp:HiddenField ID="ItemOrList" runat="server" Value="ByList" />
                <asp:HiddenField ID="uxSelectListGridDataDirtyFields" runat="server" />
                <asp:HiddenField ID="uxSelectItemsGridDataDirtyFields" runat="server" />
                <asp:HiddenField ID="uxSelectedItemsGridData" runat="server" />
                <asp:HiddenField ID="uxCurrentTabId" runat="server" Value="0" />
                <asp:HiddenField ID="uxMapList" runat="server" />
                <asp:HiddenField ID="uxConnectionName" runat="server" />
                <asp:HiddenField ID="DeleteContent" runat="server" />
                <asp:HiddenField ID="LanguageID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
