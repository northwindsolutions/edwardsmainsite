﻿namespace Ektron.Workarea.DxH
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using System.IO;
    using Ektron.Cms.Framework.Context;
    using Ektron.Cms;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms.Instrumentation;
    using Ektron.DxH.Client.Objects;
    using Ektron.DxH.Client.Sharepoint;
    using Ektron.DxH.Client;
    using Ektron.DxH.Common.Connectors;
    using System.Web.Script.Serialization;
    using Ektron.DxH.Common.Objects;
    using Ektron.Cms.Framework.Settings.DxH;
    using Ektron.Cms.Settings.DxH;
    using System.Web.Caching;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.UI;
    using Ektron.Newtonsoft.Json;
    using Ektron.DxH.Tasks;
    using System.Collections.Concurrent;
    using Ektron.Cms.Common;

    public partial class ImportSharepointContent : System.Web.UI.Page
    {
        #region members;

        #region const members
        private const string CLASS_NAME = "Ektron.Workarea.DxH.ImportSharepointContent";
        private const string BeginMsg = "-Begin {0}.{1}";
        private const string FinishMsg = "+Finish {0}.{1}";
        private const string ErrMsg = "{0}.{1} failed: {2}";
        private const string InfoMsg = "{0}.{1} Message: {2}";
        private long folderId = 0;

        #endregion

        private static readonly object _cacheobject = new object();
        private DateTime _cacheexptime = DateTime.MinValue;

        private bool MetaDataNotMapped { get; set; }
        private bool ErrorAcknowledged
        {
            get
            {
                var rtnVal = ViewState["ErrorAcknowledged"] as bool?;
                if (rtnVal == null)
                {
                    ViewState["ErrorAcknowledged"] = false;
                    return false;
                }
                else
                {
                    return (bool)rtnVal;
                }

            }
            set
            {
                ViewState["ErrorAcknowledged"] = value;
            }

        }

        private string EktronConnectionName
        {
            get
            {

                string rtnVal = HttpContext.Current.Cache["EktronConnectionName"] as string;
                if (rtnVal == null)
                {
                    lock (_cacheobject)
                    {
                        DxHUtils dxhUtils = new DxHUtils();
                        Connection conn = dxhUtils.GetDxhConnection("", "Ektron");
                        if (conn == null)
                            throw new NullReferenceException("The ektron connection could not be found.  Please check the DxH Connection settings for an inbound Ektron connection.");
                        rtnVal = conn.Name;
                        HttpContext.Current.Cache.Add("EktronConnectionName", conn.Name, null, CacheExpirationDateTime, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                    }

                }
                return rtnVal;

            }
        }

        private int _selectedlanguageid = 0;
        private int SelectedLanguageId
        {
            get
            {
                if (_selectedlanguageid == 0)
                    int.TryParse(LanguageID.Value, out  _selectedlanguageid);
                return _selectedlanguageid;
            }
        }


        private FolderManager FolderManager = new FolderManager();
        private ContextBusClient ContextBusClient = new ContextBusClient();
        private ConnectionManagerClient ConnectionManager = new ConnectionManagerClient();

        private DateTime CacheExpirationDateTime
        {
            get
            {
                if (_cacheexptime == DateTime.MinValue)
                {
                    string cacheinterval = System.Configuration.ConfigurationManager.AppSettings["ek_dxh_sharepoint_cache"].ToString();
                    if (string.IsNullOrEmpty(cacheinterval))
                        _cacheexptime = DateTime.Now.AddSeconds(900);
                    else
                        _cacheexptime = DateTime.Now.AddSeconds(Convert.ToInt32(cacheinterval));

                }
                return _cacheexptime;
            }
        }

        private SharepointClient _SharePointClient;
        private SharepointClient SharepointClient
        {
            get
            {
                if (this._SharePointClient == null)
                {
                    this._SharePointClient = new SharepointClient();
                    this.ContextBusClient = new Ektron.DxH.Client.ContextBusClient();

                    var connId = (!string.IsNullOrEmpty(uxConnectionList.sharePointConnectionId) ? uxConnectionList.sharePointConnectionId :
                        (!string.IsNullOrEmpty(this.uxConnectionName.Value) ? this.uxConnectionName.Value : string.Empty));

                    if (string.IsNullOrEmpty(connId))
                        throw new NullReferenceException("Connection Name. The sharepoint client cannot log in.");
                    // login:
                    List<ConnectionParam> connectionParams = this.ConnectionManager.LoadConnection(uxConnectionList.sharePointConnectionId, "SharePoint").ToList();
                    if (connectionParams == null)
                        throw new NullReferenceException("SharePoint Connection Parameters for Login");

                    _SharePointClient.Login(uxConnectionList.sharePointConnectionId);
                }
                return this._SharePointClient;
            }
        }



        private List<MappingTask> DxHMaps
        {
            get
            {
                //trying to avoid viewsate but can't serialize the mapping task.
                //var rtnVal = JsonConvert.DeserializeObject<List<MappingTask>>(this.uxMapList.Value);
                //return rtnVal != null ? rtnVal : new List<MappingTask>();

                var rtnVal = ViewState["DxhMaps"] as List<MappingTask>;
                if (rtnVal == null)
                {
                    rtnVal = new List<MappingTask>();
                    ViewState["DxhMaps"] = rtnVal;
                }
                return rtnVal;

            }
            set
            {
                //this.uxMapList.Value = JsonConvert.SerializeObject(value);

                ViewState["DxhMaps"] = value;
            }
        }

        private string _SelectedConnectionName;
        private string SelectedConnectionName
        {
            get
            {
                if (string.IsNullOrEmpty(_SelectedConnectionName))
                {
                    _SelectedConnectionName = !String.IsNullOrEmpty((string)ViewState["ConnName"]) ? (string)ViewState["ConnName"] : String.Empty;
                    if (String.IsNullOrEmpty(_SelectedConnectionName))
                    {
                        _SelectedConnectionName = Request.QueryString["ConnName"];

                        if (string.IsNullOrEmpty(_SelectedConnectionName))
                            throw new ArgumentNullException("Connection Name cannot be empty.  QueryString 'ConnName' must identify a valid connection name.'");

                        ViewState["ConnName"] = _SelectedConnectionName;
                    }
                }
                return _SelectedConnectionName;
            }

        }

        private long m_intId
        {

            get
            {
                return folderId = !(Request.QueryString["id"] == null) ? Convert.ToInt64(Request.QueryString["id"]) : 0;
            }
        }
        #endregion
        #region private properties
        private FolderData _FolderData;
        private FolderData FolderData
        {
            get
            {
                if (_FolderData == null)
                {
                    try
                    {
                        if (SelectedLanguageId > 0)
                            FolderManager.RequestInformation.ContentLanguage = SelectedLanguageId;
                        _FolderData = FolderManager.GetItem((Request.QueryString["id"] != null) ? Convert.ToInt64(Request.QueryString["id"]) : 0, true);
                        if (Request.QueryString["id"] == null)
                            DisplayWarning("Folder Data for folder 0 was retrieved as the querystring value was not present. Is this correct?");
                    }
                    catch (Exception ex)
                    {
                        Log.WriteError(string.Format(ErrMsg, CLASS_NAME, "GetGetFolderData Property Method", ex.Message));
                        throw;
                    }
                }
                return _FolderData;
            }

        }


        #endregion

        #region page events

        protected override void OnInit(EventArgs e)
        {
            this.uxErrorModal.Visible = false;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            RegisterResources();
            SubscribeToErrorEvents();
            SubscribeToWarningEvents();
            LoadToolBar();
            this.theMappingUtil.OnAfterMapCreated += (o, ev) =>
            {
                ContentMetaData[] folderMeta;
                if (this.FolderData.IsMetaInherited)
                {
                    folderMeta = FolderManager.GetItem(FolderData.MetaInheritedFrom, true).FolderMetadata;
                }
                else
                {
                    folderMeta = FolderData.FolderMetadata;
                }


                if (folderMeta == null)
                    Log.WriteWarning(string.Format("No folder metadata was returned for folder {0}.", this.FolderData.Id.ToString()));

                if (folderMeta != null && folderMeta.Any(meta => meta.Required) && ev.NotAssignedFieldDefinitionIds.Count > 0)
                {
                    foreach (var metadata in folderMeta)
                    {

                        if (ev.NotAssignedFieldDefinitionIds.Any(id => id.ToLower().Trim().Contains("metadata") && id.ToLower().Trim().Contains(metadata.Id.ToString().ToLower().Trim()) && metadata.Required))
                        {
                            if (!this.ErrorAcknowledged)
                            {
                                DisplayWarning(GetLocalResourceObject("requiredMetaWarning").ToString());
                                this.MetaDataNotMapped = true;
                            }
                            else
                            {
                                this.MetaDataNotMapped = false;
                            }
                        }
                    }
                }
            };

            this.uxSharePointWizard.NextButtonClick += (o, ev) =>
            {
                if (this.MetaDataNotMapped)
                {
                    ev.Cancel = true;
                }
            };



        }


        private void LoadToolBar()
        {
            StyleHelper m_refStyle = new StyleHelper();
            string backurl = "action=" + HttpUtility.UrlDecode(Request.QueryString["back_action"]) + "&id=" + HttpUtility.UrlDecode(Request.QueryString["back_id"]) + "&LangType=" + HttpUtility.UrlDecode(Request.QueryString["back_LangType"]) + "&form_id=" + HttpUtility.UrlDecode(Request.QueryString["back_form_id"]);
            ltrBackButton.Text = m_refStyle.GetButtonEventsWCaption(CmsContextService.Current.WorkareaPath + "/images/UI/Icons/cancel.png", CmsContextService.Current.WorkareaPath + "/content.aspx?action=ViewContentByCategory&id=" + this.FolderData.Id.ToString(), string.Empty, new ContentAPI().EkMsgRef.GetMessage("btn cancel"), "", StyleHelper.CancelButtonCssClass, true);
        }

        private void SubscribeToErrorEvents()
        {
            this.uxImportSPGridTreeList.OnError += (o, ev) =>
            {
                DisplayError(ev.ex.Message);
            };
            this.theMappingUtil.OnError += (o, ev) =>
            {
                DisplayError(ev.ex.Message);
            };
            this.uxConnectionList.OnError += (o, ev) =>
            {
                DisplayError(ev.ex.Message);
            };
            this.theMappingUtil.OnError += (o, ev) =>
            {
                DisplayError(ev.ex.Message);
            };

        }

        private void SubscribeToWarningEvents()
        {
            this.theMappingUtil.OnWarning += (o, ev) =>
            {
                if (!this.ErrorAcknowledged)
                    DisplayWarning(ev.ex.Message);
            };

        }
        protected override void OnLoadComplete(EventArgs e)
        {

            if (uxConnectionList.IsVerified)
            {
                Ektron.Cms.Framework.UI.Controls.EktronUI.Button startButton = (Ektron.Cms.Framework.UI.Controls.EktronUI.Button)uxSharePointWizard.FindControl("StartNavigationTemplateContainerID$uxStartButton");
                startButton.Enabled = true;
            }
            base.OnLoadComplete(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            SetupWizard();
            base.OnPreRender(e);

        }

        #endregion
        #region protected event voids

        protected void Error_Acknowledgement(object sender, EventArgs e)
        {
            ErrorAcknowledged = true;
            DataBindSelectionStep();
        }

        protected void uxSharePointWizard_StartButtonClick(object sender, EventArgs e)
        {
            ViewState["ConnName"] = uxConnectionList.sharePointConnectionId;
        }

        protected void
            SharepointItemCommand(object sender, CommandEventArgs e)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "SharepointItemCommand"));


            var theObjectDefinitionId = (string)e.CommandArgument;
            if (string.IsNullOrEmpty(theObjectDefinitionId))
                throw new NullReferenceException("ObjectDefinition ID");

            theMappingUtil.FolderId = this.FolderData.Id;
            theMappingUtil.DataStoreLocations = ObjectStoreLocation.UseViewState;
            theMappingUtil.TargetStoreName = (string)e.CommandArgument + "targetStoreName";
            theMappingUtil.SourceStoreName = (string)e.CommandArgument + "sourceStoreName";
            theMappingUtil.MappingStoreName = (string)e.CommandArgument + "mappingStoreName";

            var SourceObjectDefinition =
                   (ViewState["SelectedObjectDefinition" + theObjectDefinitionId] as ObjectDefinition) != null ?
                            (ViewState["SelectedObjectDefinition" + theObjectDefinitionId] as ObjectDefinition) :
                             null;

            if (SourceObjectDefinition == null)
            {

                SourceObjectDefinition =
                   SharepointClient.GetObjectDefinition(theObjectDefinitionId, uxConnectionList.sharePointConnectionId);
                //save this to viewstate as it is dynamic and we can't have a declarative control for it.
                ViewState["SelectedObjectDefinition" + theObjectDefinitionId] = SourceObjectDefinition;
            }

            if (SourceObjectDefinition == null)
                throw new NullReferenceException("The Source Object Definition for Mapping.");

            theMappingUtil.SourceDefinition = SourceObjectDefinition;

            theMappingUtil.TargetDefinition = this.GetEktronSharepointSmartFormObjectDefinition(SourceObjectDefinition);

            //theMappingUtil.DataBind();
            theMappingUtil.DataBindGridViewModel();


            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "SharepointItemCommand"));
        }

        protected void GenerateAllMaps()
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "GenerateAllMaps"));
            try
            {
                foreach (var selectedItem in this.uxImportSPGridTreeList.SelectedItemsGridData.OrderBy(item => item.List.ToLowerInvariant()))
                {
                    this.SharepointItemCommand(null, new CommandEventArgs(string.Empty, selectedItem.ObjectDefinitionId));
                    GenerateMap(null, new EventArgs());

                }

            }
            catch (Exception ex)
            {
                Log.WriteError(string.Format(ErrMsg, CLASS_NAME, "GenerateAllMaps", ex.Message));
                throw;
            }


            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "GenerateAllMaps"));
        }

        protected void GenerateMap(object sender, EventArgs e)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "GenerateMap"));
            var map = theMappingUtil.CreateMap();

            var match = DxHMaps.Find(theMap => theMap.SourceObject.Id == map.SourceObject.Id);

            if (match != null)
            {
                match = map;
            }
            else
            {
                var maps = DxHMaps;
                maps.Add(map);
                DxHMaps = maps;
            }
            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "GenerateMap"));
        }



        protected void uxNextButton_Click(object sender, EventArgs e)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "uxNextButton_Click"));
            DataBindSelectionStep();
            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "uxNextButton_Click"));
        }

        protected void uxBackButton_Click(object sender, EventArgs e)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "uxBackButton_Click"));
            DataBindSelectionStep(false);
            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "uxBackButton_Click"));
        }



        protected void uxButton_Cancel(object sender, EventArgs e)
        {
            try
            {
                this.SharepointClient.Logout();
            }
            catch (Exception ex)
            {
                //this catch is for if there are no connections to DxH or to sharepoint.
            }
            Response.Redirect(CmsContextService.Current.WorkareaPath + "/content.aspx?action=ViewContentByCategory&id=" + this.FolderData.Id.ToString());
        }

        protected void uxFinishButton_Click(object sender, EventArgs e)
        {

            Ektron.Cms.UserAPI uapi = new UserAPI();
            if (this.DxHMaps == null || this.DxHMaps.Count == 0)
            {
                DisplayError("There are no mappings to map.  Were any items selected for import?");
                return;
            }

            bool DeleteFiles = false;
            bool.TryParse(this.DeleteContent.Value, out DeleteFiles);
            int CmsLanguage = uapi.RequestInformationRef.ContentLanguage;
            int.TryParse(this.LanguageID.Value, out CmsLanguage);

            if (this.uxImportSPGridTreeList.ByList())
            {
                this.SharepointClient.ImportListsToFolder(
                    this.EktronConnectionName,
                    uxConnectionList.sharePointConnectionId,
                    this.FolderData.Id,
                    DxHMaps,
                    DeleteFiles,
                    CmsLanguage
                    );
            }

            if (!this.uxImportSPGridTreeList.ByList())
            {

                var groupedItemLists = this.uxImportSPGridTreeList.SelectedItemsGridData
                    .GroupBy(dataObj => dataObj.List.ToLowerInvariant())
                    .Select(groups => new { ListName = groups.Key, Items = groups.ToList() })
                    .OrderBy(group => group.ListName.ToLowerInvariant());
                foreach (var item in groupedItemLists)
                {
                    ConcurrentDictionary<int, MappingTask> listIdAndMappingTasks = new ConcurrentDictionary<int, MappingTask>();

                    foreach (var mappedItem in item.Items)
                    {
                        var newVal = DxHMaps.First(map => map.SourceObject.Id.ToLowerInvariant() == mappedItem.ObjectDefinitionId.ToLowerInvariant());
                        listIdAndMappingTasks.AddOrUpdate(
                                Convert.ToInt32(mappedItem.Id.Split('-').Last()),
                                newVal,
                                (key, oldvalue) => oldvalue = newVal
                            );
                    }

                    this.SharepointClient.ImportItemsToFolderWithOutListDependency(
                            this.EktronConnectionName,
                            uxConnectionList.sharePointConnectionId,
                            item.Items.First().ObjectDefinitionId,
                            this.FolderData.Id,
                            listIdAndMappingTasks,
                            DeleteFiles,
                            CmsLanguage
                            );

                }

            }


            Response.Redirect(CmsContextService.Current.WorkareaPath + "/content.aspx?action=ViewContentByCategory&id=" + this.FolderData.Id.ToString());


        }
        protected void uxSharePointWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "uxSharePointWizard_ActiveStepChanged"));

            var wizard = (sender as System.Web.UI.WebControls.Wizard);
            if (wizard == null)
            {
                Log.WriteError(string.Format(ErrMsg, CLASS_NAME, "uxSharePointWizard_ActiveStepChanged", "The wizard control (sender) is null or could not be casted as Type Wizard"));
                throw new NullReferenceException("wizard");
            }

            if (wizard.ActiveStep.Title.Trim().ToLower() == "selection")
                this.uxImportSPGridTreeList.DataBindAll();


            if (wizard.ActiveStep.Title.Trim().ToLower() == "item or list")
                this.uxImportSPGridTreeList.DataBindAll();

            var nextButton = this.uxSharePointWizard.FindControl("StepNavigationTemplateContainerID$uxNextButton") as Ektron.Cms.Framework.UI.Controls.EktronUI.Button;
            if (nextButton != null)
            {
                if (!string.IsNullOrEmpty(this.ItemOrList.Value) && !string.IsNullOrEmpty(this.DeleteContent.Value))
                    nextButton.Enabled = true;
                else
                    nextButton.Enabled = false;
            }

            //set everything back to defaults
            if (wizard.ActiveStepIndex == 0 || wizard.ActiveStepIndex == 1)
            {


                if (this.uxImportSPGridTreeList != null)
                {
                    this.uxImportSPGridTreeList.SelectedItemsGridData = new List<DataListObject>();
                    this.uxImportSPGridTreeList.SelectListDataDirtyFields = new List<GridViewDirtyField>();
                    this.uxImportSPGridTreeList.SelectItemsGridDirtyFields = new List<GridViewDirtyField>();
                    this.uxImportSPGridTreeList.Clear();
                    this.uxSelectedItemsGridData.Value = null;
                    this.uxSelectListGridDataDirtyFields.Value = null;
                    this.uxSelectItemsGridDataDirtyFields.Value = null;

                }
                this.ItemOrList.Value = "ByList";
                this.DeleteContent.Value = "";
                this.DxHMaps = new List<MappingTask>();
            }

            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "uxSharePointWizard_ActiveStepChanged"));
        }

        #endregion

        #region private methods

        private void DisplayError(string errorMessage)
        {

            this.uxErrorMessage.Text = errorMessage;
            this.uxErrorMessage.Visible = true;
            this.uxErrorModal.Visible = true;
            this.uxErrorModal.AutoOpen = true;
            this.ltrDxHError.Visible = true;
            this.dxhErrorImage.Visible = true;
        }

        private void DisplayWarning(string warningMessage)
        {
            this.uxErrorMessage.DisplayMode = Message.DisplayModes.Warning;
            this.uxErrorMessage.Text = warningMessage;
            this.uxErrorMessage.Visible = true;
            this.uxErrorModal.Visible = true;
            this.uxErrorModal.AutoOpen = true;
            this.ltrDxHError.Visible = true;
            this.dxhErrorImage.Visible = true;
        }

        private ObjectDefinition GetEktronSharepointSmartFormObjectDefinition(ObjectDefinition sharePointItem)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "GetEktronSharepointSmartFormObjectDefinition()"));
            ObjectDefinition rtnVal = HttpContext.Current.Cache["EktronSharepointSmartFormObjectDefinition" + sharePointItem.Id] as ObjectDefinition;
            if (rtnVal == null)
            {
                lock (_cacheobject)
                {
                    Log.WriteVerbose(string.Format(InfoMsg, CLASS_NAME, "GetEktronSharepointSmartFormObjectDefinition", "Begin  SharePointClient.CreateSmartFormObjectDefintion"));
                    rtnVal = this.SharepointClient.CreateSmartFormObjectDefinition(sharePointItem, SelectedLanguageId);
                    Log.WriteVerbose(string.Format(InfoMsg, CLASS_NAME, "GetEktronSharepointSmartFormObjectDefinition", "Finish  SharePointClient.CreateSmartFormObjectDefintion"));
                    HttpContext.Current.Cache.Add("EktronTargetObjectDefinition", rtnVal, null, CacheExpirationDateTime, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                }
            }
            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "GetEktronSharepointSmartFormObjectDefinition"));
            return rtnVal;
        }

        private void DataBindSelectionStep(bool SteppingForward = true)
        {
            Log.WriteVerbose(string.Format(BeginMsg, CLASS_NAME, "DataBindSelectionStep"));
            try
            {
                var wizard = Page.FindControl("uxSharePointWizard") as System.Web.UI.WebControls.Wizard;
                if (wizard == null)
                    throw new NullReferenceException("Wizard");


                if (wizard.ActiveStepIndex > 1)
                {

                    var GridTreeControl = wizard.FindControl("uxImportSPGridTreeList") as ImportSPGridTreeList;

                    if (GridTreeControl == null)
                        throw new NullReferenceException("uxImportSPGridTreeList");


                    var items = GridTreeControl.SelectedItemsGridData;
                    if (items == null)
                        throw new NullReferenceException("SelectedItemsGridData (SelectedItems)");

                    DatabinduxSelectedItemsRepeater(items);

                    //fill the grid
                    if (!string.IsNullOrEmpty(items.FirstOrDefault().ObjectDefinitionId))
                        this.SharepointItemCommand(null, new CommandEventArgs(string.Empty, items.GroupBy(objDef => objDef.List).OrderBy(grp => grp.Key).First().First().ObjectDefinitionId));
                }

                if (wizard.ActiveStepIndex == wizard.WizardSteps.Count - 2 && SteppingForward)
                {
                    GenerateAllMaps();
                }

            }
            catch (Exception ex)
            {
                Log.WriteError(string.Format(ErrMsg, CLASS_NAME, "DataBindSelectionStep", ex.Message));
                throw;
            }
            Log.WriteVerbose(string.Format(FinishMsg, CLASS_NAME, "DataBindSelectionStep"));
        }

        private void DatabinduxSelectedItemsRepeater(List<DataListObject> items)
        {

            this.uxSelectedItemsRepeater.DataSource = items.GroupBy(objDef => objDef.List).OrderBy(grp => grp.Key).Select(grp => grp.First());
            this.uxSelectedItemsRepeater.DataBind();

            this.uxSelectedItemsRepeaterDivs.DataSource = items.GroupBy(objDef => objDef.List).OrderBy(grp => grp.Key).Select(grp => grp.First());
            this.uxSelectedItemsRepeaterDivs.DataBind();



        }

        private string EncodeDelimiters(string text)
        {
            if (string.IsNullOrEmpty(text)) { return text; }
            return ((text.Replace('|', '\u0011')).Replace('^', '\u0012')).Replace('_', '\u0013');
        }

        private string DecodeDelimiters(string text)
        {
            if (string.IsNullOrEmpty(text)) { return text; }
            return ((text.Replace('\u0011', '|')).Replace('\u0012', '^')).Replace('\u0013', '_');
        }

        private void SetupWizard()
        {
            var headerTemplate = this.uxSharePointWizard.FindControl("HeaderContainer");
            if (headerTemplate == null)
                throw new NullReferenceException("Could not find Wizard Header Template");

            var ltrTitle = headerTemplate.FindControl("ltrTitle") as Literal;
            if (ltrTitle == null)
                throw new NullReferenceException("Could not find Title Literal");


            ltrHelp.Text = new StyleHelper().GetHelpButton("import_sp", String.Empty);

            this.ltrMapText.Text = string.Format(GetLocalResourceObject("ltrMapText").ToString(), "<span class='ektron-ui-required'>", "</span>");


            switch (this.uxSharePointWizard.ActiveStep.Title.ToLower())
            {
                case "select connection":
                    ltrTitle.Text = GetLocalResourceObject("SelectConnection").ToString();
                    break;
                case "item or list":
                    ltrTitle.Text = string.Format(GetLocalResourceObject("ltrTitle").ToString(), this.FolderData.Name.ToString());
                    break;
                case "selection":
                    if (this.uxImportSPGridTreeList.ByList())
                    {
                        ltrTitle.Text = GetLocalResourceObject("SelectListsOrLibraries").ToString();
                    }
                    else
                    {
                        ltrTitle.Text = GetLocalResourceObject("SelectItems").ToString();
                    }
                    break;
                case "mapping":
                    ltrTitle.Text = GetLocalResourceObject("MapMetaData").ToString();
                    break;
                case "finish":
                    ltrTitle.Text = GetLocalResourceObject("ScheduleImport").ToString();
                    break;
            }

            ltrPageTitle.Text = string.Format(GetLocalResourceObject("PageTitle").ToString(), this.FolderData.Name);

            this.ltrDxHError.Text = GetLocalResourceObject("ltrDxHError").ToString();

            theMappingUtil.FolderId = this.FolderData.Id;

        }

        private void RegisterResources()
        {
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.Framework.UI.Css.Create(CmsContextService.Current.WorkareaPath + "/csslib/ektron.fixedPositionToolbar.css").Register(this);
            Packages.jQuery.jQueryUI.Tabs.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Ektron.Cms.Framework.UI.Css.Register(this, CmsContextService.Current.WorkareaPath + "/dxh/css/Ektron.Workarea.Dxh.ImportSharepoint.css");
            Ektron.Cms.Framework.UI.JavaScript.Register(this, CmsContextService.Current.WorkareaPath + "/DxH/js/Ektron.Workarea.DxH.ImportSharePoint.js");
            Ektron.Cms.Framework.UI.JavaScript.Register(this, CmsContextService.Current.WorkareaPath + "/java/jfunct.js");
        }
        #endregion

    }
}
