﻿namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Common;
    using Ektron.Cms.Core;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Settings.DxH;
    using Ektron.DxH.Client;
    using Ektron.DxH.Common.Connectors;
    using Ektron.DxH.Common.Objects;

    public partial class ViewDXHConnections : Ektron.Cms.Workarea.Page
    {
        /*Class fields*/
        private ContextBusClient cbClient = null;
        private ConnectionManagerClient ConnectionManager;
        //private DxHConnectionManager dxhMgr = null;
        //private DxHConnectionCriteria dxCriteria = null;
        DxHUtils dxhUtils;
        int itemsPerPage = 10;
        Ektron.Cms.PagingInfo GridViewPagerInfo = null;

        public bool m_blnRefreshFrame
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["refreshMenu"]))
                {
                    if (Request.QueryString["refreshMenu"].ToLower() == "true")
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        private bool ConnectionAvaliable = false;
        private int TotalPages
        {
            get
            {
                if (ViewState["VW_pager_totalPage"] != null)
                {
                    return (int)ViewState["VW_pager_totalPage"];
                }
                return 0;
            }
            set
            {
                ViewState["VW_pager_totalPage"] = value;
            }
        }
        private int CurrentPageIndex
        {
            get
            {
                if (ViewState["VW_pager_CurrentPageIndex"] != null)
                {
                    return (int)ViewState["VW_pager_CurrentPageIndex"];
                }
                return 0;
            }
            set
            {
                ViewState["VW_pager_CurrentPageIndex"] = value;
            }
        }
        List<ConnectorFlyWeight> adps = null;
        private List<DxHConnectionData> dxhConnectionData = null;
        /// <summary>
        /// Private StyleHelper reference
        /// </summary>
        private StyleHelper refStyle = new StyleHelper();
        //Page Events
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Ektron.Cms.SiteAPI _siteApi = new Ektron.Cms.SiteAPI();
            if (!Utilities.ValidateUserLogin())
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            if (!_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) &&
                !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser) && !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.DxHConnectionAdmin))
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }

            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Package MenuCss = new Package()
            {
                Components = new List<Component>()
                {
                    
                    // CSS
                    Css.Create(ResolveUrl("~/workarea/wamenu/css/com.ektron.ui.menu.css")),
                }
            };
            MenuCss.Register(this);
            cbClient = new ContextBusClient();
            ConnectionManager = new ConnectionManagerClient();
            dxhUtils = new DxHUtils();

            ConnectionAvaliable = cbClient.TestDxhConnection(cbClient.ServiceUrl);
            if (!string.IsNullOrEmpty(Request.QueryString["AdapterName"]))
            {

                string AdapterName = Request.QueryString["AdapterName"];
                string ConnectionName = Request.QueryString["ConnectionName"];
                string controlPath = "~/Workarea/DxH/Adapter_AddEditConn.ascx";
                Control addeditCtrl = LoadControl(controlPath);
                addeditCtrl.GetType().GetProperty("AdapterName").SetValue(addeditCtrl, AdapterName, null);
                if (!string.IsNullOrEmpty(ConnectionName))
                    addeditCtrl.GetType().GetProperty("ConnectionName").SetValue(addeditCtrl, ConnectionName, null);

                addeditCtrl.ID = "Adapter_AddEditConn_" + AdapterName;
                //addeditCtrl
                this.pnlCtrlHolder.Controls.Add(addeditCtrl);
            }

            EkGrid.RowDataBound += new GridViewRowEventHandler(EkGrid_RowDataBound);

            if(ConnectionAvaliable) BuildMenu();

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ltrConnURL.Text = cbClient.ServiceUrl;
            if (!ConnectionAvaliable)
            {
                //divNoDxHMask.Visible = true;
                trDXHConnection.Visible = false;
                msgNoDxHConn.Visible = true;
                msgNoDxHConn.Text = string.Format(GetLocalResourceObject("NoDxHConn").ToString(), cbClient.ServiceUrl);
                trError.Visible = true;
                uxInformation.Visible = false;
            }
            else
            {
                uxInformation.Visible = false;
                divNoDxHMask.Visible = false;
                divDxHURL.Visible = true;
                msgNoDxHConn.Visible = false;
                trError.Visible = false;
                if (!Page.IsPostBack)
                {
                    dxhConnectionData = dxhUtils.GetDxHConnectionList();
                    AddInboundConnection(dxhConnectionData);
                    EkGrid.DataSource = dxhConnectionData;
                    EkGrid.EktronUIPagingInfo = new Ektron.Cms.PagingInfo(itemsPerPage);
                    EkGrid.EktronUIPagingInfo.TotalRecords = dxhConnectionData.Count;
                    EkGrid.EktronUIOrderByFieldText = "ConnectionName";
                }
            }
        }

        protected void AddInboundConnection(List<DxHConnectionData> dxhConnections)
        {
            SiteSetting settings = new SiteSetting();
            SiteSettingData settingData = settings.GetItem((long)EkEnumeration.SiteSetting.DxHInboundConnection);
            if (settingData.Value != null)
            {
                var exists = dxhConnections.Find(x => x.ConnectionName == settingData.Value);
                if (exists == null)
                {
                    DxHUtils dxhUtils = new DxHUtils();
                    Connection data = dxhUtils.GetDxhConnection(settingData.Value, "");
                    if (data != null)
                    {
                        dxhConnections.Insert(0, new DxHConnectionData() { ConnectionName = data.Name, AdapterName = data.ConnectorName });
                    }
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            EkGrid.DataBind();
        }

        //Control Events
        private void EkGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DxHConnectionData data = (DxHConnectionData)e.Row.DataItem;

                Menu mnu = (Menu)e.Row.Cells[0].FindControl("itemCM");
                MenuItem nameItem = new MenuItem();
                nameItem.Text = data.ConnectionName;
                nameItem.Selectable = true;
                nameItem.NavigateUrl = "#";
                if (string.Compare(data.ConnectionName, "default", StringComparison.OrdinalIgnoreCase) != 0)
                {
                    MenuItem delItem = new MenuItem();
                    delItem.Text = "Delete";
                    //delItem.Value = data.AdapterName + "|'|" + data.ConnectionName;
                    delItem.NavigateUrl = string.Format("javascript:DeleteConfirm('{0}','{1}',{2});", data.ConnectionName, data.AdapterName, data.Id);
                    nameItem.ChildItems.Add(delItem);
                    diagDelConfirm.Title = "\"" + string.Format(GetLocalResourceObject("strDelConfirm_Dlg_Title").ToString(), getAdapterDisplayName(data.AdapterName)) + "\"";//.Replace("Confirm", ltrDeleteHeader_2.Text);

                    if (ConnectionAvaliable)
                    {
                        MenuItem editItem = new MenuItem();
                        editItem.Text = "Edit";
                        editItem.NavigateUrl = string.Format("ViewDXHConnections.aspx?AdapterName={0}&ConnectionName={1}", data.AdapterName, data.ConnectionName);
                        nameItem.ChildItems.Add(editItem);
                    }
                }

                mnu.Items.Add(nameItem);


            }
        }
        protected void mnu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string s = e.Item.Value;
        }
        protected void btnDelConfim_Click(object sender, EventArgs e)
        {
            this.pnlCtrlHolder.Controls.Clear();
            try
            {
                string connName = Request.Form["ekHid_hidConnName"];
                string adpName = Request.Form["ekHid_hidAdpName"];

                if (ConnectionAvaliable)
                {
                    ConnectionManager.Delete(
                    new Connection()
                    {
                        Name = connName,
                        ConnectorName = adpName
                    });
                }
                //else
                //{
                //    string connId = Request.Form["ekHid_hidConnId"];
                //    if (!string.IsNullOrEmpty(connId))
                //    {
                //        var id = long.Parse(connId);
                //        if (id > 0)

                //            dxhMgr.Delete(id);
                //    }
                //}

                //dxhConnectionData = dxhMgr.GetList(dxCriteria);
                ////Remove default DxH Connection from the grid.
                //dxhConnectionData.Remove(dxhConnectionData[dxhConnectionData.Count - 1]);
                //EkGrid.DataSource = dxhConnectionData;
                //EkGrid.EktronUIPagingInfo = this.dxCriteria.PagingInfo;
                //EkGrid.EktronUIOrderByFieldText = this.dxCriteria.OrderByField.ToString();

                dxhConnectionData = dxhUtils.GetDxHConnectionList();

                EkGrid.DataSource = dxhConnectionData;
                EkGrid.EktronUIPagingInfo = new Ektron.Cms.PagingInfo(itemsPerPage);
                EkGrid.EktronUIPagingInfo.TotalRecords = dxhConnectionData.Count;
            }
            catch (Exception ex)
            {
                ltrExcText.Text = ex.Message + "<br/>" + ex.StackTrace;
                diagException.AutoOpen = true;
            }

        }
        protected void EkGrid_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            //EkGrid.EktronUIPagingInfo = dxCriteria.PagingInfo = e.PagingInfo;
            EkGrid.EktronUIPagingInfo = e.PagingInfo;
            dxhConnectionData = dxhUtils.GetDxHConnectionList();
            dxhConnectionData.Remove(dxhConnectionData[dxhConnectionData.Count - 1]);
            EkGrid.DataSource = dxhConnectionData;
        }
        protected void EkGrid_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            //update sorting info

        }
        protected void RetryClick_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewDXHConnections.aspx", true);
        }

        //Misc. Functions
        //public string getAdapterDisplayName(string adapterName)
        //{
        //    if (GetLocalResourceObject("AdapterName_" + adapterName) != null)
        //        return GetLocalResourceObject("AdapterName_" + adapterName).ToString();
        //    else
        //        return adapterName;
        //}
        private void BuildMenu()
        {
            string buttonId = Guid.NewGuid().ToString();
            string buttonId2 = Guid.NewGuid().ToString();
            System.Text.StringBuilder result = new System.Text.StringBuilder();

            result.Append("<table><tr>" + "\r\n");


            result.Append("<td class=\"menuRootItem\" onclick=\"MenuUtil.use(event, \'file\', \'" + buttonId + "\');\" onmouseover=\"this.className=\'menuRootItemSelected\';MenuUtil.use(event, \'file\', \'" + buttonId + "\');\" onmouseout=\"this.className=\'menuRootItem\'\"><span id=\"" + buttonId + "\" class=\"new\">" + GetLocalResourceObject("btnAddButton").ToString() + "</span></td>");
            if (ConnectionAvaliable)
                result.Append("<td class=\"menuRootItem\" onmouseover=\"this.className=\'menuRootItemSelected\';MenuUtil.hide();\" onmouseout=\"this.className=\'menuRootItem\'\"><span id=\"" + buttonId2 + "\" class=\"new\">" + "<a href='ErrorLogView.aspx'>" + GetLocalResourceObject("btnViewErrorLog").ToString() + "</a>" + "</span></td>");
            result.Append(StyleHelper.ActionBarDivider);
            result.Append("<td>" + this.refStyle.GetHelpButton("dxh_view", string.Empty) + "</td>");

            result.Append("</tr></table>");

            result.Append("<script type=\"text/javascript\">" + Environment.NewLine);
            result.Append("    var filemenu = new Menu( \"file\" );" + Environment.NewLine);
            try
            {
                adps = cbClient.GetRegisteredAdapterList();

                foreach (ConnectorFlyWeight adp in adps.OrderBy(x => x.DisplayName))
                {
                    //if (adp.Id == "Ektron")
                    //{
                    //    if (isInboundConnExists(adp.Id))
                    //        continue;
                    //}
                    result.Append("    filemenu.addItem(\"&nbsp;&nbsp;&nbsp;" + string.Format(GetLocalResourceObject("MenuItemFmt").ToString(), adp.DisplayName) + "\", function() { window.location.href = \'ViewDXHConnections.aspx?AdapterName=" + adp.Id + "\' } );" + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                result.Append("    filemenu.addItem(\"&nbsp;&nbsp;&nbsp;Connect to DxH\", function() { window.location.href = \'DxHSetup.aspx\' } );" + Environment.NewLine);
            }


            result.Append("    MenuUtil.add( filemenu );" + Environment.NewLine);
            result.Append("    </script>" + Environment.NewLine);
            result.Append("" + Environment.NewLine);



            ltrMenuHtml.Text = result.ToString();
        }

        public string getAdapterDisplayName(string adapterId)
        {
            if (adps == null)
                return adapterId;
            try
            {
                return adps.Find(x => x.Id == adapterId).DisplayName;
            }
            catch (Exception ex)
            {
                return adapterId;
            }
        }

        public bool isInboundConnExists(string inBoundStr)
        {
 
            Connection conn = dxhUtils.GetDxhConnection("", inBoundStr);
            return (conn != null) ? true : false;
        }
    }

}