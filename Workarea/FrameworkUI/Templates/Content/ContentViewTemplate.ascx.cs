﻿using System;
using Ektron.Cms.Framework.UI.Controllers;
using Ektron.Cms.Framework.UI.Controls.MVC;
using Ektron.Cms.Framework.UI.MVC;
using Ektron.Cms.Framework.UI.Models;
using Ektron.Cms;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.UI;
using System.Web.UI;
using System.Reflection;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

public partial class FrameworkUI_Templates_Content_ContentViewTemplate : BaseTemplate<IContentController, ContentModel>, IItemTemplate<ContentModel>
{
	public event ModelHandler<ContentModel> ModelChangedEvent;


    protected override void OnInit(EventArgs e)
	{
        Ektron.Cms.Framework.UI.Css.Register(Page, Ektron.Cms.Framework.UI.Css.UIPath + "/css/Ektron/Controls/ektron-ui-Content-item.css");
        Ektron.Cms.Framework.UI.Packages.EktronCoreJS.Register(this);

        base.OnInit(e);
	}

}
