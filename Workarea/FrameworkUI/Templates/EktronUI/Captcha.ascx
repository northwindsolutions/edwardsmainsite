﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Captcha.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Captcha" %>
<div class="ektron-ui-control" >
    <div class="ektron-ui-captcha ui-widget ui-widget-content ui-widget-header ui-corner-all">
        <div class="ektron-ui-captcha-image ui-widget-content ui-corner-all">
            <div class="captcha">
                <asp:Image ID="captcha" runat="server" />
            </div>
        </div>
        <div class="ektron-ui-captcha-interface ui-corner-all ui-helper-clearfix">
            <div class="ektron-ui-captcha-controlSet ektron-ui-floatRight">
                <ektronUI:ButtonSet ID="buttonSet" runat="server" >
                    <ektronUI:Button DisplayMode="RadioButton"  ID="btn_Refresh" PrimaryIcon="Refresh" runat="server"
                        ToolTip="<%#this.ControlContainer.RefreshToolTip %>" />
                    <ektronUI:Button DisplayMode="RadioButton" ID="btn_VolumnOn" PrimaryIcon="VolumeOn"
                        runat="server" ToolTip="<%#this.ControlContainer.SpeakerToolTip %>" />
                </ektronUI:ButtonSet>
            </div>
            <div class="ektron-ui-captcha-inputWrapper ektron-ui-floatLeft ui-state-default ui-corner-all">
                <div class="ektron-ui-captcha-instructions ektron-ui-text-small">
                    <ektronUI:Label AssociatedControlID="textField" ID="lblText" runat="server" />
                </div>
                <ektronUI:TextField ID="textField" runat="server">                
                        <ValidationRules>                        
                            <ektronUI:RequiredRule ErrorMessage="The captcha text field is required."  />                                 
                        </ValidationRules>                        
                </ektronUI:TextField>
                <ektronUI:ValidationMessage AssociatedControlID="textField" ID="textFieldValidationMessage"
                    runat="server" />
            </div>
        </div>
    </div>
</div>
   <ektronUI:JavaScriptBlock ID="verticalButtonSet" ExecutionMode="OnEktronReady" runat="server">
            <ScriptTemplate>
            (function( $ ){
                //plugin buttonset vertical
                $.fn.buttonsetv = function() {
                  $(':radio, :checkbox', this).wrap('<div style="margin: 1px"/>');
                  $(this).buttonset();
                  $('label:first', this).removeClass('ui-corner-left').addClass('ui-corner-top');
                  $('label:last', this).removeClass('ui-corner-right').addClass('ui-corner-bottom');
                  mw = 0; // max witdh
                  $('label', this).each(function(index){
                     w = $(this).width();
                     if (w > mw) mw = w; 
                  })
                  $('label', this).each(function(index){
                    $(this).width(mw);
                  })
                };
            })( $ektron );        

            $("#<%#this.buttonSet.ClientID %>").buttonsetv();
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
