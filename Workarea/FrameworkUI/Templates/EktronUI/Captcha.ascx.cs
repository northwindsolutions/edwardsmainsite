﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Core.Validation;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    public partial class Captcha : ValidatingTemplateBase<CaptchaBase>, IValidatableTemplate, INamingContainer 
	{
        private string _defText = "Type the text shown.";
        //private string _helplink = "captcha/"; 
        ICmsContextService cmsContext = null;

		public Captcha()
		{
            cmsContext = ServiceFactory.CreateCmsContextService();
			this.ID = "Captcha";                        
		}
        #region Properties

        public string InitParams
		{
			get { return GetInitParams(); }
		}

        public override string Text
        {
            get
            {
                return textField.Text;
            }
            set { textField.Text = value; }
        }

        public override Control ControlToValidate
        {
            get
            {
                return textField;
            }
        }

        public override string ValidationGroup
        {           
            get
            {
                 return textField.ValidationGroup;
            }
            set
            {
                textField.ValidationGroup = value;
            }
        }
        public override string LabelableControlID
        {
            get
            {
                return textField.ClientID;
            }
        }        
        #endregion

        #region Events   
  
        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);            
        }
		protected override void OnPreRender(EventArgs e)
		{            
            base.OnPreRender(e);            
		}
		protected override void OnLoad(EventArgs e)
		{                       
			captcha.ImageUrl = ControlContainer.HandlerPath + "?" + GetInitParams();
            captcha.ToolTip = this.ControlContainer.Title;
            captcha.AlternateText = this.ControlContainer.AlternateText;
            //ControlContainer.HelpUrl = ControlContainer.HelpUrl.Length > 0 ? ControlContainer.HelpUrl : GetUrl();
            ControlContainer.Selector = ControlContainer.ClientID;
            lblText.Text = ControlContainer.Text.Length > 0 ? ControlContainer.Text:_defText ;                 
			base.OnLoad(e);
		}
        protected override void OnInitialize(object sender, EventArgs e) 
        {
            this.ControlContainer.Selector = this.ClientID;            
        }
        protected override void OnReady(object sender, EventArgs e) { }
        protected override void OnRegisterResources(object sender, EventArgs e)
        {                         
            Ektron.Cms.Framework.UI.JavaScript.Register(this, cmsContext.UIPath + "/js/Ektron/Controls/EktronUI/Ektron.Controls.EktronUI.Captcha.js");
            Ektron.Cms.Framework.UI.JavaScript.Register(this, cmsContext.UIPath + "/Templates/EktronUI/Captcha/soundmanager/js/soundmanager2.js");            
        }
        #endregion

        #region Private methods

        private string GetInitParams()
		{
			IDictionary<string, string> initParamList = new Dictionary<string, string>();

			if (!ControlContainer.BackgroundColor.IsEmpty)
			{
				initParamList["bc"] = ControlContainer.BackgroundColor.Name;
			}
			if (!ControlContainer.ForegroundColor.IsEmpty)
			{
				initParamList["fc"] = ControlContainer.ForegroundColor.Name;
			}
			if (ControlContainer.FontFamily != null && ControlContainer.FontFamily.Length > 0)
			{
				initParamList["ff"] = ControlContainer.FontFamily.ToString();
			}
			if (ControlContainer.CodeLength > 0)
			{
				initParamList["cl"] = ControlContainer.CodeLength.ToString();
			}            
            initParamList["codeType"] = ControlContainer.CodeType.GetHashCode().ToString();
            initParamList["unique"] = System.Guid.NewGuid().ToString().Replace("-", "");
            
			return UI.Controls.EktronUI.Widgets.FileUploadUtility.BuildInitParamsString(initParamList).Replace(',', '&');
        }
       /* private string GetUrl()
        {
            _helplink = "http://";
            if (Context.Request.IsSecureConnection || Context.Request.Url.ToString().ToLower().StartsWith("https://"))
            {
                _helplink = "https://";
            }
            _helplink += Context.Request.Url.Host + "/captcha/";
            return _helplink;
        }*/
        #endregion
    }
}