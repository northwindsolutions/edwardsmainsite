﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemTemplate.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Web.FrameworkUI.Templates.EktronUI.EditorMenu.ItemTemplate" %>
<%@ Register Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.EventBoundControls"
    TagPrefix="eventBoundControls" %>
<li>
    <eventBoundControls:LinkButton ID="link1" runat="server" CommandName='<%# Eval("CommandName") %>' CommandArgument='<%# Eval("CommandArgument") %>' BoundCommand='<%# Eval("CommandHandler") %>' Text='<%# Eval("Text") %>' />
</li>