﻿using System.Collections.Specialized;
using System.Web.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.ContextMenuBase;

namespace Ektron.Cms.Framework.UI.Web.FrameworkUI.Templates.ContextMenuBase
{
    public partial class ItemTemplate : UserControl, IBindableTemplate, IPlaceHolder
	{
		public IOrderedDictionary ExtractValues(Control container)
		{
			return new OrderedDictionary();
		}

		public void InstantiateIn(Control container)
		{
			container.Controls.Add(this);
		}

        #region IPlaceHolder Members

        public Control PlaceHolder {
            get { return this.itemPlaceholder; }
        }

        #endregion
    }
}