﻿using System.Collections.Specialized;
using System.Web.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.ContextMenuBase;

namespace Ektron.Cms.Framework.UI.Web.FrameworkUI.Templates.ContextMenuBase
{
    public partial class ListTemplate : UserControl, IBindableTemplate, IPlaceHolder
	{
		#region IBindableTemplate Members

		public IOrderedDictionary ExtractValues(Control container)
		{
			return new OrderedDictionary();
		}

		public void InstantiateIn(Control container)
		{
			container.Controls.Add(this);
		}

		#endregion

        #region IPlaceHolder Members

        public Control PlaceHolder {
            get { return this.listPlaceholder; }
        }

        #endregion
    }
}