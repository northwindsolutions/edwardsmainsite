﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Datepicker.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Datepicker" %>

<div ID="uxDatepicker" runat="server">
    <ektronUI:DateField ID="uxDateField" runat="server" CssClass="key" Visible="true" />
    <div id="uxInline" runat="server" visible="false" class="key"></div>    
</div>