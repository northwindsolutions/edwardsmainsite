﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileUpload.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.FileUpload" %>
<div id="<%# this.ControlContainer.ClientID %>" class="ektron-ui-control ektron-ui-fileUpload">
	<div class="ektron-ui-fileUpload-files">
	</div>
	<div class="ektron-ui-clearfix">
	</div>
	<div class="ektron-ui-fileUpload-browse">
		<object data="data:application/x-silverlight-2," type="application/x-silverlight-2"
			width='<%# Width %>' height='<%# Height %>'>
			<param name="windowless" value="true" />
			<param name="source" value='<%# XapPath %>' />
			<param name="onError" value="onSilverlightError" />
			<param name="minRuntimeVersion" value="3.0.40818.0" />
			<param name="initParams" value="<%# InitParams %>" />
            <param name="background" value="transparent" />
            <param name="PluginBackground" value="transparent" />
		</object>
	</div>
	<ektronUI:ValidationMessage ID="vm1" AssociatedControlID="uxHiddenField1" runat="server" />
	<div class="ektron-ui-fileUpload-hidden ektron-ui-hidden">
			<ektronUI:TextField ID="uxHiddenField1" runat="server"></ektronUI:TextField>
	</div>
	<iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px;
		border: 0px"></iframe>
	<div class="ektron-ui-clearfix">
	</div>
</div>
<ektronUI:Css ID="baseCss" runat="server" Path="{UIPath}/css/Ektron/ektron-ui.css" />
<ektronUI:Css ID="fileUploadCss" runat="server" Path="{UIPath}/css/Ektron/Controls/ektron-ui-fileUpload.css" />
<script id="uploadTemplate" type="text/x-jquery-tmpl">
	<div id='${FileGuid}' class='ektron-ui-fileUpload-file'>
		<div class='ektron-ui-fileUpload-file-inner ektron-ui-clearfix'>
			<span class='ektron-ui-fileUpload-message'></span>
			<div class='ektron-ui-fileUpload-cancel'>
				<span class="ektron-ui-control ektron-ui-button">					<button onclick="return false;" title="<asp:Literal runat="server" ID="aspCancelButtonTitle" Text="<%$ Resources:CancelButtonText%>" />" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only" role="button" aria-disabled="false">
                        <span class="ui-button-icon-primary ui-icon ui-icon-cancel" onclick="return false;"></span>
                        <span class="ui-button-text"><asp:Literal runat="server" ID="aspCancelButtonText" Text="<%$ Resources:CancelButtonText%>" /></span>
                    </button>
				</span>
			</div>    
			<div class='ektron-ui-fileUpload-delete ektron-ui-hidden'>
				<span class="ektron-ui-control ektron-ui-button">					<button onclick="return false;" title="<asp:Literal runat="server" ID="aspDeleteButtonTitle" Text="<%$ Resources:DeleteButtonText%>" />" class=" ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only" role="button" aria-disabled="false">
                        <span class="ui-button-icon-primary ui-icon ui-icon-closethick" onclick="return false;"></span>
                        <span class="ui-button-text"><asp:Literal runat="server" ID="aspDeleteButtonText" Text="<%$ Resources:DeleteButtonText%>" /></span>
                    </button>
				</span>		
			</div>
            <div class='ektron-ui-fileUpload-progressBar'>
				<div class="ektron-ui-control ektron-ui-progressbar"></div>
			</div>
            <div class='ektron-ui-fileUpload-fileName ektron-ui-fileUpload-label'>
				<span>${FileName}</span>
			</div>
		</div>
	</div>
</script>
<script type="text/javascript">
	function EktronFileUploadReady_<%# this.ControlContainer.ClientID %>(){
		Ektron.Controls.EktronUI.FileUpload.init('<%# this.ControlContainer.ClientID %>');
	}
</script>
