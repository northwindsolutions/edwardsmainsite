﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Core.Validation;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Interfaces.Context;
using Ektron.Newtonsoft.Json.Linq;

namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{

	public partial class FileUpload : TemplateBase<EktronUI.FileUpload>
	{
		public string InitParams
		{
			get { return GetInitParams(); }
		}

		public string XapPath
		{
			get
			{
				try
				{
					ICmsContextService cmsContext = ServiceFactory.CreateCmsContextService();

					return (cmsContext != null) ?
													cmsContext.UIPath + "/Silverlight/EktronUI/Ektron.Cms.Framework.UI.Controls.EktronUI.FileUpload.xap" :
													String.Empty;
				}
				catch (NullReferenceException)
				{
					return String.Empty;
				}
			}
		}

		public bool IsRequired
		{
			get { return ControlContainer.IsRequired; }
			set { ControlContainer.IsRequired = value; }
		}

		public string Height
		{
			get { return ControlContainer.ButtonHeight.ToString(); }
		}

		public string Width
		{
			get { return ControlContainer.ButtonWidth.ToString(); }
		}

		public string AddMessage
		{
			get { return ControlContainer.AddMessage; }
			set { ControlContainer.AddMessage = value; }
		}

		public string CanceledMessage
		{
			get { return ControlContainer.CanceledMessage; }
			set { ControlContainer.CanceledMessage = value; }
		}

		public string FinishedMessage
		{
			get { return ControlContainer.FinishedMessage; }
			set { ControlContainer.FinishedMessage = value; }
		}

		public string UploadingMessage
		{
			get { return ControlContainer.UploadingMessage; }
			set { ControlContainer.UploadingMessage = value; }
		}

		public string UploadDirectory
		{
			get
			{
				if (ViewState["uploadDirectory"] == null)
					return String.Empty;

				return ViewState["uploadDirectory"].ToString();
			}
			set { ViewState["uploadDirectory"] = value; }
		}

		protected override void OnInitialize(object sender, EventArgs e)
		{
			Packages.jQuery.jQueryUI.Progressbar.Register(this);
			Packages.jQuery.Plugins.Tmpl.Register(this);
			Packages.Microsoft.Silverlight.Register(this);
			Packages.Ektron.JSON.Register(this);
		}

		protected override void OnReady(object sender, EventArgs e)
		{
			DataBind();

			if (ControlContainer.Path != null)
			{
				UploadDirectory =
					(ControlContainer.Path.StartsWith(@"~\") || ControlContainer.Path.StartsWith(@"~/"))
						? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ControlContainer.Path.Substring(2))
						: Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ControlContainer.Path);
			}
		}

		protected override void OnRegisterResources(object sender, EventArgs e)
		{
		}

		protected string ErrorMessage
		{
			get { return ControlContainer.ErrorMessage; }
			set { ControlContainer.ErrorMessage = value; }
		}

		protected override void OnLoad(EventArgs e)
		{
			StringBuilder initScript = new StringBuilder();

			initScript.Append(@"Ektron.Controls.EktronUI.FileUpload.Messages.push({Control:'" + this.ControlContainer.ClientID +  "', Message: '" + this.AddMessage + "', Type: 'AddMessage'})" + Environment.NewLine);

			initScript.Append(@"Ektron.Controls.EktronUI.FileUpload.Messages.push({Control:'" + this.ControlContainer.ClientID + "', Message: '" + this.CanceledMessage + "', Type: 'CanceledMessage'})" + Environment.NewLine);

			initScript.Append(@"Ektron.Controls.EktronUI.FileUpload.Messages.push({Control:'" + this.ControlContainer.ClientID + "', Message: '" + this.FinishedMessage + "', Type: 'FinishedMessage'})" + Environment.NewLine);

			initScript.Append(@"Ektron.Controls.EktronUI.FileUpload.Messages.push({Control:'" + this.ControlContainer.ClientID + "', Message: '" + this.UploadingMessage + "', Type: 'UploadingMessage'})" + Environment.NewLine);

			Ektron.Cms.Framework.UI.JavaScript.RegisterJavaScriptBlock(this, initScript.ToString());

			if (IsRequired)
			{
				var rule = new Core.Validation.RequiredRule(ErrorMessage)
				           	{
				           		ValidationGroup = ""
				           	};

				ValidationAPI.AddRule(uxHiddenField1.ControlToValidate, rule);
			}

			var deserializeObjects = Newtonsoft.Json.JsonConvert.DeserializeObject(uxHiddenField1.Value) as JContainer;

			if (deserializeObjects == null) { base.OnLoad(e); return; }

			foreach (JObject jObject in deserializeObjects)
			{
				var fileName = String.Empty;
				JToken jTokenFileName;

				if (jObject.TryGetValue("FileName", out jTokenFileName))
					fileName = jTokenFileName.ToString().Replace("\"", String.Empty);

				var fileGuid = String.Empty;
				JToken jTokenFileGuid;

				if (jObject.TryGetValue("FileGuid", out jTokenFileGuid))
					fileGuid = jTokenFileGuid.ToString().Replace("\"", String.Empty); ;

				if (String.IsNullOrEmpty(fileName) || String.IsNullOrEmpty(fileGuid)) continue;

				var lastIndexOf = fileName.LastIndexOf(".");

				if (lastIndexOf > 0)
				{
					if (!ControlContainer.TurnOffFileExistenceValidation)
					{
						var fileOnDisk = Path.Combine(UploadDirectory, fileGuid);

						if (System.IO.File.Exists(fileOnDisk))
						{
							var uploadedFile = new UploadedFile
							                   	{
							                   		FileName = fileGuid,
							                   		OriginalFileName = fileName,
							                   		Path = ControlContainer.Path
							                   	};

							ControlContainer.UploadedFiles.Add(uploadedFile);
						}
					}
					else
					{
						var uploadedFile = new UploadedFile
						{
							FileName = fileGuid,
							OriginalFileName = fileName,
							Path = ControlContainer.Path
						};

						ControlContainer.UploadedFiles.Add(uploadedFile);
					}
				}
			}

			base.OnLoad(e);
		}

		private string GetInitParams()
		{
			IDictionary<string, string> initParamList = new Dictionary<string, string>();
			initParamList["UploadHandlerPath"] = ControlContainer.HandlerPath;
			initParamList["RemoveHandlerPath"] = ControlContainer.DeleteHandlerPath;

			initParamList["Path"] = ControlContainer.Path;

			if (ControlContainer.MaxUploads > 0)
			{
				initParamList["MaxUploads"] = ControlContainer.MaxUploads.ToString();
			}

			if (ControlContainer.MaxFileSizeKB > 0)
			{
				initParamList["MaxFileSizeKB"] = ControlContainer.MaxFileSizeKB.ToString();
			}

			if (ControlContainer.ChunkSizeMB > 0)
			{
				initParamList["ChunkSizeMB"] = ControlContainer.ChunkSizeMB.ToString();
			}

			if (!String.IsNullOrEmpty(ControlContainer.FileTypeFilter))
			{
				initParamList["FileFilter"] = ControlContainer.FileTypeFilter;
			}

			if (ControlContainer.ButtonHeight > 0)
			{
				initParamList["ButtonHeight"] = ControlContainer.ButtonHeight.ToString();
			}

			if (ControlContainer.ButtonWidth > 0)
			{
				initParamList["ButtonWidth"] = ControlContainer.ButtonWidth.ToString();
			}

			initParamList["UniqueNamingOff"] = ControlContainer.UniqueNamingOff.ToString();
			initParamList["UploadedFileProcessorType"] = ControlContainer.UploadedFileProcessorType;
			initParamList["MultiSelect"] = ControlContainer.AllowMultipleFiles.ToString();
			initParamList["ClientID"] = ControlContainer.ClientID;

			if (!String.IsNullOrEmpty(ControlContainer.ImageUrl)) initParamList["ImageUrl"] = ControlContainer.ImageUrl;
			if (!String.IsNullOrEmpty(ControlContainer.MouseDownImageUrl)) initParamList["MouseDownImageUrl"] = ControlContainer.MouseDownImageUrl;
			if (!String.IsNullOrEmpty(ControlContainer.MouseOverImageUrl)) initParamList["MouseOverImageUrl"] = ControlContainer.MouseOverImageUrl;

			return UI.Controls.EktronUI.Widgets.FileUploadUtility.BuildInitParamsString(initParamList);
		}
	}
}