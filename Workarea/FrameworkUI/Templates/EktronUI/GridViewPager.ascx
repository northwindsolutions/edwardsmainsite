﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridViewPager.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.GridViewPager" %>
<div class="ui-toolbar ui-widget-header ektron-ui-row-pager-content ektron-ui-clearfix">
    <div class="total-entries-text">
        <asp:Literal ID="uxTotalEntriesText" runat="server" />
    </div>
    <asp:PlaceHolder ID="uxInteraction" runat="server">
        <div class="paging-interaction" style="padding-right:.5em;">
            <ektronUI:Button ID="uxFirst" runat="server" PrimaryIcon="SeekFirst" DisplayMode="Anchor" ToolTip="<%$ Resources:First %>" OnCommand="OnPagingCommand" CommandName="First" />
            <ektronUI:Button ID="uxPrevious" runat="server" PrimaryIcon="TriangleWest" DisplayMode="Anchor" ToolTip="<%$ Resources:Previous %>" OnCommand="OnPagingCommand" CommandName="Previous" />
            <div class="ad-hoc-paging" style="padding-left:.5em;">
                <asp:Literal ID="uxPageText" runat="server" Text="<%$ Resources:Page %>" />
                <ektronUI:IntegerField ID="uxAdHocPageNumber" runat="server" MinValue="1" MaxValue="<%# this.PagingInfo.TotalPages %>" />
                <asp:Literal ID="uxTotalPagesText" runat="server" />
            </div>
            <ektronUI:Button ID="uxNext" runat="server" DisplayMode="Anchor" PrimaryIcon="TriangleEast" ToolTip="<%$ Resources:Next %>" OnCommand="OnPagingCommand" CommandName="Next" />
            <ektronUI:Button ID="uxLast" runat="server" DisplayMode="Anchor" PrimaryIcon="SeekNext" ToolTip="<%$ Resources:Last %>" OnCommand="OnPagingCommand" CommandName="Last" />
        </div>
    </asp:PlaceHolder>
</div>
<ektronUI:JavaScriptBlock ID="JavaScriptBlock1" runat="server">
    <ScriptTemplate>
        $ektron.widget("ektron.gridview", $ektron.extend({}, $ektron.ektron.gridview.prototype, {
            Pager: {
                _bindEvents: function(instance){
                    var pager = this;
                    instance.find(".ektron-ui-row-pager-content .ad-hoc-paging input").bind("keypress", function(e){
                        var code = (e.keyCode ? e.keyCode : e.which);
                        if(code == 13) {
                            var input = $(this);
                            var number = parseInt(input.val(), 10);
                            if(number > pager.PagingInfo.TotalPages){
                                input.val(pager.PagingInfo.TotalPages);
                            }
                            if(number < 1){
                                input.val(1);
                            }
                            <%= this.PostBackTrigger %>

                            if (e.preventDefault){
                                e.preventDefault();
                            } 
                        }
                    });
                },
                _init: function (instance) {
                    this._bindEvents(instance);  
                },
                PagingInfo: <%= this.PagingInfoText %>
            }
        }));
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>