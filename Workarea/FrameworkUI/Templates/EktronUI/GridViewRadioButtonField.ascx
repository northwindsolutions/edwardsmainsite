﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridViewRadioButtonField.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.GridViewRadioButtonField" %>
<asp:MultiView ID="uxTemplate" runat="server" ActiveViewIndex="<%# this.View %>">
    <asp:View ID="uxHeaderTemplate" runat="server">
        <asp:PlaceHolder ID="uxHeaderRadioButton" runat="server" Visible="<%# this.HeaderFieldVisible %>">
            <input id="<%= this.HeaderFieldId %>" type="radio" name="ektron-ui-item-selector-header" <%= this.HeaderFieldToolTip %> <%= this.HeaderFieldEnabledText %> class="ektron-ui-item-selector-header ektron-ui-gridview-radioButton-header" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="uxHeaderLabel" runat="server" Visible="<%# this.HeaderFieldVisible && !String.IsNullOrEmpty(this.HeaderText) %>">
            <label for="<%= this.HeaderFieldId %>"><%= this.HeaderText %></label>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="<%# !this.HeaderFieldVisible %>">
            <%= this.HeaderText %>
        </asp:PlaceHolder>
        <input type="hidden" name="<%= this.DirtyFieldsName %>" class="ektron-ui-gridview-column-data" value='<%= this.DirtyFieldsJson %>' />
        <input type="hidden" class="ektron-ui-item-selector-autoPostBack" value="<%= this.AutoPostBack.ToString().ToLower() %>" />
        <input type="hidden" class="ektron-ui-item-selector-uniqueId" value="<%= this.UniqueID %>" />
    </asp:View>
    <asp:View ID="uxItemTemplate" runat="server">
        <input type="radio" id="<%= this.RadioButtonFieldClientID %>" <%= this.ToolTip %> <%= this.EnabledText %> name="ektron-ui-gridview-radioButton" class="ektron-ui-item-selector ektron-ui-gridview-radioButton" <%= this.Checked ? " checked=\"checked\"" : String.Empty %> />
        <input type="hidden" name="<%= this.RadioButtonFieldName %>" class="ektron-ui-gridview-radioButton-data" value='<%= this.RadioButtonFieldValue %>' />
    </asp:View>
</asp:MultiView>