﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using System.Web.Script.Serialization;
    using Ektron.Newtonsoft.Json;

    public partial class GridViewRadioButtonField : System.Web.UI.UserControl, ITemplate, IGridViewDataControlField, IPostBackEventHandler
    {
        //enum
        public enum RenderState
        {
            InitialPageLoad,
            PostBackWithDataBind,
            PostBackNoDataBind
        }

        //members
        private bool isDataBound;
        private List<GridViewDirtyField> dirtyFields;
        private List<GridViewDirtyField> currentFields;
        private bool isCurrentFieldsDeserialized;

        //constructor
        public GridViewRadioButtonField()
        {
            this.ID = "RadioButtonField";
            this.isDataBound = false;
        }

        public RenderState State
        {
            get
            {
                RenderState state = RenderState.InitialPageLoad;
                if (this.Page.IsPostBack)
                {
                    state = this.isDataBound ? RenderState.PostBackWithDataBind : RenderState.PostBackNoDataBind;
                }
                return state;
            }
        }

        private List<GridViewDirtyField> CurrentFields
        {
            get
            {
                if (!this.isCurrentFieldsDeserialized)
                {
                    string currentFieldsJson = HttpContext.Current.Request.Form[this.RadioButtonFieldName];
                    if (!String.IsNullOrEmpty(currentFieldsJson))
                    {
                        this.currentFields = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridViewDirtyField>>("[" + currentFieldsJson + "]");
                    }
                    else
                    {
                        this.currentFields = new List<GridViewDirtyField>();
                    }
                    this.isCurrentFieldsDeserialized = true;
                }
                return this.currentFields;
            }
        }

        //properties
        public bool IsDataBound { get; set; }

        //header
        public string HeaderFieldId
        {
            get
            {
                string headerFieldId = String.Empty;
                if (ViewState["HeaderFieldId"] != null)
                {
                    headerFieldId = (string)ViewState["HeaderFieldId"];
                }
                return headerFieldId;
            }
            set
            {
                ViewState["HeaderFieldId"] = value;
            }
        }
        public string HeaderFieldToolTip
        {
            get
            {
                string headerFieldToolTip = String.Empty;
                if (ViewState["HeaderFieldToolTip"] != null)
                {
                    headerFieldToolTip = "title=\"" + (string)ViewState["HeaderFieldToolTip"] + "\"";
                }
                return headerFieldToolTip;
            }
            set
            {
                ViewState["HeaderFieldToolTip"] = value;
            }
        }
        public bool HeaderTextBeforeFieldVisible
        {
            get
            {
                bool headerTextBeforeFieldVisible = false;
                if (ViewState["HeaderTextBeforeFieldVisible"] != null)
                {
                    headerTextBeforeFieldVisible = (bool)ViewState["HeaderTextBeforeFieldVisible"];
                }
                return headerTextBeforeFieldVisible;
            }
            set
            {
                ViewState["HeaderTextBeforeFieldVisible"] = value;
            }
        }
        public bool HeaderTextAfterFieldVisible
        {
            get
            {
                bool headerTextAfterFieldVisible = false;
                if (ViewState["HeaderTextAfterFieldVisible"] != null)
                {
                    headerTextAfterFieldVisible = (bool)ViewState["HeaderTextAfterFieldVisible"];
                }
                return headerTextAfterFieldVisible;
            }
            set
            {
                ViewState["HeaderTextAfterFieldVisible"] = value;
            }
        }
        public bool HeaderFieldVisible
        {
            get
            {
                bool headerFieldVisible = false;
                if (ViewState["HeaderFieldVisible"] != null)
                {
                    headerFieldVisible = (bool)ViewState["HeaderFieldVisible"];
                }
                return headerFieldVisible;
            }
            set
            {
                ViewState["HeaderFieldVisible"] = value;
            }
        }
        public bool HeaderFieldEnabled
        {
            get
            {
                bool headerFieldEnabled = false;
                if (ViewState["HeaderFieldEnabled"] != null)
                {
                    headerFieldEnabled = (bool)ViewState["HeaderFieldEnabled"];
                }
                return headerFieldEnabled;
            }
            set
            {
                ViewState["HeaderFieldEnabled"] = value;
            }
        }
        public string HeaderFieldEnabledText
        {
            get
            {
                return this.HeaderFieldEnabled ? String.Empty : "disabled=\"disabled\"";
            }
        }
        public string HeaderText
        {
            get
            {
                string headerText = String.Empty;
                if (ViewState["HeaderText"] != null)
                {
                    headerText = (string)ViewState["HeaderText"];
                }
                return headerText;
            }
            set
            {
                ViewState["HeaderText"] = value;
            }
        }
        public bool AutoPostBack
        {
            get
            {
                bool autoPostBack = false;
                if (ViewState["AutoPostBack"] != null)
                {
                    autoPostBack = (bool)ViewState["AutoPostBack"];
                }
                return autoPostBack;
            }
            set
            {
                ViewState["AutoPostBack"] = value;
            }
        }
        public string ColumnId
        {
            get
            {
                string columnId = String.Empty;
                if (ViewState["ColumnId"] != null)
                {
                    columnId = (string)ViewState["ColumnId"];
                }
                return columnId;
            }
            set
            {
                ViewState["ColumnId"] = value;
            }
        }
        public string DirtyFieldsJson
        {
            get
            {
                string dirtyFieldsJson = String.Empty;
                switch (this.State)
                {
                    case RenderState.InitialPageLoad:
                        if (ViewState["DirtyFieldsJson"] != null)
                        {
                            dirtyFieldsJson = (string)ViewState["DirtyFieldsJson"];
                        }
                        break;
                    case RenderState.PostBackNoDataBind:
                        dirtyFieldsJson = HttpContext.Current.Request.Form[this.RadioButtonFieldClientID];
                        if (String.IsNullOrEmpty(dirtyFieldsJson))
                        {
                            dirtyFieldsJson = (string)ViewState["DirtyFieldsJson"];
                        }
                        else
                        {
                            ViewState["DirtyFieldsJson"] = dirtyFieldsJson;
                        }
                        break;
                    case RenderState.PostBackWithDataBind:
                        if (ViewState["DirtyFieldsJson"] != null)
                        {
                            dirtyFieldsJson = (string)ViewState["DirtyFieldsJson"];
                        }
                        break;
                }
                return dirtyFieldsJson;
            }
            set
            {
                ViewState["DirtyFieldsJson"] = value;
            }
        }
        public string DirtyFieldsName
        {
            get
            {
                string dirtyFieldsName = String.Empty;
                if (ViewState["DirtyFieldsName"] != null)
                {
                    dirtyFieldsName = (string)ViewState["DirtyFieldsName"];
                }
                return dirtyFieldsName;
            }
            set
            {
                ViewState["DirtyFieldsName"] = value;
            }
        }

        //helpers
        public DataControlRowType RowType
        {
            get
            {
                DataControlRowType rowType = DataControlRowType.DataRow;
                if (ViewState["RowType"] != null)
                {
                    rowType = (DataControlRowType)ViewState["RowType"];
                }
                return rowType;
            }
            set
            {
                ViewState["RowType"] = value;
            }
        }
        public int View
        {
            get
            {
                return this.RowType == DataControlRowType.Header ? 0 : 1;
            }
        }

        //datarow - RadioButton
        public string ToolTip
        {
            get
            {
                string toolTip = String.Empty;
                if (ViewState["ToolTip"] != null)
                {
                    toolTip = "title=\"" + (string)ViewState["ToolTip"] + "\"";
                }
                return toolTip;
            }
            set
            {
                ViewState["HeaderToolTip"] = value;
            }
        }
        public bool Enabled
        {
            get
            {
                bool enabled = false;
                if (ViewState["Enabled"] != null)
                {
                    enabled = (bool)ViewState["Enabled"];
                }
                return enabled;
            }
            set
            {
                ViewState["Enabled"] = value;
            }
        }
        public bool Checked
        {
            get
            {
                bool isChecked = false;
                switch (this.State)
                {
                    case RenderState.InitialPageLoad:
                        if (ViewState["Checked"] != null)
                        {
                            isChecked = (bool)ViewState["Checked"];
                        }
                        break;
                    case RenderState.PostBackNoDataBind:
                        bool isDirty = false;
                        this.CurrentFields.FindAll(x => x.FieldClientID == this.RadioButtonFieldClientID).ForEach(delegate(GridViewDirtyField currentField)
                        {
                            isDirty = true;
                            if (currentField.DirtyState == currentField.OriginalState || String.IsNullOrEmpty(currentField.DirtyState))
                            {
                                //set to original state
                                ViewState["Checked"] = isChecked = (currentField.OriginalState == "checked") ? true : false;
                            }
                            else
                            {
                                //set to dirty state
                                ViewState["Checked"] = isChecked = (currentField.DirtyState == "checked") ? true : false;
                            }
                        });
                        if (!isDirty)
                        {
                            //field is not dirty - set to original value
                            if (ViewState["Checked"] != null)
                            {
                                isChecked = (bool)ViewState["Checked"];
                            }
                        }
                        break;
                    case RenderState.PostBackWithDataBind:
                        if (ViewState["Checked"] != null)
                        {
                            isChecked = (bool)ViewState["Checked"];
                        }
                        break;
                }

                return isChecked;
            }
            set
            {
                ViewState["Checked"] = value;
            }
        }
        public string RadioButtonFieldClientID
        {
            get
            {
                string RadioButtonFieldClientID = String.Empty;
                if (ViewState["RadioButtonFieldClientID"] != null)
                {
                    RadioButtonFieldClientID = (string)ViewState["RadioButtonFieldClientID"];
                }
                return RadioButtonFieldClientID;
            }
            set
            {
                ViewState["RadioButtonFieldClientID"] = value;
            }
        }
        public string RadioButtonFieldName
        {
            get
            {
                string RadioButtonFieldName = String.Empty;
                if (ViewState["RadioButtonFieldName"] != null)
                {
                    RadioButtonFieldName = (string)ViewState["RadioButtonFieldName"];
                }
                return RadioButtonFieldName;
            }
            set
            {
                ViewState["RadioButtonFieldName"] = value;
            }
        }
        public string RadioButtonFieldValue
        {
            get
            {
                string RadioButtonFieldValue = String.Empty;
                switch (this.State)
                {
                    case RenderState.InitialPageLoad:
                        if (ViewState["RadioButtonFieldValue"] != null)
                        {
                            RadioButtonFieldValue = (string)ViewState["RadioButtonFieldValue"];
                        }
                        break;
                    case RenderState.PostBackNoDataBind:
                        this.CurrentFields.FindAll(x => x.FieldClientID == this.RadioButtonFieldClientID).ForEach(delegate(GridViewDirtyField currentField)
                        {
                            JavaScriptSerializer json = new JavaScriptSerializer();
                            RadioButtonFieldValue = json.Serialize(currentField);
                        });
                        if (String.IsNullOrEmpty(RadioButtonFieldValue))
                        {
                            RadioButtonFieldValue = (string)ViewState["RadioButtonFieldValue"];
                        }
                        else
                        {
                            ViewState["RadioButtonFieldValue"] = RadioButtonFieldValue;
                        }
                        break;
                    case RenderState.PostBackWithDataBind:
                        if (ViewState["RadioButtonFieldValue"] != null)
                        {
                            RadioButtonFieldValue = (string)ViewState["RadioButtonFieldValue"];
                        }
                        break;
                }
                return RadioButtonFieldValue;
            }
            set
            {
                ViewState["RadioButtonFieldValue"] = value;
            }
        }
        public string EnabledText
        {
            get
            {
                return this.Enabled ? String.Empty : "disabled=\"disabled\"";
            }
        }

        //events
        protected override void OnPreRender(EventArgs e)
        {
            if (this.Visible)
            {
                Packages.Ektron.Controls.EktronUI.GridView.RadioButtonField.Register(this);
                if (!this.isDataBound)
                {
                    this.DataBind();
                }
            }
            base.OnPreRender(e);
        }
        public void RowDataBound(GridViewDataControlFieldEventArgs e)
        {
            this.RadioButtonFieldClientID = e.FieldClientID;
            this.RadioButtonFieldName = e.FieldName;
            this.RadioButtonFieldValue = e.FieldValue;
            this.ToolTip = e.ToolTip;
            this.Checked = (e as GridViewRadioButtonFieldEventArgs).Checked;
            this.Visible = (e as GridViewRadioButtonFieldEventArgs).RadioButtonVisible;
            this.Enabled = e.Enabled;

            this.isDataBound = true;
        }
        public void SetDirtyFields(List<GridViewDirtyField> dirtyFields, string dirtyFieldsName)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            this.DirtyFieldsJson = json.Serialize(dirtyFields);
            this.DirtyFieldsName = dirtyFieldsName;
        }

        //methods
        public void InstantiateIn(Control container)
        {
            container.Controls.Add(this);
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            GridViewSelectedIndexChangedEventArgs eventArgs = new GridViewSelectedIndexChangedEventArgs();
            eventArgs.ColumnId = this.ColumnId;
            eventArgs.DirtyField = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<GridViewDirtyField>(eventArgument);
            RaiseBubbleEvent(this, eventArgs as EventArgs);
        }
    }
}