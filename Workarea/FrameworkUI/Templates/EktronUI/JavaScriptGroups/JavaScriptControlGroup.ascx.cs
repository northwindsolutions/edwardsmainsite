﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.JavaScriptGroups
{
    using System;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;

    public partial class JavaScriptControlGroup : TemplateBase<EktronUI.JavaScriptControlGroup>
    {
        #region members
        protected string AssociatedTriggers { get; set; }
        protected string AssociatedTargets { get; set; }
        protected string AssociatedEvents { get; set; }
        #endregion

        #region events
        protected override void OnInitialize(object sender, EventArgs e)
        {
            ControlContainer.ModulePlaceHolder = aspModulePlaceholder;
        }

        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            // register any required Javascript or CSS resources here:
        }

        protected override void OnReady(object sender, EventArgs e)
        {
            if (Visible)
            {
                AssociatedTriggers = ControlContainer.MetaData.GetAllTriggers();
                AssociatedTargets = ControlContainer.MetaData.GetAllTargets();
                AssociatedEvents = ControlContainer.MetaData.GetEvents();
            }
        }
        #endregion
    }
}