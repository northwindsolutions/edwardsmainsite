﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordField.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.PasswordField" %>
<span id="uxPasswordField" runat="server" class="ektron-ui-control ektron-ui-input ektron-ui-passwordField">
    <asp:TextBox ID="aspInput" runat="server" TextMode="Password" />
</span>