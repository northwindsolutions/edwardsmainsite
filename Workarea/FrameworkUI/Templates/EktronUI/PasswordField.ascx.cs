﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core.Validation;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using System.IO;

    public partial class PasswordField : ValidatingTemplateBase<EktronUI.PasswordField>, IValidatableTemplate, ILabelable
    {
        #region Members, Enums, Constants

        private const string Key = "Ektron0249";

        #endregion

        #region Constructor

        public PasswordField()
        {
            this.ID = "PasswordField";
        }

        #endregion

        #region Properties

        private string ViewstatePassword
        {
            get
            {
                string key = EncryptString(PasswordField.Key as String);
                if (ViewState[key] != null)
                {
                    return DecryptString(ViewState[key] as String);
                }
                return null;
            }
            set
            {
                string key = EncryptString(PasswordField.Key as String);
                ViewState.Add(key, EncryptString(value as String));
            }
        }
        private string TypedPassword { get; set; }

        #endregion

        #region Events

        protected override void OnInitialize(object sender, EventArgs e)
        {
            aspInput.ToolTip = this.ControlContainer.ToolTip;
        }

        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            // register any required Javascript or CSS resources here:
        }

        protected override void OnReady(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (!String.IsNullOrEmpty(this.ControlContainer.CssClass))
                {
                    uxPasswordField.Attributes.Add("class", "ektron-ui-control ektron-ui-input ektron-ui-passwordField  " + this.ControlContainer.CssClass);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (this.ControlContainer.Enabled)
            {
                aspInput.CssClass = String.Empty;
                aspInput.Enabled = true;
            }
            else
            {
                aspInput.CssClass = "ektron-ui-disabled";
                aspInput.Enabled = false;
            }
            base.OnPreRender(e);
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        #endregion

        #region Helpers

        public static string EncryptString(string Message)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(PasswordField.Key));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(Message);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }

        public static string DecryptString(string password)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(PasswordField.Key));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(password);
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }

        #endregion

        #region IValidatableTemplate

        public override string Text
        {
            get
            {
                //return (String.IsNullOrEmpty(aspInput.Text) ? this.TypedPassword : aspInput.Text);

                //if this is a postback, check to see if this postback contains password field, and
                //if so, either use it's value if it has one, or blank out the stored value if the user
                //has posted back an empty value
                if (this.IsPostBack)
                {
                    if (HttpContext.Current.Request.Form[aspInput.UniqueID] != null)
                    {
                        string typedValue = HttpContext.Current.Request.Form[aspInput.UniqueID];
                        this.ViewstatePassword = this.TypedPassword = typedValue;
                    }
                }
                return aspInput.Text = this.ViewstatePassword;
            }
            set 
            {
                aspInput.Text = this.TypedPassword = value;
            }
        }

        public override Control ControlToValidate
        { 
            get 
            { 
                return aspInput; 
            }
        }

        public override string ValidationGroup
        {
            get
            {
                return aspInput.ValidationGroup;
            }
            set
            {
                aspInput.ValidationGroup = value;
            }
        }

        #endregion

        #region ILabelable

        public override string LabelableControlID
        {
            get
            {
                return aspInput.ClientID;
            }
        }

        #endregion
    }
}