﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;

    public partial class Progressbar : TemplateBase<EktronUI.Progressbar>
    {
        public string CssClass { get;set; }

        protected override void OnInitialize(object sender, EventArgs e)
        {
            this.ControlContainer.Selector = "#" + this.ControlContainer.ClientID + " > div";
        }
        protected override void OnReady(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                List<string> cssClass = new List<string>();
                cssClass.Add("ektron-ui-control");
                cssClass.Add("ektron-ui-progressbar");

                if (this.ControlContainer.AutoHide)
                {
                    Packages.Ektron.CssFrameworkBase.RegisterCss(this);
                    cssClass.Add("ektron-ui-hidden");
                }
                cssClass.Add(this.ControlContainer.CssClass);
                this.CssClass = String.Join(" ", cssClass.ToArray());
            }
            this.DataBind();
        }
        protected override void OnRegisterResources(object sender, EventArgs e)
        {

        }
    }
}