﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TextField.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.TextField" %>
<span id="<%# this.WrapperId %>" class="<%# this.CssClass %>">
    <asp:MultiView ID="aspMode" runat="server">
        <asp:View ID="aspInputView" runat="server">
            <asp:TextBox ID="aspInput" runat="server" TextMode="SingleLine" autocomplete="off"  />
        </asp:View>
        <asp:View ID="aspTextareaView" runat="server">
            <asp:TextBox ID="aspTextarea" runat="server" TextMode="MultiLine" />
        </asp:View>
    </asp:MultiView>
    
</span>