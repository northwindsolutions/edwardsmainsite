﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Text;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core.Validation;

    public partial class TextField : ValidatingTemplateBase<EktronUI.TextField>, IValidatableTemplate, ILabelable
    {
        #region Constructor

        public TextField()
        {
            this.ID = "TextField";
        }

        #endregion

        #region properties

        public string WrapperId
        {
            get
            {
                return this.ControlContainer.ClientID;
            }
        }
        public string CssClass
        {
            get
            {
                string cssClass = "ektron-ui-control ektron-ui-input ektron-ui-textField";
                if (!String.IsNullOrEmpty(this.ControlContainer.CssClass))
                {
                    cssClass = "ektron-ui-control ektron-ui-input ektron-ui-textField  " + this.ControlContainer.CssClass;
                }
                return cssClass;
            }
        }

        #endregion

        #region Events

        protected override void OnInitialize(object sender, EventArgs e)
        {
            aspInput.ToolTip = this.ControlContainer.ToolTip;
            if (this.IsPostBack)
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.Form[aspInput.UniqueID]))
                {
                    this.ControlContainer.Text = HttpContext.Current.Request.Form[aspInput.UniqueID];
                }
            }
        }

        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            // register any required Javascript or CSS resources here:
        }

        protected override void OnReady(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            aspMode.SetActiveView(this.ControlContainer.TextMode == EktronUI.TextField.TextModes.SingleLine ? aspInputView : aspTextareaView);
            if (this.ControlContainer.Enabled)
            {
                aspTextarea.CssClass = aspInput.CssClass = String.Empty;
                aspTextarea.Enabled = aspInput.Enabled = true;
            }
            else
            {
                aspTextarea.CssClass = aspInput.CssClass = "ui-state-disabled";
                aspTextarea.Enabled = aspInput.Enabled = false;
            }
            this.DataBind();
            base.OnPreRender(e);
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        #endregion

        #region IValidatableTemplate

        public override string Text
        {
            get
            {
                string text = String.Empty;
                string uniqueId = String.Empty;
                switch (this.ControlContainer.TextMode)
                {
                    case EktronUI.TextField.TextModes.SingleLine:
                        text = aspInput.Text;
                        uniqueId = aspInput.UniqueID;
                        break;
                    case EktronUI.TextField.TextModes.MultiLine:
                        text = aspTextarea.Text;
                        uniqueId = aspTextarea.UniqueID;
                        break;
                }
                return text
                    ?? (this.IsPostBack ? HttpContext.Current.Request.Form[uniqueId] : string.Empty)
                    ?? string.Empty;
            }
            set { aspTextarea.Text = aspInput.Text = value; }
        }

        public override Control ControlToValidate
        { 
            get 
            {
                Control controlToValidate = null;
                switch (this.ControlContainer.TextMode)
                {
                    case EktronUI.TextField.TextModes.SingleLine:
                        controlToValidate = aspInput;
                        break;
                    case EktronUI.TextField.TextModes.MultiLine:
                        controlToValidate = aspTextarea;
                        break;
                }
                return controlToValidate; 
            }
        }

        public override string ValidationGroup
        {
            get
            {
                string validationGroup = String.Empty;
                switch (this.ControlContainer.TextMode)
                {
                    case EktronUI.TextField.TextModes.SingleLine:
                        validationGroup = aspInput.ValidationGroup;
                        break;
                    case EktronUI.TextField.TextModes.MultiLine:
                        validationGroup = aspTextarea.ValidationGroup;
                        break;
                }
                return validationGroup;
            }
            set
            {
                aspTextarea.ValidationGroup = aspInput.ValidationGroup = value;
            }
        }

        #endregion

        #region ILabelable

        public override string LabelableControlID
        {
            get
            {
                string clientID = String.Empty;
                switch(this.ControlContainer.TextMode)
                {
                    case EktronUI.TextField.TextModes.SingleLine:
                        clientID = aspInput.ClientID;
                        break;
                    case EktronUI.TextField.TextModes.MultiLine:
                        clientID = aspTextarea.ClientID;
                        break;
                }
                return clientID;
            }
        }

        #endregion
    }
}