﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderTreeItemTemplate.ascx.cs" Inherits="Tree.TreeResources.Template.ServerRender.FolderTreeItemTemplate" %>
<asp:Repeater ID="nodeRepeater" runat="server" OnItemDataBound="HandleItemDataBound">
    <ItemTemplate>
        <li data-ektron-id="<%# Eval("Id") %>" data-ektron-parentid="<%# Eval("ParentId") %>" 
            data-ektron-rootid="<%# Eval("RootId") %>" data-ektron-path="<%# Eval("Path") %>" 
            data-ektron-level="<%# Eval("Level") %>" data-ektron-selectionmode="<%# Eval("SelectionModeString") %>"
            <asp:Literal id="commandName" runat="server" /> <asp:Literal id="commandArgument" runat="server" />
            <asp:Literal id="clickCausesPostback" runat="server" Text="true" Visible="false" />
            class="<asp:Literal id="liClass" runat="server" />"
        >
            <div class="hitLocation" >
                <div class="selectionLocation <%# (Eval("SelectionModeString") ?? "").ToString().ToLower() %>" >
                    <span class="ui-icon"> </span>
                    <a id="anchorTag" runat="server" visible="false" />
                    <span id="noUrlSpan" runat="server" visible="false" />
                </div>
            </div>
            <ul id="childContainer" runat="server" visible="false" />
        </li>
    </ItemTemplate>
</asp:Repeater>
