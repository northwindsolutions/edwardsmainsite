﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderTreeTemplate.ascx.cs" Inherits="Tree.TreeResources.Template.ServerRender.FolderTreeTemplate" %>
<div id="<%= ClientID %>" <%= CssManager %> >
    <asp:PlaceHolder ID="placeHolder" runat="server" />
    <ul id="<%= ClientID %>_TreeRootElement" class="ektron-ui-tree-root" >
        <asp:PlaceHolder ID="childContainerPlaceHolder" runat="server" />
    </ul>
</div>