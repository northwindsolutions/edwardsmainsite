<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuTreeItemTemplate.ascx.cs" Inherits="Tree.TreeResources.Template.ServerRender.MenuTreeItemTemplate" %>
<asp:Repeater ID="nodeRepeater" runat="server" OnItemDataBound="HandleItemDataBound">
    <ItemTemplate>
        <li data-ektron-id="<%# Eval("Id") %>" data-ektron-parentid="<%# Eval("ParentId") %>" 
            data-ektron-rootid="<%# Eval("RootId") %>" data-ektron-path="<%# Eval("Path") %>" 
            data-ektron-level="<%# Eval("Level") %>" data-ektron-selectionmode="<%# Eval("SelectionModeString") %>"
            <asp:Literal id="commandName" runat="server" /> <asp:Literal id="commandArgument" runat="server" />
            <asp:Literal id="clickCausesPostback" runat="server" Text="true" Visible="false" />
            class="<asp:Literal id="liClass" runat="server" />"
        >
            <div class="hitLocation" >
                <div class="selectionLocation ui-state-default ui-corner-all ui-helper-reset <%# (Eval("SelectionModeString") ?? "").ToString().ToLower() %>" >
                    <span class="ui-icon ui-icon-triangle-1-e"> </span>
                    <a id="anchorTag" runat="server" />
                </div>
            </div>
            <ul id="childContainer" runat="server" visible="false" />
        </li>
    </ItemTemplate>
</asp:Repeater>
