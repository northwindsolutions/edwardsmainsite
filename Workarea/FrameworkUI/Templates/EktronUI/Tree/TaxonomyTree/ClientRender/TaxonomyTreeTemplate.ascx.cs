﻿using System;
    using System.Web;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.UI.Tree;
    using Ektron.Cms.Interfaces.Context;

namespace Tree.TreeResources.Template.ClientRender {
    public partial class TaxonomyTreeTemplate : TreeTemplateBase {

        public TaxonomyTreeTemplate() {
            DefaultCssClass = "ektron-ui-control ektron-ui-tree ektron-ui-taxonomyTree";
        }

        protected override PlaceHolder ControlPlaceHolder { get { return placeHolder; } }

        public override string SerializeData(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection data, bool ajaxCallback) {
            return JSON = base.SerializeData(data, ajaxCallback);
        }

        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);

            if (this.Visible) {
                Packages.EktronCoreJS.Register(this);
                Packages.jQuery.Plugins.Tmpl.Register(this);
                Packages.Ektron.JSON.Register(this);
                Packages.jQuery.jQueryUI.Widget.Register(this);

                ICmsContextService cmsContext = ServiceFactory.CreateCmsContextService();
                Ektron.Cms.Framework.UI.JavaScript.Register(this, cmsContext.UIPath + "/js/Ektron/Controls/EktronUI/Ektron.Controls.EktronUI.Tree.js");
                if (base.RegisterCss) {
                    Packages.jQuery.jQueryUI.ThemeRoller.Register(this);
                    Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Common/commonTree.css");
                    Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Taxonomy/ektron-ui-taxonomyTree.css");
                }

                base.JavaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                JavaScript.RegisterJavaScriptBlock(this.placeHolder, base.GetInitializationScript(JSON), true);
            }
        }

    }
}