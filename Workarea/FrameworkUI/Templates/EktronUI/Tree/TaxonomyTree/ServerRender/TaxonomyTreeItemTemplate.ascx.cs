﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Framework.UI.Tree;
using Ektron.Cms.Interfaces.Context;
using System.Web.UI.HtmlControls;

namespace Tree.TreeResources.Template.ServerRender {
    public partial class TaxonomyTreeItemTemplate : System.Web.UI.UserControl {

        private ICmsContextService _iCmsContextService;

        public TaxonomyTreeItemTemplate() { }

        protected ICmsContextService CmsContextService {
            get { return _iCmsContextService ?? (_iCmsContextService = ServiceFactory.CreateCmsContextService()); }
        }

        protected void HandleItemDataBound(Object sender, RepeaterItemEventArgs e) {
            if (e != null && e.Item != null && e.Item.DataItem != null) {
                var node = e.Item.DataItem as Ektron.Cms.Framework.UI.Tree.ITaxonomyTreeNode;
                if (node != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) {
                    e.Item.Visible = node.Visible;
                    ((Literal)e.Item.FindControl("commandName")).Text = string.IsNullOrEmpty(node.CommandName) ? null : "data-ektron-CommandName=\"" + node.CommandName + "\"";
                    ((Literal)e.Item.FindControl("commandArgument")).Text = string.IsNullOrEmpty(node.CommandArgument) ? null : "data-ektron-commandArgument=\"" + node.CommandArgument + "\"";
                    ((Literal)e.Item.FindControl("clickCausesPostback")).Visible = node.ClickCausesPostback;
                    
                    var classValue = node.CanHaveChildren ? "ektron-ui-tree-branch" : "ektron-ui-tree-leaf";
                    classValue += " ektron-ui-tree-taxonomy";
                    classValue += " ektron-ui-tree-type-" + (node.Type ?? "").ToLower();
                    classValue += " ektron-ui-tree-subtype-" + (node.SubType ?? "").ToLower();
                    if (node.HasChildren || node.ChildrenNotLoaded) {
                        classValue += node.Expanded ? " collapsible" : " expandable";
                    }
                    classValue += node.Selected ? " selected" : "";
                    ((Literal)e.Item.FindControl("liClass")).Text = classValue;

                    HtmlContainerControl control;
                    if (string.IsNullOrEmpty(node.NavigateUrl)) {
                        control = ((HtmlContainerControl)e.Item.FindControl("noUrlSpan"));
                    } else {
                        control = ((HtmlContainerControl)e.Item.FindControl("anchorTag"));
                        ((HtmlAnchor)control).HRef = node.NavigateUrl;
                    }
                    SetAttributeIfValid(control, "data-ektron-action", node.Action);
                    SetAttributeIfValid(control, "onclick", node.OnClientClick);
                    control.Attributes.Add("class", "textWrapper" + (string.IsNullOrEmpty(node.OnClientClick) && string.IsNullOrEmpty(node.Action) ? "" : " clickable"));
                    control.InnerText = node.Text;
                    control.Visible = true;

                    if (node.Items != null && node.Items.Count > 0) {
                        var childContainer = ((HtmlGenericControl)e.Item.FindControl("childContainer"));
                        childContainer.Visible = true;
                        var childTemplate = Page.LoadControl(CmsContextService.UIPath + "/Templates/EktronUI/Tree/TaxonomyTree/ServerRender/TaxonomyTreeItemTemplate.ascx");
                        childContainer.Controls.Add(childTemplate);
                        var childNodeRepeater = (Repeater)childTemplate.FindControl("nodeRepeater");
                        childNodeRepeater.DataSource = node.Items;
                        childNodeRepeater.DataBind();
                    }
                }
            }
        }

        protected void SetAttributeIfValid(HtmlControl control, string attributeName, string attributeValue) {
            if (control != null && !string.IsNullOrEmpty(attributeValue)) {
                control.Attributes.Add(attributeName, attributeValue);
            }
        }
    }
}