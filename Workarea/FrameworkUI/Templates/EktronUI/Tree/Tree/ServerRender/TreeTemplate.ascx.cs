using System;
using System.Web;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Framework.UI.Tree;
using Ektron.Cms.Interfaces.Context;
using System.Web.UI.HtmlControls;

namespace Tree.TreeResources.Template.ServerRender {
    public partial class TreeTemplate : TreeTemplateBase {

        private ICmsContextService _iCmsContextService;

        public TreeTemplate() {
            DefaultCssClass = "ektron-ui-control ektron-ui-tree ektron-ui-genericTree";
        }

        protected ICmsContextService CmsContextService {
            get { return _iCmsContextService ?? (_iCmsContextService = ServiceFactory.CreateCmsContextService()); }
        }

        protected override PlaceHolder ControlPlaceHolder { get { return placeHolder; } }

        public override string SerializeData(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection data, bool ajaxCallback) {
            var childTemplate = Page.LoadControl(CmsContextService.UIPath + "/Templates/EktronUI/Tree/Tree/ServerRender/TreeItemTemplate.ascx");
            childContainerPlaceHolder.Controls.Add(childTemplate);
            var childNodeRepeater = (Repeater)childTemplate.FindControl("nodeRepeater");
            childNodeRepeater.DataSource = data;
            childNodeRepeater.DataBind();

            return null;
        }

        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);

            if (this.Visible) {
                Packages.EktronCoreJS.Register(this);
                Packages.jQuery.Plugins.Tmpl.Register(this);
                Packages.Ektron.JSON.Register(this);
                Packages.jQuery.jQueryUI.Widget.Register(this);

                ICmsContextService cmsContext = ServiceFactory.CreateCmsContextService();
                Ektron.Cms.Framework.UI.JavaScript.Register(this, cmsContext.UIPath + "/js/Ektron/Controls/EktronUI/Ektron.Controls.EktronUI.Tree.js");
                if (base.RegisterCss) {
                    Packages.jQuery.jQueryUI.ThemeRoller.Register(this);
                    Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Common/commonTree.css");
                    Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Tree/ektron-ui-genericTree.css");
                }

                JavaScript.RegisterJavaScriptBlock(this.placeHolder, base.GetInitializationScript(null), true);
            }
        }
   }
}