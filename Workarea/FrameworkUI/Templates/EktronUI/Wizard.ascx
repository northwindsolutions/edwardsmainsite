﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wizard.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Wizard" %>
<ektronUI:JavaScriptBlock runat="server">
    <ScriptTemplate>
        Ektron.Controls.EktronUI.Wizard.init("#<%= this.ControlContainer.ClientID %>");
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>
<div id="<%= this.ControlContainer.ClientID %>" <%= ClassAttribute %>>
    <asp:Panel ID="uxStepListing" CssClass="steps" runat="server">
        <asp:ListView ID="uxSteps" runat="server">
            <LayoutTemplate>
                <ol class="ektron-ui-clearfix">
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                </ol>
            </LayoutTemplate>
            <ItemTemplate>
                <li id="uxStep" runat="server">
                    <asp:LinkButton ID="uxGo" Enabled="true" CommandName="Go" CommandArgument='<%# Eval("Id") %>'
                        OnCommand="Go_Command" runat="server">
                        <span class="label">
                            <%# Eval("Name") %></span><asp:PlaceHolder ID="uxStatus" runat="server"><span class="separator">
                                <%= this.ControlContainer.Separator %></span> <span class="status">
                                    <%# Eval("Status") %></span></asp:PlaceHolder>
                    </asp:LinkButton>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    <fieldset class="form">
        <legend>
            <%= this.ControlContainer.CurrentStep.Name %></legend>
        <asp:Panel ID="uxStepForm" runat="server">
            <asp:PlaceHolder ID="aspContentTemplateControl" runat="server" />
        </asp:Panel>
    </fieldset>
    <asp:Panel ID="uxNavigation" CssClass="navigation" runat="server">
        <ektronUI:Button ID="uxCancel" CausesValidation="false" CssClass="cancel" DisplayMode="Button"
            OnClick="Cancel" OnClientClick="<%# this.ControlContainer.OnCancelClientClick %>"
            PrimaryIcon="Cancel" Text="<%$ Resources:Cancel %>" runat="server" />
        <ektronUI:Button ID="uxClose" CausesValidation="false" CssClass="cancel" DisplayMode="Button"
            OnClick="Close" OnClientClick="<%# this.ControlContainer.OnCloseClientClick %>"
            PrimaryIcon="Close" Text="<%$ Resources:Close %>" runat="server" />
        <ektronUI:Button ID="uxPrevious" CausesValidation="false" CssClass="previous" DisplayMode="Button"
            OnClick="Previous" PrimaryIcon="SeekPrevious" Text="<%$ Resources:Previous %>"
            runat="server" />
        <ektronUI:Button ID="uxNext" CausesValidation="true" CssClass="next" DisplayMode="Button"
            OnClick="Next" SecondaryIcon="SeekNext" Text="<%$ Resources:Next %>" runat="server" />
        <ektronUI:Button ID="uxStart" CausesValidation="true" CssClass="next" DisplayMode="Button"
            OnClick="Start" SecondaryIcon="Play" Text="<%$ Resources:Start %>" runat="server" />
        <ektronUI:Button ID="uxFinish" CausesValidation="true" CssClass="finish" DisplayMode="Button"
            OnClick="Finish" SecondaryIcon="Check" Text="<%$ Resources:Finish %>" runat="server" />
    </asp:Panel>
</div>
