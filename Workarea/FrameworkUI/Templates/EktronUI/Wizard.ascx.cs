﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.Wizard;
    using Ektron.Cms.Interfaces.Context;

    /// <summary>
    /// Display template for the Wizard server control
    /// </summary>
    public partial class Wizard : TemplateBase<EktronUI.Wizard>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wizard"/> class.
        /// </summary>
        public Wizard()
        {
            this.ID = "Wizard";
            this.Classes = new List<string>();
        }

        /// <summary>
        /// Gets the full class attribute for the outer tag of the control.
        /// </summary>
        public string ClassAttribute
        {
            get { return " class=\"" + string.Join(" ", this.Classes.ToArray()) + "\""; }
        }

        /// <summary>
        /// Gets or sets the classes to apply to the outer tag of the control.
        /// </summary>
        /// <value>
        /// The icon classes.
        /// </value>
        private List<string> Classes { get; set; }

        /// <summary>
        /// Perform the developer-provided action to exit the wizard.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Cancel(object sender, EventArgs e)
        {
            this.ControlContainer.Exit();
        }

        /// <summary>
        /// An alias for "Cancel" after finishing the wizard.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Close(object sender, EventArgs e)
        {
            this.ControlContainer.Exit();
        }

        /// <summary>
        /// Trigger the wizard completion process.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Finish(object sender, EventArgs e)
        {
            this.ControlContainer.Complete();
        }

        /// <summary>
        /// Gets the step class attribute.
        /// </summary>
        /// <param name="completed">if set to <c>true</c> step is completed.</param>
        /// <param name="current">if set to <c>true</c> step is current.</param>
        /// <param name="pending">if set to <c>true</c> step is pending.</param>
        /// <param name="summary">if set to <c>true</c> step is summary.</param>
        /// <param name="enabled">if set to <c>true</c> step is enabled.</param>
        /// <returns>
        /// The contents for an HTML class attribute.
        /// </returns>
        protected string GetStepClassAttributeValue(bool completed, bool current, bool pending, bool summary, bool enabled)
        {
            List<string> classes = new List<string>();

            // Define style classes for step state
            if (completed)
            {
                classes.Add("completed");
            }

            if (current)
            {
                classes.Add("current");
            }

            if (pending)
            {
                classes.Add("pending");
            }

            if (summary)
            {
                classes.Add("summary");
            }

            if (enabled)
            {
                classes.Add("enabled");
            }

            return string.Join(" ", classes);
        }

        /// <summary>
        /// Trigger the wizard to proceed to the next step.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Next(object sender, EventArgs e)
        {
            this.ControlContainer.Next();
        }

        /// <summary>
        /// Raises the <see cref="E:Init"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ControlContainer.InstantiateIn(this.aspContentTemplateControl);

            this.uxSteps.ItemDataBound += new EventHandler<System.Web.UI.WebControls.ListViewItemEventArgs>(this.Steps_ItemDataBound);
        }

        /// <summary>
        /// Handles the ItemDataBound event of the uxSteps control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
        protected void Steps_ItemDataBound(object sender, System.Web.UI.WebControls.ListViewItemEventArgs e)
        {
            IWizardStepView step = e.Item.DataItem as IWizardStepView;

            // Find and enable/disable the step "go to" link
            LinkButton uxGo = e.Item.FindControl("uxGo") as LinkButton;
            uxGo.Enabled = this.ControlContainer.CanGo(step);

            // Establish validation context for the "go to" link
            if (uxGo.Enabled)
            {
                uxGo.ValidationGroup = this.ControlContainer.CurrentStep.ValidationGroup;
                uxGo.CausesValidation = this.ControlContainer.CausesValidation(step);
            }

            // Find and style the step list item
            HtmlControl uxStep = e.Item.FindControl("uxStep") as HtmlControl;
            uxStep.Attributes["class"] = this.GetStepClassAttributeValue(step.IsComplete, step.IsCurrent, step.IsPending, step.IsSummary, uxGo.Enabled);

            // Find and display/hide status panel
            PlaceHolder uxStatus = e.Item.FindControl("uxStatus") as PlaceHolder;
            uxStatus.Visible = step.ShowStatus;
        }

        /// <summary>
        /// Handles the Command event of the Go control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        protected void Go_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "go")
            {
                this.ControlContainer.Go(e.CommandArgument.ToString());
            }
        }

        /// <summary>
        /// Called when initializing.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInitialize(object sender, EventArgs e)
        {
            // Do nothing
        }

        /// <summary>
        /// Called when when the control is loaded and ready.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnReady(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                // Set visual control-level classes
                this.Classes.Add("ektron-ui-control");
                this.Classes.Add(this.ControlContainer.ControlClass);
                if (!string.IsNullOrEmpty(this.ControlContainer.CssClass))
                {
                    this.Classes.Add(this.ControlContainer.CssClass);
                }

                // Update step listing
                this.uxStepListing.Visible = !this.ControlContainer.HideStepListing;

                if (!this.ControlContainer.HideStepListing)
                {
                    // Bind to enabled steps
                    this.uxSteps.DataSource = this.ControlContainer.StepListing;
                    this.uxSteps.DataBind();
                }

                // Cause buttons to validate the group for the current step
                this.UpdateButtonValidation(this.ControlContainer.CurrentStep.ValidationGroup);

                // Show the correct configuration of buttons
                this.UpdateButtonNavigation();

                // Set the correct advancement button as default when visible
                this.UpdateDefaultButton();
            }
        }

        /// <summary>
        /// Called when registering resources.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();

            // Register any resources required for styling (vs. behavior/JS) here
            Package stylePackage = new Package()
            {
                Components = new List<Ektron.Cms.Framework.UI.Component>()
                {
                    Packages.jQuery.jQueryUI.ThemeRoller,
                    Packages.Ektron.CssFrameworkBase,
                    UI.Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-wizard.css"),
                    UI.JavaScript.Create(cmsContextService.UIPath + "/js/Ektron/Controls/EktronUI/Ektron.Controls.EktronUI.Wizard.js")
                }
            };

            stylePackage.Register(this);
        }

        /// <summary>
        /// Trigger the wizard to proceed to the next step.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Previous(object sender, EventArgs e)
        {
            this.ControlContainer.Previous();
        }

        /// <summary>
        /// Alias for "Next" on the first step."
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Start(object sender, EventArgs e)
        {
            this.ControlContainer.Next();
        }

        /// <summary>
        /// Updates the validation group for the validating buttons.
        /// </summary>
        /// <param name="validationGroup">The validation group.</param>
        private void UpdateButtonValidation(string validationGroup)
        {
            this.uxFinish.ValidationGroup = validationGroup;
            this.uxNext.ValidationGroup = validationGroup;
            this.uxStart.ValidationGroup = validationGroup;
        }

        /// <summary>
        /// Updates the button navigation.
        /// </summary>
        private void UpdateButtonNavigation()
        {
            // Display the appropriate buttons
            this.uxCancel.Visible = this.ControlContainer.ShowCancel;
            this.uxClose.Visible = this.ControlContainer.ShowClose;
            this.uxFinish.Visible = this.ControlContainer.ShowFinish;
            this.uxNext.Visible = this.ControlContainer.ShowNext;
            this.uxPrevious.Visible = this.ControlContainer.ShowPrevious;
            this.uxStart.Visible = this.ControlContainer.ShowStart;
        }

        /// <summary>
        /// Updates the default button.
        /// </summary>
        private void UpdateDefaultButton()
        {
            if (this.ControlContainer.ShowNext)
            {
                this.uxStepForm.DefaultButton = this.uxNext.ID;
            }
            else if (this.ControlContainer.ShowStart)
            {
                this.uxStepForm.DefaultButton = this.uxStart.ID;
            }
            else if (this.ControlContainer.ShowFinish)
            {
                this.uxStepForm.DefaultButton = this.uxFinish.ID;
            }
        }
    }
}