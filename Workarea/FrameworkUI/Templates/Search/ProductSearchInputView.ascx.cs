﻿namespace Ektron.Cms.Framework.UI.Controls.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI.Views;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;

    public partial class ProductSearchInputView : BaseTemplate<IProductSearchView, ProductSearchController>
    {
        protected Controls.ProductSearchInputView View { get; private set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.View = (Controls.ProductSearchInputView)this.NamingContainer.Parent;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (this.Visible)
            {
                ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
                aspAdvancedSearchLink.NavigateUrl = cmsContextService.WorkareaPath + "JavascriptRequired.aspx";
                aspAdvancedSearchIcon.NavigateUrl = aspAdvancedSearchLink.NavigateUrl;

                if (!IsPostBack)
                {
                    // Render sort properties drop down if more than one
                    // sort property has been specified.

                    if (this.View.SortProperties == null || this.View.SortProperties.Count < 2)
                    {
                        aspSortProperties.Visible = false;
                        aspSortLabel.Visible = false;
                    }
                    else
                    {
                        aspSortProperties.Visible = true;
                        aspSortLabel.Visible = true;
                        aspSortProperties.DataSource = this.View.SortProperties;
                        aspSortProperties.DataBind();
                    }
                }

                // create a package that will register the UI JS and CSS we need
                Package searchControlPackage = new Package() {
                    Components = new List<Component>()
                    {
                        // Register JS Files
                        Packages.EktronCoreJS,
                        Packages.jQuery.jQueryUI.Position,
                        Packages.jQuery.Plugins.BGIframe,
                        Packages.jQuery.Plugins.ToTop,
                        UI.JavaScript.Create(cmsContextService.UIPath + "/js/Ektron/Controls/Ektron.Controls.Search.SiteSearch.js"),
                        Packages.jQuery.Plugins.BindReturnKey,
                        // Register CSS Files
                        Packages.Ektron.CssFrameworkBase,
                        UI.Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-search.css")
                    }
                };
                searchControlPackage.Register(this);
            }
        }
        protected void uxSearch_Click(object sender, EventArgs e)
        {
            if (this.View.SortProperties != null && this.View.SortProperties.Count > 0)
            {
                int propertyIndex = aspSortProperties.SelectedIndex >= 0
                    ? aspSortProperties.SelectedIndex
                    : 0;

                this.Controller.OrderBy = new SortableSearchPropertyCollection() { this.View.SortProperties[propertyIndex] };
            }
            this.Controller.Search(uxSearchText.Text);
        }

        protected void uxAdvancedSearch_Click(object sender, EventArgs e)
        {
            if (this.View.SortProperties != null && this.View.SortProperties.Count > 0)
            {
                int propertyIndex = aspSortProperties.SelectedIndex >= 0
                    ? aspSortProperties.SelectedIndex
                    : 0;

                this.Controller.OrderBy = new SortableSearchPropertyCollection() { this.View.SortProperties[propertyIndex] };
            }
            this.Controller.Search(uxWithAllWords.Text, uxWithoutWords.Text, uxAnyWords.Text, uxExactPhrase.Text);
        }
    }
}