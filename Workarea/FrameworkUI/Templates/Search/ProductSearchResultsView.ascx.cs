﻿namespace Ektron.Cms.Framework.UI.Controls.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using Ektron.Cms.Framework.UI.Views;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls;

    public partial class ProductSearchResultsView : UI.Views.BaseTemplate<IProductSearchView, IProductSearchController>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.Visible)
            {
                Packages.Ektron.CssFrameworkBase.Register(this);
            }
        }

        protected void searchResults_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HtmlGenericControl span = e.Item.FindControl("listPrice") as HtmlGenericControl;
                ProductSearchResultData item = e.Item.DataItem as ProductSearchResultData;
                if (span != null && item != null)
                {
                    try
                    {
                        span.InnerText = Ektron.Cms.Common.EkFunctions.FormatCurrency(decimal.Parse(item.ListPrice.ToString()), Ektron.Cms.ObjectFactory.GetRequestInfoProvider().GetRequestInformation().CommerceSettings.CurrencyCultureCode);
                    }
                    catch
                    {
                        span.InnerText = item.ListPrice.ToString();
                    }
                }

                span = e.Item.FindControl("salePrice") as HtmlGenericControl;
                if (span != null && item != null)
                {
                    try
                    {
                        span.InnerText = Ektron.Cms.Common.EkFunctions.FormatCurrency(decimal.Parse(item.SalePrice.ToString()), Ektron.Cms.ObjectFactory.GetRequestInfoProvider().GetRequestInformation().CommerceSettings.CurrencyCultureCode);
                    }
                    catch
                    {
                        span.InnerText = item.SalePrice.ToString();
                    }
                }
            }
        }
    }
}