﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteSearchResultsView.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.Templates.SiteSearchResultsView" %>
<div class="ektron-ui-control ektron-ui-search ektron-ui-search-results ektron-ui-search-results-site" id="<%# this.Parent.ClientID %>">
    <asp:ListView ID="aspSuggestedSpellings" runat="server" DataSource='<%# Eval("SuggestedSpellings") %>' ItemPlaceholderID="aspPlaceholder">
        <LayoutTemplate>
            <div class="section suggested-spelling">
                <p><asp:Literal ID="aspDidYouMean" runat="Server" Text="<%$ Resources:DidYouMean %>"></asp:Literal>&#160;</p>
                <ul>
                    <asp:PlaceHolder ID="aspPlaceholder" runat="server"></asp:PlaceHolder>
                </ul>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <li class="suggested-spelling">
                <asp:LinkButton ID="aspSuggestedSpellingsSearch" runat="server" Text='<%# Container.DataItem %>' OnCommand="aspSuggestedSpellingsSearch_Click" CommandArgument=' <%# Container.DataItem %>' ToolTip=' <%# Container.DataItem %>' />
            </li>
        </ItemTemplate>
    </asp:ListView>
    <div class="section suggested-results">
        <asp:ListView ID="aspSuggestedResults" runat="server" DataSource='<%# Eval("SuggestedResults") %>' ItemPlaceholderID="aspPlaceholder">
            <LayoutTemplate>
                <ul>
                    <asp:PlaceHolder ID="aspPlaceholder" runat="server"></asp:PlaceHolder>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="suggested-result ektron-ui-clearfix">
                    <h3 class="title"><a href="<%# Eval("Url") %>"><%# Eval("Title")%></a></h3>
                    <div class="summary"><%# Eval("Summary")%></div>
                    <span class="url ektron-ui-quiet"><%# Eval("Url")%></span>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div class="section no-results">
        <asp:Label ID="aspNoResults" runat="server" 
            Visible='<%# (Ektron.Cms.Framework.UI.SearchState)Eval("State") == Ektron.Cms.Framework.UI.SearchState.NoResults %>' 
            meta:resourcekey="aspNoResultsResource"></asp:Label>
    </div>
   

    <div class="section results">
        <asp:ListView ID="aspResults" runat="server" OnDataBound="searchResults_OnDataBound"  OnItemDataBound="searchResults_OnItemDataBound" DataSource='<%# Eval("Results") %>' ItemPlaceholderID="aspPlaceholder">
            <LayoutTemplate>
                <ul>
                    <asp:PlaceHolder ID="aspPlaceholder" runat="server"></asp:PlaceHolder>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="result">
                    <h3 class="title"><a id="searchurl" runat="server"></a></h3>
                    <div class="summary"><%# Eval("Summary")%></div>
                    <span class="url ektron-ui-quiet"><%# Eval("Url")%></span>
                    <span class="date ektron-ui-quiet"><%# Eval("Date")%></span>
                    <asp:HiddenField ID="aspSearchResultItemData" runat="server" />
                </li>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div class="SiteSearchResultsHandler">
        <asp:HiddenField ID="hdnHandlerPath" runat="server" ></asp:HiddenField>
    </div>
    <div class="SearchQueryInfo">
        <asp:HiddenField ID="hdnSearchQueryInfo" runat="server" ></asp:HiddenField>
    </div>
    <div class="SearchResultInfo">
        <asp:HiddenField ID="hdnSearchResultInfo" runat="server" ></asp:HiddenField>
    </div>

    <ektronUI:JavaScriptBlock ID="uxSearchQueryData" ExecutionMode="OnEktronReady" runat="server">
        <ScriptTemplate>
            $ektron("#<%# this.Parent.ClientID%>").data("searchUrl", 
                {
                    ClickSourceUrl: "<%# this.Request.Url.AbsoluteUri %>"
                }
            );
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
</div>