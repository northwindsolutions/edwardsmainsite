﻿namespace Ektron.Cms.Framework.UI.Controls.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Views;
    using Ektron.Cms.Interfaces.Context;
    using System.Web.UI.HtmlControls;
    using Ektron.Cms.Search.QueryStatistics;
    using Ektron.Newtonsoft.Json;

    public partial class SiteSearchResultsView : BaseTemplate<ISearchView, SiteSearchController>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void aspSuggestedSpellingsSearch_Click(object sender, CommandEventArgs e)
        {
            this.Controller.Search(e.CommandArgument.ToString());
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            if (this.Visible)
            {

                // create a package that will register the UI JS and CSS we need
                Package searchResultsControlPackage = new Package()
                {
                    Components = new List<Component>()
                    {
                        // Register JS Files
                        Packages.EktronCoreJS,
                        // Register CSS Files
                        Packages.Ektron.CssFrameworkBase,
                        Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-search-results.css")
                    }
                };
                if (this.Controller.EnableQueryStatistics == true)
                {
                    searchResultsControlPackage.Components.Add(Packages.Ektron.JSON);
                    searchResultsControlPackage.Components.Add(UI.JavaScript.Create(cmsContextService.UIPath + "/js/Ektron/Controls/Ektron.Controls.Search.SiteSearch.QueryStatistics.js"));
                    hdnHandlerPath.Value = cmsContextService.UIPath + "/Templates/Search/SiteSearchResultsHandler.ashx";
                }

                searchResultsControlPackage.Register(this);
            }
        }

        protected void searchResults_OnDataBound(object sender, EventArgs e)
        {
            Ektron.Cms.Framework.UI.SearchModel model = DataBinder.GetDataItem(this.NamingContainer) as Ektron.Cms.Framework.UI.SearchModel;

            if (model != null)
            {

                SearchQueryInfo queryData = new SearchQueryInfo();
                Ektron.Cms.Search.QueryStatistics.SearchType type;
                Enum.TryParse<Ektron.Cms.Search.QueryStatistics.SearchType>(model.SearchType.ToString(), out type);
                queryData.SearchType = type;
                queryData.QueryText = model.QueryText;
                queryData.WithAllWords = model.WithAllWords;
                queryData.WithAnyWord = model.WithAnyWord;
                queryData.WithoutWords = model.WithoutWords;
                hdnSearchQueryInfo.Value = JsonConvert.SerializeObject(queryData, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });

                SearchResultInfo searchResultInfo = new SearchResultInfo();
                searchResultInfo.SuggestedResultCount = model.SuggestedResults.Count;
                searchResultInfo.TotalHits = model.PageInfo.ResultCount;
                searchResultInfo.ResultsPageInfo = new Cms.Search.QueryStatistics.PageInfo();
                searchResultInfo.ResultsPageInfo.CurrentPageIndex =  model.PageInfo.CurrentPageIndex;
                searchResultInfo.ResultsPageInfo.NumberOfPages = model.PageInfo.NumberOfPages;
                searchResultInfo.ResultsPageInfo.PageSize = model.PageInfo.ResultsPerPage;
                searchResultInfo.ResultsPageInfo.PageStartOffset = model.PageInfo.StartCount;
                hdnSearchResultInfo.Value = JsonConvert.SerializeObject(searchResultInfo, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
            }
        }

        protected void searchResults_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HtmlAnchor a = e.Item.FindControl("searchurl") as HtmlAnchor;
                SiteSearchResultData item = e.Item.DataItem as SiteSearchResultData;
                if (a != null && item != null)
                {
                    a.HRef = item.Url;
                    a.InnerText = item.Title;
                    if (this.Controller.EnableQueryStatistics == true)
                    {
                        IRecordQueryStatisticsHelperService service =
                            ObjectFactory.Get<IRecordQueryStatisticsHelperService>();

                        SearchResultItem resultItem = new SearchResultItem();
                        a.Attributes.Add("onclick",
                            "return Ektron.Controls.Search.SiteSearch.QueryStatistics.addQuerySuggestion($ektron(this), '"
                                    + this.Parent.ClientID + "');");
                        HiddenField listItemHiddenField = e.Item.FindControl("aspSearchResultItemData") as HiddenField;
                        if (listItemHiddenField != null)
                        {
                            // assign values and then serialize for use on the client side
                            resultItem.Url = item.Url;
                            resultItem.Rank = item.Rank;
                            resultItem.Id = item.Id;
                            resultItem.SiteId = item.SiteId;
                            resultItem.Title = item.Title;
                            resultItem.ResultItemType = (int)ResultItemType.SearchResult;
                            resultItem.CultureInfo.LanguageId = item.LanguageId;
                            resultItem.CultureInfo.Culture = service.GetSearchCulture(item.LanguageId);
                            listItemHiddenField.Value = JsonConvert.SerializeObject(resultItem, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                        }
                    }
                }
            }
        }
    }
}