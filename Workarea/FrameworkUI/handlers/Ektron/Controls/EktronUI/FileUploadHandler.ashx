<%@ WebHandler Language="C#" Class="Ektron.Cms.Framework.UI.Controls.EktronUI.FileUpload.FileUploadHandler" %>
namespace Ektron.Cms.Framework.UI.Controls.EktronUI.FileUpload
{
    using System;
    using System.Web;
    using System.Threading;
    using System.IO;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

	public class FileUploadHandler : IHttpAsyncHandler
	{
		public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
		{
			var result = new FileUploadAsyncResult(context, cb, extraData);

			// parse the parameters sent to the request and bundle them up to be passed to the worker thread
			var processFileState = new FileUploadProcessFileState(context, result);

			// do all of the work on a custom thread pool so that the asp.net thread pool doesn't get blocked up

			// TODO: This example uses the built-in ThreadPool, but you should use a custom thread pool!  The built-in thread pool
			// draws from the same threads that the web server uses to service web requests, so you will see no benefit.
			// See the codeplex project for some links to open source custom thread pool implementations.

			// change this line to queue ProcessFile onto your custom thread pool
			ThreadPool.QueueUserWorkItem(ProcessFile, processFileState);

			return result;
		}

		/// <summary>
		/// Process the file.
		/// </summary>
		private static void ProcessFile(object state)
		{
			var processFileState = state as FileUploadProcessFileState;

			try
			{
				string fileName = processFileState.Guid;

				var uploadDirectoryPath = new FileUploadSandbox(processFileState.RelativePath).UploadDirectoryPath;

				// the path to where this file will be written on disk
				string sandboxPath = Path.Combine(uploadDirectoryPath, fileName);
				
				// Is it the first chunk or is the upload for this file being canceled? Create the file on the server
				if (processFileState.Cancel || processFileState.FirstChunk)
				{
					// if the file already exists in the sandbox, delete it
					if (System.IO.File.Exists(sandboxPath))
						System.IO.File.Delete(sandboxPath);
				}

				if (!processFileState.Cancel)
				{
					// if the process flag is set to true, the entire file has been sent, so call the custom file processor
					if (processFileState.Process && !string.IsNullOrEmpty(processFileState.UploadedFileProcessorType))
					{
						string typeName = processFileState.UploadedFileProcessorType;
						string className = typeName.Substring(0, typeName.IndexOf(",")).Trim();
						var processor = Type.GetType(typeName, true).Assembly.CreateInstance(className) as IUploadedFileProcessor;

						if (processor != null)
							processor.ProcessFile(processFileState.AsyncResult.Context, uploadDirectoryPath, processFileState.Guid, processFileState.Name, processFileState.ContextParam);
					}
					else
					{
						// this is another chunk of data
						var buffer = new byte[4096];

						using (FileStream fs = System.IO.File.Open(sandboxPath, FileMode.Append))
						{
							int bytesRead;
							while ((bytesRead = processFileState.AsyncResult.Context.Request.InputStream.Read(buffer, 0, buffer.Length)) != 0)
								fs.Write(buffer, 0, bytesRead);
						}
					}
				}

				processFileState.AsyncResult.CompleteCall();
			}
			catch (Exception ex)
			{
				processFileState.AsyncResult.Exception = ex;
				processFileState.AsyncResult.CompleteCall();
			}
		}

		/// <summary>
		/// EndProcessRequest only does exception handling for this handler.  There is no other information currently sent
		/// back to the file uploader control.
		/// </summary>
		/// <param name="result"></param>
		public void EndProcessRequest(IAsyncResult result)
		{
			var asyncOp = result as FileUploadAsyncResult;

			if (asyncOp == null) return;

			if (asyncOp.Exception != null)
			{
				asyncOp.Context.Response.Write(asyncOp.Exception.ToString());
				//Ektron.Cms.Instrumentation.Log.WriteError(asyncOp.Exception);
			}
		}

		/// <summary>
		/// State required by the asynchronous ProcessFile method.
		/// </summary>
		private class FileUploadProcessFileState
		{
			public FileUploadAsyncResult AsyncResult { get; set; }
			public bool Cancel { get; private set; }
			public bool Process { get; private set; }
			public bool FirstChunk { get; private set; }
			private bool LastChunk { get; set; }
			public bool IsAuthenicated { get; private set; }
			public string Guid { get; private set; }
			public string Name { get; private set; }
			public string UploadedFileProcessorType { get; private set; }
			public string ContextParam { get; private set; }
			public string RelativePath { get; private set; }
			public bool UniqueNamingOff { get; private set; }

			public FileUploadProcessFileState(HttpContext context, FileUploadAsyncResult result)
			{
				AsyncResult = result;
				Guid = context.Request.QueryString["guid"];
				FirstChunk = string.IsNullOrEmpty(context.Request.QueryString["first"]) ? false : bool.Parse(context.Request.QueryString["first"]);
				LastChunk = string.IsNullOrEmpty(context.Request.QueryString["last"]) ? false : bool.Parse(context.Request.QueryString["last"]);
				Cancel = string.IsNullOrEmpty(context.Request.QueryString["cancel"]) ? false : bool.Parse(context.Request.QueryString["cancel"]);
				Process = string.IsNullOrEmpty(context.Request.QueryString["process"]) ? false : bool.Parse(context.Request.QueryString["process"]);
				Name = context.Request.QueryString["name"];
				RelativePath = context.Request.QueryString["p"];
				UploadedFileProcessorType = context.Request.QueryString["processor"];
				ContextParam = context.Request.QueryString["context"];
				IsAuthenicated = context.User.Identity.IsAuthenticated;

				bool uniqueNamingOff;
				bool.TryParse(context.Request.QueryString["unique"], out uniqueNamingOff);
				UniqueNamingOff = uniqueNamingOff;
			}
		}

		private class FileUploadAsyncResult : IAsyncResult
		{
			private readonly HttpContext _context;
			private readonly AsyncCallback _cb;
			private readonly object _state;
			private ManualResetEvent _event;
			private bool _completed;
			private object _lock = new object();

			public FileUploadAsyncResult(HttpContext context, AsyncCallback cb, object state)
			{
				_context = context;
				_cb = cb;
				_state = state;
			}

			public object AsyncState { get { return _state; } }

			public WaitHandle AsyncWaitHandle
			{
				get
				{
					lock (_lock)
					{
						if (_event == null)
							_event = new ManualResetEvent(IsCompleted);
						return _event;
					}
				}
			}

			public bool CompletedSynchronously { get { return false; } }

			public bool IsCompleted { get { return _completed; } }

			public HttpContext Context { get { return _context; } }

			/// <summary>
			/// If an exception occurs in the worker thread, a reference is set here, and then EndProcessRequest will
			/// write it to the output.
			/// </summary>
			public Exception Exception { get; set; }

			/// <summary>
			/// Call this when work is done.
			/// </summary>
			public void CompleteCall()
			{
				lock (_lock)
				{
					_completed = true;
					if (_event != null) _event.Set();
				}

				if (_cb != null) _cb(this);
			}
		}

		public bool IsReusable
		{
			get { return false; }
		}

		public void ProcessRequest(HttpContext context)
		{
			// never called
		}
	}
}