<%@ Page language="c#" ValidateRequest="true" %>
<%
    Ektron.Cms.Interfaces.Context.ICmsContextService cmsContextService = Ektron.Cms.Framework.UI.ServiceFactory.CreateCmsContextService();
    Ektron.Cms.CommonApi api = new Ektron.Cms.CommonApi();

    string pluginsToLoad = String.Empty;
    if (!String.IsNullOrEmpty(Request.QueryString["plugins"]))
    {
        pluginsToLoad = Uri.UnescapeDataString(Request.QueryString["plugins"]);
        if (api.RequestInformationRef.IsMembershipUser == 1 || api.RequestInformationRef.UserId == 0)
        {
            pluginsToLoad = pluginsToLoad.Replace(",ektron/translate,", ",");
        }
    }

    if (Request.Cookies["ecm"] != null && Request.Cookies["ecm"]["UserCulture"] != null)
    {
        Int32 UserCulture = Int32.Parse(Request.Cookies["ecm"]["UserCulture"].ToString());
        System.Globalization.CultureInfo currentculture = System.Globalization.CultureInfo.CurrentCulture;
        if (UserCulture == 0)
        {
            UserCulture = 1033;
        }
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(UserCulture); 
    }

    string resourceTextReview = GetLocalResourceObject("Ektron.Controls.Editor.Aloha.Tabs.ResourceText.Review").ToString();
    string resourceTextFile = GetLocalResourceObject("Ektron.Controls.Editor.Aloha.Tabs.ResourceText.File").ToString();
%>    
var Aloha = window.Aloha || ( window.Aloha = {} );

Aloha.settings = {
    jQuery: window.$ektron,
    logLevels: { 'error': true, 'warn': false, 'info': false, 'debug': false, 'deprecated': false },
    errorhandling: false,
    baseUrl: "<%= cmsContextService.UIPath%>/js/ektron/Controls/EktronUI/Editor/Aloha/lib",
    basePath: "<%= cmsContextService.UIPath %>/js/ektron/Controls/EktronUI/Editor/Aloha/plugins/",
    ribbon: false,
    locale: 'en',
    sidebar: { disabled: true },
    plugins: {
        load: "<%= pluginsToLoad %>",
        block: { 
            defaults : {
                '.ektron-imageset-wrapper': {   
                    'aloha-block-type': 'DefaultBlock'
                } 
            }
        }
    },
    contentHandler: {
    insertHtml: [ 'word', 'generic', 'sanitize' ],
      initEditable: [ 'blockelement' ],
      getContents: ['basic'],
      sanitize: 'relaxed',
      handler: {
        generic: {
          transformFormattings: false // this will disable the transformFormattings method in the generic content handler
        },
        sanitize: {
          '.aloha-captioned-image-caption': { elements: [ 'em', 'strong' ] }
        },
      }
	},
    templatePlugin: {
        FolderID: 0
    },
    fontfamilies: [
                'Georgia, serif',
                '"Palatino Linotype", "Book Antiqua", Palatino, serif',
                '"Times New Roman", Times, serif',
                'Arial, Helvetica, sans-serif',
                '"Arial Black", Gadget, sans-serif',
                '"Comic Sans MS", cursive, sans-serif',
                '"Lucida Sans Unicode, Lucida Grande", sans-serif',
                'Tahoma, Geneva, sans-serif',
                '"Trebuchet MS", Helvetica, sans-serif',
                'Verdana, Geneva, sans-serif',
                '"Courier New", Courier, monospace',
                '"Lucida Console", Monaco, monospace'
            ],
    toolbar: {
        tabs: [
                {
					label: "tab.format.label"
				},
				{
					label: "tab.insert.label",
					showOn: { scope: 'Aloha.continuoustext' },
					components: [
						[
							'hyperlink', 'library', 'template', 'embed'
						]
					]
				}
                <% string matchPlugins = pluginsToLoad.ToLower();
                   if (matchPlugins.Contains("ektron/validator") || matchPlugins.Contains("ektron/translate") || matchPlugins.Contains("ektron/inspector") || matchPlugins.Contains("ektron/sourceview"))
                   { %>
                ,
				{
					label: "<%= resourceTextReview%>",
					showOn: { scope: 'Aloha.continuoustext' },
					components: [
						[
							'validator', 'translate', 'inspector', 'sourceview', 'inboundwriter'
						]
					]
                }
				<% } %>
                <% if(matchPlugins.Contains("ektron/editincontext")) {%>
                ,
				{
					label: "<%= resourceTextFile%>",
					showOn: { scope: 'Aloha.continuoustext' },
					components: [
						[
							'save', 'cancel'
						]
					]
				}
                <% } %>
			]	
    }
};