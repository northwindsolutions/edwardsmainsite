// define the Dialog Mod module using RequireJS
define([
    'aloha',
    'jquery',
    'ui/ui'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
        $,
        Ui) {
        var defaultOptions,
            dialogElement = $('<div class="ektron-aloha-dialog"></div>');
        $(document).ready(function() {
            dialogElement.prependTo("body");
        
            dialogElement.dialog({
                autoOpen: false,
                draggable: true,
                resizable: true,
                modal: true,
                zIndex: 100001,
                title: '',
            });
            if (typeof dialogElement.data('dialog') != 'undefined') {
                defaultOptions = dialogElement.data('dialog').options;
                dialogElement.prop('_ektronAlohaDialogDefaultOptions', defaultOptions);
            }
        });
        return function() {
            return dialogElement;
        };
    }
);