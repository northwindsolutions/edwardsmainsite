﻿(function ($ektron) {
    var _init = $ektron.ui.dialog.prototype._init;

    //Optional top margin for page, wont let a user move a dialog into this spot.
    var topMargin = 0;

    //Custom Dialog Init
    $ektron.ui.dialog.prototype._init = function () {
        var self = this;
        if (this.options.title != "Inspector")
            return;
        _init.apply(this, arguments);
        uiDialogTitlebar = this.uiDialogTitlebar;

        //we need two variables to preserve the original width and height so that can be restored.
        this.options.originalWidth = this.options.width;
        this.options.originalHeight = this.options.height;

        //save a reference to the resizable handle so we can hide it when necessary.
        this.resizeableHandle = this.uiDialog.resizable().find('.ui-resizable-se');

//        //Save the height of the titlebar for the minimize operation
//        this.titlebarHeight = parseInt(uiDialogTitlebar.css('height')) + parseInt(uiDialogTitlebar.css('padding-top')) + parseInt(uiDialogTitlebar.css('padding-bottom')) + parseInt(this.uiDialog.css('padding-top')) + parseInt(this.uiDialog.css('padding-bottom'));
        //<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"><span aria-hidden="true" title="undefined" data-ux-icon=""></span></span></button>
        uiDialogTitlebar.append('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-restore" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-newwin"></span></button>');
        uiDialogTitlebar.append('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-minimize" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-minusthick"></span></button>');

        //Minimize Button
        this.uiDialogTitlebarMin = $ektron('.ui-dialog-titlebar-minimize', uiDialogTitlebar).click(function () {
            self.minimize();
            return false;
        });
        //Restore Button
        this.uiDialogTitlebarRest = $ektron('.ui-dialog-titlebar-restore', uiDialogTitlebar).click(function () {
            self.restore();
            self.moveToTop(true);
            return false;
        });

        //restore the minimize button on close
        this.uiDialog.bind('dialogbeforeclose', function (event, ui) {
            self.uiDialogTitlebarRest.hide();
            self.uiDialogTitlebarMin.show();
        });
        this.minimize();

        $ektron(window).on('resize', function (event) {
            if(self.uiDialog.position().top < 0) { self.uiDialog.position().top = 20; }
            self.uiDialog.css('position', 'fixed');
        });
    };
    //Custom Dialog Functions
    $ektron.extend($ektron.ui.dialog.prototype, {
        restore: function () {
            this.uiDialog.resizable({disabled: false});
            this.uiDialog.draggable("option", "disabled", false)
                        .on('drag', function (event, ui) {
                            if(ui.position.top < 0) { ui.position.top = 20; }
                        });
            this.uiDialog.css('position', 'fixed');
            //We want to prevent the dialog from expanding off the screen
            var windowHeight = $ektron(window).height();
            var dialogHeight = this.options.originalHeight;
            var dialogTop = parseInt(this.uiDialog.css('top'));
            if (isNaN(dialogTop)) {
                dialogTop = $ektron(this.uiDialog).position().top;
            }
            if (dialogHeight > windowHeight) {
                dialogHeight = windowHeight - 10;
                this.uiDialog.css('top', 0);
            }
            $ektron('#advInspectorm').height(dialogHeight - 55);
            var windowWidth = $ektron(window).width();
            var dialogWidth = this.options.originalWidth;
            var dialogLeft = parseInt(this.uiDialog.css('left'));
            if (dialogWidth + dialogLeft > windowWidth) {
                //there are 2 pixels of padding per css
                var newLeft = windowWidth - dialogWidth - 2;
                this.uiDialog.css('left', newLeft);
            }
            this.uiDialog.css({ width: this.options.originalWidth, height: dialogHeight });
            this.element.show();
            this.titlebarHeight = parseInt(uiDialogTitlebar.css('height')) + parseInt(uiDialogTitlebar.css('padding-top')) + parseInt(uiDialogTitlebar.css('padding-bottom')) + parseInt(this.uiDialog.css('padding-top')) + parseInt(this.uiDialog.css('padding-bottom'));

            this.resizeableHandle.show();
            this.uiDialogTitlebarRest.hide();
            this.uiDialogTitlebarMin.show();
        },
        minimize: function () {
            //Store the original height/width
            this.uiDialog.resizable({disabled: true});
            this.uiDialog.draggable({disabled: false});
            this.uiDialog.css('position', 'fixed');
            this.options.originalWidth = this.options.width;
            this.options.originalHeight = this.options.height;

            this.titlebarHeight = parseInt(uiDialogTitlebar.css('height')) + parseInt(uiDialogTitlebar.css('padding-top')) + parseInt(uiDialogTitlebar.css('padding-bottom')) + parseInt(this.uiDialog.css('padding-top')) + parseInt(this.uiDialog.css('padding-bottom'));
            this.uiDialog.animate({ width: 200, height: this.titlebarHeight }, 200);
            this.element.hide();

            this.uiDialogTitlebarMin.hide();
            this.uiDialogTitlebarRest.show();
            this.resizeableHandle.hide();
            this.uiDialog.css({ 'bottom': 0, 'top': 'auto' });
            this.uiDialog.removeClass('ui-state-disabled');
        }
    });
})($ektron);