﻿{
    "DIV": {
        "styles": {
            "Indent":
            { 
                "type": "style",
                        "value": "padding-left:20px;",
                        "workareaBox": "padding-left:20px;"
            },
        "Center":
                { 
                    "type": "style",
                            "value": "text-align:center;",
                            "workareaBox": "text-align:center;"
                },
        "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
        "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
        "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                }

        }
    },
    "P": {
        "styles": {
            "No spacing": 
            {
                "type": "style",
                        "value": "margin:0;",
                        "workareaBox": "margin:0;"
            },
            "Alert": 
            {
                "type": "style",
                        "value": "color:red;",
                        "workareaBox": "color:red;"
            },
	        "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                }

        }
    },

    "H1": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                },
       "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }

        }
    },
    "H2": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                },
       "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }

        }
    },
    "H3": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                },
       "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }

        }
    },
    "H4": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                },
       "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }

        }
    },
    "H5": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                },
       "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }

        }
    },
    "H6": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
            "Center":
                { 
                    "type": "style",
                            "value": "text-align: center;",
                            "workareaBox": "text-align: center;"
                },
            "Left Align":
                { 
                    "type": "style",
                            "value": "text-align:left;",
                            "workareaBox": "text-align:left;"
                },
            "Right Align":
                { 
                    "type": "style",
                            "value": "text-align:right;",
                            "workareaBox": "text-align:right;"
                },
            "Justify":
                { 
                    "type": "style",
                            "value": "text-align:justify;",
                            "workareaBox": "text-align:justify;"
                },
       "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }

        }
    },

    "SPAN": {
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Underline":
                { 
                    "type": "style",
                            "value": "text-decoration:underline;",
                            "workareaBox": "text-decoration:underline;"
                },
	    "Strikethrough":
                { 
                    "type": "style",
                            "value": "text-decoration:line-through;",
                            "workareaBox": "text-decoration:line-through;"
                },
	    "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }
        }
    },
    "STRONG":{
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
                "Alert2": 
                {
                    "type": "style",
                    "value": "color:blue;",
                    "workareaBox": "color:blue;"
                },
	    "Underline":
                { 
                    "type": "style",
                            "value": "text-decoration:underline;",
                            "workareaBox": "text-decoration:underline;"
                },
	    "Strikethrough":
                { 
                    "type": "style",
                            "value": "text-decoration:line-through;",
                            "workareaBox": "text-decoration:line-through;"
                },
	    "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }
	   
        }
    },
    "B":{
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Underline":
                { 
                    "type": "style",
                            "value": "text-decoration:underline;",
                            "workareaBox": "text-decoration:underline;"
                },
	    "Strikethrough":
                { 
                    "type": "style",
                            "value": "text-decoration:line-through;",
                            "workareaBox": "text-decoration:line-through;"
                },
	    "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }
	   
        }
    },
    "EM":{
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Underline":
                { 
                    "type": "style",
                            "value": "text-decoration:underline;",
                            "workareaBox": "text-decoration:underline;"
                },
	    "Strikethrough":
                { 
                    "type": "style",
                            "value": "text-decoration:line-through;",
                            "workareaBox": "text-decoration:line-through;"
                },
	    "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }
	   
        }
    },
    "I":{
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Underline":
                { 
                    "type": "style",
                            "value": "text-decoration:underline;",
                            "workareaBox": "text-decoration:underline;"
                },
	    "Strikethrough":
                { 
                    "type": "style",
                            "value": "text-decoration:line-through;",
                            "workareaBox": "text-decoration:line-through;"
                },
	    "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }
	   
        }
    },
    "BLOCKQUOTE":{
        "styles": {
            "Alert": 
            {
                "type": "style",
                "value": "color:red;",
                "workareaBox": "color:red;"
            },
	    "Underline":
                { 
                    "type": "style",
                            "value": "text-decoration:underline;",
                            "workareaBox": "text-decoration:underline;"
                },
	    "Strikethrough":
                { 
                    "type": "style",
                            "value": "text-decoration:line-through;",
                            "workareaBox": "text-decoration:line-through;"
                },
	    "Indent":
                { 
                    "type": "style",
                            "value": "padding-left:20px;",
                            "workareaBox": "padding-left:20px;"
                },
	    "lowercase text":
                { 
                    "type": "style",
                            "value": "text-transform: lowercase;",
                            "workareaBox": "text-transform: lowercase;"
                },	    
	  "Uppercase Text":
                { 
                    "type": "style",
                            "value": "text-transform: uppercase;",
                            "workareaBox": "text-transform: uppercase;"
                }
        }
    },

    "IMG":{
        "styles": {
            "Text Right": 
            { 
                "type": "style",
                "value": "float:left; margin:1em",
                "workareaBox": "padding-left:1em;background-image:url('[FrameworkUIPath]/images/silk/icons/image.png');background-repeat:no-repeat;background-position:left;"
            },
"Border": 
                { 
                    "type": "style",
                    "value": "border-width:.1em; border-style:solid;",
                    "workareaBox": "padding-left:1em;background-image:url('[FrameworkUIPath]/images/silk/icons/image.png');background-repeat:no-repeat;background-position:left;"
                },

             "Border and Text Right": 
                { 
                    "type": "style",
                    "value": "float:left; margin:1em;border-width:.1em; border-style:solid;",
                    "workareaBox": "padding-left:1em;background-image:url('[FrameworkUIPath]/images/silk/icons/image.png');background-repeat:no-repeat;background-position:left;"
                },
            "Text Left": 
                { 
                    "type": "style",
                    "value": "float:right; margin:1em",
                    "workareaBox": "padding-right:1em;background-image:url('[FrameworkUIPath]/images/silk/icons/image.png');background-repeat:no-repeat;background-position:right;"
                },
            "Border and Text Left": 
                { 
                    "type": "style",
                    "value": "float:right; margin:1em:1em;border-width:.1em; border-style:solid;",
                    "workareaBox": "padding-right:1em;background-image:url('[FrameworkUIPath]/images/silk/icons/image.png');background-repeat:no-repeat;background-position:right;"
                }
        } 
    },
    "OL":{
        "styles": {
            "lowercase-roman": 
            {
                "type": "style",
                "value": "list-style-type:lower-roman;",
                "workareaBox": "list-style-type: lower-roman;"
            },
	    "Uppercase-Roman":
                { 
                    "type": "style",
                            "value": "list-style-type: upper-roman;",
                            "workareaBox": "list-style-type: upper-roman;"
                },
	    "Uppercase Alpha":
                { 
                    "type": "style",
                            "value": "list-style-type: upper-alpha;",
                            "workareaBox": "list-style-type: upper-alpha;"
                },
	    "lowercase-alpha":
                { 
                    "type": "style",
                            "value": "list-style-type: lower-alpha;",
                            "workareaBox": "list-style-type: lower-alpha;"
                }
	   
        }
    },
    "UL":{
        "styles": {
            "Circle Bulleted": 
            {
                "type": "style",
                "value": "list-style-type: circle;",
                "workareaBox": "list-style-type: circle;"
            },
	    "Square Bulleted":
                { 
                    "type": "style",
                            "value": "list-style-type: Square;",
                            "workareaBox": "list-style-type: Square;"
                },
	    "Disc Bulleted":
                { 
                    "type": "style",
                            "value": "list-style-type: disc;",
                            "workareaBox": "list-style-type: disc;"
                }
	    	   
        }
    },
"TABLE":{
    "styles": {
        "Clean 100% width": 
        {
            "type": "style",
            "value": "width: 100%; border-spacing: 0; border-collapse: collapse;",
            "workareaBox": ""
        },
        "Clean 75% width": 
            {
                "type": "style",
                "value": "width: 75%; border-spacing: 0; border-collapse: collapse;",
                "workareaBox": ""
            },
        "Clean 50% width": 
            {
                "type": "style",
                "value": "width: 50%; border-spacing: 0; border-collapse: collapse;",
                "workareaBox": ""
            },
        "Clean 25% width": 
            {
                "type": "style",
                "value": "width: 25%; border-spacing: 0; border-collapse: collapse;",
                "workareaBox": ""
            },

        "Solid Border 100% width": 
            {
                "type": "style",
                "value": "width: 100%; border-spacing: 0; border-collapse: collapse; border: .1em solid;",
                "workareaBox": "border: .1em solid;"
            },
        "Solid Border 75% width": 
            {
                "type": "style",
                "value": "width: 75%; border-spacing: 0; border-collapse: collapse; border: .1em solid;",
                "workareaBox": "border: .1em solid;"
            },
        "Solid Border 50% width": 
            {
                "type": "style",
                "value": "width: 50%; border-spacing: 0; border-collapse: collapse; border: .1em solid;",
                "workareaBox": "border: .1em solid;"
            },
        "Solid Border 25% width": 
            {
                "type": "style",
                "value": "width: 25%; border-spacing: 0; border-collapse: collapse;border: .1em solid;",
                "workareaBox": "border: .1em solid;"
            },
"Dashed Border 100% width": 
            {
                "type": "style",
                "value": "width: 100%; border-spacing: 0; border-collapse: collapse; border: .01em dashed;",
                "workareaBox": "border: .01em dashed;"
            },
        "Dashed Border 75% width": 
            {
                "type": "style",
                "value": "width: 75%; border-spacing: 0; border-collapse: collapse; border: .01em dashed;",
                "workareaBox": " border: .01em dashed;"
            },
        "Dashed Border 50% width": 
            {
                "type": "style",
                "value": "width: 50%; border-spacing: 0; border-collapse: collapse; border: .01em dashed;",
                "workareaBox": " border: .01em dashed;"
            },
        "Dashed Border 25% width": 
            {
                "type": "style",
                "value": "width: 25%; border-spacing: 0; border-collapse: collapse;border: .01em dashed;",
                "workareaBox": "border: .01em dashed;"
            }
	   
    }
},
    "TR":{
        "styles": {
            "Grey Row": 
            {
                "type": "style",
                "value": "background: none repeat scroll 0 0 #E8EDFF;",
                "workareaBox": "background: none repeat scroll 0 0 #E8EDFF;"
            },
	       "Yellow Row": 
                {
                    "type": "style",
                    "value": "background: none repeat scroll 0 0 #FFFF00;",
                    "workareaBox": "background: none repeat scroll 0 0 #FFFF00;"
                },
"Blue Row": 
                {
                    "type": "style",
                    "value": "background: none repeat scroll 0 0 #6699FF;",
                    "workareaBox": "background: none repeat scroll 0 0 #6699FF;"
                },
"Red Row": 
                {
                    "type": "style",
                    "value": "background: none repeat scroll 0 0 #FF0000;",
                    "workareaBox": "background: none repeat scroll 0 0 #FF0000;"
                }
        }
    },
 "TH":{
     "styles": {
         "Clean Bottom Lined": 
         {
             "type": "style",
             "value": "border-bottom: .2em solid;font-weight: strong;padding: .8em .6em;",
             "workareaBox": "border-bottom: .2em solid;font-weight: strong;"
         },
         "Grey Horizonal Boxed": 
             {
                 "type": "style",
                 "value": "background: none repeat scroll 0 0 #CCCCCC;border-bottom: .01em solid #FFFFFF;padding: .5em;",
                 "workareaBox": "background: none repeat scroll 0 0 #CCCCCC;border-bottom: .01em solid #FFFFFF;"
             },
         "Grey Vertical Boxed": 
             {
                 "type": "style",
                 "value": "background: none repeat scroll 0 0 #CCCCCC;border-left: .01em solid #FFFFFF; border-right: .01em solid #FFFFFF; padding: .5em;",
                 "workareaBox": "background: none repeat scroll 0 0 #CCCCCC;border-left: .01em solid #FFFFFF; border-right: .01em solid #FFFFFF;"
             }
     }
 },

 "TD":{
     "styles": {
         "Horizonal Lined": 
         {
             "type": "style",
             "value": "border-bottom: .01em solid;padding: .4em .2em;",
             "workareaBox": "border-bottom: .01em solid;;"
         },
         "Vertical Lined": 
             {
                 "type": "style",
                 "value": "border-left: .01em solid; border-right: .01em solid; padding: .4em .2em;",
                 "workareaBox": "border-left: .01em solid; border-right: .01em solid;"
             },
         "Grey Horizontal Boxed": 
             {
                 "type": "style",
                 "value": "background: none repeat scroll 0 0 #F0F0F0;border-bottom: .01em solid #FFFFFF;border-top: .01em solid transparent;padding: .5em;",
                 "workareaBox": "background: none repeat scroll 0 0 #F0F0F0;border-bottom: .01em solid #FFFFFF;border-top: .01em solid transparent;;"
             },
         "Grey Vertical Boxed": 
             {
                 "type": "style",
                 "value": "background: none repeat scroll 0 0 #F0F0F0;border-left: .01em solid #FFFFFF;border-right: .01em solid #FFFFFF;padding: .5em;",
                 "workareaBox": "background: none repeat scroll 0 0 #F0F0F0;border-left: .01em solid #FFFFFF;border-right: .01em solid #FFFFFF;"
             }
     }
 }

}