﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../../../../../../../../Ektron.Namespace.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var namespace = "ektron-aloha-advanced-link", advancedlinkidlink, advancedlinkidemail, advancedlinkidsubject, advancedlinkidurl, advancedlinkidbookmark,
        advancedbookmarkid, advancedotherbookmarkid, advancedlinkradio, advancedlinkidtitle, ddadvancedlinkid, advancedlinkidemailtext, advancedlinkidurltext, advancedlinklinkselect,
        advancedlinklinkremove, advancededitlinks, advancededitbookmarks, advancedbookmarktext, advancedlinkbookmarkremove, modal,
		WorkareaPath = Ektron.Context.Cms.WorkareaPath;

    function Advancedlink(id, obj, i18n, Dialog) {
        this.id = id;
        this.targetElem = obj;
        this.currenteditable = "";
        advancedlinkidlink = "ektron-aloha-advancedlink-link" + this.id;
        advancedlinkidemail = "ektron-aloha-advancedlink-email" + this.id;
        advancedlinkidemailtext = "ektron-aloha-advancedlink-email-text" + this.id;
        advancedlinkidsubject = "ektron-aloha-advancedlink-email-subject" + this.id;
        advancedlinkidurl = "ektron-aloha-advancedlink-url" + this.id;
        advancedlinkidurltext = "ektron-aloha-advancedlink-url-text" + this.id;
        advancedlinkidbookmark = "ektron-aloha-advancedlink-bookmark" + this.id;
        advancedbookmarkid = 'ektron-aloha-advancedlink-bookmark-dropdown' + this.id;
        advancedotherbookmarkid = "ektron-aloha-advancedlink-bookmark-other" + this.id;
        advancedlinkidtitle = "ektron-aloha-advancedlink-title" + this.id;
        ddadvancedlinkid = "ektron-aloha-advancedlink-dropdown" + this.id;
        advancedlinklinkselect = "ektron-aloha-advancedlink-select" + this.id;
        advancedlinklinkremove = "ektron-aloha-advancedlink-remove" + this.id;
        advancededitlinks = "ektron-aloha-advancededit-links" + this.id;
        advancededitbookmarks = "ektron-aloha-advancededit-bookmarks" + this.id;
        advancedbookmarktext = "ektron-aloha-advancedtext-bookmark" + this.id;
        advancedlinkbookmarkremove = "ektron-aloha-advancedbookmark-remove" + this.id;


        this.create = function () {
            //create jquery object for Basic-Link module
            var advancedlink = $ektron('<fieldset></fieldset>'), advancedlinkedit, advancedlinkelem, advancedlinkclick, advancedlinkcheck, that = this;
            advancedlink.attr('id', this.id);
            //div to show edit urllink/bookmarklink, emaillink
            advancedlinkedit = $ektron('<div class="' + advancededitlinks + '">');
            //  Link/Email select buttons
            advancedlinkelem = $ektron('<div class="' + that.nsClass('linktype') + '">');
            $ektron('<div><label class="' + that.nsClass('type-label') + '">' + i18n.t('basiclink.label.type') + '</label></div>').appendTo(advancedlinkelem);
            advancedlinkclick = $ektron('<input id="' + advancedlinkidlink + '" type="button" />').val(i18n.t('basiclink.button.link.label')).appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.linkSelected(); });
            advancedlinkclick = $ektron('<input id="' + advancedlinkidemail + '" type="button" />').val(i18n.t('basiclink.button.email.label')).appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.emailSelected(); });
            advancedlinkelem.appendTo(advancedlinkedit);
            //  URL/Existing Bookmark Select
            advancedlinkelem = $ektron('<div class="' + that.nsClass('linkselect') + '">');
            $ektron('<div><label class="' + that.nsClass('type-label') + '">' + i18n.t('basiclink.label.url') + ':</label></div>').appendTo(advancedlinkelem);
            advancedlinkclick = $ektron('<input id="' + advancedlinkidurl + '" type="radio" name="advlinksource" checked = "checked" />').appendTo(advancedlinkelem);
            $ektron('<label for="' + advancedlinkidurl + '">' + i18n.t('basiclink.label.url') + '</>').appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.urlSelected(); });
            advancedlinkclick = $ektron('<input id="' + advancedlinkidbookmark + '" type="radio" name="advlinksource" />').appendTo(advancedlinkelem);
            $ektron('<label for="' + advancedlinkidbookmark + '">' + i18n.t('basiclink.label.existingbookmark') + '</>').appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.bookmarkSelected(); });
            advancedlinkelem.appendTo(advancedlinkedit);

            // TODO: Put elements that vary by selection here

            // input for Link/URL
            advancedlinkelem = $ektron('<div class="' + that.nsClass('linkselecturl') + '">');
            advancedlinkclick = $ektron('<span><input id="' + advancedlinkidurltext + '" type="text"  /></span>').appendTo(advancedlinkelem);
            advancedlinkclick.on('input', function () { that.urlChanged(); });
            advancedlinkclick = $ektron('<span><input id="' + advancedlinklinkselect + '" type="button" class="' + that.nsClass('selectlibraryurl') + '" /></span>').appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.linkSelectButtonClick(); });
            advancedlinkclick = $ektron('<span><input id="' + advancedlinklinkremove + '" type="button" class="' + that.nsClass('removelibraryurl') + '" /></span>').appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.linkRemoveButtonClick(); });
            advancedlinkelem.appendTo(advancedlinkedit);

            // input for Link/Bookmark
            advancedlinkelem = $ektron('<div style="display: none;" class="' + that.nsClass('bookmarkselecturl') + '">');
            advancedlinkclick = $ektron('<select id="' + advancedbookmarkid + '" class="select-box"><option>Select</option></select>').appendTo(advancedlinkelem);
            advancedlinkclick.on('change', function () { that.bookmarkLinkChanged(); });

            $ektron('<span>&nbsp;or # </span>').appendTo(advancedlinkelem);
            advancedlinkclick = $ektron('<span><input id="' + advancedotherbookmarkid + '" type="text" class="input-bookmark" /></span>').appendTo(advancedlinkelem);
            advancedlinkclick.on('input', function () { that.bookmarkLinkChanged(); });


            advancedlinkelem.appendTo(advancedlinkedit);

            // email only, input email address regex to confirm '@' plus at least one '.'
            advancedlinkelem = $ektron('<ul style="display: none;" class="' + that.nsClass('emailaddress') + '">');
            $ektron('<li><label for="' + advancedlinkidemailtext + '">' + i18n.t('basiclink.text.label.email') + '</></li>').appendTo(advancedlinkelem);
            advancedlinkclick = $ektron('<input id="' + advancedlinkidemailtext + '" type="text" />').appendTo(advancedlinkelem);
            advancedlinkclick.on('blur', function () { that.buildMailToLink(); });
            $ektron('<li><label for="' + advancedlinkidsubject + '">' + i18n.t('basiclink.text.label.subject') + '</></li>').appendTo(advancedlinkelem);
            advancedlinkclick = $ektron('<li><input id="' + advancedlinkidsubject + '" type="text" /></li>').appendTo(advancedlinkelem);
            advancedlinkclick.on('input', function () { that.buildMailToLink(); });
            advancedlinkelem.appendTo(advancedlinkedit);

            // Common Text with varying Label
            //  URL/Existing Bookmark Select
            advancedlinkelem = $ektron('<ul class="' + that.nsClass('titlesubject') + '">');
            $ektron('<li><label for="' + advancedlinkidtitle + '">' + i18n.t('basiclink.text.label.title') + '</></li>').appendTo(advancedlinkelem);
            advancedlinkclick = $ektron('<li><input id="' + advancedlinkidtitle + '" type="text" /></li>').appendTo(advancedlinkelem);
            advancedlinkclick.on('input', function () { that.titleChanged(); });
            advancedlinkelem.appendTo(advancedlinkedit);

            // Dropdown for links only (not email)
            advancedlinkelem = $ektron('<div class="' + that.nsClass('targetselect') + '">');
            advancedlinkcheck = $ektron('<select id="' + ddadvancedlinkid + '" class="select-box">').html('<option value="_self">' + i18n.t('advancedlink.text.select.same') + '</option><option value="_blank">' + i18n.t('advancedlink.text.select.new') + '</option><option value="_parent">' + i18n.t('advancedlink.text.select.parent') + '</option><option value="_top">' + i18n.t('advancedlink.text.select.browser') + '</option>').appendTo(advancedlinkelem);
            advancedlinkcheck.on('change', function () { that.linkTargetSelectClick(); });
            //$ektron('<label for="' + ddadvancedlinkid + '">' + i18n.t('advancedlink.check.label.target') + '</>').appendTo(advancedlinkelem);
            advancedlinkelem.appendTo(advancedlinkedit);
            advancedlinkedit.appendTo(advancedlink);
            //Bookmarks only (anchor tag with ID)
            advancedlinkedit = $ektron('<div class="' + advancededitbookmarks + '">');
            advancedlinkelem = $ektron('<div class="' + that.nsClass('linktype') + '">');
            $ektron('<div><label class="' + that.nsClass('type-label') + '">' + i18n.t('basiclink.label.type') + '</label></div>').appendTo(advancedlinkelem);
            $ektron('<input id="' + advancedlinkidlink + '" type="button" />').val(i18n.t('basiclink.button.bookmark.label')).appendTo(advancedlinkelem);
            advancedlinkelem.appendTo(advancedlinkedit);

            // Populate selected bookmark
            advancedlinkelem = $ektron('<div class="' + that.nsClass('linkbookmark') + '">');
            advancedlinkclick = $ektron('<input id="' + advancedbookmarktext + '" type="text" />').appendTo(advancedlinkelem);
            advancedlinkclick.on('input', function () { that.bookmarkIdChanged(); });
            advancedlinkclick.on('blur', function () { that.fillBookmarkList(); });

            advancedlinkclick = $ektron('<span><input id="' + advancedlinkbookmarkremove + '" type="button" class="' + that.nsClass('removebookmark') + ' input-box" /></span>').appendTo(advancedlinkelem);
            advancedlinkclick.on('click', function () { that.linkRemoveButtonClick(); });
            $ektron('<div>' + i18n.t("basiclink.label.existingbookmarks") + '</div>').appendTo(advancedlinkelem);
            $ektron('<p><textarea class="' + this.nsClass('existingbookmark-field') + '" readonly="readonly"></textarea></p>').appendTo(advancedlinkelem);
            advancedlinkelem.appendTo(advancedlinkedit);

            advancedlinkedit.appendTo(advancedlink);
            return advancedlink;
        };
        // Show link type selector options
        this.linkSelected = function () {
            var radioselect = '';
            $ektron('.' + this.nsClass('linkselect')).show();
            $ektron('.' + this.nsClass('linkselecturl')).show();
            $ektron('.' + this.nsClass('emailaddress')).hide();
            //Reset Link to URL/Bookmark settings
            radioselect = $('input[name="advlinksource"]:checked').attr('id');
            if (radioselect === advancedlinkidurl) {
                this.urlSelected();
            }
            if (radioselect === advancedlinkidbookmark) {
                this.bookmarkSelected();
            }

        };
        // Show email type selector options
        this.emailSelected = function () {
            $ektron('.' + this.nsClass('linkselecturl')).hide();
            $ektron('.' + this.nsClass('linkselect')).hide();
            $ektron('.' + this.nsClass('targetselect')).hide();
            $ektron('.' + this.nsClass('emailaddress')).show();
            $ektron('.' + this.nsClass('bookmarkselecturl')).hide();
            $ektron('.' + this.nsClass('titlesubject')).hide();
            //$ektron('#' + advancedlinkidtitle).hide();

        };
        // Show URL type selector options
        this.urlSelected = function () {
            $ektron('.' + this.nsClass('targetselect')).show();
            $ektron('.' + this.nsClass('bookmarkselecturl')).hide();
            $ektron('.' + this.nsClass('linkselecturl')).show();
            $ektron('.' + this.nsClass('titlesubject')).show();

        };
        // Show Bookmark type selector options
        this.bookmarkSelected = function () {
            $ektron('.' + this.nsClass('linkselecturl')).hide();
            $ektron('.' + this.nsClass('targetselect')).show();
            $ektron('.' + this.nsClass('bookmarkselecturl')).show();
            $ektron('.' + this.nsClass('titlesubject')).show();
            var bookmarkList = this.getBookmarks();
            this.updateBookmarkLists(bookmarkList);
            //this.updateBookmarkLists();
        };
        //Prepend Namespace method
        this.nsClass = function () {
            var stringBuilder = [], prefix = namespace;
            jQuery.each(arguments, function () {
                stringBuilder.push(this == '' ? prefix : prefix + '-' + this);
            });
            return jQuery.trim(stringBuilder.join(' '));
        };
        //Initialize modal
        this.init = function (obj) {
            this.currenteditable = obj;
            this.populateLinkFields();
            //populate URL fields
            if (Ektron.Namespace.Register) {
                Ektron.Namespace.Register('Ektron.AdvancedInspector.Link.Parent');
                Ektron.Namespace.Register('Ektron.AdvancedInspector.Link.Advanced');
                Ektron.AdvancedInspector.Link.Advanced.AcceptLink = this.acceptLink;
                Ektron.AdvancedInspector.Link.Parent;

                Ektron.Namespace.Register('Ektron.AdvancedInspector');
                Ektron.AdvancedInspector.Dialog = Dialog();
            }

            //Need to stop event propagation so edittable class is not removed
            $ektron(".ektron-aloha-advinspector-modal").find('#' + this.id).on('mousedown', function (event) {
                event.stopPropagation();
            });
        };
        //Open library modal to quicklinks
        this.linkSelectButtonClick = function (elem) {
            var that = this,
            dialogOptions = $.extend({}, Ektron.AdvancedInspector.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                dialogClass: 'ektron-ux ektron-ux-dialog',
                autoOpen: false,
                width: 820,
                modal: true,
                zIndex: 100000001,
                closeText: '<span class="ui-icon ui-icon-closethick" title="' + i18n.t('advanced-inspector.label.close') + '" />',
                title: i18n.t('advanced-inspector.label.library')
            });
            Ektron.AdvancedInspector.Link.Parent = 'advanced';
            Ektron.AdvancedInspector.Dialog.html('<div class="ektron-aloha-inspector-modal"><iframe id="ektron-aloha-inspector-modal-iframe" class="ektron-aloha-inspector-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
            Ektron.AdvancedInspector.Dialog.dialog('option', dialogOptions);

            Ektron.AdvancedInspector.Dialog.dialog('open');

            var titlebar = Ektron.AdvancedInspector.Dialog.parents('.ui-dialog').find('.ui-dialog-titlebar');
            titlebar.find('.ui-dialog-titlebar button').remove();
            $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-closethick"></span></button>')
                .appendTo(titlebar)
                .click(function () {
                    Ektron.AdvancedInspector.Dialog.dialog('close');
                });

            $ektron("iframe.ektron-aloha-inspector-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/mediamanager.aspx?actiontype=library&scope=Quicklinks&type=quicklinks&EditorName=Aloha&autonav=0");
        };

        //Remove an a tag and clear the current item from the href Field
        this.linkRemoveButtonClick = function (elem) {
            var range = Aloha.Selection.getRangeObject(),
                    foundMarkup = this.targetElem;//this.findLinkMarkup();

            // clear the current item from the href field
            this.targetElem.href = null;
            if (foundMarkup) {
                // remove the link
                GENTICS.Utils.Dom.removeFromDOM(foundMarkup, range, true);

                range.startContainer = range.endContainer;
                range.startOffset = range.endOffset;

                // select the (possibly modified) range
                range.select();
                //* @deprecated see aloha.js line: 28700
                //if (typeof terminateLinkScope == 'undefined' ||
                //		terminateLinkScope === true) {
                //    Scopes.setScope('Aloha.continuoustext');
                //}
            }
        };

        //Insert quicklink from library
        this.acceptLink = function (htmlObj) {
            var that = this,
                foundMarkup,
                h = {
                    href: "",
                    title: "",
                    type: ""
                };

            jQuery.extend(h, htmlObj);

            //update text and property - as this does not trigger input event
            $ektron('#' + advancedlinkidurltext).val(h.href);
            $ektron(obj).attr('href', h.href);
            $ektron(obj).attr('data-ektron-url', h.href);

            Ektron.AdvancedInspector.Dialog.dialog("close");
        };


        // retrieve the existing bookmarks of the current content 
        this.getBookmarks = function (delimeter) {
            var editor = jQuery('#' + this.currenteditable).get(0),
                sBookmarkList = "";
            if (jQuery('.aloha-editable-active').length > 0) {
                editor = jQuery('.aloha-editable-active').get(0);
            }
            if (editor != "") {
                var bookmarks = [],
                    docAnchors = editor.getElementsByTagName('A');

                if (docAnchors && docAnchors.length > 0) {
                    for (var i = 0; i < docAnchors.length; i++) {
                        if (docAnchors[i].id.length > 0) {
                            bookmarks.push(docAnchors[i].id);
                        }
                    }
                    sBookmarkList = bookmarks.sort();
                }
            }
            return (sBookmarkList);
        };


        //Populate Existing Bookmarks dropdown (First entry defined as Bookmark -> use bookmark in adjoining text box)
        this.updateBookmarkLists = function (bookmarkList) {
            var itemlist = $ektron('#' + advancedbookmarkid);
            $(itemlist).children('option:not(:first)').remove();
            jQuery.each(bookmarkList, function () {
                $('<option>' + this + '</option>').appendTo(itemlist);
            });
            // bookmark tab
            //jQuery('textarea' + this.nsSel('existingbookmark-field')).val(bookmarkList.replace(/\:/g, " \n"));
            //// link tab
            //bookmarkSelectList = bookmarkList.replace(/\:/g, "</option><option>");
            //jQuery(this.nsSel('linkExistingBookmarks')).html("<option></option><option>" + bookmarkSelectList + "</option>");
        };

        ///methods for modifying Fields
        //Update url changed - take link from textbox
        this.urlChanged = function () {
            this.targetElem.href = $ektron('#' + advancedlinkidurltext).val();
            $ektron(obj).attr('data-ektron-url', $ektron('#' + advancedlinkidurltext).val());
        };

        //UPdate bookmark changed. if dropdown value is Select, get value from other textbox
        this.bookmarkLinkChanged = function () {
            if ($ektron('#' + advancedbookmarkid)[0].selectedIndex === 0) {
                this.targetElem.href = '#' + $ektron('#' + advancedotherbookmarkid).val();
                $ektron(obj).attr('data-ektron-url', '#' + $ektron('#' + advancedotherbookmarkid).val());
                $('#' + advancedotherbookmarkid).prop('disabled', false);
            }
            else {
                this.targetElem.href = '#' + $ektron('#' + advancedbookmarkid).val();
                $ektron('#' + advancedotherbookmarkid).val('');
                $ektron(obj).attr('data-ektron-url', '#' + $ektron('#' + advancedbookmarkid).val());
                $('#' + advancedotherbookmarkid).prop('disabled', true);
            }

        };


        //Update title/subject changed - take text from textbox
        this.titleChanged = function () {
            this.targetElem.title = $ektron('#' + advancedlinkidtitle).val();
        };

        //Update bookmark id changed. 
        this.bookmarkIdChanged = function () {
            this.targetElem.id = $ektron('#' + advancedbookmarktext).val();
            //No url for Bookmark
            $ektron(obj).removeAttr('data-ektron-url');
        };
        //set target for link on checkbox value
        this.linkTargetSelectClick = function () {
            this.targetElem.target = $ektron('#' + ddadvancedlinkid).val();
        };

        //set checkbox for link on target value
        this.advLinkTargetSet = function () {
            $ektron('#' + ddadvancedlinkid).val(this.targetElem.target);
        };

        //parse email address from href
        // remove mailto, parse address and subject on '?' querystring Subject
        this.populateEmail = function () {
            var address, subject, qindex;
            address = this.targetElem.href.replace('mailto:', '');
            qindex = address.indexOf("?");
            // no subject text
            if (qindex < 0) {
                $ektron('#' + advancedlinkidemailtext).val(address);
            }
            else {
                $ektron('#' + advancedlinkidemailtext).val(address.substr(0, qindex));
                $ektron('#' + advancedlinkidsubject).val(decodeURIComponent(address.substring(qindex + 9)));
            }
            $ektron(obj).attr('data-ektron-url', this.targetElem.href);
        };

        ////parse email subject from href
        //this.getEmailSubject = function () {

        //};

        //regex email for '@' followed by '.' characters
        //encodeURIComponent on Subject field
        this.buildMailToLink = function () {
            var prefix = 'mailto:', address, subject;
            //email regex:  \b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b
            address = $ektron('#' + advancedlinkidemailtext).val();
            var myRegexp = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i;
            var match = myRegexp.exec(address);
            if (null === match) {
                alert("Invalid email address");
                event.stopPropagation();
                event.preventDefault();
            }
            else {

                subject = $ektron('#' + advancedlinkidsubject).val();
            }
            if (subject.length > 0) {
                this.targetElem.href = prefix + address + '?Subject=' + encodeURIComponent(subject);
            }
            else {
                this.targetElem.href = prefix + address;
            }
            $ektron(obj).attr('data-ektron-url', $ektron('#' + advancedlinkidurltext).val());


        };
        //Parse Href for Uemail, URL, ookmark
        this.populateLinkFields = function () {
            var itemlist, exists = false;
            if (this.targetElem.id === "") {
                // Disable Delete/Edit Bookmark
                $ektron("." + advancededitbookmarks).hide();
                var hashindex = 0, bookmark = "";
                hashindex = this.targetElem.href.indexOf('#');
                if (hashindex < 1) {
                    if (this.targetElem.href.substr(0, 6).toLowerCase() === 'mailto') {
                        this.populateEmail();
                        this.emailSelected();
                    }
                    else {
                        // URL link
                        $ektron('#' + advancedlinkidurltext).val($ektron(obj).attr('data-ektron-url'));
                        $ektron('#' + advancedlinkidtitle).val(this.targetElem.title);
                        this.advLinkTargetSet();
                    }
                }
                else {
                    // has '#'-> bookmark link
                    $ektron("#" + advancedlinkidbookmark).prop('checked', true);
                    this.bookmarkSelected();
                    $ektron('#' + advancedlinkidtitle).val(this.targetElem.title);
                    bookmark = this.targetElem.href.substring(hashindex + 1);
                    //TODO: check if bookmark is in dropdown, link may be to other content
                    itemlist = $ektron('#' + advancedbookmarkid + ' option').slice(1);

                    $(itemlist).each(function () {
                        if (this.value === bookmark) {
                            exists = true;
                            return false;
                        }
                    });
                    if (exists) {
                        $ektron('#' + advancedbookmarkid).val(bookmark);
                        $('#' + advancedotherbookmarkid).prop('disabled', true);
                    }
                    else {
                        $ektron('#' + advancedotherbookmarkid).val(bookmark);
                    }
                    this.advLinkTargetSet();
                }
            }
            else {
                // Allow Delete/Edit Bookmark
                $ektron("." + advancededitbookmarks).show();
                $ektron("." + advancededitlinks).hide();

                this.fillBookmarkList();
                //hashindex = this.targetElem.href.indexOf('#');
                //bookmark = this.targetElem.href.substring(hashindex + 1);
                $ektron("#" + advancedbookmarktext).val(this.targetElem.id);
            }
        };

        this.fillBookmarkList = function () {
            var bookmarks = this.getBookmarks();
            var bookmarkList = $ektron('.' + this.nsClass('existingbookmark-field'));
            var bookmarkText = "";
            jQuery.each(bookmarks, function () {
                bookmarkText = bookmarkText + this + ' \n';
            });
            bookmarkList.html(bookmarkText);
        };
    }
    return Advancedlink;
});


//$ektron('<input id="btnResetClassStyle" type="button" class="button" />').val(i18n.t('reset-style.label.button'));
//'<p class="' + pl.nsClass('link-type') + '"><label class="' + pl.nsClass('type-label') + '">' + Ektron.Controls.Editor.Aloha.Plugins.Link.ResourceText.typeLabel + '</label>' +
//'<a href="#tab-link" class="' + pl.nsClass('link-button') + '">' + Ektron.Controls.Editor.Aloha.Plugins.Link.ResourceText.linkLabel + '</a>' +
//'<a href="#tab-bookmark" class="' + pl.nsClass('bookmark-button') + '">' + Ektron.Controls.Editor.Aloha.Plugins.Link.ResourceText.bookmarkLabel + '</a></p>' +
