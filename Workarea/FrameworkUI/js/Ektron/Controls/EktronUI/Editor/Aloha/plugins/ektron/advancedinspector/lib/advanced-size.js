﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-advanced-size';
    function AdvancedSizeBlock(id, obj, i18n) {
        this.id = id;
        this.targetElem = {
            elem : obj,
            tagName : 'img',
            ratio : 1,
            width : 0,
            height : 0
        }; 

        this.create = function () {
            var sizeUnitOptions = "<option>px</option><option>%</option>", 
                advancedSizeBlock, advancedSizeCheckbox, advancedSizeLabel, advancedSizeGroup, advancedSizeList, sizeInput, sizeSelect, ProportionsCheckbox,
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'),  
                currUnit = "px", currVal = 0,
                that = this, i;
            this.deserializeStyle(currStyle);
            this.inspectElement();
            advancedSizeBlock = document.createElement('fieldset');
            advancedSizeBlock.setAttribute('id', this.id);
            advancedSizeBlock.className = pluginNamespace;

            //fieldset checkbox and label
            advancedSizeCheckbox = $ektron('<input type="checkbox" id="chk-advancedSize" />');
            advancedSizeCheckbox.on('click', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasSize()) {
                advancedSizeCheckbox.attr('checked', true);
            }
            advancedSizeBlock.appendChild(advancedSizeCheckbox.get(0)); 
            advancedSizeLabel = document.createElement('label');
            advancedSizeLabel.setAttribute('for', 'chk-advancedSize');
            advancedSizeLabel.setAttribute('title', i18n.t('advanced-size.checkbox.size'));
            advancedSizeLabel.innerHTML = i18n.t('advanced-size.checkbox.size'); 
            advancedSizeBlock.appendChild(advancedSizeLabel); 

            // fields group
            advancedSizeGroup = document.createElement('ul');
            // Contrain Proportions checkbox
            if (this.targetElem.tagName !== 'table') {
                advancedSizeList = document.createElement('li');
                advancedSizeList.className = 'constrainProportions-row';
                ProportionsCheckbox = $ektron('<input type="checkbox" id="chk-constrainProportions" />');
                ProportionsCheckbox.on('click', function () { that.updateAspectRatio(this); that.updateStyle(); });
                if (this.hasSize() && this.keepProportion()) {
                    ProportionsCheckbox.attr('checked', true);
                }
                advancedSizeList.appendChild(ProportionsCheckbox.get(0));
                advancedSizeLabel = document.createElement('label');
                advancedSizeLabel.setAttribute('for', 'chk-constrainProportions');
                advancedSizeLabel.setAttribute('title', i18n.t('advanced-size.label.constrain-proportions'));
                advancedSizeLabel.className = this.nsClass('label');
                advancedSizeLabel.innerHTML = i18n.t('advanced-size.label.constrain-proportions');
                advancedSizeList.appendChild(advancedSizeLabel);
                advancedSizeGroup.appendChild(advancedSizeList);
            }

            // width value
            advancedSizeList = document.createElement('li');
            advancedSizeLabel = document.createElement('label');
            advancedSizeLabel.setAttribute('for', 'txtWidth');
            advancedSizeLabel.setAttribute('title', i18n.t('advanced-size.label.width'));
            advancedSizeLabel.className = this.nsClass('label');
            advancedSizeLabel.innerHTML = i18n.t('advanced-size.label.width');
            advancedSizeList.appendChild(advancedSizeLabel);
            advancedSizeGroup.appendChild(advancedSizeList);
            advancedSizeList = document.createElement('li');
            sizeInput = $ektron('<input id="txtWidth" type="text" class="input-box width textbox-narrow" />');
            sizeInput.on('blur', function () { that.constrainProportions(this); that.updateStyle(); });
            if (this.targetElem.width !== 0) { 
				currVal = this.targetElem.width.match(/\d/g).join('');
                sizeInput.val(currVal);
				currUnit = this.targetElem.width.replace(currVal, "");
            }
            advancedSizeList.appendChild(sizeInput.get(0));
            sizeSelect = $ektron('<select id="ddWidthUnit" class="select-box ddWidthUnit">').html(sizeUnitOptions);
            sizeSelect.on('blur', function () { that.updateStyle(); });
            sizeSelect.on('change', function () { that.updateFields(this); that.updateStyle(); });
            if (this.targetElem.width !== 0) {
                sizeSelect.val(currUnit);
            }
            advancedSizeList.appendChild(sizeSelect.get(0));
            advancedSizeGroup.appendChild(advancedSizeList);
            
            // height value
            if (this.targetElem.tagName !== 'table') {
                advancedSizeList = document.createElement('li');
                advancedSizeList.className = 'height-row';
                advancedSizeLabel = document.createElement('label');
                advancedSizeLabel.setAttribute('for', 'txtHeight');
                advancedSizeLabel.setAttribute('title', i18n.t('advanced-size.label.height'));
                advancedSizeLabel.className = 'spacing-label';
                advancedSizeLabel.innerHTML = i18n.t('advanced-size.label.height');
                advancedSizeList.appendChild(advancedSizeLabel);
                advancedSizeGroup.appendChild(advancedSizeList);
                advancedSizeList = document.createElement('li');
                advancedSizeList.className = 'height-row';
                sizeInput = $ektron('<input id="txtHeight" type="text" class="input-box height textbox-narrow" />');
                sizeInput.on('blur', function () { that.constrainProportions(this); that.updateStyle(); }).on('input', function() { $ektron(this).removeClass('grey-text'); });
                if (this.targetElem.height !== 0) {
                    currVal = this.targetElem.height.match(/\d/g).join('');
                    sizeInput.val(currVal);
                    currUnit = this.targetElem.height.replace(currVal, "");
                }
                advancedSizeList.appendChild(sizeInput.get(0));
                advancedSizeLabel = document.createElement('label');
                advancedSizeLabel.setAttribute('for', 'txtHeight');
                advancedSizeLabel.setAttribute('title', currUnit);
                advancedSizeLabel.className = 'spacing-label unit';
                advancedSizeLabel.innerHTML = currUnit;
                advancedSizeList.appendChild(advancedSizeLabel);
                advancedSizeGroup.appendChild(advancedSizeList);
            }

            advancedSizeBlock.appendChild(advancedSizeGroup); 
            if (!this.hasSize()) {
                advancedSizeGroup.setAttribute('style', 'display:none');
            }
            if (this.targetElem.tagName !== 'table' && 0 === this.targetElem.height && true === $ektron('#chk-constrainProportions', advancedSizeBlock).is(':checked')) {
                this.updateAspectRatio($ektron('#chk-constrainProportions', advancedSizeBlock).get(0));
                $ektron('#txtHeight', advancedSizeBlock).addClass('grey-text').val('auto');
            }
            return advancedSizeBlock;
        };

        this.updateAspectRatio = function (checkbox) {
            var jWidthBox, jHeightBox, jParentFieldset = $ektron(checkbox).closest('fieldset'), jResult = $ektron(this.targetElem.elem);
            if (true === checkbox.checked) { 
                jResult.removeAttr('data-ektron-constrainProportions');
                jWidthBox = $ektron('#txtWidth', jParentFieldset);
                jHeightBox = $ektron('#txtHeight', jParentFieldset);
                if (!isNaN(jHeightBox.val()) && jHeightBox.val() > 0) {
                    this.targetElem.ratio = jWidthBox.val() / jHeightBox.val();
                }
                else {
                    var tureImg = new Image();
                    tureImg.src = this.targetElem.elem.getAttribute('src');
                    this.targetElem.ratio = tureImg.width / tureImg.height;
                }
            }
            else {
                jResult.attr('data-ektron-constrainProportions', false);
        }
        }

        this.constrainProportions = function (textbox) {
            var jThis = $ektron(textbox), jWidthBox, jHeightBox, 
                jParentFieldset = jThis.closest('fieldset'), 
                jWidthUnit = $ektron('.ddWidthUnit', jParentFieldset).val();
            if (true === $ektron('#chk-constrainProportions', jParentFieldset).is(':checked') && this.targetElem.ratio > 0) { 
                if (jThis.hasClass('width')) {
                    jWidthBox = jThis;
                    jHeightBox = $ektron('#txtHeight', jParentFieldset);
                    jHeightBox.val(Math.ceil(jWidthBox.val() / this.targetElem.ratio));
                }
                else {
                    jWidthBox = $ektron('#txtWidth', jParentFieldset);
                    jHeightBox = jThis;
                    jWidthBox.val(Math.ceil(jHeightBox.val() * this.targetElem.ratio));
                }
            }
        };

        this.updateFields = function (selectbox) {
            var jThis = $ektron(selectbox), 
                jParentFieldset = jThis.closest('fieldset'), 
                jWidthUnit = $ektron('.ddWidthUnit', jParentFieldset).val(),
                jWidthBox = $ektron('#txtWidth', jParentFieldset), jHeightBox;
            $ektron('.unit[for=txtHeight]', jParentFieldset).html(jWidthUnit);
            if ('%' === jWidthUnit) {
                jHeightBox = $ektron('#txtHeight', jParentFieldset);
                if (jWidthBox.val() > 100) {
                    jWidthBox.val('100');
                }
                if (jHeightBox.val() > 100) {
                    jHeightBox.val('100');
                }
                $ektron('li.height-row').hide();
                $ektron('li.constrainProportions-row').hide();
            }
            else {
                $ektron('li.height-row').show();
                $ektron('li.constrainProportions-row').show(); 
            }
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem.elem), unit, that = this, jSelectbox;
            if (true === document.getElementById('chk-advancedSize').checked) { 
                if (!that.hasSize()) {
                    if ('IMG' == this.targetElem.tagName) {
                        var tureImg = new Image();
                        tureImg.src = this.targetElem.elem.getAttribute('src');
                        jResult.css('width', tureImg.width).css('height', tureImg.height);
                    }
                    else {
                        jResult.css('width', '100%');
                        jSelectbox = $ektron('.select-box', document.getElementById(this.id)).val('%');
                        this.updateFields(jSelectbox.get(0));
                        $ektron('input.width', document.getElementById(this.id)).val('100');
                    }
                }
                else {
            jResult.css('width', "").css('height', "");
                unit = $ektron('.select-box', document.getElementById(this.id)).val();
                $ektron('.input-box', document.getElementById(this.id)).each(function (index, value) {
                    var jThis = $ektron(this);
                    if (jThis.hasClass('width') && !isNaN(jThis.val())) {
                        styleString = jThis.val() + unit;
                        jResult.css('width', styleString);
                    }

                    if (that.targetElem.tagName !== 'table') {
                        if (jThis.hasClass('height') && !isNaN(jThis.val())) {
                            styleString = jThis.val() + unit;
                            jResult.css('height', styleString);
                        }
                    }
                });
                }
            }
            else {
                jResult.css('width', "").css('height', "").removeAttr('data-ektron-constrainProportions');
            }
                $ektron(document).trigger('updateDimensions');
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, strStyle;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
                arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]);
                    if (0 === strStyle.indexOf('width:'))
                    {
                        arrProperty = strStyle.split(':');
                        this.targetElem.width = $ektron.trim(arrProperty[1]); 
                    }

                    if  (0 === strStyle.indexOf('height:')) {
                        arrProperty = strStyle.split(':');
                        this.targetElem.height = $ektron.trim(arrProperty[1]); 
                    }
                }

                if (this.targetElem.height > 0) {
                    this.targetElem.ratio = this.targetElem.width / this.targetElem.height;
                }
            }
        };

        this.inspectElement = function () {
            this.targetElem.tagName = this.targetElem.elem.tagName.toLowerCase(); 
        };

        this.hasSize = function () {
            if (typeof this.targetElem.elem.style !== 'undefined' && (this.targetElem.elem.style.width.length > 0 || this.targetElem.elem.style.height.length > 0)) {
                return true;
            }
            return false;
        };

        this.keepProportion = function () { 
            var dataAttr = this.targetElem.elem.getAttribute('data-ektron-constrainProportions');
            if (null == dataAttr || 'undefined' == typeof dataAttr) {
                return true;
            }
            else {
            return false;
            }
        };

        this.showHideModule = function (elem) {
            //$ektron(elem).siblings('ul').toggle();
            var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                jElem.siblings('ul').show();
            }
            else {
                jElem.siblings('ul').hide();
            }
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return AdvancedSizeBlock;
});
