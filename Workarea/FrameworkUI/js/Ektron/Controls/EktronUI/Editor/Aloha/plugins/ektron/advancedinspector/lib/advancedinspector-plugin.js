/*!
* Aloha Embed Plugin
* -----------------
* This plugin provides an interface to allow the user to access the CMS400 embed, 
* and to add assests from the template to the editable container.
* It presents its user interface in the Toolbar and a modal dialog.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'aloha/plugin',
    'vendor/ektron/ektron.aloha.dialog',
    'i18n!aloha/nls/i18n',
    'i18n!advancedinspector/nls/i18n',
    'advancedinspector/colorhelper',
    'vendor/ektron/ColorPicker/minicolors',
    'advancedinspector/DialogExtend',
    'block/blockmanager',
    'vendor/ektron/jquerypicture/jquery-picture-min',
    'css!./css/advancedinspector',
    'css!vendor/ektron/ColorPicker/minicolors',
    'css!./css/responsive-image',
    'css!./css/basic-link',
    'css!../../../../../../../../../../../FrameworkUI/css/Ektron/Controls/ektron-ui-message',
    'css!../../../../../../../../../../../../UX/vendor/jQuery/UI/css/ektron-ux/jquery-ui-complete.min.css'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
			Plugin,
            Dialog,
            i18nCore,
            i18n,
            ColorHelper,
            BlockManager) {
        // members
        var namespace = "ektron-aloha-advinspector-",
            modal = $ektron(".ektron-aloha-advinspector-modal"),
            trails = [],
            activeId = "",
            BasicStyle = null,
            BasicSize = null,
            AdvancedSize = null,
            i18nCore = null,
            ListStyle = null,
            TableStyle = null,
            Position = null,
            SpacingStyle = null,
            Border = null,
            Background = null,
            ResponsiveImage = null,
            TextTreatment = null,
            TextAlignment = null,
            BasicLink = null,
            AdvancedLink = null,
            ResetStyle = null,
            ImageProperties = null,
            TableProperties = null,
            advDialog = null,
            styleConfigJson = null,
            FigAltTag = null;

        // create and register the Plugin
        return Plugin.create("advinspector", {
            defaults: {},

            init: function () {
                var that = this, path = Ektron.Context.Cms.UIPath;

                this.createModal();
                this.getStyleConfig();

                require(['' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/basic-style.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/list-style.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/basic-size.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/advanced-size.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/basic-link.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/advanced-link.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/table-style.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/position.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/spacing-style.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/border.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/background.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/responsive-image.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/text-treatment.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/text-alignment.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/reset-style.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/image-properties.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/table-properties.js',
                         '' + path + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/AdvancedInspector/lib/alttag-figure.js',
                         '' + Ektron.Context.Cms.SitePath + '/ux/vendor/jquery/plugins/jcarousel/jquery.jcarousel.min.js'],
                    function (basicstyle, liststyle, basicsize, advancedsize, basiclink, advancedlink, tablestyle, position, spacingstyle, border, background, responsiveimage, texttreatment, textalignment, resetstyle, imageproperties, tableproperties, figalttag) {
                        BasicStyle = basicstyle;
                        ListStyle = liststyle;
                        BasicSize = basicsize;
                        AdvancedSize = advancedsize;
                        BasicLink = basiclink;
                        AdvancedLink = advancedlink;
                        TableStyle = tablestyle;
                        Position = position;
                        SpacingStyle = spacingstyle;
                        Border = border;
                        Background = background;
                        ResponsiveImage = responsiveimage;
                        TextTreatment = texttreatment;
                        TextAlignment = textalignment;
                        ResetStyle = resetstyle;
                        ImageProperties = imageproperties;
                        TableProperties = tableproperties;
                        FigAltTag = figalttag;
                    });
                Aloha.bind('aloha-editable-created', function (event, editable) {
                    if (Ektron.Namespace.Register) {
                        //Ektron.Namespace.Register('Ektron.AdvancedInspector.ImageSetBlock');
                        //Ektron.AdvancedInspector.ImageSetBlock.ImageSetInit = that.ImageSetInit;
                        //Ektron.AdvancedInspector.ImageSetBlock.ImageSetMakeClean = that.makeClean;
                        //Ektron.AdvancedInspector.ImageSetBlock.ImageSetDeactivate = that.ImageSetDeactivate;
                    }

                    that.advDialog.parent().show();
                    setTimeout(function () {
                        var uiDialogTitlebar = $ektron('div#advInspectorm').parent().find('.ui-dialog-titlebar'),
							dialogbarHeight = parseInt(uiDialogTitlebar.css('height')) + parseInt(uiDialogTitlebar.css('padding-top')) + parseInt(uiDialogTitlebar.css('padding-bottom'));
                        if ($ektron('button.ui-dialog-titlebar-minimize').is(':visible')) {
                            $ektron('button.ui-dialog-titlebar-minimize').click();
                        }
                        that.advDialog.parent().css('height', dialogbarHeight).css('left', '250px');
                        that.advDialog.css('height', 615 - dialogbarHeight - 15);
                    }, 500);


                    editable.obj.find('.ektron-responsive-imageset').each(function (index, elem) {
                        var obj = $ektron(elem);
                        obj.picture();
                        obj.find('img').css('max-width', '100%');
                        //that.ImageSetInit(elem);
                        that.setFocusedImageSet(undefined);
                    });
                });
                Aloha.bind('aloha-editable-activated', function (event, myeditable) {
                    activeId = myeditable.editable.obj[0].id;
                    that.initClickEffects(myeditable);
                    that.advDialog.parent().show();
                    //that.ImageSetInit();
                });

                Aloha.bind('aloha-editable-deactivated', function (event, myeditable) {
                    activeId = myeditable.editable.obj[0].id;
                    var obj = Aloha.jQuery(myeditable.editable.obj[0]);
                    obj.find('.ektron-responsive-imageset').each(function (index, elem) {
                        that.ImageSetDeactivate(elem);
                        //if ($ektron(elem).children('noscript').length == 0) {
                        //    var img = document.createElement('img');
                        //    img.src = $ektron(elem).attr('data-ektron-image-src');
                        //    var noscript = $ektron('<noscript></noscript>').append(img);
                        //    $ektron(elem).append(noscript);
                        //    //$ektron(elem).children('noscript').append();
                        //}
                    });
                    obj.find('.ektron-imageset-wrapper > img').each(function (index, elem) {
                        $ektron(elem).unwrap();
                    });
                    obj.find('.aloha-cleanme').remove();
                    $ektron('*[data-ektron-highlight="true"]').removeAttr("data-ektron-highlight");
                });


                $ektron(document).on("minimizeInspector", function () {
                    $ektron("div[aria-describedby='advInspectorm'] a.dialog-minimize").click();
                });
                //$ektron('body').on('click', '*', function (event) {
                //    console.log(this);
                //    event.stopPropagation();
                //});

                $ektron('li#liContent, li#liSummary').on('click', function () {
                    that.advDialog.parent().show();
                });
                $ektron('li#liMetadata, li#liSchedule, li#liComment, li#liTemplates, li#liAlias').on('click', function () {
                    that.advDialog.parent().hide();
                });
                $ektron('.ektron-aloha-advinspector-modal .advanced, .ektron-aloha-advinspector-modal .basic').text('No advanced properties or styles available, create/click an element to activate');
                //$ektron('.ektron-aloha-advinspector-modal').parent().show();
            },

            /* Helpers
            ----------------------------------*/
            initClickEffects: function (myeditable) {
                var that = this;
                this.setFocusedImageSet(undefined);
                $ektron('#' + myeditable.editable.obj[0].id + '').off('click');
                $ektron('#' + myeditable.editable.obj[0].id + '').on('click', '*', function (event) {
                    that.buildWindow(this, true);
                    that.createTrail(this);

                    if (!$ektron('.ektron-aloha-advinspector-modal #insAdvanced').children().length) {
                        $ektron('.ektron-aloha-advinspector-modal .advanced').text('No advanced properties or styles available');
                    }
                    if (!$ektron('.ektron-aloha-advinspector-modal #insBasic').children().length) {
                        $ektron('.ektron-aloha-advinspector-modal .basic').text('No basic properties or styles available');
                    }

                    event.stopPropagation();
                    event.preventDefault();
                });
            },

            buildWindow: function (clickedon, killTrail) {
                $ektron('.ektron-aloha-advinspector-modal .basic').empty();
                $ektron('.ektron-aloha-advinspector-modal .advanced').empty();
                if (killTrail) {
                    $ektron('.ektron-aloha-advinspector-modal .trail').empty();
                }
                $(document).off("ekcolorChanged", colorChangedHandler);
                $(document).on("ekcolorChanged", colorChangedHandler);
                // newMessage event handler
                function colorChangedHandler(e) {
                    $ektron(e.obj).val(e.message);
                    $ektron(e.obj).trigger('input');

                }
                if ($ektron('#advShowHighlights').prop('checked')) {
                    $ektron('*[data-ektron-highlight="true"]').removeAttr("data-ektron-highlight");
                    $ektron(clickedon).attr("data-ektron-highlight", true);
                }

                switch (clickedon.nodeName) {
                    case "P":
                    case "EM":
                    case "I":
                    case "B":
                    case "SPAN":
                    case "STRONG":
                    case "H1":
                    case "H2":
                    case "H3":
                    case "H4":
                    case "H5":
                    case "ABBR":
                    case "ADDRESS":
                    case "Q":
                    case "SMALL":
                    case "SUP":
                    case "SUB":
                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var resetstyle = new ResetStyle("resetid", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);

                        var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                        texttreatment.start();

                        var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);

                        var spacing = new SpacingStyle("spacingid1", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                        var border = new Border("borderid1", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                        border.init();

                        var resetstyleadv = new ResetStyle("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyleadv);
                        break;
                    case "SECTION":
                    case "ARTICLE":
                    case "MAIN":
                    case "ASIDE":
                    case "HGROUP":
                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var basicsize = new BasicSize("id", clickedon);
                        var block = basicsize.create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(block);
                        basicsize.start();

                        var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                        texttreatment.start();

                        var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);

                        var spacing = new SpacingStyle("spacingid1", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                        var border = new Border("borderid1", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                        border.init();

                        var background = new Background("backgroundid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(background.create());
                        background.start();

                        var position = new Position("sectionposid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(position.create());

                        var advancedsize = new AdvancedSize("imageadvid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedsize.create());

                        var resetstyle = new ResetStyle("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);
                        var resetstyleadv = new ResetStyle("resetA", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyleadv);

                        break;
                    case "DD":
                    case "DT":
                        var thisinstance = new BasicStyle("ddid", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                        texttreatment.start();

                        var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);
                        break;
                    case "LI":
                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var resetstyle = new ResetStyle("resetid", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);

                        var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                        texttreatment.start();

                        var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);

                        var resetstyleadv = new ResetStyle("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyleadv);
                        break;
                    case "UL":
                    case "OL":
                        var thisinstance = new BasicStyle("listid", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var ls = new ListStyle("id", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(ls);
                        break;
                    case "IMG":
                        var jClickedon = $ektron(clickedon);
                        if (clickedon.parentElement.tagName == "FIGURE" && jClickedon.parent().hasClass('ektron-responsive-imageset')) {
                            jClickedon.parent().click();
                        }
                        else {
                            //if (clickedon.parentElement.tagName == "FIGURE" || clickedon.parentElement.parentElement.tagName == "FIGURE") {
                            //    var msg = $ektron('<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>This image belongs to a responsive image set. To modify the image set click FIGURE in the tag-trail above.</div>')
                            //    var msg2 = $ektron('<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>This image belongs to a responsive image set. To modify the image set click FIGURE in the tag-trail above.</div>')
                            //    $ektron('.ektron-aloha-advinspector-modal .basic').append(msg);
                            //    $ektron('.ektron-aloha-advinspector-modal .advanced').append(msg2);
                            //}
                            //else {
                            //    //Display message to indicate this image belongs to a responsive set
                            //    var msg = $ektron('<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>This image belongs to a responsive image set. To modify the image set click FIGURE in the tag-trail above.</div>')
                            //    var msg2 = $ektron('<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>This image belongs to a responsive image set. To modify the image set click FIGURE in the tag-trail above.</div>')
                            //    $ektron('.ektron-aloha-advinspector-modal .basic').append(msg);
                            //    $ektron('.ektron-aloha-advinspector-modal .advanced').append(msg2);
                            //}
                            var imgProp = new ImageProperties("id", clickedon, i18n, Dialog);
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(imgProp.create());
                            imgProp = new ImageProperties("imgpropadvid", clickedon, i18n, Dialog);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(imgProp.create());
                            imgProp.init();

                            var basicsize = new BasicSize("id", clickedon);
                            var block = basicsize.create();
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(block);
                            basicsize.start();

                            var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                            var resetstyle = new ResetStyle("reset", clickedon, i18n).create();
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);

                            var advancedsize = new AdvancedSize("imageadvid", clickedon, i18n);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedsize.create());

                            var position = new Position("tableposid", clickedon, i18n);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(position.create());

                            var spacing = new SpacingStyle("spacingid", clickedon, i18n);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                            var border = new Border("borderid", clickedon, i18n, ColorHelper);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                            border.init();
                            if (clickedon.parentElement.tagName != "FIGURE" && clickedon.parentElement.parentElement.tagName != "FIGURE") {
                                var responsiveimage = new ResponsiveImage("responsiveid", clickedon, i18n, Dialog);
                                $ektron('.ektron-aloha-advinspector-modal .advanced').append(responsiveimage.create());
                                responsiveimage.init();
                            }
                            resetstyle = new ResetStyle("resetadvid", clickedon, i18n).create();
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyle);
                        }
                        break;
                    case "TABLE":
                        var tblProp = new TableProperties("id", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(tblProp.create());
                        tblProp = new TableProperties("tblpropadvid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(tblProp.create());

                        var basictablestyle = new TableStyle("tablestyid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(basictablestyle.create());

                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var resetstyle = new ResetStyle("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);

                        var advancedsize = new AdvancedSize("tableadvid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedsize.create());

                        var tablestyle = new TableStyle("tablestyid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(tablestyle.create());

                        var position = new Position("tableposid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(position.create());

                        var spacing = new SpacingStyle("spacingid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                        var border = new Border("borderid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                        border.init();

                        var background = new Background("backgroundid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(background.create());
                        background.start();

                        resetstyle = new ResetStyle("resetadvid", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyle);
                        break;
                    case "TD":
                    case "TH":
                        var basicsize = new BasicSize("id", clickedon);
                        var block = basicsize.create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(block);
                        basicsize.start();

                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var advancedsize = new AdvancedSize("imageadvid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedsize.create());

                        var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);

                        var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                        texttreatment.start();

                        var spacing = new SpacingStyle("spacingid1", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                        var border = new Border("borderid1", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                        border.init();

                        var background = new Background("backgroundid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(background.create());
                        background.start();

                        var resetstyle = new ResetStyle("resetadvid", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyle);
                        break;
                    case "TR":
                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var spacing = new SpacingStyle("spacingid1", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                        var border = new Border("borderid1", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                        border.init();

                        var background = new Background("backgroundid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(background.create());
                        background.start();

                        var resetstyle = new ResetStyle("resetadvid", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyle);
                        break;
                    case "FIGURE":
                        this.setFocusedImageSet(clickedon);

                        if ($ektron(clickedon).hasClass('ektron-responsive-imageset')) {
                            var responsiveimage = new ResponsiveImage("responsivefigid", clickedon, i18n, Dialog);
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(responsiveimage.create());
                            responsiveimage.init();
                        }

                        var alttagfig = new FigAltTag("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(alttagfig);

                        var thisinstance = new BasicStyle("ddid", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var advancedsize = new AdvancedSize("imageadvid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedsize.create());

                        var border = new Border("borderid1", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                        border.init();
                        var spacing = new SpacingStyle("spacingid1", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());
                        var position = new Position("tableposid", clickedon, i18n);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(position.create());
                        break;
                    case "DIV":
                    case "BLOCKQUOTE":
                        if (clickedon.parentElement != null && clickedon.parentElement.tagName == "TD") {
                            $ektron(clickedon).parent().click();
                        }
                        else if (clickedon.parentElement != null && clickedon.parentElement.tagName == "TH") {
                            $ektron(clickedon).parent().click();
                        } else {
                            var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                            var resetstyle = new ResetStyle("resetid", clickedon, i18n).create();
                            $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);

                            var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                            texttreatment.start();

                            var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);

                            var spacing = new SpacingStyle("spacingid1", clickedon, i18n);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(spacing.create());

                            var advancedsize = new AdvancedSize("imageadvid", clickedon, i18n);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedsize.create());

                            var position = new Position("tableposid", clickedon, i18n);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(position.create());

                            var border = new Border("borderid1", clickedon, i18n, ColorHelper);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(border.create());
                            border.init();

                            var background = new Background("backgroundid", clickedon, i18n, ColorHelper);
                            $ektron('.ektron-aloha-advinspector-modal .advanced').append(background.create());
                            background.start();
                        }
                        break;
                    case "A":
                        var basiclink = new BasicLink("basiclinkid", clickedon, i18n, Dialog);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(basiclink.create());
                        basiclink.init(activeId);

                        var thisinstance = new BasicStyle("id", clickedon, styleConfigJson);
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(thisinstance.create());

                        var resetstyle = new ResetStyle("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .basic').append(resetstyle);

                        var advancedlink = new AdvancedLink("advancedlinkid", clickedon, i18n, Dialog);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(advancedlink.create());
                        advancedlink.init(activeId);

                        var texttreatment = new TextTreatment("texttreatid", clickedon, i18n, ColorHelper);
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(texttreatment.create());
                        texttreatment.start();

                        var textalignment = new TextAlignment("textalign", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(textalignment);

                        var resetstyle = new ResetStyle("reset", clickedon, i18n).create();
                        $ektron('.ektron-aloha-advinspector-modal .advanced').append(resetstyle);

                        break;
                }
            },

            createTrail: function (object) {
                $ektron('.ektron-aloha-advinspector-modal .trail').empty();
                var that = this;
                var trail = $ektron("<ul id='trail'></ul>");
                trails.length = 0;
                var builder = function (obj) {
                    trails.push(obj);
                    if (obj[0].id != activeId) {
                        if (obj.parent() != 'undefined') {
                            builder(obj.parent());
                        }
                    }
                }
                builder($ektron(object));
                if (trails.length > 0) {
                    trails.reverse();
                    $ektron.each(trails, function (index, value) {
                        if (!value.hasClass('aloha-table-cell-editable') && !value.hasClass('ektron-imageset-wrapper')) {
                            if (value.hasClass('ektron-responsive-imageset')) {
                                $ektron('.ektron-aloha-advinspector-modal .trail').append('<li><button class="trailbtn" data-trail="' + index + '">Responsive Image</button></li>');
                            } else {
                                $ektron('.ektron-aloha-advinspector-modal .trail').append('<li><button class="trailbtn" data-trail="' + index + '">' + value[0].tagName + '</button></li>');
                            }
                            if (value.hasClass('ektron-responsive-imageset')) {
                                // do not show the IMG tab inside the FIGURE.ektron-responsive-imageset 
                                return false; // break from the each loop
                            }
                        }
                    });
                }
                $ektron('.ektron-aloha-advinspector-modal .trail li button').off('click');
                $ektron('.ektron-aloha-advinspector-modal .trail li button').on('click', function (event) {
                    $ektron('.ektron-aloha-advinspector-modal .trail li button').removeClass('active');
                    $ektron(this).addClass('active');
                    that.buildWindow(trails[parseInt($ektron(this).attr("data-trail"))][0], false);
                    //event.stopPropagation();
                    event.preventDefault();
                });
                $ektron('.ektron-aloha-advinspector-modal .trail li button:last').addClass('active');
            },

            createModal: function () {
                var that = this;
                $ektron('<div id="advInspectorm" class="ektron-aloha-advinspector-modal"><ul class="trail"><li><button>Tag Breadcrumbs</button></li></ul><div style="clear:both"/><div id="highlightoption"><input type="checkbox" id="advShowHighlights" /><label for="advShowHighlights">Highlight Tags On Click</label></div><div style="clear:both"/><div id="instabs"><ul><li><a href="#insBasic">Basic</a></li><li><a href="#insAdvanced">Advanced</a></li></ul><div id="insContainer"><div id="insBasic" class="basic"></div><div id="insAdvanced" class="advanced"></div></div></div></div>').appendTo("body");
                that.advDialog = $ektron(".ektron-aloha-advinspector-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: true,
                    dialogClass: 'ektron-ux ektron-ux-UITheme ektron-ux-dialog ux-app-siteApp-dialog',
                    closable: false,
                    width: 480,
                    height: 615,
                    modal: false,
                    zIndex: 10000001,
                    title: "Inspector",
                    closeOnEscape: false,
                    open: function (event, ui) { $(this).parent().children().children(".ui-dialog-titlebar-close").hide(); }
                });

                $ektron(".ektron-aloha-advinspector-modal").not('.minicolors-panel').on('mousedown', function (event) {
                    //event.stopPropagation();
                    //event.preventDefault();
                });
                $ektron("#instabs").tabs();
                var highlightoption = false;
                $ektron('#highlightoption').click(function () {
                    if (!$ektron('#advShowHighlights').prop('checked')) {
                        $ektron('*[data-ektron-highlight="true"]').removeAttr("data-ektron-highlight");
                    }
                });
                $ektron('div#advInspectorm').parent().find('.ui-dialog-titlebar-close').hide();
            },

            ////////////////////// ImageSet //////////////////////////
            ImageSetInit: function (elem) {
                var jElem, figureWrapper;
                if ('undefined' === typeof elem) {
                    elem = Aloha.jQuery(activeId);
                }
                if ('undefined' === typeof elem) { return; }
                Aloha.jQuery("figure.ektron-responsive-imageset").each(function () {
                    jElem = Aloha.jQuery(this);

                    if (0 === jElem.parent('.ektron-imageset-wrapper').length) {
                        figureWrapper = $ektron(
			                '<div class="ektron-imageset-wrapper" data-block-skip-scope="true"></div>'
		                );
                        jElem.wrap(figureWrapper);
                    }
                    var jParent = jElem.parent('div');
                    if (typeof jParent.alohaBlock !== 'undefined') {
                        jParent.alohaBlock();
                    }

                    jParent.get(0).onresizestart = function (e) { return false; };
                    jParent.get(0).oncontrolselect = function (e) { return false; };
                    jParent.get(0).ondragstart = function (e) { return false; };
                    jParent.get(0).onmovestart = function (e) { return false; };
                    jParent.get(0).onselectstart = function (e) { return false; };
                });
                //return jParent;
            },

            ImageSetDeactivate: function (elem) {

                if ('undefined' === typeof elem) {
                    elem = Aloha.jQuery(activeId);
                }

                Aloha.jQuery("figure.ektron-responsive-imageset").each(function (index, elem) {
                    //if (Aloha.jQuery(elem).parent()[0].tagName == "A") {
                    //    jParent = Aloha.jQuery(elem).closest('a');
                    //} else {
                    var jParent = Aloha.jQuery(elem).closest('div');
                    //}
                    var block = Aloha.Block.BlockManager.getBlock(jParent);
                    if (jParent.mahaloBlock) {
                        jParent.mahaloBlock();
                    }
                    if (block) {
                        block.deactivate();
                        block.unblock();
                        jParent.unbind();
                    }
                    if (jParent.hasClass('ektron-imageset-wrapper')) {
                        $ektron(elem).unwrap();
                    }
                });
                $ektron('.aloha-cleanme').remove();
            },

            setFocusedImageSet: function (focusImageSet) {
                var that = this, thisBlock, jParent, currBlock;

                $ektron('.ektron-responsive-imageset').each(function (index, elem) {
                    //if (Aloha.jQuery(elem).parent()[0].tagName == "A")
                    //{
                    //    jParent = Aloha.jQuery(elem).closest('a');
                    //} else {
                    var jParent = Aloha.jQuery(elem).closest('div');
                    //}
                    currBlock = Aloha.Block.BlockManager.getBlock(jParent);
                    if (typeof currBlock != 'undefined') {
                        currBlock.deactivate();
                    }
                });
            },

            makeClean: function (obj) {
                //var that = this;
                //obj.find('.ektron-responsive-imageset').each(function (index, elem) {
                //    that.ImageSetDeactivate(elem);
                //});
                //obj.find('.ektron-imageset-wrapper > img').each(function (index, elem) {
                //    $ektron(elem).unwrap();
                //});
                //obj.find('.aloha-cleanme').remove();
                //$ektron('*[data-ektron-highlight="true"]').removeAttr("data-ektron-highlight");
            },

            getStyleConfig: function () {
                var that = this;
                if ($ektron.isEmptyObject(styleConfigJson)) {
                    $.ajax({
                        url: Ektron.Context.Cms.UIPath + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/lib/StyleConfig.js',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            styleConfigJson = data;
                        }
                    });
                }
            },
            //////////////////////////// ImageSet ///////////////////////////

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);