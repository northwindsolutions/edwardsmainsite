﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-alttag-figure';
    function FigAltTag(id, obj, i18n) {
        this.id = id;
        this.targetElem = {
            elem : obj
        }; 

        this.create = function () {
            var ResetBlock, resetGroup, resetList, resetLabel, resetInput, resetClassInput, resetStyleInput, currStyle, currClass,
                jTarget = $ektron(this.targetElem.elem), 
                currAlt = jTarget.children('img').attr('alt'),  
                that = this;


            ResetBlock = document.createElement('fieldset');
            ResetBlock.setAttribute('id', this.id);
            ResetBlock.className = pluginNamespace + ' aloha';

            //class and style fields group
            resetGroup = document.createElement('ul');
            
            //reset style
            resetList = document.createElement('li');
            resetLabel = document.createElement('label');
            resetLabel.className = 'reset-label';
            resetLabel.innerHTML = "Responsive Image Alt Tag";
            resetList.appendChild(resetLabel);
            resetGroup.appendChild(resetList);
            resetList = document.createElement('li');
            resetInput = $ektron('<textarea id="txtfigAlt" class="input-box" />');
            resetInput.val(currAlt);
            resetInput.on('keyup', function () { that.updateAlt(); });
            resetList.appendChild(resetInput.get(0));
            resetGroup.appendChild(resetList);

            ResetBlock.appendChild(resetGroup);


            return ResetBlock;
        };

        this.updateAlt = function () {
            var jResult = $ektron(this.targetElem.elem);
            jResult.children('img').attr('alt', $("#txtfigAlt").val());
        };

        //Creates string with this component's namepspace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return FigAltTag;
});

    