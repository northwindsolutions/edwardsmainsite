﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="colorhelper.js" />
define([''], function () {
    "use strict";
    var namespace = "ektron-aloha-background-", backgroundidtext, backgroundidcheck;

    function Background(id, obj, i18n, ColorHelper) {
        this.id = id;
        this.targetElem = obj;
        backgroundidtext = "ektron-aloha-background-text" + this.id;
        backgroundidcheck = "ektron-aloha-background-check" + this.id;

        this.create = function () {
            //create jquery object for Backround module
            var background = $ektron('<fieldset></fieldset>'), colorSelector, colorSelected, that = this;
            background.attr('id', this.id);
            $ektron('<input id="' + backgroundidcheck + '" type="checkbox" /><label class="strong">' + i18n.t('background.checkbox.label') + '</label>').appendTo(background);
            $ektron('<ul><li><span><input id="' + backgroundidtext + '" type="text" /></span><span id="Alpha"></span></li></ul>').appendTo(background);
            return background;
        };

        this.start = function () {
            var selectedColor = $ektron(this.targetElem).css('backgroundColor'), colorPicker, that = this;
            //Initialize colorpicker
            colorPicker = new ColorHelper($('#' + backgroundidtext));
            if ('transparent' === selectedColor) {
                colorPicker.start('rgb(255,255,255)');
            }
            else {
                colorPicker.start(selectedColor);
            }

            //attach events
            $ektron('#' + backgroundidtext).on('input', function () { that.updateBackground(); });
            $ektron('#' + backgroundidcheck).on('click', function () {
                that.showHideModule(this);
                that.modifyBackground($(this).is(':checked'));
            });
            //init checkbox
            $ektron('#' + backgroundidtext).closest('ul').css('display', 'none');
            if (colorPicker.hexc(selectedColor).length > 0) {
                $ektron('#' + backgroundidcheck).prop('checked', true);
                $ektron('#' + backgroundidtext).closest('ul').css('display', 'block');
            }

            //$(document).on("ekcolorChanged", colorChangedHandler);
            //// newMessage event handler
            //function colorChangedHandler(e) {

            //    if (e.obj.prop('id') === backgroundidtext) {
            //        if (true === $ektron('#' + backgroundidcheck).is(':checked')) {
            //            $(that.targetElem).css('background-color', e.message);
            //        }
            //    }
            //}

        };

        this.updateBackground = function () {
            //set background color on textbox blur event
            var jResult = $ektron(this.targetElem), colorSelected;
            if (true === $ektron('#' + backgroundidcheck).is(':checked')) {
                colorSelected = $ektron('#' + backgroundidtext).val();
                //if there is a color prefix with hash cor css
                if (colorSelected.length > 0) {
                    colorSelected = '#' + colorSelected;
                }
                jResult.css('background-color', colorSelected);
            }

        };

        this.modifyBackground = function (arg) {
            //set/clear background color on checkbox check event and color selected 
            var jResult = $ektron(this.targetElem), colorSelected;
            colorSelected = '#' + $ektron('#' + backgroundidtext).val();
            if (arg && colorSelected.length > 1) {
                jResult.css('background-color', colorSelected);
            }
            else {

                var jResult = $ektron(this.targetElem), colorSelected;
                jResult.css('background-color', "");
            }
        };

        
        this.showHideModule = function (elem) {
             var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                $ektron('#' + backgroundidtext).closest('ul').css('display', 'block');
            }
            else {
                $ektron('#' + backgroundidtext).closest('ul').css('display', 'none');
            }
        };
    }
    return Background;
});
