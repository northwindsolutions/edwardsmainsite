﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../../../../../../../../Ektron.Namespace.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var namespace = "ektron-aloha-basic-link", basiclinkidlink, basiclinkidemail, basiclinkidsubject, basiclinkidurl, basiclinkidbookmark,
        basicbookmarkid, basicotherbookmarkid, basiclinkradio, basiclinkidtitle, basiclinktidcheck, basiclinkidemailtext, basiclinkidurltext, basiclinklinkselect,
        basiclinklinkremove, basiclinkemailremove, basiceditlinks, basiceditbookmarks, basicbookmarktext, basiclinkbookmarkremove, modal,
		WorkareaPath = Ektron.Context.Cms.WorkareaPath;

    function BasicLink(id, obj, i18n, Dialog) {
        this.id = id;
        this.targetElem = obj;
        this.currenteditable = "";
        basiclinkidlink = "ektron-aloha-basiclink-link" + this.id;
        basiclinkidemail = "ektron-aloha-basiclink-email" + this.id;
        basiclinkidemailtext = "ektron-aloha-basiclink-email-text" + this.id;
        basiclinkidsubject = "ektron-aloha-basiclink-email-subject" + this.id;
        basiclinkidurl = "ektron-aloha-basiclink-url" + this.id;
        basiclinkidurltext = "ektron-aloha-basiclink-url-text" + this.id;
        basiclinkidbookmark = "ektron-aloha-basiclink-bookmark" + this.id;
        basicbookmarkid = 'ektron-aloha-basiclink-bookmark-dropdown' + this.id;
        basicotherbookmarkid = "ektron-aloha-basiclink-bookmark-other" + this.id;
        basiclinkidtitle = "ektron-aloha-basiclink-title" + this.id;
        basiclinktidcheck = "ektron-aloha-basiclink-check" + this.id;
        basiclinklinkselect = "ektron-aloha-basiclink-select" + this.id;
        basiclinklinkremove = "ektron-aloha-basiclink-remove" + this.id;
        basiclinkemailremove = "ektron-aloha-basicemail-remove" + this.id;
        basiceditlinks = "ektron-aloha-basicedit-links" + this.id;
        basiceditbookmarks = "ektron-aloha-basicedit-bookmarks" + this.id;
        basicbookmarktext = "ektron-aloha-basictext-bookmark" + this.id;
        basiclinkbookmarkremove = "ektron-aloha-basicbookmark-remove" + this.id;


        this.create = function () {
            //create jquery object for Basic-Link module
            var basiclink = $ektron('<fieldset></fieldset>'), basiclinkedit, basiclinkelem, basiclinkclick, basiclinkcheck, that = this;
            basiclink.attr('id', this.id);
            //div to show edit urllink/bookmarklink, emaillink
            basiclinkedit = $ektron('<div class="' + basiceditlinks + '">');
            //  Link/Email select buttons
            basiclinkelem = $ektron('<div class="' + that.nsClass('linktype') + '">');
            $ektron('<div><label class="' + that.nsClass('type-label') + '">' + i18n.t('basiclink.label.type') + '</label></div>').appendTo(basiclinkelem);
            basiclinkclick = $ektron('<input id="' + basiclinkidlink + '" type="button" />').val(i18n.t('basiclink.button.link.label')).appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.linkSelected(); });
            basiclinkclick = $ektron('<input id="' + basiclinkidemail + '" type="button" />').val(i18n.t('basiclink.button.email.label')).appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.emailSelected(); });
            basiclinkelem.appendTo(basiclinkedit);
            //  URL/Existing Bookmark Select
            basiclinkelem = $ektron('<div class="' + that.nsClass('linkselect') + '">');
            $ektron('<div><label class="' + that.nsClass('type-label') + '">' + i18n.t('basiclink.label.url') + ':</label></div>').appendTo(basiclinkelem);
            basiclinkclick = $ektron('<input id="' + basiclinkidurl + '" type="radio" name="linksource" checked = "checked" />').appendTo(basiclinkelem);
            $ektron('<label for="' + basiclinkidurl + '">' + i18n.t('basiclink.label.url') + '</>').appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.urlSelected(); });
            basiclinkclick = $ektron('<input id="' + basiclinkidbookmark + '" type="radio" name="linksource" />').appendTo(basiclinkelem);
            $ektron('<label for="' + basiclinkidbookmark + '">' + i18n.t('basiclink.label.existingbookmark') + '</>').appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.bookmarkSelected(); });
            basiclinkelem.appendTo(basiclinkedit);

            // input for Link/URL
            basiclinkelem = $ektron('<div class="' + that.nsClass('linkselecturl') + '">');
            basiclinkclick = $ektron('<span><input id="' + basiclinkidurltext + '" type="text"  /></span>').appendTo(basiclinkelem);
            basiclinkclick.on('input', function () { that.urlChanged(); });
            basiclinkclick = $ektron('<span><input id="' + basiclinklinkselect + '" type="button" class="' + that.nsClass('selectlibraryurl') + '" /></span>').appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.linkSelectButtonClick(); });
            basiclinkclick = $ektron('<span><input id="' + basiclinklinkremove + '" type="button" class="' + that.nsClass('removelibraryurl') + '" /></span>').appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.linkRemoveButtonClick(); });
            basiclinkelem.appendTo(basiclinkedit);

            // input for Link/Bookmark
            basiclinkelem = $ektron('<div style="display: none;" class="' + that.nsClass('bookmarkselecturl') + '">');
            basiclinkclick = $ektron('<select id="' + basicbookmarkid + '" class="select-box"><option>Select</option></select>').appendTo(basiclinkelem);
            basiclinkclick.on('change', function () { that.bookmarkLinkChanged(); });

            $ektron('<span>&nbsp;or # </span>').appendTo(basiclinkelem);
            basiclinkclick = $ektron('<span><input id="' + basicotherbookmarkid + '" type="text" class="input-bookmark"  /></span>').appendTo(basiclinkelem);
            basiclinkclick.on('input', function () { that.bookmarkLinkChanged(); });


            basiclinkelem.appendTo(basiclinkedit);

            // email only, input email address regex to confirm '@' plus at least one '.'
            basiclinkelem = $ektron('<ul style="display: none;" class="' + that.nsClass('emailaddress') + '">');
            $ektron('<li><label for="' + basiclinkidemailtext + '">' + i18n.t('basiclink.text.label.email') + '</></li>').appendTo(basiclinkelem);
            basiclinkclick = $ektron('<input id="' + basiclinkidemailtext + '" type="text" />').appendTo(basiclinkelem);
            basiclinkclick.on('blur', function () { that.buildMailToLink(); });
            basiclinkclick = $ektron('<span><input id="' + basiclinkemailremove + '" type="button" class="' + that.nsClass('removelibraryurl') + '" /></span>').appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.linkRemoveButtonClick(); });
            $ektron('<li><label for="' + basiclinkidsubject + '">' + i18n.t('basiclink.text.label.subject') + '</></li>').appendTo(basiclinkelem);
            basiclinkclick = $ektron('<li><input id="' + basiclinkidsubject + '" type="text" /></li>').appendTo(basiclinkelem);
            basiclinkclick.on('input', function () { that.buildMailToLink(); });
            basiclinkelem.appendTo(basiclinkedit);

            // Common Text with varying Label
            //  URL/Existing Bookmark Select
            basiclinkelem = $ektron('<ul class="' + that.nsClass('titlesubject') + '">');
            $ektron('<li><label for="' + basiclinkidtitle + '">' + i18n.t('basiclink.text.label.title') + '</></li>').appendTo(basiclinkelem);
            basiclinkclick = $ektron('<li><input id="' + basiclinkidtitle + '" type="text" /></li>').appendTo(basiclinkelem);
            basiclinkclick.on('input', function () { that.titleChanged(); });
            basiclinkelem.appendTo(basiclinkedit);

            // Checkbox for links only (not email)
            basiclinkelem = $ektron('<div class="' + that.nsClass('targetselect') + '">');
            basiclinkcheck = $ektron('<input type="checkbox" id="' + basiclinktidcheck + '" />').appendTo(basiclinkelem);
            basiclinkcheck.on('click', function () { that.linkTargetSelectClick(); });
            $ektron('<label for="' + basiclinktidcheck + '">' + i18n.t('basiclink.check.label.target') + '</>').appendTo(basiclinkelem);
            basiclinkelem.appendTo(basiclinkedit);
            basiclinkedit.appendTo(basiclink);

            //Bookmarks only (anchor tag with ID)
            basiclinkedit = $ektron('<div class="' + basiceditbookmarks + '">');
            basiclinkelem = $ektron('<div class="' + that.nsClass('linktype') + '">');
            $ektron('<div><label class="' + that.nsClass('type-label') + '">' + i18n.t('basiclink.label.type') + '</label></div>').appendTo(basiclinkelem);
            $ektron('<input id="' + basiclinkidlink + '" type="button" />').val(i18n.t('basiclink.button.bookmark.label')).appendTo(basiclinkelem);
            basiclinkelem.appendTo(basiclinkedit);

            // Populate selected bookmark
            basiclinkelem = $ektron('<div class="' + that.nsClass('linkbookmark') + '">');
            basiclinkclick = $ektron('<input id="' + basicbookmarktext + '" type="text" />').appendTo(basiclinkelem);
            basiclinkclick.on('input', function () { that.bookmarkIdChanged(); });
            basiclinkclick.on('blur', function () { that.fillBookmarkList(); });

            basiclinkclick = $ektron('<span><input id="' + basiclinkbookmarkremove + '" type="button" class="' + that.nsClass('removebookmark') + ' input-box" /></span>').appendTo(basiclinkelem);
            basiclinkclick.on('click', function () { that.linkRemoveButtonClick(); });
            $ektron('<div>' + i18n.t("basiclink.label.existingbookmarks") + '</div>').appendTo(basiclinkelem);
            $ektron('<p><textarea class="' + this.nsClass('existingbookmark-field') + '" readonly="readonly"></textarea></p>').appendTo(basiclinkelem);
            basiclinkelem.appendTo(basiclinkedit);

            basiclinkedit.appendTo(basiclink);
            return basiclink;
        };
        // Show link type selector options
        this.linkSelected = function () {
            var radioselect = '';
            $ektron('.' + this.nsClass('linkselect')).show();
            $ektron('.' + this.nsClass('linkselecturl')).show();
            $ektron('.' + this.nsClass('emailaddress')).hide();
            //Reset Link to URL/Bookmark settings
            radioselect = $('input[name="linksource"]:checked').attr('id');
            if (radioselect === basiclinkidurl) {
                this.urlSelected();
            }
            if (radioselect === basiclinkidbookmark) {
                this.bookmarkSelected();
            }

        };
        // Show email type selector options
        this.emailSelected = function () {
            $ektron('.' + this.nsClass('linkselecturl')).hide();
            $ektron('.' + this.nsClass('linkselect')).hide();
            $ektron('.' + this.nsClass('targetselect')).hide();
            $ektron('.' + this.nsClass('emailaddress')).show();
            $ektron('.' + this.nsClass('bookmarkselecturl')).hide();
            $ektron('.' + this.nsClass('titlesubject')).hide();

        };
        // Show URL type selector options
        this.urlSelected = function () {
            $ektron('.' + this.nsClass('targetselect')).show();
            $ektron('.' + this.nsClass('bookmarkselecturl')).hide();
            $ektron('.' + this.nsClass('linkselecturl')).show();
            $ektron('.' + this.nsClass('titlesubject')).show();
        };
        // Show Bookmark type selector options
        this.bookmarkSelected = function () {
            $ektron('.' + this.nsClass('linkselecturl')).hide();
            $ektron('.' + this.nsClass('targetselect')).show();
            $ektron('.' + this.nsClass('bookmarkselecturl')).show();
            $ektron('.' + this.nsClass('titlesubject')).show();
            var bookmarkList = this.getBookmarks();
            this.updateBookmarkLists(bookmarkList);
        };
        //Prepend Namespace method
        this.nsClass = function () {
            var stringBuilder = [], prefix = namespace;
            jQuery.each(arguments, function () {
                stringBuilder.push(this == '' ? prefix : prefix + '-' + this);
            });
            return jQuery.trim(stringBuilder.join(' '));
        };
        //Initialize modal
        this.init = function (obj) {
            this.currenteditable = obj;
            this.populateLinkFields();
            //populate URL fields
            if (Ektron.Namespace.Register) {
                Ektron.Namespace.Register('Ektron.AdvancedInspector.Link.Parent');
                Ektron.Namespace.Register('Ektron.AdvancedInspector.Link.Basic');
                Ektron.AdvancedInspector.Link.Basic.AcceptLink = this.acceptLink;
                Ektron.AdvancedInspector.Link.Parent;

                Ektron.Namespace.Register('Ektron.AdvancedInspector');
                Ektron.AdvancedInspector.Dialog = Dialog();
            }

            //Need to stop event propagation so edittable class is not removed
            $ektron(".ektron-aloha-advinspector-modal").find('#' + this.id).on('mousedown', function (event) {
                event.stopPropagation();
            });
        };
        //Open library modal to quicklinks
        this.linkSelectButtonClick = function (elem) {         
            var that = this,
            dialogOptions = $.extend({}, Ektron.AdvancedInspector.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                dialogClass: 'ektron-ux ektron-ux-dialog',
                autoOpen: false,
                width: 820,
                modal: true,
                zIndex: 100000001,
                closeText: '<span class="ui-icon ui-icon-closethick" title="' + i18n.t('advanced-inspector.label.close') + '" />',
                title: i18n.t('advanced-inspector.label.library')
            });
            Ektron.AdvancedInspector.Link.Parent = 'basic';
            Ektron.AdvancedInspector.Dialog.html('<div class="ektron-aloha-inspector-modal"><iframe id="ektron-aloha-inspector-modal-iframe" class="ektron-aloha-inspector-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
            Ektron.AdvancedInspector.Dialog.dialog('option', dialogOptions);

            Ektron.AdvancedInspector.Dialog.dialog('open');

            var titlebar = Ektron.AdvancedInspector.Dialog.parents('.ui-dialog').find('.ui-dialog-titlebar');
            titlebar.find('.ui-dialog-titlebar button').remove();
            $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-closethick"></span></button>')
                .appendTo(titlebar)
                .click(function () {
                    Ektron.AdvancedInspector.Dialog.dialog('close');
                });

            $ektron("iframe.ektron-aloha-inspector-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/mediamanager.aspx?actiontype=library&scope=Quicklinks&type=quicklinks&EditorName=Aloha&autonav=0");
        };

        //Remove an a tag and clear the current item from the href Field
        this.linkRemoveButtonClick = function (elem) {
            var range = Aloha.Selection.getRangeObject(),
                    foundMarkup = this.targetElem;//this.findLinkMarkup();

            // clear the current item from the href field
            this.targetElem.href = null;
            if (foundMarkup) {
                // remove the link
                GENTICS.Utils.Dom.removeFromDOM(foundMarkup, range, true);

                range.startContainer = range.endContainer;
                range.startOffset = range.endOffset;

                // select the (possibly modified) range
                range.select();
                //* @deprecated see aloha.js line: 28700
                //if (typeof terminateLinkScope == 'undefined' ||
                //		terminateLinkScope === true) {
                //    Scopes.setScope('Aloha.continuoustext');
                //}
            }
        };

        //Insert quicklink from library
        this.acceptLink = function (htmlObj) {
            var that = this,
                foundMarkup,
                h = {
                    href: "",
                    title: "",
                    type: ""
                };

            jQuery.extend(h, htmlObj);

            //update text and property - as this does not trigger input event
            $ektron('#' + basiclinkidurltext).val(h.href);
            $ektron(obj).attr('href', h.href);
            $ektron(obj).attr('data-ektron-url', h.href);

            Ektron.AdvancedInspector.Dialog.dialog("close");
        };


        // retrieve the existing bookmarks of the current content 
        this.getBookmarks = function (delimeter) {
            var editor = jQuery('#' + this.currenteditable).get(0),
                sBookmarkList = "";
            if (jQuery('.aloha-editable-active').length > 0) {
                editor = jQuery('.aloha-editable-active').get(0);
            }
            if (editor != "") {

                var bookmarks = [],
                    docAnchors = editor.getElementsByTagName('A');

                if (docAnchors && docAnchors.length > 0) {
                    for (var i = 0; i < docAnchors.length; i++) {
                        if (docAnchors[i].id.length > 0) {
                            bookmarks.push(docAnchors[i].id);
                        }
                    }
                    sBookmarkList = bookmarks.sort();
                }
            }
            return (sBookmarkList);
        };


        //Populate Existing Bookmarks dropdown (First entry defined as Bookmark -> use bookmark in adjoining text box)
        this.updateBookmarkLists = function (bookmarkList) {
            var itemlist = $ektron('#' + basicbookmarkid);
            $(itemlist).children('option:not(:first)').remove();
            jQuery.each(bookmarkList, function () {
                $('<option>' + this + '</option>').appendTo(itemlist);
            });
        };

        ///methods for modifying Fields
        //Update url changed - take link from textbox
        this.urlChanged = function () {
            this.targetElem.href = $ektron('#' + basiclinkidurltext).val();
            $ektron(obj).attr('data-ektron-url', $ektron('#' + basiclinkidurltext).val());
        };

        //Update bookmark link changed. if dropdown value is Select, get value from other textbox
        this.bookmarkLinkChanged = function () {
            if ($ektron('#' + basicbookmarkid)[0].selectedIndex === 0) {
                this.targetElem.href = '#' + $ektron('#' + basicotherbookmarkid).val();
                $ektron(obj).attr('data-ektron-url', '#' + $ektron('#' + basicotherbookmarkid).val());
                $('#' + basicotherbookmarkid).prop('disabled', false);
            }
            else {
                this.targetElem.href = '#' + $ektron('#' + basicbookmarkid).val();
                $ektron(obj).attr('data-ektron-url', '#' + $ektron('#' + basicbookmarkid).val());
                $ektron('#' + basicotherbookmarkid).val('');
                $('#' + basicotherbookmarkid).prop('disabled', true);
            }

        };
        //Update bookmark id changed. if dropdown value is Select, get value from other textbox
        this.bookmarkIdChanged = function () {
            this.targetElem.id = $ektron('#' + basicbookmarktext).val();
            //No url for Bookmark
            $ektron(obj).removeAttr('data-ektron-url');
        };

        //Update title/subject changed - take text from textbox
        this.titleChanged = function () {
            this.targetElem.title = $ektron('#' + basiclinkidtitle).val();
        };

        //set target for link on checkbox value
        this.linkTargetSelectClick = function () {
            if ($ektron('#' + basiclinktidcheck).is(':checked')) {
                this.targetElem.target = '_blank';
            }
            else {
                this.targetElem.target = '_self';
            }

        };

        //set checkbox for link on target value
        this.linkTargetSet = function () {
            if (this.targetElem.target === '_blank') {
                $ektron('#' + basiclinktidcheck).prop('checked', true);
            }
            else {
                $ektron('#' + basiclinktidcheck).prop('checked', false);
            }
        };

        //parse email address from href
        // remove mailto, parse address and subject on '?' querystring Subject
        this.populateEmail = function () {
            var address, subject, qindex;
            address = this.targetElem.href.replace('mailto:', '');
            qindex = address.indexOf("?");
            // no subject text
            if (qindex < 0) {
                $ektron('#' + basiclinkidemailtext).val(address);
            }
            else {
                $ektron('#' + basiclinkidemailtext).val(address.substr(0, qindex));
                $ektron('#' + basiclinkidsubject).val(decodeURIComponent(address.substring(qindex + 9)));
            }
            $ektron(obj).attr('data-ektron-url', this.targetElem.href);
        };

        ////parse email subject from href
        //regex email for '@' followed by '.' characters
        //encodeURIComponent on Subject field
        this.buildMailToLink = function () {
            var prefix = 'mailto:', address, subject;
            //email regex:  \b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b
            address = $ektron('#' + basiclinkidemailtext).val();
            var myRegexp = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i;
            var match = myRegexp.exec(address);
            if (null === match) {
                alert("Invalid email address");
                event.stopPropagation();
                event.preventDefault();
            }
            else {

                subject = $ektron('#' + basiclinkidsubject).val();
            }
            if (subject.length > 0) {
                this.targetElem.href = prefix + address + '?Subject=' + encodeURIComponent(subject);
            }
            else {
                this.targetElem.href = prefix + address;
            }
            $ektron(obj).attr('data-ektron-url', this.targetElem.href);
        };
        //Parse Href for Uemail, URL, ookmark
        this.populateLinkFields = function () {
            var itemlist, exists = false;
            if (this.targetElem.id === "") {
                // Disable Delete/Edit Bookmark
                $ektron("." + basiceditbookmarks).hide();
                //$ektron("." + basiceditlinks).show();
                var hashindex = 0, bookmark = "";
                hashindex = this.targetElem.href.indexOf('#');
                if (hashindex < 1) {
                    if (this.targetElem.href.substr(0, 6).toLowerCase() === 'mailto') {
                        this.populateEmail();
                        this.emailSelected();
                    }
                    else {
                        // URL link
                        $ektron(obj).attr('data-ektron-url', this.targetElem.href);
                        $ektron('#' + basiclinkidurltext).val($ektron(obj).attr('data-ektron-url'));
                        $ektron('#' + basiclinkidtitle).val(this.targetElem.title);
                        this.linkTargetSet();
                    }
                }
                else {
                    // has '#'-> bookmark link
                    $ektron("#" + basiclinkidbookmark).prop('checked', true);
                    this.bookmarkSelected();
                    $ektron('#' + basiclinkidtitle).val(this.targetElem.title);
                    bookmark = this.targetElem.href.substring(hashindex + 1);
                    //TODO: check if bookmark is in dropdown, link may be to other content
                    itemlist = $ektron('#' + basicbookmarkid + ' option').slice(1);

                    $(itemlist).each(function () {
                        if (this.value === bookmark) {
                            exists = true;
                            return false;
                        }
                    });
                    if (exists) {
                        $ektron('#' + basicbookmarkid).val(bookmark);
                        $('#' + basicotherbookmarkid).prop('disabled', true);
                    }
                    else {
                        $ektron('#' + basicotherbookmarkid).val(bookmark);
                    }
                    this.linkTargetSet();
                }
            }
            else {
                // Allow Delete/Edit Bookmark
                $ektron("." + basiceditbookmarks).show();
                $ektron("." + basiceditlinks).hide();
                this.fillBookmarkList();
                $ektron("#" + basicbookmarktext).val(this.targetElem.id);
            }
        };

        this.fillBookmarkList = function () {
            var bookmarks = this.getBookmarks();
            var bookmarkList = $ektron('.' + this.nsClass('existingbookmark-field'));
            var bookmarkText = "";
            jQuery.each(bookmarks, function () {
                bookmarkText = bookmarkText + this + ' \n';
            });
            bookmarkList.html(bookmarkText);
        };
    }
    return BasicLink;
});
