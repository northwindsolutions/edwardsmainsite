﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
define([''], function () {
    "use strict";
    function BasicSizeBlock(id, obj) {
        this.id = id;
        this.targetElem = obj;
        this.StyleConfigs = {};
        this.create = function () {
            basicSizeGroup = document.createElement('ul');
            var basicSizeBlock, slider, input, label, BasicSizeCheckbox, basicSizeGroup, that = this, targetElem = that.targetElem;
            //fieldset checkbox and label
            basicSizeBlock = document.createElement('fieldset');
            basicSizeBlock.setAttribute('id', this.id);
            basicSizeBlock.className = this.id + '-basic-style basic-size';

            BasicSizeCheckbox = $ektron('<input type="checkbox" id="chk-basicSize" />');
            BasicSizeCheckbox.on('click', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasSize()) {
                BasicSizeCheckbox.attr('checked', true);
            }
            basicSizeBlock.appendChild(BasicSizeCheckbox.get(0));

            label = document.createElement('label');
            label.setAttribute('id', this.id + '-label');
            label.htmlFor = this.id + '-slider';
            label.innerHTML = "Size";
            label.className = this.id + '-label';

            basicSizeBlock.appendChild(label);

            // fields group
            basicSizeGroup = document.createElement('ul');

            slider = document.createElement('div');
            slider.setAttribute('id', this.id + '-slider');
            slider.className = this.id + '-slider slider-left';

            basicSizeGroup.appendChild(slider);

            input = document.createElement('input');
            input.setAttribute('id', this.id + '-input');
            input.setAttribute('type', 'text');
            input.setAttribute('value', '100%');
            input.className = this.id + '-input textbox-narrow textbox-right';

            basicSizeGroup.appendChild(input);
            if (!this.hasSize()) {
                basicSizeGroup.setAttribute('style', 'display:none');
            }
            basicSizeBlock.appendChild(basicSizeGroup);
            
            return basicSizeBlock;
        };

        this.start = function () {
            var self = this;
            $ektron("." + self.id + "-slider").slider({
                value: 100,
                step: 1,
                min: 1,
                max: 500,
                slide: function (event, ui) {
                    $ektron("." + self.id + "-input").val(ui.value + "%");
                    self.updateStyle();
                }
            });
            $ektron("." + self.id + "-input").change(function () {
                var value = this.value.trim('%');
                $ektron("." + self.id + "-slider").slider("value", parseInt(value, 10));
                self.updateStyle();
            });

            var triggerEvent = true;
            $ektron(document).on('updateDimensions', function (e) {
                setTimeout(function () {
                    if (triggerEvent) {
                        triggerEvent = false;
                        setTimeout('triggerEvent = true', 500); 
                        self.targetElem.click();
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }, 500); 
                e.stopPropagation();
                e.preventDefault();
            });

            //Preload
            var width, percent, parentWidth; 
            if (typeof this.targetElem.style !== 'undefined' && this.targetElem.style.width.length > 0 && this.originalWidth > 0) {
                width = parseInt(this.targetElem.style.width, 10);
                if ('%' == this.targetElem.style.width.substr(this.targetElem.style.width.length -1, 1)){
                    parentWidth = $(self.targetElem).offsetParent().width();
                    percent = Math.round(parentWidth * width / this.originalWidth);
                }
                else {
                    percent = Math.round(100 * width / this.originalWidth);
                }
                $ektron("." + self.id + "-slider").slider("value", percent);
                $ektron("." + self.id + "-input").val(percent + "%");
            }
        };

        this.hasSize = function () {
            if (typeof this.targetElem.style !== 'undefined' && (this.targetElem.style.width.length > 0 || this.targetElem.style.height.length > 0)) {
                return true;
            }
            return false;
        };

        this.showHideModule = function (elem) {
            var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                jElem.siblings('ul').show();
            }
            else {
                jElem.siblings('ul').hide();
            }
        };

        this.updateStyle = function () {
            var styleString, jResult = $ektron(this.targetElem), unit, that = this, percent;
            jResult.css('width', "").css('height', "");
            if (true === document.getElementById('chk-basicSize').checked && this.originalWidth > 0) { 
                var jThis = $ektron('.' + this.id + '-input');
                percent = parseInt(jThis.val(), 10);
                if (percent > 0) {
                    styleString = Math.round(percent /100 * this.originalWidth) + 'px';
                    jResult.css('width', styleString);
                    $ektron(document).trigger('updateDimensions');
                }
            }    
        };

        this.getOriginalWidth = function (elem) {
            var trueImg = new Image();
            trueImg.src = elem.getAttribute('src');
            return trueImg.width;
        };
        this.originalWidth = this.getOriginalWidth(obj);
    }
    return BasicSizeBlock;
});