﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
define([''], function () {
    "use strict";
    function BasicStyleBlock(id, obj, StyleConfig) {
        this.id = id;
        this.targetElem = obj;
        this.StyleConfigs = StyleConfig;
        this.create = function () {
            var basicStyleBlock, that = this, targetElem = that.targetElem;
            basicStyleBlock = document.createElement('fieldset');
            basicStyleBlock.setAttribute('id', this.id);
            basicStyleBlock.className = 'basic-style';

            //Fill styleconfig

            //Output UI
            if (that.StyleConfigs[targetElem.tagName] != null) {
                //Reading style data from config, found matched element defined
                var optionStr = '';
                $ektron.each(eval(that.StyleConfigs[targetElem.tagName].styles), function (key, value) {
                    var selected = $ektron(targetElem);
                    if ((selected.hasClass(value.value)) || (selected.attr("style") == value.value)) {
                        optionStr += '<div class="styleOption styleSelected" data-StyleRef="' + key + '" data-StyleType="' + that.StyleConfigs[targetElem.tagName].styles[key].type + '" style="padding:5px;border:1px solid #F2EF38;cursor: pointer;"><span style="' + that.StyleConfigs[targetElem.tagName].styles[key].workareaBox + ';font-size:1.5em;">AaBbCc</span><span> - ' + key + ' (' + that.StyleConfigs[targetElem.tagName].styles[key].type + ')</span></div>';
                    } else {
                        optionStr += '<div class="styleOption" data-StyleRef="' + key + '" data-StyleType="' + that.StyleConfigs[targetElem.tagName].styles[key].type + '" style="padding:5px;border:1px solid #F2EF38;cursor: pointer;"><span style="' + that.StyleConfigs[targetElem.tagName].styles[key].workareaBox + ';font-size:1.5em;">AaBbCc</span><span> - ' + key + ' (' + that.StyleConfigs[targetElem.tagName].styles[key].type + ')</span></div>';
                    }
                });
                var item = $ektron('<div class=""><label style="font-weight:bold;" for="basic-style-box">Style Selector</label><div id="basic-style-box" class="" style="width:100%;overflow:auto;">' + optionStr + '</div></div>');
                basicStyleBlock.appendChild(item[0]);
            } else {
                $ektron(basicStyleBlock).append('<div class="">No basic styles are defined for this element.</div>');
                return basicStyleBlock;
            }

            //Apply Bindings
            $ektron('body').off('click', '.styleOption');
            $ektron('body').on('click', '.styleOption', function (event) {
                var clickedelem = $ektron(this);

                var key = clickedelem.attr("data-StyleRef");
                var type = clickedelem.attr("data-StyleType");

                if (type == "style") {
                    jQuery("div[data-StyleType='style']").not(this).removeClass("styleSelected");
                }

                clickedelem.toggleClass("styleSelected");
                var action, strStyleVals = that.StyleConfigs[targetElem.tagName].styles[key].value, arrStyle, arrProperty, i;
                if (clickedelem.hasClass("styleSelected"))
                    action = "add";
                else
                    action = "remove";

                if (that.StyleConfigs[targetElem.tagName].styles[key].type == "style") {
                    arrStyle = strStyleVals.split(';');
                    for (i = 0; i < arrStyle.length; i += 1) {
                        arrProperty = $.trim(arrStyle[i]).split(':');
                        if ('string' == typeof arrProperty[0]) {
                            if (action == "add") {
                                $ektron(targetElem).css(arrProperty[0], arrProperty[1]);
                            }
                            else if (action == "remove") {
                                $ektron(targetElem).css(arrProperty[0], '');
                            }
                        }
                    }
                } else if (that.StyleConfigs[targetElem.tagName].styles[key].type == "class") {
                    if (action == "add")
                        $ektron(targetElem).addClass(strStyleVals);
                    else if (action == "remove")
                        $ektron(targetElem).removeClass(strStyleVals);
                }
                $ektron(targetElem).click();
                event.stopPropagation();
                event.preventDefault();
            });

            return basicStyleBlock;
        };

        //this.getStyleConfig = function () {
        //    var that = this;
        //    if ($ektron.isEmptyObject(that.StyleConfigs)) {
        //        $.ajax({
        //            url: Ektron.Context.Cms.UIPath + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/inspector/lib/StyleConfig.js',
        //            dataType: 'json',
        //            async: false,
        //            success: function (data) {
        //                that.StyleConfigs = data;
        //            }
        //        });
        //    }
        //};

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem);

        };
    }
    return BasicStyleBlock;
});