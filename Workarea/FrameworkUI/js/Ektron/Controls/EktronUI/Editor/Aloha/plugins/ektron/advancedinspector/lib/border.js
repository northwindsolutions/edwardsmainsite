﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([], function () {
    "use strict";
    var pluginNamespace = 'aloha-border';
    function BorderBlock(id, obj, i18n, ColorHelper) {
        this.id = id;
        this.targetElem = {
            elem : obj,
            type : '',
            thickness : 0,
            style : '',
            color : 'rgb(0,0,0)'
        }; 
		this.allStyle = [
			'solid', 'dotted', 'dashed', 'double', 'groove', 'ridge', 'inset', 'outset'
		];

		this.create = function () {
            var UnitOptions = "<option>px</option><option>%</option>", 
                borderBlock, borderCheckbox, borderLabel, borderGroup, borderList, borderInput, borderSelect, 
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'),  
                currUnit = "px", currVal = 0,
                that = this;
            this.deserializeStyle(currStyle);
            borderBlock = document.createElement('fieldset');
            borderBlock.setAttribute('id', this.id);
            borderBlock.className = pluginNamespace;

            //fieldset checkbox and label
            borderCheckbox = $ektron('<input type="checkbox" id="chk-border' + this.id + '" />');
            borderCheckbox.on('change', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.targetElem.type !== '') {
                borderCheckbox.attr('checked', true);
            }
            borderBlock.appendChild(borderCheckbox.get(0)); 
            borderLabel = document.createElement('label');
            borderLabel.setAttribute('for', 'chk-border' + this.id);
            borderLabel.setAttribute('title', i18n.t('border.checkbox.border'));
            borderLabel.innerHTML = i18n.t('border.checkbox.border'); 
            borderBlock.appendChild(borderLabel); 

            // fields group
            borderGroup = document.createElement('ul');
            // border type select box
            borderList = document.createElement('li');
            borderLabel = document.createElement('label');
            borderLabel.setAttribute('for', 'ddBorderType');
            borderLabel.setAttribute('title', i18n.t('border.label.border-type'));
            borderLabel.className = this.nsClass('label');
            borderLabel.innerHTML = i18n.t('border.label.border-type');
            borderList.appendChild(borderLabel);
            borderGroup.appendChild(borderList);
            borderList = document.createElement('li');
            borderSelect = $ektron('<select id="ddBorderType" class="select-box ddBorderType">').html('<option value="all">' + i18n.t('border.label.all') + '</option><option value="top">' + i18n.t('border.label.top') + '</option><option value="bottom">' + i18n.t('border.label.bottom') + '</option><option value="top and bottom">' + i18n.t('border.label.top-bottom') + '</option><option value="left">' + i18n.t('border.label.left') + '</option><option value="right">' + i18n.t('border.label.right') + '</option><option value="left and right">' + i18n.t('border.label.left-right') + '</option>');
            borderSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.type !== '') {
                borderSelect.val(this.targetElem.type);
            }
            borderList.appendChild(borderSelect.get(0));
            borderGroup.appendChild(borderList);


            // border thickness
            borderList = document.createElement('li');
            borderLabel = document.createElement('label');
            borderLabel.setAttribute('for', 'txtThickness');
            borderLabel.setAttribute('title', i18n.t('border.label.border-thickness'));
            borderLabel.className = this.nsClass('label');
            borderLabel.innerHTML = i18n.t('border.label.border-thickness');
            borderList.appendChild(borderLabel);
            borderGroup.appendChild(borderList);
            borderList = document.createElement('li');
            borderInput = $ektron('<input id="txtThickness" type="text" class="input-box txtThickness textbox-narrow" />');
            borderInput.on('blur', function () { that.updateStyle(); });
            if (this.targetElem.thickness !== 0) { 
				currVal = this.targetElem.thickness.match(/\d/g).join('');
                borderInput.val(currVal);
				currUnit = this.targetElem.thickness.replace(currVal, "");
            }
            borderList.appendChild(borderInput.get(0));
            borderSelect = $ektron('<select id="ddThicknessUnit" class="select-box ddThicknessUnit">').html(UnitOptions);
            borderSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.thickness !== 0) { 
                borderSelect.val(currUnit);
            }
            borderList.appendChild(borderSelect.get(0));
            borderGroup.appendChild(borderList);
            
            // border style select box
            borderList = document.createElement('li');
            borderLabel = document.createElement('label');
            borderLabel.setAttribute('for', 'ddBorderStyle');
            borderLabel.setAttribute('title', i18n.t('border.label.border-style'));
            borderLabel.className = this.nsClass('label');
            borderLabel.innerHTML = i18n.t('border.label.border-style');
            borderList.appendChild(borderLabel);
            borderGroup.appendChild(borderList);
            borderList = document.createElement('li');
            borderSelect = $ektron('<select id="ddBorderStyle" class="select-box ddBorderStyle">').html('<option value="solid">' + i18n.t('border.label.solid') + '</option><option value="dotted">' + i18n.t('border.label.dotted') + '</option><option value="dashed">' + i18n.t('border.label.dashed') + '</option><option value="double">' + i18n.t('border.label.double') + '</option><option value="groove">' + i18n.t('border.label.groove') + '</option><option value="ridge">' + i18n.t('border.label.ridge') + '</option><option value="inset">' + i18n.t('border.label.inset') + '</option>' + '<option value="outset">' + i18n.t('border.label.outset') + '</option>');
            borderSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.style !== '') {
                borderSelect.val(this.targetElem.style);
            }
            borderList.appendChild(borderSelect.get(0));
            borderGroup.appendChild(borderList);

            // color value
            borderList = document.createElement('li');
            borderLabel = document.createElement('label');
            borderLabel.setAttribute('for', 'txtBorderColor' + this.id);
            borderLabel.setAttribute('title', i18n.t('border.label.border-color'));
            borderLabel.className = 'border-label';
            borderLabel.innerHTML = i18n.t('border.label.border-color');
            borderList.appendChild(borderLabel);
            borderGroup.appendChild(borderList);
            borderList = document.createElement('li');
            borderInput = $ektron('<input id="txtBorderColor' + this.id + '" type="text" class="input-box txtBorderColor" />');
            borderInput.on('input', function () { that.updateStyle(); });
            borderList.appendChild(borderInput.get(0));
            borderGroup.appendChild(borderList);

            borderBlock.appendChild(borderGroup); 
            if (this.targetElem.type === '') {
                borderGroup.setAttribute('style', 'display:none');
            }
            return borderBlock;
        };

        this.init = function () {
            var jResult = $ektron(this.targetElem.elem), colorPicker, borderType, that = this;

            //Initialize colorpicker
            colorPicker = new ColorHelper($ektron('#txtBorderColor' + that.id));
            colorPicker.start(that.targetElem.color);
        };

        this.updateStyle = function () {
            var styleRule = 'border', styleString = '', jResult = $ektron(this.targetElem.elem), that = this, type, thinkness, thinknessUnit, style, color;
            jResult.css('border', "").css('border-top', "").css('border-right', "").css('border-bottom', "").css('border-left', "");
            if (true === document.getElementById('chk-border' + this.id).checked) { 
                type = $ektron('#ddBorderType', document.getElementById(this.id)).val().toLowerCase();
                thinkness = $ektron('#txtThickness', document.getElementById(this.id)).val();
                thinknessUnit = $ektron('.ddThicknessUnit', document.getElementById(this.id)).val(); 
                style = $ektron('#ddBorderStyle', document.getElementById(this.id)).val().toLowerCase();
                color = $ektron('#txtBorderColor' + this.id, document.getElementById(this.id)).val();
                if (color.indexOf('#') < 0) { color = '#' + color;}

				styleString += ' ' + style;
                if (thinkness > 0) {
                    styleString += ' ' + thinkness + thinknessUnit;
                }
                else {
                    styleString += ' 1' + thinknessUnit;
                }
                if (color.length > 0) {
                    styleString += ' ' + color;
                }
				
				switch (type) {
				case "top and bottom":
					jResult.css('border-top', styleString);
					jResult.css('border-bottom', styleString);
					break;
				case "left and right":
					jResult.css('border-left', styleString);
					jResult.css('border-right', styleString);
					break;
				case "top":
				case "bottom":
				case "left":
				case "right":
					jResult.css('border-' + type, styleString);
					break;
				default:
					jResult.css('border', styleString);
					break;
                }
            }     
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, j, strStyle, currStyle, strProperty, allStyles, mutliType = new Array;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
				arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]).toLowerCase();
                    if (0 === strStyle.indexOf('border:') || 0 === strStyle.indexOf('border-'))
                    {
                        //Default to all, then update if other
                        this.targetElem.type = 'all';
						if (navigator.appVersion.indexOf( "MSIE" ) !== -1 || navigator.appVersion.indexOf( "WebKit" ) !== -1) {
                            if (0 === strStyle.indexOf('border-') && strStyle.indexOf('-style') > 0) {
                                currStyle = strStyle.substring(7, strStyle.indexOf(':')).replace(/-style/, "");
                                mutliType.push(currStyle);
                            }
						}
						else {
                            if (0 === strStyle.indexOf('border-') || 0 === strStyle.indexOf('border:')) {
                                currStyle = strStyle.substring(7, strStyle.indexOf(':'));
                                mutliType.push(currStyle);
						    }
						} 
                        arrProperty = strStyle.replace(/, /g, ",").split(' ');
						for (j = 1; j < arrProperty.length; j += 1)
						{
							strProperty = $ektron.trim(arrProperty[j]);
							if (strProperty.indexOf('px') > 0 || strProperty.indexOf('%') > 0)
							{
								this.targetElem.thickness = strProperty; 
							}
                            else if ($ektron.inArray(strProperty, this.allStyle) > -1) {
								this.targetElem.style = strProperty.toLowerCase();
							}
							else {
								this.targetElem.color = strProperty;
							}
						}
                    }
                }
                if (mutliType.length > 1) {
                    switch (mutliType[0]) {
                    case "top":
                    case "bottom":
                        this.targetElem.type = 'top and bottom';
                        break;
                    case "right":
                    case "left":
                        this.targetElem.type = 'left and right';
                        break;
                    default:
                        this.targetElem.type = 'all';
                        break;
                    }
                }
                else if (1 === mutliType.length) {
                    switch (mutliType[0]) {
                    case "top":
                        this.targetElem.type = 'top';
                        break;
                    case "bottom":
                        this.targetElem.type = 'bottom';
                        break;
                    case "left":
                        this.targetElem.type = 'left';
                        break;
                    case "right":
                        this.targetElem.type = 'right';
                        break;
                    default:
                        this.targetElem.type = 'all';
                        break;
                    }
                }
            }
        };

        this.showHideModule = function (elem) {
            $ektron(elem).siblings('ul').toggle();
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return BorderBlock;
});
