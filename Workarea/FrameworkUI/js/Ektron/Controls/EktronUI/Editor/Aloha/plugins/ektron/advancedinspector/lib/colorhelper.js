﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var namespace = "ektron-aloha-background-";

    function ColorHelper(textElem, obj) {
        this.textElem = textElem;
        this.obj = obj;
        this.hexColor = "";

        this.create = function () {
            return this;
        };

        // selectedColor is rgb value
        this.start = function (selectedColor) {
            var that = this;
            that.hexColor = that.hexc(selectedColor);
            $ektron(textElem).val(that.hexColor);
            //$ektron(textElem).val(that.hexColor);
            //Initialize colorpicker
            
            $ektron(textElem).minicolors({
                change: function (hex, opacity) {
                    if (hex.length === 7) {
                        $ektron.event.trigger({
                            type: "ekcolorChanged",
                            message: hex || 'transparent',
                            obj: $ektron(this)
                        });
                    }
                }
            });
        };

        this.hexc = function (colorval) {
            //translate color value rgb to hex - if no rgb value return white
            var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            if (parts) {
                delete (parts[0]);
                for (var i = 1; i <= 3; ++i) {
                    parts[i] = parseInt(parts[i]).toString(16);
                    if (parts[i].length == 1) parts[i] = '0' + parts[i];
                }
                return parts.join('');
            }
            else {
                return '';
            } 
        }
    }
    return ColorHelper;
});
