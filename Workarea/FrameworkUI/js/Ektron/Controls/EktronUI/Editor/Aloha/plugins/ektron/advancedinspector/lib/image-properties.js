﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var pluginNamespace = 'aloha-image-properties', modal;
    function ImagePropertiesBlock(id, obj, i18n, Dialog) {
        this.id = id;
        this.folderId = 0;
        this.targetElem = {
            imageObj: obj,
            url: '',
            altText: ''
        };

        this.create = function () {
            var imgPropBlock, imgPropGroup, urlLabel, urlInput, libButton,
                imgPropList, libLink, altLabel, altInput, editImgButton,
                that = this;
            this.deserializeProperties();
            imgPropBlock = document.createElement('fieldset');
            imgPropBlock.setAttribute('id', this.id);
            imgPropBlock.className = pluginNamespace + ' aloha';
            imgPropGroup = document.createElement('ul');

            if ($ektron(this.targetElem.imageObj).parent()[0].tagName == "FIGURE") { // hidden Image Source for Image Edit popup
                urlInput = $ektron('<input id="txturl' + this.id + '" type="hidden" class="input-box image-properties" placeholder="http://" data-attrname="src" />');
                if (this.targetElem.url !== '') {
                    urlInput.val(this.targetElem.url);
                }
                imgPropGroup.appendChild(urlInput.get(0));
            }
            else {//Image Source
                imgPropList = document.createElement('li');
                urlLabel = document.createElement('label');
                urlLabel.setAttribute('for', 'txturl' + this.id);
                urlLabel.setAttribute('title', i18n.t('image-properties.label.image-src'));
                urlLabel.innerHTML = i18n.t('image-properties.label.image-src');
                imgPropList.appendChild(urlLabel);
                imgPropGroup.appendChild(imgPropList);
                imgPropList = document.createElement('li');
                urlInput = $ektron('<input id="txturl' + this.id + '" type="text" class="input-box image-properties" placeholder="http://" data-attrname="src" />');
                urlInput.on('blur', function () { that.syncFields(this); that.updateProperties(); });
                if (this.targetElem.url !== '') {
                    urlInput.val(this.targetElem.url);
                }
                imgPropList.appendChild(urlInput.get(0));
                if (Ektron.Namespace.Exists("Ektron.Library.Media.AcceptInsert")) {
                    libButton = $ektron('<span id="btnLibrary' + this.id + '" class="btn-library" title="' + i18n.t('image-properties.label.library') + '" >...</span>').button();
                    imgPropList.appendChild(libButton.get(0));
                }
                imgPropGroup.appendChild(imgPropList);
            }
            //Alternate Text
            imgPropList = document.createElement('li');
            altLabel = document.createElement('label');
            altLabel.setAttribute('for', 'txtalt' + this.id);
            altLabel.setAttribute('title', i18n.t('image-properties.label.alt-text'));
            altLabel.innerHTML = i18n.t('image-properties.label.alt-text');
            imgPropList.appendChild(altLabel);
            imgPropGroup.appendChild(imgPropList);
            imgPropList = document.createElement('li');
            altInput = $ektron('<textarea id="txtalt' + this.id + '" row="2" col="50" class="input-box image-properties alttext" data-attrname="alt" />');
            altInput.on('blur', function () { that.syncFields(this); that.updateProperties(); });
            if (this.targetElem.altText != '') {
                altInput.val(this.targetElem.altText);
            }
            imgPropList.appendChild(altInput.get(0));
            imgPropGroup.appendChild(imgPropList);

            //Edit Image Button
            imgPropList = document.createElement('li');
            editImgButton = $ektron('<span id="btnImageEdit' + this.id + '" class="btn-imageedit btn-wide" title="' + i18n.t('image-properties.label.edit-image') + '" >' + i18n.t('image-properties.label.edit-image') + '</span>').button();
            imgPropList.appendChild(editImgButton.get(0));
            imgPropGroup.appendChild(imgPropList);

            imgPropBlock.appendChild(imgPropGroup);

            if ($ektron(this.targetElem.imageObj).parent()[0].tagName != "FIGURE") {
                var delFigtag = $ektron('<li><a href="#">Delete Image</a></li>').button();
                delFigtag.on('click', function () {
                    $ektron(that.targetElem.imageObj).parent().click();
                    $ektron(that.targetElem.imageObj).remove();
                    $ektron("a#dialog-minimize").click();
                });
                imgPropBlock.appendChild(delFigtag.get(0));

                var linkFigtag = $ektron('<li><a href="#">Add hyperlink to Image</a></li>').button();
                linkFigtag.on('click', function () {
                    if ($ektron(that.targetElem.imageObj).parent().get(0).tagName != "A") {
                        $ektron(that.targetElem.imageObj).wrap("<a>");
                        $ektron(that.targetElem.imageObj).click();
                        var trails = $ektron("button.trailbtn");
                        $ektron(trails[trails.length - 2]).click();
                    }
                });
                imgPropBlock.appendChild(linkFigtag.get(0));
            }

            return imgPropBlock;
        };

        this.syncFields = function (elem) {
            var updatedText = elem.value, tagname = elem.tagName.toLowerCase();
            $ektron(tagname + ".input-box.image-properties").each(function () {
                $ektron(this).val(updatedText);
            });
        };

        this.updateProperties = function () {
            var updatedImg = $ektron(Ektron.AdvancedInspector.ImageProperties.currentElement.imageObj), eInput, attrName, updatedText;
            $ektron(".input-box.image-properties").each(function () {
                eInput = $ektron(this);
                attrName = eInput.attr("data-attrname");
                if (updatedImg.attr(attrName) != 'undefined') {
                    updatedImg.attr(attrName, eInput.val());
                    if ('alt' === attrName) {
                        updatedImg.attr('title', eInput.val());
                        Ektron.AdvancedInspector.ImageProperties.currentElement.altText = eInput.val();
                    }
                    else {
                        Ektron.AdvancedInspector.ImageProperties.currentElement.url = eInput.val();
                    }
                }
            });
        };

        this.init = function () {
            var that = this;
            $ektron('.btn-library').on('click', function () {
                that.libraryButtonClick(this);
            });
            $ektron('.btn-imageedit').on('click', function () {
                that.imageEditButtonClick(this);
            });

            $ektron(document).on("ImageUpdate", function (event, jImg) {
                if ('string' === typeof jImg.tagName) {
                    jImg = $ektron(jImg);
                }
                jImg.click();
            });

            if (Ektron.Namespace.Register) {
                Ektron.Namespace.Register('Ektron.AdvancedInspector.ImageProperties');
                Ektron.AdvancedInspector.ImageProperties.currentElement = this.targetElem;
                Ektron.AdvancedInspector.ImageProperties.updateProperties = this.updateProperties;

                Ektron.Namespace.Register('Ektron.AdvancedInspector');
                Ektron.AdvancedInspector.Dialog = Dialog();

                Ektron.Namespace.Register('Ektron.Inspector.ImageEdit');
                Ektron.Inspector.ImageEdit.AcceptInsert = this.acceptInsert;
                Ektron.Inspector.ImageEdit.CloseDialog = this.closeDialog;
                Ektron.Inspector.ImageEdit.IsThumbnailImage = this.isThumbnailImage;
            }
        };

        this.deserializeProperties = function () {
            if (Aloha.activeEditable != null && Aloha.activeEditable.obj != null && Aloha.activeEditable.obj.data("ektronEditorData") != null) {
                this.folderId = Aloha.activeEditable.obj.data("ektronEditorData").folderId.toString();
            }
            var jElem = $ektron(this.targetElem.imageObj);
            if (jElem.length > 0) {
                this.targetElem.url = jElem.attr('src');
                this.targetElem.altText = jElem.attr('alt');
            }
        };

        this.libraryButtonClick = function () {
            var that = this,
            dialogOptions = $.extend({}, Ektron.Library.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                dialogClass: 'ektron-ux ektron-ux-dialog',
                autoOpen: false,
                width: 820,
                modal: true,
                zIndex: 100000001,
                closeText: '<span class="ui-icon ui-icon-closethick" title="' + i18n.t('advanced-inspector.label.close') + '" />',
                title: i18n.t('advanced-inspector.label.library')
            });
            Ektron.Library.Dialog.html('<div class="ektron-aloha-inspector-modal"><iframe id="ektron-aloha-inspector-modal-iframe" class="ektron-aloha-inspector-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
            Ektron.Library.Dialog.dialog('option', dialogOptions);

            Ektron.Library.Dialog.dialog('open');
            $ektron("iframe.ektron-aloha-inspector-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/mediamanager.aspx?actiontype=library&scope=all&EditorName=Aloha&autonav=" + this.folderId);
        };

        this.imageEditButtonClick = function (elem) {
            var that = this, imgPath = encodeURIComponent($ektron("input#txturl" + that.id).val()),
            dialogOptions = $.extend({}, Ektron.AdvancedInspector.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                dialogClass: 'ektron-ux ektron-ux-dialog',
                autoOpen: false,
                width: 820,
                modal: true,
                zIndex: 100000001,
                closeText: '<span class="ui-icon ui-icon-closethick" title="' + i18n.t('advanced-inspector.label.close') + '" />',
                title: i18n.t('image-properties.label.edit-image')
            });
            Ektron.AdvancedInspector.Dialog.html('<div class="ektron-aloha-inspector-modal"><iframe id="ektron-aloha-inspector-modal-iframe" class="ektron-aloha-inspector-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
            Ektron.AdvancedInspector.Dialog.dialog('option', dialogOptions);

            Ektron.AdvancedInspector.Dialog.dialog('open');
            setTimeout(function() {
                $ektron("iframe.ektron-aloha-inspector-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/ImageTool/ImageEdit.aspx?i=" + imgPath );
            }, 10);
        };

        this.acceptInsert = function (imageObj) {
            var image = {
                "ImageURL": "",
                "Width": "0",
                "Height": "0"
            }
            jQuery.extend(image, imageObj);

            var selectedEdit = jQuery('.aloha-editable-active'),
                imageSelected = $ektron(Ektron.AdvancedInspector.ImageProperties.currentElement.imageObj);
            if (imageSelected != null && imageSelected.length > 0) {
                var oImgParent = imageSelected.parent().get(0);
                if (Ektron.Inspector.ImageEdit.IsThumbnailImage(oImgParent)) {
                    var oldthumbnail = imageSelected.attr("data-ektron-url"), newthumbnail,
                        oldfilename = oldthumbnail.replace("/thumb_", "/"), newfilename = image.ImageURL,
                        modifynode;
                    // figure out what to replace at the parent
                    // we'll just replace the "oldfilename." (no extension) with "newfilename."
                    if (oldfilename.lastIndexOf(".") > 0) {
                        oldfilename = oldfilename.substring(0, oldfilename.lastIndexOf(".") + 1);
                    }
                    if (newfilename.lastIndexOf(".") > 0) {
                        newfilename = newfilename.substring(0, newfilename.lastIndexOf(".") + 1);
                    }
                    if (oldthumbnail.lastIndexOf(".") > 0) {
                        oldthumbnail = oldthumbnail.substring(0, oldthumbnail.lastIndexOf(".") + 1);
                    }
                    newthumbnail = newfilename.substring(0, newfilename.lastIndexOf("/") + 1) + "thumb_" + newfilename.substring(newfilename.lastIndexOf("/") + 1);

                    // have to modify grandparent element's innerHTML because we can't modify outerHTML
                    modifynode = imageSelected.parent().parent();
                    modifynode.html(modifynode.html().replace(new RegExp(oldthumbnail, "g"), newthumbnail).replace(new RegExp(oldfilename, "g"), newfilename));
                    imageSelected = $ektron("img[data-ektron-url^='" + newthumbnail + "']", modifynode);
                }
                else {
                    // change the src link to point at the updated image
                    imageSelected.attr("src", image.ImageURL);
                    imageSelected.attr("data-ektron-url", image.ImageURL);
                    imageSelected.removeAttr("width");
                    imageSelected.removeAttr("height");
                    imageSelected.width("");
                    imageSelected.height("");
                }
                $ektron(document).trigger("ImageUpdate", imageSelected);
            }
            Ektron.AdvancedInspector.Dialog.empty();
            Ektron.AdvancedInspector.Dialog.dialog("close");
        };

        this.isThumbnailImage = function (oImgParent) {
            var thumbstring = "try{window.open('",
                isThumbnail = false;
            if (oImgParent && (oImgParent.tagName == "A")) {
                var onclickattr = oImgParent.getAttribute("onclick");
                if (onclickattr != null) {
                    var clickinfo = onclickattr.toString();
                    var startindex = clickinfo.indexOf(thumbstring);
                    if (clickinfo.substring(startindex, startindex + thumbstring.length) == thumbstring) {
                        var imgstring = clickinfo.substring(startindex + thumbstring.length);
                        if (imgstring.indexOf("'")) {
                            isThumbnail = true;
                        }
                    }
                }
            }
            return isThumbnail;
        };

        this.closeDialog = function () {
            Ektron.AdvancedInspector.Dialog.dialog("close");
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function () {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function () {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return ImagePropertiesBlock;
});