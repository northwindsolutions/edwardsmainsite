﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-position';
    function PositionBlock(id, obj, i18n) {
        this.id = id;
        this.targetElem = {
            elem : obj,
            float : 0
        }; 

        this.create = function () {
            var positionBlock, positionCheckbox, positionLabel, positionBtnSet,
                positionInput, indentInput, alignmentSelect, 
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'), arrStyle, currUnit = 'EM',
                that = this;
            this.deserializeStyle(currStyle);
            positionBlock = document.createElement('fieldset');
            positionBlock.setAttribute('id', this.id);
            positionBlock.className = pluginNamespace;

            //fieldset checkbox and label
            positionCheckbox = $ektron('<input type="checkbox" id="chk-position" />');
            positionCheckbox.on('click', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasPosition()) {
                positionCheckbox.attr('checked', true);
            }
            positionBlock.appendChild(positionCheckbox.get(0)); 
            positionLabel = document.createElement('label');
            positionLabel.setAttribute('for', 'chk-position');
            positionLabel.setAttribute('title', i18n.t('position.checkbox.positionwrapping'));
            positionLabel.innerHTML = i18n.t('position.checkbox.positionwrapping'); 
            positionBlock.appendChild(positionLabel); 

            //position group
            positionBtnSet = document.createElement('div');
            positionBtnSet.setAttribute('id', 'position-radio');
            positionBtnSet.className = 'aloha';
            //float left
            positionInput = $ektron('<input type="radio" id="position-left" name="position-radio" />');
            positionInput.className = this.nsClass('position-radio');
            positionInput.on('click', function () { that.updateStyle(); return true; });
            if ('left' === this.targetElem.float || '0' === this.targetElem.float) {
                //default to left
                positionInput.attr('checked', true);
            }
            positionBtnSet.appendChild(positionInput.get(0));
            positionLabel = document.createElement('label');
            positionLabel.setAttribute('for', 'position-left');
            positionLabel.setAttribute('title', i18n.t('position.label.left'));
            positionLabel.className = this.nsClass('position-buttonset');
            positionLabel.innerHTML = i18n.t('position.label.left');
            positionBtnSet.appendChild(positionLabel);

            //float none
            positionInput = $ektron('<input type="radio" id="position-none" name="position-radio" />');
            positionInput.className = this.nsClass('position-radio');
            positionInput.on('click', function () { that.updateStyle(); return true; });
            if ('none' === this.targetElem.float) {
                positionInput.attr('checked', true);
            }
            positionBtnSet.appendChild(positionInput.get(0));
            positionLabel = document.createElement('label');
            positionLabel.setAttribute('for', 'position-none');
            positionLabel.setAttribute('title', i18n.t('position.label.none'));
            positionLabel.className = this.nsClass('position-buttonset');
            positionLabel.innerHTML = i18n.t('position.label.none');
            positionBtnSet.appendChild(positionLabel);

            //float right
            positionInput = $ektron('<input type="radio" id="position-right" name="position-radio" />');
            positionInput.className = this.nsClass('position-radio');
            positionInput.on('click', function () { that.updateStyle(); return true; });
            if ('right' === this.targetElem.float) {
                positionInput.attr('checked', true);
            }
            positionBtnSet.appendChild(positionInput.get(0));
            positionLabel = document.createElement('label');
            positionLabel.setAttribute('for', 'position-right');
            positionLabel.setAttribute('title', i18n.t('position.label.right'));
            positionLabel.className = this.nsClass('position-buttonset');
            positionLabel.innerHTML = i18n.t('position.label.right');
            positionBtnSet.appendChild(positionLabel);

            positionBlock.appendChild(positionBtnSet); 
            if (!this.hasPosition()) {
                positionBtnSet.setAttribute('style', 'display:none');
            }
            return positionBlock;
        };

        this.init = function () {
            $ektron('#position-radio', $ektron('#' + this.id)).buttonset();
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem.elem), valSelected, jSelected; 
            jResult.css('float', "");
            if (true === $ektron('input#chk-position', $ektron('#' + this.id)).is(':checked')) { 
                jSelected = $ektron('input[type=radio]:checked', $ektron('#' + this.id));
                if (jSelected.length > 0) {
                    valSelected = jSelected.attr('id').toLowerCase().replace(/position-/, "");
                    if (valSelected !== 'none') {
                        jResult.css('float', valSelected);
                    }
                }
            }                
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, strStyle;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
                arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]);
                    if (0 === strStyle.indexOf('float:'))
                    {
                        arrProperty = strStyle.split(':');
                        this.targetElem.float = $ektron.trim(arrProperty[1]); 
                    }
                }
            }
        };

        this.hasPosition = function () {
            if (this.targetElem.float !== 0) {
                return true;
            }
            return false;
        };
        
        this.showHideModule = function (elem) {
            $ektron(elem).siblings('div#position-radio').toggle();
            $ektron('#position-radio', $ektron('#' + this.id)).buttonset();
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return PositionBlock;
});