﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-reset';
    function ResetBlock(id, obj, i18n) {
        this.id = id;
        var resetClassInput = "txtResetClass" + this.id;
        var resetStyleInput = "txtResetStyle" + this.id;
        this.targetElem = {
            elem : obj
        }; 

        this.create = function () {
            var ResetBlock, resetGroup, resetList, resetLabel, resetInput, resetClassInput, resetStyleInput, currStyle, currClass,
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'),  
                currClass = jTarget.attr('class'),
                that = this;

            // aloha puts in the blue background for the recently modified link.
            if ('rgb(128,181,242)' === jTarget.css('background-color').replace(/, /g, ",")) {
                jTarget.css('background-color', '');
                currStyle = jTarget.attr('style');
            }

            ResetBlock = document.createElement('fieldset');
            ResetBlock.setAttribute('id', this.id);
            ResetBlock.className = pluginNamespace + ' aloha';

            //class and style fields group
            resetGroup = document.createElement('ul');
            //reset class
            resetList = document.createElement('li');
            resetLabel = document.createElement('label');
            resetLabel.setAttribute('for', "txtResetClass");
            resetLabel.setAttribute('title', i18n.t('reset-style.label.class'));
            resetLabel.className = this.nsClass('label');
            resetLabel.innerHTML = i18n.t('reset-style.label.class');
            resetList.appendChild(resetLabel);
            resetGroup.appendChild(resetList);
            resetList = document.createElement('li');
            resetInput = $ektron('<input id="txtResetClass" type="text" class="input-box" />');
            resetInput.val(currClass);
            resetInput.on('keyup', function () { that.updateClass(); });
            resetList.appendChild(resetInput.get(0));
            resetGroup.appendChild(resetList);
            
            //reset style
            resetList = document.createElement('li');
            resetLabel = document.createElement('label');
            resetLabel.setAttribute('for', "txtResetStyle");
            resetLabel.setAttribute('title', i18n.t('reset-style.label.style'));
            resetLabel.className = 'reset-label';
            resetLabel.innerHTML = i18n.t('reset-style.label.style');
            resetList.appendChild(resetLabel);
            resetGroup.appendChild(resetList);
            resetList = document.createElement('li');
            resetInput = $ektron('<input id="txtResetStyle" type="text" class="input-box" />');
            resetInput.val(currStyle);
            resetInput.on('keyup', function () { that.updateStyle(); });
            resetList.appendChild(resetInput.get(0));
            resetGroup.appendChild(resetList);

            //reset button
            resetList = document.createElement('li');
            resetInput = $ektron('<span id="btnResetClassStyle" type="button" class="button btn-wide" title="' + i18n.t('reset-style.label.button') + '" >' + i18n.t('reset-style.label.button') + '</span>').button();
            resetInput.on('click', function () { that.resetStyle(); });
            resetList.appendChild(resetInput.get(0));
            resetGroup.appendChild(resetList);

            ResetBlock.appendChild(resetGroup);


            return ResetBlock;
        };

        this.resetStyle = function () {
            var jResult = $ektron(this.targetElem.elem);
            jResult.attr('class', "").attr('style', "");
            $("#advInspectorm").find("input:checkbox:checked").prop('checked', false).trigger('change');
            jResult.click();
        };

        this.updateClass = function () {
            var jResult = $ektron(this.targetElem.elem);
            jResult.attr('class', $("#txtResetClass").val());
        };


        this.updateStyle = function () {
            var jResult = $ektron(this.targetElem.elem);
            jResult.attr('style', $("#txtResetStyle").val());
        };

        //Creates string with this component's namepspace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return ResetBlock;
});

    