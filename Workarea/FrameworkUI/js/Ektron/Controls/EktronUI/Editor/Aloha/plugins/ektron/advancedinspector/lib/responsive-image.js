﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../../../../../../../../Ektron.Namespace.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../../../../../../../../../../../../ux/vendor/jquery/plugins/jcarousel/jquery.jcarousel.min.js" />
define([''], function () {
    "use strict";
    var breakpointsList, breakpointsInfo, carouselGroup, modal,
		WorkareaPath = Ektron.Context.Cms.WorkareaPath, // todo: need to test with Ektron.Context.Cms.WorkareaPath
		pluginNamespace = 'aloha-responsive-image';
    function RespImgBlock(moduleId, obj, i18n, Dialog) {
        this.id = moduleId;
        this.targetElem = {
            elem: obj,
            tagName: '',
            imgPath: '',
            title: ''
        };
        this.widthPixelSpec = null;
        this.currentBreakpoint = {
            imgElem: null,
            id: '',
            name: '',
            width: 1,
            fileLabel: ''
        };

        this.create = function () {
            var respImgBlock, respImgCheckbox, respImgLabel, respImgButton, respImgDiv, respImgGroup, respImgList, respImgSpan,
                that = this;
            this.deserializeElem();
            //if (null === Ektron.Controls.Editor.Aloha.ResponsiveImageBreakPoints || 'undefined' == typeof Ektron.Controls.Editor.Aloha.ResponsiveImageBreakPoints) {
            breakpointsInfo = this.getBreakpoints();
            // }
            //else {
            // breakpointsInfo = Ektron.Controls.Editor.Aloha.ResponsiveImageBreakPoints;
            //}

            respImgBlock = document.createElement('fieldset');
            respImgBlock.setAttribute('id', this.id);
            respImgBlock.className = pluginNamespace + ' aloha';

            //fieldset checkbox and label
            respImgCheckbox = $ektron('<input type="checkbox" id="chk-responsive-image" />');
            respImgCheckbox.on('click', function () { that.showHideModule(this); that.updateStyle(); });
            if ('FIGURE' === this.targetElem.tagName) {
                respImgCheckbox.attr('checked', true);
            }
            respImgBlock.appendChild(respImgCheckbox.get(0));
            respImgLabel = document.createElement('label');
            respImgLabel.setAttribute('for', 'chk-responsive-image');
            respImgLabel.setAttribute('title', i18n.t('responsive-image.checkbox.responsive-image-selection'));
            respImgLabel.innerHTML = i18n.t('responsive-image.checkbox.responsive-image-selection');
            respImgBlock.appendChild(respImgLabel);

            respImgDiv = document.createElement('div');
            respImgDiv.className = this.nsClass('div');

            //breakpoint carousel
            if (typeof carouselGroup !== 'undefined') {
                respImgGroup = carouselGroup;
            }
            else {
                respImgGroup = document.createElement('ul');
                respImgGroup.className = 'jcarousel';
                // default breakpoint msg
                respImgList = document.createElement('li');
                respImgList.innerHTML = i18n.t('responsive-image.label.no-breakpoints-found');
                respImgGroup.appendChild(respImgList);
            }
            respImgDiv.appendChild(respImgGroup);
            respImgBlock.appendChild(respImgDiv);
            if (this.targetElem.tagName !== 'FIGURE') {
                respImgDiv.setAttribute('style', 'display:none');
            }
            var delFigtag = $ektron('<li><a href="#">Delete Responsive Image</a></li>').button();
            delFigtag.on('click', function () {
                $ektron(that.targetElem.elem).parent().click();
                $ektron(that.targetElem.elem).remove();
                $ektron("a#dialog-minimize").click();
            });
            respImgDiv.appendChild(delFigtag.get(0));

            var linkFigtag = $ektron('<li><a href="#">Add hyperlink to Responsive Image</a></li>').button();
            linkFigtag.on('click', function () {
                if ($ektron(that.targetElem.elem).parent().get(0).tagName != "A") {
                    $ektron(that.targetElem.elem).wrap("<a>");
                    $ektron(that.targetElem.elem).click();
                    var trails = $ektron("button.trailbtn");
                    $ektron(trails[trails.length - 2]).click();
                }
            });
            respImgDiv.appendChild(linkFigtag.get(0));

            return respImgBlock;
        };

        this.init = function () {
            var that = this, breakpointItems;
            if (typeof carouselGroup !== 'undefined' && 0 === $ektron('.change-btn').length) {
                $ektron('.jcarousel', document.getElementById(this.id)).replaceWith(carouselGroup);
            }
            try {
                breakpointItems = $ektron('.' + this.nsClass('breakpoints'), document.getElementById(this.id));
                if (breakpointItems.length > 0) {
                    breakpointItems.jcarousel();
                }
            }
            catch (ex) {
                if (typeof console !== 'undefined') {
                    console.log(ex.message);
                }
            }

            $ektron('.change-btn', document.getElementById(this.id)).on('click', function () {
                that.imageChangeButtonClick(this);
            });

            $ektron('.btn-imageedit').on('click', function () {
                that.imageEditButtonClick(this);
            });

            this.showHideModule(document.getElementById('chk-responsive-image'));

            if (Ektron.Namespace.Register) {
                Ektron.Namespace.Register('Ektron.AdvancedInspector.ResponsiveImage');
                Ektron.AdvancedInspector.ResponsiveImage.AcceptResponsiveInsert = this.acceptResponsiveInsert;
                Ektron.AdvancedInspector.ResponsiveImage.AcceptInsert = this.acceptInsert;
                Ektron.AdvancedInspector.ResponsiveImage.currentBreakpoint = this.currentBreakpoint;
                Ektron.AdvancedInspector.ResponsiveImage.updateStyle = this.updateStyle;
                Ektron.AdvancedInspector.ResponsiveImage.getImageSrc = this.getImageSrc;
                Ektron.AdvancedInspector.ResponsiveImage.getBreakPointSrc = this.getBreakPointSrc;
                Ektron.AdvancedInspector.ResponsiveImage.targetElem = this.targetElem;
                Ektron.AdvancedInspector.ResponsiveImage.widthPixelSpec = this.widthPixelSpec;
                Ektron.AdvancedInspector.ResponsiveImage.deserializeElem = this.deserializeElem;

                Ektron.Namespace.Register('Ektron.AdvancedInspector');
                Ektron.AdvancedInspector.Dialog = Dialog();
            }
        };

        this.updateStyle = function () {
            var output = "", jOutput, jResult = $ektron(Ektron.AdvancedInspector.ResponsiveImage.targetElem.elem), i, nextWidth = '', imgPath,
                defaultPath, prevPath, jFigureSet;
            if (null === this.widthPixelSpec) {
                if (null === Ektron.Controls.Editor.Aloha.ResponsiveImageBreakPoints) {
                    breakpointsInfo = this.getBreakpoints();
                }
                this.widthPixelSpec = breakpointsInfo.WidthPixelSpec;
                Ektron.AdvancedInspector.ResponsiveImage.widthPixelSpec = this.widthPixelSpec;
            }
            if (this.widthPixelSpec !== null) {
                if ($ektron('#chk-responsive-image', document.getElementById(moduleId)).is(':checked')) {
                    // FIGURE block
                    output = '<figure class="ektron-responsive-imageset fancy" title="' + Ektron.AdvancedInspector.ResponsiveImage.targetElem.title + '" data-ektron-image-src="' + Ektron.AdvancedInspector.ResponsiveImage.targetElem.imgPath + '" ';
                    if ('string' === typeof jResult.attr('id') && jResult.attr('id') !== 'undefined') {
                        output += 'id="' + jResult.attr('id') + '" ';
                    }
                    if ('string' === typeof jResult.attr('style') && jResult.attr('style') !== 'undefined') {
                        output += 'style="' + jResult.attr('style') + '" ';
                    }
                    //each breakpoint 
                    for (i = 0; i < Ektron.AdvancedInspector.ResponsiveImage.widthPixelSpec.length; i += 1) {
                        imgPath = $ektron('span.thumb[data-ektron-breakpointwidth=' + Ektron.AdvancedInspector.ResponsiveImage.widthPixelSpec[i] + ']').attr('data-ektron-breakpoint-image-src');
                        if (i + 1 == Ektron.AdvancedInspector.ResponsiveImage.widthPixelSpec.length) {
                            imgPath = imgPath.substr(0, imgPath.indexOf('?'));
                        }
                        if (prevPath !== imgPath) {
                            output += 'data-media' + nextWidth + '="' + imgPath + '" ';
                            prevPath = imgPath;
                        }
                        nextWidth = Ektron.AdvancedInspector.ResponsiveImage.widthPixelSpec[i] + 1;
                    }
                    output += '>';
                    output += '<noscript>';
                    output += '<img style="max-width:100%" src="' + Ektron.AdvancedInspector.ResponsiveImage.targetElem.imgPath + '" alt="' + Ektron.AdvancedInspector.ResponsiveImage.targetElem.title + '"/>';
                    output += '</noscript>';
                    output += '</figure>';
                    jOutput = $ektron(output);

                    jResult.replaceWith(jOutput);
                    jFigureSet = $ektron('figure.ektron-responsive-imageset');
                    jFigureSet.picture();
                    jFigureSet.find('img').css('max-width', '100%');

                    window.DragonDrop.bindDraggables();
                }
                else {
                    //IMG tag
                    output = '<img src="' + this.targetElem.imgPath + '" alt="' + this.targetElem.title + '"/>';
                    jOutput = $ektron(output);
                    if ('string' === typeof jResult.attr('id') && jResult.attr('id') !== 'undefined') {
                        jOutput.attr('id', jResult.attr('id'));
                    }
                    jResult.replaceWith(jOutput);
                    jOutput.click();
                    //Ektron.AdvancedInspector.ImageSetBlock.ImageSetDeactivate();
                }

                Ektron.AdvancedInspector.ResponsiveImage.targetElem.elem = jOutput.get(0);
                Ektron.AdvancedInspector.ResponsiveImage.deserializeElem();
            }
        };

        this.deserializeElem = function () {
            var origImg;
            this.targetElem.tagName = this.targetElem.elem.tagName;
            if ('IMG' === this.targetElem.tagName) {
                this.targetElem.imgPath = this.targetElem.elem.getAttribute('src').replace(/\\/g, "/");
                this.targetElem.title = this.targetElem.elem.getAttribute('alt');
            }
            else { //FIGURE tag
                origImg = this.targetElem.elem.getAttribute('data-ektron-image-src');
                if ('string' === typeof origImg) {
                    this.targetElem.imgPath = origImg.replace(/\\/g, "/");
                }
                else {
                    origImg = $ektron('img', this.targetElem.elem).attr('src');
                    if ('string' === typeof origImg) {
                        this.targetElem.imgPath = origImg.replace(/\\/g, "/");
                    }
                }
                this.targetElem.title = this.targetElem.elem.getAttribute('title');
            }
        };

        this.getBreakpoints = function () {
            var that = this, breakpointElements = null;
            $ektron.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/resources/advancedinspector.ashx",
                data: { "action": "getdevicebreakpointdatalist" },
                success: function (msg) {
                    breakpointsInfo = $ektron.parseJSON(msg)
                    breakpointElements = that.createBreakpointsCarousel();
                    Ektron.Controls.Editor.Aloha.ResponsiveImageBreakPoints = breakpointsInfo;
                }
            });
            return breakpointElements;
        };

        this.createBreakpointsCarousel = function () {
            var respImgList, respImgLabel, respImgInput, respImgPrefix, editImgButton, nameSpan, imgSpan, imgPath, imgFilename, breakpointFilename, thumbPath, dataMediaPath, widthSpan, buttonSpan, i, itemId, prevWidth = '', lastSlash, lastPeriod, that = this;
            if (typeof breakpointsInfo !== 'undefined' && breakpointsInfo.enableDeviceDetection && typeof breakpointsInfo.WidthPixelSpec !== 'undefined' && breakpointsInfo.WidthPixelSpec !== null && breakpointsInfo.WidthPixelSpec.length > 0) {
                carouselGroup = document.createElement('ul');
                carouselGroup.className = this.nsClass('breakpoints') + ' jcarousel-skin-tango';
                carouselGroup.setAttribute('id', this.nsClass('breakpoints'));

                this.widthPixelSpec = breakpointsInfo.WidthPixelSpec;
                breakpointsList = breakpointsInfo.BreakpointDataList;
                //each breakpoint 
                for (i = 0; i < breakpointsList.length; i += 1) {
                    itemId = breakpointsList[i].Name.replace(/\s/g, "") + '_' + this.id + '_' + breakpointsList[i].Width;
                    respImgList = document.createElement('li');
                    // radio button
                    respImgInput = $ektron('<input type="radio" name="breakpointImage" id="' + itemId + '" value="' + breakpointsList[i].Name + '">');
                    respImgList.appendChild(respImgInput.get(0));
                    // breakpoint group label  
                    respImgLabel = document.createElement('label');
                    respImgLabel.setAttribute('for', itemId);
                    respImgLabel.setAttribute('title', breakpointsList[i].Name);
                    respImgLabel.className = this.nsClass('label') + ' breakpointName';
                    // name span
                    nameSpan = document.createElement('span');
                    nameSpan.className = 'name';
                    nameSpan.innerHTML = breakpointsList[i].Name;
                    respImgLabel.appendChild(nameSpan);
                    respImgPrefix = breakpointsList[i].Prefix;
                    // background image span
                    imgPath = this.targetElem.imgPath;
                    imgFilename = imgPath;
                    dataMediaPath = '';
                    if ('FIGURE' === this.targetElem.tagName) {
                        dataMediaPath = $ektron(this.targetElem.elem).attr('data-media' + prevWidth);
                        if (typeof dataMediaPath !== 'undefined' && dataMediaPath.length > 0) {
                            imgFilename = dataMediaPath;
                        }
                        thumbPath = this.getImageSrc(imgFilename, 'thumb');
                        if (!this.urlExists(thumbPath)) {
                            thumbPath = this.getImageSrc(imgPath, 'thumb');
                        }
                        breakpointFilename = this.getBreakPointSrc(imgFilename, breakpointsList[i].FileLabel);
                        imgSpan = $ektron('<span class="thumb" data-ektron-breakpointwidth="' + breakpointsList[i].Width + '" data-ektron-breakpoint-image-src="' + breakpointFilename + '">').css('backgroundImage', 'url(' + thumbPath + ')');
                        prevWidth = breakpointsList[i].Width + 1;
                    }
                    else {
                        breakpointFilename = this.getBreakPointSrc(this.targetElem.imgPath, breakpointsList[i].FileLabel);
                        imgSpan = $ektron('<span class="thumb" data-ektron-breakpointwidth="' + breakpointsList[i].Width + '" data-ektron-breakpoint-image-src="' + breakpointFilename + '">').css('backgroundImage', 'url(' + this.getImageSrc(imgPath) + ')');
                    }
                    respImgLabel.appendChild(imgSpan.get(0));
                    // width span
                    widthSpan = document.createElement('span');
                    widthSpan.className = 'width';
                    widthSpan.innerHTML = '(' + this.getWidthLabel(breakpointsList[i].Width) + ')';
                    respImgLabel.appendChild(widthSpan);
                    // button
                    buttonSpan = $ektron('<span class="change-btn" id="' + itemId + '-change-btn" data-ektron-name="' + breakpointsList[i].Name + '" data-ektron-breakpointwidth="' + breakpointsList[i].Width + '" data-ektron-fileLabel="' + breakpointsList[i].FileLabel + '">').html(i18n.t('responsive-image.label.change-button')).button();
                    //var editimgpath = thumbPath.replace('thumb_', respImgPrefix + '_');

                    //editImgButton = $ektron('<span id="btnImageEdit' + this.id + '" class="btn-imageedit btn-wide" title="Edit" data-ektron-breakpoint-image-src="http://localhost' + editimgpath + '">Edit</span>').button();
                    respImgLabel.appendChild(buttonSpan.get(0));
                    //respImgLabel.appendChild(editImgButton.get(0));

                    respImgList.appendChild(respImgLabel);
                    carouselGroup.appendChild(respImgList);
                }
            }
        };

        this.imageEditButtonClick = function (elem) {
            var that = this,
            dialogOptions = $.extend({}, Ektron.AdvancedInspector.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                dialogClass: 'ektron-ux ektron-ux-UITheme ektron-ux-dialog ux-app-siteApp-dialog',
                autoOpen: false,
                width: 820,
                modal: true,
                zIndex: 100001,
                title: i18n.t('image-properties.label.edit-image')
            });
            Ektron.AdvancedInspector.Dialog.html('<div class="ektron-aloha-inspector-modal"><iframe id="ektron-aloha-inspector-modal-iframe" class="ektron-aloha-inspector-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
            Ektron.AdvancedInspector.Dialog.dialog('option', dialogOptions);

            Ektron.AdvancedInspector.Dialog.dialog('open');
            var path = $ektron(elem).data('ektron-breakpoint-image-src');

            $ektron("iframe.ektron-aloha-inspector-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/ImageTool/ImageEdit.aspx?i=" + encodeURIComponent(path));
        };

        this.imageChangeButtonClick = function (elem) {
            var that = this,
                dialogOptions = $.extend({}, Ektron.AdvancedInspector.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                    autoOpen: false,
                    width: 620,
                    modal: true,
                    zIndex: 100000001,
                    closeText: '<span class="ui-icon ui-icon-closethick" title="' + i18n.t('advanced-inspector.label.close') + '" />',
                    title: i18n.t('responsive-image.checkbox.responsive-image-selection')
                });
            Ektron.AdvancedInspector.Dialog.html('<div class="ektron-aloha-inspector-modal"><iframe id="ektron-aloha-inspector-modal-iframe" class="ektron-aloha-inspector-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="582"/></div>');
            Ektron.AdvancedInspector.Dialog.dialog('option', dialogOptions);

            this.currentBreakpoint.imgElem = $ektron(elem).siblings('.thumb');
            this.currentBreakpoint.id = $ektron(elem).attr('id');
            this.currentBreakpoint.name = $ektron(elem).attr('data-ektron-name');
            this.currentBreakpoint.width = $ektron(elem).attr('data-ektron-breakpointwidth');
            this.currentBreakpoint.fileLabel = $ektron(elem).attr('data-ektron-fileLabel');

            Ektron.AdvancedInspector.Dialog.dialog('open');
            $ektron("iframe.ektron-aloha-inspector-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/imageinsert.aspx?breakpointwidth=" + this.currentBreakpoint.width);
            Ektron.AdvancedInspector.Dialog.dialog({ title: i18n.t('responsive-image.dialog.title') + this.currentBreakpoint.name });
        };

        this.acceptInsert = function (imageObj) {
            if (this.currentBreakpoint.imgElem !== null) {
                var FileNameForBreakpoint = this.getBreakPointSrc(imageObj.path, this.currentBreakpoint.fileLabel);
                $ektron(this.currentBreakpoint.imgElem).attr('data-ektron-breakpoint-image-src', FileNameForBreakpoint).css('background-image', 'url("' + this.getImageSrc(imageObj.path) + '")');
                this.currentBreakpoint.name = imageObj.title;
                if (typeof Ektron.AdvancedInspector.Dialog !== 'undefined') {
                    Ektron.AdvancedInspector.Dialog.dialog("close");
                }
                this.updateStyle();
            }
        };

        this.acceptResponsiveInsert = function (imageObj) {
            //todo
            Ektron.AdvancedInspector.Dialog.empty();
            Ektron.AdvancedInspector.Dialog.dialog("close");
        };

        this.getImageSrc = function (imgSrc, preName) {
            imgSrc = imgSrc.replace(/\s/g, '%20');
            var retSrc = imgSrc, lastSlash, lastPeriod, paramPos, ext;
            if ('undefined' === typeof preName) {
                preName = 'thumb';
            }
            if ('string' === typeof imgSrc && imgSrc.length > 0 && imgSrc.toLowerCase().indexOf('/uploadedimages/') > -1) {
                lastSlash = imgSrc.lastIndexOf('/');
                lastPeriod = imgSrc.lastIndexOf('.');
                paramPos = imgSrc.lastIndexOf('?');
                if (paramPos > 0) {
                    imgSrc = imgSrc.substring(0, paramPos);
                }
                ext = imgSrc.substr(lastPeriod + 1);
                if ('gif' === ext.toLowerCase()) {
                    ext = 'png';
                }
                retSrc = imgSrc.substring(0, lastSlash + 1) + preName + '_' + imgSrc.substring(lastSlash + 1, lastPeriod + 1) + ext;
            }
            return retSrc;
        };

        this.getBreakPointSrc = function (imgSrc, breakpointLabel) {
            var retSrc = imgSrc, paramPos;
            if ('string' === typeof imgSrc && imgSrc.length > 0) {
                paramPos = imgSrc.lastIndexOf('?');
                if (paramPos > 0) {
                    imgSrc = imgSrc.substring(0, paramPos);
                }
                retSrc = imgSrc + '?targetTypeID=' + breakpointLabel;
            }
            return retSrc;
        };

        this.getWidthLabel = function (widthValue) {
            var retLabel = '', idx;
            if (this.widthPixelSpec.length > 0) {
                idx = $ektron.inArray(widthValue, this.widthPixelSpec);

                if (0 === idx) {
                    retLabel = '0 &lt; ' + widthValue + 'px';
                }
                else if (idx > 0) {
                    retLabel = (this.widthPixelSpec[idx - 1] + 1) + ' &lt; ' + widthValue + 'px';
                }
            }
            return retLabel;
        };

        this.urlExists = function (testUrl) {
            var http = $ektron.ajax({
                type: "HEAD",
                url: testUrl,
                async: false
            })
            return (200 === http.status ? true : false);
        };

        this.showHideModule = function (elem) {
            var jElem = $ektron(elem), breakpointItems;
            if (jElem.is(':checked')) {
                jElem.siblings('div.' + this.nsClass('div')).css('display', 'block');
                jElem.closest('fieldset').height('400');
            }
            else {
                jElem.siblings('div.' + this.nsClass('div')).css('display', 'none');
                jElem.closest('fieldset').height('1.3em');
            }
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function () {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function () {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return RespImgBlock;
});