﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-table-style';
    function TableStyleBlock(id, obj, i18n) {
        this.id = id;
        this.targetElem = {
            elem : obj,
            borderCollapse : 0,
            captionSide : 0
        }; 

        this.create = function () {
            var tableStyleBlock, tableStyleCheckbox, tableStyleLabel, tableStyleSelect, tableStyleGroup, tableStyleList,
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'), arrStyle, currPosition = 'top',
                that = this;
            this.deserializeStyle(currStyle);
            tableStyleBlock = document.createElement('fieldset');
            tableStyleBlock.setAttribute('id', this.id);
            tableStyleBlock.className = pluginNamespace;

            //fieldset checkbox and label
            tableStyleCheckbox = $ektron('<input type="checkbox" id="chk-tablestyle" />');
            tableStyleCheckbox.on('click', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasTableStyles(currStyle)) {
                tableStyleCheckbox.attr('checked', true);
            }
            tableStyleBlock.appendChild(tableStyleCheckbox.get(0)); 
            tableStyleLabel = document.createElement('label');
            tableStyleLabel.setAttribute('for', 'chk-tablestyle');
            tableStyleLabel.setAttribute('title', i18n.t('table-style.checkbox.table-styles'));
            tableStyleLabel.innerHTML = i18n.t('table-style.checkbox.table-styles'); 
            tableStyleBlock.appendChild(tableStyleLabel); 

            //all styles group
            tableStyleGroup = document.createElement('ul');
            //Border Collapse
            tableStyleList = document.createElement('li');
            tableStyleCheckbox = $ektron('<input type="checkbox" id="chk-bordercollapse" />');
            tableStyleCheckbox.on('click', function () { that.updateStyle(); });
            if ('collapse' === this.targetElem.borderCollapse) {
                tableStyleCheckbox.attr('checked', this.targetElem.borderCollapse);
            }
            tableStyleList.appendChild(tableStyleCheckbox.get(0)); 
            tableStyleGroup.appendChild(tableStyleList);
            tableStyleLabel = document.createElement('label');
            tableStyleLabel.setAttribute('for', 'chk-bordercollapse');
            tableStyleLabel.setAttribute('title', i18n.t('table-style.label.collapse-cell-borders'));
            tableStyleLabel.className = this.nsClass('label');
            tableStyleLabel.innerHTML = i18n.t('table-style.label.collapse-cell-borders');
            tableStyleList.appendChild(tableStyleLabel);
            tableStyleGroup.appendChild(tableStyleList);

            //Caption position
            tableStyleList = document.createElement('li');
            tableStyleLabel = document.createElement('label');
            tableStyleLabel.setAttribute('for', 'ddCaptionPos');
            tableStyleLabel.setAttribute('title', i18n.t('table-style.label.caption-position'));
            tableStyleLabel.className = this.nsClass('label');
            tableStyleLabel.innerHTML = i18n.t('table-style.label.caption-position');
            tableStyleList.appendChild(tableStyleLabel);
            tableStyleGroup.appendChild(tableStyleList);
            tableStyleList = document.createElement('li');
            tableStyleSelect = $ektron('<select id="ddCaptionPos" class="select-box"><option>' + i18n.t('table-style.label.top') + '</option><option>' + i18n.t('table-style.label.bottom') + '</option></select>');
            tableStyleSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.captionSide !== 0) {
                tableStyleSelect.val(this.targetElem.captionSide.charAt(0).toUpperCase() + this.targetElem.captionSide.slice(1));
            }
            tableStyleList.appendChild(tableStyleSelect.get(0));
            tableStyleGroup.appendChild(tableStyleList);

            tableStyleBlock.appendChild(tableStyleGroup); 
            if (!this.hasTableStyles(currStyle)) {
                tableStyleGroup.setAttribute('style', 'display:none');
            }
            return tableStyleBlock;
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem.elem), captionSide, borderCollapse;
            jResult.css('border-collapse', "").css('caption-side', "");
            if (true === document.getElementById('chk-tablestyle').checked) { 
                borderCollapse = $ektron('#chk-bordercollapse').is(':checked');
                if (borderCollapse === true)
                {
                    jResult.css('border-collapse', 'collapse'); 
                }

                captionSide = $ektron('#ddCaptionPos').val().toLowerCase();
                jResult.css('caption-side', captionSide);
            }                
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, strStyle;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
                arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]);
                    if (0 === strStyle.indexOf('border-collapse:'))
                    {
                        arrProperty = strStyle.split(':');
                        this.targetElem.borderCollapse = $ektron.trim(arrProperty[1]); 
                    }

                    if  (0 === strStyle.indexOf('caption-side:')) {
                        arrProperty = strStyle.split(':');
                        this.targetElem.captionSide = $ektron.trim(arrProperty[1]); 
                    }
                }
            }
        };

        this.hasTableStyles = function () {
            if (this.targetElem.borderCollapse !== 0 || this.targetElem.captionSide !== 0) {
                return true;
            }
            return false;
        };

        this.showHideModule = function (elem) {
            //$ektron(elem).siblings('ul').toggle();
            var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                jElem.siblings('ul').show();
            }
            else {
                jElem.siblings('ul').hide();
            }
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return TableStyleBlock;
});