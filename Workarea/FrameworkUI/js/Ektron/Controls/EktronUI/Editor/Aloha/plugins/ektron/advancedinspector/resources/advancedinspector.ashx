<%@ WebHandler Language="C#" Class="responsive" %>

using System;
using System.Web;
using System.Text;
using System.Collections.Generic;
using Ektron.Cms;
using Ektron.Cms.Framework;
using Ektron.Cms.Settings.Mobile;
using Ektron.Cms.Framework.Settings.Mobile;
using Ektron.Newtonsoft.Json;

#region JsonObject
[JsonObject(MemberSerialization.OptIn)]
public class BreakpointsInfo
{
    [JsonProperty("WidthPixelSpec")]
    public int[] WidthPixelSpec = null;
    [JsonProperty("BreakpointDataList")]
    public List<BreakpointData> BreakpointDataList = null;
    [JsonProperty("enableDeviceDetection")]
    public bool enableDeviceDetection = false;
    [JsonProperty("folderPath")]
    public string folderPath = "\\";
}

[JsonObject(MemberSerialization.OptIn)]
public class BreakpointData
{
    [JsonProperty("Name")]
    public string Name = "";
    [JsonProperty("Width")]
    public int Width = 1;
    [JsonProperty("FileLabel")]
    public string FileLabel = "";
    [JsonProperty("Prefix")]
    public string Prefix = "";
}

[JsonObject(MemberSerialization.OptIn)]
public class Jsonexception
{
    [JsonProperty("message")]
    public string message = "";
    [JsonProperty("innerMessage")]
    public string innerMessage = "";
}
#endregion
    public class responsive : IHttpHandler
    {
        private List<int> WidthPixelSpec = new List<int>();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Buffer = false;

            try
            {
                string response = "";
                if (context.Request["action"] != null)
                {
                    switch (context.Request["action"])
                    {
						case "getdevicebreakpointdatalist":
                            response = getBreakpointImageData(context);
							break;
                        case "createbreakpointimages":
                            if (context.Request["path"] != null)
                            {
                                response = createBreakpointImages(context.Request["path"]);
                            }
                            break;
                        case "getallfonts":
                            response = getallfonts();
                            break;
                    }
                }
                
				context.Response.Write(response);
            }
            catch (Exception e)
            {
                Jsonexception ex = new Jsonexception();
                ex.message = e.Message;
                if (e.InnerException != null) ex.innerMessage = e.InnerException.Message;

                context.Response.Write(JsonConvert.SerializeObject(ex));
            }
            context.Response.End();
        }

        public string getBreakpointImageData(HttpContext context)
		{
            DeviceBreakpointManager m = new DeviceBreakpointManager();
            DeviceBreakpointCriteria c = new DeviceBreakpointCriteria();
            c.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            c.OrderByField = DeviceBreakpointProperty.Width;

            List<Ektron.Cms.Settings.Mobile.DeviceBreakpointData> lst = m.GetList(c);
            BreakpointsInfo retInfo = new BreakpointsInfo();
            List<BreakpointData> bpDataList = new List<BreakpointData>();
            foreach (Ektron.Cms.Settings.Mobile.DeviceBreakpointData data in lst)
            {
                if (data.AdaptiveImageData != null && true == data.AdaptiveImageData.Enabled)
                {
                BreakpointData newlst = new BreakpointData();
                newlst.Name = data.Name;
                newlst.Prefix = data.AdaptiveImageData.FileLabel;
                newlst.Width = data.Width;
                newlst.FileLabel = data.FileLabel;
                this.WidthPixelSpec.Add(data.Width);
                bpDataList.Add(newlst);
            }
            }
            if (bpDataList.Count > 0)
            {
            this.WidthPixelSpec.Sort();
            retInfo.WidthPixelSpec = WidthPixelSpec.ToArray();
            retInfo.BreakpointDataList = bpDataList;
            }
            
            string enabledevice = System.Configuration.ConfigurationManager.AppSettings["ek_EnableDeviceDetection"].ToString();
            if (enabledevice.ToLower() == "true")
            {
                retInfo.enableDeviceDetection = true;
            }

            if (!string.IsNullOrEmpty(context.Request["folderid"]))
            {
                retInfo.folderPath = getfolderPath(context.Request["folderid"]);
            }
            
            return JsonConvert.SerializeObject(retInfo);
		}
        
        public string createBreakpointImages(string path)
        {
            string retMessage = "complete";
            ContentAPI m_refContentApi = new ContentAPI();
            try
            {
                Ektron.ASM.EkHttpDavHandler.AdaptiveImageProcessor.Instance.ProcessImageForAllConfig(m_refContentApi.RequestInformationRef, HttpContext.Current.Server.MapPath(path));
            }
            catch (Exception ex)
            {
                retMessage = ex.Message;
            }
            return retMessage;
        }
        
        public string getfolderPath(string folderId)
        {
            string folderPath = "/";
            long fid = 0;
            if (!string.IsNullOrEmpty(folderId))
            {
                long.TryParse(folderId, out fid);
                Ektron.Cms.API.Folder fldApi = new Ektron.Cms.API.Folder();
                folderPath = fldApi.GetPath(fid);
            }
	    folderPath = folderPath.Replace(" ","_");
            return folderPath + "\\";
        }

        public string getallfonts()
        {
            string ret = string.Empty;
			FontData[] font_data_list;
            ContentAPI refContApi = new ContentAPI();
            font_data_list = refContApi.GetAllFonts();
            if (!(font_data_list == null))
            {
				for (int i=0; i <= font_data_list.Length - 1; i++) {
					if (i > 0) { ret += ","; }
					ret += @"""" + font_data_list[i].Face + @"""";
                }
				ret = @"[" + ret + @"]";
            }
            return ret;
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }