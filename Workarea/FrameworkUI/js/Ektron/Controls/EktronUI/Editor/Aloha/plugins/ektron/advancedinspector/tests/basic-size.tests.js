﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../lib/spacing-style.js" />

var block, myblock, BasicSizeBlock, $ektron, StyleConfigs = {}, Ektron = {Context : {Cms : { UIPath : "/e87/Workarea/FrameworkUI/"}}},
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    };
    

module("basic style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        BasicSizeBlock = window.createModule();
        block = new BasicSizeBlock();
    }
});

test('Does the slider load properly', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block = new BasicSizeBlock('my-div', document.getElementById('tabletest')),
        arrInputBox, arrSelectBox;
    myblock = block.create();
    container.append(myblock);
    block.start();
    ok(container.length > 0);
});

test('Does the image size properly', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block = new BasicSizeBlock('my-div2', document.getElementById('imgtest')),
        arrInputBox, arrSelectBox;
    myblock = block.create();
    container.append(myblock);
    block.start();
    ok(container.length > 0);
});