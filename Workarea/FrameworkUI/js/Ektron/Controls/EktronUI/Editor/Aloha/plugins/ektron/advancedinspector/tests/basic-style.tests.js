﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../lib/spacing-style.js" />

var block, myblock, BasicStyleBlock, $ektron, StyleConfigs = {}, Ektron = {Context : {Cms : { UIPath : "/e87/Workarea/FrameworkUI/"}}},
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    getStyleConfig = function () {
        return StyleConfigs = {
            "DIV": {
                "styles": {
                    "Indent":
                    {
                        "type": "style",
                        "value": "padding-left:20px;",
                        "workareaBox": "padding-left:20px;"
                    },
                    "Center":
                            {
                                "type": "style",
                                "value": "text-align:center;",
                                "workareaBox": "text-align:center;"
                            },
                    "Left Align":
                            {
                                "type": "style",
                                "value": "text-align:left;",
                                "workareaBox": "text-align:left;"
                            },
                    "Right Align":
                            {
                                "type": "style",
                                "value": "text-align:right;",
                                "workareaBox": "text-align:right;"
                            },
                    "Justify":
                            {
                                "type": "style",
                                "value": "text-align:justify;",
                                "workareaBox": "text-align:justify;"
                            }

                }
            }
        };
    };

module("basic style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        BasicStyleBlock = window.createModule();
        block = new BasicStyleBlock();
    }
});

test('Create list of styles and then try applying Center style', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block = new BasicStyleBlock('my-div', document.getElementById('divResult'), getStyleConfig()),
        arrInputBox, arrSelectBox;
    myblock = block.create();
    container.append(myblock);
    ok(container.length > 0, 'Style box has elements');
    stop();
    var style = $ektron("div[data-styleref='Center']");
    style.click();
    setTimeout(function () {
        //Make assertion
        ok($ektron("#divResult").attr('style').length > 0);
        start();
    }, 1);
});