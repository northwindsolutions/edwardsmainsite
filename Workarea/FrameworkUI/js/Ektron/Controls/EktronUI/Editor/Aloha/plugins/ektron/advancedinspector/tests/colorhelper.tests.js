﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../lib/jpicker-1.1.6.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) {
    var thisLoc = document.location.href,
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9);
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, ColorHelper, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "background.checkbox.label": "Background" },
        t: function (index) {
            return this.i18njs[index];
        }
    };

module("ColorHelper ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        ColorHelper = window.createModule(i18n);
        block = new ColorHelper();
    }
});

test('ColorHelper fieldset created successfully', function () {
    "use strict";
    var foundObj = false, cssColor, hexColor,
        container = $ektron('#dialog-container'),
        block = new ColorHelper(document.getElementById('Text1'));
    myblock = block.create();
    container.append(myblock);
    cssColor = $ektron('#divResult').css('background-color');
    hexColor = block.hexc(cssColor);
    //Pass in rgb value of .css('color')
    block.start(cssColor);
    foundObj = $ektron('.minicolors-panel');
    ok(foundObj.length > 0, 'ColorHelper jPicker found.');
    ok(cssColor = "rgb(255, 0, 255)");
    ok(hexColor = "ff00ff");
    //Attach event to handle jPicker OK click
    setTimeout(function () {
        $ektron('table.jPicker').find('input.Ok').on('click', function () { alert('OK clicked'); });
    }, 300);
});

test('ColorHelper fieldset created successfully', function () {
    "use strict";
    var foundObj = false, cssColor, hexColor,
        container = $ektron('#dialog-container'),
        block = new ColorHelper(document.getElementById('Text2'));
    myblock = block.create();
    container.append(myblock);
    cssColor = $ektron('#divResult1').css('background-color');
    hexColor = block.hexc(cssColor);
    //Pass in rgb value of .css('color')
    block.start(cssColor);
    foundObj = $ektron('.minicolors-panel');
    ok(foundObj.length > 0, 'ColorHelper jPicker found.');

});




