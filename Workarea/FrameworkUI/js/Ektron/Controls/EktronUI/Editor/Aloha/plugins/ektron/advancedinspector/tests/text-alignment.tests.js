﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/text-alignment.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, TextAlignmentBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "text-alignment.checkbox.text-alignment": "Text Alignment", "text-alignment.label.text-alignment": "Text Alignment", "text-alignment.label.left": "Left", 
        "text-alignment.label.right": "Right", "text-alignment.label.center": "Center", "text-alignment.label.default": "Default", "text-alignment.label.top": "Top", 
        "text-alignment.label.middle": "Middle", "text-alignment.label.baseline": "Baseline", "text-alignment.label.bottom": "Bottom", 
        "text-alignment.label.justify": "Justify", "text-alignment.label.vertical-alignment": "Vertical Alignment", "text-alignment.label.indent-first-line": "Indent first Line" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Text alignment style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        TextAlignmentBlock = window.createModule(i18n);
        block = new TextAlignmentBlock();
    }
});

test('Text alignment fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundAlignBox, foundVAlignBox,
        container = $ektron('#dialog-container'),
        block = new TextAlignmentBlock('my-div', document.getElementById('divResult'), i18n),
        foundIndentBox, foundSelectBox, forId;
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('#my-div', container);
    foundCheckbox = $ektron('input#chk-alignment', container);
    foundAlignBox = $ektron('#ddTextAlignment', container);
    foundVAlignBox = $ektron('#ddVAlignment', container);
    foundIndentBox = $ektron('#txtIndent', container);
    foundSelectBox = $ektron('.select-box', container);

    ok(foundBlock.length > 0, 'Text alignment fieldset found.');
    equal(foundCheckbox.length, 1, 'Text alignment checkbox found.');
    ok(foundAlignBox.length > 0, 'Options for text align found.');
    ok(foundVAlignBox.length > 0, 'Options for vertical align found.');
    equal(foundIndentBox.length, 1, 'Indent text box found.');
    ok(foundSelectBox.length > 0, 'select box found.');
    forId = foundSelectBox.siblings('.input-box');
    ok(foundSelectBox.hasClass(forId.attr('id')), 'correct class name found in select box');
});

test('Text alignment are not set when checkbox is not checked', function () {
    "use strict";
    var align = 'Left',
        valign = 'Top',
        indent = 40,
        unit = "px",
        container = $ektron('#dialog-container'),
        block = new TextAlignmentBlock('my-div', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#ddTextAlignment', container).val(align);
    $ektron('#ddVAlignment', container).val(valign);
    $ektron('#txtIndent', container).val(indent);
    $ektron('.txtIndent', container).val(unit);
    $ektron('#chk-alignment', container).attr('checked', false);
    block.updateStyle();
    block.showHideModule($ektron('#chk-alignment', container));

    ok(typeof $ektron('#divResult').attr('style') === 'undefined' || 0 === $ektron('#divResult').attr('style').length, 'align is set correctly.');
});

test('Text alignment are set correctly when checkbox is checked', function () {
    "use strict";
    var align = 'Justify',
        valign = 'Top',
        indent = 40,
        unit = "px",
        container = $ektron('#dialog-container'),
        block = new TextAlignmentBlock('my-div', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#ddTextAlignment', container).val(align);
    $ektron('#ddVAlignment', container).val(valign);
    $ektron('#txtIndent', container).val(indent);
    $ektron('.txtIndent', container).val(unit);
    $ektron('#chk-alignment', container).attr('checked', true);
    block.updateStyle();
    block.showHideModule($ektron('#chk-alignment', container));

    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('text-align: justify;') > -1, 'align is set correctly.');
    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('vertical-align: top;') > -1, 'vertical align is set correctly.');
    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('text-indent: 40px;') > -1, 'first line indent is set correctly.');
});

test('fields are reloaded correctly when target element contains text alignment style.', function () {
    "use strict";
    var targetElement = $ektron('#divResult1'), 
        align = 'Justify',
        valign = 'Top',
        indent = 40,
        unit = "px",
        container = $ektron('#dialog-container1'),
        block = new TextAlignmentBlock('my-div', targetElement.get(0), i18n);
    targetElement.css('text-align', align.toLowerCase()).css('vertical-align', valign.toLowerCase()).css('text-indent', indent + unit);
    myblock = block.create();
    container.append(myblock);

    equal($ektron('#ddTextAlignment', container).val(), align, 'value for text align is loaded correctly.');
    equal($ektron('#ddVAlignment', container).val(), valign, 'value for vertical align is loaded correctly.');
    equal($ektron('#txtIndent', container).val(), indent, 'value for text indent is loaded correctly.');
    equal($ektron('.txtIndent', container).val(), unit, 'value for text indent unit is loaded correctly.');
    equal($ektron('#chk-alignment', container).is(':checked'), true, 'text alignment checkbox is loaded correctly.');
});