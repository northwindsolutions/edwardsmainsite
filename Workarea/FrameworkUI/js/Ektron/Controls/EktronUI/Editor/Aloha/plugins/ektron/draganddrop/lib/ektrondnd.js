﻿; (function ($) {
    // Optional, but considered best practice by some
    "use strict";

    // Name the plugin so it's only in one place
    var pluginName = 'EktronDND';

    // Default options for the plugin as a simple object
    var defaults = {
        folderId: 0,
        path: "",
        size: 10500000,
        browserSupported: true,
        acceptedTypes: {
            'png': true,
            'jpg': true,
            'gif': true
        }
    };

    var tests = {
        filereader: typeof FileReader != 'undefined',
        dnd: 'draggable' in document.createElement('span'),
        formdata: !!window.FormData,
        progress: "upload" in new XMLHttpRequest
    }, options = {},

    cordX = 0, cordY = 0;

    function EktronDND(element, options) {
        this.element = element;
        // Merge the options given by the user with the defaults
        this.options = $.extend({}, defaults, options)
        defaults = this.options;
        // Attach data to the elment
        this.$el = $(element);
        this.$el.data(name, this);

        this.init();
    }

    EktronDND.prototype = {
        init: function () {
            window.DragonDrop = new DRAGON_DROP({
                draggables: $('.fancy'),
                dropzones: $('[contenteditable]'),
                noDrags: $('.fancy img')
            });

            $(this.element).on('mouseover', function (e) {
                cordX = e.clientX;
                cordY = e.clientY;
            });

            if (tests.dnd) {
                this.element.ondragover = function (e) { e.preventDefault(); e.stopPropagation(); return false; };
                this.element.ondragend = function (e) { e.preventDefault(); return false; };
                this.element.ondrop = function (e) {
                    if (!$(this).hasClass('aloha-editable-active')) {
                        defaults.fileUploadError("Editable must be activated before you can drag and drop.");
                        event.preventDefault();
                        event.stopPropagation();
                        return false;
                    }
                    if (e.dataTransfer.files.length > 0) {
                        e.preventDefault();
                        if (e.dataTransfer.files[0].size > defaults.size) {
                            defaults.onError("Maximum image file size is: " + defaults.size / 1000000 + " MB");
                            return false;
                        } else {
                            readfiles(e.dataTransfer.files);
                        }
                    }
                }
            } else {
                defaults.browserSupported = false;
            }
        }
    };

    $.fn[pluginName] = function (options) {
        // Iterate through each DOM element and return it
        return this.each(function () {
            // prevent multiple instantiations
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new EktronDND(this, options));
            }
        });
    };

    // Private functions
    var readfiles = function (files) {
        var that = this;
        for (var i = 0; i < files.length; i++) {
            var fileName = files[i].name;
            var fileType = files[i].type;
            var fileSize = files[i].size;

            var extension = fileName.split('.').pop()

            if (!defaults.acceptedTypes[extension.toLowerCase()]) {
                defaults.onError("The file type is not allowed, file must be an image.")
                return false;
            }
            if (!defaults.browserSupported) {
                defaults.onError("The browser you are currently using does not support drag and drop uploads.")
                return false;
            }

            defaults.onMsg("Uploading...");
            var r = new FileReader();
            r.onload = function (e) {
                var contents = e.target.result;
                $.ajax({
                    url: defaults.path + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/draganddrop/resources/draganddrop.ashx?folderid=" + defaults.folderId,
                    type: "POST",
                    data: "{'file':'" + fileName + "', 'contents':'" + contents + "'}",
                    success: function (response) {
                        if (response != '') {
                            defaults.onError(response);
                        } else {
                            defaults.uploadFinished(files[0].name, cordX, cordY);
                        }
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage); // Optional
                    }
                });
            }
            r.readAsDataURL(files[0]);
        }
    }

})($ektron);

function DRAGON_DROP(o) {
    var DD = this;

    // "o" params:
    DD.$draggables = null;
    DD.$dropzones = null;
    DD.$noDrags = null; // optional

    DD.currDrag = null;
    DD.dropLoad = null;
    DD.engage = function (o) {
        DD.$draggables = $(o.draggables);
        DD.$dropzones = $(o.dropzones);
        DD.$draggables.attr('draggable', 'true');
        DD.$noDrags = (o.noDrags) ? $(o.noDrags) : $();
        DD.$dropzones.attr('dropzone', 'copy');
        DD.bindDraggables();
        DD.bindDropzones();
    };
    DD.bindDraggables = function () {
        DD.$draggables = $(document).find('.fancy'); // reselecting
        DD.$noDrags = $(DD.$noDrags.selector);
        //DD.$noDrags.attr('draggable', 'false');
        $(document).off('dragstart', '.fancy').on('dragstart', '.fancy', function (event) {
            var e = event.originalEvent;
            var elem = $(e.target);
            if (elem.parent().hasClass('ektron-responsive-imageset')) {
                elem = elem.parent();
            }
            $(e.target).removeAttr('dragged');
            var dt = e.dataTransfer,
				content = elem[0].outerHTML;
            //var is_draggable = DD.$draggables.is(e.target);
            //if (is_draggable) {
            dt.effectAllowed = 'copy';
            //dt.setData('text/plain', ' ');
            DD.dropLoad = content;
            DD.currDrag = elem;
            //}
        });
    };
    DD.bindDropzones = function () {
        DD.$dropzones = $(DD.$dropzones.selector); // reselecting
        DD.$dropzones.off('dragleave').on('dragleave', function (event) {
            var e = event.originalEvent;

            var dt = e.dataTransfer;
            var relatedTarget_is_dropzone = DD.$dropzones.is(e.relatedTarget);
            var relatedTarget_within_dropzone = DD.$dropzones.has(e.relatedTarget).length > 0;
            var acceptable = relatedTarget_is_dropzone || relatedTarget_within_dropzone;
            if (!acceptable) {
                dt.dropEffect = 'none';
                dt.effectAllowed = 'null';
            }
        });
        DD.$dropzones.off('drop').on('drop', function (event) {
            var e = event.originalEvent;

            if (!$(this).hasClass('aloha-editable-active')) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            }

            if (!DD.dropLoad) return true;
            var range = null;

            var upImage = $ektron(DD.dropLoad).get(0);
            var range = document.createRange();
            if (document.caretPositionFromPoint) {
                var pos = document.caretPositionFromPoint(e.clientX, e.clientY);
                if (pos != null) {
                    range.setStart(pos.offsetNode, pos.offset);
                } else {
                    range = null;
                }
                range.collapse(false);
                range.insertNode(upImage);
            }
                // Next, the WebKit way
            else if (document.caretRangeFromPoint) {
                range = document.caretRangeFromPoint(e.clientX, e.clientY);
                range.collapse(false);
                range.insertNode(upImage);
            }
            else if (document.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToPoint(e.clientX, e.clientY);
                range.select();
                var selection = window.getSelection();
                if (selection.rangeCount > 0) {
                    range = selection.getRangeAt(0);
                    range.collapse(false);
                    range.insertNode(upImage);
                }
            }

            $(DD.currDrag).remove();

            //$(sel.anchorNode).closest(DD.$dropzones.selector).get(0).focus(); // essential
            //document.execCommand('insertHTML', false, '<param name="dragonDropMarker" />' + DD.dropLoad);
            //sel.removeAllRanges();

            //// verification with dragonDropMarker
            //var $DDM = $('param[name="dragonDropMarker"]');
            //var insertSuccess = $DDM.length > 0;
            //if (insertSuccess) {
            //    $(DD.$draggables.selector).filter('[dragged]').remove();
            //    $DDM.remove();
            //}

            DD.dropLoad = null;
            $ektron('figure.ektron-responsive-imageset').picture();
            e.preventDefault();
        });
    };
    DD.disengage = function () {
        DD.$draggables = $(DD.$draggables.selector); // reselections
        DD.$dropzones = $(DD.$dropzones.selector);
        DD.$noDrags = $(DD.$noDrags.selector);
        DD.$draggables.removeAttr('draggable').removeAttr('dragged').off('dragstart');
        DD.$noDrags.removeAttr('draggable');
        DD.$dropzones.removeAttr('droppable').off('dragenter');
        DD.$dropzones.off('drop');
    };
    if (o) DD.engage(o);
}