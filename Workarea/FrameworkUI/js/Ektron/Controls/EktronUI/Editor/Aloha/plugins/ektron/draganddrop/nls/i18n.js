﻿define({
    root: {
        "draganddrop.msg.FileTypeNotAllowed": "This file is not a supported image format. 'image/jpeg', 'image/png', 'image/gif' can be uploaded via drag and drop.",
        "draganddrop.msg.BrowserNotSupported": "Currently dragging and dropping files is not supported in this browser."
    }
});