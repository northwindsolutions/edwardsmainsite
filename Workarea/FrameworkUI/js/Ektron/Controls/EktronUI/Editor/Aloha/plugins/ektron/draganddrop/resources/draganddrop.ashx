﻿<%@ WebHandler Language="C#" Class="draganddrop" %>

using System;
using System.Web;
using System.IO;
using Ektron.Storage;
using System.Web.Script.Serialization;

public class draganddrop : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        string json = new StreamReader(context.Request.InputStream).ReadToEnd();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;
        UploadedFile data = ser.Deserialize<UploadedFile>(json);
        data.contents = data.contents.Replace("data:image/jpeg;base64,", "");
        data.contents = data.contents.Replace("data:image/png;base64,", "");
        data.contents = data.contents.Replace("data:image/gif;base64,", "");
        string file = string.Empty;
        string contents = string.Empty;
        
        long folderid = 0;
        if (!string.IsNullOrEmpty(context.Request.QueryString["folderid"]))
        {
            long.TryParse(context.Request["folderid"], out folderid);
        }
        Ektron.Cms.API.Folder fldApi = new Ektron.Cms.API.Folder();
        string strPath = fldApi.GetPath(folderid);
        string savedFileName = "";
        Ektron.Cms.Framework.Content.LibraryManager libraryManager = new Ektron.Cms.Framework.Content.LibraryManager();
        Ektron.Cms.CommonApi capi = new Ektron.Cms.CommonApi();

        string docFilePath = HttpContext.Current.Server.MapPath("~/uploadedimages") + strPath + "\\";
	docFilePath = docFilePath.Replace(" ", "_");

        byte[] encodedDataAsBytes = System.Convert.FromBase64String(data.contents);
        File.WriteAllBytes(@docFilePath + data.file, encodedDataAsBytes);        

        string filename = capi.SitePath + "uploadedimages/" + strPath.Replace("\\", "/") + "/" + System.IO.Path.GetFileName(data.file);
        filename = filename.Replace("//", "/");
            
        Ektron.Cms.LibraryData libraryData = new Ektron.Cms.LibraryData()
        {
            Title = data.file,
            ParentId = folderid,
            FileName = filename,
            LanguageId = capi.RequestInformationRef.ContentLanguage,
            TypeId = 1,
            ContentType = 7
        };

        try
        {
            libraryManager.Add(libraryData);
            Ektron.ASM.EkHttpDavHandler.AdaptiveImageProcessor.Instance.ProcessImageForAllConfig(capi.RequestInformationRef, docFilePath + data.file);
            Utilities.ProcessThumbnail(docFilePath, System.IO.Path.GetFileName(filename));
        }
        catch (Exception ex)
        {
            context.Response.Write(ex.Message);
        }
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }
}

public class UploadedFile
{
    public string file { get; set; }
    public string contents { get; set; }
}