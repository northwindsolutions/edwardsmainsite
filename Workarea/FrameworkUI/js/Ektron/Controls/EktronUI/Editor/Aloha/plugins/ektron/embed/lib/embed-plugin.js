/*!
* Aloha Embed Plugin
* -----------------
* This plugin provides an interface to allow the user to access the CMS400 embed, 
* and to add assests from the template to the editable container.
* It presents its user interface in the Toolbar and a modal dialog.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
    'aloha/plugin',
    'ui/ui',
	'ui/button',
	'ui/toolbar',
	'ui/ui-plugin',
	'aloha/console'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
			i18n,
			i18nCore,
            console) {
        // members
        var namespace = "ektron-aloha-embed-",
            modal = $ektron(".ektron-aloha-embed-modal");

        // create and register the Plugin
        return Plugin.create("embed", {
            defaults: {},

            init: function () {
                // executed on plugin initialization
                this.createButton();
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Embed');
                    Ektron.Embed.AcceptHTML = this.acceptHTML;
                }
                if (!($ektron(".ektron-aloha-embed-modal").is(".ui-dialog"))) {
                    modal = this.createModal();
                }
                modal.parents('.ui-dialog').find('.ui-dialog-titlebar button').remove();
                var titlebar = modal.parents('.ui-dialog').find('.ui-dialog-titlebar');
                $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-closethick"></span></button>')
                    .appendTo(titlebar)
                    .click(function () {
                        modal.dialog('close');
                    });
                modal.parents('.ui-dialog').find('.ui-dialog-titlebar button .ui-icon-closethick').show();
            },

            /* Helpers
            ----------------------------------*/
            acceptHTML: function (embedObj) {
                // get the current selection range
                var range = Aloha.Selection.getRangeObject(),
                    selectedEdit = jQuery('.aloha-editable-active');
                var html = jQuery(embedObj.html);

                if (range.isCollapsed()) {
                    GENTICS.Utils.Dom.insertIntoDOM(html, range, selectedEdit);
                }
                else {
                    // remove the contents of the current selection
                    range.deleteContents();
                    // insert our Hello World elements
                    GENTICS.Utils.Dom.insertIntoDOM(html, range, selectedEdit);
                    // deselect the current range object
                    Aloha.getSelection().removeAllRanges();
                }
                $ektron(".ektron-aloha-embed-modal").dialog("close");
            },

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;
                // define the embed button
                this.embedButton = Ui.adopt('embed', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.buttonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.embedButtonClick();
                    }
                });
            },

            createModal: function () {
                $ektron('<div class="ektron-aloha-embed-modal"><iframe class="ektron-aloha-embed-modal-iframe" id="ektron-aloha-embed-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="380" width="450"/></div>').appendTo("body");
                return $ektron(".ektron-aloha-embed-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: false,
                    width: 480,
                    modal: true,
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                    zIndex: 100000001,
                    closeText: '<span class="ui-icon ui-icon-closethick" title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" />',
                    title: Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.buttonTitle
                });
            },

            embedButtonClick: function () {
                modal.dialog("open");
               $ektron("iframe.ektron-aloha-embed-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/embedinsert.aspx");
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);