/*!
* Ektron Aloha General Plugin
* -----------------
* This plugin provides an general helper to allow the user to manipulate the content.
* There is no UI for the plugin.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
	'aloha/plugin',
	'aloha/console'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
            console) {
        // members
        var namespace = "ektron-aloha-";

        // create and register the Plugin
        return Plugin.create("general", {
            defaults: {},

            init: function () {
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Controls.Editor.Aloha');
                    Ektron.Controls.Editor.Aloha.GetContent = this.getContent;
                    Ektron.Controls.Editor.Aloha.UpdateAllContents = this.ensureUpdateAllContents;
                }
            },

            /* Helpers
            ----------------------------------*/
            ensureUpdateAllContents: function () {
                var i, editor, xhtmlContent, hiddenFieldId;
                for (i = 0; i < Aloha.editables.length; i++) {
                    editorId = Aloha.editables[i].getId();
                    hiddenFieldId = editorId.replace(/_EIC_Body_Content/, '_textValue');
                    xhtmlContent = Aloha.editables[i].getContents();

                    if (Ektron.Controls.Editor.Aloha.AllowScripts) {
                        //using jquery to parse it would trigger the script.
                        xhtmlContent = xhtmlContent.replace(/(<textarea.*>)([\s\S]*?)(<\/textarea>)/ig, '<script type="text/javascript">$2</script>');
                    }

                    if (Ektron.Controls.Editor.Aloha.HtmlEncoded) {
                        xhtmlContent = xhtmlContent.replace(/</gi, '&lt;').replace(/>/gi, '&gt;');
                    }
                    xhtmlContent = xhtmlContent.replace(/<br>/gi, '<br/>');

                    $ektron('#' + hiddenFieldId).val(xhtmlContent);
                }
            },

            getContent: function () {
                return Aloha.activeEditable.snapshotContent;
            },

            updateEktronUrl: function (element, attr) {
                var srcUrl = element.getAttribute('data-ektron-url');
                elemObj = $ektron(element);
                if (srcUrl && srcUrl.length > 0) {
                    elemObj.attr(attr, srcUrl);
                }
                elemObj.removeAttr('data-ektron-url');
            },

            updateEktronClick: function (element) {
                element = jQuery(element);
                element.attr('onclick', element.attr('onclick').replace(/^(return false;)/, ''));
            },

            makeClean: function (obj) {
                console.log('general make clean');
                var that = this;
                obj.find('img[data-ektron-url]').each(function () { that.updateEktronUrl(this, 'src'); });
                obj.find('a[data-ektron-url]').each(function () { that.updateEktronUrl(this, 'href'); });
                obj.find('a[onclick]').each(function () { that.updateEktronClick(this); });
                obj.find('img[data-ektron-constrainProportions]').removeAttr('data-ektron-constrainProportions');
                obj.find('*[data-ektron-highlight]').removeAttr('data-ektron-highlight');
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);