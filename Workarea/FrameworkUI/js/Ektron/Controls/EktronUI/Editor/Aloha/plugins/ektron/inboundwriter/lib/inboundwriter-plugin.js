/*!
* Aloha inboundwriter Plugin
* -----------------
* This plugin provides an interface to allow the user to access the CMS400 inboundwriter, 
* and to add assests from the template to the editable container.
* It presents its user interface in the Toolbar and a modal dialog.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
    'aloha/plugin',
    'ui/ui',
	'ui/button',
	'ui/toolbar',
	'ui/ui-plugin',
	'aloha/console',
    'css!inboundwriter/css/inboundwriter-plugin.css'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
			i18n,
			i18nCore,
            console) {
        // members
        var namespace = "ektron-aloha-inboundwriter-",
            modal = $ektron(".ektron-aloha-inboundwriter-modal");

        // create and register the Plugin
        return Plugin.create("inboundwriter", {
            defaults: {},

            init: function () {
                // executed on plugin initialization
                this.createButton();
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.inboundwriter');
                }
                if (!($ektron(".ektron-aloha-inboundwriter-modal").is(".ui-dialog"))) {
                    modal = this.createModal();
                }
            },

            /* Helpers
            ----------------------------------*/

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;
                // define the inboundwriter button
                this.inboundwriterButton = Ui.adopt('inboundwriter', Button, {
                    tooltip: "SEO Dashboard",
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.inboundwriterButtonClick();
                    }
                });
            },

            createModal: function () {
                $ektron('<div class="ektron-aloha-inboundwriter-modal"><iframe class="ektron-aloha-inboundwriter-modal-iframe" id="ektron-aloha-inboundwriter-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="380" width="450"/></div>').appendTo("body");
                return $ektron(".ektron-aloha-inboundwriter-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: false,
                    width: 480,
                    modal: true,
                    zIndex: 100001,
                    title: "SEO Dashboard"
                });
            },

            inboundwriterButtonClick: function () {
                modal.dialog("open");
                Ektron.inboundwriter.docID = 1;
                Ektron.inboundwriter.content = Aloha.getActiveEditable().getContents();
                Ektron.inboundwriter.title = $(document).find("#content_title").val();
               $ektron("iframe.ektron-aloha-inboundwriter-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/inboundwriterinsert.aspx");
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);