/*!
* Aloha Editor
* Author & Copyright (c) 2010 Gentics Software GmbH
* aloha-sales@gentics.com
* Licensed unter the terms of http://www.aloha-editor.com/license.html
*/
define(
['aloha',
    'jquery',
	'aloha/plugin',
	'ui/ui',
	'ui/button',
	'ui/toolbar',
	'ui/ui-plugin',
//'align/align-plugin',
	'aloha/console',
    'css!inspector/css/inspector.css'],
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
			i18n,
			i18nCore,
            console) {

        var
		jQuery = Aloha.jQuery,
		$ = jQuery,
		GENTICS = window.GENTICS,
		Aloha = window.Aloha,
        namespace = "ektron-aloha-inspector-",
        modalLibrary,
        modalImageEdit;

        return Plugin.create('inspector', {
            defaults: {},

            // namespace prefix for this plugin
            // Pseudo-namespace prefix
            ns: 'aloha-inspector',
            uid: 'inspector',
            // namespaced classnames
            nsClasses: {},
            StyleConfigs: {},


            supplant: function (str, obj) {
                return str.replace(/\{([a-z0-9\-\_]+)\}/ig, function (str, p1, offset, s) {
                    var replacement = obj[p1] || str;
                    return (typeof replacement == 'function') ? replacement() : replacement;
                });
            },

            /**
            * Wrapper to all the supplant method on a given string, taking the
            * nsClasses object as the associative array containing the replacement
            * pairs
            *
            * @param {String} str
            * @return {String}
            */
            renderTemplate: function (str) {
                return (typeof str === 'string') ? this.supplant(str, this.nsClasses) : str;
            },

            /**
            * Generates a selector string with this component's namepsace prefixed the
            * each classname
            *
            * Usage:
            *		nsSel('header,', 'main,', 'foooter ul')
            *		will return
            *		".aloha-myplugin-header, .aloha-myplugin-main, .aloha-mypluzgin-footer ul"
            *
            * @return {String}
            */
            nsSel: function () {
                var strBldr = [], prx = this.ns;
                $.each(arguments, function () { strBldr.push('.' + (this == '' ? prx : prx + '-' + this)); });
                return strBldr.join(' ').trim();
            },

            /**
            * Generates s string with this component's namepsace prefixed the each
            * classname
            *
            * Usage:
            *		nsClass('header', 'innerheaderdiv')
            *		will return
            *		"aloha-myplugin-header aloha-myplugin-innerheaderdiv"
            *
            * @return {String}
            */
            nsClass: function () {
                var strBldr = [], prx = this.ns;
                $.each(arguments, function () { strBldr.push(this == '' ? prx : prx + '-' + this); });
                return strBldr.join(' ').trim();
            },

            config: ['true'],

            //activeOn: 'a,span,div,p,q,blockquote,h1,h2,h3,h4,h5,h6,em,i,b',

            /**
            * Initialize the plugin
            */
            init: function () {
                var that = this;
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Inspector.ImageEdit');
                    Ektron.Inspector.ImageEdit.AcceptInsert = this.acceptInsert;
                    Ektron.Inspector.ImageEdit.CloseDialog = this.closeDialog;
                    Ektron.Inspector.ImageEdit.IsThumbnailImage = this.isThumbnailImage;
                }

                this.createButton();

                this.nsClasses = {
                    container: this.nsClass('container'),
                    attribcontainer: this.nsClass('attribcontainer'),
                    newattribute: this.nsClass('newattribute'),
                    item: this.nsClass('item'),
                    element: this.nsClass('element'),
                    iteminput: this.nsClass('iteminput'),
                    SelectStyleDrop: this.nsClass('SelectStyleDrop')
                };
                jQuery.getJSON(Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/inspector/lib/StyleConfig.js", function (styleinfo) {
                    styleModify = JSON.stringify(styleinfo);
                    styleModify = styleModify.replace(/\[FrameworkUIPath\]/g, Ektron.Context.Cms.UIPath);
                    that.StyleConfigs = $ektron.parseJSON(styleModify);
                });
                this.initSidebar(Aloha.Sidebar.right);
                //Aloha.Sidebar.right.open();
                $ektron(document).on('InspectorOut', function () {
                    that.removeHoverEffects();
                    var showHighlightsId = that.nsString("showHighlights")
                    $ektron('#' + showHighlightsId).removeProp('checked');
                });
                Aloha.bind('aloha-editable-deactivated', function (event, myeditable) {
                    that.removeHoverEffects();
                    var showHighlightsId = that.nsString("showHighlights")
                    $ektron('#' + showHighlightsId).removeProp('checked');

                    var attrcontainer = that.content.find('.' + that.nsClass('attribcontainer')),
                    msgcontainer = that.content.find('.' + that.nsClass('container'));

                    msgcontainer.html("<div style='font-family:arial;font-size:12px;color:#000000;'>No active editable detected or no HTML tags present in selected content. Please select an editable area or add HTML tags into your content to inspect elements.</div>");
                    attrcontainer.empty();
                    that.correctHeight();
                });

                Aloha.bind('aloha-editable-activated', function (event, myeditable) {
                    //debugger;
                    that.initClickEffects(myeditable);
                });

                $ektron(document).on("ImageUpdate", function () {
                    that.updateImageProperty();
                });

                if (!($ektron(".ektron-aloha-imageedit-modal").is(".ui-dialog"))) {
                    modalImageEdit = this.createImageEditModal();
                }

                if (!($ektron(".ektron-aloha-library-modal").is(".ui-dialog"))) {
                    if (Ektron.Namespace.Exists("Ektron.Library.Media.CreateModal")) {
                        modalLibrary = Ektron.Library.Media.CreateModal();
                    }
                }
            },

            getSidebarContent: function () {
                return this.renderTemplate(
                    '<div class="{container}">\
						No active editable detected or no HTML tags present in selected content. Please select an editable area or add HTML tags into your content to inspect elements.</div>\
						<div class="{attribcontainer}">\
							\
						</div>\
					</div>'
				);
            },

            findImgMarkup: function (range) {
                var that = this;
                var config = this.config;
                var result, targetObj;

                targetObj = jQuery(range);

                try {
                    if (Aloha.activeEditable) {
                        result = targetObj.find('img')[0];
                        if (result != null) {
                            if (!result.css) {
                                result.css = '';
                            }

                            if (!result.title) {
                                result.title = '';
                            }

                            if (!result.src) {
                                result.src = '';
                            }
                            return result;
                        }
                        else {
                            return null;
                        }
                    }
                } catch (e) {
                    Aloha.Log.debug(e, "Error finding img markup.");
                }
                return null;

            },

            ScaleImage: function (srcwidth, srcheight, targetwidth, targetheight) {

                var result = { width: 0, height: 0, fScaleToTargetWidth: true };

                if ((srcwidth <= 0) || (srcheight <= 0) || (targetwidth <= 0) || (targetheight <= 0)) {
                    return result;
                }

                // scale to the target width
                var scaleX1 = targetwidth;
                var scaleY1 = (srcheight * targetwidth) / srcwidth;

                result.width = Math.floor(scaleX1);
                result.height = Math.floor(scaleY1);

                return result;
            },

            correctHeight: function () {
                this.sidebar.correctHeight();
            },

            updateImageProperty: function () {
                var updatedImg = $ektron("img[data-ektron-image-tagclick-clicked]");
                $ektron("input.aloha-inspector-iteminput").each(function () {
                    var eInput = $ektron(this);
                    var attrName = eInput.attr("data-attrname");
                    switch (attrName) {
                        case "width":
                            eInput.val(updatedImg.width());
                            break;
                        case "height":
                            eInput.val(updatedImg.height());
                            break;
                        default:
                            eInput.val(updatedImg.attr(eInput.attr("data-attrname")));
                            break;
                    }
                });
            },

            initClickEffects: function (myeditable) {
                var that = this;
                if (Aloha.getActiveEditable() != null) {

                    var element = $ektron(Aloha.getActiveEditable().obj[0]);

                    element.on('click.' + that.nsString("bindings"), function (event) {
                        that.TargetClicked(event.target, false);
                    });
                }
            },

            TargetClicked: function (event, secondary) {
                var that = this;
                var target = $(event);
                var dom = Aloha.getActiveEditable().obj[0];
                var element = $ektron(Aloha.getActiveEditable().obj[0]);

                var jdom = $ektron(dom);
                if (target.hasClass("aloha-table-cell-editable")) {
                    target = target.parent();
                }
                else if ('IMG' == target.get(0).tagName) {
                    jQuery('img[data-ektron-image-tagclick-clicked]', dom).removeAttr('data-ektron-image-tagclick-clicked');
                    target.attr('data-ektron-image-tagclick-clicked', 'true');
                }

                var $container = $('body').find(that.nsSel('attribcontainer')),
                msgcontainer = that.content.find('.' + that.nsClass('container')),
                item,
                nodeName;

                if (target[0].tagName != "TABLE" && target[0].tagName != "UL" && target[0].tagName && "OL") {
                    $container.html('');
                }
                if (target[0] != null && target[0].id != element[0].id) {
                    msgcontainer.empty();
                    if (secondary) {
                        item = jQuery(that.renderTemplate('<div style="width:100%;height:10px;background:#444444;" /><h2>Element: <strong>' + target[0].tagName + '</strong></h2>'));
                    } else {
                        item = jQuery(that.renderTemplate('<h2>Element: <strong>' + target[0].tagName + '</strong></h2>'));
                    }
                    $container.append(item);

                    if ("IMG" == target[0].tagName) {
                        item = jQuery(that.renderTemplate('<div class="{item}"><label for="imageeditbtn">' + Ektron.Controls.Editor.Aloha.Tabs.ResourceText.imageEdit + '</label><button id="imageeditbtn" class="x-btn-btn ektron-aloha-button ektron-aloha-imageedit-button"></button</div>'));
                        $container.append(item);
                        item = jQuery(that.renderTemplate('<div class="{item}"><label for="{iteminput}' + 'height' + '">' + 'Height' + '</label><input id="{iteminput}' + 'height' + '" class="{iteminput}" data-attrname="' + 'height' + '" type="text" value="' + target[0].height + '"/></div>'));
                        $container.append(item);
                        item = jQuery(that.renderTemplate('<div class="{item}"><label for="{iteminput}' + 'width' + '">' + 'Width' + '</label><input id="{iteminput}' + 'width' + '" class="{iteminput}" data-attrname="' + 'width' + '" type="text" value="' + target[0].width + '"/></div>'));
                        $container.append(item);
                    }

                    for (i = 0; i < target[0].attributes.length; i++) {
                        attr = target[0].attributes[i];
                        nodeName = attr.nodeName;
                        item = jQuery(that.renderTemplate('<div class="{item}"><label for="{iteminput}' + nodeName + '">' + nodeName + '</label><input id="{iteminput}' + nodeName + '" class="{iteminput}" data-attrname="' + nodeName + '" type="text" value="' + attr.nodeValue + '"/></div>'));
                        if (-1 == nodeName.indexOf("data-ektron") && -1 == nodeName.indexOf("_moz_") && -1 == nodeName.indexOf("height") && -1 == nodeName.indexOf("width")) {
                            if (nodeName.indexOf("src") != -1 && Aloha.settings.plugins.load.indexOf(",ektron/library,")) {
                                item = jQuery(that.renderTemplate('<div class="{item}"><label for="{iteminput}' + nodeName + '">' + nodeName + '</label><input id="{iteminput}' + nodeName + '" disabled="disabled" class="{iteminput}" data-attrname="' + nodeName + '" type="text" value="' + attr.nodeValue + '"/><button id="librarybtn" class="x-btn-btn ektron-aloha-button ektron-aloha-library-button"></button</div></div>'));
                            }
                            $container.append(item);
                        }
                        that.correctHeight();
                    }
                    that.correctHeight();

                    if (that.StyleConfigs[target[0].tagName] != null) {
                        //Reading style data from config, found matched element defined
                        var optionStr = '';
                        jQuery.each(eval(that.StyleConfigs[target[0].tagName].styles), function (key, value) {
                            var selected = target;
                            if ((selected.hasClass(value.value)) || (selected.attr("style") == value.value)) {
                                optionStr += '<div class="' + that.nsClass('SelectStyleOption') + ' styleSelected" data-StyleRef="' + key + '" data-StyleType="' + that.StyleConfigs[target[0].tagName].styles[key].type + '" style="padding:5px;border:1px solid #F2EF38;cursor: pointer;"><span style="' + that.StyleConfigs[target[0].tagName].styles[key].workareaBox + ';font-size:1.5em;">AaBbCc</span><span> - ' + key + ' (' + that.StyleConfigs[target[0].tagName].styles[key].type + ')</span></div>';
                            } else {
                                optionStr += '<div class="' + that.nsClass('SelectStyleOption') + '" data-StyleRef="' + key + '" data-StyleType="' + that.StyleConfigs[target[0].tagName].styles[key].type + '" style="padding:5px;border:1px solid #F2EF38;cursor: pointer;"><span style="' + that.StyleConfigs[target[0].tagName].styles[key].workareaBox + ';font-size:1.5em;">AaBbCc</span><span> - ' + key + ' (' + that.StyleConfigs[target[0].tagName].styles[key].type + ')</span></div>';
                            }
                        });


                        var item = jQuery(that.renderTemplate('<div class="' + that.nsString("StyleInputDiv") + '"><label for="{SelectStyleDrop}">Style Selector</label><div id="{SelectStyleDrop}" class="{SelectStyleDrop}" style="background:white;width:100%;overflow:auto;">' + optionStr + '</div></div>'));
                        $container.append(item);
                        that.correctHeight();
                    }

                    $container.find(that.nsSel('SelectStyleOption')).click(function () {
                        var clickedelem = jQuery(this);

                        var key = clickedelem.attr("data-StyleRef");
                        var type = clickedelem.attr("data-StyleType");

                        if (type == "style") {
                            jQuery("div[data-StyleType='style']").not(this).removeClass("styleSelected");
                        }

                        clickedelem.toggleClass("styleSelected");
                        var action;
                        if (clickedelem.hasClass("styleSelected"))
                            action = "add";
                        else
                            action = "remove";

                        if (that.StyleConfigs[target[0].tagName].styles[key].type == "style") {
                            if (action == "add")
                                jQuery(target[0]).attr('style', that.StyleConfigs[target[0].tagName].styles[key].value);
                            else if (action == "remove")
                                jQuery(target[0]).removeAttr('style');
                        } else if (that.StyleConfigs[el.tagName].styles[key].type == "class") {
                            if (action == "add")
                                jQuery(target[0]).addClass(that.StyleConfigs[target[0].tagName].styles[key].value);
                            else if (action == "remove")
                                jQuery(target[0]).removeClass(that.StyleConfigs[target[0].tagName].styles[key].value);
                        }
                    });

                    $container.find('#aspect_check').change(function () {
                        jQuery(this).prop('checked') ? $container.find('#aloha-inspector-iteminputheight').attr("disabled", "disabled") : $container.find('#aloha-inspector-iteminputheight').removeAttr("disabled");
                    });

                    $container.find("button.ektron-aloha-imageedit-button").click(function () {
                        modalImageEdit.dialog("open");
                        $ektron("iframe.ektron-aloha-imageedit-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/ImageTool/ImageEdit.aspx?i=" + encodeURIComponent($container.find("#aloha-inspector-iteminputsrc").val()));
                    });

                    $container.find("button.ektron-aloha-library-button").click(function () {
                        if (Ektron.Namespace.Exists("Ektron.Library.Media.LibraryButtonClick")) {
                            Ektron.Library.Media.LibraryButtonClick();
                        }
                    });

                    $container.find(that.nsSel('iteminput')).bind('keyup', function () {
                        var dom = Aloha.getActiveEditable().obj[0];
                        var jdom = $ektron(dom);

                        var imageclicked = jdom.find("img[data-ektron-image-tagclick-clicked='true']");

                        if (imageclicked[0] != null) {
                            var aspectratio;

                            aspectratio = $container.find('#aspect_check');

                            if (aspectratio.prop("checked")) {
                                var imageSrc, originalImg, oldHeight, oldWidth, newWidth, newHeight, aspectRatio, heightField, widthField;

                                imageSrc = $container.find('#aloha-inspector-iteminputsrc');
                                oldImage = new Image();
                                oldImage.src = imageSrc.val();
                                oldwidth = oldImage.width;
                                oldheight = oldImage.height;
                                heightField = $container.find('#aloha-inspector-iteminputheight');
                                widthField = $container.find('#aloha-inspector-iteminputwidth');
                                newwidth = parseInt(widthField.val(), 10);
                                newheight = parseInt(heightField.val(), 10);

                                // if width field is either  not a number, zero, or a negative value don't compute new values
                                if (!(isNaN(newwidth) || newwidth <= 0)) {

                                    scaledAspectRatio = that.ScaleImage(oldwidth, oldheight, newwidth, newheight, true);

                                    imageclicked[0].width = scaledAspectRatio.width <= 1 ? 1 : scaledAspectRatio.width;
                                    imageclicked[0].height = scaledAspectRatio.height <= 1 ? 1 : scaledAspectRatio.height;
                                    widthField.val(imageclicked[0].width + "");
                                    heightField.val(imageclicked[0].height + "");
                                }
                            }
                        }

                        var value = jQuery(this).val();
                        var name = jQuery(this).attr('data-attrname');

                        if (typeof value !== 'undefined') {
                            jQuery(target).attr(name, value);
                        }

                    });
                    var elemheader = that.content.find('#' + that.nsClass('element'));
                    elemheader.html("Element: " + target[0].tagName);
                    that.correctHeight();
                    if (target[0].tagName == "TD") {
                        this.TargetClicked(target.parent().parent().parent(), true);
                    } else if (target[0].tagName == "LI") {
                        this.TargetClicked(target.parent(), true);
                    }
                } else {
                    var elemheader = that.content.find('#' + that.nsClass('element'));
                    var attrcontainer = that.content.find('#' + that.nsClass('attribcontainer'));
                    elemheader.html("<div style='font-family:arial;font-size:12px;color:#000000;'>No active editable detected or no HTML tags present in selected content. Please select an editable area or add HTML tags into your content to inspect elements.</div>");
                    attrcontainer.empty();
                    that.correctHeight();
                }
                //stop
                that.correctHeight();
            },

            initHoverEffects: function () {
                var that = this;
                if (Aloha.getActiveEditable() != null) {
                    var el = Aloha.getActiveEditable().obj[0];
                    var element = $ektron(el);
                    element.on('mouseover.' + that.nsString("bindings"), function (event) {
                        var $tgt = jQuery(event.target);

                        if ($tgt[0].id != element[0].id) {
                            if ($tgt.hasClass('aloha-table-cell-editable')) {
                                $tgt = $tgt.parent();
                                $tgt.attr("data-ektron-highlight", "true");
                            } else {
                                if ($tgt[0].tagName == "IMG") {
                                    $tgt.attr("data-ektron-image-highlight", "true");
                                }
                                else {
                                    $tgt.attr("data-ektron-highlight", "true");
                                }
                            }
                        }
                    });
                    element.on('mouseout.' + that.nsString("bindings"), function (event) {
                        var $tgt = jQuery(event.target);
                        if ($tgt.hasClass('aloha-table-cell-editable')) {
                            $tgt = $tgt.parent();
                        }
                        $tgt.removeAttr("data-ektron-highlight");
                        $tgt.removeAttr("data-ektron-image-highlight");
                    });
                    element.on('click.' + that.nsString("bindings"), function (event) {
                        var $tgt = jQuery(event.target);

                        jQuery(el).removeAttr("data-ektron-highlight-clicked");
                        jQuery("*", el).removeAttr("data-ektron-highlight-clicked").removeAttr("data-ektron-image-highlight-clicked").removeAttr("data-ektron-td-highlight-clicked");
                        if ($tgt[0].id != element[0].id) {
                            if ($tgt.hasClass('aloha-table-cell-editable')) {
                                $tgt = $tgt.parent();
                                $tgt.removeAttr("data-ektron-highlight");
                                $tgt.attr("data-ektron-highlight-clicked", "true");
                            } else {
                                if ($tgt[0].tagName == "IMG") {
                                    $tgt.removeAttr("data-ektron-highlight").removeAttr("data-ektron-image-highlight");
                                    $tgt.attr("data-ektron-image-highlight-clicked", "true");
                                }
                                else {
                                    $tgt.removeAttr("data-ektron-highlight");
                                    $tgt.attr("data-ektron-highlight-clicked", "true");
                                }
                            }
                        }
                    });
                }
            },

            removeHoverEffects: function (elementObject) {
                var that = this;
                var el;

                if (Aloha.getActiveEditable() != null) {

                    if (elementObject == null)
                        el = Aloha.getActiveEditable().obj[0];
                    else
                        el = elementObject;

                    var element = $ektron(el);
                    $ektron(element).removeAttr("data-ektron-highlight-clicked").removeAttr("data-ektron-image-highlight-clicked").removeAttr("data-ektron-td-highlight-clicked").removeAttr("data-ektron-tagclick-clicked").removeAttr("data-ektron-image-tagclick-clicked").removeAttr("data-ektron-td-tagclick-clicked");
                    $ektron("*", element).removeAttr("data-ektron-highlight-clicked").removeAttr("data-ektron-image-highlight-clicked").removeAttr("data-ektron-td-highlight-clicked").removeAttr("data-ektron-tagclick-clicked").removeAttr("data-ektron-image-tagclick-clicked").removeAttr("data-ektron-td-tagclick-clicked");

                    $ektron(element).off('mouseover.' + that.nsString("bindings")).off('mouseout.' + that.nsString("bindings")).off('click.' + that.nsString("bindings"));
                    $ektron("*", element).off('mouseover.' + that.nsString("bindings")).off('mouseout.' + that.nsString("bindings")).off('click.' + that.nsString("bindings"));
                }
            },

            // start Image Edit Module
            createImageEditModal: function () {
                $ektron('<div class="ektron-aloha-imageedit-modal"><iframe class="ektron-aloha-imageedit-modal-iframe" id="ektron-aloha-imageedit-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>').appendTo("body");
                return $ektron(".ektron-aloha-imageedit-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: false,
                    width: 820,
                    modal: true,
                    zIndex: 100000001,
                    title: Ektron.Controls.Editor.Aloha.Tabs.ResourceText.modalTitle,
                    close: function (event, ui) {
                        if (Aloha.Sidebar.right.isOpen || Aloha.Sidebar.left.isOpen) {
                            $ektron(document).trigger("ImageUpdate");
                        }
                    },
                    beforeClose: function (event, ui) {
                        if (typeof $ektron("#ektron-aloha-imageedit-modal-iframe")[0].contentWindow.handleWindowClose != "undefined") {
                            $ektron("#ektron-aloha-imageedit-modal-iframe")[0].contentWindow.handleWindowClose();
                        }
                    }
                });
            },

            acceptInsert: function (imageObj) {
                var image = {
                    "ImageURL": "",
                    "Width": "0",
                    "Height": "0"
                }
                jQuery.extend(image, imageObj);

                var selectedEdit = jQuery('.aloha-editable-active'),
                    dom = Aloha.getActiveEditable().obj[0],
                    jdom = $ektron(dom),
                    imageSelected = jdom.find("img[data-ektron-image-tagclick-clicked='true']");
                if (imageSelected[0] != null) {
                    var oImgParent = imageSelected.parent().get(0);
                    if (Ektron.Inspector.ImageEdit.IsThumbnailImage(oImgParent)) {
                        var oldthumbnail = imageSelected.attr("data-ektron-url"), newthumbnail,
                            oldfilename = oldthumbnail.replace("/thumb_", "/"), newfilename = image.ImageURL,
                            modifynode;
                        // figure out what to replace at the parent
                        // we'll just replace the "oldfilename." (no extension) with "newfilename."
                        if (oldfilename.lastIndexOf(".") > 0) {
                            oldfilename = oldfilename.substring(0, oldfilename.lastIndexOf(".") + 1);
                        }
                        if (newfilename.lastIndexOf(".") > 0) {
                            newfilename = newfilename.substring(0, newfilename.lastIndexOf(".") + 1);
                        }
                        if (oldthumbnail.lastIndexOf(".") > 0) {
                            oldthumbnail = oldthumbnail.substring(0, oldthumbnail.lastIndexOf(".") + 1);
                        }
                        newthumbnail = newfilename.substring(0, newfilename.lastIndexOf("/") + 1) + "thumb_" + newfilename.substring(newfilename.lastIndexOf("/") + 1);

                        // have to modify grandparent element's innerHTML because we can't modify outerHTML
                        modifynode = imageSelected.parent().parent();
                        modifynode.html(modifynode.html().replace(new RegExp(oldthumbnail, "g"), newthumbnail).replace(new RegExp(oldfilename, "g"), newfilename));
                    }
                    else {
                        // change the src link to point at the updated image
                        imageSelected.attr("src", image.ImageURL);
                        imageSelected.attr("data-ektron-url", image.ImageURL);
                        imageSelected.removeAttr("width");
                        imageSelected.removeAttr("height");
                        imageSelected.width("");
                        imageSelected.height("");
                    }
                }

                modalImageEdit.dialog("close");
            },

            isThumbnailImage: function (oImgParent) {
                var thumbstring = "try{window.open('",
                    isThumbnail = false;
                if (oImgParent && (oImgParent.tagName == "A")) {
                    var onclickattr = oImgParent.getAttribute("onclick");
                    if (onclickattr != null) {
                        var clickinfo = onclickattr.toString();
                        var startindex = clickinfo.indexOf(thumbstring);
                        if (clickinfo.substring(startindex, startindex + thumbstring.length) == thumbstring) {
                            var imgstring = clickinfo.substring(startindex + thumbstring.length);
                            if (imgstring.indexOf("'")) {
                                isThumbnail = true;
                            }
                        }
                    }
                }
                return isThumbnail;
            },

            closeDialog: function () {
                modalImageEdit.dialog("close");
            },
            // end Image Edit Module

            inspectorButtonClick: function () {
                Aloha.Sidebar.right.open();
            },

            createButton: function () {
                var that = this,
                    tabInsert = i18nCore.adoptInto('floatingmenu.tab.insert', 'inspector');

                // define the translate button
                this.translateButton = Ui.adopt('inspector', Button, {
                    tooltip: "Inspector",
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.inspectorButtonClick();
                    }
                });
            },

            nsString: function (string) {
                return namespace + string;
            },

            initSidebar: function (sidebar) {
                var pl = this;
                pl.sidebar = sidebar;
                var sidebarcontent = this.getSidebarContent();

                var panelTitle = '',
                clearfixClass = ' ' + this.nsString("clearfix"),
                contentPanelId = this.nsString("contentPanel"),
                checkboxClass = this.nsString("checkbox"),
                headerWrapperClass = this.nsString("header") + clearfixClass,
                markerClass = this.nsString("marker"),
                optionsWrapperClass = this.nsString("optionsWrapper"),
                titleClass = this.nsString("title"),
                showHighlightsId = this.nsString("showHighlights"),
                widenCheckboxClass = checkboxClass + ' ' + showHighlightsId,
                widenCheckboxLabelClass = this.nsString("widenCheckboxLabel"),
                HighlightOn = false;

                panelTitle += '<div class="' + headerWrapperClass + '">';
                panelTitle += '  <span class="' + titleClass + '">Inspector</span>\n';
                panelTitle += '  <div class=' + optionsWrapperClass + '>\n';
                panelTitle += '    <input type="checkbox" id="' + showHighlightsId + '" class="' + widenCheckboxClass + '" />\n';
                panelTitle += '    <label for="' + showHighlightsId + '" class="' + widenCheckboxLabelClass + '">Highlight Tags On Hover</label>\n';
                panelTitle += '  </div>\n';
                panelTitle += '</div>\n';

                sidebar.addPanel({
                    id: pl.nsClass('sidebar-panel'),
                    title: panelTitle,
                    content: '',
                    expanded: true,
                    onInit: function () {
                        var that = this;
                        pl.content = this.setContent(sidebarcontent).content;

                        this.title.find('#' + showHighlightsId).click(function (ev) {
                            ev.stopPropagation();
                        });

                        this.title.find('.' + widenCheckboxLabelClass).click(function (ev) {
                            ev.stopPropagation();
                        });

                        this.title.find('#' + showHighlightsId).change(function () {
                            jQuery(this).prop('checked') ? pl.initHoverEffects() : pl.removeHoverEffects();
                            sidebar.open(0);
                        });
                    },
                    onActivate: function (effective) {
                        if (effective.length) {
                            pl.TargetClicked(effective, false);
                        }
                    }
                });
                Aloha.Sidebar.right.show();
            }
        });
    });