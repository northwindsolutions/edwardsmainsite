/*!
* Aloha Library Plugin
* -----------------
* This plugin provides an interface to allow the user to access the CMS400 Library, 
* and to add assests from the library to the editable container.
* It presents its user interface in the Toolbar, and a modal dialog.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
    'aloha/plugin',
    'ui/ui',
	'ui/button',
    'vendor/ektron/ektron.aloha.dialog',
	'ui/toolbar',
	'ui/ui-plugin',
	'aloha/console',
    'css!./css/library-plugin'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
            Dialog,
			i18n,
			i18nCore,
            console) {
        // members
        var namespace = "ektron-aloha-library-",
            libraryDialog;

        // create and register the Plugin
        return Plugin.create("library", {
            defaults: {},

            init: function () {
                var that = this;
                // executed on plugin initialization
                that.libraryDialog = Dialog();
                that.createButton();
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Library.Media');
                    Ektron.Library.Media.AcceptInsert = this.acceptInsert;
                    Ektron.Library.Media.AcceptInsertThumb = this.acceptInsertThumb;
                    Ektron.Library.Media.AcceptLink = this.acceptLink;
                    Ektron.Library.Media.LibraryButtonClick = this.openDialog;
                    Ektron.Library.Media.CloseDialog = this.closeDialog;
                    Ektron.Library.Media.getBreakpoints = this.getBreakpoints;
                    Ektron.Library.Dialog = that.libraryDialog;
                    Ektron.Library.Media.enableDeviceDetection = false;
                }
                //if (!($ektron(".ektron-aloha-library-modal").is(".ui-dialog"))) {
                //    modal = this.createModal();
                //}
            },

            /* Helpers
            ----------------------------------*/
            acceptInsert: function (imageObj) {
                var image = {
                    "id": "",
                    "title": "",
                    "altText": "",
                    "path": "",
                    "width": "0",
                    "height": "0",
                    "figureTag": ""
                }
                jQuery.extend(image, imageObj);

                // get the current selection range
                var range = Aloha.Selection.getRangeObject(), output, nextWidth = '', jFigureSet,
                    selectedEdit = jQuery('.aloha-editable-active'),
                    selectedImg, breakpointArray = Ektron.Library.Media.getBreakpoints();
                if ('undefined' === typeof range.isCollapsed) {
                    // selection range is lost.
                    if ('string' === typeof Ektron.AdvancedInspector.ImageProperties.currentElement.url) {
                        selectedImg = Ektron.AdvancedInspector.ImageProperties.currentElement.imageObj;
                    }
                }
                else if (0 == selectedEdit.length) {
                    selectedEdit = jQuery(range.commonAncestorContainer);
                }
                if (breakpointArray != null && breakpointArray.length > 0 && Ektron.Library.Media.enableDeviceDetection == true) {
                    if (typeof imageObj.figureTag !== 'undefined' && imageObj.figureTag.length > 0) {
                        output = $ektron(unescape(imageObj.figureTag));
                    }
                    else {
                        output = '<figure contenteditable="false" class="ektron-responsive-imageset fancy" title="' + imageObj.title + '" data-ektron-image-src="' + imageObj.path + '" ';
                        //each breakpoint 
                        for (i = 0; i < breakpointArray.length; i += 1) {
                            var path = imageObj.path;
                            if (i + 1 == breakpointArray.length) {
                                output += 'data-media' + nextWidth + '="' + path + '" ';
                            } else {
                                output += 'data-media' + nextWidth + '="' + path + '?targetTypeId=' + breakpointArray[i].FileLabel + '" ';
                            }
                            nextWidth = breakpointArray[i].Width + 1;
                        }
                        output += '>';
                        output += '<img style="max-width:100%" src="' + imageObj.path + '" alt="' + imageObj.title + '"/>';
                        output += '</figure>';
                    }
                    output = $ektron(output);
                }
                else {
                    output = jQuery('<img src="' + image.path + '" alt="' + image.altText + '" title="' + image.title + '" data-ektron-url="' + image.path + '" class="" />');
                }

                if ('undefined' === typeof range.isCollapsed && selectedImg != null) {
                    selectedImg = jQuery(selectedImg);
                    selectedImg.after(output);
                    selectedImg.remove();
                }
                else if (range.isCollapsed()) {
                    GENTICS.Utils.Dom.insertIntoDOM(output, range, selectedEdit);
                }
                else {
                    // remove the contents of the current selection
                    range.deleteContents();
                    // insert our Hello World elements
                    GENTICS.Utils.Dom.insertIntoDOM(output, range, selectedEdit);
                    // deselect the current range object
                    Aloha.getSelection().removeAllRanges();
                }

                jFigureSet = $ektron('figure.ektron-responsive-imageset');
                if (jFigureSet.length > 0) {
                    jFigureSet.picture();
                    jFigureSet.find('img').css('max-width', '100%');
                    if (Ektron.Namespace.Exists("Ektron.AdvancedInspector.ImageSetBlock.ImageSetInit")) {
                        //Ektron.AdvancedInspector.ImageSetBlock.ImageSetInit(jFigureSet);
                    }
                }

                $ektron(document).trigger("ImageUpdate", output);
                Ektron.Library.Dialog.dialog("close");
                window.DragonDrop.bindDraggables();
            },

            acceptInsertThumb: function (htmlObj) {
                var h = {
                    html: "",
                    title: ""
                }
                jQuery.extend(h, htmlObj);

                // get the current selection range
                var range = Aloha.Selection.getRangeObject(),
                    selectedEdit = jQuery('.aloha-editable-active'),
                    html = jQuery(h.html),
                    img = html.children("img");
                html.attr("onclick", "return false;" + html.attr("onclick"));
                img.attr("data-ektron-url", img.attr("src"));

                if (range.isCollapsed()) {
                    GENTICS.Utils.Dom.insertIntoDOM(html, range, selectedEdit);
                }
                else {
                    // remove the contents of the current selection
                    range.deleteContents();
                    // insert our Hello World elements
                    GENTICS.Utils.Dom.insertIntoDOM(html, range, selectedEdit);
                    // deselect the current range object
                    Aloha.getSelection().removeAllRanges();
                }
                Ektron.Library.Dialog.dialog("close");
            },

            acceptLink: function (htmlObj) {
                var that = this,
                    foundMarkup,
                    h = {
                        href: "",
                        title: "",
                        type: ""
                    };

                jQuery.extend(h, htmlObj);

                // get the current selection range
                var range = Aloha.Selection.getRangeObject(),
                    selectedEdit = jQuery('.aloha-editable-active');

                if (range.isCollapsed()) {
                    linkText = h.title;
                    newLink = jQuery('<a href="' + h.href + '" title="' + linkText + '" data-ektron-url="' + h.href + '" class="" >' + linkText + '</a>');
                    GENTICS.Utils.Dom.insertIntoDOM(newLink, range, selectedEdit);
                    range.startContainer = range.endContainer = newLink.contents().get(0);
                    range.startOffset = 0;
                    range.endOffset = linkText.length;
                }
                else {
                    newLink = jQuery('<a href="' + h.href + '" title="' + h.title + '" data-ektron-url="' + h.href + '" class="" ></a>');
                    GENTICS.Utils.Dom.addMarkup(range, newLink, false);
                    range.select();
                }
                Ektron.Library.Dialog.dialog("close");
            },

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;

                // define the library button
                this.libraryButton = Ui.adopt('library', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.Library.ResourceText.buttonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.openDialog();
                    }
                });
            },

            getBreakpoints: function () {
                var that = this, breakpointArray = null;
                $ektron.ajax({
                    type: "POST",
                    cache: false,
                    async: false,
                    url: Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/resources/advancedinspector.ashx",
                    data: { "action": "getdevicebreakpointdatalist" },
                    success: function (msg) {
                        breakpointArray = $ektron.parseJSON(msg);
                    }
                });
                Ektron.Library.Media.enableDeviceDetection = breakpointArray.enableDeviceDetection;

                return breakpointArray.BreakpointDataList;
            },

            openDialog: function () {
                var that = this;
                dialogOptions = $.extend({}, Ektron.Library.Dialog.prop('_ektronAlohaDialogDefaultOptions'), {
                    autoOpen: false,
                    width: 820,
                    modal: true,
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                    closeText: '<span class="ui-icon ui-icon-closethick" title="' + Ektron.Controls.Editor.Aloha.Plugins.General.CloseLabel + '" />',
                    zIndex: 100000001,
                    title: Ektron.Controls.Editor.Aloha.Plugins.Library.ResourceText.modalTitle,
                    close: function (event, ui) {
                        if (Aloha.Sidebar.right.isOpen || Aloha.Sidebar.left.isOpen) {
                            $ektron(document).trigger("ImageUpdate");
                        }
                        Ektron.Library.Media.CloseDialog();
                    }
                });
                Ektron.Library.Dialog.html('<div class="ektron-aloha-library-modal"><iframe id="ektron-aloha-library-modal-iframe" class="ektron-aloha-library-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
                Ektron.Library.Dialog.dialog('option', dialogOptions);

                Ektron.Library.Dialog.dialog('open');

                var titlebar = Ektron.Library.Dialog.parents('.ui-dialog').find('.ui-dialog-titlebar');
                $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-closethick"></span></button>')
                    .appendTo(titlebar)
                    .click(function () {
                        Ektron.Library.Dialog.dialog('close');
                    });

                var folderId = "0";
                if (Aloha.activeEditable.obj.data("ektronEditorData") != null) {
                    folderId = Aloha.activeEditable.obj.data("ektronEditorData").folderId.toString();
                }
                $ektron("iframe.ektron-aloha-library-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/mediamanager.aspx?actiontype=library&scope=all&EditorName=Aloha&autonav=" + folderId);
            },

            closeDialog: function () {
                Ektron.Library.Dialog.empty();
                // reset dialog back to default options
                if (typeof Ektron.Library.Dialog.prop('_ektronAlohaDialogDefaultOptions') != 'undefined') {
                    Ektron.Library.Dialog.dialog('option', Ektron.Library.Dialog.prop('_ektronAlohaDialogDefaultOptions'));
                }
                else {
                    Ektron.Library.Dialog.dialog();
                }
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);