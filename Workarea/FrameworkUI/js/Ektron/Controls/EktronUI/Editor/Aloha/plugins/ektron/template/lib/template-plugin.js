/*!
* Aloha Template Plugin
* -----------------
* This plugin provides an interface to allow the user to access the CMS400 template, 
* and to add assests from the template to the editable container.
* It presents its user interface in the Toolbar and a modal dialog.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
    'aloha/plugin',
    'ui/ui',
	'ui/button',
    'vendor/ektron/ektron.aloha.dialog',
	'ui/toolbar',
	'ui/ui-plugin',
	'aloha/console'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
            Dialog,
			i18n,
			i18nCore,
            console) {
        // members
        var namespace = "ektron-aloha-template-",
            templateDialog;

        // create and register the Plugin
        return Plugin.create("template", {
            defaults: {},

            init: function () {
                var that = this;
                //initialize dialog
                that.templateDialog = Dialog();
                // create the Aloha button to launch the source viewer
                that.createButton();
                // executed on plugin initialization
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Template');
                    Ektron.Template.AcceptHTML = this.acceptHTML;
                    Ektron.Template.Dialog = that.templateDialog;
                }
            },

            /* Helpers
            ----------------------------------*/
            acceptHTML: function (templateObj) {
                var that = this;
                // get the current selection range
                var range = Aloha.Selection.getRangeObject(),
                    selectedEdit = jQuery('.aloha-editable-active');
                var html = jQuery(templateObj.html);

                if (range.isCollapsed()) {
                    GENTICS.Utils.Dom.insertIntoDOM(html, range, selectedEdit);
                }
                else {
                    // remove the contents of the current selection
                    range.deleteContents();
                    // insert our Hello World elements
                    GENTICS.Utils.Dom.insertIntoDOM(html, range, selectedEdit);
                    // deselect the current range object
                    Aloha.getSelection().removeAllRanges();
                }
                //Close dialog after insert 
                Ektron.Template.Dialog.dialog("close");
            },

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;
                // define the template button
                this.templateButton = Ui.adopt('template', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.Template.ResourceText.buttonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.openDialog();
                    }
                });
            },

            openDialog: function () {
                var that = this;
                dialogOptions = $.extend({}, this.templateDialog.prop('_ektronAlohaDialogDefaultOptions'), {
                    title: Ektron.Controls.Editor.Aloha.Plugins.Template.ResourceText.buttonTitle,
                    width: 820,
                    height: 500,
                    modal: true,
                    zIndex: 100000001,
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                    closeText: '<span class="ui-icon ui-icon-closethick" title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" />',
                    close: function (event, ui) {
                        that.closeDialog();
                    }
                });

                that.templateDialog.html('<div class="ektron-aloha-template-modal"><iframe class="ektron-aloha-template-modal-iframe" id="ektron-aloha-template-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>');
                that.templateDialog.dialog('option', dialogOptions);

                that.templateDialog.dialog('open');

                var titlebar = that.templateDialog.parents('.ui-dialog').find('.ui-dialog-titlebar');
                $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-closethick"></span></button>')
                    .appendTo(titlebar)
                    .click(function () {
                        that.templateDialog.dialog('close');
                    });
                that.templateDialog.parents('.ui-dialog').find('.ui-dialog-titlebar button').css('color', 'white');
                $ektron("iframe.ektron-aloha-template-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/templateinsert.aspx?folderid=" + Aloha.settings.templatePlugin.FolderID);
            },

            closeDialog: function () {
                var that = this;
                that.templateDialog.empty();
                // reset dialog back to default options
                if (typeof that.templateDialog.prop('_ektronAlohaDialogDefaultOptions') != 'undefined') {
                    that.templateDialog.dialog('option', that.templateDialog.prop('_ektronAlohaDialogDefaultOptions'));
                }
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);