/*!
* Aloha Translate Plugin
* -----------------
* This plugin provides an interface to allow the user to translate the content 
* by worldLingo machine translation.
* It presents its user interface in the Toolbar.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
	'aloha/plugin',
	'ui/ui',
	'ui/button',
	'ui/toolbar',
	'ui/ui-plugin',
	'aloha/console',
    'css!translate/css/translate-plugin.css'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
			i18n,
			i18nCore,
            console)
    {
        // members
        var namespace = "ektron-aloha-translate-",
            activeEditor = {},
            modal;

        // create and register the Plugin
        return Plugin.create("translate", {
            defaults: {},

            init: function () {
                // debugger;
                // executed on plugin initialization
                this.createButton();
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Translate.WorldLingo');
                    Ektron.Translate.WorldLingo.AcceptInsert = this.acceptInsert;
                    Ektron.Translate.WorldLingo.GetContent = this.getContent;
                    Ektron.Translate.WorldLingo.CloseDialog = this.closeDialog;
                }
                if (!($ektron(".ektron-aloha-translate-modal").is(".ui-dialog"))) {
                    modal = this.createModal();
                }

                modal.parents('.ui-dialog').find('.ui-dialog-titlebar button').remove();
                var titlebar = modal.parents('.ui-dialog').find('.ui-dialog-titlebar');
                $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-icon ui-icon-closethick"></span></button>')
                    .appendTo(titlebar)
                    .click(function () {
                        modal.dialog('close');
                    });
                modal.parents('.ui-dialog').find('.ui-dialog-titlebar button .ui-icon-closethick').show();
            },

            /* Helpers
            ----------------------------------*/
            getContent: function () {
                return Aloha.activeEditable.snapshotContent;
            },

            acceptInsert: function (htmlObj) {
                var h = {
                    content: "",
                    contentlanguage: ""
                }
                jQuery.extend(h, htmlObj);
                activeEditor.html(h.content);

                Aloha.getSelection().removeAllRanges();
                modal.dialog("close");
            },

            closeDialog: function ()
            {
                $ektron(".ektron-aloha-translate-modal").dialog("close");
            },

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;

                // define the translate button
                this.translateButton = Ui.adopt('translate', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.Translate.ResourceText.buttonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function ()
                    {
                        that.translateButtonClick();
                    }
                });
            },

            createModal: function () {
                $ektron('<div class="ektron-aloha-translate-modal"><iframe class="ektron-aloha-translate-modal-iframe" id="ektron-aloha-translate-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>').appendTo("body");
                return $ektron(".ektron-aloha-translate-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: false,
                    width: 820,
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                    modal: true,
                    zIndex: 100000001,
                    closeText: '<span class="ui-icon ui-icon-closethick" title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" />',
                    title: Ektron.Controls.Editor.Aloha.Plugins.Translate.ResourceText.modalTitle
                });
            },

            translateButtonClick: function () {
                modal.dialog("open");
                var contentlanguage = "1033",
                defaultcontentlanguage = "1033",
                contentid = "0";
                activeEditor = jQuery('.aloha-editable-active');
                if (Aloha.activeEditable.obj.data("ektronEditorData") != null) {
                    var data = Aloha.activeEditable.obj.data("ektronEditorData");
                    contentlanguage = data.contentLanguage.toString();
                    defaultcontentlanguage = data.defaultContentLanguage.toString();
                    contentid = data.contentId.toString();
                }
                $ektron("iframe.ektron-aloha-translate-modal-iframe").attr("src", Ektron.Context.Cms.WorkareaPath + "/GoogleTranslate.aspx?LangType=" + contentlanguage + "&DefaultContentLanguage=" + defaultcontentlanguage + "&htmleditor=content_html_content_html_aloha&id=" + contentid);
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);