﻿if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Validator)
{
    Ektron.Validator =
    {
        LoadForm: function() {
            this.ValidationTypeChanged("access");
        },

        ValidationTypeChanged: function(type) {
            var preDoc, postDoc, form = jQuery("form"),
                    content = parent.window.Ektron.Validator.GetContent();
            jQuery("textarea.w3c-fragment").val("");
            jQuery("textarea.achecker-pastehtml").val("");
            jQuery("input.w3c-doctype").val("");

            jQuery("input[name='radio_gid[]']").removeAttr("check");
            switch (type)
            {
                case "access":
                    jQuery("input#radio_gid_2").attr("checked", "checked");
                    break;
                case "wcag20a":
                    jQuery("input#radio_gid_7").attr("checked", "checked");
                    break;
                case "wcag20aa":
                    jQuery("input#radio_gid_8").attr("checked", "checked");
                    break;
                case "wcag20aaa":
                    jQuery("input#radio_gid_9").attr("checked", "checked");
                    break;
            }

            switch (type)
            {
                case "access":
                case "wcag20a":
                case "wcag20aa":
                case "wcag20aaa":
                    jQuery("textarea.achecker-pastehtml").val(content);
                    form.attr("action", "http://achecker.ca/checker/index.php");
                    jQuery(".standard_button").hide();
                    jQuery("input.validation_button").show();
                    break;
                case "xhtml":
                    preDoc = "<?xml version=\"1.0\"?>";
                    preDoc += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
                    preDoc += "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>W3c Validator</title></head><body>";
                    postDoc = "</body></html>";
                    jQuery("textarea.w3c-fragment").val(preDoc + content + postDoc);
                    form.attr("action", "http://validator.w3.org/check");
                    jQuery(".standard_button").show();
                    jQuery("input.validation_button").hide();
                    break;
                case "html5":
                default:
                    preDoc = "<!DOCTYPE html><html><head><title>W3c Validator</title></head><body>";
                    postDoc = "</body></html>";
                    jQuery("textarea.w3c-fragment").val(preDoc + content + postDoc);
                    jQuery("input.w3c-doctype").val("HTML5");
                    form.attr("action", "http://validator.w3.org/check");
                    jQuery(".standard_button").show();
                    jQuery("input.validation_button").hide();
                    break;
            }
        },

        ValidateContent: function(type) {
            var input, eDoctype = $ektron("textarea.doctype");
            switch (type.toLowerCase()) {
                case "access":
                case "wcag20a":
                case "wcag20aa":
                case "wcag20aaa":
                    $ektron("input.validation_button").click();
                    break;
                case "xhtml":
                    if (eDoctype.length > 0)
                    {
                        eDoctype.remove();
                    }
                    $ektron("form").submit();
                    break;
                case "html5":
                default:
                    if (eDoctype.length > 0)
                    {
                        eDoctype.remove();
                    }
                    // Validate as HTML 5              
                    //input = $ektron('<textarea id="doctype" class="doctype" name="doctype">HTML5</textarea>');
                    $ektron("form").append(input).submit();
                    break;
            }
        }
    }; }
