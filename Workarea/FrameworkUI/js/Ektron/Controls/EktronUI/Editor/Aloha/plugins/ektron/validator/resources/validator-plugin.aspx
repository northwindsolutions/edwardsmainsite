﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="validator-plugin.aspx.cs" Inherits="Workarea_FrameworkUI_js_Aloha_plugins_extra_validator_resources_validator_plugin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" style="height:500px;">
    <title></title>
    <script type="text/javascript">
        Ektron.ready(function ()
        {
            jQuery("iframe.validateIFrame").attr("src", Ektron.Context.Cms.WorkareaPath + "/blank.htm");
            Ektron.Validator.LoadForm();

            jQuery("span.standard_button").click(function ()
            {
                var validationType = jQuery("select.ektron-aloha-validator-dropdownlist").val();
                Ektron.Validator.ValidateContent(validationType);
            });
            jQuery("select.ektron-aloha-validator-dropdownlist").change(function ()
            {
                var validationType = jQuery("select.ektron-aloha-validator-dropdownlist").val();
                Ektron.Validator.ValidationTypeChanged(validationType);
            });
        });
    </script>
</head>
<body style="height:500px;">
    <form style="height:100%" id="form1" runat="server" name="input_form" enctype="multipart/form-data" method="post" target="validateIFrame" action="http://achecker.ca/checker/index.php">
    <div class="ektron-aloha-validator-header">
    <ektronUI:Label runat="server" ID="uxLabel"  Text="<%$ Resources:Aloha_Plugins_Validator_ModalDescription  %>"></ektronUI:Label>
    <div class="ektron-aloha-validator-optionsWrapper">
        <select id="ValidatorDropdownlist" class="ektron-aloha-validator-dropdownlist" >
            <option value="access"><asp:Literal ID="lblAccess" runat="server" Text="<%$ Resources:Aloha_Plugins_Validator_AccessLabel%>" /></option>
            <option value="wcag20a"><asp:Literal ID="lblWcag20a" runat="server" Text="<%$ Resources:Aloha_Plugins_Validator_WCAG20ALabel%>" /></option>
            <option value="wcag20aa"><asp:Literal ID="lblWcag20aa" runat="server" Text="<%$ Resources:Aloha_Plugins_Validator_WCAG20AALabel%>" /></option>
            <option value="wcag20aaa"><asp:Literal ID="lblWcag20aaa" runat="server" Text="<%$ Resources:Aloha_Plugins_Validator_WCAG20AAALabel%>" /></option>
            <option value="html5"><asp:Literal ID="lblHtml5" runat="server" Text="<%$ Resources:Aloha_Plugins_Validator_HTML5Label %>" /></option>
            <option value="xhtml"><asp:Literal ID="lblXHtml" runat="server" Text="<%$ Resources:Aloha_Plugins_Validator_XHTMLLabel%>" /></option>
        </select>
        <ektronUI:Button ID="uxSubmit" CssClass="ektron-aloha-validator-validateButton standard_button" DisplayMode="Button" runat="server" Text="<%$Resources:Aloha_Plugins_Validator_buttonTitle %>" />
        <input class="validation_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="submit" name="validate_paste" id="validate_paste" runat="server"/>
        <!-- AChecker.cs -->
        <br />
        <textarea rows="20" cols="10" name="pastehtml" id="checkpaste" class="achecker-pastehtml" style="display:none"></textarea>
        <input type="radio" name="radio_gid[]" id="radio_gid_2" value="2" style="display:none" />
        <input type="radio" name="radio_gid[]" id="radio_gid_7" value="7" style="display:none" />
        <input type="radio" name="radio_gid[]" id="radio_gid_8" value="8" style="display:none" />
        <input type="radio" name="radio_gid[]" id="radio_gid_9" value="9" style="display:none" />
        <input type="radio" name="rpt_format" id="option_rpt_gdl" value="1" checked="checked" style="display:none" />
        <!-- W3C.org -->
        <textarea rows="20" cols="10" name="fragment" id="fragment" class="w3c-fragment" style="display:none"></textarea>
        <input type="text" id="group" name="group" value='0' style="display:none" />
    </div>
    </div>
    <iframe name="validateIFrame" class="validateIFrame" width="99%" height="85%"></iframe>
    </form>
</body>
</html>
