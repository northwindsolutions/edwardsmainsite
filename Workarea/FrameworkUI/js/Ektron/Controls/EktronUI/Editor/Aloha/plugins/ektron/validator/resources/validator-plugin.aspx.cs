﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI;

public partial class Workarea_FrameworkUI_js_Aloha_plugins_extra_validator_resources_validator_plugin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        validate_paste.Value = GetLocalResourceObject("Aloha_Plugins_Validator_buttonTitle").ToString();
        //uxSubmit1.Value = GetLocalResourceObject("Aloha_Plugins_Validator_buttonTitle").ToString();
        this.RegisterResources();
    }

    private void RegisterResources()
    {
        Package editorProfilePackage = new Package()
        {
            Components = new List<Ektron.Cms.Framework.UI.Component>()
                {
                    Packages.Ektron.CssFrameworkBase,
                    Packages.EktronCoreJS,
                    Packages.Ektron.Namespace,
                    Packages.Ektron.Context.Cms,
                    Packages.Ektron.Workarea.Core,
                    Ektron.Cms.Framework.UI.JavaScript.Create("ektron.validator.js")
                }
        };

        editorProfilePackage.Register(this);
    }
}