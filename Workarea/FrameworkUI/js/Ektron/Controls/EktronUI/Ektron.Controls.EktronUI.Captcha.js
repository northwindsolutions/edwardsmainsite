
if ("undefined" == typeof (Ektron)) { Ektron = {}; }
if ("undefined" == typeof (Ektron.Controls)) { Ektron.Controls = {}; }
if ("undefined" == typeof (Ektron.Controls.EktronUI)) { Ektron.Controls.EktronUI = {}; }
if ("undefined" == typeof (Ektron.Controls.EktronUI.Captcha)) {

    var mySound;
    Ektron.Controls.EktronUI.Captcha = {

        CaptchaSubmit: function () {

            if ($ektron(".ektron-ui-captcha-inputWrapper input").val() == "") {
                return false;
            }
            var captchaValue = $ektron(".ektron-ui-captcha-inputWrapper input").val();
            var uniqueID = Ektron.Controls.EktronUI.Captcha.GetQueryStringValue();
            var result = $ektron.ajax({
                url: "/Workarea/FrameworkUI/handlers/Ektron/Controls/EktronUI/CaptchaImage.ashx",
                type: "POST",
                async: false,
                data: { captchaCode: captchaValue, action: "submit", unique: uniqueID }
            }).responseText;

            if (result == "false") {
                return false;
            }
            return true;
        },

        SoundManager: function () {
            soundManager.debugMode = false;
            soundManager.url = "/workarea/FrameworkUI/Templates/EktronUI/Captcha/soundmanager/swf/";
        },
        Refresh: function () {
            var captchaImg = $ektron(".captcha img").attr("src");
            captchaImg = captchaImg + "&rn=" + Math.random();
            $ektron(".captcha img").attr("src", captchaImg);
            $ektron(".ektron-ui-captcha-inputWrapper input").val("");
            $ektron(".ektron-ui-captcha-inputWrapper input").focus();
            return false;
        },
        PlaySound: function () {
            var captcha = Ektron.Controls.EktronUI.Captcha.Handler("play");
            mySound = soundManager.createSound({ id: "aReSound" + Math.floor(Math.random() * 10001),
                url: "/workarea/FrameworkUI/Templates/EktronUI/Captcha/soundmanager/en-us/" + captcha + ".mp3?r=" + Math.floor(Math.random() * 10001),
                autoLoad: false,
                autoPlay: false,
                whileplaying: function () {
                    Ektron.Controls.EktronUI.Captcha.Handler("delete");
                }
            });
            mySound.play();
            return false;
        },
        Handler: function (act) {
            var uniqueID = Ektron.Controls.EktronUI.Captcha.GetQueryStringValue();
            return $ektron.ajax({
                url: "/Workarea/FrameworkUI/handlers/Ektron/Controls/EktronUI/CaptchaImage.ashx",
                type: "POST",
                async: false,
                data: { action: act, unique: uniqueID }
            }).responseText;
        },
        GetQueryStringValue: function () {
            var sPageURL = $ektron(".captcha img").attr("src");
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == "unique") {
                    return sParameterName[1];
                }
            }
        }
    }
}
