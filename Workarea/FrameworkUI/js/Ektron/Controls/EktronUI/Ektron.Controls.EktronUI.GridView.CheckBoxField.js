﻿$ektron.widget("ektron.gridview", $ektron.extend({}, $ektron.ektron.gridview.prototype, {
    CheckBoxField: {
        AutoPostBack: {
            data: [],
            execute: function (instance, columnIndex) {
                var autoPostBackSelector = "thead th:nth-child(" + columnIndex + ") input.ektron-ui-item-selector-autoPostBack";
                var autoPostBack = instance.find(autoPostBackSelector);
                var uniqueIdSelector = "thead th:nth-child(" + columnIndex + ") input.ektron-ui-item-selector-uniqueId";
                var uniqueId = instance.find(uniqueIdSelector);

                //if autopostback is true, postback
                if (autoPostBack.val() == "true") {
                    __doPostBack(uniqueId.val(), Ektron.JSON.stringify(this.data));
                }

                //clear data
                this.data = [];
            }
        },
        _bindEvents: function (instance, ancestor) {
            var self = this;
            instance.find("tbody td > input.ektron-ui-gridview-checkbox").change(function () {
                var checkbox = $(this);
                var checkboxIsChecked = checkbox.is(":checked");
                var input = checkbox.next();
                var data = Ektron.JSON.parse(input.val());
                data.DirtyState = checkboxIsChecked ? "checked" : "unchecked";
                input.val(Ektron.JSON.stringify(data));

                //add data to autopostback array
                ancestor.AutoPostBack.data.push(data);

                var cell = checkbox.closest("td");
                var columnIndex = cell.parent("tr").children().index(cell) + 1;
                var selector = "thead th:nth-child(" + columnIndex + ") input:checkbox:not(:disabled)";
                var headerCheckbox = $(selector);
                if (selector.length > 0) {
                    var headerCheckboxIsChecked = headerCheckbox.is(":checked");
                    if (!checkboxIsChecked && headerCheckboxIsChecked) {
                        headerCheckbox.prop("checked", false);
                    }
                    if (checkboxIsChecked) {
                        var all = $("tbody td:nth-child(" + columnIndex + ") input:checkbox:not(:disabled)").length;
                        var checked = $("tbody td:nth-child(" + columnIndex + ") input:checkbox:not(:disabled):checked").length;

                        if (all === checked) {
                            headerCheckbox.prop("checked", true);
                        } else {
                            headerCheckbox.prop("checked", false);
                        }
                    }
                }
                if (!ancestor.Header.CheckBox._isHeaderCheckboxClicked) {
                    //execute AutoPostBack if necssary
                    ancestor.AutoPostBack.execute(instance, columnIndex);
                }
            });
        },
        _init: function (instance, ancestor) {
            this._bindEvents(instance, this);
            this.Header._init(instance, this);
        },
        Header: {
            CheckBox: {
                _bindEvents: function (instance, ancestor, checkbox) {
                    var self = this;
                    checkbox.change(function () {
                        //set header checkbox is clicked property to true so that autopostback only fires after the last click!
                        self._isHeaderCheckboxClicked = true;

                        var headerCheckbox = $(this);
                        var headerCheckboxIsChecked = headerCheckbox.is(":checked");
                        var cell = headerCheckbox.closest("th");
                        var columnIndex = cell.parent("tr").children().index(cell) + 1;
                        var selector = "tbody td:nth-child(" + columnIndex + ") input:checkbox:not(:disabled)";

                        instance.find(selector).each(function () {
                            var checkbox = $(this);
                            var isCheckboxChecked = checkbox.is(":checked");
                            //check checkbox is header is checked and checkbox is not checked
                            if (headerCheckboxIsChecked && !isCheckboxChecked) {
                                $(this).click();
                            }
                            //uncheck checkbox if header is not checked and checkbox is checked
                            if (!headerCheckboxIsChecked && isCheckboxChecked) {
                                $(this).click();
                            }
                        });

                        //autopostback if necessary
                        ancestor.AutoPostBack.execute(instance, columnIndex);

                        //set header checkbox is clicked property to true so that autopostback only fires after the last click!
                        self._isHeaderCheckboxClicked = false;
                    });
                },
                _init: function (instance, ancestor) {
                    var checkbox = this;
                    instance.find("thead th > input.ektron-ui-gridview-checkbox-header").each(function () {
                        //set default checked state
                        var headerCheckbox = $(this);
                        var cell = headerCheckbox.closest("th");
                        var columnIndex = cell.parent("tr").children().index(cell) + 1;
                        var checked = $("tbody td:nth-child(" + columnIndex + ") input:checkbox:not(:disabled):checked").length;
                        var unchecked = $("tbody td:nth-child(" + columnIndex + ") input:checkbox:not(:disabled):not(:checked)").length;
                        if (checked > 0 && unchecked === 0) {
                            headerCheckbox.prop("checked", true);
                        } else {
                            headerCheckbox.prop("checked", false);
                        }

                        //bind events
                        checkbox._bindEvents(instance, ancestor, $(this));
                    });
                },
                _isHeaderCheckboxClicked: false
            },
            _init: function (instance, ancestor) {
                this.CheckBox._init(instance, ancestor);
            }
        }
    }
}));