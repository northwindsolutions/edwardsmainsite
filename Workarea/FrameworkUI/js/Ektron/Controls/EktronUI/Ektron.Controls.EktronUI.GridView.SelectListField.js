﻿$ektron.widget("ektron.gridview", $ektron.extend({}, $ektron.ektron.gridview.prototype, {
    SelectListField: {
        AutoPostBack: {
            data: [],
            execute: function (instance, columnIndex) {
                var autoPostBackSelector = "thead th:nth-child(" + columnIndex + ") input.ektron-ui-item-selector-autoPostBack";
                var autoPostBack = instance.find(autoPostBackSelector);
                var uniqueIdSelector = "thead th:nth-child(" + columnIndex + ") input.ektron-ui-item-selector-uniqueId";
                var uniqueId = instance.find(uniqueIdSelector);

                //if autopostback is true, postback
                if (autoPostBack.val() == "true") {
                    __doPostBack(uniqueId.val(), Ektron.JSON.stringify(this.data));
                }

                //clear data
                this.data = [];
            }
        },
        _bindEvents: function (instance, ancestor) {
            var self = this;
            instance.find("tbody td > select.ektron-ui-gridview-selectlist").change(function () {
                var select = $(this);
                var input = select.next();
                var data = Ektron.JSON.parse(input.val());
                data.DirtyState = select.val();
                input.val(Ektron.JSON.stringify(data));

                //add data to autopostback array
                ancestor.AutoPostBack.data.push(data);

                var cell = select.closest("td");
                var columnIndex = cell.parent("tr").children().index(cell) + 1;

                //execute autopostback (if necessary)
                ancestor.AutoPostBack.execute(instance, columnIndex);
            });
        },
        _init: function (instance, ancestor) {
            this._bindEvents(instance, this);
        }
    }
}));