﻿(function ($) {
    $.widget("ektron.gridview", {
        options: {
            autoResize: true,
            dataField: {},
            fixedFooter: true,
            fixedHeader: true,
            resizableColumns: true
        },
        _create: function (options) {
            var instance = this.element;

            //setup ui
            this.UI.set(instance, this.options);

            //setup paging
            this.Paging._init(instance);

            //setup sorting
            this.Sorting._init(instance);

            //setup selection columns
            if ("undefined" != typeof (this.CheckBoxField)) {
                this.CheckBoxField._init(instance);
            }
            if ("undefined" != typeof (this.RadioButtonField)) {
                this.RadioButtonField._init(instance);
            }
		
       	    if ("undefined" != typeof (this.SelectListField)) {
                this.SelectListField._init(instance);
            }
               

            if ("undefined" != typeof (this.Pager)) {
                this.Pager._init(instance);
            }
        },
        Paging: {
            _init: function (instance) {
                this.UI.set(instance);
            },
            UI: {
                set: function (instance) {
                    //move footer to tfoot - note this cannot be added server-side since <asp:GridView
                    //adds thead, tbody, tfoot in the wrong order (invalid!)
                    var footer = $(instance).find("tbody .ektron-ui-row-pager-content").closest("tr");
                    footer.find("script").remove();
                    footer.addClass("ektron-ui-row-pager");
                    var thead = $(instance).children("thead");
                    $("<tfoot></tfoot>").insertAfter(thead);
                    var tfoot = $(instance).children("tfoot");
                    tfoot.append(footer);
                    $(instance).find("tbody .ektron-ui-row-footer-content").parent().addClass("ui-toolbar ui-widget-header");
                }
            }
        },
        Sorting: {
            bindEvents: function (instance, parent) {
                var sortableColumns = $(instance).find("thead th.ektron-ui-sortable-column");
                sortableColumns.hover(function () { $(this).addClass("ui-state-hover"); }, function () { $(this).removeClass("ui-state-hover"); });
                sortableColumns.find(".sort-wrapper > a").click(function () {
                    var data = parent.Data.get();
                    data.OrderByDirection = (data.OrderByDirection == 0) ? 1 : 0;
                    data.OrderByFieldText = $(this).siblings("input.ektron-ui-sort-expression").val();
                    parent.Data.set(data);
                });
            },
            Data: {
                _init: function (instance) {
                    this.dataField = $(instance).parent().prevAll("input.ektron-ui-gridview-sortingdata");
                    this._data = Ektron.JSON.parse(this.dataField.val());
                },
                get: function () {
                    return this._data;
                },
                set: function (data) {
                    this.dataField.val(Ektron.JSON.stringify(data));
                }
            },
            _init: function (instance) {
                this.Data._init(instance);
                this.UI.set(instance);
                this.bindEvents(instance, this);
            },
            UI: {
                set: function (instance) {
                    var sortableColumns = $(instance).find("thead th.ektron-ui-sortable-column");
                    sortableColumns.addClass("ui-state-default");
                    sortableColumns.each(function () {
                        $(this).find("a")
                            .append("<span class='ui-icon ui-icon-carat-2-n-s'></span>")
                            .closest("th").contents().wrapAll("<div class=\"sort-wrapper\"></div>")
                    });
                    $(instance).find("thead th.ektron-ui-sort-ascending").find("span.ui-icon")
                        .removeClass("ui-icon-carat-2-n-s")
                        .addClass("ui-icon-triangle-1-n");
                    $(instance).find("thead th.ektron-ui-sort-descending").find("span.ui-icon")
                        .removeClass("ui-icon-carat-2-n-s")
                        .addClass("ui-icon-triangle-1-s");
                }
            }
        },
        UI: {
            ResizableColumns: {
                set: function (instance) {
                    $(instance).find("thead>tr>th").each(function () {
                        var column = $(this);
                        var input = column.find("input.ektron-ui-resizable-column-width");
                        var data = $.parseJSON(input.val());
                        if (data.width === "default") {
                            data.width = column.width();
                            input.val(Ektron.JSON.stringify(data));
                        } else {
                            column.width(data.width + "px");
                        }
                    });

                    $(instance).colResizable({ "disable": true });
                    $(instance).colResizable({
                        onResize: function (e) {
                            instance.find("thead>tr>th").each(function () {
                                var column = $(this);
                                var input = column.find("input.ektron-ui-resizable-column-width");
                                var data = $.parseJSON(input.val());
                                data.width = column.width();
                                input.val(Ektron.JSON.stringify(data));
                            });
                        }
                    });
                }
            },
            set: function (instance, options) {
                //add striping
                $(instance).find("tbody tr:odd").addClass("ektron-ui-row-odd");
                $(instance).find("tbody tr:even").addClass("ektron-ui-row-even");

                //add themeroller class to header cells
                $(instance).find("thead th").addClass("ui-state-default");

                //setup resizable columns
                if (options.resizableColumns) {
                    this.ResizableColumns.set(instance);
                }
            }
        },
        _setOption: function (key, value) {
            switch (key) {
                case "clear":
                    break;
            }
            $.Widget.prototype._setOption.apply(this, arguments);
            this._super("_setOption", key, value);
        },
        destroy: function () {
            $.Widget.prototype.destroy.call(this);
        }
    });
} ($ektron));