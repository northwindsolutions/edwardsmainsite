﻿if ("undefined" == typeof (Ektron)) { Ektron = {}; }
if ("undefined" == typeof (Ektron.Controls)) { Ektron.Controls = {}; }
if ("undefined" == typeof (Ektron.Controls.EktronUI)) { Ektron.Controls.EktronUI = {}; }
if ("undefined" == typeof (Ektron.Controls.EktronUI.Wizard)) {
    Ektron.Controls.EktronUI.Wizard = {
        init: function (ui) {
            // Wire up styles for all steps
            $ektron(ui).find(".steps li")
                .addClass("ui-widget")
                .addClass("ui-state-default")
                .bind("mousedown", function () {
                    $ektron(this).addClass("ui-state-focus");
                })
                .bind("mouseup", function () {
                    $ektron(this).removeClass("ui-state-focus");
                });

            // Mark current step with the jQuery UI state "active"
            $ektron(ui).find(".steps li.current").addClass("ui-state-active");

            // Only enable hover state on enabled steps
            $ektron(ui).find(".steps li.enabled")
                .bind("mouseenter", function () {
                    $ektron(this).addClass("ui-state-hover");
                })
                .bind("mouseleave", function () {
                    $ektron(this).removeClass("ui-state-hover");
                })
        }
    };
}