﻿/*
* Depends:
*  jQuery/Ektron Core
*  Ektron.Namespace.js
*  ektron.glob.js
*/

// create our namespace
Ektron.Namespace.Register("Ektron.Controls.EktronUI.Validation");

//lessThan = 0,
//lessThanOrEquals,
//equals,
//GreaterThanOrEqual,
//greaterThan

Ektron.Controls.EktronUI.Validation.ComparisonHelpers = {
    /* 
    For each of the helpers below, the params used are:
    compareMode:  the string mapping for the opperand used in the comparison,
    field1: a jQuery object referring to the first field to be compared,
    field2: a jQuery object referring to the second field to be compared
    */

    dateCompare: function (compareMode, field1, field2) {
        var isValid = false;
        var field1Culture = Ektron.Controls.EktronUI.Validation.ComparisonHelpers.getFieldCulture(field1);
        var field2Culture = Ektron.Controls.EktronUI.Validation.ComparisonHelpers.getFieldCulture(field2);

        try {
            // parse the values as date objects
            var date1 = $ektron.global.parseDate(field1.val(), "d", field1Culture);
            var date2 = $ektron.global.parseDate(field2.val(), "d", field2Culture);

            switch (compareMode) {
                case "lessThan":
                    if (date1 < date2) {
                        isValid = true;
                    }
                    break;
                case "lessThanOrEquals":
                    if (date1 <= date2) {
                        isValid = true;
                    }
                    break;
                case "equals":
                    if (date1 == date2) {
                        isValid = true;
                    }
                    break;
                case "greaterThanOrEqual":
                    if (date1 >= date2) {
                        isValid = true;
                    }
                    break;
                case "greaterThan":
                    if (date1 > date2) {
                        isValid = true;
                    }
                    break;
            }
        }
        catch (e) {
            return false;
        }

        return isValid;
    },

    decimalCompare: function (compareMode, field1, field2) {
        var isValid = false;
        var field1Culture = Ektron.Controls.EktronUI.Validation.ComparisonHelpers.getFieldCulture(field1);
        var field2Culture = Ektron.Controls.EktronUI.Validation.ComparisonHelpers.getFieldCulture(field2);

        try {
            // parse the values as decimals
            value1 = $ektron.global.parseFloat(value1, 10, field1Culture);
            value2 = $ektron.global.parseFloat(value2, 10, field2Culture);

            // ensure we have valid decimals
            if (value1 == null || isNaN(value1)) {
                return false;
            }
            if (value2 == null || isNaN(value2)) {
                return false;
            }

            switch (compareMode) {
                case "lessThan":
                    if (value1 < value2) {
                        isValid = true;
                    }
                    break;
                case "lessThanOrEquals":
                    if (value1 <= value2) {
                        isValid = true;
                    }
                    break;
                case "equals":
                    if (value1 == value2) {
                        isValid = true;
                    }
                    break;
                case "greaterThanOrEqual":
                    if (value1 >= value2) {
                        isValid = true;
                    }
                    break;
                case "greaterThan":
                    if (value1 > value2) {
                        isValid = true;
                    }
                    break;
            }
        }
        catch (e) {
            return false;
        }

        return isValid;
    },

    intergerCompare: function (compareMode, field1, field2) {
        var isValid = false;
        var field1Culture = Ektron.Controls.EktronUI.Validation.ComparisonHelpers.getFieldCulture(field1);
        var field2Culture = Ektron.Controls.EktronUI.Validation.ComparisonHelpers.getFieldCulture(field2);

        try {
            // parse the values as decimals
            value1 = $ektron.global.parseInt(value1, 10, field1Culture);
            value2 = $ektron.global.parseInt(value2, 10, field2Culture);

            // ensure we have valid decimals
            if (value1 == null || isNaN(value1)) {
                return false;
            }
            if (value2 == null || isNaN(value2)) {
                return false;
            }

            switch (compareMode) {
                case "lessThan":
                    if (value1 < value2) {
                        isValid = true;
                    }
                    break;
                case "lessThanOrEquals":
                    if (value1 <= value2) {
                        isValid = true;
                    }
                    break;
                case "equals":
                    if (value1 == value2) {
                        isValid = true;
                    }
                    break;
                case "greaterThanOrEqual":
                    if (value1 >= value2) {
                        isValid = true;
                    }
                    break;
                case "greaterThan":
                    if (value1 > value2) {
                        isValid = true;
                    }
                    break;
            }
        }
        catch (e) {
            return false;
        }

        return isValid;
    },

    stringCompare: function (compareMode, field1, field2) {
        var isValid = false;
        var value1Length = field1.val().length;
        var value2Length = field2.val().length;

        switch (compareMode) {
            case "lessThan":
                if (value1Length < value2Length) {
                    isValid = true;
                }
                break;
            case "lessThanOrEquals":
                if (value1Length <= value2Length) {
                    isValid = true;
                }
                break;
            case "equals":
                if (value1 == value2) {
                    isValid = true;
                }
                break;
            case "greaterThanOrEqual":
                if (value1Length >= value2Length) {
                    isValid = true;
                }
                break;
            case "greaterThan":
                if (value1Length > value2Length) {
                    isValid = true;
                }
                break;
        }

        return isValid;
    },

    /* helpers */
    getCulture: function (field) {
        return field.data()["ektron-global-culture"] ? field.data()["ektron-global-culture"] : "default";
    }
};