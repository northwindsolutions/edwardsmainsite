﻿$ektron.validator.addMethod("alluppercase", function (value, element) {
    return this.optional(element) || value === value.toUpperCase();
}, "Upper case value only please.");