﻿$ektron.validator.addMethod("integer", function (value, element) {
    var isInteger = false;
    var elementObj = $ektron(element);
    var culture = elementObj.data()["ektron-global-culture"] ? elementObj.data()["ektron-global-culture"] : "default";

    // ensure the globalization plugin is loaded
    if ("undefined" == typeof ($ektron.global.parseInt)) {
        throw "The Globalization plugin must be loaded to use this method.";
        return isInteger;
    }
    try {
        var tryInteger = $ektron.global.parseInt(value, 10, culture);
        var tryDecimal = $ektron.global.parseFloat(value, 10, culture);
        if (tryInteger !== null && !(isNaN(tryInteger)) && (tryDecimal == tryInteger)) {
            isInteger = true;
        }
    }
    catch (e) {
        return false;
    }
    return this.optional(element) || isInteger;
}, "Please enter a correctly formatted decimal.");