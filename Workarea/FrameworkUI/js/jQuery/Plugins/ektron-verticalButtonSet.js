﻿/*
* Ektron UI VerticalButtonSet
*
* Copyright (c) 2011, 
* Dual licensed under the MIT or GPL licenses.
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
*/
(function ($) {
    //plugin buttonset vertical
    $.fn.buttonsetv = function () {
        $(':radio, :checkbox', this).wrap('<div style="margin: 1px"/>');
        $('label:first', this).removeClass('ui-corner-left').addClass('ui-corner-top');
        $('label:last', this).removeClass('ui-corner-right').addClass('ui-corner-bottom');
        mw = 0; // max witdh
        $('label', this).each(function (index) {
            w = $(this).width();
            if (w > mw) mw = w;
        })
        $('label', this).each(function (index) {
            $(this).width(mw);
        })
    };
})($ektron);
