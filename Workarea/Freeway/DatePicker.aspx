<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DatePicker.aspx.cs" Inherits="Workarea_Freeway_DatePicker" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Freeway Calendar</title>
    <script type = "text/javascript">
        function returndate(textboxcontrol,hiddencontrol,date)
        {
            opener.document.getElementById(textboxcontrol).value = date;
            opener.document.getElementById(hiddencontrol).value = date;
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Calendar ID="DatePickerControl" runat="server" OnSelectionChanged="DatePickerControl_SelectionChanged">
            <NextPrevStyle ForeColor="Highlight" />
            <DayHeaderStyle BackColor="Gray" />
            <TitleStyle ForeColor="WindowText" />
        </asp:Calendar>
    </div>
    </form>
</body>
</html>
