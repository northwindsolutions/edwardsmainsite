using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Workarea_Freeway_DatePicker : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void DatePickerControl_SelectionChanged(object sender, EventArgs e)
    {
        string textboxcontrol = Request.QueryString["textboxcontrol"];
        string hiddencontrol = Request.QueryString["hiddencontrol"];
        string returndate = "javascript:returndate('" + textboxcontrol + "','" + hiddencontrol + "','" + DatePickerControl.SelectedDate.ToShortDateString() + "')";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "call returndate", returndate, true);
    }
}
