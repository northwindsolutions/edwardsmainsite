<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="FreewayAddViewTask.aspx.cs"
    Inherits="Workarea_Freeway_FreewayAddViewTask" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <asp:literal id="jsStyleSheet" runat="server" />

    <script language="JavaScript" type="text/javascript" src="../java/jfunct.js"></script>

    <script language="JavaScript" type="text/javascript" src="../java/toolbar_roll.js"></script>

    <script type="text/javascript">
    
        function SelectAllLanguages(allchecked)
        {
            var gvSTL = document.getElementById('gvSTL');
            for (i=1; i<gvSTL.rows.length; i++)
            {
                var row = gvSTL.rows[i].cells[0].getElementsByTagName('input');
                for (j=0;j<row.length;j++)
                {
                    if(row[j].type == 'checkbox') 
                    { 
                       document.getElementById(row[j].id).checked = allchecked.checked;
                       break;
                    } 
                }
            }
        }
        
        function CheckforLanguages()
        {
            var booleanvalue = false;
            var gvSTL = document.getElementById('gvSTL');
            for (i=1; i<gvSTL.rows.length; i++)
            {
                var row = gvSTL.rows[i].cells[0].getElementsByTagName('input');
                for (j=0;j<row.length;j++)
                {
                    if(row[j].type == 'checkbox'&& document.getElementById(row[j].id).checked) 
                    { 
                       booleanvalue = true;
                       break;
                    } 
                }
            }
            
            return booleanvalue;
        }
	    function SubmitForm(FormName, Validate) {
			    var go = true;
			    if (Validate.length > 0) {
				    if (eval(Validate)) {
					    document.forms[0].submit();
					    return false;
				    }
				    else {
					    return false;
				    }
			    }
			    else {
				    document.forms[0].submit();
				    return false;
			    }
		    }
		    

    		
	    function VerifyForm () {
    	       /*if(Trim(document.forms[0].ddlSourceLanguages.options[document.forms[0].ddlSourceLanguages.selectedIndex].value) == "Select") 
    	       {
        	        
    	            document.forms[0].ddlSourceLanguages.focus();
    	            alert('<asp:Literal ID="error_ddlSourceLanguagesCantBeBlank" Text="Source Language is required." runat="server"/>');
    	            return false;
    	       }
    	       else if(Trim(document.forms[0].ddlTargetLanguages.options[document.forms[0].ddlTargetLanguages.selectedIndex].value) == "Select") 
    	       {
        	        
    	            document.forms[0].ddlTargetLanguages.focus();
    	            alert('<asp:Literal ID="error_ddlTargetLanguagesCantBeBlank" Text="Target Language is required." runat="server"/>');
    	            return false;
    	       }*/
    	       if(!CheckforLanguages()) 
    	       {
        	        
    	            document.getElementById('gvSTL').focus();
    	            alert('<asp:Literal ID="error_ddlLanguagesCantBeBlank" Text="Selection of atleast one Language is required." runat="server"/>');
    	            return false;
    	       }
    	       else if(Trim(document.forms[0].ddlTasks.options[document.forms[0].ddlTasks.selectedIndex].value) == "Select") 
    	       {
    	            document.forms[0].ddlTasks.focus();
    	            alert('<asp:Literal ID="error_ddlTasksCantBeBlank" Text="Task is required" runat="server"/>');
    	            return false;
               }
               else if(Trim(document.forms[0].ddlComponents.options[document.forms[0].ddlComponents.selectedIndex].value) == "Select") 
    	       {
    	            document.forms[0].ddlComponents.focus();
    	            alert('<asp:Literal ID="error_ddlComponentsCantBeBlank" Text="Component is required" runat="server"/>');
    	            return false;
               }
               else if(Trim(document.forms[0].ddlSubjects.options[document.forms[0].ddlSubjects.selectedIndex].value) == "Select") 
    	       {
    	            document.forms[0].ddlSubjects.focus();
    	            alert('<asp:Literal ID="error_ddlSubjectsCantBeBlank" Text="Subject is required" runat="server"/>');
    	            return false;
               }
               else if(Trim(document.forms[0].ddlSubtasks.options[document.forms[0].ddlSubtasks.selectedIndex].value) == "Select") 
    	       {
    	            document.forms[0].ddlSubtasks.focus();
    	            alert('<asp:Literal ID="error_ddlSubtasksCantBeBlank" Text="Subtasks is required" runat="server"/>');
    	            return false;
               }
               else if(Trim(document.forms[0].txtVolume.value).length == 0 || isNaN(Trim(document.forms[0].txtVolume.value)) )
    	       {
    	            document.forms[0].txtVolume.focus();
    	            alert('<asp:Literal ID="error_txtVolumeCantBeBlank" Text="Volume is not a numeric" runat="server"/>');
    	            return false;
               }
               else if(Trim(document.forms[0].ddlUOM.options[document.forms[0].ddlUOM.selectedIndex].value) == "Select") 
    	       {
    	            document.forms[0].ddlUOM.focus();
    	            alert('<asp:Literal ID="error_ddlUOMCantBeBlank" Text="UOM is required" runat="server"/>');
    	            return false;
               }
               else
               {
                    document.forms[0].hdnsaveclick.value = "Clicked";
    	            return true;
    	       }
    	    }
    </script>

    <style type="text/css">
            #dvUserGroups p {padding:0em 1em 1em 1em;margin:0;font-weight:bold;color:#1d5987;}
            #dvUserGroups ul.userGroups {list-style:none;margin:0 0 0 1em;padding:0;border:1px solid #d5e7f5;}
            #dvUserGroups ul.userGroups li {display:block;padding:.25em;border-bottom:none;}
            #dvUserGroups ul.userGroups li.stripe {background-color:#e7f0f7;}
            #dvWorkpage td.label {width:auto;}
            .gridViewPager  
            { 
             text-align: left; 
             padding: 50px 50px 50px 50px; 
            }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div id="dhtmltooltip">
                    </div>

                    <script language="JavaScript" type="text/javascript" src="../java/workareahelper.js"></script>

                    <table width="100%" class="ektronPageHeader">
                        <tr>
                            <td id="txtTitleBar" class="ektronTitlebar" runat="server" nowrap>
                            </td>
                        </tr>
                        <tr>
                            <td id="htmToolBar" class="ektronToolbar" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="ektronGrid" id="_addViewTasksDiv" runat="server" style="border-collapse: collapse;"
            cellspacing="0" rules="all" border="1">
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblprojectID" Text="Project ID:"></asp:Label>
                </td>
                <td class="value" style="width: 60%;" >
                    <asp:Label ID="lblstaticprojectID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblFSTL" Text="Files Source and Target Langugaes:"></asp:Label>
                </td>
                <td class="value" >
                    <div id = "dvSTL" runat = "server" style ="height:80px; overflow:auto;width:60%;">
                        <asp:GridView ID="gvSTL" CssClass="ektronGrid" runat="server" BorderColor="#999999"
                                BorderStyle="Solid" BorderWidth="1px" CellPadding="3" AutoGenerateColumns="False" >
                                <HeaderStyle CssClass="title-header" Width="10px" />
                                <RowStyle Width="10px" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox runat="server" ID="HeaderLevelCheckBox" Text = "All" onclick = "SelectAllLanguages(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="RowLevelCheckBox" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label runat="server" ID="lblHeaderSourceLanguage" Text = "Source Language" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSourceLanguageID" Visible = "false"  Text='<%# Eval("SourceLanguageID") %>'></asp:Label> 
                                        <asp:Label runat="server" ID="lblSourceLanguageDescription"  Text='<%# Eval("SourceLanguageDescription") %>'></asp:Label> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label runat="server" ID="lblHeaderTargetLanguage" Text = "Target Language" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTargetLanguageID" Visible = "false"  Text='<%# Eval("TargetLanguageID") %>'></asp:Label> 
                                        <asp:Label runat="server" ID="lblTargetLanguageDescription" Text='<%# Eval("TargetLanguageDescription") %>'></asp:Label> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <%--<tr>
            <td class="label" style = "Width:40%;">
                    <asp:Label runat="server" ID="lblSourceLanguages" Text="Source Languages:"></asp:Label> 
            </td>
            <td class="value" style = "Width:60%;">
                <asp:DropDownList Width="156px" ID="ddlSourceLanguages" runat="server" AutoPostBack = "true" OnSelectedIndexChanged="ddlSourceLanguages_SelectedIndexChanged" ></asp:DropDownList>
            </td>
         </tr>
         <tr>
            <td class="label" style = "Width:40%;">
                    <asp:Label runat="server" ID="lblTargetLanguages" Text="Target Languages:"></asp:Label> 
            </td>
            <td class="value" style = "Width:60%;">
                <asp:DropDownList Width="156px" ID="ddlTargetLanguages" runat="server" ></asp:DropDownList>
            </td>
         </tr>--%>
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblTasks" Text="Available Tasks:"></asp:Label>
                </td>
                <td class="value" style="width: 60%;" >
                    <asp:DropDownList runat="server" ID="ddlTasks" Width="156px" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlTasks_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblComponents" Text="Available Components:"></asp:Label>
                </td>
                <td class="value" style="width: 60%;" >
                    <asp:DropDownList runat="server" ID="ddlComponents" Width="156px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblSubjects" Text="Available Subjects:"></asp:Label>
                </td>
                <td class="value" style="width: 60%;" >
                    <asp:DropDownList runat="server" ID="ddlSubjects" Width="156px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblsubTasks" Text="Available Sub-Tasks:"></asp:Label>
                </td>
                <td class="value" style="width: 60%;" >
                    <asp:DropDownList runat="server" ID="ddlSubtasks" Width="156px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label" style="height: 27px; width: 30%;">
                    <asp:Label runat="server" ID="lblVolume" Text="Volume:"></asp:Label>
                </td>
                <td class="value" style="height: 27px; width: 70%;" >
                    <asp:TextBox runat="server" ID="txtVolume" MaxLength="9" Width="156px"> </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 40%;">
                    <asp:Label runat="server" ID="lblUOM" Text="UOM:"></asp:Label>
                </td>
                <td class="value" style="width: 60%;" >
                    <asp:DropDownList runat="server" ID="ddlUOM" Width="156px">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;">
                    <div id = "dvTasks" runat = "server" class="ektronPageContainer ektronPageGrid" style ="height:150px; overflow:auto;">
                        <asp:GridView CssClass="ektronGrid" ID="gvTasks" runat="server" BorderColor="#999999"
                            BorderStyle="Solid" BorderWidth="1px" CellPadding="3" AutoGenerateColumns="False"
                            OnRowDataBound="gvTasks_RowDataBound">
                            <HeaderStyle CssClass="title-header" Width="10px" />
                            <RowStyle Width="10px" />
                            <Columns>
                                <asp:BoundField DataField="Task" HeaderText="Task" SortExpression="Task" />
                                <asp:BoundField DataField="Subtask" HeaderText="SubTask" SortExpression="SubTask" />
                                <asp:BoundField DataField="UOM" HeaderText="UOM" SortExpression="UOM" />
                                <asp:BoundField DataField="SourceLanguage" HeaderText="SourceLanguage" SortExpression="SourceLanguage" />
                                <asp:BoundField DataField="TargetLanguage" HeaderText="TargetLanguage" SortExpression="TargetLanguage" />
                                <asp:BoundField DataField="Component" HeaderText="Component" SortExpression="Component" />
                                <asp:BoundField DataField="SubjectMatter" HeaderText="SubjectMatter" SortExpression="SubjectMatter" />
                                <asp:BoundField DataField="Rate" HeaderText="Rate" SortExpression="Rate" />
                                <asp:BoundField DataField="Volume" HeaderText="Volume" SortExpression="Volume" />
                                <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" SortExpression="Subtotal" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" runat="server" id="hdnsaveclick" style="display: none;" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
