#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : This page will avail the task view and add functionality.
---------------------------------
*/
#endregion

#region using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LB.FreewayLib.VojoService;
using Ektron.Cms.Common;
using Ektron.Cms;
using System.Text;
#endregion


public partial class Workarea_Freeway_FreewayAddViewTask : System.Web.UI.Page
{
    #region Private and Protected Variables
    private StyleHelper m_refStyle = new StyleHelper();
    private EkMessageHelper m_refMsg;
    protected string StyleSheetJS = "";
    #endregion

    #region Private Function
    /// <summary>
    /// Retrieves the tasks from Freeway and binds to the Grid 
    /// showing latest task at the top
    /// </summary>
    private void loadTasks()
    {
        // Binds the datagrid to the Freeway Tasks data
        projectdetailLine[] tasks = FreewayConfiguration.GetTasks(lblstaticprojectID.Text);

        DataTable tasktable = new DataTable("Task_Table");
        tasktable.Columns.Add("Task", typeof(string));
        tasktable.Columns.Add("SubTask", typeof(string));
        tasktable.Columns.Add("UOM", typeof(string));
        tasktable.Columns.Add("SourceLanguage", typeof(string));
        tasktable.Columns.Add("TargetLanguage", typeof(string));
        tasktable.Columns.Add("Component", typeof(string));
        tasktable.Columns.Add("SubjectMatter", typeof(string));
        tasktable.Columns.Add("Rate", typeof(string));
        tasktable.Columns.Add("Volume", typeof(string));
        tasktable.Columns.Add("Subtotal", typeof(string));

        if (tasks != null)
        {
            foreach (projectdetailLine task in tasks)
            {
                DataRow dr = tasktable.NewRow();
                dr["Task"] = task.Description;
                dr["SubTask"] = task.Subtask.Value;
                dr["UOM"] = task.UOM.Value;
                dr["SourceLanguage"] = task.SourceLanguage.Value;
                dr["TargetLanguage"] = task.TargetLanguage.Value;
                dr["Component"] = task.Component.Value;
                dr["SubjectMatter"] = task.SubjectMatter.Value;
                dr["Rate"] = task.Rate;
                dr["Volume"] = task.Volume;
                dr["Subtotal"] = task.Subtotal;
                tasktable.Rows.Add(dr);
            }
        }

        //DataView taskview = tasktable.DefaultView;
        //taskview.Sort = SortOrder.MostRecent

        gvTasks.DataSource = tasktable;
        gvTasks.DataBind();



        FileStatusList fileStatus = FreewayConfiguration.GetFileStatus(Convert.ToString(Session["projectid"]));

        DataTable dt = new DataTable();
        dt.Columns.Add("SourceLanguageID");
        dt.Columns.Add("SourceLanguageDescription");
        dt.Columns.Add("TargetLanguageID");
        dt.Columns.Add("TargetLanguageDescription");

        Hashtable htLanguage = new Hashtable();
        foreach (Language vojoLanguage in FreewayConfiguration.SourceLanguages)
        {
            if (!htLanguage.ContainsKey(vojoLanguage.ID))
                htLanguage.Add(vojoLanguage.ID, vojoLanguage.Description);
            foreach (Language vojoTargetLanguage in FreewayConfiguration.TargetLanguages(vojoLanguage.ID))
                if (!htLanguage.ContainsKey(vojoTargetLanguage.ID))
                    htLanguage.Add(vojoTargetLanguage.ID, vojoTargetLanguage.Description);
        }

        if (fileStatus != null)
        {
            foreach (FileStatus status in fileStatus.FileStatuses)
            {
                DataRow dr = dt.NewRow();
                dr["SourceLanguageID"] = status.SourceLanguageID;
                dr["SourceLanguageDescription"] = htLanguage[status.SourceLanguageID];
                dr["TargetLanguageID"] = status.TargetLanguageID;
                dr["TargetLanguageDescription"] = htLanguage[status.TargetLanguageID];
                dt.Rows.Add(dr);
            }
        }
        gvSTL.DataSource = dt;
        gvSTL.DataBind();

    }
    #endregion

    #region Page Events
    /// <summary>
    /// Loads the Task respective data after retrieving from Freeway 
    /// at the 1st time load of the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //StyleSheetJS = m_refStyle.GetClientScript();
        jsStyleSheet.Text = (new StyleHelper()).GetClientScript();

        m_refMsg = (new CommonApi()).EkMsgRef;


        if (Request.QueryString["projectid"] == null
                || Convert.ToString(Request.QueryString["projectid"]).ToLower() != "undefined"
                )
            Session["projectid"] = Request.QueryString["projectid"];
        lblstaticprojectID.Text = Convert.ToString(Session["projectid"]);



        

        if (!IsPostBack)
        {

            //Bind the Available Tasks
            ddlTasks.Items.Clear();
            ddlTasks.Items.Add(new ListItem("Select", "Select"));
            foreach (LB.FreewayLib.Freeway.VojoTask task in FreewayConfiguration.Tasks)
                ddlTasks.Items.Add(new ListItem(task.Description + "-" + task.ID, task.ID));


            //Bind the Source Languages
            //ddlSourceLanguages.Items.Clear();
            //ddlSourceLanguages.Items.Add(new ListItem("Select", "Select"));
            //foreach (Language vojoLanguage in FreewayConfiguration.SourceLanguages)
            //    ddlSourceLanguages.Items.Add(new ListItem(vojoLanguage.Description, vojoLanguage.ID));

            //Bind the Target Languages
            //ddlTargetLanguages.Items.Clear();
            //ddlTargetLanguages.Items.Add(new ListItem("Select", "Select"));



            //Bind the Available Components
            ddlComponents.Items.Clear();
            ddlComponents.Items.Add(new ListItem("Select", "Select"));
            foreach (LB.FreewayLib.Freeway.VojoComponent component in FreewayConfiguration.Components)
                ddlComponents.Items.Add(new ListItem(component.Description, component.ID));

            //Bind the Available Subjects
            ddlSubjects.Items.Clear();
            ddlSubjects.Items.Add(new ListItem("Select", "Select"));
            foreach (LB.FreewayLib.Freeway.VojoSubject subject in FreewayConfiguration.Subjects)
                ddlSubjects.Items.Add(new ListItem(subject.Description, subject.ID));

            //Bind the SubTasks
            ddlSubtasks.Items.Clear();
            ddlSubtasks.Items.Add(new ListItem("Select", "Select"));

            //Bind the UOMs
            ddlUOM.Items.Clear();
            ddlUOM.Items.Add(new ListItem("Select", "Select"));

            loadTasks();
        }

        if (gvSTL.Rows.Count > 1)
        {
            dvSTL.Style.Add("height", "80px");
            dvTasks.Style.Add("height", "150px");
        }
        else if (gvSTL.Rows.Count == 1)
        {
            dvSTL.Style.Add("height", "55px");
            dvTasks.Style.Add("height", "175px");
        }
        else if (gvTasks.Rows.Count == 0)
        {
            dvTasks.Style.Add("height", "0px");
        }
    }

    /// <summary>
    /// Performs the Validation  and submittion of the task in Freeway
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // If the page is a result of roundtrip and not displayed 1st time as well 
        // the page is a result of the save click then
        try
        {
            if (IsPostBack && hdnsaveclick.Value == "Clicked")
            {

                foreach (GridViewRow grv in gvSTL.Rows)
                {
                    if (grv.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox RowLevelCheckBox = ((CheckBox)(grv.FindControl("RowLevelCheckBox")));
                        Label lblSourceLanguageID = ((Label)(grv.FindControl("lblSourceLanguageID")));
                        Label lblTargetLanguageID = ((Label)(grv.FindControl("lblTargetLanguageID")));
                        if (RowLevelCheckBox.Checked)
                            FreewayConfiguration.AddTask(lblstaticprojectID.Text, lblSourceLanguageID.Text, lblTargetLanguageID.Text, ddlComponents.SelectedValue, ddlSubjects.SelectedValue, ddlTasks.SelectedValue, ddlSubtasks.SelectedValue, Convert.ToDouble(txtVolume.Text), ddlUOM.SelectedValue);
                        RowLevelCheckBox.Checked = false;
                        CheckBox HeaderLevelCheckBox = ((CheckBox)(gvSTL.HeaderRow.FindControl("HeaderLevelCheckBox")));
                        HeaderLevelCheckBox.Checked = false;
                    }

                }
                loadTasks();
                hdnsaveclick.Value = string.Empty;
                //ddlSourceLanguages.SelectedValue = "Select";
                //ddlTargetLanguages.SelectedValue = "Select";
                ddlTasks.SelectedValue = "Select";
                ddlComponents.SelectedValue = "Select";
                ddlSubjects.SelectedValue = "Select";
                ddlSubtasks.SelectedValue = "Select";
                txtVolume.Text = string.Empty;
                ddlUOM.SelectedValue = "Select";
            }

            // Page header style
            SiteAPI refSiteApi = new SiteAPI();

            txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("Add Task");

            StringBuilder sb = new StringBuilder();

            sb.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
            sb.Append("<td>");

            sb.Append(@"<table><tr>");
            sb.Append("<td>");

            sb.Append(m_refStyle.GetButtonEventsWCaption(refSiteApi.AppImgPath + "btn_update-nm.gif", "#", "Save Task",
                    m_refMsg.GetMessage("btn save"), @"Onclick=""javascript:return SubmitForm('Workarea_Freeway_FreewayAddViewTask', 'VerifyForm()');"""));


            sb.Append("</td>");

            sb.Append("</tr></table>");

            htmToolBar.InnerHtml = sb.ToString();


        }
        catch (Exception ex)
        {
            string alertmessage = "javascript:alert('For Project Id - " + Convert.ToString(Session["projectid"]) + " unhandled exception is raised. ');";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert Message", alertmessage, true);
        }


        return;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSourceLanguages_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Bind the Target Languages
        //ddlTargetLanguages.Items.Clear();
        //ddlTargetLanguages.Items.Add(new ListItem("Select", "Select"));
        //foreach (Language vojoLanguage in FreewayConfiguration.TargetLanguages(ddlSourceLanguages.SelectedValue))
        //    ddlTargetLanguages.Items.Add(new ListItem(vojoLanguage.Description, vojoLanguage.ID));
    }
    protected void ddlTasks_SelectedIndexChanged(object sender, EventArgs e)
    {
        LB.FreewayLib.Freeway.VojoTask vojoTask = (LB.FreewayLib.Freeway.VojoTask)FreewayConfiguration.Tasks[ddlTasks.SelectedValue];

        ////Bind the SubTasks
        ddlSubtasks.Items.Clear();
        ddlSubtasks.Items.Add(new ListItem("Select", "Select"));
        foreach (LB.FreewayLib.Freeway.VojoSubTask subTask in FreewayConfiguration.SubTasks(vojoTask))
            ddlSubtasks.Items.Add(new ListItem(subTask.Description, subTask.ID));

        ////Bind the UOMs
        ddlUOM.Items.Clear();
        ddlUOM.Items.Add(new ListItem("Select", "Select"));
        foreach (LB.FreewayLib.Freeway.VojoUOM uom in FreewayConfiguration.Uoms(vojoTask))
            ddlUOM.Items.Add(new ListItem(uom.Description, uom.ID));
    }
    protected void gvTasks_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        loadTasks();
        gvTasks.PageIndex = e.NewPageIndex;
        gvTasks.DataBind();
    }
    protected void gvTasks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }
    #endregion
}
