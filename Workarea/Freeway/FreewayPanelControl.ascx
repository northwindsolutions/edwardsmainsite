<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FreewayPanelControl.ascx.cs" Inherits="FreewayPanelControl" %>

<div style="float:right;width:233px;text-align:center;">
    <asp:Panel ID="_createProjectPanel" runat="server">
        <asp:HyperLink ID="_createProjectLink" runat="server" NavigateUrl="javascript://">Send To Freeway</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="_dashboardLink" runat="server">
        <a href="javascript://" onclick="javascript:EkTbWebMenuPopUpWindow('/CMS400Demo/WorkArea/workarea.aspx?page=freeway/freewaydashboard.aspx&amp;action=View&amp&amp;TreeVisible=module&amp;LangType=1033', 'Admin400', 790,580, 1, 1);return false;">Freeway Dashboard</a>
    </asp:Panel>
    <asp:ListBox ID="_ids" runat="server"></asp:ListBox><br />
    <a href="http://freeway.lionbridge.com" target="_blank"><asp:Image ID="_freewayLogo" runat="server" ImageUrl="~/Workarea/images/freeway_logo_color150pixels.jpg" AlternateText="Freeway Logo" /></a>
</div>
