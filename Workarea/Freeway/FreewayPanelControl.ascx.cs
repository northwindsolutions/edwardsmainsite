#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : Freeway Panel control
---------------------------------
*/
#endregion
#region using
using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.Controls;
using Ektron.Cms.API;
#endregion
#region FreewayPanelControl
public partial class FreewayPanelControl : System.Web.UI.UserControl
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
	{
		CommonApi commonApi = new CommonApi();

		Visible = !(commonApi.RequestInformationRef.IsMembershipUser != 0 || commonApi.RequestInformationRef.UniqueId == 0);

		return;
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		List<long> contentIds = new List<long>();

		if (Request.QueryString["id"] != null)
		{
			try
			{
				contentIds.Add(int.Parse(Request.QueryString["id"]));
			}
			catch { }
		}
		else
		{
			List<Control> controls = new List<Control>();

			if (Page.Master != null)
				fillControlList(Page.Master, controls);
			else
				fillControlList(Page, controls);

			foreach (Control control in controls)
				if (control is ContentBlock)
				{
					contentIds.Add((control as ContentBlock).DefaultContentID);

					_ids.Items.Add((control as ContentBlock).DefaultContentID.ToString());
				}

#if false
		foreach (Control control in controls)
			_ids.Items.Add(control.GetType().FullName);
#endif
		}

		if (contentIds.Count > 0)
			Session[FreewayConfiguration.LocalizationContentIdSessionName] = contentIds;
		else
			Session[FreewayConfiguration.LocalizationContentIdSessionName] = null;

#if true
		_ids.Visible = false;
#else
		if (_ids.Items.Count == 0)
			_ids.Visible = false;
#endif

		CommonApi commonApi = new CommonApi();

		_createProjectLink.Enabled = commonApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.EditAlias);
		_dashboardLink.Enabled = commonApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminXliff);
		
		if (contentIds.Count > 0)
		{
			List<string> stringIds = new List<string>();

			foreach (long id in contentIds)
				stringIds.Add(id.ToString());

			_createProjectLink.Attributes.Add("onclick",
				string.Format("javascript:EkTbWebMenuPopUpWindow('/CMS400Demo/WorkArea/workarea.aspx?page=freeway/freewayprojectcreation.aspx&amp;CIDs={0}&amp;action=View&amp&amp;TreeVisible=module&amp;LangType=1033', 'Admin400', 790,580, 1, 1);return false;",
				string.Join(",", stringIds.ToArray())));
		}

		return;
    }
    #endregion

    #region Methods
    /// <summary>
    /// Add comment here.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _sendToFreeway_Click(object sender, EventArgs e)
	{
		return;
	}
    /// <summary>
    /// To populate controllist 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="controls"></param>
	public void fillControlList(Control control, List<Control> controls)
	{
		foreach (Control childControl in control.Controls)
			fillControlList(childControl, controls);

		controls.Add(control);

		return;
    }
    #endregion 
}
#endregion