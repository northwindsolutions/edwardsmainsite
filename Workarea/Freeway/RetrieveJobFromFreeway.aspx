<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RetrieveJobFromFreeway.aspx.cs" Inherits="Freeway_RetrieveJobFromFreeway" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Job Title: <asp:Label ID="_jobTitle" runat="server"></asp:Label><br />
        Job Id: <asp:Label ID="_jobId" runat="server"></asp:Label><br />
        Submitted When: <asp:Label ID="_submitDate" runat="server"></asp:Label><br />
        <table>
            <tr>
                <td><div style="border:solid 1px;"><asp:CheckBoxList ID="_fileList" runat="server"></asp:CheckBoxList></div></td>
            </tr>
            <tr align="right">
                <td><asp:Button ID="_cancel" runat="server" Text="Cancel" OnClick="_cancel_Click" width="80px"/><asp:Button ID="_retrieve" runat="server" Text="Retrieve" OnClick="_retrieve_Click" width="80px" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
