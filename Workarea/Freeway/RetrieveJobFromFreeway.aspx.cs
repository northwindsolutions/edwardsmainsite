#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : Retrieve Job From Freeway
---------------------------------
*/
#endregion
#region using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using DS = Ektron.Cms.LocalizationJobDataSet;
using System.Globalization;
#endregion
#region Freeway_RetrieveJobFromFreeway
public partial class Freeway_RetrieveJobFromFreeway : System.Web.UI.Page
{
    #region Objects
    SiteAPI siteApi = new SiteAPI();
	LocalizationAPI locApi = new LocalizationAPI();
    #endregion
    #region Page event
    protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			bool canRetrieve = false;

			LocalizationJobDataSet.LocalizationJobRow jobRow = locApi.GetJobByID(JobId);

			_jobTitle.Text = jobRow.JobTitle;
			_jobId.Text = jobRow.JobID.ToString();
			_submitDate.Text = jobRow.StartTime.ToString();

			EktronProject = new EktronProject(JobId);

            EktronProject.GetFilesStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

			foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
			{
				CultureInfo sourceCulture = new CultureInfo(file.SourceLang);
				CultureInfo targetCulture = new CultureInfo(file.TargetLang[0]);

				ListItem item = new ListItem(string.Format("{0} to {1}", sourceCulture.DisplayName, targetCulture.DisplayName));

				item.Value = file.Id;
				item.Enabled = file.Ready;
				if (!item.Enabled)
					item.Text = string.Format("{0} (File Not Ready)", item.Text);
				else
					canRetrieve = true;
				item.Selected = item.Enabled;
				_fileList.Items.Add(item);
			}

			_retrieve.Enabled = canRetrieve;
		}

		return;
    }
    #endregion
    #region Public properties
    public int JobId
	{
		get
		{
			try
			{
				return int.Parse(Page.Request.QueryString["jobid"]);
			}
			catch
			{
				return 0;
			}
		}
	}

	public string BackUrl
	{
		get
		{
			try
			{
				return Page.Request.QueryString["backurl"];
			}
			catch
			{
				return string.Empty;
			}
		}
	}

	public EktronProject EktronProject
	{
		get { return this.Session["EktronProject"] as EktronProject; }
		set { this.Session["EktronProject"] = value; }
	}

	public int ProjectId
	{
		get { return (int)this.ViewState["ProjectId"]; }
		set { this.ViewState["ProjectId"] = value; }
    }
    #endregion 
#region Methods
    /// <summary>
    /// To redirect to previous page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _cancel_Click(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(BackUrl))
			Response.Redirect(BackUrl);

		return;
	}
    /// <summary>
    /// retrieve Jobs 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
	protected void _retrieve_Click(object sender, EventArgs e)
	{
		bool fileProceeded = false;

		if (EktronProject != null)
		{
			EktronProject.ImportFiles(_fileList.Items, true);

			if (!string.IsNullOrEmpty(BackUrl))
				Response.Redirect(BackUrl);
		}

		return;
	}
#endregion
}
#endregion