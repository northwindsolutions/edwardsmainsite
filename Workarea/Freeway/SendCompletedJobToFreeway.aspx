<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendCompletedJobToFreeway.aspx.cs"
    Inherits="Freeway_SendCompletedJobToFreeway" %>

<%@ Register Src="SendToFreewayControl.ascx" TagName="SendToFreewayControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="pnlAddproject" runat="server" BorderStyle="solid" BorderColor="black"
                BorderWidth="1px" Height="335px">
                <table width="100%"  cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 100%; background-color: Gray; color: White;">
                            <table>
                                <tr class="ektronToolbar">
                                    <td style="width: 23%;">
                                    </td>
                                    <td style="width: 8%;">
                                    </td>
                                    <td align="right" style="width: 69%;">
                                        <asp:Label ID="lblFreewayDetails" Font-Size="11px" Font-Names="verdana" runat="server"> </asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   
                    <tr>
                        <td colspan="2" style="padding:7px 7px 7px 7px;font-family:Verdana;">
                            <fieldset style="border: solid 1px Black; padding-left: 5px; vertical-align:middle;">
                                <legend style="font-size:10pt; ">Add files to the project</legend>
                                <table>
                                <tr>
                                        <td style="width: 590px;height:10px;" colspan="2"></td>
                                        </tr>
                                        
                                    <tr>
                                        <td style="width: 590px" colspan="2">
                                            <asp:Label ID="_lbljobTitle" runat="server" Text="Job Title: " ForeColor="#2e6e9e"
                                                Font-Size="8pt" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="_jobTitle" Font-Size="8pt" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 590px" colspan="2">
                                           <%-- <asp:Label ID="_lbljobId" runat="server" Text="Job Id: " ForeColor="#2e6e9e" Font-Size="8pt"
                                                Font-Bold="true"></asp:Label>
                                            <asp:Label ID="_jobId" Font-Size="8pt" runat="server"></asp:Label>--%>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width: 590px" colspan="2">
                                            <uc1:SendToFreewayControl ID="_sendToFreewayControl" runat="server" />
                                        </td>
                                    </tr>                                  
                                    <tr>
                                        <td style="width: 590px" colspan="2">
                                            <div>
                                                <asp:Label ID="lblFRTerLang" runat="server" Text="Freeway Target Language(s)" ForeColor="#2e6e9e"
                                                    Font-Size="8pt" Font-Bold="true"></asp:Label></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 340px" align="left">
                                            <asp:CheckBoxList ID="_fileList" runat="server" Font-Names="Verdana" Font-Size="8pt">
                                            </asp:CheckBoxList>
                                        </td>
                                        <td>
                                        
                                            <%-- <table >
                        <tr>
                            <td style=" width:96%" align = "left">
                               <asp:Label CssClass="text" ID="Label1" runat="server" Text="&nbsp;&nbsp;<b>Powered by:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Font-Names="Verdana" Font-Size="8pt"></asp:Label> <br/>
                            </td>
                        </tr>
                        <tr>
                            <td style=" width:96%" align = "right">
                               <asp:Image ID="Image2" runat="server" ImageUrl="~/Workarea/Freeway/images/freeway_logo_color150pixels.jpg" /></td>
                        </tr>
                    </table>--%>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td style="width: 590px;height:10px;" colspan="2"></td>
                                        </tr>                                  
                                    <tr>
                                        <td align="left" colspan="2">
                                            <asp:Button ID="_cancel" runat="server" Text="Cancel" OnClick="_cancel_Click" Width="70px"
                                                Font-Size="8pt" Font-Names="Vardana" />
                                            <asp:Button ID="_submitfile" runat="server" Text="Add to Project" OnClick="_submitfile_Click"
                                                Width="120px" Font-Size="8pt" Font-Names="Vardana" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 590px;height:10px;" colspan="2"></td>
                                        </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
