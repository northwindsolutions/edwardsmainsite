﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="WorkAreaTrees" CodeFile="WorkAreaTrees.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>WorkAreaTrees</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
    <script type="text/javascript">
		var ContentUrl = "";
		var FrameName = "<asp:Literal id="frameName" runat="server" />";
		var rootFolderName;
		var rootTaxonomyName;
		var rootCollectionName;
		var rootMenuName;
		var treesLoaded = 0;
		var g_selectedFolderList = "<asp:Literal id="selectedFolderList" runat="server" />";
		var __EkFolderId = "-1";
        var __TaxonomyOverrideId = 0;
        var AccordionIndex = "<asp:Literal id="szAccordionIndex" runat="server" />";

		switch (FrameName)
		{
			case("Library"):
			{
				rootFolderName = "<asp:Literal id="genericLibraryTitle" runat="server" />";
				break;
			}
			default:
			{
				rootFolderName = "<asp:Literal id="genericContentTitle" runat="server" />";
				rootTaxonomyName = "<asp:Literal id="labelTaxonomies" runat="server" />";
				rootCollectionName = "<asp:Literal id="genericCollectionName" runat="server" />";
				rootMenuName = "<asp:Literal id="genericMenuTitle" runat="server" />";
			}
		}
    </script>
    <script type="text/javascript">
        //Debug.LEVEL = LogUtil.ALL;
        //LogUtil.logType = LogUtil.LOG_CONSOLE;
        $ektron().ready(function()
        {
            $ektron("#accordion").accordion({ fillSpace: true});

            // initialize the trees
            Main.start();
            displayTree();
            showSelectedFolderTree();

            $ektron("#accordion").accordion('activate', parseInt(AccordionIndex));

            // initialize context menu AppPath property for the context menus
//            Ektron.ContextMenu.AppPath = '<asp:Literal id="jsAppPath" runat="server" />';
//            Ektron.ContextMenu.Folder.confirmFolderDelete = '<asp:Literal id="jsConfirmFolderDelete" runat="server" />';
//            Ektron.ContextMenu.Collections.confirmCollectionDelete = '<asp:Literal id="jsConfirmCollectionDelete" runat="server" />';
//            Ektron.ContextMenu.Menus.confirmMenuDelete = '<asp:Literal id="jsConfirmMenuDelete" runat="server" />';

            // whenever the CMSAPIAjaxComplete event fires,
            // call the contesxt menu Init again to bind to new elements
            $ektron().bind("CMSAPIAjaxComplete", function()
            {
                Ektron.ContextMenu.Init();
            });
        });

        $ektron.addLoadEvent(function()
        {
            $(window).resize(function(){
		        return adjustAccordionHeight();
            });
            adjustAccordionHeight();
        });

        function adjustAccordionHeight()
        {
            if ($ektron.browser.msie)
            {
                // IE fires multiple resize events and messes up the height of things as we redraw.
                // Force it to place nice.
                $ektron("form#Form1").height($ektron("body").height()).css("zoom", "1");
                $ektron("#accordion").accordion("resize");
            }
            else
            {
                $ektron("#accordion").accordion("resize");
            }
            return false;
        }
    </script>
    <style type="text/css">
        * {
	        margin: 0;
	        padding: 0;
        }

        html, body {
            background: #3f3f3f;
            width:100%;
	        height:100%;
	        overflow: hidden;
        }

    form#Form1 {display: block; position: absolute; top: -1px; left: 0; right: 0; bottom: 0; overflow:hidden; border-right: solid 1px #000000; height: 100%;}
    </style>
</head>
<body>
    <!-- Folder Context Menu -->
    <ul id="folderContextMenu" class="ektronContextMenu Menu">
        <li class="addFolder">
            <a href="#addFolder"><asp:Literal ID="folderContextAddFolder" runat="server" /></a>
        </li>
        <li class="addBlogFolder">
            <a href="#addBlogFolder"><asp:Literal ID="folderContextAddBlogFolder" runat="server" /></a>
        </li>
        <li class="addBoardFolder">
            <a href="#addBoardFolder"><asp:Literal ID="folderContextAddDiscussionBoard" runat="server" /></a>
        </li>
        <li class="addCommunityFolder">
            <a href="#addCommunityFolder"><asp:Literal ID="folderContextAddCommunityFolder" runat="server" /></a>
        </li>
        <li class="addCalendarFolder">
            <a href="#addCalendarFolder"><asp:Literal ID="folderContextAddCalendarFolder" runat="server" /></a>
        </li>
        <li class="addEcommerceFolder">
            <a href="#addEcommerceFolder"><asp:Literal ID="folderContextAddEcommerceFolder" runat="server" /></a>
        </li>
        <li class="addSiteFolder">
            <a href="#addSiteFolder"><asp:Literal ID="folderContextAddSiteFolder" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewFolderProperties"><asp:Literal ID="folderContextViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteFolder">
            <a href="#deleteFolder"><asp:Literal ID="folderContextDeleteFolder" runat="server" /></a>
        </li>
        <li class="deleteFolderContent">
            <a href="#deleteFolderContent"><asp:Literal ID="folderContextDeleteFolderContent" runat="server" /></a>
        </li>
    </ul>
    <!-- End Folder Context Menu -->
    <!-- Site Folder Context Menu -->
    <ul id="siteFolderContextMenu" class="ektronContextMenu Menu">
        <li class="addFolder">
            <a href="#addFolder"><asp:Literal ID="siteFolderContextAddFolder" runat="server" /></a>
        </li>
        <li class="addBlogFolder">
            <a href="#addBlogFolder"><asp:Literal ID="siteFolderContextAddBlogFolder" runat="server" /></a>
        </li>
        <li class="addBoardFolder">
            <a href="#addBoardFolder"><asp:Literal ID="siteFolderContextAddDiscussionBoard" runat="server" /></a>
        </li>
        <li class="addCommunityFolder">
            <a href="#addCommunityFolder"><asp:Literal ID="siteFolderContextAddCommunityFolder" runat="server" /></a>
        </li>
        <li class="addCalendarFolder">
            <a href="#addCalendarFolder"><asp:Literal ID="siteFolderContextAddCalendarFolder" runat="server" /></a>
        </li>
        <li class="addEcommerceFolder">
            <a href="#addEcommerceFolder"><asp:Literal ID="siteFolderContextAddEcommerceFolder" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewFolderProperties"><asp:Literal ID="siteFolderContextViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteSiteFolder">
            <a href="#deleteFolder"><asp:Literal ID="siteFolderContextDeleteFolder" runat="server" /></a>
        </li>
        <li class="deleteFolderContent">
            <a href="#deleteFolderContent"><asp:Literal ID="siteFolderContextDeleteFolderContent" runat="server" /></a>
        </li>
    </ul>
    <!-- End Site Folder Context Menu -->
    <!-- Blog Folder Context Menu -->
    <ul id="blogFolderContextMenu" class="ektronContextMenu Menu">
        <li class="viewProperties">
            <a href="#viewFolderProperties"><asp:Literal ID="blogFolderContextViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteBlogFolder">
            <a href="#deleteFolder"><asp:Literal ID="blogFolderContextDeleteBlog" runat="server" /></a>
        </li>
        <li class="deleteBlogContent">
            <a href="#deleteFolderContent"><asp:Literal ID="blogFolderContextDeleteBlogPosts" runat="server" /></a>
        </li>
    </ul>
    <!-- End Blog Folder Context Menu -->
    <!-- Community Folder Context Menu -->
    <ul id="communityFolderContextMenu" class="ektronContextMenu Menu">
        <li class="addBlogFolder">
            <a href="#addBlogFolder"><asp:Literal ID="communityFolderContextAddBlog" runat="server" /></a>
        </li>
        <li class="addBoardFolder">
            <a href="#addBoardFolder"><asp:Literal ID="communityFolderContextAddBoard" runat="server" /></a>
        </li>
        <li class="addCommunityFolder">
            <a href="#addCommunityFolder"><asp:Literal ID="communityFolderContextAddCommunityFolder" runat="server" /></a>
        </li>
        <li class="addCalendarFolder">
            <a href="#addCalendarFolder"><asp:Literal ID="communityFolderContextAddCalendarFolder" runat="server" /></a>
        </li>
        <li class="addEcommerceFolder">
            <a href="#addEcommerceFolder"><asp:Literal ID="communityFolderContextAddEcommerceFolder" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewFolderProperties"><asp:Literal ID="communityFolderContextViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteCommunityFolder">
            <a href="#deleteFolder"><asp:Literal ID="communityFolderContextDeleteFolder" runat="server" /></a>
        </li>
        <li class="deleteFolderContent">
            <a href="#deleteFolderContent"><asp:Literal ID="communityFolderContextDeleteFolderContent" runat="server" /></a>
        </li>
    </ul>
    <!-- End Community Folder Context Menu -->
    <!-- Disscussion Board Folder Context Menu -->
    <ul id="discussionBoardFolderContextMenu" class="ektronContextMenu Menu">
        <li class="addDiscussionForum">
            <a href="#addDiscussionForum"><asp:Literal ID="boardFolderContextAddDiscussionForum" runat="server" /></a>
        </li>
        <li class="addSubject">
            <a href="#addSubject"><asp:Literal ID="boardFolderContextAddSubject" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewBoardProperties"><asp:Literal ID="boardFolderContextViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteBoardFolder">
            <a href="#deleteFolder"><asp:Literal ID="boardFolderContextDeleteBoard" runat="server" /></a>
        </li>
    </ul>
    <!-- End Disscussion Board Folder Context Menu -->
    <!-- Disscussion Forum Folder Context Menu -->
    <ul id="discussionForumFolderContextMenu" class="ektronContextMenu Menu">
        <li class="add">
            <a href="#addForumTopic"><asp:Literal ID="forumFolderContextAddTopic" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewForumProperties"><asp:Literal ID="forumFolderContextViewProperties" runat="server" /></a>
        </li>
        <li class="viewPermissions">
            <a href="#viewForumPermissions"><asp:Literal ID="forumFolderContextViewPermissions" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteBoardFolder">
            <a href="#deleteForum"><asp:Literal ID="forumFolderContextDeleteForum" runat="server" /></a>
        </li>
    </ul>
    <!-- End Disscussion Forum Folder Context Menu -->
    <!-- Ecommerce Folder Context Menu -->
    <ul id="ecommerceFolderContextMenu" class="ektronContextMenu Menu">
        <li class="addEcommerceFolder">
            <a href="#addEcommerceFolder"><asp:Literal ID="ecommerceContentAddFolder" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewFolderProperties"><asp:Literal ID="ecommerceContentViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteEcommerceFolder">
            <a href="#deleteFolder"><asp:Literal ID="ecommerceContentDeleteFolder" runat="server" /></a>
        </li>
        <li class="deleteEcommerceContent">
            <a href="#deleteFolderContent"><asp:Literal ID="ecommerceContextDeleteContent" runat="server" /></a>
        </li>
    </ul>
    <!-- End Ecommerce Folder Context Menu -->
    <!-- Calendar Context Menu -->
    <ul id="calendarFolderContextMenu" class="ektronContextMenu Menu">
        <li class="viewProperties">
            <a href="#viewFolderProperties"><asp:Literal ID="calendarViewProperties" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="deleteCalendarFolder">
            <a href="#deleteFolder"><asp:Literal ID="calendarDeleteFolder" runat="server" /></a>
        </li>
    </ul>
    <!-- End Site Folder Context Menu -->
    <!-- Collections Context Menu -->
    <ul id="collectionsContextMenu" class="ektronContextMenu Menu">
        <li class="add">
            <a href="#addCollection"><asp:Literal ID="collectionContextAddCollection" runat="server" /></a>
        </li>
        <li class="addContent">
            <a href="#addCollectionItems"><asp:Literal ID="collectionContextAdd" runat="server" /></a>
        </li>
        <li class="remove">
            <a href="#removeCollection"><asp:Literal ID="collectionContextRemove" runat="server" /></a>
        </li>
        <li class="reorder">
            <a href="#reorderCollection"><asp:Literal ID="collectionContextReorder" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewCollectionProperties"><asp:Literal ID="collectionContextView" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="delete">
            <a href="#deleteCollection"><asp:Literal ID="collectionContextDelete" runat="server" /></a>
        </li>
    </ul>
    <!-- End Collections Folder Context Menu -->
    <!-- Taxonomy Context Menu -->
    <ul id="taxonomyContextMenu" class="ektronContextMenu Menu">
        <li class="add">
            <a href="#addTaxonomy"><asp:Literal ID="taxonomyAdd" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewTaxonomyProperties"><asp:Literal ID="taxonomyContextView" runat="server" /></a>
        </li>
        <li class="assignItems">
            <a href="#taxonomyAddContent"><asp:Literal ID="taxonomyAddContent" runat="server" /></a>
        </li>
        <li class="assignFolders">
            <a href="#taxonomyAddFolders"><asp:Literal ID="taxonomyAddFolder" runat="server" /></a>
        </li>
    </ul>
    <!-- End Taxonomy Folder Context Menu -->
    <!-- Menus Context Menu -->
    <ul id="menutreeContextMenu" class="ektronContextMenu Menu">
        <li class="add">
            <a href="#addMenu"><asp:Literal ID="menuAdd" runat="server" /></a>
        </li>
        <li class="addContent">
            <a href="#addMenuItems"><asp:Literal ID="menuContentAdd" runat="server" /></a>
        </li>
        <li class="remove">
            <a href="#removeMenuItems"><asp:Literal ID="menuRemoveItems" runat="server" /></a>
        </li>
        <li class="reorder">
            <a href="#reorderMenu"><asp:Literal ID="menuContextReorder" runat="server" /></a>
        </li>
        <li class="viewProperties">
            <a href="#viewMenuProperties"><asp:Literal ID="menuContextView" runat="server" /></a>
        </li>
        <li class="separator"></li>
        <li class="delete">
            <a href="#deleteMenu"><asp:Literal ID="menuContextDelete" runat="server" /></a>
        </li>
    </ul>
    <!-- End Menus Context Menu -->
    <form id="Form1" method="post" runat="server">
        <div id="accordion">
            <h3>
                <a href="#"><%=m_refMsgApi.GetMessage("generic content title") %></a>
            </h3>
            <div>
                <div id="TreeOutput" class="ektronTreeContainer">
                </div>
            </div>
            <asp:Placeholder ID="plContentTrees" Visible="false" runat="server">
            <asp:PlaceHolder ID="plTaxonomyTree" runat="server">
            <h3>
                <a href="#"><%=m_refMsgApi.GetMessage("lbl taxonomies") %></a>
            </h3>
            <div>
                <div id="TaxonomyTreeOutput" class="ektronTreeContainer">
                </div>
            </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="plCollectionTree" runat="server">
            <h3>
                <a href="#"><%=m_refMsgApi.GetMessage("generic collection title")%></a>
            </h3>
            <div>
                <div id="CollectionTreeOutput" class="ektronTreeContainer">
                </div>
            </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="plmenuTree" runat="server">
            <h3>
                <a href="#"><%=m_refMsgApi.GetMessage("generic menu title")%></a>
            </h3>
            <div>
                <div id="MenuTreeOutput" class="ektronTreeContainer">
                </div>
            </div>
            </asp:PlaceHolder>
            </asp:Placeholder>
        </div>
        <input type="hidden" id="folderName" name="folderName" />
        <input type="hidden" id="selected_folder_id" name="selected_folder_id" value="0" />
        <input type="hidden" id="contLanguage" name="contLanguage" runat="server" />
    </form>
</body>
</html>
