#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : WorkArea Trees
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization
Imports Ektron.Cms
Imports Ektron.Cms.Common.EkEnumeration
Imports System.Collections.Generic
Imports Ektron.Cms.Common
#End Region
#Region "WorkAreaTrees Class"
Partial Class WorkAreaTrees
    Inherits System.Web.UI.Page

#Region "Protected Objects"
    Protected m_refContentApi As ContentAPI
    Protected m_refMsgApi As Common.EkMessageHelper
    Protected m_refApi As New CommonApi
#End Region
#Region "Protected Variables"
    Protected m_selectedFolderList As String = ""
    Protected ContentLanguage As Integer
    Protected m_maxTreeTopNodes As Integer = 999999
#End Region
#Region "Page Event"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1

        If (IsNothing(m_refContentApi)) Then
            m_refContentApi = New ContentAPI
        End If

        Utilities.SetLanguage(m_refContentApi)
        ContentLanguage = m_refContentApi.ContentLanguage
        contLanguage.Value = m_refContentApi.ContentLanguage

        m_refMsgApi = m_refContentApi.EkMsgRef
        If Not Request.QueryString("method") Is Nothing Then
            If isInCallbackMode() Then Return
        End If
        If (Not IsNothing(Request.QueryString("AutoNav"))) Then
            m_selectedFolderList = getFolderList(Request.QueryString("AutoNav"))
        End If

        plContentTrees.Visible = Request.QueryString("Tree") = "Content"


        ' hide taxonomy tree if user doesn't have enough privileges
        'Dim security_data As PermissionData = m_refContentApi.LoadPermissions(0, "content")
        'If (Not (security_data.IsAdmin Or _
        '         m_refContentApi.IsARoleMember(Common.EkEnumeration.CmsRoleIds.TaxonomyAdministrator))) Then


        plTaxonomyTree.Visible = False
        plCollectionTree.Visible = False
        plmenuTree.Visible = False


        'End If

        Dim visibletree As String = Request.QueryString("TreeVisible").ToLower()
        If (visibletree = "menu") Then
            szAccordionIndex.Text = "3"
        ElseIf (visibletree = "collection") Then
            szAccordionIndex.Text = "2"
        ElseIf (visibletree = "taxonomy") Then
            szAccordionIndex.Text = "1"
        Else
            szAccordionIndex.Text = "0"
        End If

        assignTextValues()
        registerResources()
    End Sub

#End Region
#Region "Methods"
    ''' <summary>
    ''' To Get Folder List
    ''' </summary>
    ''' <param name="FolderPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getFolderList(ByVal FolderPath As String) As String
        Dim result As String = "0"
        Dim contObj As Ektron.Cms.Content.EkContent = m_refContentApi.EkContentRef
        Dim folderId As Long

        If (FolderPath.Length > 0) Then
            folderId = contObj.GetFolderID(FolderPath)
            If (folderId >= 0) Then
                result = contObj.GetFolderParentFolderIdRecursive(folderId)
            End If
        End If
        contObj = Nothing
        Return result
    End Function
    ''' <summary>
    ''' To check Is In Callback Mode
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function isInCallbackMode() As Boolean
        If Not Request.QueryString("method") Is Nothing Then
            'The following line added to support response to read as responseXml
            'Remove this if you want to read responseText
            Response.ContentType = "text/xml"
            Response.Write(raiseCallbackEvent())
            Response.Flush()
            Response.End()
            Return True
        End If
        Return False
    End Function
    ' ****************************************************************************************************
    ' Implement the callback interface
    Private Function raiseCallbackEvent() As String
        Dim result As String = ""
        Dim folder_arr_data As FolderData()
        Dim folder_data As FolderData
        Dim m_intId As Long
        Try
            ' handle language switch if needed
            Dim LangId As Integer = -1
            If (Request.Params.Item("langid") IsNot Nothing AndAlso IsNumeric(Request.Params.Item("langid"))) Then
                LangId = Convert.ToInt32(Request.Params.Item("langid"))
                If (LangId <> -99) Then
                    m_refContentApi.SetCookieValue("LastValidLanguageID", LangId)
                    m_refContentApi.ContentLanguage = LangId
                    m_refApi.ContentLanguage = LangId
                    'm_refApi.EkContentRef.RequestInformation.ContentLanguage = LangId
                End If
            End If
            If (Request.QueryString("method").ToLower = "get_folder") Then
                m_intId = Convert.ToInt64(Request.Params.Item("id"))
                folder_data = m_refContentApi.GetFolderDataWithPermission(m_intId) 'GetFolderById(m_intId)
                folder_data.XmlConfiguration = Nothing
                result = serializeAsXmlData(folder_data, folder_data.GetType)
            ElseIf (Request.QueryString("method").ToLower = "get_child_folders") Then
                m_intId = Convert.ToInt64(Request.Params.Item("folderid"))
                folder_arr_data = m_refContentApi.GetChildFolders(m_intId, False, FolderOrderBy.Name)
                'when there are no folders in the content tree like CMS400min
                If folder_arr_data IsNot Nothing Then
                    result = serializeAsXmlData(folder_arr_data, folder_arr_data.GetType)
                End If
            ElseIf (Request.QueryString("method").ToLower = "get_child_category") Then
                Dim m_refContent As Ektron.Cms.Content.EkContent
                Dim TaxFolderId As Long = -1
                If (Request.Params.Item("folderid") IsNot Nothing) Then
                    TaxFolderId = Convert.ToInt64(Request.Params.Item("folderid"))
                End If
                Dim TaxOverrideId As Long = -1
                If (Request.Params.Item("taxonomyoverrideid") IsNot Nothing) Then
                    TaxOverrideId = Convert.ToInt64(Request.Params.Item("taxonomyoverrideid"))
                End If
                Dim TaxLangId As Long = -99
                If (Request.Params.Item("langid") IsNot Nothing) Then
                    If (Request.Params.Item("langid") <> "undefined") Then
                        TaxLangId = Convert.ToInt64(Request.Params.Item("langid"))
                    End If
                End If

                Dim taxonomy_request As New TaxonomyRequest
                Dim taxonomy_data_arr As TaxonomyBaseData() = Nothing
                Utilities.SetLanguage(m_refContentApi)
                m_refContent = m_refContentApi.EkContentRef
                If (TaxFolderId = -2) Then
                    taxonomy_data_arr = m_refContent.GetAllTaxonomyByConfig(TaxonomyType.Group)
                Else
                    m_intId = Convert.ToInt64(Request.Params.Item("taxonomyid"))
                    taxonomy_request.TaxonomyId = m_intId
                    If ((TaxFolderId > -1) AndAlso (TaxOverrideId <= 0) AndAlso (m_intId = 0)) Then
                        taxonomy_data_arr = m_refContent.GetAllFolderTaxonomy(TaxFolderId)
                    Else
                        If (TaxLangId <> -99) Then
                            taxonomy_request.TaxonomyLanguage = TaxLangId
                        Else
                            taxonomy_request.TaxonomyLanguage = m_refContentApi.ContentLanguage
                        End If
                        taxonomy_request.PageSize = m_maxTreeTopNodes    ' default of 0 used to mean "everything" but storedproc changed
                        taxonomy_data_arr = m_refContent.ReadAllSubCategories(taxonomy_request)
                    End If
                End If
                result = serializeAsXmlData(taxonomy_data_arr, taxonomy_data_arr.GetType)
            ElseIf (Request.QueryString("method").ToLower = "get_taxonomy") Then
                Dim m_refContent As Ektron.Cms.Content.EkContent
                Dim taxonomy_request As New TaxonomyRequest
                m_intId = Convert.ToInt64(Request.Params.Item("taxonomyid"))
                Utilities.SetLanguage(m_refContentApi)
                m_refContent = m_refContentApi.EkContentRef
                taxonomy_request.TaxonomyId = m_intId
                taxonomy_request.TaxonomyLanguage = m_refContentApi.ContentLanguage
                Dim taxonomy_data As TaxonomyData = m_refContent.ReadTaxonomy(taxonomy_request)
                If taxonomy_data IsNot Nothing Then
                    result = serializeAsXmlData(taxonomy_data, taxonomy_data.GetType)
                End If
            ElseIf (Request.QueryString("method").ToLower = "get_taxonomies") Then
                Dim request As New TaxonomyRequest
                request.TaxonomyId = 0
                Utilities.SetLanguage(m_refContentApi)
                request.TaxonomyLanguage = m_refContentApi.ContentLanguage
                request.PageSize = m_maxTreeTopNodes
                request.CurrentPage = 1
                Dim taxonomy_data As TaxonomyBaseData() = m_refApi.EkContentRef.ReadAllSubCategories(request)
                result = serializeAsXmlData(taxonomy_data, taxonomy_data.GetType)
            ElseIf (Request.QueryString("method").ToLower = "get_collections") Then
                Dim request As New PageRequestData
                request.PageSize = m_maxTreeTopNodes
                request.CurrentPage = 1
                Dim collection_list As CollectionListData() = m_refApi.EkContentRef.GetCollectionList("", request)
                result = serializeAsXmlData(collection_list, collection_list.GetType)
            ElseIf (Request.QueryString("method").ToLower = "get_menus") Then
                Dim request As New PageRequestData
                request.PageSize = m_maxTreeTopNodes
                request.CurrentPage = 1
                Dim menus As Collection = m_refApi.EkContentRef.GetMenuReport("", request)
                Dim menuList As List(Of AxMenuData) = getMenuList(menus)
                result = serializeAsXmlData(menuList, menuList.GetType)
            ElseIf (Request.QueryString("method").ToLower = "get_submenus") Then
                m_intId = Convert.ToInt64(Request.Params.Item("menuid"))
                Dim items As List(Of AxMenuData) = m_refApi.EkContentRef.GetSubMenus(m_intId, m_refContentApi.ContentLanguage)
                result = serializeAsXmlData(items, items.GetType)
            End If
        Catch ex As Exception
            EkException.LogException(ex)
            result = ""
        End Try
        Return result
    End Function

    ''' <summary>
    ''' To Get Menu List
    ''' </summary>
    ''' <param name="menus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getMenuList(ByVal menus As Collection) As List(Of AxMenuData)
        Dim menuList As New List(Of AxMenuData)
        Dim menuData As AxMenuData
        For i As Integer = 1 To menus.Count
            menuData = New AxMenuData()
            menuData.ID = menus(i)("MenuId")
            menuData.FolderID = menus(i)("FolderId")
            menuData.Title = menus(i)("MenuTitle")
            menuData.Description = menus(i)("MenuDescription")
            menuData.HasChildren = menus(i)("HasChildren")
            menuData.ContentLanguage = menus(i)("ContentLanguage")
            menuData.ParentID = menus(i)("ParentID")
            menuData.ItemCount = menus(i)("ItemCount")
            menuData.AncestorID = menus(i)("AncestorID")
            menuData.Type = CMSMenuItemType.Menu 'TODO: Ross - Not sure if this is the correct value, but it is necessary to set some value for serialization
            menuList.Add(menuData)
        Next
        Return menuList
    End Function

    ''' <summary>
    ''' To Serialize As Xml Data
    ''' </summary>
    ''' <param name="data"></param>
    ''' <param name="datatype"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function serializeAsXmlData(ByVal data As Object, ByVal datatype As Type) As String
        Dim result As String = ""
        Dim XmlOutStream As New System.IO.MemoryStream
        Dim XmlSer As XmlSerializer
        Dim byteArr As Byte()
        Dim Utf8 As New System.Text.UTF8Encoding
        XmlSer = New XmlSerializer(datatype)
        XmlSer.Serialize(XmlOutStream, data)
        byteArr = XmlOutStream.ToArray
        result = System.Convert.ToBase64String(byteArr, 0, byteArr.Length)
        result = Utf8.GetString(Convert.FromBase64String(result))
        Return result
    End Function

    ''' <summary>
    ''' To Assign Text Values
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub assignTextValues()
        ' assign the various resource text strings
        frameName.Text = Request.QueryString("Tree")
        selectedFolderList.Text = m_selectedFolderList
        genericLibraryTitle.Text = m_refMsgApi.GetMessage("generic library title")
        genericContentTitle.Text = m_refMsgApi.GetMessage("generic content title")
        labelTaxonomies.Text = m_refMsgApi.GetMessage("lbl taxonomies")
        genericCollectionName.Text = m_refMsgApi.GetMessage("generic collection title")
        genericMenuTitle.Text = m_refMsgApi.GetMessage("generic menu title")

        ' folder context menu strings
        folderContextAddFolder.Text = m_refMsgApi.GetMessage("btn add folder")
        folderContextAddBlogFolder.Text = m_refMsgApi.GetMessage("btn add blog")
        folderContextAddDiscussionBoard.Text = m_refMsgApi.GetMessage("add discussion board")
        folderContextAddCommunityFolder.Text = m_refMsgApi.GetMessage("add community folder")
        folderContextAddCalendarFolder.Text = m_refMsgApi.GetMessage("add calendar folder")
        folderContextAddEcommerceFolder.Text = m_refMsgApi.GetMessage("btn add catalog")
        folderContextAddSiteFolder.Text = m_refMsgApi.GetMessage("add site")
        folderContextViewProperties.Text = m_refMsgApi.GetMessage("DmsMenuViewProperties")
        folderContextDeleteFolder.Text = String.Format(m_refMsgApi.GetMessage("delete x"), "<span class='triggerName'></span>")
        folderContextDeleteFolderContent.Text = String.Format(m_refMsgApi.GetMessage("delete content from x"), "<span class='triggerName'></span>")

        ' siteFolder context menu strings
        siteFolderContextAddFolder.Text = folderContextAddFolder.Text
        siteFolderContextAddBlogFolder.Text = folderContextAddBlogFolder.Text
        siteFolderContextAddDiscussionBoard.Text = folderContextAddDiscussionBoard.Text
        siteFolderContextAddCommunityFolder.Text = folderContextAddCommunityFolder.Text
        siteFolderContextAddEcommerceFolder.Text = folderContextAddEcommerceFolder.Text
        siteFolderContextViewProperties.Text = folderContextViewProperties.Text
        siteFolderContextDeleteFolder.Text = folderContextDeleteFolder.Text
        siteFolderContextDeleteFolderContent.Text = folderContextDeleteFolderContent.Text
        siteFolderContextAddCalendarFolder.Text = folderContextAddCalendarFolder.Text

        ' blog context menu strings
        blogFolderContextViewProperties.Text = folderContextViewProperties.Text
        blogFolderContextDeleteBlog.Text = folderContextDeleteFolder.Text
        blogFolderContextDeleteBlogPosts.Text = String.Format(m_refMsgApi.GetMessage("delete posts from x"), "<span class='triggerName'></span>")

        'community context menu strings
        communityFolderContextAddBlog.Text = folderContextAddBlogFolder.Text
        communityFolderContextAddBoard.Text = folderContextAddDiscussionBoard.Text
        communityFolderContextAddCommunityFolder.Text = folderContextAddCommunityFolder.Text
        communityFolderContextAddEcommerceFolder.Text = folderContextAddEcommerceFolder.Text
        communityFolderContextViewProperties.Text = folderContextViewProperties.Text
        communityFolderContextDeleteFolder.Text = folderContextDeleteFolder.Text
        communityFolderContextDeleteFolderContent.Text = folderContextDeleteFolderContent.Text
        communityFolderContextAddCalendarFolder.Text = folderContextAddCalendarFolder.Text

        ' discussion board menu strings
        boardFolderContextAddDiscussionForum.Text = m_refMsgApi.GetMessage("lbl add discussion forum")
        boardFolderContextAddSubject.Text = m_refMsgApi.GetMessage("lnk add new subject")
        boardFolderContextViewProperties.Text = folderContextViewProperties.Text
        boardFolderContextDeleteBoard.Text = folderContextDeleteFolder.Text

        ' discussion forum menu strings
        forumFolderContextAddTopic.Text = m_refMsgApi.GetMessage("add topic")
        forumFolderContextViewPermissions.Text = m_refMsgApi.GetMessage("btn view permissions")
        forumFolderContextViewProperties.Text = folderContextViewProperties.Text
        forumFolderContextDeleteForum.Text = folderContextDeleteFolder.Text

        ' ecommerce folder menu strings
        ecommerceContentAddFolder.Text = m_refMsgApi.GetMessage("btn add catalog")
        ecommerceContentViewProperties.Text = folderContextViewProperties.Text
        ecommerceContentDeleteFolder.Text = folderContextDeleteFolder.Text
        ecommerceContextDeleteContent.Text = String.Format(m_refMsgApi.GetMessage("delete entries from x"), "<span class='triggerName'></span>")

        ' calendar folder menu strings
        calendarViewProperties.Text = folderContextViewProperties.Text
        calendarDeleteFolder.Text = folderContextDeleteFolder.Text

        ' taxonomy menu strings
        taxonomyAdd.Text = m_refMsgApi.GetMessage("btn add taxonomy")
        taxonomyContextView.Text = folderContextViewProperties.Text
        taxonomyAddContent.Text = m_refMsgApi.GetMessage("lbl assign items")
        taxonomyAddFolder.Text = m_refMsgApi.GetMessage("lbl assign folders")

        ' collections menu strings
        collectionContextAddCollection.Text = m_refMsgApi.GetMessage("add collection title")
        collectionContextAdd.Text = m_refMsgApi.GetMessage("add collection items")
        collectionContextRemove.Text = m_refMsgApi.GetMessage("remove collection items")
        collectionContextReorder.Text = m_refMsgApi.GetMessage("reorder menu item title")
        collectionContextView.Text = folderContextViewProperties.Text
        collectionContextDelete.Text = folderContextDeleteFolder.Text

        ' menu menu strings
        menuAdd.Text = m_refMsgApi.GetMessage("add menu title")
        menuContentAdd.Text = collectionContextAdd.Text
        menuRemoveItems.Text = collectionContextRemove.Text
        menuContextReorder.Text = collectionContextReorder.Text
        menuContextView.Text = folderContextViewProperties.Text
        menuContextDelete.Text = folderContextDeleteFolder.Text

        'jsAppPath.Text = m_refApi.AppPath.ToString()
        'jsConfirmFolderDelete.Text = m_refMsgApi.GetMessage("js contextmenu confirm delete folder")
        'jsConfirmCollectionDelete.Text = m_refMsgApi.GetMessage("js: confirm collection deletion msg")
        'jsConfirmMenuDelete.Text = m_refMsgApi.GetMessage("delete menu confirm")
    End Sub
    ''' <summary>
    ''' To Register Resources
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub registerResources()
        Dim AppPath As String = m_refApi.AppPath.ToString()
        ' register JS
        API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronJS)
        API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronUICoreJS)
        API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronUIEffectsJS)
        ' API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronUIAccordionJS
        API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronUIAccordionJS)
        API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronContextMenuJS)
        API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronStringJS)
        API.JS.RegisterJS(Me, AppPath & "java/ektron.workarea.trees.contextMenus.js", "EktronWorkareaTreesContextMenusJS")
        API.JS.RegisterJS(Me, AppPath & "controls/permission/permissionsCheckHandler.ashx?action=getPermissionJsClass", "EktronPermissionJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.explorer.init.js", "EktronExplorerInitJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.cms.types.js", "EktronCmsTypesJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.cms.parser.js", "EktronCmsParserJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.cms.toolkit.js", "EktronCmsToolkitJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.cms.api.js", "EktronCmsApiJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.ui.explore.js", "EktronUIExploreJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.ui.contextmenu.js", "EktronUIContextMenuJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.ui.tree.js", "EktronUITreeJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.net.http.js", "EktronNetHttpJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.utils.log.js", "EktronUtilsLogJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.utils.dom.js", "EktronUtilsDomJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.utils.debug.js", "EktronUtilsDebugJS")
        API.JS.RegisterJS(Me, AppPath & "Tree/js/com.ektron.utils.string.js", "EktronUtilsStringJS")
        API.JS.RegisterJS(Me, AppPath & "java/ektron.workareatrees.js", "EktronWorkareaTreesJS")

        ' register CSS
        API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaCss)
        API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaIeCss, API.Css.BrowserTarget.AllIE)
        API.Css.RegisterCss(Me, AppPath & "Tree/css/com.ektron.ui.tree.css", "EktronUITreesCSS")
        API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronContextMenuCss)

    End Sub
#End Region
End Class
#End Region