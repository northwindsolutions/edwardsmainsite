﻿#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
*/
#endregion

#region Imports
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.DataIO.LicenseManager;
#endregion

#region WorkareaAnalyticReportSubtree
public partial class WorkareaAnalyticReportSubtree : System.Web.UI.UserControl
{

    #region Private members
    private ContentAPI _contentApi = null;
    private EkMessageHelper _messageHelperRef = null;
    #endregion
    #region Public Property
    public System.Web.UI.HtmlControls.HtmlControl TreeContainer 
    { get { return SiteAnalytics; } }
    
    public ContentAPI ContentApi { get { return _contentApi ?? (_contentApi = new ContentAPI()); } }      
  
    public EkMessageHelper MessageHelper { get { return (_messageHelperRef ?? (_messageHelperRef = ContentApi.EkMsgRef)); } }
    #endregion

    #region Method
    public string getMessage(string key) 
    { return MessageHelper.GetMessage(key); }

    protected void Page_Init(object sender, EventArgs e) {
    }

    protected void Page_Load(object sender, EventArgs e) {
    }
    #endregion
}
#endregion