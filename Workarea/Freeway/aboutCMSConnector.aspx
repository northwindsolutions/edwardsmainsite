<%@ Page Language="C#" AutoEventWireup="true" CodeFile="aboutCMSConnector.aspx.cs" Inherits="Workarea_Freeway_aboutCMSConnector" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>About Ektron 9 Connector for Freeway</title>
      <script type="text/javascript">
     function HideOpner()
     { 
    
    window.opener.close();
     }
     
     </script>
</head>
<body onload="HideOpner();">
 <form id="form1" runat="server">
    <div>
    <table style="font-family:Arial;" width="100%">
    <tr>
   <td colspan="2"></td></tr>
    <tr>
    <td style="width:40%"><img src="images/freeway_logo_color150pixels.jpg" alt=""/></td>
    <td style="font-size:21px;color:Navy;width:60%;">Ektron 9 Connector for Freeway</td>
    
    </tr>
     <tr>
    <td colspan="2" style="height: 20px">&nbsp;</td></tr>
    <tr>
    
    <td colspan="2" style="font-size:12.5px;">
    Version:&nbsp;5.0<br />
    Translation Mode:&nbsp;<asp:Label runat="server" ID="lblConnectorHistory"></asp:Label><br />
    Freeway account being used:&nbsp;<asp:Label runat="server" ID="lblUserWithMode"></asp:Label> <br />
    Freeway server:&nbsp;<asp:HyperLink runat="server"  Target="_blank"  ID="hpLnkCurrentServerWithLink"></asp:HyperLink><br />
    </td>
    
    </tr>
    <tr>
    <td colspan="2">&nbsp;</td></tr>
    <tr>
   
    <td colspan="2" style="font-size:12.5px;">
    Warning: This computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law. 
    </td>
   </tr>
    <tr>
    <td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">
<table width="100%">
 <tr >     
     <td style="font-size:12.5px;width:70%;" valign="bottom" > <a href="http://www.lionbridge.com" target="_blank" >� 2010 Lionbridge Technologies, Inc.</a></td>
     <td  align="right" valign="bottom">
     <input type="button" value="OK" style="width:80px;cursor:pointer;" onclick="window.close();" />
     </td>
     </tr>
</table>
</td></tr>
    
    </table>
    </div>
    </form>
</body>
</html>

