using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;

public partial class Workarea_Freeway_aboutCMSConnector : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //--------
            if (!string.IsNullOrEmpty( ConfigurationManager.AppSettings["ShowEktronXLIFFHistory"]))
            {
                if (Convert.ToBoolean( ConfigurationManager.AppSettings["ShowEktronXLIFFHistory"])  )
                    lblConnectorHistory.Text = "Ektron";
                else
                    lblConnectorHistory.Text = "Freeway Connector";
            }
            else
                lblConnectorHistory.Text = "Freeway Connector";
            
            //-------------
            //------------
            UserAPI userApi = new UserAPI();
        DataRow userRow = FreewayDB.GetUserRow(userApi.UserId, false);
        if (userRow != null)
        {            
            DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            if (globalUserRow != null && Convert.ToBoolean(globalUserRow["ForceGlobalSettings"]) == true)
                lblUserWithMode.Text = globalUserRow["FreewayUsername"] + "&nbsp;(Global ";
            else
                lblUserWithMode.Text = userRow["FreewayUsername"] + "&nbsp;(Standard ";

            if (FreewayConfiguration.IsExecutiveUser)
               lblUserWithMode.Text += "Executive ";
           lblUserWithMode.Text += "User)";

           hpLnkCurrentServerWithLink.Text = userRow["CurrentEnvironment"].ToString ();
           if (userRow["CurrentEnvironment"].ToString() == "Demo")
           {
               hpLnkCurrentServerWithLink.NavigateUrl = "https://freeway.demo.lionbridge.com";
           }
           else if (userRow["CurrentEnvironment"].ToString() == "Live")
           {
               hpLnkCurrentServerWithLink.NavigateUrl = "http://freeway.lionbridge.com/";
           }
           
           
        }


            

        }

    }
}
