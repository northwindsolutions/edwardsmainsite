#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : This page will be used to perform the Content translation function
' and creating the XLIFF file from selected content for export
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports Ektron.Cms
Imports Ektron.Cms.Common.EkConstants
Imports Ektron.Cms.API
#End Region

Partial Class content
    Inherits System.Web.UI.Page

#Region "Member Variables"
    ''' <summary>
    ''' Creates enum flag with enabled and disabled status
    ''' </summary>
    ''' <remarks></remarks>
    Protected Enum Flag
        Disable = 0
        Enable = 1
    End Enum

    Protected m_bAjaxTree As Boolean = False
    Protected m_viewfolder As viewfolder
    Protected PageAction As String = ""
    Protected m_refMsg As Ektron.Cms.Common.EkMessageHelper
    Protected ContentLanguage As Integer = -1
    Protected m_intFolderId As Long = -1
    Protected m_refContentApi As New ContentAPI
    Protected m_bLangChange As Boolean = False
    Protected m_strReloadJS As String = ""
    Protected m_strEmailArea As String = ""
    Protected m_strMembership As String = ""
    Protected m_jsResources As Sync_jsResources
    Protected m_refCatalog As Ektron.Cms.Commerce.CatalogEntry = Nothing

    Private _ApplicationPath As String
    Private _SitePath As String

#End Region

#Region "Properties"

    ''' <summary>
    ''' Set or returns the Site path to the calling function
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SitePath() As String
        Get
            Return _SitePath
        End Get
        Set(ByVal Value As String)
            _SitePath = Value
        End Set
    End Property

    ''' <summary>
    ''' Set or returns the Application path to the calling function
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ApplicationPath() As String
        Get
            Return _ApplicationPath
        End Get
        Set(ByVal Value As String)
            _ApplicationPath = Value
        End Set
    End Property

#End Region

#Region "Constructor"

    ''' <summary>
    ''' Create the instance of the class
    ''' with setting the applicationpath, sitepath and Ektron message refernce
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()

        Dim endSlash() As Char = {"/"}
        Me.ApplicationPath = m_refContentApi.ApplicationPath.TrimEnd(endSlash)
        Me.SitePath = m_refContentApi.SitePath.TrimEnd(endSlash)
        m_refMsg = m_refContentApi.EkMsgRef
    End Sub

#End Region

#Region "PreInit, Load, PreRender"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    ''' <summary>
    ''' Varifies the action querystring against the "view", "viewstaged"
    ''' and sets the content information
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreInit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreInit

        If (Not (Request.QueryString("action") Is Nothing)) Then
            PageAction = Convert.ToString(Request.QueryString("action")).ToLower.Trim
        End If


        'Load Conditional JS
        If Trim(Request.QueryString("ShowTStatusMsg")) IsNot Nothing Then
            If Trim(Request.QueryString("ShowTStatusMsg")) = "1" Then
                phShowTStatusMessage.Visible = True
            End If
        End If
        If m_bAjaxTree = True Then
            phShowAjaxTree.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Generates the Tree Structure for the content and implements the respective Javascript and Stylesheet
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init

        Me.showAjaxTreeJsValues()
        Me.setEktronContentTemplateJsValues()
        Me.registerJs()
        Me.registerCss()

        ' Refreshes the Cashe from IIS 
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1
        m_refMsg = m_refContentApi.EkMsgRef

        ' Varifies the Previous link and found empty will redirect to the login page
        If (m_refContentApi.GetCookieValue("user_id") = 0) Then
            If Not (Request.QueryString("callerpage") Is Nothing) Then
                Session("RedirectLnk") = "Content.aspx?" & Request.QueryString.ToString()
            End If
            Response.Redirect("login.aspx?fromLnkPg=1", False)
            Exit Sub
        End If

        If (Not (Request.QueryString("action") Is Nothing)) Then
            PageAction = Convert.ToString(Request.QueryString("action")).ToLower.Trim
        End If

        If (Not (Request.QueryString("membership") Is Nothing)) Then
            m_strMembership = Convert.ToString(Request.QueryString("membership")).ToLower.Trim
        End If

        ' Generates the Contents tree structure
        If (m_refContentApi.TreeModel = 1) Then
            m_bAjaxTree = True
        End If
        If (Not (Request.QueryString("LangType") Is Nothing)) Then
            If (Request.QueryString("LangType") <> "") Then
                If Convert.ToInt32(Request.QueryString("LangType")) <> Convert.ToInt32(m_refContentApi.GetCookieValue("LastValidLanguageID")) Then
                    m_bLangChange = True
                End If
                ContentLanguage = Convert.ToInt32(Request.QueryString("LangType"))
                m_refContentApi.SetCookieValue("LastValidLanguageID", ContentLanguage)

            Else
                If m_refContentApi.GetCookieValue("LastValidLanguageID") <> "" Then
                    ContentLanguage = Convert.ToInt32(m_refContentApi.GetCookieValue("LastValidLanguageID"))
                End If
            End If
        Else
            If m_refContentApi.GetCookieValue("LastValidLanguageID") <> "" Then
                ContentLanguage = Convert.ToInt32(m_refContentApi.GetCookieValue("LastValidLanguageID"))
            End If
        End If
        If ContentLanguage = CONTENT_LANGUAGES_UNDEFINED Then
            m_refContentApi.ContentLanguage = ALL_CONTENT_LANGUAGES
        Else
            m_refContentApi.ContentLanguage = ContentLanguage
        End If
        ltrStyleSheetJs.Text = (New StyleHelper).GetClientScript
        txtContentLanguage.Text = m_refContentApi.ContentLanguage
        txtDefaultContentLanguage.Text = m_refContentApi.DefaultContentLanguage
        txtEnableMultilingual.Text = m_refContentApi.EnableMultilingual

        ' Performs the conditional action based on the QueryString action
        Select Case PageAction
            Case "viewarchivecontentbycategory", "viewcontentbycategory"
                UniqueLiteral.Text = "viewfolder"
                m_viewfolder = CType(LoadControl("viewfolder.ascx"), viewfolder)
                m_viewfolder.ID = "viewfolder"
                If m_bLangChange = True Then
                    m_viewfolder.ResetPostData()
                End If
                DataHolder.Controls.Add(m_viewfolder)
        End Select
        Dim m_mail As New EmailHelper

        Dim strEmailArea As String
        strEmailArea = m_mail.EmailJS
        strEmailArea += m_mail.MakeEmailArea
        ltrEmailAreaJs.Text = strEmailArea

        If (PageAction.ToLower().ToString() <> "viewcontentbycategory") Then
            showDropUploader(False)
        End If

        ' resource text string tokens
        'Dim portNumberText = m_refMsg.GetMessage("lbl port number")
        'Dim remoteServerText = m_refMsg.GetMessage("lbl remote server")
        'Dim stepText = m_refMsg.GetMessage("lbl step")
        'Dim ofText = m_refMsg.GetMessage("lbl of")
        Dim closeDialogText As String = m_refMsg.GetMessage("close this dialog")
        Dim cancelText As String = m_refMsg.GetMessage("btn cancel")
        'Dim backText = m_refMsg.GetMessage("back")
        'Dim nextText = m_refMsg.GetMessage("btn next")
        'Dim createText = m_refMsg.GetMessage("btn create")
        'Dim toggleDirectionText = m_refMsg.GetMessage("btn toggle direction")

        ' assign resource text string values
        btnConfirmOk.Text = m_refMsg.GetMessage("lbl ok")
        btnConfirmOk.NavigateUrl = "#" & m_refMsg.GetMessage("lbl ok")
        btnConfirmCancel.Text = cancelText
        btnConfirmCancel.NavigateUrl = "#" & cancelText
        btnCloseSyncStatus.Text = m_refMsg.GetMessage("close title")
        btnCloseSyncStatus.NavigateUrl = "#" & m_refMsg.GetMessage("close title")
        btnStartSync.Text = m_refMsg.GetMessage("btn sync now")

        closeDialogLink.Text = m_refMsg.GetMessage("close title")
        closeDialogLink.NavigateUrl = "#" + System.Text.RegularExpressions.Regex.Replace(closeDialogText, "\s+", "")
        closeDialogLink.ToolTip = closeDialogText
        closeDialogLink2.Text = closeDialogLink.Text
        closeDialogLink2.NavigateUrl = closeDialogLink.NavigateUrl
        closeDialogLink2.ToolTip = closeDialogText
        closeDialogLink3.Text = closeDialogLink.Text
        closeDialogLink3.NavigateUrl = closeDialogLink.NavigateUrl
        closeDialogLink3.ToolTip = closeDialogText

        lblSyncStatus.Text = m_refMsg.GetMessage("lbl sync status")

        m_jsResources = CType(LoadControl("sync_jsResources.ascx"), Sync_jsResources)
        m_jsResources.ID = "jsResources"
        sync_jsResourcesPlaceholder.Controls.Add(m_jsResources)
    End Sub

    ''' <summary>
    ''' Performs the Folder level functionalities based on the action performed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Dim bCompleted As Boolean
        Try
            If (Request.QueryString("reloadtrees") <> "") Then
                ltrEktronReloadJs.Text = ReloadClientScript()
            End If

            Select Case PageAction
                Case "localize", "localizeexport"
                    UniqueLiteral.Text = "localize"
                    Dim m_localization As freewaylocalization_uc
                    m_localization = CType(LoadControl("freewaylocalization_uc.ascx"), ASP.workarea_freeway_freewaylocalization_uc_ascx)
                    m_localization.ID = "freewaylocalization_uc"
                    DataHolder.Controls.Add(m_localization)
                    bCompleted = m_localization.Display
            End Select
        Catch ex As Exception
            Utilities.ShowError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Helper Methods"

    ''' <summary>
    ''' Assigns the Folder id to the AjaxTreeFolderId textbox
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub showAjaxTreeJsValues()
        litShowAjaxTreeFolderId.Text = Request.QueryString("id")
    End Sub

    ''' <summary>
    ''' Assigns the alert messages to the respective controls 
    ''' and will be showed to intimate to the users.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub setEktronContentTemplateJsValues()
        'This method populates literals for <script id="EktronContentTemplateJs">
        litConfirmContentDeletePublish.Text = m_refMsg.GetMessage("js: confirm content delete")
        litConfirmContentDeleteSubmission.Text = m_refMsg.GetMessage("js: confirm content delete submission")
        litConfirmContentDeleteDialog.Text = m_refMsg.GetMessage("js: confirm content delete")
        litConfirmFolderDelete.Text = m_refMsg.GetMessage("js: confirm folder delete")
        litConfirmFolderDeleteBelowRoot.Text = m_refMsg.GetMessage("js: confirm delete folders below root")
        litAlertSupplyFoldername.Text = m_refMsg.GetMessage("js: alert supply foldername")
        litAlertRequiredDomain.Text = m_refMsg.GetMessage("js required domain msg")
        litAlertMissingAlternateStylesheet.Text = m_refMsg.GetMessage("js: alert stylesheet must have css")
        litConfirmDeleteGroupPermissions.Text = m_refMsg.GetMessage("js: confirm delete group permissions")
        litConfirmDeleteUserPermissions.Text = m_refMsg.GetMessage("js: confirm delete user permissions")
        litAlertCannotDisableReadonly.Text = m_refMsg.GetMessage("js: alert cannot disable readonly")
        litAlertCannotDisableLibraryReadonly.Text = m_refMsg.GetMessage("js: alert cannot disable library readonly")
        litAlertCannotDisablePostReply.Text = m_refMsg.GetMessage("js: alert cannot disable postreply")
        litAlertCannotDisableEdit.Text = m_refMsg.GetMessage("js: alert cannot disable edit")
        litAlertSelectPermission.Text = m_refMsg.GetMessage("js: alert select permission")
        litAlertReadContentPermissionRemovalEffectWarning.Text = m_refMsg.GetMessage("js: read content permission removal effect warning")
        litConfirmDisableInheritance.Text = m_refMsg.GetMessage("js: confirm disable inheritance")
        litConfirmEnableInheritance.Text = m_refMsg.GetMessage("js: confirm enable inheritance")
        litConfirmMakeContentPrivate.Text = m_refMsg.GetMessage("js: confirm make content private")
        litConfirmMakeContentPublic.Text = m_refMsg.GetMessage("js: confirm make content public")
        litAlertCannotAlterPContSetting.Text = m_refMsg.GetMessage("js: alert cannot alter pcont setting")
        litConfirmMakeFolderPrivate.Text = m_refMsg.GetMessage("js: confirm make folder private")
        litConfirmMakeFolderPublic.Text = m_refMsg.GetMessage("js: confirm make folder public")
        litConfirmAddApproverGroup.Text = m_refMsg.GetMessage("js: confirm add approver group")
        litConfirmAddApproverUser.Text = m_refMsg.GetMessage("js: confirm add approver user")
        litConfirmDeleteApproverGroup.Text = m_refMsg.GetMessage("js: confirm delete approver group")
        litConfirmDeleteApproverUser.Text = m_refMsg.GetMessage("js: confirm delete approver user")
        litAlertSelectUserOrGroup.Text = m_refMsg.GetMessage("js select user or group")
        litAlertMetadataNotCompleted.Text = m_refMsg.GetMessage("js: alert meta data not completed")
        litContentTypeFolderId.Text = Request.QueryString("id")
        litTemplateConfigSaveFolderId.Text = Request.QueryString("id")
        litConfirmBreakInheritanceFlagging.Text = m_refMsg.GetMessage("js: confirm break inheritance")
        If m_strMembership.ToLower.Trim = "true" Then
            litDisableInheritenceIfMembershipTrue.Text = "&membership=true"
            litEnableInheritenceIfMembershipTrue.Text = "&membership=true"
            litEnableItemPrivateSettingMembershipTrue.Text = "&membership=true"
            litDisableItemPrivateSettingMembershipTrue.Text = "&membership=true"
        End If
    End Sub

    ''' <summary>
    ''' Loads the javascript at runtime to update the Content Treeview
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function reloadClientScript() As String
        Dim result As New System.Text.StringBuilder
        Dim pid As Long = 0
        Dim FolderPath As String = ""
        Try
            If (Not (Request.QueryString("id") Is Nothing)) Then
                pid = Request.QueryString("id")
            ElseIf (Not (Request.QueryString("id") Is Nothing)) Then
                pid = Request.QueryString("folder_id")
            End If

            Dim contObj As Ektron.Cms.Content.EkContent = m_refContentApi.EkContentRef
            If (pid >= 0) Then
                FolderPath = contObj.GetFolderPath(pid)
            End If
            contObj = Nothing

            result.Append("<script language=""javascript"">" & vbCrLf)
            If (m_refContentApi.TreeModel = 1 And pid <> 0) Then
                If ((Not (Request.QueryString("TreeUpdated") Is Nothing)) AndAlso (Request.QueryString("TreeUpdated") = 1)) Then
                    pid = m_refContentApi.GetParentIdByFolderId(pid)
                    If (pid = -1) Then
                        result.Length = 0
                        Exit Try
                    End If
                End If
                result.Append("if (typeof (reloadFolder) == 'function'){" & vbCrLf)
                result.Append("     reloadFolder(" + Convert.ToString(pid) + ");" & vbCrLf)
                result.Append("}" & vbCrLf)
                FolderPath = FolderPath.Replace("\", "\\")
                result.Append("top.TreeNavigation(""LibraryTree"", """ & FolderPath & """);" & vbCrLf)
                result.Append("top.TreeNavigation(""ContentTree"", """ & FolderPath & """);" & vbCrLf)
                If PageAction = "viewboard" Then
                    result.Append("window.location.href = 'threadeddisc/addeditboard.aspx?action=View&id=" & Request.QueryString("id") & "';" & vbCrLf)
                End If
            Else
                result.Append("<!--" & vbCrLf)
                result.Append("	// If reloadtrees paramter exists, reload selected navigation trees:" & vbCrLf)
                result.Append("	var m_reloadTrees = """ & Request.QueryString("reloadtrees") & """;" & vbCrLf)
                result.Append("	top.ReloadTrees(m_reloadTrees);" & vbCrLf)
                result.Append("	self.location.href=""" & Request.ServerVariables("path_info") & "?" & Replace(Request.ServerVariables("query_string"), "&reloadtrees=" & Request.QueryString("reloadtrees"), "") & """;" & vbCrLf)
                result.Append("	// If TreeNav parameters exist, ensure the desired folders are opened:" & vbCrLf)
                result.Append("	var strTreeNav = """ & Request.QueryString("TreeNav") & """;" & vbCrLf)
                result.Append("	if (strTreeNav != null) {" & vbCrLf)
                result.Append("		strTreeNav = strTreeNav.replace(/\\\\/g,""\\"");" & vbCrLf)
                result.Append("		top.TreeNavigation(""LibraryTree"", strTreeNav);" & vbCrLf)
                result.Append("		top.TreeNavigation(""ContentTree"", strTreeNav);" & vbCrLf)
                result.Append("	}" & vbCrLf)
                result.Append("//-->" & vbCrLf)
            End If
            result.Append("</script>" & vbCrLf)
            'Inner code of the Ektron so dont know how to trace it
        Catch ex As Exception
        End Try
        Return (result.ToString)
    End Function

    ''' <summary>
    ''' Avail to perform the DragDrop functionality to add the file to the Treeview
    ''' </summary>
    ''' <param name="bShow"></param>
    ''' <remarks></remarks>
    Private Sub showDropUploader(ByVal bShow As Boolean)
        Dim sJS As New System.Text.StringBuilder
        sJS.Append("<script language=""Javascript"">" & vbCrLf)
        If (bShow) Then
            sJS.Append(" if (typeof top != 'undefined') { " & vbCrLf)
            sJS.Append("    top.ShowDragDropWindow();" & vbCrLf)
            sJS.Append(" }" & vbCrLf)
        Else
            sJS.Append("if ((typeof(top.GetEkDragDropObject)).toLowerCase() != 'undefined') {" & vbCrLf)
            sJS.Append("	var dragDropFrame = top.GetEkDragDropObject();" & vbCrLf)
            sJS.Append("		if (dragDropFrame != null) {" & vbCrLf)
            sJS.Append("			dragDropFrame.location.href = ""blank.htm"";" & vbCrLf)
            sJS.Append("		}" & vbCrLf)
            sJS.Append("}" & vbCrLf)
            sJS.Append("if ((typeof(top.GetEkDragDropObject)).toLowerCase() != 'undefined') {" & vbCrLf)
            sJS.Append("	top.HideDragDropWindow();" & vbCrLf)
            sJS.Append("}" & vbCrLf)
        End If
        sJS.Append("</script>" & vbCrLf)
        Page.ClientScript.RegisterClientScriptBlock(GetType(Page), "DragUploaderJS", sJS.ToString())
    End Sub


#End Region


#Region "JS, CSS"

    ''' <summary>
    ''' Registers the Javascript for the page
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub registerJs()
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronThickBoxJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronXmlJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronModalJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronStringJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronDmsMenuJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronDetermineOfficeJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronJsonJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronDnRJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronScrollToJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/java/jfunct.js", "EktronJfuncJs")
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/java/toolbar_roll.js", "EktronToolbarRollJs")
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/java/platforminfo.js", "EktronPlatformInfoJs")
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/java/designformentry.js", "EktronDesignFormEntryJs")
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/java/internCalendarDisplayFuncs.js", "EktronInternCalendarDisplayJs")
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/sync/ektron.sync.js", "EktronSyncJS")
        Ektron.Cms.API.JS.RegisterJS(Me, Me.ApplicationPath & "/wamenu/includes/com.ektron.ui.menu.js", "EktronWamenuJs")
    End Sub

    ''' <summary>
    ''' Registers the StyleSheet for the page
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub registerCss()
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss)
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.LessThanEqualToIE7)
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronDmsMenuCss)
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronModalCss)
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronModalCss)
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronDmsMenuIE6Css, Ektron.Cms.API.Css.BrowserTarget.LessThanEqualToIE6)
        Ektron.Cms.API.Css.RegisterCss(Me, Me.ApplicationPath & "/sync/sync.css", "EktronSyncCss")
        Ektron.Cms.API.Css.RegisterCss(Me, Me.ApplicationPath & "/csslib/box.css", "EktronBoxCss")
        Ektron.Cms.API.Css.RegisterCss(Me, Me.ApplicationPath & "/csslib/tables/tableutil.css", "EktronTableUtilCss")
        Ektron.Cms.API.Css.RegisterCss(Me, Me.ApplicationPath & "/Tree/css/com.ektron.ui.tree.css", "EktronTreeCss")
        Ektron.Cms.API.Css.RegisterCss(Me, Me.ApplicationPath & "/wamenu/css/com.ektron.ui.menu.css", "EktronWamenuCss")
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss)
    End Sub

#End Region


End Class
