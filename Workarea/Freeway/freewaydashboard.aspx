<%@ Page Language="C#" AutoEventWireup="true" CodeFile="freewaydashboard.aspx.cs"
    Inherits="freewaydashboard" %>

<%@ Register Src="freewaydashboardprojectcontrol.ascx" TagName="freewaydashboardprojectcontrol"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Freeway Project Dashboard</title>
    <asp:literal id="jsStyleSheet" runat="server" />

    <script language="JavaScript" type="text/javascript" src="../java/jfunct.js"></script>

    <script language="JavaScript" type="text/javascript" src="../java/toolbar_roll.js"></script>

    <script type="text/javascript">
     function showAbout()
     { 
    
     window.open("aboutCMSConnector.aspx","About","width=455,height=370,left=280px,top=180px,resizable=no",false)
    
     }
      function showCreateProject()
     { 
     window.open("freewayprojectcreation.aspx","ProjectCreation","width=800,height=400,resizable=no",false)
         }
         
         
function setHeight()
{ 

var pnl=document.getElementById('pnl');
var Screenhight= screen.height;
if(navigator.appName=="Microsoft Internet Explorer")
{
pnl.style.height=Screenhight-((Screenhight*19)/100)+"px";
}
else
{
pnl.style.height=Screenhight-((Screenhight*21)/100)+"px";
}
pnl.style.width="100%"//screen.width-((screen.width*38)/100);
}

     
    </script>

    <%--<style type="text/css">
.modalBackground {
	background-color:Gray;
    background-repeat:true;
	filter:alpha(opacity=70);
	opacity:0.7;
}
</style> --%>
</head>
<body onload="setHeight()">
    <form id="dashboardForm" runat="server">
 <asp:ScriptManager ID="scrpprox" runat="server"></asp:ScriptManager>

<asp:Panel runat="server" ID="pnl"  ScrollBars="auto">  
  <div id="dhtmltooltip">
                    </div>

                    <script language="JavaScript" type="text/javascript" src="../java/workareahelper.js"></script>
<%--<table border="1" width = "120%" cellpadding="0" cellspacing="0">--%>
        <table border="1" width="100%"  cellpadding="0" cellspacing="0">
            <tr>
                <td>
                 <%--<asp:Panel ID="Panel3" Height="43" width="120%" runat="server">--%>
                  <asp:Panel ID="Panel2" Width="100%"  runat="server">
                        <table class="ektronPageHeader" width="100%" >
                            <tr>
                                <td id="txtTitleBar" colspan="2" class="ektronTitlebar" runat="server" nowrap>
                                </td>
                            </tr>
                            <tr>
                            <td id="htmToolBar" style="width: 40%" class="ektronToolbar" runat="server">
                                &nbsp;User:<asp:DropDownList ID="_userSelect" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="_userSelect_SelectedIndexChanged">
                                </asp:DropDownList>
                                <!--&nbsp;Filter:<asp:DropDownList ID="_projectStatusFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="_projectStatusFilter_SelectedIndexChanged"></asp:DropDownList>
                                        &nbsp;View:<asp:DropDownList ID="_projectCount" runat="server" AutoPostBack="true" OnSelectedIndexChanged="_projectCount_SelectedIndexChanged"></asp:DropDownList>-->
                                &nbsp;<asp:Button ID="_reload" runat="server" Text="Refresh" OnClick="_reload_Click"
                                    Font-Names="Verdana" Font-Size="6.5pt" Width="60px" />
                            </td>
                            <td class="ektronToolbar" style="width: 59%">
                                <asp:Label ID="lblFreewayDetails" ForeColor="white" runat="server"> </asp:Label>
                            </td>
                        </tr>
                        </table>
                    </asp:Panel>
                    <%-- <asp:Panel ID="Panel3" Height="411" width="120%" ScrollBars="Vertical" runat="server"  >--%>
                         
                        <%--  <asp:Panel ID="Panel1" width="100%" ScrollBars="auto" runat="server"  >--%>
            <table cellpadding="0" width="95%" cellspacing="0"  class="ektronGrid"  >
                <tr>
                    <td>
                       <%-- <asp:ScriptManager ID="scriptManager1" runat="server">
                        </asp:ScriptManager>
                        <ajaxToolkit:ModalPopupExtender ID="mpeNewProject" BackgroundCssClass="modalBackground"
                            PopupControlID="pnlNewProject" TargetControlID="lnkbtnNewProject" CancelControlID="CancelBtn"
                            runat="server">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnlNewProject" ScrollBars="None" runat="server">
                            <table width="100%" style="background-color: White;">
                                <tr>
                                    <td align="RIGHT" valign="top">
                                        <div style="text-align: right; padding-right: 2px;">
                                            <asp:LinkButton ID="CancelBtn" runat="server" Font-Names="verdana" Font-Size="10px"
                                                Style="text-align: center; color: Black;" Text="Close" OnClick="CancelBtn_Click"></asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <iframe id="ifrmNewProject" width="560px" height="320px" src="freewayprojectcreation.aspx">
                                        </iframe>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:LinkButton runat="server" ForeColor="#2e6e9e" OnClick="LinkButton_Onclick" Text="New Project"
                            ID="lnkbtnNewProject"></asp:LinkButton>--%>
                        <a href="#" onclick="showCreateProject();"  style="color:#2e6e9e;text-decoration:underline;font-family:Verdana;font-size:11px;">New Project</a>
             
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<ajaxToolkit:ModalPopupExtender ID="mpeProjectDetails" BackgroundCssClass="modalBackground"
                            PopupControlID="pnlProjectDetails" TargetControlID="_dataList" CancelControlID="CancelButton"
                            runat="server">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnlProjectDetails" Width="300px" Height="500px"  ScrollBars="None" runat="server">
                            <table width="100%" style="background-color: White;">
                                <tr>
                                    <td align="RIGHT" valign="top">
                                        <div style="text-align: right; padding-right: 2px;">
                                            <asp:LinkButton ID="CancelButton" runat="server" Font-Names="verdana" Font-Size="10px"
                                                Style="text-align: center; color: Black;" Text="Close" OnClick="CancelButton_Click"></asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>--%>
                        <asp:DataList ID="_dataList" CssClass="ektronGrid" runat="server" ShowFooter="False"
                            OnItemDataBound="_dataList_ItemDataBound" OnSelectedIndexChanged="_dataList_SelectedIndexChanged"
                            Width="100%" CellPadding="4 " >
                            <HeaderStyle CssClass="title-header" />
                            <SelectedItemStyle BorderColor="DarkBlue" BorderStyle="solid" BorderWidth="2px" />
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr valign="bottom" style="height: 18px">
                                        <td width="13%">
                                            <asp:LinkButton ID="_projectIdHeader" runat="server" OnClick="Header_Click" Text="<u>Project Id</u>"
                                                ToolTip="Sort by Project Id">
                                                <asp:Image ID="_projectIdSortImage" runat="server" ImageUrl="~/Workarea/images/application/arrow_up_green.gif"
                                                    Visible="true" /></asp:LinkButton></td>
                                        <td width="25%">
                                            <asp:LinkButton ID="_projectNameHeader"  runat="server" OnClick="Header_Click" Text="<u>Description</u>"
                                                ToolTip="Sort by Description">
                                                <asp:Image ID="_projectNameSortImage" runat="server" ImageUrl="~/Workarea/images/application/arrow_up_green.gif"
                                                    Visible="false" /></asp:LinkButton></td>
                                        <td width="16%">
                                            <asp:LinkButton ID="_submittedByHeader" runat="server" OnClick="Header_Click" Text="<u>Submitted By</u>"
                                                ToolTip="Sort by Submitted By">
                                                <asp:Image ID="_submittedBySortImage" runat="server" ImageUrl="~/Workarea/images/application/arrow_up_green.gif"
                                                    Visible="false" /></asp:LinkButton></td>
                                        <td width="17%">
                                            <asp:LinkButton ID="_submittedWhenHeader" runat="server" OnClick="Header_Click" Text="<u>Submitted When</u>"
                                                ToolTip="Sort by Submitted When">
                                                <asp:Image ID="_submittedWhenSortImage" runat="server" ImageUrl="~/Workarea/images/application/arrow_up_green.gif"
                                                    Visible="false" /></asp:LinkButton></td>
                                        <td width="13%">
                                            <asp:LinkButton ID="_projectStatusHeader" runat="server" OnClick="Header_Click" Text="<u>Status</u>"
                                                ToolTip="Sort by Status">
                                                <asp:Image ID="_projectStatusSortImage" runat="server" ImageUrl="~/Workarea/images/application/arrow_up_green.gif"
                                                    Visible="false" /></asp:LinkButton></td>
                                        <td width="16%">
                                            <asp:LinkButton ID="_retrievedWhenHeader" runat="server" OnClick="Header_Click" Text="<u>Retrieved When</u>"
                                                ToolTip="Sort by Retrieve When">
                                                <asp:Image ID="_retrievedWhenSortImage" runat="server" ImageUrl="~/Workarea/images/application/arrow_up_green.gif"
                                                    Visible="false" /></asp:LinkButton></td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="height: 2px;">
                                </div>
                                <div style="cursor: pointer;">
                                    <asp:Table ID="_listTable" runat="server" Width="100%" >
                                        <asp:TableRow ID="TableRow">
                                            <asp:TableCell Width="13%">
                                                <asp:LinkButton ID="_selectButton" CommandName="Select" runat="server" OnClick="_selectButton_Click"></asp:LinkButton>
                                                <asp:HyperLink Font-Underline="true" ID="_projectId" runat="server" Text='<%# Bind("Id") %>'
                                                    Target="_blank"></asp:HyperLink>
                                            </asp:TableCell>
                                            <asp:TableCell Width="25%" >
                                                <asp:Label ID="_projectName"  runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="16%">
                                                <asp:Label ID="_submittedBy" runat="server" Text='<%# Bind("SubmittedBy") %>'></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="17%">
                                                <asp:Label ID="_submittedWhen" runat="server" Text='<%# Bind("SubmittedWhen") %>'></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="13%">
                                                <asp:Label ID="_projectStatus" runat="server" Text='<%# Bind("StatusText") %>'></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="16%">
                                                <asp:Label ID="_retrievedWhen" runat="server" Text='<%# Bind("RetrievedWhen") %>'></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="16%">
                                                <asp:Label ID="_JobId" runat="server" Visible="false" Text='<%# Bind("JobId") %>'></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                                <div>
                                    <uc1:freewaydashboardprojectcontrol ParentPath="Settings" ID="_Freewaydashboardprojectcontrol"
                                        runat="server" Visible="false" OnRetrieveClick="_Freewaydashboardprojectcontrol_OnRetrieveClick" />
                                </div>
                                <div style="height: 2px;">
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
        <%-- </asp:Panel> --%>
        
                        </td>
                        </tr>
                    </table>
              <%--  </td>
            </tr>
          </table>        
        --%>
</asp:Panel>
</form>
</body>
</html>
