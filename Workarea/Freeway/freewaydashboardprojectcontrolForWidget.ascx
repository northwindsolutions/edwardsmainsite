<%@ Control Language="C#" AutoEventWireup="true" CodeFile="freewaydashboardprojectcontrolForWidget.ascx.cs"
    Inherits="freewaydashboardprojectcontrolForWidget" %>
<%@ Register Src="freewaydashboardquotation.ascx" TagName="freewaydashboardquotation"
    TagPrefix="uc1" %>

<%--<script type="text/javascript">
var filesSelected=false;
	 function AddViewTask(projectid)
	 {
	  //debugger;
	    var url = "FreewayAddViewTask.aspx?projectid="+projectid;
	    window.open(url,"FreewayAddViewTask","status = yes,resizable = no,top = 0, left = 0,height = 480, width = 1012");
	 }
        function public_GetParentByTagName(element, tagName) 
        { 
 //debugger;
        var parent = element.parentNode; 
        var upperTagName = tagName.toUpperCase(); 

        while (parent && (parent.tagName.toUpperCase() != upperTagName)) 
        { 
        parent = parent.parentNode ? parent.parentNode : 
        parent.parentElement; 
        } 
        return parent; 
        } 
        function setParentUnChecked(objParentDiv) 
        { 
 //debugger;
        //var objParentDiv = public_GetParentByTagName(objNode,"INPUT"); 
        if(objParentDiv==null || objParentDiv == "undefined") 
        { 
        return; 
        } 
        var objID = objParentDiv.getAttribute("ID"); 
        objID = objID.substring(0,objID.indexOf("Nodes")); 
        objID = objID+"CheckBox"; 
        var objParentCheckBox = document.getElementById(objID); 
        if(objParentCheckBox==null || objParentCheckBox == "undefined") 
        { 
        return; 
        } 
        if(objParentCheckBox.tagName!="INPUT" && objParentCheckBox.type == 
        "checkbox") 
        return; 
        objParentCheckBox.checked = false; 
        setParentUnChecked(objParentCheckBox); 
        } 

        // set the children to be unchecked.
        function setChildUnChecked(divID) 
        { 
         //debugger;
        var objchild = divID.children; 
        var count = objchild.length; 
        for(var i=0;i<objchild.length;i++) 
        { 
        var tempObj = objchild[i]; 
        if(tempObj.tagName=="INPUT" && tempObj.type == "checkbox") 
        { 
        tempObj.checked = false; 
        } 
        setChildUnChecked(tempObj); 
        } 
        } 
        // set the children to be checked
        function setChildChecked(divID) 
        { 
        //debugger;
        var objchild = divID.children; 
        var count = objchild.length; 
        for(var i=0;i<objchild.length;i++) 
        { 
        var tempObj = objchild[i]; 
        if(tempObj.tagName=="INPUT" && tempObj.type == "checkbox") 
        { 
        tempObj.checked = true; 
        } 
        setChildChecked(tempObj); 
        } 
        } 
        // trigger the event
        function CheckEvent() 
        {  
             //debugger;
            var objNode = event.srcElement; 
            if(objNode.tagName!="INPUT" || objNode.type!="checkbox") 
            return; 
                if(objNode.checked==true) 
                { 
                filesSelected=true;
                ToggleParentCheckBox (objNode);
                //setParentChecked(objNode); 
                var objID = objNode.getAttribute("ID"); 
                var objID = objID.substring(0,objID.indexOf("CheckBox")); 
                var objParentDiv = document.getElementById(objID+"Nodes"); 
                    if(objParentDiv==null || objParentDiv == "undefined") 
                    { 
                    return; 
                    } 
                setChildChecked(objParentDiv); 
                } 
                else 
                { 
                   
                    var objID ;
                     var objIDSub ;
                      if(objNode.title=="parentnode")
                      {
                      objID=objNode.getAttribute("ID"); 
                       objIDSub= objID.substring(0,objID.indexOf("CheckBox")); 
                       objParentDiv= document.getElementById(objIDSub+"Nodes");               
                       setParentUnChecked(objParentDiv); 
                       setChildUnChecked(objParentDiv); 
                      }
                      else
                      {                     
                       objID=objNode.getAttribute("ID"); 
                       objIDSub= objID.substring(0,objID.indexOf("CheckBox")); 
                        var objParentDiv=null ;
                                if(objParentDiv==null || objParentDiv == "undefined") 
                                {
                                   var ParentDivObj;
                                   var iLoop=0;
                                   var NodeObj=objNode.parentNode;
                                        for (iLoop=1;iLoop>0;iLoop++)
                                        {                   
                                            if(NodeObj.tagName=="DIV")
                                            {
                                            setParentUnChecked(NodeObj);
                                            return;                            
                                            } 
                                             NodeObj=NodeObj.parentNode;
                                        } 
                               
                                }                 
                        } 
                }
     }


 //returns the ID of the parent container if the current checkbox is unchecked
    function GetParentNodeById(element, id)
    {   
     //debugger;
        var parent = element.parentNode;
        if (parent == null)
        {
            return false;
        }
        if (parent.id.search(id) == -1)
        {
           parent=GetParentNodeById(parent, id);
          return parent;
        }
        else
        {
        return parent;
        }
    }
   
    function CheckParentIfAllChildNodesSelected(parentContainer, id)
    {   
     //debugger;
        var childCheckBox;
        var ParentCheckBox ;
        
        var allChildrenSelected=1;
         var parentCheckBoxId = parentContainer.id.substring(0, parentContainer.id.search("Nodes")) + "CheckBox";
             ParentCheckBox=document.getElementById(parentCheckBoxId);
             if(ParentCheckBox!=null)
             {
                 if(ParentCheckBox.checked ==false)
                 {
                     for(var i=0;i<parentContainer.childNodes.length;i++)
                     {
                     if( parentContainer.childNodes[i].getElementsByTagName("INPUT").length>0 )
                        {
                            childCheckBox= document.getElementById ( parentContainer.childNodes[i].getElementsByTagName("INPUT")[0].id);
                            if(ParentCheckBox!=null)
                             {
                                if(childCheckBox.checked ==false)
                                 {
                                 allChildrenSelected=0;
                                 } 
                             }
                         }                 
                         
                     }
                     if(allChildrenSelected==1)
                     {
                     ParentCheckBox.checked=true;
                     }
                 }
            }
    }

function ToggleParentCheckBox(checkBox)
    {  
     //debugger;  
        if(checkBox.checked == true)
        {       
            var parentContainer = GetParentNodeById(checkBox, "_files");
            if(parentContainer) 
            {
                 CheckParentIfAllChildNodesSelected(parentContainer, "_files");
            }
        }
    }

function isFileSelected ()
    {    
       //debugger;    
        var returnval = true;
        if(!filesSelected) 
        {
           alert('Please select the file to retieve.');
           returnval = false;
        }
        else
        {      
        alert('Please Wait retieval process is started...');          
        returnval =true;
        }
        
        return  returnval;      
    }

</script>--%>

<script type="text/javascript">
 function OnTreeClick(event) 
 {
     var src = event.srcElement || event.target;

     if (src.tagName == "INPUT" && src.type == "checkbox") 
    {
//        if (src.checked)
//         {
             var parentTable = GetParentByTagName("table", src);
             var nxtSibling = parentTable.nextSibling;   
               //check if nxt sibling is not null & is an element node     
                if(nxtSibling && nxtSibling.nodeType == 1)   
                  {        
                   if(nxtSibling.tagName.toLowerCase() == "div")
                    //if node has children       
                     {
                         //check or uncheck children at all levels            
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                     }
                 } 
                 //check or uncheck parents at all levels 
                       CheckUncheckParents(src, src.checked);
        //}
    }
 }

 function CheckUncheckChildren(childContainer, check) {
     
     var childChkBoxes = childContainer.getElementsByTagName("input");
     var childChkBoxCount = childChkBoxes.length;
     for (var i = 0; i < childChkBoxCount; i++)
      {
          childChkBoxes[i].checked = check;
      }
  }

  function CheckUncheckParents(srcChild, check) {
     
      var parentDiv = GetParentByTagName("div", srcChild); 
      var parentNodeTable = parentDiv.previousSibling; 
  if(parentNodeTable) 
  {    
   var checkUncheckSwitch;    
       if(check) 
       //checkbox checked     
       {         
           var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);         
           if(isAllSiblingsChecked)                
            checkUncheckSwitch = true;        
             else                
              return; 
              //do not need to check parent if any(one or more) child not checked     
        }     
        else //checkbox unchecked  
        {        
            checkUncheckSwitch = false;    
        }       
        
     var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");    
      
     if(inpElemsInParentTable.length > 0)    
       {        
           var parentNodeChkBox = inpElemsInParentTable[0];         
           parentNodeChkBox.checked = checkUncheckSwitch;         
           //do the same recursively       
            CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);     
        }
    }
 }
 function AreAllSiblingsChecked(chkBox) {
   
    var parentDiv = GetParentByTagName("div", chkBox); 
    var childCount = parentDiv.childNodes.length; 
    for(var i=0;i<childCount;i++) 
    {     
       if(parentDiv.childNodes[i].nodeType == 1)     
       {         
       //check if the child node is an element node         
         if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")        
         {             
           var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
           //if any of sibling nodes are not checked, return false             
           if (!prevChkBox.checked)
            {
               return false;
            }
         }       }
    }
     return true;
}
//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj)
    {
       var parent = childElementObj.parentNode; 
                  while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())    
                   {        
                    parent = parent.parentNode;     
                   } 
               return parent;
     }

</script>

<table width="100%" border="2" cellpadding="2" cellspacing="2">
    <tr valign="top">
        <td colspan="3">
            <fieldset style="border: solid 1px skyblue; padding-left: 4px; width: 98%;">
                <legend>Project Details</legend>
                <table width="100%" style="vertical-align: top;">
                    <tr>
                        <td nowrap colspan="2">
                            Starting Date:&nbsp;
                            <asp:Label ID="_statingDate" runat="server"></asp:Label>
                        </td>
                        <td nowrap colspan="2">
                            Delivery Date:&nbsp;
                            <asp:Label ID="_deliveryDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap colspan="2">
                            Project Manager:&nbsp;
                            <asp:HyperLink ID="_pmLink" runat="server">[_pmLink]</asp:HyperLink>
                        </td>
                        <td nowrap colspan="2">
                            Project Cost:&nbsp;
                            <asp:Label ID="_projectCost" runat="server">[_projectCost]</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <uc1:freewaydashboardquotation ID="_freewaydashboardquotation" runat="server" Visible="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="overflow: auto;" runat="server" id="divContents">
                                <asp:LinkButton ID="_showHideContentIds" runat="server" OnClick="_showHideContentIds_Click">
                                    <asp:Image ID="_showHideContentIdsImage" runat="server" ImageUrl="~/Workarea/images/application/folders/fcpln.gif" />Content
                                    Id(s):
                                </asp:LinkButton><br />
                                <asp:Panel ID="_contentIdsPanel" runat="server" Visible="false">
                                    <asp:Label ID="_contentIds" runat="server" Visible="false"></asp:Label>
                                    <div id="_contentIdsDiv" runat="server">
                                    </div>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <td colspan="3">
            <table width="100%" id="tbltargetfiles" runat="server">
                <tr>
                    <td>
                        <asp:Label ID="lblerror" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Medium"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset id="fldsetContentDetails" runat="server" visible="false" style="border: solid 1px skyblue;
                            padding-left: 4px;">
                            <legend>Content Details</legend>
                            <div style="overflow: auto; height: 140px; vertical-align: middle; width: 100%;">
                                <asp:TreeView ID="_files" runat="server">
                                </asp:TreeView>
                                <%--<asp:CheckBoxList ID="_files" runat="server"></asp:CheckBoxList>--%>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<asp:Button ID="_retrieve" runat="server" OnClick="_retrieve_Click" OnClientClick="javascript:return isFileSelected();" Text="Retrieve Selected"
                            Width="130px" Font-Names="Verdana" Font-Size="8pt" />
                           --%>
                    </td>
                    <td align="right">
                        <%-- <asp:Button ID="_history" runat="server" OnClick="_history_Click" Text="View History"
                            Width="130px" Font-Names="Verdana" Font-Size="8pt" />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset id="fldSetHistory" runat="server" visible="false" style="border: solid 1px skyblue;
                            padding-left: 4px;">
                            <legend>Retrieval History</legend>
                            <asp:Panel ID="_historyPanel" runat="server" Visible="false" Style="overflow: auto;
                                height: 120px;">
                            </asp:Panel>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table width="100%" id="tbladdviewtask" runat="server">
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Button ID="btnaddviewtask" runat="server" Text="View/Add Task" Font-Names="Verdana"
                            Font-Size="8pt" Visible="false" />
                    </td>
                    <td align="right">
                        <%-- <asp:CheckBox ID="chkquote" runat="server" Text="For Quote" />
                        <asp:Button ID="btnsubProj" runat="server" Text="Submit Project" OnClick="btnsubProj_Click"
                            Font-Names="Verdana" Font-Size="8pt" />--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table width="100%">
                <tr>
                    <td>
                        <asp:Button ID="btnsubProj" runat="server" Text="Submit Project" OnClick="btnsubProj_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
                        <asp:Button ID="btnsubProjWithQuote" runat="server" Text="Submit as Quote" OnClick="btnsubProjWithQuote_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
                        <%--  <asp:ImageButton ID="btnsubProj" Width="25px" Height="25px"  runat="server" ToolTip="Submit Project" OnClick="btnsubProj_Click"
                            />
                        <asp:ImageButton ID="btnsubProjWithQuote" Width="25px" Height="25px" runat="server" ToolTip="Submit as Quote" OnClick="btnsubProjWithQuote_Click"
                            />--%>
                    </td>
                    <td align="center">
                        <%--<asp:Button ID="_retrieve" runat="server" OnClick="_retrieve_Click" OnClientClick="javascript:return isFileSelected();"
                            Text="Retrieve Selected" Font-Names="Verdana" Font-Size="8pt" />--%>
                        <asp:Button ID="_retrieve" runat="server" OnClick="_retrieve_Click" Text="Retrieve Selected"
                            Font-Names="Verdana" Font-Size="8pt" />
                        <asp:Button ID="_history" runat="server" OnClick="_history_Click" Text="View History"
                            Font-Names="Verdana" Font-Size="8pt" />
                        &nbsp;&nbsp;
                        <%--  <asp:ImageButton ID="_retrieve" Width="25px" Height="25px"  ImageUrl="~/Workarea/Freeway/images/UI/Icons/Download-icon.png" runat="server" OnClick="_retrieve_Click" OnClientClick="javascript:return isFileSelected();"
                            ToolTip="Retrieve Selected" />
                        <asp:ImageButton ID="_history" Width="25px" Height="25px"  ImageUrl="~/Workarea/Freeway/images/UI/Icons/HistoryInfo.jpg"  runat="server" OnClick="_history_Click" ToolTip="View History"
                           />
                        <asp:ImageButton ID="_removeProject" Width="25px" Height="25px"   runat="server"  ImageUrl="~/Workarea/Freeway/images/UI/Icons/Archive-icon.png" ToolTip="Archive" OnClick="_removeProject_Click"
                             />
                        <asp:ImageButton ID="_cancelProject" Width="25px" Height="25px"   runat="server"  ImageUrl="~/Workarea/Freeway/images/UI/Icons/Cancel-icon.png" ToolTip="Cancel" OnClick="_cancelProject_Click"
                             />--%>
                    </td>
                    <td align="right">
                        <asp:Button ID="_removeProject" runat="server" Text="Archive" OnClick="_removeProject_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
                        <asp:Button ID="_cancelProject" runat="server" Text="Cancel" OnClick="_cancelProject_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Panel ID="_errorPanel" runat="server" Visible="false">
                <asp:Label ID="_errorLabel" runat="server" Style="color: Red"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="_progressPanel" runat="server" Visible="false">
                <iframe id="_framelocalization" runat="server" width="100%" height="80" frameborder="1"
                    marginwidth="1" marginheight="4"></iframe>
            </asp:Panel>
        </td>
    </tr>
</table>
