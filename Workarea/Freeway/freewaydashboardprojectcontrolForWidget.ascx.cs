#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : This page will be displayed by clicking on project from the Freeway Dashboard screen.
 * This page allows displaying the respective project summary 
 * and functionality to the user based on the project status.
---------------------------------
*/
#endregion

#region using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using Ektron.Cms;
using Ektron.Cms.Common;
using System.Text;
using DS = Ektron.Cms.LocalizationJobDataSet;
using LB.FreewayLib.VojoService;

#endregion

#region freewaydashboardprojectcontrol
public partial class freewaydashboardprojectcontrolForWidget : System.Web.UI.UserControl
{
    #region private and protected variables
    private EktronProject _ektronProject = null;
    private bool _retrieved = false;
    private bool _progressPanelVisible = FreewayConfiguration.Version75OrGreater;
    string _parentPath = string.Empty;
    #endregion

    #region Page events
    /// <summary>
    /// Loads the project information from Ektron database
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        _freewaydashboardquotation.EktronProject = EktronProject;
        btnaddviewtask.Attributes.Add("onclick", "javascript:AddViewTask(" + ProjectId + ");return false;");

	//Hides the content from display
        divContents.Visible = false;

        if (ParentPath == "Settings")
            _framelocalization.Attributes.Add("src", "../localizationjobs.aspx");
        else
            _framelocalization.Attributes.Add("src", "./localizationjobs.aspx");

        lblerror.Visible = false;

	return;
    }

//    /// <summary>
//    /// Loads the data for Ektron project
//    /// </summary>
//    /// <param name="e"></param>
//    protected override void OnPreRender(EventArgs e)
//    {
//        base.OnPreRender(e);

//#if false
//        UserAPI userApi = new UserAPI();

//        // only an admin can remove project from the list
//        _removeProject.Visible = userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers, userApi.UserId, false);
//#endif

//        if (EktronProject != null)
//        {
//            if (EktronProject.FullStatus == null)
//            {
//                // Ektron project status updates
//                EktronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
//                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
//                EktronProject.GetFilesStatus(FreewayConfiguration.IsExecutiveUser ?
//                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
//            }

//            //			if (!IsPostBack)
//            {
//                _statingDate.Text = EktronProject.ExpectedStartingDate.ToString("g");
//                _deliveryDate.Text = EktronProject.ExpectedDeliveryDate.ToString("g");
//                _pmLink.Text = EktronProject.FullStatus.ProjectManager.Value;
//                _pmLink.NavigateUrl = "mailto:" + EktronProject.FullStatus.ProjectManager.Email;
//            }

//            //Project cost information
//            //Default is 0.0$
//            _projectCost.Text = (0.0).ToString("C");

//            //Displays the project cost if project is in below state
//            // Forcasted, InProduction,Closed or Completed
//            switch (EktronProject.Status)
//            {

//                case LB.FreewayLib.VojoService.ProjectStatusCode.Forecasted:
//                case LB.FreewayLib.VojoService.ProjectStatusCode.InProduction:
//                case LB.FreewayLib.VojoService.ProjectStatusCode.Closed:
//                case LB.FreewayLib.VojoService.ProjectStatusCode.Completed:
//                    if (string.IsNullOrEmpty(EktronProject.ProjectOwner))
//                        EktronProject.RetrieveDetails(FreewayConfiguration.IsExecutiveUser ?
//                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

//                    _projectCost.Text = EktronProject.TotalPrice.ToString("C") + " " + EktronProject.Currency;

//                    break;
//            }

//            //Quote accept reject functionality will be available 
//            //only when the project status is Forcasted
//            _freewaydashboardquotation.Visible = EktronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Forecasted;

//            _files.Items.Clear();

//            //Hiding the file for retrival when the Project Status is Draft 
//            if (FileVisible)
//            {
//                if (_files.Items.Count == 0)
//                {
//                    bool canRetrieve = false;

//                    foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
//                    {
//                        CultureInfo sourceCulture = new CultureInfo(file.SourceLang);

//                        foreach (string targetLang in file.TargetLang)
//                        {
//                            CultureInfo targetCulture = new CultureInfo(targetLang);

//                            ListItem item = new ListItem(string.Format("{0} (from {1})", targetCulture.DisplayName, sourceCulture.DisplayName));

//                            item.Value = file.Id;
//                            item.Enabled = file.Ready;
//                            if (!item.Enabled)
//                                item.Text = string.Format("{0} (Not ready)", item.Text);
//                            else
//                            {
//                                canRetrieve = true;

//                                DateTime lastRetrieve = EktronProject.GetLastRetrieve(file);

//                                if (lastRetrieve != DateTime.MinValue)
//                                    item.Text += string.Format(" - Last Retrieved: {0}", lastRetrieve.ToString("g"));
//                            }
//                            item.Selected = item.Enabled;

//                            _files.Items.Add(item);
//                        }
//                    }

//                    _retrieve.Enabled = canRetrieve;

//                }
//            }

//            //Target Files information will be visible   
//            //only when the project status is not Draft
//            tbltargetfiles.Visible = FileVisible;

//            //Add and View task functionality will be available   
//            //only when the project status is Draft
//            tbladdviewtask.Visible = !FileVisible;


//            // display Content ID for that project/job
//            if (_contentIdsPanel.Visible)
//            {
//                try
//                {
//                    _contentIds.Text = string.Empty;

//                    foreach (int id in EktronProject.ContentIds)
//                        _contentIds.Text += string.Format("{0}, ", id);

//                    _contentIds.Text = _contentIds.Text.TrimEnd(",".ToCharArray());

//                    ContentAPI contentApi = new ContentAPI();

//                    StringBuilder sb = new StringBuilder();

//                    foreach (int id in EktronProject.ContentIds)
//                    {

//                        ContentData content = contentApi.GetContentById(id, ContentAPI.ContentResultType.Published);

//                        if (content == null)
//                            throw new Exception("Content id - " + id + " is not available and throwing invalid Content id exception.");

//                        if (EktronProject.ContentIds[EktronProject.ContentIds.Length - 1] != id)
//                            sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcn.gif"));
//                        else
//                            sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcln.gif"));
//                        sb.Append(string.Format(@"<a href=""../content.aspx?action=View&folder_id=0&id={0}&LangType={2}&callerpage=content.aspx&origurl=action%3dViewContentByCategory%26id%3d0"">{0} - {1}</a><br />", content.Id, content.Title, EktronProject.SourceLangId));
//                    }

//                    _contentIdsDiv.InnerHtml = sb.ToString();

//                    if (_contentIdsDiv.InnerHtml.Trim().Length == 0)
//                    {
//                        _contentIdsDiv.InnerHtml = "<span>" + "No Contents are available." + "</span>";
//                        _contentIds.Text = "No Contents are available.";
//                    }
//                }
//                catch (Exception ex)
//                {
//                    _contentIdsDiv.InnerHtml = "<span>" + ex.Message + "</span>";
//                    _contentIds.Text = ex.Message;
//                }
//            }

//            _history.Enabled = EktronProject.GetAllRetrieveDate(null).Length > 0;

//            _cancelProject.Enabled = EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Cancelled &&
//                EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.InProduction &&
//                EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Completed &&
//                EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Closed;

//            _cancelProject.Attributes.Add("onclick", "return confirm('Are you sure you want to cancel this project?');return false;");

//            if (_historyPanel.Visible)
//            {
//                Table table = new Table();

//                string genericBorderStyle = "blue 1px solid";

//                table.Width = new Unit("100%");
//                table.Style.Add("border-right", genericBorderStyle);
//                table.Style.Add("border-top", genericBorderStyle);
//                table.Style.Add("border-left", genericBorderStyle);
//                table.Style.Add("border-bottom", genericBorderStyle);
//                table.CellPadding = 0;
//                table.CellSpacing = 0;

//                TableHeaderRow headerRow = new TableHeaderRow();

//                TableCell headerCell1 = new TableCell();
//                headerCell1.Text = "Target Language(s)";
//                headerCell1.HorizontalAlign = HorizontalAlign.Center;
//                headerCell1.Style.Add("border-right", genericBorderStyle);

//                headerRow.Cells.Add(headerCell1);

//                TableCell headerCell2 = new TableCell();
//                headerCell2.Text = "Date of Retrieval";
//                headerCell2.HorizontalAlign = HorizontalAlign.Center;

//                headerRow.Cells.Add(headerCell2);

//                table.Rows.Add(headerRow);

//                foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
//                {
//                    DateTime[] retrieveDates = EktronProject.GetAllRetrieveDate(file);

//                    if (retrieveDates.Length == 0)
//                        continue;

//                    foreach (string targetLang in file.TargetLang)
//                    {
//                        CultureInfo targetCulture = new CultureInfo(targetLang);

//                        TableCell fileName = new TableCell();

//                        fileName.Text = targetCulture.DisplayName;
//                        fileName.RowSpan = retrieveDates.Length;
//                        fileName.VerticalAlign = VerticalAlign.Top;
//                        fileName.Style.Add("border-top", genericBorderStyle);
//                        fileName.Style.Add("border-right", genericBorderStyle);

//                        TableRow fileRow = new TableRow();

//                        fileRow.Cells.Add(fileName);

//                        bool firstDateRow = true;

//                        foreach (DateTime date in retrieveDates)
//                        {
//                            TableCell dateCell = new TableCell();

//                            dateCell.Text = date.ToString("g");

//                            if (firstDateRow)
//                            {
//                                dateCell.Style.Add("border-top", genericBorderStyle);
//                                firstDateRow = false;
//                            }

//                            fileRow.Cells.Add(dateCell);

//                            table.Rows.Add(fileRow);

//                            fileRow = new TableRow();
//                        }
//                    }
//                }

//                _historyPanel.Controls.Clear();
//                _historyPanel.Controls.Add(table);
//            }
//        }

//        if (!_retrieved)
//        {
//            if (EktronProject != null && _progressPanelVisible)
//                _progressPanel.Visible = EktronProject.IsJobRunning;
//            else
//                _progressPanel.Visible = false;
//            _errorPanel.Visible = false;
//        }

//        return;
//    }

    /// <summary>
    /// Loads the data for Ektron project
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
       

#if false
		UserAPI userApi = new UserAPI();

		// only an admin can remove project from the list
		_removeProject.Visible = userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers, userApi.UserId, false);
#endif

        if (EktronProject != null)
        {
            ContentAPI contentApi = new ContentAPI();
            LocalizationAPI locApi = new LocalizationAPI();

            #region
            if (EktronProject.FullStatus == null)
            {
                // Ektron project status updates
                EktronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
                EktronProject.GetFilesStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
            }

            //			if (!IsPostBack)
            {
                _statingDate.Text = EktronProject.ExpectedStartingDate.ToString("g");
                _deliveryDate.Text = EktronProject.ExpectedDeliveryDate.ToString("g");
                _pmLink.Text = EktronProject.FullStatus.ProjectManager.Value;
                _pmLink.NavigateUrl = "mailto:" + EktronProject.FullStatus.ProjectManager.Email;
            }

            //Project cost information
            //Default is 0.0$
            _projectCost.Text = (0.0).ToString("C");

            //Displays the project cost if project is in below state
            // Forcasted, InProduction,Closed or Completed
            switch (EktronProject.Status)
            {

                case LB.FreewayLib.VojoService.ProjectStatusCode.Forecasted:
                case LB.FreewayLib.VojoService.ProjectStatusCode.InProduction:
                case LB.FreewayLib.VojoService.ProjectStatusCode.Closed:
                case LB.FreewayLib.VojoService.ProjectStatusCode.Completed:
                    if (string.IsNullOrEmpty(EktronProject.ProjectOwner))
                        EktronProject.RetrieveDetails(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

                    _projectCost.Text = EktronProject.TotalPrice.ToString("C") + " " + EktronProject.Currency;

                    break;
            }

            //Quote accept reject functionality will be available 
            //only when the project status is Forcasted
            _freewaydashboardquotation.Visible = EktronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Forecasted;
            #endregion
            #region code hided
            //_files.Items.Clear();

            ////Hiding the file for retrival when the Project Status is Draft 
            //if (FileVisible)
            //{
            //    if (_files.Items.Count == 0)
            //    {
            //        bool canRetrieve = false;

            //        foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
            //        {
            //            CultureInfo sourceCulture = new CultureInfo(file.SourceLang);

            //            foreach (string targetLang in file.TargetLang)
            //            {
            //                CultureInfo targetCulture = new CultureInfo(targetLang);

            //                ListItem item = new ListItem(string.Format("{0} (from {1})", targetCulture.DisplayName, sourceCulture.DisplayName));

            //                item.Value = file.Id;
            //                item.Enabled = file.Ready;
            //                if (!item.Enabled)
            //                    item.Text = string.Format("{0} (Not ready)", item.Text);
            //                else
            //                {
            //                    canRetrieve = true;

            //                    DateTime lastRetrieve = EktronProject.GetLastRetrieve(file);

            //                    if (lastRetrieve != DateTime.MinValue)
            //                        item.Text += string.Format(" - Last Retrieved: {0}", lastRetrieve.ToString("g"));
            //                }
            //                item.Selected = item.Enabled;

            //                _files.Items.Add(item);
            //            }
            //        }

            //        _retrieve.Enabled = canRetrieve;

            //    }
            //}
            #endregion

            #region File Info Treeview
            //Retrieve ProjectID, JobID and XLIFF files grouping 
            Hashtable XliffJobHashTable = getXliffJobHashTable();
            string contenterrormsg = null;
            System.Collections.Hashtable contentIdwithJobs = null;
            try
            {
                // Retrieve Ektron Project Jobs specific contents
                contentIdwithJobs = EktronProject.ContentswithJobs;
            }
            catch (Exception ex)
            {
                //bool enableventlog = false;

                ////checks if event log is enabled
                //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
                //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

                //if (enableventlog)
                //{
                //    EventLogs el = new EventLogs();
                //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
                //}
                contenterrormsg = ex.Message;
            }
           // _files.Nodes.Clear();
           //int  FilesNodeCount=0;
            //Hiding the file for retrival when the Project Status is Draft 
            //if (FileVisible)
            //{
            if (_files.Nodes.Count == 0)
            {
                bool canRetrieve = false;
                //_files.ID = "filesTreeView";
                foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
                {
                    CultureInfo sourceCulture = new CultureInfo(file.SourceLang);

                    foreach (string targetLang in file.TargetLang)
                    {
                        if(file!=null)
                            fldsetContentDetails.Visible = true;   
                        CultureInfo targetCulture = new CultureInfo(targetLang);

                        //ListItem item = new ListItem(string.Format("{0} (from {1})", targetCulture.DisplayName, sourceCulture.DisplayName));
                        TreeNode languagenode = new TreeNode(string.Format("{0} (from {1})", targetCulture.DisplayName, sourceCulture.DisplayName), string.Format("{0} (from {1})", targetCulture.DisplayName, sourceCulture.DisplayName));
                        // If base node doesn't contain the above treenode 
                        // Insert the language node under base node 
                        bool boolmatchingnode = false;
                        foreach (TreeNode childnode in _files.Nodes)
                        {
                            if (childnode.Text == string.Format("{0} (from {1})", targetCulture.DisplayName, sourceCulture.DisplayName))
                            {
                                boolmatchingnode = true;
                                languagenode = childnode;
                            }
                        }
                        if (!boolmatchingnode)
                        {
                            // Insert the language node under base node 
                            //languagenode.ShowCheckBox = file.Ready; 
                            //languagenode.Checked = false;
                            languagenode.ToolTip = "parentnode";
                            _files.Nodes.Add(languagenode);
                            //_files.Attributes.Add("onclick", "javascript:CheckEvent();");
                            _files.Attributes.Add("onclick", "OnTreeClick(event)");
                        }


                        //Adding the XLIFF file node with checkbox languagenode
                        TreeNode xlifffilenode = new TreeNode(file.Filename, file.Id);
                        // The file node will display checkbox 
                        // based on the file ready status
                        xlifffilenode.ShowCheckBox = file.Ready;
                        if (file.Ready)
                        {
                            canRetrieve = true;

                            DateTime lastRetrieve = EktronProject.GetLastRetrieve(file);

                            if (lastRetrieve != DateTime.MinValue)
                                xlifffilenode.Text += string.Format(" Last Retrieved: {0}", lastRetrieve.ToString("g"));
                        }
                        else
                            xlifffilenode.Text = string.Format("{0} (Not ready)", xlifffilenode.Text);

                        xlifffilenode.Checked = false;
                        xlifffilenode.ToolTip = "childnode";
                        languagenode.ChildNodes.Add(xlifffilenode);
                        //for (int cnt = 0; cnt < languagenode.ChildNodes.Count; cnt++)
                        //{
                        //    if (languagenode.ChildNodes[cnt].ShowCheckBox == true)
                        //        languagenode.ShowCheckBox = true;
                        //}
                        //Retrieve Xliff file specific contents

                        StringBuilder sb = new StringBuilder();
                        string xlifffilenodetext = string.Empty;
                        if (xlifffilenode.Text.Contains(" Last Retrieved: "))
                            xlifffilenodetext = xlifffilenode.Text.Remove(xlifffilenode.Text.IndexOf(" Last Retrieved: "));
                        else if (xlifffilenode.Text.Contains(" (Not ready)"))
                            xlifffilenodetext = xlifffilenode.Text.Remove(xlifffilenode.Text.IndexOf(" (Not ready)"));
                        else
                            xlifffilenodetext = xlifffilenode.Text;

                        if (EktronProject.XliffFilesAvailable.Length > 0 && EktronProject.XliffFilesAvailable.Contains(","))
                        {
                            string[] strXliffFiles = EktronProject.XliffFilesAvailable.Split(',');
                            foreach (string XliffFileJobid in strXliffFiles)
                            {
                                string[] strXliffFileJob = XliffFileJobid.Split('*');
                                bool dobreakloop = false;
                                if (strXliffFileJob.Length > 0 && strXliffFileJob.Length < 3)
                                {
                                    string XliffFile = strXliffFileJob[0].ToString();
                                    string XliffJobid = strXliffFileJob[1].ToString();
                                    if (xlifffilenodetext.Length > 0 && XliffFile.Trim() == xlifffilenodetext.Trim())
                                    {
                                        if (contentIdwithJobs != null)
                                        {
                                            string[] contents = contentIdwithJobs[XliffJobid].ToString().Split('*');
                                            //Adding the XLIFF Content files node with checkbox languagenode
                                            foreach (string content in contents)
                                            {
                                                if (content.Trim().Length > 0)
                                                {
                                                    TreeNode xliffcontentsfilenode = new TreeNode(content, xlifffilenodetext);
                                                    xlifffilenode.ChildNodes.Add(xliffcontentsfilenode);
                                                }
                                            }

                                            #region
                                            //LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(Convert.ToInt64(XliffJobid));
                                            //System.Collections.Generic.List<long> _contentIds = new System.Collections.Generic.List<long>();
                                            //for (int job = 0; job < jobs.Rows.Count; job++)
                                            //{
                                            //    LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

                                            //    LocalizationJobDataSet.LocalizationSkeletonDataTable contentItems = locApi.GetContentItemsByJob(jobRow.JobID);

                                            //    if (contentItems.Rows.Count == 0)
                                            //        continue;

                                            //    for (int item = 0; item < contentItems.Rows.Count; item++)
                                            //    {
                                            //        LocalizationJobDataSet.LocalizationSkeletonRow contentItem = contentItems[item];

                                            //        if (!_contentIds.Contains(contentItem.ItemID))
                                            //            _contentIds.Add(contentItem.ItemID);
                                            //    }
                                            //}
                                            //_contentIds.Sort();

                                            //foreach (int id in _contentIds)
                                            //{
                                            //    ContentData content = contentApi.GetContentById(id, ContentAPI.ContentResultType.Published);

                                            //    if (content == null)
                                            //        throw new Exception("Content id - " + id + " is not available and throwing invalid Content id exception.");

                                            //    if (_contentIds[_contentIds.Count - 1] != id)
                                            //        sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcn.gif"));
                                            //    else
                                            //        sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcln.gif"));
                                            //    sb.Append(string.Format(@"<a href=""../content.aspx?action=View&folder_id=0&id={0}&LangType={2}&callerpage=content.aspx&origurl=action%3dViewContentByCategory%26id%3d0"">{0} - {1}</a><br />", content.Id, content.Title, EktronProject.SourceLangId));
                                            //}

                                            //if (sb.ToString().Trim().Length == 0)
                                            //{
                                            //    sb.Append("<span>" + "No Contents are available." + "</span>");
                                            //}
                                            //dobreakloop = true;
                                            //break;

                                            #endregion
                                            dobreakloop = true;
                                        }
                                        else
                                        {
                                            TreeNode xliffcontentsfilenode = new TreeNode(contenterrormsg, contenterrormsg);
                                            xlifffilenode.ChildNodes.Add(xliffcontentsfilenode);
                                        }
                                    }
                                    if (dobreakloop)
                                        break;
                                }
                            }
                        }
                    }
                }
                //for (int i = 0; i < _files.Nodes.Count; i++)
                //{
                //    TreeNode langNode = _files.Nodes[i];
                //    if (langNode.ChildNodes.Count > 0)
                //    {
                //        for (int cnt = 0; cnt < langNode.ChildNodes.Count; cnt++)
                //        {
                //            if (langNode.ChildNodes[cnt].ShowCheckBox == true)
                //            {
                //                langNode.ShowCheckBox = true;
                //                break;
                //            }
                //        }
                //    }
                //}
                _retrieve.Enabled = canRetrieve;

            }
           
            
            #endregion

            #region Commented code
            //Hiding the hardcoded file treenode
            //_files.Nodes.Clear();
            //if (FileVisible)
            //{
            //    TreeNode _firstlanguagenode = new TreeNode("German (Germany) (from English (United States))", "German (Germany) (from English (United States))");
            //    _files.Nodes.Add(_firstlanguagenode);
            //    TreeNode _firstxliffforfirstlanguagenode = new TreeNode("xlf20100730T005330_u1_040a-es-ES.zip - Last Retrieved: 8/11/2010 10:54 AM");
            //    _firstlanguagenode.ChildNodes.Add(_firstxliffforfirstlanguagenode);
            //    _firstxliffforfirstlanguagenode.ShowCheckBox = true;
            //    TreeNode _secondxliffforfirstlanguagenode = new TreeNode("xlf31100730T005330_u1_040a-es-ES.zip - Last Retrieved: 8/01/2010 8:14 PM");
            //    _firstlanguagenode.ChildNodes.Add(_secondxliffforfirstlanguagenode);
            //    _secondxliffforfirstlanguagenode.ShowCheckBox = true;
            //    TreeNode _firstcontentforfirstxliffnode = new TreeNode("60 - Partner Requirements");
            //    _firstxliffforfirstlanguagenode.ChildNodes.Add(_firstcontentforfirstxliffnode);
            //    TreeNode _secondcontentforfirstxliffnode = new TreeNode("935 - Ektron In The News");
            //    _secondxliffforfirstlanguagenode.ChildNodes.Add(_secondcontentforfirstxliffnode);
            //    TreeNode _thirdcontentforfirstxliffnode = new TreeNode("936 - EktronTech Hearabout");
            //    _secondxliffforfirstlanguagenode.ChildNodes.Add(_thirdcontentforfirstxliffnode);

            //    TreeNode _secondlanguagenode = new TreeNode("French (France) (from English (United States))", "French (France) (from English (United States))");
            //    _files.Nodes.Add(_secondlanguagenode);
            //    TreeNode _secondxliffnode = new TreeNode("xlf20100730T005330_u1_040c-fr-FR.zip - Last Retrieved: 8/02/2010 7:00 AM");
            //    _secondlanguagenode.ChildNodes.Add(_secondxliffnode);
            //    _secondxliffnode.ShowCheckBox = true;
            //    TreeNode _secondcontentnode = new TreeNode("58 - Partner Benefits");
            //    _secondxliffnode.ChildNodes.Add(_secondcontentnode);

            //}
            #endregion

            #region
            //Target Files information will be visible   
            //only when the project status is not Draft
            //tbltargetfiles.Visible = FileVisible;
            tbltargetfiles.Visible = true;

            //Add and View task functionality will be available   
            //only when the project status is Draft
            tbladdviewtask.Visible = !FileVisible;
            #endregion

            #region Content display code
            // display Content ID for that project/job
            if (_contentIdsPanel.Visible)
            {
                try
                {
                    _contentIds.Text = string.Empty;

                    foreach (int id in EktronProject.ContentIds)
                        _contentIds.Text += string.Format("{0}, ", id);

                    _contentIds.Text = _contentIds.Text.TrimEnd(",".ToCharArray());


                    StringBuilder sb = new StringBuilder();

                    foreach (int id in EktronProject.ContentIds)
                    {

                        ContentData content = contentApi.GetContentById(id, ContentAPI.ContentResultType.Published);

                        if (content == null)
                            throw new Exception("Content id - " + id + " is not available and throwing invalid Content id exception.");

                        if (EktronProject.ContentIds[EktronProject.ContentIds.Length - 1] != id)
                            sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcn.gif"));
                        else
                            sb.Append(string.Format(@"<img src=""{0}""/>", "../images/application/folders/fcln.gif"));
                        sb.Append(string.Format(@"<a href=""../content.aspx?action=View&folder_id=0&id={0}&LangType={2}&callerpage=content.aspx&origurl=action%3dViewContentByCategory%26id%3d0"">{0} - {1}</a><br />", content.Id, content.Title, EktronProject.SourceLangId));
                    }

                    _contentIdsDiv.InnerHtml = sb.ToString();

                    if (_contentIdsDiv.InnerHtml.Trim().Length == 0)
                    {
                        _contentIdsDiv.InnerHtml = "<span>" + "No Contents are available." + "</span>";
                        _contentIds.Text = "No Contents are available.";
                    }
                }
                catch (Exception ex)
                {
                    //bool enableventlog = false;

                    ////checks if event log is enabled
                    //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
                    //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

                    //if (enableventlog)
                    //{
                    //    EventLogs el = new EventLogs();
                    //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
                    //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
                    //}
                    _contentIdsDiv.InnerHtml = "<span>" + ex.Message + "</span>";
                    _contentIds.Text = ex.Message;
                }
            }
            #endregion

            #region History Info
            //_history.Enabled = EktronProject.GetAllRetrieveDate(null).Length > 0;

            //_cancelProject.Enabled = EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Cancelled &&
            //    EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.InProduction &&
            //    EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Completed &&
            //    EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Closed;

            //_cancelProject.Attributes.Add("onclick", "return confirm('Are you sure you want to cancel this project?');return false;");

            //if (_historyPanel.Visible)
            //{
            //    Table table = new Table();

            //    string genericBorderStyle = "blue 1px solid";

            //    table.Width = new Unit("100%");
            //    table.Style.Add("border-right", genericBorderStyle);
            //    table.Style.Add("border-top", genericBorderStyle);
            //    table.Style.Add("border-left", genericBorderStyle);
            //    table.Style.Add("border-bottom", genericBorderStyle);
            //    table.CellPadding = 0;
            //    table.CellSpacing = 0;

            //    TableHeaderRow headerRow = new TableHeaderRow();

            //    TableCell headerCell1 = new TableCell();
            //    headerCell1.Text = "File(s)";
            //    headerCell1.HorizontalAlign = HorizontalAlign.Center;
            //    headerCell1.Style.Add("border-right", genericBorderStyle);
            //    headerCell1.Style.Add("font-weight", "bold");
            //    headerCell1.Style.Add("font-size", "11px");
            //    headerCell1.Style.Add("text-align", "center");


            //    headerRow.Cells.Add(headerCell1);

            //    TableCell headerCell2 = new TableCell();
            //    headerCell2.Text = "Date of Retrieval";
            //    headerCell2.HorizontalAlign = HorizontalAlign.Center;
            //    headerCell2.Style.Add("text-align", "center");
            //    headerCell2.Style.Add("font-weight", "bold");
            //    headerCell2.Style.Add("font-size", "11px");

            //    headerRow.Cells.Add(headerCell2);

            //    table.Rows.Add(headerRow);

            //    foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
            //    {

            //        DateTime[] retrieveDates = EktronProject.GetAllRetrieveDate(file);

            //        if (retrieveDates.Length == 0)
            //            continue;

            //        foreach (string targetLang in file.TargetLang)
            //        {
            //            CultureInfo targetCulture = new CultureInfo(targetLang);
            //            CultureInfo SourceCulture = new CultureInfo(file.SourceLang);

            //            TableCell languageName = new TableCell();

            //            languageName.Text = string.Format("{0} (from {1})", targetCulture.DisplayName, SourceCulture.DisplayName);
            //            //targetCulture.DisplayName + "(" + source;
            //            languageName.ColumnSpan = 2;
            //            languageName.VerticalAlign = VerticalAlign.Top;
            //            languageName.Style.Add("border-top", genericBorderStyle);
            //            languageName.Style.Add("border-right", genericBorderStyle);
            //            languageName.Style.Add("font-weight", "bold");
            //            languageName.Style.Add("font-size", "11px");

            //            TableRow languageNameRow = new TableRow();

            //            languageNameRow.Cells.Add(languageName);
            //            int rowid = int.MinValue;
            //            string strbreak = string.Empty;
            //            foreach (TableRow tr in table.Rows)
            //            {
            //                if (tr.Cells[0].Text == languageName.Text)
            //                {
            //                    //for (int i = table.Rows.GetRowIndex(tr) + 1; i < table.Rows.Count - table.Rows.GetRowIndex(tr) - 1; i++)
            //                    for (int i = table.Rows.GetRowIndex(tr); i < table.Rows.Count; i++)
            //                    {
            //                        if (table.Rows[i].Cells[0].ColumnSpan == 2)
            //                        {
            //                            rowid = i - 1;
            //                            //rowid = i;
            //                        }
            //                        if(table.Rows.Count == i)
            //                            strbreak = "break";
            //                    }
            //                }
            //                if (strbreak == "break")
            //                    break;
            //            }
            //            if (rowid == int.MinValue)
            //                table.Rows.Add(languageNameRow);

            //            languageNameRow = new TableRow();

            //            TableCell fileName = new TableCell();
            //            fileName.Text = file.Filename;
            //            //targetCulture.DisplayName + "(" + source;
            //            fileName.RowSpan = retrieveDates.Length;
            //            fileName.VerticalAlign = VerticalAlign.Top;
            //            fileName.Style.Add("border-top", genericBorderStyle);
            //            fileName.Style.Add("border-right", genericBorderStyle);

            //            TableRow fileRow = new TableRow();

            //            fileRow.Cells.Add(fileName);

            //            bool firstDateRow = true;

            //            foreach (DateTime date in retrieveDates)
            //            {
            //                TableCell dateCell = new TableCell();

            //                dateCell.Text = date.ToString("g");

            //                if (firstDateRow)
            //                {
            //                    dateCell.Style.Add("border-top", genericBorderStyle);
            //                    firstDateRow = false;
            //                }

            //                fileRow.Cells.Add(dateCell);

            //                if (rowid == int.MinValue)
            //                    table.Rows.Add(fileRow);
            //                else
            //                {
            //                    //table.Rows.AddAt(rowid + 1, fileRow);
            //                    table.Rows.AddAt(rowid , fileRow);
            //                }

            //                fileRow = new TableRow();
            //            }
            //        }
            //    }

            //    _historyPanel.Controls.Clear();
            //    _historyPanel.Controls.Add(table);
            //}
            #endregion

            #region History Info
            _history.Enabled = EktronProject.GetAllRetrieveDate(null).Length > 0;

            _cancelProject.Enabled = EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Cancelled &&
                EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.InProduction &&
                EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Completed &&
                EktronProject.Status != LB.FreewayLib.VojoService.ProjectStatusCode.Closed;

            _cancelProject.Attributes.Add("onclick", "return confirm('Are you sure you want to cancel this project?');return false;");

            if (_historyPanel.Visible)
            {
                Table table = new Table();

                string genericBorderStyle = "Navy 1px solid";

                //table.Width = new Unit("100%");
                table.Style.Add("width", "100%");
                table.Style.Add("border-right", genericBorderStyle);
                table.Style.Add("border-top", genericBorderStyle);
                table.Style.Add("border-left", genericBorderStyle);
                table.Style.Add("border-bottom", genericBorderStyle);
                table.CellPadding = 0;
                table.CellSpacing = 0;

                TableHeaderRow headerRow = new TableHeaderRow();

                TableCell headerCell1 = new TableCell();
                headerCell1.Text = "File(s)";
                headerCell1.HorizontalAlign = HorizontalAlign.Center;
                headerCell1.Style.Add("border-right", genericBorderStyle);
                headerCell1.Style.Add("font-weight", "bold");
                headerCell1.Style.Add("font-size", "12px");
                headerCell1.Style.Add("color", "white");
                headerCell1.Style.Add("text-align", "left");
                headerCell1.Style.Add("background-color", "Gray");
                

                headerRow.Cells.Add(headerCell1);

                TableCell headerCell2 = new TableCell();
                headerCell2.Text = "Date of Retrieval";
                headerCell2.HorizontalAlign = HorizontalAlign.Center;
                headerCell2.Style.Add("text-align", "left");
                headerCell2.Style.Add("font-weight", "bold");
                headerCell2.Style.Add("font-size", "12px");
                headerCell2.Style.Add("color", "white");
                headerCell2.Style.Add("background-color", "Gray");

                headerRow.Cells.Add(headerCell2);

                table.Rows.Add(headerRow);

                
                foreach (LB.FreewayLib.Freeway.VojoFile file in EktronProject.Files)
                {

                    DateTime[] retrieveDates = EktronProject.GetAllRetrieveDate(file);

                    if (retrieveDates.Length == 0)
                        continue;

                    foreach (string targetLang in file.TargetLang)
                    {
                        CultureInfo targetCulture = new CultureInfo(targetLang);
                        CultureInfo SourceCulture = new CultureInfo(file.SourceLang);

                        TableCell languageName = new TableCell();

                        languageName.Text = string.Format("{0} (from {1})", targetCulture.DisplayName, SourceCulture.DisplayName);
                        //targetCulture.DisplayName + "(" + source;
                        languageName.ColumnSpan = 2;
                        languageName.VerticalAlign = VerticalAlign.Top;
                        languageName.Style.Add("border-top", genericBorderStyle);
                        languageName.Style.Add("border-right", genericBorderStyle);
                        languageName.Style.Add("font-weight", "bold");
                        languageName.Style.Add("font-size", "11px");

                        TableRow languageNameRow = new TableRow();

                        languageNameRow.Cells.Add(languageName);
                        int rowid = int.MinValue;
                        string strbreak = string.Empty;
                        foreach (TableRow tr in table.Rows)
                        {
                        //    if (tr.Cells[0].Text == languageName.Text)
                        //    {
                        //        //for (int i = table.Rows.GetRowIndex(tr) + 1; i < table.Rows.Count - table.Rows.GetRowIndex(tr) - 1; i++)
                        //        for (int i = table.Rows.GetRowIndex(tr); i < table.Rows.Count; i++)
                        //        {
                        //            if (table.Rows[i].Cells[0].ColumnSpan == 2)
                        //            {
                        //                //rowid = i - 1;
                        //                if (i == 1)
                        //                    rowid = i + 1;
                        //                else
                        //                    rowid = i;
                        //                //rowid = i + 1;
                        //            }
                        //            if (table.Rows.Count == i)
                        //                strbreak = "break";
                        //        }
                        //    }
                        //    if (strbreak == "break")
                        //        break;

                            if (tr.Cells[0].Text == languageName.Text)
                            {
                                rowid = table.Rows.GetRowIndex(tr) + 1;
                                break;
                            }


                        }
                        if (rowid == int.MinValue)
                            table.Rows.Add(languageNameRow);

                        languageNameRow = new TableRow();

                        TableCell fileName = new TableCell();
                        fileName.Text = file.Filename;
                        //targetCulture.DisplayName + "(" + source;
                        fileName.RowSpan = retrieveDates.Length + 1;
                        fileName.VerticalAlign = VerticalAlign.Top;
                        fileName.Style.Add("border-top", genericBorderStyle);
                        fileName.Style.Add("border-right", genericBorderStyle);
                        fileName.Style.Add("vertical-align", "top");
                        TableRow fileRow = new TableRow();

                        
                        fileRow.Cells.Add(fileName);

                        if (rowid == int.MinValue)
                        {
                            rowid = table.Rows.Add(fileRow);
                            rowid++;
                        }
                        else
                        {
                            //table.Rows.AddAt(rowid + 1, fileRow);
                            try
                            {
                                table.Rows.AddAt(rowid, fileRow);
                                rowid++;
                            }
                            catch(Exception ex)
                            {
                                //bool enableventlog = false;

                                ////checks if event log is enabled
                                //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
                                //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

                                //if (enableventlog)
                                //{
                                //    EventLogs el = new EventLogs();
                                //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
                                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
                                //}
                                table.Rows.Add(fileRow);
                                rowid++;
                            }
                        }


                        fileRow = new TableRow();

                        bool firstDateRow = true;

                        foreach (DateTime date in retrieveDates)
                        {
                            TableCell dateCell = new TableCell();

                            dateCell.Text = date.ToString("g");

                            if (firstDateRow)
                            {
                                dateCell.Style.Add("border-top", genericBorderStyle);
                                firstDateRow = false;
                            }

                            fileRow.Cells.Add(dateCell);

                            try
                            {
                                table.Rows.AddAt(rowid, fileRow);
                                rowid++;
                            }
                            catch(Exception ex)
                            {
                                //bool enableventlog = false;

                                ////checks if event log is enabled
                                //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
                                //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

                                //if (enableventlog)
                                //{
                                //    EventLogs el = new EventLogs();
                                //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
                                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
                                //}
                                rowid = table.Rows.Add(fileRow);
                                rowid++;
                            }

                            fileRow = new TableRow();

                        }



                    }
                }

                _historyPanel.Controls.Clear();                           
                _historyPanel.Controls.Add(table);
               

            }
            #endregion
        }

        #region Retrieve Info
        if (!_retrieved)
        {
            if (EktronProject != null && _progressPanelVisible)
                _progressPanel.Visible = EktronProject.IsJobRunning;
            else
                _progressPanel.Visible = false;
            _errorPanel.Visible = false;
        }
        #endregion

        return;
    }




  

    #endregion

    #region Properties
    public EktronProject EktronProject
    {
        get
        {
            if (_ektronProject == null && !string.IsNullOrEmpty(ProjectId) && EktronProjectCollection.CurrentProjects != null)
                _ektronProject = EktronProjectCollection.CurrentProjects[ProjectId];

            return _ektronProject;
        }
        set
        {
            _ektronProject = value;

            ProjectId = value.Id;

            return;
        }
    }
    public string ProjectId
    {
        get { return ViewState["ProjectId"] as string; }
        set { ViewState["ProjectId"] = value; }
    }
    /// <summary>
    /// Returns the Project status whether Draft or not
    /// </summary>
    public bool FileVisible
    {
        get
        {
            bool returnval = true;
            if (EktronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Draft)
            {
                returnval = false;
            }

            return returnval;
        }
    }
    /// <summary>
    /// Invisibling the btnsubProj, chkquote and divContents
    /// controls when Project status is Draft and 
    /// No content is added in Project
    /// </summary>
    public bool SubmitProjectVisible
    {
        set
        {
            //if (!value)
            //{
                //btnsubProj.Visible = value;
               // btnsubProjWithQuote.Visible = value;
                btnsubProj.Enabled = value;
                btnsubProjWithQuote.Enabled = value;

		//Hides the content from display
		//divContents.Visible = value;
          //  }
        }
    }

    /// <summary>
    /// Invisibling the btnsubProj, chkquote and divContents
    /// controls when Project status is Draft and 
    /// No content is added in Project
    /// </summary>
    public string ParentPath
    {
        get
        {
            return _parentPath;
        }
        set
        {
            _parentPath = value;
        }
    }

    #endregion

    //#region Methods
    ///// <summary>
    ///// Retrieves the selected target languages translated files from Freeway 
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void _retrieve_Click(object sender, EventArgs e)
    //{
    //    if (EktronProject == null)
    //    {
    //        Page.Response.Redirect("./freewaydashboard.aspx", true);

    //        return;
    //    }
    //    // If XLIFF files importing job is running then on click of retrieve button
    //    // "Import in progress. Please wait." meesage will get populated
    //    if (isImportJobRunning())
    //    {
    //        _errorPanel.Visible = true;
    //        _errorLabel.Text = "Import in progress. Please wait.";
    //        //_progressPanel.Visible = _progressPanelVisible;
    //        _retrieved = true;

    //        return;
    //    }
    //    else
    //    {
    //        _errorPanel.Visible = false;
    //    }

    //    try
    //    {
    //        //Importing files functionality
    //        if (this.EktronProject.ImportFiles(_files.Items, true))
    //        {
    //            _retrieved = true;

    //            _progressPanel.Visible = _progressPanelVisible;


    //            Session["ForceDaskboardRefresh"] = "Please";
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        // On exception the error panel will be visible and progressbar will be invisible.
    //        _errorPanel.Visible = true;
    //        _errorLabel.Text = ex.Message;
    //        _progressPanel.Visible = _progressPanelVisible;
    //        _retrieved = true;
    //    }

    //    return;
    //}

    #region Methods

    //public event RetrieveClickEventHandler RetrieveClickEvent;
    //public delegate void RetrieveClickEventHandler(object sender,
    //RetrieveClickEventArgs e);
    //public class RetrieveClickEventArgs : EventArgs
    //{
    //    private int _reached;
    //    public RetrieveClickEventArgs(int num)
    //    {
    //        this._reached = num;
    //    }
    //    public int ReachedNumber
    //    {
    //        get
    //        {
    //            return _reached;
    //        }
    //    }
    //}
    //private void this_RetrieveClickEvent(object sender, RetrieveClickEventArgs e)
    //{
    //    MessageBox.Show("Reached: ");
    //}
    /// <summary>
    /// Retrieves the selected target languages translated files from Freeway 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _retrieve_Click(object sender, EventArgs e)
    {
        if (EktronProject == null)
        {
            Page.Response.Redirect("./freewaydashboard.aspx", true);
           
            return;
        }
        // If XLIFF files importing job is running then on click of retrieve button
        // "Import in progress. Please wait." meesage will get populated
        if (isImportJobRunning())
        {
            _errorPanel.Visible = true;
            _errorLabel.Text = "Import in progress. Please wait.";
            //_progressPanel.Visible = _progressPanelVisible;
            _retrieved = true;

            return;
        }
        else
        {
            _errorPanel.Visible = false;
        }
        try
        {

            //this.RetrieveClickEvent += new RetrieveClickEventHandler(this_RetrieveClickEvent);

            //Importing files functionality
           
            if (_files.CheckedNodes.Count >0)
            {               
                
                if (this.EktronProject.ImportFiles(_files, true, true))
                {

                    _retrieved = true;
                    _progressPanel.Visible = _progressPanelVisible;
                    Session["ForceDaskboardRefresh"] = "Please";
                    lblerror.Visible = false;
                   // lblPleaseWait.Text = "";
                  //  _retrieve.Visible = true;

                }
            }
            else
            {
                lblerror.Visible = true;
                lblerror.Text = "Please select the file to retrieve.";
                //string strerror = "javascript:alert('Please select the file to retieve.');";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", strerror, true);
            }
        }
        catch (Exception ex)
        {
            //bool enableventlog = false;

            ////checks if event log is enabled
            //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
            //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

            //if (enableventlog)
            //{
            //    EventLogs el = new EventLogs();
            //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
            //}

            // On exception the error panel will be visible and progressbar will be invisible.
            _errorPanel.Visible = true;
            _errorLabel.Text = ex.Message;
            _progressPanel.Visible = _progressPanelVisible;
            _retrieved = true;
        }

        return;
    }


    /// <summary>
    /// Checks the retrieving of XLIFF files job is running
    /// </summary>
    /// <returns></returns>
    private bool isImportJobRunning()
    {
        // with this version, we can kick off another import
        if (FreewayConfiguration.Version75OrGreater)
            return false;

        LocalizationAPI locApi = new LocalizationAPI();
        //Identifies any job is running or not.
        DS.LocalizationJobDataTable jobs = locApi.GetJobs();

        for (int jobRowIndex = 0; jobRowIndex < jobs.Count; jobRowIndex++)
        {
            DS.LocalizationJobRow jobRow = jobs[jobRowIndex];

            if ((jobRow.State == (int)LocalizationJobDataSet.LocalizationJobRow.States.Running
                || jobRow.State == (int)LocalizationJobDataSet.LocalizationJobRow.States.Initializing)
                && jobRow.JobType == (int)DS.LocalizationJobRow.Types.Import)
                return true;
        }

        // very dangerous because when a job is cancel, files are not deleted.
        // but this prevent normal user (not Admin) to check if there's import done by other user.
        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(locApi.GetTranslationUploadDirectory());

        if (directory.GetFiles().Length > 0)
            return true;

        return false;
    }
    /// <summary>
    /// Displays the history of the retrieved target languages XLIFF files with retrieved date and time.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _history_Click(object sender, EventArgs e)
    {

        if (_historyPanel.Visible)
        {
            _historyPanel.Visible = false;
            fldSetHistory.Visible = false;
        }
        else
        {
            _historyPanel.Visible = true;
            fldSetHistory.Visible = true;
        }
        _history.Text = _historyPanel.Visible ? "Hide History" : "View History";
      //  _history.ToolTip = _historyPanel.Visible ? "Hide History" : "View History";

        return;
    }
    /// <summary>
    /// Abendons the project in Freeway and project status changed to Cancelled
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _cancelProject_Click(object sender, EventArgs e)
    {
        try
        {
            if (EktronProject != null)
            {
                EktronProject.Cancel(FreewayConfiguration.IsExecutiveUser ?
                                    FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

                EktronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
                                    FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

                Session["ForceDaskboardVisibleRefresh"] = "Please";
            }
        }
        catch (Exception ex)
        {
            //bool enableventlog = false;

            ////checks if event log is enabled
            //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
            //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

            //if (enableventlog)
            //{
            //    EventLogs el = new EventLogs();
            //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
            //}
            string strerror = "javascript:alert('" + ex.Message + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", strerror, true);
        }

        //return;
    }
    /// <summary>
    /// Archives the project from Dashboard 
    /// so the project will not be available for future reference
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _removeProject_Click(object sender, EventArgs e)
    {
        if (EktronProject != null)
        {
            EktronProject.Visible = false;
            if (EktronProjectCollection.CurrentProjects != null)
                EktronProjectCollection.CurrentProjects.Remove(EktronProject);

            Session["ForceDaskboardVisibleRefresh"] = "Please";
        }

        return;
    }

    /// <summary>
    /// On expanding the contents '+' button the contents will be visible 
    /// and on collapsing the contents will be invisible 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _showHideContentIds_Click(object sender, EventArgs e)
    {
        _contentIdsPanel.Visible = !_contentIdsPanel.Visible;
        if (_contentIdsPanel.Visible)
        {
            _showHideContentIdsImage.ImageUrl = "~/Workarea/images/application/folders/fcmn.gif";
            divContents.Style.Add("height", "105px");
        }
        else
        {
            _showHideContentIdsImage.ImageUrl = "~/Workarea/images/application/folders/fcpln.gif";
            divContents.Style.Remove("height");
        }

        return;
    }
    #endregion

    protected void btnsubProjWithQuote_Click(object sender, EventArgs e)
    {
        SubmitProject(true);
        return;
    }
    protected void btnsubProj_Click(object sender, EventArgs e)
    {
        SubmitProject(false);
        return;
    }


    private void SubmitProject(bool withQuote)
    {
        if (EktronProject != null)
        {
            Session["projectid"] = EktronProject.Id;
            bool exceptionraised = false;
            try
            {

                string ComponentsValue = string.Empty;
                string SubjectsValue = string.Empty;
                string TasksValue = string.Empty;
                string SubtasksValue = string.Empty;
                double VolumeValue = double.MinValue;
                string UOMValue = string.Empty;

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ComponentsValue"]))
                    ComponentsValue = System.Configuration.ConfigurationManager.AppSettings["ComponentsValue"].ToString();
                else
                    ComponentsValue = "Web";

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["SubjectsValue"]))
                    SubjectsValue = System.Configuration.ConfigurationManager.AppSettings["SubjectsValue"].ToString();
                else
                    SubjectsValue = "std";

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["TasksValue"]))
                    TasksValue = System.Configuration.ConfigurationManager.AppSettings["TasksValue"].ToString();
                else
                    TasksValue = "LP";

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["SubtasksValue"]))
                    SubtasksValue = System.Configuration.ConfigurationManager.AppSettings["SubtasksValue"].ToString();
                else
                    SubtasksValue = "New Words";

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["SubtasksValue"]))
                    VolumeValue = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["VolumeValue"]);
                else    
                    VolumeValue = 5;

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["UOMValue"]))
                    UOMValue = System.Configuration.ConfigurationManager.AppSettings["UOMValue"].ToString();
                else
                    UOMValue = "Word";

                FileStatusList fileStatus = FreewayConfiguration.GetFileStatus(Convert.ToString(EktronProject.Id));
                foreach (FileStatus status in fileStatus.FileStatuses)
                {
                    FreewayConfiguration.AddTask(EktronProject.Id,
                    status.SourceLanguageID,
                    status.TargetLanguageID,
                    ComponentsValue,
                    SubjectsValue,
                    TasksValue,
                    SubtasksValue,
                    VolumeValue,
                    UOMValue);
                }


            }
            catch (Exception ex)
            {
                //bool enableventlog = false;

                ////checks if event log is enabled
                //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
                //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

                //if (enableventlog)
                //{
                //    EventLogs el = new EventLogs();
                //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
                //}
                exceptionraised = true;
                string alertmessage = "javascript:alert('Tasks information configured are not appropriate. Please check the configuration file.');";
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert Message", alertmessage, true);
            }

            try
            {

                if (!exceptionraised)
                {

                    EktronProject.Submit(true, withQuote);


                }
            }
            catch (Exception ex)
            {
                //bool enableventlog = false;

                ////checks if event log is enabled
                //if (!string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EventLog"])))
                //    enableventlog = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLog"]);

                //if (enableventlog)
                //{
                //    EventLogs el = new EventLogs();
                //    el.AddException(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.TargetSite);
                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "alert('One or more error occured.Please consult the Windows Event Log for More details (EktronFreewayLog).');", true);
                //}
                string alertmessage = string.Empty;
               
                alertmessage = "javascript:alert('For Project Id - " + EktronProject.Id + " unhandled exception is raised. ');";
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert Message", alertmessage, true);
            }
            EktronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

            Session["ForceDaskboardVisibleRefresh"] = "Please";


        }
    }

    #region Private Functions
    Hashtable getXliffJobHashTable()
    {
        Hashtable XliffJobHashTable = new Hashtable();
        LocalizationAPI locApi = new LocalizationAPI();

        if (EktronProject.JobsAvailable.Length > 0)
        {
            string[] jobcollection = EktronProject.JobsAvailable.Split(',');

            for (int i = 0; i < jobcollection.Length; i++)
            {
                long jobid = Convert.ToInt64(jobcollection[i]);
                LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(jobid);

                for (int job = 0; job < jobs.Rows.Count; job++)
                {
                    LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

                    LocalizationJobDataSet.LocalizationJobFileDataTable xliffItems = locApi.GetFilesByJob(jobRow.JobID);
                    foreach (DataRow xliffRow in xliffItems.Rows)
                    {
                        if (!XliffJobHashTable.ContainsKey(xliffItems.FileNameColumn))
                            XliffJobHashTable.Add(xliffRow[xliffItems.FileNameColumn], jobRow.JobID);
                    }
                }
            }
        }

        return XliffJobHashTable;
    }
    #endregion
}
#endregion
