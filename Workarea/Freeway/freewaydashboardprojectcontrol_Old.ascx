<%@ Control Language="C#" AutoEventWireup="true" CodeFile="freewaydashboardprojectcontrol_Old.ascx.cs"
    Inherits="freewaydashboardprojectcontrol_Old" %>
<%@ Register Src="freewaydashboardquotation.ascx" TagName="freewaydashboardquotation"
    TagPrefix="uc1" %>

<script type="text/javascript">
var filesSelected=false;
	 function AddViewTask(projectid)
	 {
	    var url = "FreewayAddViewTask.aspx?projectid="+projectid;
	    window.open(url,"FreewayAddViewTask","status = yes,resizable = no,top = 0, left = 0,height = 480, width = 1012");
	 }
        function public_GetParentByTagName(element, tagName) 
        { 

        var parent = element.parentNode; 
        var upperTagName = tagName.toUpperCase(); 

        while (parent && (parent.tagName.toUpperCase() != upperTagName)) 
        { 
        parent = parent.parentNode ? parent.parentNode : 
        parent.parentElement; 
        } 
        return parent; 
        } 
        function setParentUnChecked(objParentDiv) 
        { 

        //var objParentDiv = public_GetParentByTagName(objNode,"INPUT"); 
        if(objParentDiv==null || objParentDiv == "undefined") 
        { 
        return; 
        } 
        var objID = objParentDiv.getAttribute("ID"); 
        objID = objID.substring(0,objID.indexOf("Nodes")); 
        objID = objID+"CheckBox"; 
        var objParentCheckBox = document.getElementById(objID); 
        if(objParentCheckBox==null || objParentCheckBox == "undefined") 
        { 
        return; 
        } 
        if(objParentCheckBox.tagName!="INPUT" && objParentCheckBox.type == 
        "checkbox") 
        return; 
        objParentCheckBox.checked = false; 
        setParentUnChecked(objParentCheckBox); 
        } 

        // set the children to be unchecked.
        function setChildUnChecked(divID) 
        { 
        var objchild = divID.children; 
        var count = objchild.length; 
        for(var i=0;i<objchild.length;i++) 
        { 
        var tempObj = objchild[i]; 
        if(tempObj.tagName=="INPUT" && tempObj.type == "checkbox") 
        { 
        tempObj.checked = false; 
        } 
        setChildUnChecked(tempObj); 
        } 
        } 
        // set the children to be checked
        function setChildChecked(divID) 
        { 
        //debugger;
        var objchild = divID.children; 
        var count = objchild.length; 
        for(var i=0;i<objchild.length;i++) 
        { 
        var tempObj = objchild[i]; 
        if(tempObj.tagName=="INPUT" && tempObj.type == "checkbox") 
        { 
        tempObj.checked = true; 
        } 
        setChildChecked(tempObj); 
        } 
        } 
        // trigger the event
        function CheckEvent() 
        {   
            debugger;
            var objNode = event.srcElement; 
            if(objNode.tagName!="INPUT" || objNode.type!="checkbox") 
            return; 
                if(objNode.checked==true) 
                { 
                filesSelected=true;
                ToggleParentCheckBox (objNode);
                //setParentChecked(objNode); 
                var objID = objNode.getAttribute("ID"); 
                var objID = objID.substring(0,objID.indexOf("CheckBox")); 
                var objParentDiv = document.getElementById(objID+"Nodes"); 
                    if(objParentDiv==null || objParentDiv == "undefined") 
                    { 
                    return; 
                    } 
                setChildChecked(objParentDiv); 
                } 
                else 
                { 
                   
                    var objID ;
                     var objIDSub ;
                      if(objNode.title=="parentnode")
                      {
                      objID=objNode.getAttribute("ID"); 
                       objIDSub= objID.substring(0,objID.indexOf("CheckBox")); 
                       objParentDiv= document.getElementById(objIDSub+"Nodes");               
                       setParentUnChecked(objParentDiv); 
                       setChildUnChecked(objParentDiv); 
                      }
                      else
                      {                     
                       objID=objNode.getAttribute("ID"); 
                       objIDSub= objID.substring(0,objID.indexOf("CheckBox")); 
                        var objParentDiv=null ;
                                if(objParentDiv==null || objParentDiv == "undefined") 
                                {
                                   var ParentDivObj;
                                   var iLoop=0;
                                   var NodeObj=objNode.parentNode;
                                        for (iLoop=1;iLoop>0;iLoop++)
                                        {                   
                                            if(NodeObj.tagName=="DIV")
                                            {
                                            setParentUnChecked(NodeObj);
                                            return;                            
                                            } 
                                             NodeObj=NodeObj.parentNode;
                                        } 
                               
                                }                 
                        } 
                }
     }


 //returns the ID of the parent container if the current checkbox is unchecked
    function GetParentNodeById(element, id)
    {   
        var parent = element.parentNode;
        if (parent == null)
        {
            return false;
        }
        if (parent.id.search(id) == -1)
        {
           parent=GetParentNodeById(parent, id);
          return parent;
        }
        else
        {
        return parent;
        }
    }
   
    function CheckParentIfAllChildNodesSelected(parentContainer, id)
    {   
        var childCheckBox;
        var ParentCheckBox ;
        
        var allChildrenSelected=1;
         var parentCheckBoxId = parentContainer.id.substring(0, parentContainer.id.search("Nodes")) + "CheckBox";
             ParentCheckBox=document.getElementById(parentCheckBoxId);
             if(ParentCheckBox!=null)
             {
                 if(ParentCheckBox.checked ==false)
                 {
                     for(var i=0;i<parentContainer.childNodes.length;i++)
                     {
                     if( parentContainer.childNodes[i].getElementsByTagName("INPUT").length>0 )
                        {
                            childCheckBox= document.getElementById ( parentContainer.childNodes[i].getElementsByTagName("INPUT")[0].id);
                            if(ParentCheckBox!=null)
                             {
                                if(childCheckBox.checked ==false)
                                 {
                                 allChildrenSelected=0;
                                 } 
                             }
                         }                 
                         
                     }
                     if(allChildrenSelected==1)
                     {
                     ParentCheckBox.checked=true;
                     }
                 }
            }
    }

function ToggleParentCheckBox(checkBox)
    {    
        if(checkBox.checked == true)
        {       
            var parentContainer = GetParentNodeById(checkBox, "_files");
            if(parentContainer) 
            {
                 CheckParentIfAllChildNodesSelected(parentContainer, "_files");
            }
        }
    }

function isFileSelected ()
    {    
//       debugger;    
        var returnval = true;
//        if(!filesSelected) 
//        {
//           alert('Please select the file to retieve.');
//           returnval = false;
//        }
//        else
//        {      
//        alert('Please Wait retieval process is started...');          
//        returnval =true;
//        }
        
        return  returnval;      
    }

</script>

<table width="100%" border="2" cellpadding="2"  cellspacing="2">
    <tr>
        <td width="40%" valign="top">
            <table width="100%" style="vertical-align: top;">
                <tr>
                    <td nowrap>
                        Starting Date:</td>
                    <td nowrap>
                        <asp:Label ID="_statingDate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td nowrap>
                        Delivery Date:</td>
                    <td nowrap>
                        <asp:Label ID="_deliveryDate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td nowrap>
                        Project Manager:</td>
                    <td nowrap>
                        <asp:HyperLink ID="_pmLink" runat="server">[_pmLink]</asp:HyperLink></td>
                </tr>
                <tr>
                    <td nowrap>
                        Project Cost:</td>
                    <td nowrap>
                        <asp:Label ID="_projectCost" runat="server">[_projectCost]</asp:Label></td>
                </tr>
                <uc1:freewaydashboardquotation ID="_freewaydashboardquotation" runat="server" Visible="true" />
                <tr>
                    <td colspan="2">
                        <div style="overflow: auto;" runat="server" id="divContents">
                            <asp:LinkButton ID="_showHideContentIds" runat="server" OnClick="_showHideContentIds_Click">
                                <asp:Image ID="_showHideContentIdsImage" runat="server" ImageUrl="~/Workarea/images/application/folders/fcpln.gif" />Content
                                Id(s):
                            </asp:LinkButton><br />
                            <asp:Panel ID="_contentIdsPanel" runat="server" Visible="false">
                                <asp:Label ID="_contentIds" runat="server" Visible="false"></asp:Label>
                                <div id="_contentIdsDiv" runat="server">
                                </div>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <%-- <asp:Button ID="_cancelProject" runat="server" Text="Cancel" OnClick="_cancelProject_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
                        <asp:Button ID="_removeProject" runat="server" Text="Archive" OnClick="_removeProject_Click"
                            Font-Names="Verdana" Font-Size="8pt" />--%>
                            </td>
                </tr>
            </table>
        </td>
        <td width="2%">
       
        </td>
        <td width="55%" valign="top">
            <table width="100%" id="tbltargetfiles" runat="server">
                <tr>
                    <td colspan="2"> 
                    <div style="overflow: auto; height: 140px; vertical-align: middle; width: 450px;">
                            <asp:TreeView ID="_files" runat="server">
                            </asp:TreeView>
                            <%--<asp:CheckBoxList ID="_files" runat="server"></asp:CheckBoxList>--%>
                        </div>
                       
                    </td>
                </tr>
                <tr>
                    <td>
                      <%--  <asp:Button ID="_retrieve" runat="server" OnClick="_retrieve_Click" OnClientClick="javascript:return isFileSelected();" Text="Retrieve Selected"
                            Width="130px" Font-Names="Verdana" Font-Size="8pt" />
                      --%>      
                          
                    </td>
                    <td align="right">
                        <%--<asp:Button ID="_history" runat="server" OnClick="_history_Click" Text="View History"
                            Width="130px" Font-Names="Verdana" Font-Size="8pt" />--%>
                            </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="_historyPanel" runat="server" Visible="false" Style="overflow: auto;
                            height: 150px; width: 450px;">
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <table width="100%" id="tbladdviewtask" runat="server">
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Button ID="btnaddviewtask" runat="server" Text="View/Add Task" Font-Names="Verdana"
                            Font-Size="8pt" Visible="false" />
                    </td>
                    <td align="right">
                        <%--<asp:CheckBox ID="chkquote" runat="server" Text="For Quote" />
                        <asp:Button ID="btnsubProj" runat="server" Text="Submit Project" OnClick="btnsubProj_Click"
                            Font-Names="Verdana" Font-Size="8pt" />--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="3"><table width="100%">
    <tr>
    <td > 
     <asp:Button ID="btnsubProj" runat="server" Text="Submit Project" OnClick="btnsubProj_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
                             <asp:Button ID="btnsubProjWithQuote" runat="server" Text="Submit as Quote" OnClick="btnsubProjWithQuote_Click"
                            Font-Names="Verdana" Font-Size="8pt" />    
                            
    </td>
    <td align="right">
     <asp:Button ID="_retrieve" runat="server" OnClick="_retrieve_Click" OnClientClick="javascript:return isFileSelected();" Text="Retrieve Selected"
                             Font-Names="Verdana" Font-Size="8pt" />  <asp:Button ID="_history" runat="server" OnClick="_history_Click" Text="View History"
                             Font-Names="Verdana" Font-Size="8pt" />
      <asp:Button ID="_removeProject" runat="server" Text="Archive" OnClick="_removeProject_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
    <asp:Button ID="_cancelProject" runat="server" Text="Cancel" OnClick="_cancelProject_Click"
                            Font-Names="Verdana" Font-Size="8pt" />
    </td>
    </tr> </table>
     
                      
                         
                            
                     
                       
                            </td></tr>
    <tr>
        <td colspan="3">
            <asp:Panel ID="_errorPanel" runat="server" Visible="false">
                <asp:Label ID="_errorLabel" runat="server" Style="color: Red"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="_progressPanel" runat="server" Visible="false">
                <%--<iframe src="/EktronTech/WorkArea/localizationjobs.aspx" width="100%" height="80" frameborder="1" marginwidth="1"
                    marginheight="4"></iframe> ../localizationjobs.aspx--%>
                    
                    <iframe id="_localizationjobsframe" src="/EktronTechTestSite/WorkArea/Freeway/freewaylocalizationjobs.aspx" runat="server"  width="100%" height="80" frameborder="1" marginwidth="1"
                    marginheight="4"></iframe>
            </asp:Panel>
        </td>
    </tr>
</table>
