<%@ Page Language="C#" AutoEventWireup="true" CodeFile="freewaydashboardprojectretrieve.aspx.cs" Inherits="freewaydashboardprojectretrieve" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Freeway Project Dashboard</title>
    <%=StyleSheetJS %>
	<script language="JavaScript" type="text/javascript" src="../java/jfunct.js"></script>
	<script language="JavaScript" type="text/javascript" src="../java/toolbar_roll.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div id="dhtmltooltip">
                    </div>

                    <script language="JavaScript" type="text/javascript" src="../java/workareahelper.js"></script>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="WorkareaTitlebar" id="txtTitleBar" runat="server" nowrap>
                            </td>
                        </tr>
                        <tr>
                            <td class="WorkareaToolbar-bk" id="htmToolBar" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Panel id="_progressPanel" runat="server" Visible="true">
            <iframe src="../localizationjobs.aspx" width="96%" height="360" frameborder="1" marginwidth="1" marginheight="4"></iframe>
        </asp:Panel>
    </form>
</body>
</html>
