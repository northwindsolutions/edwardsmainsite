#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : freeway dashboard project retrieve
---------------------------------
*/
#endregion
#region using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
#endregion
#region freewaydashboardprojectretrieve
public partial class freewaydashboardprojectretrieve : System.Web.UI.Page
{
    #region Object
    StyleHelper m_refStyle = new StyleHelper();
    #endregion 
    #region protected variables
    protected string StyleSheetJS = "";
    #endregion
    #region Page event
    protected void Page_Load(object sender, EventArgs e)
	{
		StyleSheetJS = m_refStyle.GetClientScript();

		return;
	}
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("Freeway Project Import");
        //htmToolBar.InnerHtml = m_refStyle.GetButtonEventsWCaption(AppImgPath & "btn_back-nm.gif", BackPage, m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "")

        return;
    }

    #endregion 
    #region Public properties
    public string BackPage
	{
		get
		{
			if (string.IsNullOrEmpty(Page.Request.QueryString["BackPage"]))
				return string.Empty;

			return Page.Request.QueryString["BackPage"];
		}
    }
    #endregion
   
}
#endregion