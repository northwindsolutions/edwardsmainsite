<%@ Control Language="C#" AutoEventWireup="true" CodeFile="freewaydashboardquotation.ascx.cs" Inherits="Workarea_Freeway_freewaydashboardquotation" %>

<tr>
    <td valign="top">PO Reference:</td>
    <td valign="top"><asp:TextBox ID="_poReference" Width="160px" MaxLength="30"  runat="server"></asp:TextBox></td>
</tr>
<tr>
    <td></td>
    <td><asp:Button ID="_acceptQuote" runat="server" Text="Accept Quote" OnClick="_acceptQuote_Click" /></td>
</tr>
<tr>
    <td valign="top">Comments:</td>
    <td valign="top"><asp:TextBox ID="_comments" TextMode="multiline" Width="215px" runat="server"></asp:TextBox></td>
</tr>
<tr>
    <td></td>
    <td align="right"><asp:Button ID="_rejectQuote" runat="server" Text="Reject Quote" OnClick="_rejectQuote_Click" />
        <asp:Button ID="_reworkQuote" runat="server" Text="Rework Quote" OnClick="_reworkQuote_Click" /></td>
</tr>
