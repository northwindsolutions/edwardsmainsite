#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : Allows creating freeway dashboard quotation
---------------------------------
*/
#endregion
#region using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
#endregion
#region Workarea_Freeway_freewaydashboardquotation
public partial class Workarea_Freeway_freewaydashboardquotation : System.Web.UI.UserControl
{
	#region Private Declaration

	private EktronProject _ektronProject = null;

	#endregion

	#region Public Members

	public EktronProject EktronProject
	{
		get { return _ektronProject; }
		set { _ektronProject = value; }
	}

	#endregion

	#region Page events

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected override void OnPreRender(EventArgs e)
	{
		if (EktronProject != null)
		{
			_poReference.Text = EktronProject.POReference;
		}

		base.OnPreRender(e);
    }
    #endregion

    #region Quote Functions
    /// <summary>
    /// Accept Quote and update the status of project 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _acceptQuote_Click(object sender, EventArgs e)
	{
        try
        {
            EktronProject.AcceptRejectQuote(FreewayConfiguration.IsExecutiveUser ?
                                    FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity(), LB.FreewayLib.VojoService.QuoteAction.Accept,
                _poReference.Text, string.Empty);

        }
        catch (Exception ex)
        {
            string alertmessage = string.Empty;
            if(ex.Message.Contains("Forecasted"))
                alertmessage = "javascript:alert('Project Id - " + EktronProject.Id + " is not in Forecasted state. ');";
            else
                alertmessage = "javascript:alert('For Project Id - " + EktronProject.Id + " unhandled exception is raised. ');";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert Message", alertmessage, true);
        }
        updateProjectStatus();
	}
    /// <summary>
    ///  Reject Quote and update the status of project 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
	protected void _rejectQuote_Click(object sender, EventArgs e)
	{
        try
        {
            EktronProject.AcceptRejectQuote(FreewayConfiguration.IsExecutiveUser ?
                                    FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity(), LB.FreewayLib.VojoService.QuoteAction.Reject,
                string.Empty, _comments.Text);

        }
        catch (Exception ex)
        {

            string alertmessage = string.Empty;
            if (ex.Message.Contains("Forecasted"))
                alertmessage = "javascript:alert('Project Id - " + EktronProject.Id + " is not in Forecasted state. ');";
            else
                alertmessage = "javascript:alert('For Project Id - " + EktronProject.Id + " unhandled exception is raised. ');";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert Message", alertmessage, true);
                    
        }
        updateProjectStatus();
	}
    /// <summary>
    ///  Rework Quote and update the status of project 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
	protected void _reworkQuote_Click(object sender, EventArgs e)
	{
        try
        {
            EktronProject.AcceptRejectQuote(FreewayConfiguration.IsExecutiveUser ?
                                    FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity(), LB.FreewayLib.VojoService.QuoteAction.Rework,
                string.Empty, _comments.Text);

           

        }
        catch (Exception ex)
        {
            string alertmessage = string.Empty;
            if (ex.Message.Contains("Forecasted"))
                alertmessage = "javascript:alert('Project Id - " + EktronProject.Id + " is not in Forecasted state. ');";
            else
                alertmessage = "javascript:alert('For Project Id - " + EktronProject.Id + " unhandled exception is raised. ');";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert Message", alertmessage, true);
        }
        updateProjectStatus();
    }
    #endregion


    #region Private Members
    /// <summary>
    /// To upload project status
    /// </summary>
    private void updateProjectStatus()
	{
        EktronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
        EktronProject.GetFilesStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());

		Session["ForceDaskboardVisibleRefresh"] = "ForceRefresh";

		return;
	}

	#endregion
}
#endregion