
#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : This page will be used to perform the loaclization function
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports Ektron.Cms
Imports Ektron.Cms.Common
#End Region

#Region "Localization class"
Partial Class localization
    Inherits System.Web.UI.Page

#Region "Member Variables"
    Protected m_objLocalizationApi As New LocalizationAPI
    Protected m_refMsg As EkMessageHelper
    Protected m_refStyle As New StyleHelper
    Protected AppImgPath As String = ""
    Protected m_refSiteApi As New SiteAPI
#End Region
#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1

        registerResources()

        m_refMsg = m_objLocalizationApi.EkMsgRef
        setServerJSVariables()

        AppImgPath = m_objLocalizationApi.AppImgPath
        StyleSheetJS.Text = m_refStyle.GetClientScript()
        If (Not Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_objLocalizationApi.RequestInformationRef, DataIO.LicenseManager.Feature.Xliff, False)) Then
            Utilities.ShowError(m_refMsg.GetMessage("feature locked error"))
        End If
        If (IsPostBack) Then
            Dim strPath As String = m_objLocalizationApi.GetTranslationUploadDirectory()

            processFileUpload(FileUpload0, strPath, FileUploadLabel0)
            processFileUpload(FileUpload1, strPath, FileUploadLabel1)
            processFileUpload(FileUpload2, strPath, FileUploadLabel2)
            processFileUpload(FileUpload3, strPath, FileUploadLabel3)
            processFileUpload(FileUpload4, strPath, FileUploadLabel4)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If m_refSiteApi.RequestInformationRef.IsMembershipUser Or m_refSiteApi.RequestInformationRef.UserId = 0 Then
            Response.Redirect("blank.htm", False)
            Exit Sub
        End If
        LoadingImg.Text = m_refMsg.GetMessage("one moment msg")
        generateToolbar()
        ' Lionbridge Start
        If (Request.QueryString("ShowFreewayMessage") <> Nothing) Then
            '_freewayMessage.Text = "There's file(s) from Freeway that are ready to be imported!"
            _freewayMessage.Text = "File(s) from Freeway ready to be imported!"
            _freewayMessage.Visible = Not IsPostBack
        End If
        ' Lionbridge End
        If (IsPostBack) Then
            importTranslation()
        End If
    End Sub
#End Region
#Region "Helper Methods"
    ''' <summary>
    ''' To generate toolbar
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub generateToolbar()
        Dim result As New System.Text.StringBuilder
        Dim sHtmlAction As String = ""

        Dim WorkareaTitlebarTitle As String
        WorkareaTitlebarTitle = m_refMsg.GetMessage("lbl import translated XLIFF files")
        txtTitleBar.InnerHtml = m_refStyle.GetTitleBar(WorkareaTitlebarTitle)

        result.Append("<table><tr>")


        result.Append(m_refStyle.GetButtonEventsWCaption(m_refSiteApi.AppPath & "Images/ui/icons/translationImport.png", "#", m_refMsg.GetMessage("alt Click here to upload and import translated XLIFF files"), m_refMsg.GetMessage("lbl import translated XLIFF files"), "onclick='return SubmitForm(0,""validate()"")'"))

        result.Append("<td>")
        result.Append(m_refStyle.GetHelpButton("import_xliff_files", "import_xliff_files"))
        result.Append("</td>")
        result.Append("</tr></table>")
        htmToolBar.InnerHtml = result.ToString
    End Sub
    ''' <summary>
    ''' To Upload file
    ''' </summary>
    ''' <param name="ctlFileUpload"></param>
    ''' <param name="Path"></param>
    ''' <param name="ctlLabel"></param>
    ''' <remarks></remarks>
    Private Sub processFileUpload(ByVal ctlFileUpload As FileUpload, ByVal Path As String, ByVal ctlLabel As Label)
        With ctlFileUpload
            ctlLabel.Text = ""
            If (.HasFile) Then
                Dim strFileExt As String = System.IO.Path.GetExtension(.FileName).ToLower()
                If (".xlf.xml.zip".Contains(strFileExt)) Then ' .xml for Trados
                    Try
                        .PostedFile.SaveAs(EkFunctions.QualifyURL(Path, .FileName))
                    Catch ex As Exception
                        ctlLabel.Text = "<p>Failed to upload file: " & .FileName & "<br />" & ex.Message & "</p>"
                    End Try
                Else
                    ctlLabel.Text = "<p>Cannot accept files of this type: " & strFileExt & "</p>"
                End If
            End If
        End With
    End Sub

    ''' <summary>
    ''' Register Resources.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub registerResources()
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss)
        Ektron.Cms.API.Css.RegisterCss(Me, Ektron.Cms.API.Css.ManagedStyleSheet.EktronModalCss)

        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronModalJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronToolBarRollJS)
        Ektron.Cms.API.JS.RegisterJS(Me, Ektron.Cms.API.JS.ManagedScript.EktronWorkareaHelperJS)
    End Sub
    ''' <summary>
    ''' Protected Function to set the JS variables.
    ''' </summary>
    ''' <remarks></remarks>
    '''
    Protected Sub setServerJSVariables()
        ltr_fileMissing.Text = m_refMsg.GetMessage("alt one or more files could not be found to be uploaded.")
        ltr_Permitted.Text = m_refMsg.GetMessage("alt Only XLIFF files (.xlf, .xml, or .zip) are permitted.")
        ltr_selectOne.Text = m_refMsg.GetMessage("alt Please select at least one XLIFF (.xlf, .xml, or .zip) file.")
    End Sub
#End Region

#Region "LOCALIZATION - Import"
    Private Sub importTranslation()
        m_objLocalizationApi.StartImportTranslation()

    End Sub
#End Region

   
End Class
#End Region

