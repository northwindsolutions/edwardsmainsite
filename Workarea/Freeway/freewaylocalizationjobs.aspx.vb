#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : This page will be used to perform the localizationjobs function
'---------------------------------
'*/
#End Region

#Region "Imports"

Imports System.io
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports Ektron.Cms
Imports Ektron.Cms.Common
Imports System.Collections.Generic
Imports Ektron.Cms.API
Imports Ektron.Cms.UserAPI
Imports EkJobDS = Ektron.Cms.LocalizationJobDataSet
#End Region


#Region "localizationjobs class"

Partial Class localizationjobs
    Inherits System.Web.UI.Page

#Region "Private/Protected Member Variables"
    Private m_objSite As New SiteAPI
    Private m_objLocalizationApi As New LocalizationAPI
    Private m_bAnyJobRunning As Boolean ' True if at least one job is currently running.
    Private m_bFirstJob As Boolean = False
    Private Const mc_nJobsToShow As Integer = 25
    Protected m_refContentApi As New ContentAPI
    Protected m_refMsg As Ektron.Cms.Common.EkMessageHelper

    Enum MessageStyleType
        ErrorMessage
        Information
    End Enum
#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1

        m_refMsg = m_refContentApi.EkMsgRef
        tvJobs.CollapseImageToolTip = m_refMsg.GetMessage("lbl collapse")
        tvJobs.ExpandImageToolTip = m_refMsg.GetMessage("lbl expand")

        If (Not Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_objLocalizationApi.RequestInformationRef, DataIO.LicenseManager.Feature.Xliff, False)) Then
            Utilities.ShowError(m_objSite.EkMsgRef.GetMessage("feature locked error"))
        Else
            Dim strCancelJobID As String = Request.QueryString("cancel")
            If IsNumeric(strCancelJobID) Then
                m_objLocalizationApi.CancelJob(Convert.ToInt64(strCancelJobID))
            End If
        End If
        If (Not IsPostBack) Then
            'Dim IsGlobalUser As Boolean
            'IsGlobalUser = False
            'Dim GlobaluserRow As Data.DataRow
            'GlobaluserRow = FreewayDB.GetUserRow(-1, False)
            'If (Not GlobaluserRow Is Nothing) Then
            '    IsGlobalUser = Convert.ToBoolean(GlobaluserRow("ForceGlobalSettings"))
            'End If
            ''Modified by Supriya on 3rd Aug 2010 To change frame title from �Freeway Settings� to �Freeway User Settings�.
            'If (IsGlobalUser) Then
            '    lblTitle.Text = "<table width='100%'><tr><td align='left' style='width:75%;color:white;'>Freeway User Settings  &nbsp;<i>(Connector is running in Global user mode)</i></td><td align='right' style='width:25%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>"
            'Else
            '    lblTitle.Text = "<table width='100%'><tr><td align='left' style='width:75%;color:white;'>Freeway User Settings  &nbsp;<i>(Connector is running in Standard user mode)</i></td><td align='right' style='width:25%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>"
            'End If
        End If
        Dim userApi As New UserAPI()
        Dim userRow As Data.DataRow
        userRow = FreewayDB.GetUserRow(userApi.UserId, False)
        Dim IsGlobalUser As Boolean
        IsGlobalUser = False
        Dim GlobaluserRow As Data.DataRow
        Dim str As String
        str = Nothing

        'GlobaluserRow = FreewayDB.GetUserRowFreewayServerWise(-1, False, userRow("CurrentEnvironment").ToString())
        GlobaluserRow = FreewayDB.GetUserRowFreewayServerWise(-1, False, str)
        If (Not GlobaluserRow Is Nothing) Then
            IsGlobalUser = Convert.ToBoolean(GlobaluserRow("ForceGlobalSettings"))
        End If
        'Modified by Supriya on 3rd Aug 2010 To change frame title from �Freeway Settings� to �Freeway User Settings�.
        If (IsGlobalUser) Then
            lblTitle.Text = "<table width='100%'><tr><td align='left' style='width:75%;color:white;'>Freeway User Settings  &nbsp;<i>(Connector is running in Global User Mode.)</i></td><td align='right' style='width:25%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>"
        Else
            lblTitle.Text = "<table width='100%'><tr><td align='left' style='width:75%;color:white;'>Freeway User Settings  &nbsp;<i>(Connector is running in Mapped Users Mode.)</i></td><td align='right' style='width:25%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>"
        End If

    End Sub
#End Region

#Region "Methods"
    ''' <summary>
    '''  Add Comment here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tvJobs_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvJobs.Load
        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_objLocalizationApi.RequestInformationRef, DataIO.LicenseManager.Feature.Xliff, False)) Then
            m_bAnyJobRunning = False
            addJobs(tvJobs)
            If (m_bAnyJobRunning) Then
                Response.AddHeader("Refresh", "3") ' in seconds
            End If
        End If
    End Sub

    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tvJobs_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvJobs.SelectedNodeChanged
        Dim strNodeType As String ' 3 chars
        Dim ID As Long

        strNodeType = tvJobs.SelectedNode.Value.Substring(0, 3).ToLower
        Dim strID As String = tvJobs.SelectedNode.Value.Substring(3)
        If (IsNumeric(strID)) Then
            ID = Convert.ToInt64(strID)
        Else
            Exit Sub
        End If

        Select Case strNodeType
            Case "pgj"
                tvJobs.Nodes.Remove(tvJobs.SelectedNode)
                addJobs(tvJobs, StartAt:=ID)
            Case Else
                ' Nothing to do
        End Select
    End Sub

    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tvJobs_TreeNodePopulate(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles tvJobs.TreeNodePopulate
        Dim strNodeType As String ' 3 chars
        Dim ID As Long

        strNodeType = e.Node.Value.Substring(0, 3).ToLower
        Dim strID As String = e.Node.Value.Substring(3)
        If (IsNumeric(strID)) Then
            ID = Convert.ToInt64(strID)
        Else
            Exit Sub
        End If

        Select Case strNodeType
            Case "run"
                populateRunNode(ID, e)
            Case "job"
                populateJobNode(ID, e)
            Case "zip"
                populateZipNode(ID, e)
            Case "skl"
                populateSklNode(ID, e)
            Case "xlf"
                populateXlfNode(ID, e)
            Case "msg"
                ' Nothing to do
            Case Else
                ' Nothing to do
        End Select
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub populateRunNode(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        addJobsInJob(ID, e)
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub populateJobNode(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        addJobsInJob(ID, e)
        addZipsInJob(ID, e)
        addSklsInJob(ID, e)
    End Sub

    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub populateZipNode(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        addMsgsInZip(ID, e)
        addZipsInZip(ID, e)
        addXlfsInZip(ID, e)
    End Sub

    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub populateSklNode(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        addXlfsInSkl(ID, e)
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub populateXlfNode(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        addMsgsInXlf(ID, e)
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="tv"></param>
    ''' <param name="StartAt"></param>
    ''' <remarks></remarks>
    Private Sub addJobs(ByVal tv As TreeView, Optional ByVal StartAt As Integer = 0)
        Dim dt As EkJobDS.LocalizationJobDataTable = m_objLocalizationApi.GetJobs
        Dim nCount As Integer = dt.Count
        Dim bMore As Boolean = False
        ' Check StartAt=0 to avoid a strange effect.
        ' Clicking 'More...' at the end of the first set works fine.
        ' Clicking 'More...' at the end of the second set resets the list to show just the first set.
        ' So, rather than spend time tracking it down, we'll just show all the jobs.
        ' There also seems to be a defect in the TreeView where the onmouseover will throw a JavaScript
        ' error if the list is long and hasn't finished loading yet.
        If (mc_nJobsToShow > 0 AndAlso nCount > StartAt + mc_nJobsToShow AndAlso StartAt = 0) Then
            nCount = StartAt + mc_nJobsToShow
            bMore = True
        End If
        m_bFirstJob = (0 = StartAt)
        For iRow As Integer = StartAt To nCount - 1
            tv.Nodes.Add(createJobNode(dt(iRow)))
            m_bFirstJob = False
        Next
        If (bMore) Then
            Dim node As New TreeNode
            node.Value = "pgj" & nCount
            node.PopulateOnDemand = False
            node.SelectAction = TreeNodeSelectAction.Select
            node.Text = m_refMsg.GetMessage("alt more")
            tv.Nodes.Add(node)
        End If
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addJobsInJob(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim dt As EkJobDS.LocalizationJobDataTable = m_objLocalizationApi.GetJobs(ID)
        For Each data As EkJobDS.LocalizationJobRow In dt.Rows
            e.Node.ChildNodes.Add(createJobNode(data))
        Next
    End Sub
    ''' <summary>
    ''' To create job node.
    ''' </summary>
    ''' <param name="data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createJobNode(ByVal data As EkJobDS.LocalizationJobRow) As TreeNode
        Static s_bFirstCompressFilesJobExpanded As Boolean = False
        Static s_bFirstExportJobExpanded As Boolean = False
        Static s_bFirstJobExpanded As Boolean = False
        Dim node As New TreeNode
        Dim sb As New StringBuilder
        Dim bJobComplete As Boolean = EkJobDS.LocalizationJobRow.IsJobDone(data.State)
        Dim bJobHasErrors As Boolean = (EkJobDS.LocalizationJobRow.States.CompleteWithErrors = data.State _
          Or EkJobDS.LocalizationJobRow.States.Canceled = data.State)
        Dim bJobRunning As Boolean = (Not bJobComplete AndAlso Not data.IsCurrentStepNull AndAlso Not data.IsTotalStepsNull AndAlso data.CurrentStep <= data.TotalSteps)

        node.Value = "job" & data.JobID
        node.PopulateOnDemand = True
        node.SelectAction = TreeNodeSelectAction.Expand

        sb.Append("<div class=""L10nJob"">")
        sb.Append("<div class=""L10nJobTitle"">")
        If (data.State = EkJobDS.LocalizationJobRow.States.CompleteWithErrors) Then
            sb.Append(formatErrorMessage(m_refMsg.GetMessage("lbl complete with alerts")))
        End If
        Select Case data.JobType
            Case EkJobDS.LocalizationJobRow.Types.CompressFiles
                sb.Append(m_refMsg.GetMessage("lbl downloadable zip files"))
                'node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/FileTypes/zip.png"
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/icons/FileTypes/zip.png"
                node.ImageToolTip = m_refMsg.GetMessage("alt zipped file")
                If (Not s_bFirstCompressFilesJobExpanded) Then
                    s_bFirstCompressFilesJobExpanded = True
                    node.Expand()
                End If
            Case EkJobDS.LocalizationJobRow.Types.Export
                sb.Append(String.Format(m_refMsg.GetMessage("lbl exported") & """{0}""", data.JobTitle))
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationExport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl exported")
                If (Not s_bFirstExportJobExpanded) Then
                    s_bFirstExportJobExpanded = True
                    node.Expand()
                End If
            Case EkJobDS.LocalizationJobRow.Types.ExportContent
                sb.Append(String.Format(m_refMsg.GetMessage("lbl exported content") & """{0}""", data.JobTitle))
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationExport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl exported content")
                If (Not s_bFirstExportJobExpanded) Then
                    s_bFirstExportJobExpanded = True
                    node.Expand()
                End If
            Case EkJobDS.LocalizationJobRow.Types.ExportFolder
                sb.Append(String.Format(m_refMsg.GetMessage("lbl exported folder") & """{0}""", data.JobTitle))
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationExport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl exported folder")
                If (Not s_bFirstExportJobExpanded) Then
                    s_bFirstExportJobExpanded = True
                    node.Expand()
                End If
            Case EkJobDS.LocalizationJobRow.Types.ExportMenu
                sb.Append(m_refMsg.GetMessage("lbl exported menus"))
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationExport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl exported menus")
                If (Not s_bFirstExportJobExpanded) Then
                    s_bFirstExportJobExpanded = True
                    node.Expand()
                End If
            Case EkJobDS.LocalizationJobRow.Types.ExtractText
                sb.Append(m_refMsg.GetMessage("lbl extracted"))
                If (Not data.IsJobTitleNull AndAlso data.JobTitle.Length > 0) Then
                    sb.Append(" " & data.JobTitle)
                End If
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationExport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl extracted")
            Case EkJobDS.LocalizationJobRow.Types.Import
                sb.Append(m_refMsg.GetMessage("lbl imported"))
                If (Not data.IsJobTitleNull AndAlso data.JobTitle.Length > 0) Then
                    sb.Append(" " & data.JobTitle)
                End If
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationImport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl imported")
            Case EkJobDS.LocalizationJobRow.Types.MergeText
                sb.Append(m_refMsg.GetMessage("lbl merged"))
                If (Not data.IsJobTitleNull AndAlso data.JobTitle.Length > 0) Then
                    sb.Append(" " & data.JobTitle)
                End If
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translationImport.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl merged")
            Case EkJobDS.LocalizationJobRow.Types.UncompressFiles
                sb.Append(m_refMsg.GetMessage("lbl uncmpresed file"))
                If (Not data.IsJobTitleNull AndAlso data.JobTitle.Length > 0) Then
                    sb.Append(" " & data.JobTitle)
                End If
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/FileTypes/zip.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl uncmpresed file")
            Case Else
                sb.Append(data.JobTitle)
        End Select
        sb.Append("</div>")
        sb.Append(formatDateTime(data.StartTime, "L10nJobStartTime"))
        If (bJobRunning) Then
            Dim nProgressPercent As Single = data.CurrentStep / data.TotalSteps
            Dim nProgressWidth As Integer
            nProgressWidth = Math.Ceiling(100 * nProgressPercent)
            ' " & String.Format("{0:0%}", nProgressPercent) & "
            sb.Append("<div class=""L10nJobProgress""><img height=""10"" width=""" & nProgressWidth & """ src=""" & getImageUrl("dbl_grad_blue.gif") & """ border=""0"" /></div>")
            node.ImageUrl = getImageUrl("loading_small.gif")
            node.Value = "run" & data.JobID
            node.Expand()
        Else
            sb.Append("<div class=""L10nJobState"">")
            Select Case data.State
                Case EkJobDS.LocalizationJobRow.States.Canceled
                    sb.Append(m_refMsg.GetMessage("lbl canceled"))
                Case EkJobDS.LocalizationJobRow.States.Complete
                    sb.Append(m_refMsg.GetMessage("lbl complete"))
                Case EkJobDS.LocalizationJobRow.States.CompleteWithErrors
                    sb.Append(m_refMsg.GetMessage("lbl alert"))
                Case EkJobDS.LocalizationJobRow.States.Initializing
                    sb.Append(m_refMsg.GetMessage("lbl initializing"))
                Case EkJobDS.LocalizationJobRow.States.Running
                    sb.Append(m_refMsg.GetMessage("lbl running"))
                Case Else
                    sb.Append(data.State)
            End Select
            sb.Append("</div>")
        End If

        ' Lionbridge Begin

        Try

            If (Session("ExportToFreeway")) Then
                If (data.JobID = Session("RunningJobId") And data.State = EkJobDS.LocalizationJobRow.States.Complete) Then

                    Dim projectDetails As ProjectDetails = CType(Session("ProjectDetails"), ProjectDetails)

                    Dim ektronProject As New EktronProject(data.JobID)

                    ektronProject.Description = projectDetails.ProjectDescription
                    ektronProject.POReference = projectDetails.POReference
                    ektronProject.SpecialInstructions = projectDetails.SpecialInstruction
                    ektronProject.ExpectedStartingDate = DateTime.Now
                    ektronProject.ExpectedDeliveryDate = DateTime.Now.AddDays(7.0)
                    Try
                        ektronProject.Submit(Session("SubmitAsQuote"))
                        'Inner code of the Ektron so dont know how to trace it
                    Catch ex As Exception
                        Utilities.ShowError(ex.Message)
                        Exit Try
                    End Try

                    Session("RunningJobId") = Nothing
                    Session("ExportToFreeway") = Nothing
                End If
            End If

            sb.Append(FreewayContentGenerator.CreateLocalizationJobContent(data))

        Catch ex As Exception

            If (ex.Message.Contains("User account is invalid")) Then

                Dim sberror As New StringBuilder
                sberror.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>")
                ' sberror.Append("<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>")
                sberror.Append("<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">No valid Freeway User Settings were found for the Freeway Connector. Please navigate to the User Settings node to complete this configuration.</td>")

                sberror.Append("</tr></table>")
                Response.Write(sberror.ToString())
                Response.Flush()
                Response.End()

            ElseIf (ex.Message.Contains("A network-related")) Then

                Dim sberror As New StringBuilder
                sberror.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>")
                sberror.Append("<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">Freeway Services are Unavailable. <br> Please contact the Administrator.</td>")

                sberror.Append("</tr></table>")
                Response.Write(sberror.ToString())
                Response.Flush()
                Response.End()

            ElseIf (ex.Message.Contains("Object reference not set to an instance of an object")) Then
                Dim srtprojectinfo As String
            Else

                Dim srtprojectinfo As String
                srtprojectinfo = "While generating the XLIFF file exception raised with the message - " & ex.Message
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "Create Click", "javascript:alert('" + srtprojectinfo + "')", True)
            End If
        End Try



        ' Lionbridge End

        If (m_bFirstJob And (Not s_bFirstJobExpanded Or bJobHasErrors)) Then
            s_bFirstJobExpanded = True
            node.Expand()
        End If
        If (Not bJobComplete) Then
            m_bAnyJobRunning = True
        End If
        sb.Append("</div>")
        If (bJobRunning AndAlso data.IsParentJobIDNull) Then
            sb.Append(" <a href=""?cancel=" & data.JobID & """ class=""L10nJobCancel"">" & m_refMsg.GetMessage("lnk cancel") & "</a>")
        End If

        node.Text = sb.ToString

        Return node
    End Function

    ''' <summary>
    ''' To add Zip in the Job.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addZipsInJob(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim dt As EkJobDS.LocalizationJobFileDataTable = m_objLocalizationApi.GetFilesByJob(ID)
        For Each data As EkJobDS.LocalizationJobFileRow In dt.Rows
            ' If (data.Table.Rows.Count > 0) Then
            e.Node.ChildNodes.Add(createZipNode(data))
            ' End If
        Next
    End Sub
    ''' <summary>
    ''' To add zip in Zip .
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addZipsInZip(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim dt As EkJobDS.LocalizationJobFileDataTable = m_objLocalizationApi.GetFilesInFile(ID)
        For Each data As EkJobDS.LocalizationJobFileRow In dt.Rows
            e.Node.ChildNodes.Add(createZipNode(data))
        Next
    End Sub
    ''' <summary>
    ''' To add message in Zip.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addMsgsInZip(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim data As EkJobDS.LocalizationJobFileRow = m_objLocalizationApi.GetFileByID(ID)
        If (Not IsNothing(data) AndAlso Not IsNothing(data.ErrorMessage) AndAlso data.ErrorMessage.Length > 0) Then
            e.Node.ChildNodes.Add(createMsgNode(data.ErrorMessage))
        End If
    End Sub
    ''' <summary>
    ''' To create Zip node.
    ''' </summary>
    ''' <param name="data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createZipNode(ByVal data As EkJobDS.LocalizationJobFileRow) As TreeNode
        Static s_bFirstErrorExpanded As Boolean = False
        Dim node As New TreeNode
        Dim sb As New StringBuilder
        Dim strUrl As String
        Dim strName As String
        Dim strFileExt As String

        strFileExt = IO.Path.GetExtension(data.FileName).ToLower
        strUrl = EkFunctions.QualifyURL(m_objLocalizationApi.GetLocalizationUrl, data.FileUrl)
        node.Value = "zip" & data.FileID
        node.PopulateOnDemand = False
        node.SelectAction = TreeNodeSelectAction.None

        Select Case strFileExt
            Case ".zip"
                node.PopulateOnDemand = True
                node.SelectAction = TreeNodeSelectAction.Expand
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/FileTypes/zip.png"
                node.ImageToolTip = m_refMsg.GetMessage("alt zip file")
            Case ".xlf", ".xml" ' .xml for Trados
                If (Not IsNothing(data.ErrorMessage) AndAlso data.ErrorMessage.Length > 0) Then
                    node.PopulateOnDemand = True
                    node.SelectAction = TreeNodeSelectAction.Expand
                End If
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translation.png"
                node.ImageToolTip = m_refMsg.GetMessage("alt xliff file")
            Case Else
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/FileTypes/text.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl generic file")
        End Select
        If (File.Exists(Server.MapPath(strUrl))) Then
            node.PopulateOnDemand = False
            node.SelectAction = TreeNodeSelectAction.Select
            node.NavigateUrl = strUrl
            sb.Append("<div class=""L10nZipLink"">")
        Else
            node.NavigateUrl = ""
            sb.Append("<div class=""L10nZip"">")
        End If

        sb.Append("<div class=""L10nZipFileName"">")
        sb.Append(formatErrorMessage(data.ErrorMessage))
        strName = data.FileName
        If (Not data.IsTargetLanguageNull) Then
            Dim objSourceLangData As LanguageData = getLanguageData(data.SourceLanguage)
            Dim objTargetLangData As LanguageData = getLanguageData(data.TargetLanguage)
            Dim strTargetFlag As String = m_objLocalizationApi.GetFlagUrl(objTargetLangData)
            If (strTargetFlag.Length > 0) Then
                If (Not data.IsSourceLanguageNull) Then
                    Dim strSourceFlag As String = m_objLocalizationApi.GetFlagUrl(objSourceLangData)
                    If (strSourceFlag.Length > 0) Then
                        sb.Append("<img src=""" & strSourceFlag & """ alt=""" & objSourceLangData.LocalName & """ class=""L10nFlag"" />&#160;&#8594;&#160;") ' 8594 is right arrow
                    End If
                End If
                strName = objTargetLangData.LocalName
                sb.Append("<img src=""" & strTargetFlag & """ alt=""" & strName & """ class=""L10nFlag"" />&#160;")
            End If
        End If
        sb.Append(strName)
        sb.Append("</div>")
        sb.Append("<div class=""L10nZipFileSize"">")
        If (data.IsFileSizeNull) Then
            sb.Append("&#160;")
        Else
            sb.Append(formatFileSize(data.FileSize))
        End If
        sb.Append("</div>")
        sb.Append("</div>")

        node.Text = sb.ToString

        If (Not s_bFirstErrorExpanded AndAlso Not IsNothing(data.ErrorMessage) AndAlso data.ErrorMessage.Length > 0) Then
            s_bFirstErrorExpanded = True
            node.Expand()
        End If

        Return node
    End Function
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addSklsInJob(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim dt As EkJobDS.LocalizationSkeletonDataTable = m_objLocalizationApi.GetContentItemsByJob(ID)
        For Each data As EkJobDS.LocalizationSkeletonRow In dt.Rows
            e.Node.ChildNodes.Add(createSklNode(data))
        Next
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createSklNode(ByVal data As EkJobDS.LocalizationSkeletonRow) As TreeNode
        Static s_bFirstErrorExpanded As Boolean = False
        ' Special case: skeleton may be used as a placeholder to no skeletons
        If (data.ItemType = EkJobDS.LocalizationSkeletonRow.Types.Information) Then
            If (data.IsErrorMessageNull OrElse 0 = data.ErrorMessage.Length) Then
                Return createMsgNode(data.Title, MessageStyleType.Information)
            Else
                Return createMsgNode(data.ErrorMessage, MessageStyleType.ErrorMessage)
            End If
        End If

        Dim node As New TreeNode
        Dim sb As New StringBuilder
        Dim strToolTipForItemID As String = ""

        node.Value = "skl" & data.SkeletonID
        node.PopulateOnDemand = True
        node.SelectAction = TreeNodeSelectAction.Expand
        Select Case data.ItemType
            Case EkJobDS.LocalizationSkeletonRow.Types.Content
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/contentHtml.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl content")
                strToolTipForItemID = m_refMsg.GetMessage("generic content id")
            Case EkJobDS.LocalizationSkeletonRow.Types.Form
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/contentForm.png"
                node.ImageToolTip = m_refMsg.GetMessage("form text")
                strToolTipForItemID = m_refMsg.GetMessage("alt form id")
            Case EkJobDS.LocalizationSkeletonRow.Types.Menu
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/menu.png"
                node.ImageToolTip = m_refMsg.GetMessage("generic menu title")
                strToolTipForItemID = m_refMsg.GetMessage("alt menu number")
            Case EkJobDS.LocalizationSkeletonRow.Types.MenuItem
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/menu.png"
                node.ImageToolTip = m_refMsg.GetMessage("alt menu items")
                strToolTipForItemID = m_refMsg.GetMessage("alt menu items number")
            Case Else
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/FileTypes/text.png"
                node.ImageToolTip = data.ItemType.ToString
                strToolTipForItemID = m_refMsg.GetMessage("generic id")
        End Select

        sb.Append("<div class=""L10nSkl"">")
        sb.Append("<div class=""L10nSklItemID""")
        If (strToolTipForItemID.Length > 0) Then
            sb.Append(" title=""" & strToolTipForItemID & """>")
            sb.Append(data.ItemID)
        Else
            sb.Append(">")
        End If
        sb.Append("</div>")
        sb.Append("<div class=""L10nSklTitle"">")
        sb.Append(formatErrorMessage(data.ErrorMessage))
        sb.Append(data.Title)
        sb.Append("</div>")
        If (data.IsLastEditDateNull) Then
            sb.Append(formatDateTime(Nothing, "L10nSklDate"))
        Else
            sb.Append(formatDateTime(data.LastEditDate, "L10nSklDate", "Last Edit Date"))
        End If
        sb.Append("</div>")

        node.Text = sb.ToString

        If (Not s_bFirstErrorExpanded AndAlso Not IsNothing(data.ErrorMessage) AndAlso data.ErrorMessage.Length > 0) Then
            s_bFirstErrorExpanded = True
            node.Expand()
        End If

        Return node
    End Function
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addXlfsInZip(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim dt As EkJobDS.LocalizationTargetDataTable = m_objLocalizationApi.GetInterchangeFilesInFile(ID)
        For Each data As EkJobDS.LocalizationTargetRow In dt.Rows
            e.Node.ChildNodes.Add(createXlfNode(data))
        Next
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addXlfsInSkl(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim dt As EkJobDS.LocalizationTargetDataTable = m_objLocalizationApi.GetInterchangeFilesByContentItem(ID)
        For Each data As EkJobDS.LocalizationTargetRow In dt.Rows
            e.Node.ChildNodes.Add(createXlfNode(data))
        Next
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addMsgsInXlf(ByVal ID As Long, ByVal e As TreeNodeEventArgs)
        Dim data As EkJobDS.LocalizationTargetRow = m_objLocalizationApi.GetInterchangeFileByID(ID)
        If (Not IsNothing(data) AndAlso Not IsNothing(data.ErrorMessage) AndAlso data.ErrorMessage.Length > 0) Then
            e.Node.ChildNodes.Add(createMsgNode(data.ErrorMessage))
        End If
    End Sub
    ''' <summary>
    ''' Add comment here.
    ''' </summary>
    ''' <param name="data"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createXlfNode(ByVal data As EkJobDS.LocalizationTargetRow) As TreeNode
        Static s_bFirstErrorExpanded As Boolean = False
        Dim node As New TreeNode
        Dim sb As New StringBuilder
        Dim strHover As String = ""

        node.Value = "xlf" & data.TargetID
        If (IsNothing(data.ErrorMessage) OrElse 0 = data.ErrorMessage.Length) Then
            node.PopulateOnDemand = False
            node.SelectAction = TreeNodeSelectAction.None
            strHover = " onmouseover=""myTVNodeHover(this)"" onmouseout=""myTVNodeUnhover(this)"""
        Else
            node.PopulateOnDemand = True
            node.SelectAction = TreeNodeSelectAction.Expand
        End If
        node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/translation.png"
        node.ImageToolTip = m_refMsg.GetMessage("alt xliff")
        'If (Not data.IsTargetLanguageNull) Then
        ' node.ImageUrl = GetFlagUrlByLanguageID(data.TargetLanguage, "../UI/Icons/translation.png") flag images are too large for this
        'End If
        sb.Append("<div class=""L10nXlf""" & strHover & ">")
        sb.Append("<div class=""L10nXlfFileName"">")
        sb.Append(formatErrorMessage(data.ErrorMessage))
        If (Not data.IsTargetLanguageNull) Then
            Dim objTargetLangData As LanguageData = getLanguageData(data.TargetLanguage)
            Dim strTargetFlag As String = m_objLocalizationApi.GetFlagUrl(objTargetLangData)
            If (strTargetFlag.Length > 0) Then
                Dim strName As String
                strName = objTargetLangData.LocalName
                sb.Append("<img src=""" & strTargetFlag & """ alt=""" & strName & """ class=""L10nFlag"" />&#160;")
            End If
        End If
        sb.Append(data.FileName)
        sb.Append("</div>")
        If (data.IsLastEditDateNull) Then
            sb.Append(formatDateTime(Nothing, "L10nXlfDate"))
        Else
            sb.Append(formatDateTime(data.LastEditDate, "L10nXlfDate", "Last Edit Date"))
        End If
        sb.Append("<div class=""L10nXlfState"">")
        If (data.IsStateNull) Then
            sb.Append("&#160;")
        Else
            Select Case data.State
                Case EkJobDS.LocalizationTargetRow.States.ErrorOccurred
                    sb.Append(m_refMsg.GetMessage("lbl error"))
                    If (Not s_bFirstErrorExpanded AndAlso Not IsNothing(data.ErrorMessage) AndAlso data.ErrorMessage.Length > 0) Then
                        s_bFirstErrorExpanded = True
                        node.Expand()
                    End If
                Case EkJobDS.LocalizationTargetRow.States.NeedsReview
                    sb.Append(m_refMsg.GetMessage("lbl needs review"))
                Case EkJobDS.LocalizationTargetRow.States.NeedsTranslation
                    sb.Append(m_refMsg.GetMessage("lbl needs translation"))
                Case EkJobDS.LocalizationTargetRow.States.SignedOff
                    sb.Append(m_refMsg.GetMessage("lbl signed off"))
                Case EkJobDS.LocalizationTargetRow.States.Translated
                    sb.Append(m_refMsg.GetMessage("lbl translated"))
                Case Else
                    sb.Append(data.State)
            End Select
        End If
        sb.Append("</div>")
        sb.Append("</div>")

        node.Text = sb.ToString

        Return node
    End Function
    ''' <summary>
    ''' To create Message node.
    ''' </summary>
    ''' <param name="Message"></param>
    ''' <param name="MessageStyle"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createMsgNode(ByVal Message As String, Optional ByVal MessageStyle As MessageStyleType = MessageStyleType.ErrorMessage) As TreeNode
        Dim node As New TreeNode
        Dim sb As New StringBuilder

        node.Value = "msg"
        node.PopulateOnDemand = False
        node.SelectAction = TreeNodeSelectAction.None
        Select Case MessageStyle
            Case MessageStyleType.ErrorMessage
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/error.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl alert")
                sb.Append("<div class=""L10nErrorMessage"">")
                sb.Append(Message)
                sb.Append("</div>")
            Case MessageStyleType.Information
                node.ImageUrl = m_refContentApi.AppPath & "images/UI/Icons/information.png"
                node.ImageToolTip = m_refMsg.GetMessage("lbl desc")
                sb.Append("<div class=""L10nMessage"">")
                sb.Append(Message)
                sb.Append("</div>")
        End Select

        node.Text = sb.ToString

        Return node
    End Function
    ''' <summary>
    ''' To formate Error mesage.
    ''' </summary>
    ''' <param name="ErrorMessage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function formatErrorMessage(ByVal ErrorMessage As String) As String
        Dim sb As New StringBuilder
        If (Not IsNothing(ErrorMessage) AndAlso ErrorMessage.Length > 0) Then
            sb.Append("<img class=""L10nErrorAlert"" src=""" & m_refContentApi.AppPath & "images/UI/Icons/error.png"" alt=""" & EkFunctions.HtmlEncode(ErrorMessage) & """ />")
        End If
        Return sb.ToString
    End Function
    ''' <summary>
    ''' To format DateTime.
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="ClassName"></param>
    ''' <param name="ToolTip"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function formatDateTime(ByVal dt As DateTime, ByVal ClassName As String, Optional ByVal ToolTip As String = "") As String
        Dim sb As New StringBuilder
        sb.Append("<div class=""" & ClassName & """")
        If (dt <> DateTime.MinValue) Then
            Dim strToolTip As String = dt.ToString()
            If (Not IsNothing(ToolTip) AndAlso ToolTip.Length > 0) Then
                strToolTip = ToolTip & ": " & strToolTip
            End If
            sb.Append(" title=""" & strToolTip & """>")
            sb.Append(dt.ToString("d"))
        Else
            sb.Append(">&#160;")
        End If
        sb.Append("</div>")
        Return sb.ToString
    End Function
    ''' <summary>
    ''' To format file SIze.
    ''' </summary>
    ''' <param name="FileSize"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function formatFileSize(ByVal FileSize As Long) As String
        If (FileSize < 1024) Then
            Return String.Format("{0:##,##0} bytes", FileSize)
        ElseIf (FileSize < 10 * 1024 * 1024) Then
            Return String.Format("{0:N} KB", (FileSize / 1024))
        Else
            Return String.Format("{0:N} MB", (FileSize / (1024 * 1024)))
        End If
    End Function
    ''' <summary>
    ''' To get Url of Image.
    ''' </summary>
    ''' <param name="FileName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getImageUrl(ByVal FileName As String) As String
        Return EkFunctions.QualifyURL(m_objSite.AppImgPath, FileName)
    End Function
    ''' <summary>
    ''' To get Data of Languages.
    ''' </summary>
    ''' <param name="LanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getLanguageData(ByVal LanguageID As Integer) As LanguageData
        Static m_LanguageData As Dictionary(Of Integer, LanguageData) = Nothing
        Dim objData As LanguageData = Nothing
        If (LanguageID <= 0) Then Return objData
        If (IsNothing(m_LanguageData)) Then
            m_LanguageData = New Dictionary(Of Integer, LanguageData)
        End If
        If (m_LanguageData.ContainsKey(LanguageID)) Then
            objData = m_LanguageData.Item(LanguageID)
        Else
            objData = m_objSite.GetLanguageById(LanguageID)
            m_LanguageData.Add(LanguageID, objData)
        End If
        Return objData
    End Function
#End Region

    Protected Sub Page_SaveStateComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SaveStateComplete

    End Sub
End Class
#End Region