<%@ Page Language="C#" AutoEventWireup="true" CodeFile="freewayprojectRetrieval.aspx.cs" Inherits="Workarea_Freeway_freewayprojectRetrieval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <%=StyleSheetJS %>
    <link rel="STYLESHEET" type="text/css" />    
	<script language="JavaScript" type="text/javascript" src="../java/jfunct.js"></script>
	<script language="JavaScript" type="text/javascript" src="../java/toolbar_roll.js"></script>
    <script language="javascript" type="text/javascript">
	    <!--
		function GoBackToCaller(){
			window.location.href = document.referrer;
			}
			
		function SubmitForm(FormName, Validate) {
			var go = true;
			if (Validate.length > 0) {
				if (eval(Validate)) {
					document.forms[0].submit();
					return false;
				}
				else {
					return false;
				}
			}
			else {
				document.forms[0].submit();
				return false;
			}
		}
			
    	function VerifyForm () {
    	
    	    return true;
    	}
			
	//-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div id="dhtmltooltip">
                    </div>

                    <script language="JavaScript" type="text/javascript" src="../java/workareahelper.js"></script>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="WorkareaTitlebar" id="txtTitleBar" runat="server" nowrap>
                            </td>
                        </tr>
                        <tr>
                            <td class="WorkareaToolbar-bk" id="htmToolBar" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="_startImport" runat="server" OnClick="_startImport_Click" Text="Start File Import Process" />
                    <asp:Panel id="_progressPanel" runat="server">
                        <iframe src="../localizationjobs.aspx" width="100%" height="580" frameborder="1" marginwidth="1" marginheight="4"></iframe>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
