#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : freeway  project retrieve
---------------------------------
*/
#endregion
#region using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
#endregion
#region  Workarea_Freeway_freewayprojectRetrieval
public partial class Workarea_Freeway_freewayprojectRetrieval : System.Web.UI.Page
{
    #region Private/Protected variables
    private StyleHelper m_refStyle = new StyleHelper();
	private Ektron.Cms.Common.EkMessageHelper m_refMsg;
	protected string StyleSheetJS = "";
#endregion
    #region Page event 
    protected void Page_Load(object sender, EventArgs e)
	{
		StyleSheetJS = m_refStyle.GetClientScript();

		ContentAPI contentApi = new ContentAPI();
		m_refMsg = contentApi.EkMsgRef;

		return;
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		if (IsPostBack)
			_startImport.Visible = false;

		SiteAPI refSiteApi = new SiteAPI();

		txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("Freeway Project Import");

		StringBuilder sb = new StringBuilder();

		sb.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
		sb.Append("<td>");

		sb.Append(m_refStyle.GetButtonEventsWCaption(refSiteApi.AppImgPath + "btn_back-nm.gif", "freewaydashboard.aspx",
			m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), ""));

		sb.Append("</td>");

		sb.Append("</tr></table>");

		htmToolBar.InnerHtml = sb.ToString();

		return;
    }
    #endregion
    #region Method
    /// <summary>
    /// To start Import 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _startImport_Click(object sender, EventArgs e)
	{
		LocalizationAPI locApi = new LocalizationAPI();

		locApi.StartImportTranslation();

		return;
    }
    #endregion
}
#endregion