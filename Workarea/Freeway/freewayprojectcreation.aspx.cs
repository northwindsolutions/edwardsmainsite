#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : Allows creating the project in Freeway
---------------------------------
*/
#endregion
#region using
using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using System.Globalization;
using DS = Ektron.Cms.LocalizationJobDataSet;
using System.Text;
#endregion
#region freewayprojectcreation
public partial class freewayprojectcreation : System.Web.UI.Page
{
    #region Private and Protected Variables
    private StyleHelper m_refStyle = new StyleHelper();
    private EkMessageHelper m_refMsg;
    protected string StyleSheetJS = "";
    #endregion

    #region Page Events
    /// <summary>
    /// Populates the Analysis code in the repsective dropdown list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Removes the page cockie from cockie repository
        Response.AddHeader("Cache-Control", "no-cache");
        Response.Expires = 0;
        Response.Cache.SetNoStore();
        Response.AddHeader("Pragma", "no-cache");

        //StyleSheetJS = m_refStyle.GetClientScript();
        jsStyleSheet.Text = (new StyleHelper()).GetClientScript();

        m_refMsg = (new CommonApi()).EkMsgRef;

        //Populating analysis code in respective Dropdownlist
        try
        {
            if (!IsPostBack)
            {
                LB.FreewayLib.Freeway.VojoIdentity vojoIdentity = FreewayConfiguration.CreateFreewayIdentity();
                if (vojoIdentity != null)
                {
                    LB.FreewayLib.Freeway.VojoAnalysisCodes analysisCodes = LB.FreewayLib.Freeway.VojoServer.GetAnalysisCodes(vojoIdentity);

                    if (analysisCodes.Count > 0)
                    {
                        if ((analysisCodes[0].Name.ToUpper() == "REFERENCE 1") || (analysisCodes[0].Values.Count == 0))
                        {
                            _lblanalysisCode1.Visible = false;
                            _ddlanalysisCode1.Visible = false;
                        }
                        else
                        {
                            _lblanalysisCode1.Visible = true;
                            _ddlanalysisCode1.Visible = true;

                            _lblanalysisCode1.Text = analysisCodes[0].Name + " :";
                            foreach (string ac in analysisCodes[0].Values)
                                _ddlanalysisCode1.Items.Add(ac);

                            ListItem _listselect = new ListItem("Select", "0");
                            _ddlanalysisCode1.Items.Insert(0, _listselect);
                            _ddlanalysisCode1.SelectedValue = "0";
                        }

                    }
                    if (analysisCodes.Count > 1)
                    {
                        if ((analysisCodes[1].Name.ToUpper() == "REFERENCE 2") || (analysisCodes[1].Values.Count == 0))
                        {
                            _lblanalysisCode2.Visible = false;
                            _ddlanalysisCode2.Visible = false;
                        }
                        else
                        {
                            _lblanalysisCode2.Visible = true;
                            _ddlanalysisCode2.Visible = true;

                            _lblanalysisCode2.Text = analysisCodes[1].Name + " :";
                            foreach (string ac in analysisCodes[1].Values)
                                _ddlanalysisCode2.Items.Add(ac);

                            ListItem _listselect = new ListItem("Select", "0");
                            _ddlanalysisCode2.Items.Insert(0, _listselect);
                            _ddlanalysisCode2.SelectedValue = "0";
                        }
                    }
                    if (analysisCodes.Count > 2)
                    {
                        if ((analysisCodes[2].Name.ToUpper() == "REFERENCE 3") || (analysisCodes[2].Values.Count == 0))
                        {
                            _lblanalysisCode3.Visible = false;
                            _ddlanalysisCode3.Visible = false;
                        }
                        else
                        {
                            _lblanalysisCode2.Visible = true;
                            _ddlanalysisCode2.Visible = true;

                            _lblanalysisCode3.Text = analysisCodes[2].Name + " :";
                            foreach (string ac in analysisCodes[2].Values)
                                _ddlanalysisCode3.Items.Add(ac);

                            ListItem _listselect = new ListItem("Select", "0");
                            _ddlanalysisCode3.Items.Insert(0, _listselect);
                            _ddlanalysisCode3.SelectedValue = "0";
                        }
                    }
                }

                else
                {
                        StringBuilder sberror = new StringBuilder();
                    sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                    //sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");
                    sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">No valid Freeway User Settings were found for the Freeway Connector. Please navigate to the User Settings node to complete this configuration.</td>");
                    sberror.Append("</tr></table>");
                    Response.Write(sberror.ToString());
                    Response.Flush();
                    Response.End();

                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("User account is invalid"))
            {
                StringBuilder sberror = new StringBuilder();
                sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
               // sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");
                sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">No valid Freeway User Settings were found for the Freeway Connector. Please navigate to the User Settings node to complete this configuration.</td>");

                sberror.Append("</tr></table>");
                Response.Write(sberror.ToString());
                Response.Flush();
                Response.End();
            }
            else if (ex.Message.Contains("A network-related"))
            {
                StringBuilder sberror = new StringBuilder();
                sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">Freeway Services are Unavailable. <br> Please contact the Administrator.</td>");

                sberror.Append("</tr></table>");
                Response.Write(sberror.ToString());
                Response.Flush();
                Response.End();
            }
            else if (ex.Message.Contains("Analysis codes are not configured"))
            {
                trAnalyasisCode.Visible = false;
                //lblNote.ForeColor = System.Drawing.Color.White;
                //lblMandatory.ForeColor = System.Drawing.Color.White;
                _analysiscode.ForeColor = System.Drawing.Color.White;
                _analysiscode.BackColor = System.Drawing.Color.White;
                _analysiscode.BorderColor = System.Drawing.Color.White;

                //    string srtprojectinfo = "Analysis codes are not configured for this customer";
                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + srtprojectinfo + "')", true);
            }
            else
            {
            //    string srtprojectinfo = "While creating Project - " + _txtprojectName.Text + " exception raised with the message - " + ex.Message;
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Create Click", "javascript:alert('" + srtprojectinfo + "')", true);
            StringBuilder sberror = new StringBuilder();
                sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                //sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">Freeway Services are Unavailable. <br> Please contact the Administrator.</td>");

                sberror.Append("</tr></table>");
                Response.Write(sberror.ToString());
                Response.Flush();
                Response.End();
            }
        }

        return;



        return;
    }

    /// <summary>
    /// on clicking the save button, the project information will be passsed to 
    /// the Freeway for project creation
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (IsPostBack)
        {
            //Modified by Supriya on 13th Aug 2010 to put Default date in startingdate and delivarydate
            DateTime startingdate;
            DateTime delivarydate;
            double NoofDays;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NoOfDays"]))
            {
                NoofDays = Convert.ToDouble(ConfigurationManager.AppSettings["NoOfDays"]);
            }
            else
            {
                NoofDays = 7;
            }

            if (hdnexpectedstartingdate.Value != string.Empty)
            {
                startingdate = Convert.ToDateTime(hdnexpectedstartingdate.Value);
                hdnexpectedstartingdate.Value = "";
            }
            else
            {
                startingdate = DateTime.Now.Date;
            }
            if (hdnexpecteddeliverydate.Value != string.Empty)
            {
                delivarydate = Convert.ToDateTime(hdnexpecteddeliverydate.Value);
                hdnexpecteddeliverydate.Value = "";
            }
            else
            {
                delivarydate = startingdate.AddDays(NoofDays);
            }

            if (startingdate >= DateTime.Now.Date && delivarydate >= DateTime.Now.Date && delivarydate >= startingdate)
            {
                // Project creation Code
                EktronProject ektronProject = new EktronProject();

                ektronProject.ExpectedStartingDate = startingdate;
                ektronProject.ExpectedDeliveryDate = delivarydate;
                ektronProject.POReference = _txtpoReference.Text;
                ektronProject.Description = _txtprojectName.Text;
                ektronProject.SpecialInstructions = _txtspecialInstruction.Text;
                if (_analysiscode.Text.Trim().Length > 0)
                {
                    if (_analysiscode.Text.Contains("/") && _analysiscode.Text.Split('/').Length == 3)
                        ektronProject.AnalysisCodes = _analysiscode.Text.Split('/');
                    else
                        ektronProject.AnalysisCodes = new string[3] { _analysiscode.Text, string.Empty, string.Empty };
                }

                try
                {
                    string projectId = ektronProject.CreateProject();

                    string srtprojectinfo = "Project - " + _txtprojectName.Text + " is created successfully and the created Project Id is - " + projectId;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Create Click", "javascript:alert('" + srtprojectinfo + "')", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Create Click", "javascript:alert('Project')",true);

                    _txtprojectName.Text = string.Empty;
                    _txtpoReference.Text = string.Empty;
                    _analysiscode.Text = string.Empty;
                    _txtspecialInstruction.Text = string.Empty;
                    _txtexpectedstartingdate.Text = string.Empty;
                    _txtexpecteddeliverydate.Text = string.Empty;
                    _ddlanalysisCode1.SelectedValue = "0";
                    _ddlanalysisCode2.SelectedValue = "0";
                    _ddlanalysisCode3.SelectedValue = "0";




                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("User account is invalid"))
                    {
                        StringBuilder sberror = new StringBuilder();
                        sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                       // sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");
                        sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">No valid Freeway User Settings were found for the Freeway Connector. Please navigate to the User Settings node to complete this configuration.</td>");
                        sberror.Append("</tr></table>");
                        Response.Write(sberror.ToString());
                        Response.Flush();
                        Response.End();
                    }
                    else if (ex.Message.Contains("A network-related"))
                    {
                        StringBuilder sberror = new StringBuilder();
                        sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                        sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">Freeway Services are Unavailable. <br> Please contact the Administrator.</td>");

                        sberror.Append("</tr></table>");
                        Response.Write(sberror.ToString());
                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        string srtprojectinfo = "While creating Project - " + _txtprojectName.Text + " exception raised with the message - " + ex.Message;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Create Click", "javascript:alert('" + srtprojectinfo + "')", true);
                    }
                }
            }
            else
            {
                if (startingdate < DateTime.Now.Date && startingdate != null)
                {
                    string errromessage = "javascript:alert('" + "Expected Starting date should not be older than the current date." + "')";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", errromessage, true);
                }
                if (delivarydate < DateTime.Now.Date && delivarydate != null)
                {
                    string errromessage = "javascript:alert('" + "Expected Delivary date should not be older than current date." + "')";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", errromessage, true);
                }
                if (delivarydate < startingdate)
                {
                    string errromessage = "javascript:alert('" + "Expected Delivary date should be greater than Expected Starting date." + "')";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", errromessage, true);
                }

            }
        }

        // Page header style
        SiteAPI refSiteApi = new SiteAPI();
        UserAPI userApi = new UserAPI();
        DataRow userRow = FreewayDB.GetUserRow(userApi.UserId, false);
        if (userRow != null)
        {
            bool IsGlobalUser = false;
            DataRow GlobaluserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(true);
            if (freewayGlobalUser != null)
            {
                //IsGlobalUser = (bool)freewayGlobalUser["ForceGlobalSettings"];
                IsGlobalUser = true;
            }
            else
            {
                IsGlobalUser = false;
                //IsGlobalUser = (bool)freewayGlobalUser["ForceGlobalSettings"];
            }
            //Modified by Supriya on 3rd Aug 2010 To change frame title from �Freeway Settings� to �Freeway User Settings�.
            if (IsGlobalUser)
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:73%'><b>Freeway Project Creation</b>  &nbsp;<i>(Connector is running in Global User Mode.)</i></td><td align='right' style='width:27%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");
            else
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:73%'><b>Freeway Project Creation</b>  &nbsp;<i>(Connector is running in Mapped Users Mode)</i></td><td align='right' style='width:27%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");

        }
        //txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("Freeway Project Creation");

        StringBuilder sb = new StringBuilder();

        sb.Append(@"<table width='100%' border='1' cellspacing='0' cellpadding='0'><tr>");
        sb.Append("<td align='left'>");

        sb.Append(@"<table width='100%' border='1' cellspacing='0' cellpadding='0'><tr>");
        sb.Append("<td align='left' style='width:1%'>");

        sb.Append(m_refStyle.GetButtonEventsWCaption(refSiteApi.AppImgPath + "btn_update-nm.gif", "#", "Save Freeway Project",
                m_refMsg.GetMessage("btn save"), @"Onclick=""javascript:return SubmitForm('freewayprojectcreation', 'VerifyForm()');"""));


        sb.Append("</td>");
       
        if (userRow != null)
        {
            sb.Append("</td> <td style='width:99%' align='Right'>");
            //---------Added by Supriya to show current environment and UserId of that environment
           // sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append(userRow["CurrentEnvironment"]);
            sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            //DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(userApi.UserId, false, userRow["CurrentEnvironment"].ToString());

            DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(true);

            if (freewayGlobalUser == null)
            {
                if (globalUserRow != null)
                {
                    sb.Append(userRow["FreewayUsername"]);
                }
            }
            else
            {
                sb.Append(freewayGlobalUser["FreewayUsername"]);
            }
            sb.Append("&nbsp; &nbsp;</b></div></td>");


            //if (globalUserRow != null && Convert.ToBoolean(globalUserRow["ForceGlobalSettings"]) == true)
            //    sb.Append(globalUserRow["FreewayUsername"]);
            //else
            //    sb.Append(userRow["FreewayUsername"]);
            //sb.Append("&nbsp; &nbsp;</b></div></td>");
        }
        else
        {
            sb.Append("</td> <td style='width:99%' align='Right'>");
            //---------Added by Supriya to show current environment and UserId of that environment
            sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            sb.Append("Not selected");
            sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            sb.Append("Not available");
            sb.Append("&nbsp; &nbsp;</b></div></td>");
        }
        sb.Append("</tr></table>");

        sb.Append("</td></tr></table>");

        htmToolBar.InnerHtml = sb.ToString();



        return;

    }
    #endregion
    /// <summary>
    /// Loads the project Description from the Ektron job.
    /// </summary>
    public FreewayContent FreewayContent
    {
        get
        {
            FreewayContent freewayContent = Session[FreewayConfiguration.FreewayContentSessionName] as FreewayContent;

            if (freewayContent == null)
            {
                freewayContent = new FreewayContent();

                freewayContent.LoadFromQueryString(Request.QueryString);

                if (freewayContent.Contents.Count == 0)
                    freewayContent.LoadFromCurrentSession();

                Session[FreewayConfiguration.FreewayContentSessionName] = freewayContent;
            }

            return freewayContent;
        }
    }



    protected void _txtexpectedstartingdate_changed(object sender, EventArgs e)
    {

    }
    protected void _txtexpecteddeliverydate_changed(object sender, EventArgs e)
    {

    }
}
#endregion