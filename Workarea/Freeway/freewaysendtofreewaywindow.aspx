<%@ Page Language="C#" AutoEventWireup="true" CodeFile="freewaysendtofreewaywindow.aspx.cs" Inherits="Workarea_Freeway_freewaysendtofreewaywindow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    function openworkarea()
    {
        var Lang_value = getQuerystring('Lang_value');
    
        if(Lang_value != "")
        {
            var url = "freewayworkarea.aspx?LangType=" + parseInt(Lang_value);
            window.open(url ,"workarea","status = yes,resizable = no,top = 0, left = 0,height = 680, width = 1010");
            window.close();
        }
        else
        {
            window.open("freewayworkarea.aspx?LangType=1033","workarea","status = yes,resizable = no,top = 0, left = 0,height = 680, width = 1010");
            window.close();
        }
    }
    
    function getQuerystring(key, default_)
    {
             
      if (default_==null) default_=""; 
      key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
      var qs = regex.exec(window.location.href);
      if(qs == null)
        return default_;
      else
        return qs[1];
    }
    </script>
</head>
<body onload = "openworkarea()">
    <form id="form1" runat="server" >
    <div>
    
    </div>
    </form>
</body>
</html>
