<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest = "false" CodeFile="freewaysettings.aspx.cs" Inherits="freewaysettings" %>

<%@ Register Src="freewayusersettingscontrol.ascx" TagName="freewayusersettingscontrol"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Freeway Project Dashboard</title>
    <asp:literal id="jsStyleSheet" runat="server"/>
    <script language="JavaScript" type="text/javascript" src="../java/jfunct.js">
	</script>
	<script language="JavaScript" type="text/javascript" src="../java/toolbar_roll.js">
	</script>
	 <script language="JavaScript" type="text/javascript" src="../java/workareahelper.js">
	 </script>
<script language="javascript" type="text/javascript">

function setHeight()
{
var pnl=document.getElementById('pnl');
var Screenhight= screen.height;
if(navigator.appName=="Internet Explorer")
{
pnl.style.height=Screenhight-((Screenhight*19)/100)+"px";
}
else
{
pnl.style.height=Screenhight-((Screenhight*21)/100)+"px";
}
pnl.style.width="100%";
}

     function showAbout()
     { 
    
     window.open("aboutCMSConnector.aspx","About","width=455,height=370,left=280px,top=180px,resizable=no",false)
    
     }
	   
		function GoBackToCaller()
		{
			window.location.href = document.referrer;
			}
			
		function SubmitForm(FormName, Validate) {
			var go = true;
			if (Validate.length > 0) {
				if (eval(Validate)) {
					document.forms[0].submit();
					return false;
				}
				else {
					return false;
				}
			}
			else {
				document.forms[0].submit();
				return false;
			}
		}
			
    	function VerifyForm () {
    	//debugger;
    	   if(document.forms[0].Freewayusersettingscontrol1__freewayServer.value!="")
    	   {

            if(document.forms[0].Freewayusersettingscontrol1__username.disabled!=true)
            {
    	           if(document.forms[0].Freewayusersettingscontrol1__username.value=="") 
    	           {    	        
    	                document.forms[0].Freewayusersettingscontrol1__username.focus();
   	                    alert('Global users Freeway Username is required');
   	                    return false;
   	               }
   	               //else if(document.forms[0].Freewayusersettingscontrol1_hdnPasswordGlobalUser.value=="")  	          
    	           else if(document.forms[0].Freewayusersettingscontrol1__password.value=="") 
  	               {
    	                document.forms[0].Freewayusersettingscontrol1__password.focus();
    	                alert('Global users Freeway Password is required');
   	                    return false;
                   }  
                   else if(document.forms[0].Freewayusersettingscontrol1__password.value!=document.forms[0].Freewayusersettingscontrol1__ConfirmPassword.value)
                   {
                        document.forms[0].Freewayusersettingscontrol1__password.focus();
    	                alert('Freeway Password and Confirm Freeway Password does not match');
   	                    return false;
                   }             
               }
            }
//            else
//            {

//                    var  cotrol = 'Freewayusersettingscontrol1_gvUsermapping_ctl';
//                    var  textbox = '_txtFreewayUserName';
//                    var gvDrv = document.getElementById('Freewayusersettingscontrol1_gvUsermapping');
//                
//                    var savecount = 0;
//                    for (i=1; i<gvDrv.rows.length; i++)
//                    {
//                        var row = gvDrv.rows[i];
//                        var returnval = getgridval(i,row.cells[1].getElementsByTagName('span'),row.cells[2].getElementsByTagName('input'),row.cells[3].getElementsByTagName('input'),row.cells[4].getElementsByTagName('input')); 
//                        
//                        //var returnval = getgridval(savecount,gvDrv.rows.length-1,i,row.cells[0].getElementsByTagName('input'),row.cells[1].getElementsByTagName('span'),row.cells[2].getElementsByTagName('input'),row.cells[3].getElementsByTagName('input')); 
//                        
//                        if(!returnval)
//                            return false;
//                    }
//            }
    	 return true;
   	}
   	
   	function getgridval(currentsave,EktronUserName,inputID,inputPasswd,inputConfirmPassword)
   	{
   	    
   	    var  FreewayUserID, FreewayUserPasswd,ConfirmFreewayUserPasswd,objFreewayUserID,objFreewayUserPasswd,objConfirmFreewayUserPasswd;
   	    if( EktronUserName != undefined && inputID != undefined && inputPasswd != undefined && inputConfirmPassword!=undefined )
   	    { 
   	            
   	             var maxid = inputID.length; 
                 for(j = 0; j < maxid; j++) 
                 {                  
                     if(inputID[j].getAttribute('type') == 'text') 
                     { 
                        objFreewayUserID = document.getElementById(inputID[j].id)
                        //FreewayUserID = inputID[j].getAttribute('value'); 
                        FreewayUserID = inputID[j].value; 
                     } 
                 }                  
                 var maxpasswd = inputPasswd.length; 
                 for(j = 0; j < maxpasswd; j++) 
                 { 
                    
                     if(inputPasswd[j].getAttribute('type') == 'password') 
                     { 
                        objFreewayUserPasswd = document.getElementById(inputPasswd[j].id)
                        //FreewayUserPasswd = inputPasswd[j].getAttribute('value'); 
                        FreewayUserPasswd = inputPasswd[j].value; 
                     } 
                 } 
                 
                 var maxektronusername = EktronUserName.length; 
                 for(j = 0; j < maxektronusername; j++) 
                 { 
                    
                    EktronUserName = EktronUserName[j].childNodes.item(0).nodeValue;
                 }   
                 var maxConfirmPassword = inputConfirmPassword.length; 
                 for(j = 0; j < maxConfirmPassword; j++) 
                 { 
                    
                     if(inputConfirmPassword[j].getAttribute('type') == 'password') 
                     { 
                        objConfirmFreewayUserPasswd = document.getElementById(inputConfirmPassword[j].id)
                        //ConfirmFreewayUserPasswd = inputConfirmPassword[j].getAttribute('value'); 
                        ConfirmFreewayUserPasswd = inputConfirmPassword[j].value; 
                     } 
                 }                  
                
                 
                if(FreewayUserPasswd == null || FreewayUserID == null ||FreewayUserPasswd==""||FreewayUserID ==""  )                
                {
                   if( EktronUserName.length>0)
                   {
                           if((FreewayUserPasswd == null ||FreewayUserPasswd=="") && (FreewayUserID == null||FreewayUserID =="") )  
                           {
                           }
                           else
                           {
                            alert('Freeway User Username and Password are required for Ektron User - '+ EktronUserName);
                            if(FreewayUserID == null)
                                objFreewayUserID.focus();
                            else if(FreewayUserPasswd == null)
                                objFreewayUserPasswd.focus();
                            return false;
                            }
                    }
                    else
                    {
                     alert('Freeway User Username and Password are required ');
                     return false;
                    }
                    
                } 
                else                
                   {
                       if(ConfirmFreewayUserPasswd!=FreewayUserPasswd)
                       {
                        alert('Freeway User Password and Confirm Password does not match.');
                        objConfirmFreewayUserPasswd.focus();
                        return false;
                       }
                   }  

         }
         return true;
   	}	
   	

</script>
   
   <style type="text/css">
            #dvUserGroups p {padding:0em 1em 1em 1em;margin:0;font-weight:bold;color:#1d5987;}
            #dvUserGroups ul.userGroups {list-style:none;margin:0 0 0 1em;padding:0;border:1px solid #d5e7f5;}
            #dvUserGroups ul.userGroups li {display:block;padding:.25em;border-bottom:none;}
            #dvUserGroups ul.userGroups li.stripe {background-color:#e7f0f7;}
            #dvWorkpage td.label {width:auto;}
        </style>
         </head>  
 <body onload="setHeight()">
    <form id="form1" runat="server" >
    <%--<asp:Panel runat="server" Height="470" ID="pnl" Width="100%" ScrollBars="Horizontal">--%>
    <asp:Panel runat="server"  ID="pnl"  ScrollBars="auto">

       <%-- <table width="120%" cellpadding="0" cellspacing="0">--%>
       <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div id="dhtmltooltip">
                    </div>
 <%--<asp:Panel ID="Panel1" Height="43" width="120%" runat="server">--%>
                   <asp:Panel Height="43" width="100%" runat="server">
                        <table width="100%" class="ektronPageHeader">
                            <tr>
                                <td id="txtTitleBar" class="ektronTitlebar" runat="server" nowrap>
                                </td>
                            </tr>
                            <tr>
                                <td id="htmToolBar" class="ektronToolbar" runat="server">
                                
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                   <%-- <asp:Panel ID="Panel1" Height="411" width="120%" ScrollBars="Vertical" runat="server">--%>
                   <%-- <asp:Panel  ScrollBars="auto" runat="server">--%>
                               <uc1:freewayusersettingscontrol ID="Freewayusersettingscontrol1" runat="server"  />
                               <input type="hidden" id="freewaySaveChecked"/>
                   <%-- </asp:Panel> --%>      
                </td>
            </tr> 
                </table>    
        </asp:Panel>
    </form>
</body>
</html>
  
    
    
    
	
  
    
 
   


