#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : This page will dispaly the user list and set global user.
---------------------------------
*/
#endregion
#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using System.Text;
#endregion

public partial class freewaysettings : System.Web.UI.Page
{
    #region Private and Protected Variables
    StyleHelper m_refStyle = new StyleHelper();
    EkMessageHelper m_refMsg;
    protected string StyleSheetJS = "";
    private string _userType = string.Empty;
    private bool _editMode = false;
    #endregion

    #region Page Events
    /// <summary>
    /// Applyies the stylesheet on page and retrieves the querystring values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //RegisterResources();

        //Applyies the stylesheet on the header of the page.
        jsStyleSheet.Text = (new StyleHelper()).GetClientScript();
        //StyleSheetJS = m_refStyle.GetClientScript();
        m_refMsg = (new CommonApi()).EkMsgRef;

        //ContentAPI contentApi = new ContentAPI();
        //m_refMsg = contentApi.EkMsgRef;

        //Retrieves the usertype from Querystrings
        if (!string.IsNullOrEmpty(Request.QueryString["usertype"]))
            _userType = Request.QueryString["usertype"];

        //Retrieves the mode from Querystrings and get the bollean value based on edit value
        if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
            _editMode = Request.QueryString["mode"].Equals("edit");

        return;
    }

    /// <summary>
    /// Manages the header And title bar information
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        //Manages the Title bar information
        SiteAPI refSiteApi = new SiteAPI();

       
        //Manages the Header information
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<table width='100%' border='1' cellspacing='0' cellpadding='0'><tr>");
        sb.Append("<td align='left'>");

        sb.Append(@"<table width='100%' border='1' cellspacing='0' cellpadding='0'><tr>");
        sb.Append("<td align='left' style='width:1%'>");

        if (_editMode)
        {
            //// Shows the Save and Back button on the header and applies javascript
            //sb.Append(m_refStyle.GetButtonEventsWCaption(refSiteApi.AppImgPath + "btn_update-nm.gif", "#", "Update the Freeway User Settings",
            //    m_refMsg.GetMessage("btn update"), @"Onclick=""javascript:return SubmitForm('userinfo', 'VerifyForm()');"""));
            //sb.Append(m_refStyle.GetButtonEventsWCaption(refSiteApi.AppPath + "images/UI/Icons/back.png", "javascript:GoBackToCaller()",
            //m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), ""));
        }

        else
       
        {
            //sb.Append(m_refStyle.GetButtonEventsWCaption(refSiteApi.AppPath + "images/UI/Icons/contentEdit.png",
            //    string.Format("freewaysettings.aspx?usertype={0}&mode=edit", _userType), "Edit the Ektron User's Freeway Credentials",
            //    m_refMsg.GetMessage("btn edit"), ""));
        }

        UserAPI userApi = new UserAPI();
        DataRow userRow = FreewayDB.GetUserRow(userApi.UserId, false);
        bool IsGlobalUser = false;
        if (userRow != null)
        {            
            DataRow GlobaluserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(true);
            if (freewayGlobalUser != null)
            {
                IsGlobalUser = true;
                //IsGlobalUser = (bool)GlobaluserRow["IsGlobal"];
            }
            else
            {
                IsGlobalUser = false;
            }
            //Modified by Supriya on 3rd Aug 2010 To change frame title from �Freeway Settings� to �Freeway User Settings�.
            if (IsGlobalUser)
                //txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:78%'>Freeway Project Dashboard  &nbsp;<i>(Connector is running in Global user mode)</i></td><td align='right' style='width:22%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:78%';  font-weight:bold>Freeway User Settings  &nbsp;<i>(Connector is running in Global User Mode.)</i></td><td align='right' style='width:22%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");
            else
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:78%';  font-weight:bold>Freeway User Settings  &nbsp;<i>(Connector is running in Mapped Users Mode.)</i></td><td align='right' style='width:22%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");


            //sb.Append("</td> <td style='width:99%' align='Right'>");
            ////---------Added by Supriya to show current environment and UserId of that environment
            //sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append(userRow["CurrentEnvironment"]);
            //sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            //DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(-1,false,userRow["CurrentEnvironment"].ToString ());



            

            //if (freewayGlobalUser == null)
            //{
            //    if (globalUserRow != null)
            //    {
            //        sb.Append(userRow["FreewayUsername"]);
            //    }
            //}
            //else
            //{
            //    sb.Append(freewayGlobalUser["FreewayUsername"]);
            //}
            //sb.Append("&nbsp; &nbsp;</b></div></td>");

            ////if (globalUserRow != null && Convert.ToBoolean( globalUserRow["ForceGlobalSettings"])==true)
            ////    sb.Append(globalUserRow["FreewayUsername"]);
            ////else
            ////    sb.Append(userRow["FreewayUsername"]);
            ////sb.Append("&nbsp; &nbsp;</b></div></td>");

            sb.Append("</td> <td style='width:99%' align='Right'>");
            //---------Added by Supriya to show current environment and UserId of that environment
           // sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append(userRow["CurrentEnvironment"]);
           // sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(-1,false,userRow["CurrentEnvironment"].ToString ());

            if (freewayGlobalUser == null)
            {
                if (globalUserRow != null)
                {
                   // sb.Append(userRow["FreewayUsername"]);
                }
            }
            else
            {
               // sb.Append(freewayGlobalUser["FreewayUsername"]);
            }
            sb.Append("&nbsp; &nbsp;</b></div></td>");

            //if (globalUserRow != null && Convert.ToBoolean( globalUserRow["ForceGlobalSettings"])==true)
            //    sb.Append(globalUserRow["FreewayUsername"]);
            //else
            //    sb.Append(userRow["FreewayUsername"]);
            //sb.Append("&nbsp; &nbsp;</b></div></td>");
        }
        else
        {
            DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(true);
            if (freewayGlobalUser != null)
            {
                IsGlobalUser = true;
            }
            else
            {
                IsGlobalUser = false;
            }
            //Modified by Supriya on 3rd Aug 2010 To change frame title from �Freeway Settings� to �Freeway User Settings�.
            if (IsGlobalUser)
                //txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:78%'>Freeway Project Dashboard  &nbsp;<i>(Connector is running in Global user mode)</i></td><td align='right' style='width:22%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:78%';font-weight:bold>Freeway User Settings  &nbsp;<i>(Connector is running in Global User Mode.)</i></td><td align='right' style='width:22%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");
            else
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:78%';font-weight:bold>Freeway User Settings  &nbsp;<i>(Connector is running in Mapped Users Mode.)</i></td><td align='right' style='width:22%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");

            //sb.Append("</td> <td style='width:99%' align='Right'>");
            ////---------Added by Supriya to show current environment and UserId of that environment
            //sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append("Not selected");
            //sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            //sb.Append("Not available");
            //sb.Append("&nbsp; &nbsp;</b></div></td>");
        }
        sb.Append("</tr></table>");

        sb.Append("</td></tr></table>");
        htmToolBar.InnerHtml = sb.ToString();

        return;
    }
    #endregion

    #region Protected Fucntoins
    /// <summary>
    /// Registers the resource files and javascripts used for the page.
    /// </summary>
    protected void RegisterResources()
    {
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);

        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronToolBarRollJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronModalJS);

    }
    #endregion

}
