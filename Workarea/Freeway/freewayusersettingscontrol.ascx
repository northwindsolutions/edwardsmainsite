﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="freewayusersettingscontrol.ascx.cs" Inherits="freewayusersettingscontrol" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>

<style type="text/css">
    #DDLcity1 {
        width: 128px;
        margin: 0 0 0 0;
        font: 12px tahoma;
        direction: ltr;
        max-height: 200px;
        overflow-y: scroll;
        position: relative;
    }
</style>




<style type="text/css">
    
    .ektronGrid {
    }

    .auto-style13 {
        width: 110px;
        height: 30px;
    }

    .auto-style14 {
        width: 398px;
        height: 30px;
    }

    .auto-style19 {
        width: 226px;
        height: 21px;
    }

    .DDlcityE {
        margin-left: 0px;
    }

    </style>


<style type="text/css">
    .Clicked {
        display: block;
        padding: 4px 18px 4px 18px;
        float: left;
        /*background-image: url('../Images/InitialImage.jpg') ;*/
        color: White;
        background-color: dimgray;
        font-weight: bold;
    }

    .Initial:hover {
        color: White;
        /*background: url('../Images/GrayImage.jpg') no-repeat right top;*/
    }

    .Initial {
        float: left;
        display: block;
        /*background: url('../Images/GrayImage.jpg') no-repeat right top;*/
        padding: 4px 18px 4px 18px;
        color: Black;
        background-color: lightgray;
    }

    .auto-style101 {
        width: 398px;
    }

    .auto-style109 {
        width: 110px;
    }

    .auto-style128 {
        height: 56px;
    }
    .auto-style129 {
        width: 195px;
    }
    </style>





<script type="text/javascript">

    function validate() {
        var valCheck = false;
        if ((document.getElementById('<%= txtUserName.ClientID %>').value == "") || (document.getElementById('<%= txtPwd.ClientID %>').value == "")) {
            valCheck = false;
        }
        else {

            valCheck = true;
        }

        return valCheck;
    }

    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        document.getElementById("<%= HiddenField1.ClientID %>").value = x;
    }
    function ConfirmGlobal() {
        var c = document.getElementById("<%= lblFlipUserMode.ClientID %>").innerText;
        if (c.indexOf("Global") != -1) {
            return confirm("Are you sure you want to flip to Global User Mode?");
        }
        else {
            return confirm("Are you sure you want to flip to Mapped Users Mode?");
        }
    }
</script>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>


<table style="width: 750px">
    <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnFieldGlobal" runat="server" />
    <asp:HiddenField ID="hdnFieldMapped" runat="server" />
    <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" Width="665px">
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblFreewayServerUrl" runat="server" Font-Bold="True" Text="Freeway Server URL:" Font-Size="Small" ForeColor="#666666" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFreeewayUrl" Width="391px" MaxLength="100" style="margin-right:5px;padding-right:0px;" ValidationGroup="Submit">https://fwapi.demo.lionbridge.com</asp:TextBox>
                             <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </td>                                                
                        
                    </tr>
                </table>
                <br />
                <fieldset id="fieldSet1" ;="" style="border: solid 1px Black; height: 139px; width: 670px;">
                    <legend>Active User Mode</legend>
                    <asp:Panel ID="Panel2" runat="server" style="padding-right:150px" Width="445px">
                        <table>
                            <tr>
                                <td class="auto-style128"><%--<asp:ImageButton ID="imgChangeMode" runat="server" Height="59px" OnClick="imgChangeMode_Click" OnClientClick="return ConfirmGlobal();" Style="margin-left: 0px; border:none 0 transparent" Width="75px" BorderStyle="None" />                               --%>
                                    <asp:RadioButtonList ID="rdbListItem" runat="server" Height="23px" style="margin-left: 0px" Width="207px">
                                        <asp:ListItem Selected="True">Global User Mode</asp:ListItem>
                                        <asp:ListItem>Mapped User Mode</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnActivate" runat="server" OnClick="btnActivate_Click" Text="Activate" Width="72px" />
                                </td>
                            </tr>
                          <tr>
                              <td>
                                  <br />
                              </td>
                          </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFlipUserMode" runat="server" Font-Bold="True" Text="The connector is currently running in : "></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
                
                <br />



            </asp:Panel>
        </td>
    </tr>
</table>
                                           
<br />

 




<table>
    <tr>
        <td style="width:194px">
            <asp:Button Text="Mapped Users Freeway Credentials" BorderStyle="None" ID="btnMappedTab" CssClass="Initial" runat="server" OnClick="btnMappedTab_Click" />
            <asp:Button Text="Global User Freeway Credentials" BorderStyle="None" ID="btnGlobalTab" CssClass="Initial" runat="server" OnClick="btnGlobalTab_Click" />
            <asp:MultiView ID="MainView" runat="server" >
                <asp:View ID="View1" runat="server">
                    <table style="width: 65%; border-width: 1px;  border-color: #666; border-style: solid">
                        <tr >
                            <td style ="padding-left:5px">
                                <asp:Label ID="Label5" runat="server" Text=" Entering credentials below will validate and save them for mapped users.  In order to switch the connector mode please click activate button above." ForeColor="#666666"></asp:Label>
                            </td>
                         </tr>
                        <tr>
                            <td>
                                <br />
                                <table>
                                    <tr>
                                        <td id="Td4" style ="padding-left:5px" runat="server" class="auto-style19">&nbsp;<asp:Label ID="lblAvailEktronUser" runat="server" Text="Available Ektron User:" Font-Bold="True" Font-Size="Small" ForeColor="#666666"></asp:Label>
                                        </td>
                                       
                                        <td class="auto-style129">
                                            <asp:DropDownList ID="ddlAvailEktronUser" runat="server"   CssClass="DDlcityE" ForeColor="#CA0202"  onmousedown="this.size=5;" onblur="this.size=1;" onfocusout="this.size=1;" 
                                                ondblclick="this.size=1;"  style="position: absolute; left: 229px; width: 184px; top: 375px;" >
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddMapping" runat="server" TabIndex="7" Text="Add Mapping" Width="146px" OnClick="btnAddMapping_Click"  Height="24px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                       </tr>


                        <tr>
                            <td style ="padding-left:5px">
                                <asp:GridView runat="server" ID="gvUserDetail" AutoGenerateColumns="False" AllowPaging="True" style="margin-top:10px"
                                    Width="678px" ShowHeaderWhenEmpty="True" CellPadding="3" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                    OnRowCancelingEdit="gvUserDetail_RowCancelingEdit" OnRowDeleting="gvUserDetail_RowDeleting" OnRowEditing="gvUserDetail_RowEditing"
                                    OnRowUpdating="gvUserDetail_RowUpdating" OnRowUpdated="gvUserDetail_RowUpdated" OnRowCommand="gvUserDetail_RowCommand" OnRowDataBound="gvUserDetail_RowDataBound" OnSelectedIndexChanged="gvUserDetail_SelectedIndexChanged">
                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Ektron User Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtEktronUserName" runat="server" Text='<%# Eval("EktronUserName") %>' Width="80%" ReadOnly="True" Enabled="false"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("EktronUserName") %>' Enabled="<%# false %>"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Freeway WS User Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFreewayUserName" runat="server" Text='<%# Eval("FreewayUsername") %>' Width="80%" AutoPostBack="<%# false %>"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("FreewayUsername") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Freeway WS Password">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPassword" runat="server" Text='<%# Eval("FreewayPassword") %>' TextMode="Password" Width="80%" AutoPostBack="<%# false %>"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("FreewayPassword") %>' Visible="<%# true %>"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Save" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="ConfirmDelete();" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                                 <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Save" />                                           
                                                 <asp:Button ID="btDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="ConfirmDelete();" />                                          
                                                 <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                      
                                    </Columns>
                                    <EditRowStyle BorderStyle="Solid" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Width="30px" />
                                    <PagerStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid" BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle Height="10px" BorderStyle="Solid" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="Gray" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#383838" />
                                </asp:GridView>
                            </td>
                        </tr>

                    </table>
                </asp:View>
                <asp:View ID="View2" runat="server">                    
                    <table style="width: 69%; margin-top:15px; border-width: 1px; border-color: #666; border-style: solid">
                        <tr>
                            <td style ="padding-left:5px">

                                <asp:Label ID="Label6" runat="server" style="margin-top:15px" Text=" Entering credentials below will validate and save them for global user.  In order to switch the connector mode please click activate button above." ForeColor="#666666"></asp:Label>
                                <br />

                                <table style="width: 676px; height: 103px;">
                                    <tr>
                                        <td id="Td1"  runat="server" style ="padding-left:2px"><asp:Label ID="Label7" runat="server" Text="UserName:" Font-Bold="True" Font-Size="Small" ForeColor="#666666"></asp:Label>
                                        </td>
                                        <td class="auto-style101">
                                            <asp:TextBox ID="txtUserName" runat="server" Width="419px" MaxLength="50" TabIndex="3" ValidationGroup="Submit" Style="margin-left: 1px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="Td5" runat="server" style ="padding-left:3px"><asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="#666666" Text="Password:"></asp:Label>
                                        </td>
                                        <td class="auto-style101">
                                            <asp:TextBox ID="txtPwd" runat="server" MaxLength="50" TabIndex="4" ValidationGroup="Submit" Width="419px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style13"></td>
                                        <td class="auto-style14">
                                            <asp:Button ID="btnSet" runat="server" OnClick="btnSet_Click" TabIndex="5" Text="Save" ValidationGroup="Submit" ViewStateMode="Enabled" Width="183px" Height="23px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style109"></td>
                                        <td class="auto-style101">
                                            <asp:Label ID="lblGlobalError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>

            </asp:MultiView>
        </td>
    </tr>
</table>







<%-- <table>
                                                <tr>
                                                    <td class="auto-style82">
                                                        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" UseVerticalStripPlacement="True" Width="601px" Font-Bold="True">                                                            
                                                          <asp:TabPanel ID="tabMappedUser" runat="server">
                                                                <HeaderTemplate>
                                                                    Mapped Users Freeway Credentials
                                                                </HeaderTemplate>
                                                                 <ContentTemplate>
                                                                
                                                                    <asp:Label ID="Label5" runat="server" Text="Entering credentials below will validate and save them for mapped users. In order to flip the connector mode please use the toggle switch above." ForeColor="#666666"></asp:Label>
                                                                    <br />
                                                                    <br />
                                                                        <table style="width: 625px">
                                                                            <tr>
                                                                                <td id="Td4" runat="server" class="auto-style19">&nbsp;<asp:Label ID="lblAvailEktronUser" runat="server" Text="Available Ektron User:" Font-Bold="True" Font-Size="Small" ForeColor="#666666"></asp:Label>
                                                                                </td>
                                                                                <td class="auto-style65">
                                                                                    <asp:DropDownList ID="ddlAvailEktronUser" runat="server" Width="183px" CssClass="DDlcityE" ForeColor="#CA0202" onmousedown="this.size=5;" onblur="this.size=1;" onfocusout="this.size=1;" ondblclick="this.size=1;" AppendDataBoundItems="True">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td class="auto-style32">
                                                                                    <asp:Button ID="btnAddMapping" runat="server" TabIndex="7" Text="Add Mapping" Width="126px" OnClick="btnAddMapping_Click" Style="margin-left: 31px" Height="23px" />
                                                                                </td>
                                                                                <td class="auto-style67"></td>
                                                                            </tr>
                                                                        </table>


                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView runat="server" ID="gvUserDetail" AutoGenerateColumns="False" AllowPaging="True"
                                                                                        Width="572px" ShowHeaderWhenEmpty="True" CellPadding="3" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                                                                        OnRowCancelingEdit="gvUserDetail_RowCancelingEdit" OnRowDeleting="gvUserDetail_RowDeleting" OnRowEditing="gvUserDetail_RowEditing"
                                                                                        OnRowUpdating="gvUserDetail_RowUpdating" OnRowUpdated="gvUserDetail_RowUpdated" OnRowCommand="gvUserDetail_RowCommand" OnRowDataBound="gvUserDetail_RowDataBound">
                                                                                        <AlternatingRowStyle BackColor="#CCCCCC" />                                                                                      
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="Ektron User Name">
                                                                                                <EditItemTemplate>
                                                                                                    <asp:TextBox ID="txtEktronUserName" runat="server" Text='<%# Eval("EktronUserName") %>' Width="80%" ReadOnly="True" Enabled="false"></asp:TextBox>
                                                                                                </EditItemTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("EktronUserName") %>' Enabled="<%# false %>"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Freeway WS User Name">
                                                                                                <EditItemTemplate>
                                                                                                    <asp:TextBox ID="txtFreewayUserName" runat="server" Text='<%# Eval("FreewayUsername") %>' Width="80%" AutoPostBack="<%# false %>"></asp:TextBox>
                                                                                                </EditItemTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FreewayUsername") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Freeway WS Password">
                                                                                                <EditItemTemplate>
                                                                                                    <asp:TextBox ID="txtPassword" runat="server" Text='<%# Eval("FreewayPassword") %>' TextMode="Password" Width="80%" AutoPostBack="<%# false %>"></asp:TextBox>
                                                                                                </EditItemTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("FreewayPassword") %>' Visible="<%# true %>"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Save" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Button ID="btDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="ConfirmDelete();" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <EditRowStyle BorderStyle="Solid" />
                                                                                        <FooterStyle BackColor="#CCCCCC" />
                                                                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Width="30px" />
                                                                                        <PagerStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid" BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                                        <RowStyle Height="10px" />
                                                                                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                                        <SortedAscendingHeaderStyle BackColor="Gray" />
                                                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                                        <SortedDescendingHeaderStyle BackColor="#383838" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                 </ContentTemplate>                                                                                                                                                                                        
                                                            </asp:TabPanel>
                                                           
                                                                     

                                                            <asp:TabPanel ID="tabGlobalUser" runat="server">
                                                                <HeaderTemplate>
                                                                    Global User Freeway Credentials
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <asp:Label ID="lblGlobalInfo" runat="server" Text="Entering credentials below will validate and save them for global user. In order to flip the connector mode please use the toggle switch above." ForeColor="#666666"></asp:Label>                                                                
                                                                        <table style="width: 591px; height: 103px;">
                                                                            <tr>
                                                                                <td id="Td2" runat="server" class="auto-style76">&nbsp;<asp:Label ID="lblUserName" runat="server" Text="UserName:" Font-Bold="True" Font-Size="Small" ForeColor="#666666"></asp:Label>
                                                                                </td>
                                                                                <td class="auto-style84">
                                                                                    <asp:TextBox ID="txtUserName" runat="server" Width="419px" MaxLength="50" TabIndex="3" ValidationGroup="Submit" Style="margin-left: 1px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td id="Td3" runat="server" class="auto-style76">&nbsp;<asp:Label ID="lblPwd" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="#666666" Text="Password:"></asp:Label>
                                                                                </td>
                                                                                <td class="auto-style84">
                                                                                    <asp:TextBox ID="txtPwd" runat="server" MaxLength="50" TabIndex="4" ValidationGroup="Submit" Width="419px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="auto-style13"></td>
                                                                                <td class="auto-style14">
                                                                                    <asp:Button ID="btnSet" runat="server" OnClick="btnSet_Click" TabIndex="5" Text="Save" ValidationGroup="Submit" ViewStateMode="Enabled" Width="183px" Height="23px" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="auto-style76"></td>
                                                                                <td class="auto-style84">
                                                                                    <asp:Label ID="lblGlobalError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                </ContentTemplate>
                                                            </asp:TabPanel>
                                                        </asp:TabContainer>
                                                    </td>
                                                </tr>
                                            </table>--%>