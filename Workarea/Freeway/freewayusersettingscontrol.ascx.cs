﻿#region Header Comment
/*
---------------------------------
Created By :    Alka Singh
Created Date :  11 July 2014
Details : This page will dispaly the user list and set global user.
---------------------------------
*/
#endregion
#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Ektron.Cms;
using System.Drawing;
using System.ComponentModel;
#endregion
#region Control
public partial class freewayusersettingscontrol : System.Web.UI.UserControl
{
    List<UserData> users1 = null;
    bool isGLobalExists = false;
    UserAPI userApi = new UserAPI();
    string globalfreewayUserName = string.Empty;
    string globalfreewayPassword = string.Empty;
    bool checkMode = true;


    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGlobalUsers();
        if (!IsPostBack)
        {
            gvUserDetail.Columns[0].ItemStyle.Width = 2000;
            gvUserDetail.Columns[1].ItemStyle.Width = 2500;
            gvUserDetail.Columns[2].ItemStyle.Width = 2000;
            gvUserDetail.Columns[3].ItemStyle.Width = 3000;
            btnMappedTab.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
            txtPwd.Attributes["type"] = "password";
            bindGridView();
           
            if (isGLobalExists)
            {
                rdbListItem.Items[0].Selected = true;
                lblFlipUserMode.Text = "The connector is currently running in: GLOBAL USER MODE";
                MainView.ActiveViewIndex = 1;
            }
            else
            {
                rdbListItem.Items[1].Selected = true;
                lblFlipUserMode.Text = "The connector is currently running in: MAPPED USER MODE";
                MainView.ActiveViewIndex = 0;
            }
        }
       
        lblGlobalError.Text = string.Empty;
    }


    protected void gvUserDetail_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var value = this.HiddenField1.Value;
        if (value == "false")
        {
            return;
        }
        try
        {
            int index = Convert.ToInt32(e.RowIndex);
            string ektronUserId = null;
            string ektronUserName = null;
            string FreewayUsername = null;
            DataTable dtCurrentTable = ViewState["GridDataTable"] as DataTable;

            dtCurrentTable.DefaultView.Sort = "EktronUserName Asc";

            dtCurrentTable = dtCurrentTable.DefaultView.ToTable();

            DataRow row = dtCurrentTable.Rows[index];
            if (!string.IsNullOrEmpty(Convert.ToString(dtCurrentTable.Rows[index]["EktronUserId"])))
            {
                ektronUserId = Convert.ToString(dtCurrentTable.Rows[index]["EktronUserId"]);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(dtCurrentTable.Rows[index]["EktronUserName"])))
            {
                ektronUserName = Convert.ToString(dtCurrentTable.Rows[index]["EktronUserName"]);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(dtCurrentTable.Rows[index]["FreewayUsername"])))
            {
                FreewayUsername = Convert.ToString(dtCurrentTable.Rows[index]["FreewayUsername"]);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(dtCurrentTable.Rows[index]["EktronUserId"])))
            {
                deleteUserRow(long.Parse(ektronUserId), FreewayUsername, false);
            }
            dtCurrentTable.Rows.Remove(row);
            gridDataBind(dtCurrentTable);

            DataTable dtEKList = ConvertToDatatable((List<UserData>)ViewState["EktronUserList"]);

            DataTable dtEkUser = (DataTable)ViewState["EktronUserData"];


            for (int i = 0; i <= dtEKList.Rows.Count - 1; i++)
            {
                if (ektronUserName == Convert.ToString(dtEKList.Rows[i]["Username"]))
                {
                    DataRow newdr = dtEkUser.NewRow();
                    newdr["Username"] = dtEKList.Rows[i]["Username"];
                    newdr["ID"] = dtEKList.Rows[i]["ID"];
                    dtEkUser.Rows.Add(newdr);
                    break;
                }
            }
            ddlAvailEktronUser.Items.Clear();
            
            DataView dvEk = (dtEkUser.DefaultView);
            dvEk.Sort = "Username ASC";
            ddlAvailEktronUser.DataSource = dvEk;
            ddlAvailEktronUser.DataValueField = "Username";
            ddlAvailEktronUser.DataBind();
            ddlAvailEktronUser.Items.Insert(0, "--Select--");
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvUserDetail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvUserDetail.EditIndex = -1;
        DataTable dtCurrentTable = (DataTable)ViewState["GridDataTable"];
        gridDataBind(dtCurrentTable);
    }
    protected void lstAvailEktron_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnAddMapping_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlAvailEktronUser.SelectedIndex > 0)
            {
                DataTable dtEkUser = (DataTable)ViewState["EktronUserData"];
                string availEkUser = Convert.ToString(ddlAvailEktronUser.SelectedItem);

                for (int irow = 0; irow <= dtEkUser.Rows.Count - 1; irow++)
                {
                    if (availEkUser == Convert.ToString(dtEkUser.Rows[irow]["Username"]))
                    {
                        dtEkUser.Rows[irow].Delete();
                        dtEkUser.AcceptChanges();
                        break;
                    }
                }
                ddlAvailEktronUser.Items.Clear();
                DataView dvEk = (dtEkUser.DefaultView);
                dvEk.Sort = "Username ASC";
                ddlAvailEktronUser.DataSource = dvEk;
                ddlAvailEktronUser.DataValueField = "Username";
                ddlAvailEktronUser.DataBind();
                ddlAvailEktronUser.Items.Insert(0, "--Select--");
                AddNewRowToGrid(availEkUser);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please select available ektron user.')", true);
                return;
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void chkFlipMode_CheckedChanged(object sender, EventArgs e)
    {
        //if (chkFlipMode.Checked)
        //{
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "ConfirmGlobal();", true);
        //    var value = this.hdnFieldGlobal.Value;
        //    if (value == "false")
        //    {
        //        return;
        //    }
        //    else
        //    {
        //        //Flip to mapped user mode
        //        if (!isGLobalExists)
        //        {
        //            if (validateUserMode())
        //            {
        //                chkFlipMode.AutoPostBack = true;
        //                SaveUserRowForGlobal(userApi.UserId, txtUserName.Text.Trim(), txtPwd.Text.Trim(), txtFreeewayUrl.Text.Trim(), chkFlipMode.Checked);
        //                string strMessage = "Connector mode successfully set for global user mode";
        //                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
        //                lblFlipUserMode.Text = "Click toggle switch above to flip the connector mode to mapped users mode";
        //            }
        //            else
        //            {
        //            }
        //        }
        //    }
        //}
        //else
        //{
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "ConfirmMapped();", true);
        //    var value = this.hdnFieldMapped.Value;
        //    if (value == "false")
        //    {
        //        return;
        //    }
        //    else
        //    {
        //        //Flip to global user mode
        //        long ektronUserId = userApi.UserId;
        //        deleteUserRow(ektronUserId, txtUserName.Text.Trim(), true);
        //        txtUserName.Text = string.Empty;
        //        txtPwd.Text = string.Empty;
        //        string strMessage = "Connector mode successfully set for mapped users mode";
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
        //        lblFlipUserMode.Text = "Click toggle switch above to flip the connector mode to global user mode";
        //        // txtUserName.Text = string.Empty;
        //        // txtPwd.Text = string.Empty;
        //    }
        //}
    }

    /* protected void rdbListItem_SelectedIndexChanged(object sender, EventArgs e)
     {
         if (rdbListItem.Items[0].Selected == true)
         {
             txtUserName.Enabled = true;
             txtPwd.Enabled = true;
             btnAddMapping.Enabled = false;
             ddlAvailEktronUser.Enabled = false;
             gvUserDetail.Enabled = false;

             txtUserName.Enabled = true;
             txtPwd.Enabled = true;
             btnSet.Enabled = true;

             //lblMandUserName.Visible = true;
             // lblMandPwd.Visible = true;
             lblGlobalError.Text = string.Empty;
             if (isGLobalExists)
             {
                 btnSetUserMode.Enabled = true;
                 btnSetUserMode.Visible = true;
             }
             //chkRemove.Visible = false;
             //btnRemove.Visible = false;

         }
         else if (rdbListItem.Items[1].Selected == true)
         {
             if (isGLobalExists)
             {
                 btnSetUserMode.Enabled = true;
             }
             else
             {
                 btnSetUserMode.Enabled = false;
             }
             btnSetUserMode.Visible = true;
             txtUserName.Enabled = false;
             txtPwd.Enabled = false;
             btnSet.Enabled = false;

             btnAddMapping.Enabled = true;
             ddlAvailEktronUser.Enabled = true;
             gvUserDetail.Enabled = true;
         }
         gvUserDetail.EditIndex = -1;
         DataTable dtCurrentTable = (DataTable)ViewState["GridDataTable"];
         gridDataBind(dtCurrentTable);
         gvUserDetail.Columns[3].Visible = false;
     }*/
    protected void gvUserDetail_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvUserDetail.EditIndex = e.NewEditIndex;
        gridDataBind((DataTable)ViewState["GridDataTable"]);

    }

  
    protected void gvUserDetail_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            DataTable dtEditTable = (DataTable)ViewState["GridDataTable"];
            //string origionalEktronUserName = Convert.ToString( dtEditTable.Rows[e.RowIndex]["EktronUserName"]);
            string origionalFreewayUserName = Convert.ToString(dtEditTable.Rows[e.RowIndex]["FreewayUsername"]);
            string origionalFreewayPassword = Convert.ToString(dtEditTable.Rows[e.RowIndex]["FreewayPassword"]);

            GridViewRow row = gvUserDetail.Rows[e.RowIndex];


            string ektronUserName = string.Empty;
            string freewayUserName = string.Empty;
            string freewayPassword = string.Empty;
            dtEditTable.Rows[row.DataItemIndex]["EktronUserName"] = ektronUserName = ((TextBox)gvUserDetail.Rows[e.RowIndex].FindControl("txtEktronUserName")).Text;

            dtEditTable.Rows[row.DataItemIndex]["FreewayUsername"] = freewayUserName = ((TextBox)gvUserDetail.Rows[e.RowIndex].FindControl("txtFreewayUserName")).Text; ;
            if (string.IsNullOrEmpty(freewayUserName))
            {
                //lblStandardError.Text = "Freeway Username can't be left blank";
                //lblStandardError.ForeColor = Color.Red;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter Freeway User name')", true);
                // txtFreeewayUrl.Text = string.Empty;
                //txtFreeewayUrl.Focus();
                return;
            }
            dtEditTable.Rows[row.DataItemIndex]["FreewayPassword"] = freewayPassword = ((TextBox)gvUserDetail.Rows[e.RowIndex].FindControl("txtPassword")).Text;
            if (string.IsNullOrEmpty(freewayPassword))
            {
                //lblStandardError.Text = "Freeway Password can't be left blank";
                //lblStandardError.ForeColor = Color.Red;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter Freeway Password')", true);
                return;
            }
            dtEditTable.Rows[row.DataItemIndex]["SubmitWhere"] = string.Empty;
            dtEditTable.Rows[row.DataItemIndex]["UseSsl"] = true;
            dtEditTable.Rows[row.DataItemIndex]["CanSubmit"] = false;
            dtEditTable.Rows[row.DataItemIndex]["ForceGlobalSettings"] = false;
            dtEditTable.Rows[row.DataItemIndex]["CurrentEnvironment"] = string.Empty;
            //gvUserDetail.EditIndex = -1;
            long id = long.Parse(getEktronUserIdFromEktronUserName(ektronUserName));
            //Need to uncomment this line for validation of valid freeway Credendials

            string validTicket = string.Empty;
            try
            {
                validTicket = CheckForValidCredentials(freewayUserName.Trim(), freewayPassword.Trim(), txtFreeewayUrl.Text.Trim());
            }
            catch (Exception ex)
            {
                exceptionMessages(ex);
                return;
            }
            //Need to uncomment the commented lines to apply the validations
            if (!string.IsNullOrEmpty(validTicket))
            {
                gvUserDetail.EditIndex = -1;
                // SaveUserRow(id, freewayUserName, freewayPassword, txtFreeewayUrl.Text.Trim(), (bool)rdbListItem.Items[0].Selected, origionalFreewayUserName, origionalFreewayPassword);
                SaveUserRow(id, freewayUserName, freewayPassword, txtFreeewayUrl.Text.Trim(), false, origionalFreewayUserName, origionalFreewayPassword);
                DataTable dtGrid = ConvertToDatatable((List<UserData>)ViewState["EktronUserList"]);
                FillGridView(dtGrid);
                
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Ektron and Freeway User mapping saved successfully')", true);
                return;
            }
        }
        catch (Exception ex)
        {
            exceptionMessages(ex);
        }

    }
    protected void btnSet_Click(object sender, EventArgs e)
    {
        if (validateUserMode())
        {
            //txtUserName.Enabled = false;
            //txtPwd.Enabled = false;
            string strMessageSuccess = "Credentials validated successfully. In order to switch the connector mode to Global User Mode please activate Global User Mode.";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessageSuccess + "')", true);
        }
    }


    protected void gvUserDetail_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {

        //gvUserDetail.EditIndex = -1;
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(true);
        if (freewayGlobalUser == null)
        {
            string srtMessage = "Force global setting not found";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + srtMessage + "')", true);
        }
        else
        {
            bool isRemoveGlobal = false;
            //if (chkRemove.Checked)
            //{
            //    isRemoveGlobal = true;
            //}
            RemoveForceGlobalSetting(isRemoveGlobal);
            string srtMessage = "Force global setting removed successfully";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + srtMessage + "')", true);
            txtUserName.Text = string.Empty;
            txtPwd.Text = string.Empty;
        }

    }
    //protected void btnSetUserMode_Click(object sender, EventArgs e)
    //{
    //    string userMode = btnSetUserMode.Text.Trim();
    //    //if ((bool)rdbListItem.Items[0].Selected)
    //    if(true)
    //    {
    //        {
    //            if (!isGLobalExists)
    //            {
    //                //SaveUserRowForGlobal(userApi.UserId, txtUserName.Text.Trim(), txtPwd.Text.Trim(), txtFreeewayUrl.Text.Trim(), (bool)rdbListItem.Items[0].Selected);
    //                SaveUserRowForGlobal(userApi.UserId, txtUserName.Text.Trim(), txtPwd.Text.Trim(), txtFreeewayUrl.Text.Trim(), true);
    //                string strMessage = "Connector mode successfully set for global user mode";
    //                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
    //                txtUserName.Enabled = true;
    //                txtPwd.Enabled = true;
    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (userMode.Contains("Global"))
    //        {
    //            btnSetUserMode.Text = "Set Connector To Mapped Users Mode";
    //            btnSetUserMode.Visible = true;
    //        }
    //        else if (userMode.Contains("Mapped"))
    //        {
    //            long ektronUserId = userApi.UserId;
    //            deleteUserRow(ektronUserId, txtUserName.Text.Trim(), true);
    //            string strMessage = "Connector mode successfully set for mapped users mode";
    //            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
    //            btnSetUserMode.Text = "Set Connector To Global User Mode";
    //            txtUserName.Text = string.Empty;
    //            txtPwd.Text = string.Empty;
    //        }
    //    }


    //    if (userMode.Contains("Global"))
    //    {
    //        btnSetUserMode.Text = "Set Connector To Mapped Users Mode";
    //        btnSetUserMode.Visible = true;
    //    }
    //    else if (userMode.Contains("Mapped"))
    //    {
    //        long ektronUserId = userApi.UserId;
    //        deleteUserRow(ektronUserId, txtUserName.Text.Trim(), true);
    //        string strMessage = "Connector mode successfully set for mapped users mode";
    //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
    //        btnSetUserMode.Text = "Set Connector To Global User Mode";
    //        txtUserName.Text = string.Empty;
    //        txtPwd.Text = string.Empty;
    //    }

    //}
    #endregion
    #region Methods

    private bool validateUserMode()
    {
        try
        {
            if (string.IsNullOrEmpty(txtFreeewayUrl.Text.Trim()))
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter Freeway Server Url')", true);
                string strMessage = "Please enter Freeway Server Url.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
                return false;
            }
            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter Freeway User name')", true);
                string strMessage = "Please enter Freeway User name.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
                return false;
            }
            if (string.IsNullOrEmpty(txtPwd.Text.Trim()))
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter Freeway Password')", true);
                string strMessage = "Please enter Freeway Password.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
                return false;
            }

            string validTicket = string.Empty;
            try
            {
                validTicket = CheckForValidCredentials(txtUserName.Text.Trim(), txtPwd.Text.Trim(), txtFreeewayUrl.Text.Trim());
            }
            catch (Exception ex)
            {
                exceptionMessages(ex);
                return false;
            }
            //string strMessageSuccess = "Credentials validated successfully. In order to flip the connector mode to Global User please click switch toggle button above.";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessageSuccess + "')", true);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public DataTable ConvertToDatatable<T>(IList<T> data)
    {
        PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        for (int i = 0; i < props.Count; i++)
        {
            PropertyDescriptor prop = props[i];
            table.Columns.Add(prop.Name, prop.PropertyType);
        }
        object[] values = new object[props.Count];
        foreach (T item in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = props[i].GetValue(item);
            }
            table.Rows.Add(values);
        }
        return table;
    }
    private bool checkIfEktronUserExists(string ektronUserName)
    {
        string srtMessage = ektronUserName + " is already mapped with Freeway User. \nYou can map " + ektronUserName + " with different Freeway user by edit and update.";
        DataTable dtEktronUser = (DataTable)ViewState["GridDataTable"];
        for (int i = 0; i <= dtEktronUser.Rows.Count - 1; i++)
        {
            if (ektronUserName == Convert.ToString(dtEktronUser.Rows[i]["EktronUserName"]))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + srtMessage + "')", true);
                return true;
            }
        }
        return false;
    }
    private void fillGlobalUsers()
    {
        DataRow freewayGlobalUser = FreewayDB.GetGlobalUserRow(true);
        if (freewayGlobalUser != null)
        {
            globalfreewayUserName = Convert.ToString(freewayGlobalUser["FreewayUsername"]);
            globalfreewayPassword = Convert.ToString(freewayGlobalUser["FreewayPassword"]);
            txtUserName.Text = globalfreewayUserName;
            txtPwd.Text = globalfreewayPassword;
            isGLobalExists = true;
        }

    }
    private void exceptionMessages(Exception ex)
    {
        //mode.ForeColor = Color.Red;
        if (ex.Message.Contains("Invalid URI: The format of the URI could not be determined."))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter valid Freeway Server URL.')", true);
            return;
        }
        else if (ex.Message.Contains("Invalid URI: The URI is empty."))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter Freeway Server URL.')", true);
            return;
        }
        else if (ex.Message.Contains("The request failed with the error message:"))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter valid Freeway User name or Password.')", true);
            return;
        }
        else if (ex.Message.Contains("An unexpected error occurred on a send"))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter valid Freeway Server URL.')", true);
            return;
        }
        else if (ex.Message.Contains("User account is invalid"))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter valid Freeway User name or Password.')", true);
            return;
        }
        else if (ex.Message.Contains("The remote name could not be resolved:"))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please enter valid Freeway Server URL.')", true);
            return;
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('Please ensure that you have entered a valid Freeway Server URL or User name and Password.')", true);
            return;
        }
    }
    private void FillAvailableEktronUsers(DataTable dtEkUser)
    {
        ViewState["EktronUserData"] = users1;

        dtEkUser.Columns.Add("EktronUserName");
        dtEkUser.Columns.Add("ID");

        for (int iUser = 0; iUser <= users1.Count - 1; iUser++)
        {
            if (!string.IsNullOrEmpty(users1[iUser].Username))
            {
                DataRow newdr = dtEkUser.NewRow();
                newdr["EktronUserName"] = users1[iUser].Username;
                newdr["ID"] = users1[iUser].Id;
                dtEkUser.Rows.Add(newdr);
            }
        }

        DataView dvEk = (dtEkUser.DefaultView);
        dvEk.Sort = "EktronUsername ASC";

        ddlAvailEktronUser.DataSource = dvEk;
        ddlAvailEktronUser.DataValueField = "EktronUserName";
        ddlAvailEktronUser.DataBind();
    }

    private void bindGridView()
    {
        DataTable dtEkUser = new DataTable();

        DataTable dt = FreewayDB.GetAllUserRow();
        ViewState["AllUsers"] = dt;
        // Retrieves the users from Ektron and fill the user dropdown list
        UserAPI userApi = new UserAPI();
        UserData[] users;


        Ektron.Cms.User.IUser userManager = Ektron.Cms.ObjectFactory.GetUser();
        Ektron.Cms.Common.Criteria<Ektron.Cms.User.UserProperty> criteria = new Ektron.Cms.Common.Criteria<Ektron.Cms.User.UserProperty>();

        // to set order by direction.
        criteria.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending;
        criteria.PagingInfo.RecordsPerPage = int.MaxValue;
        criteria.AddFilter(Ektron.Cms.User.UserProperty.IsDeleted, Ektron.Cms.Common.CriteriaFilterOperator.DoesNotContain, 1);
        criteria.AddFilter(Ektron.Cms.User.UserProperty.IsMemberShip, Ektron.Cms.Common.CriteriaFilterOperator.DoesNotContain, 1);

        // If User is Ektron Admin then display all the available user 
        // Else display only self information to the users
        try
        {
            if (userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers, userApi.UserId, false))
            {
                users1 = userManager.GetList(criteria);
                ViewState["EktronUserList"] = users1;
                DataTable dtEk = new DataTable();
                dtEk = ConvertToDatatable((List<UserData>)ViewState["EktronUserList"]);
                FillEktronDropdown(dtEk);
            }
        }
        catch (Exception ex)
        {
            users = new UserData[1];
            users[0] = userApi.UserObject(userApi.UserId);
            users1[0] = userApi.UserObject(userApi.UserId);
        }

        //If User is Ektron Admin then bind all the available user to the grid
        // Else display only self information to the user in grid
        DataTable userRows = new DataTable();
        if (userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers, userApi.UserId, false))
        {
            //userRows = FreewayDB.GetAllUsersFreewayServerWise("demo");
            userRows = FreewayDB.GetAllUserRow();

            if (userRows != null)
            {
                if (userRows.Rows.Count > 0)
                {
                    userRows.Columns.Add("EktronUserName");
                    //foreach (UserData user in users)
                    //{
                    foreach (UserData user in users1)
                    {

                        DataRow[] dr = userRows.Select("EktronUserId = " + user.Id);
                        if (dr.Length >= 1)
                        {
                            dr[0]["EktronUserName"] = (user.Username.Length > 0 ? user.Username : "null");
                        }
                        else
                        {
                            DataRow newdr = userRows.NewRow();
                            newdr["EktronUserId"] = user.Id;
                            newdr["FreewayUsername"] = string.Empty;
                            newdr["FreewayPassword"] = string.Empty;
                            newdr["EktronUserName"] = (user.Username.Length > 0 ? user.Username : "null");
                            newdr["CurrentEnvironment"] = string.Empty;
                            userRows.Rows.Add(newdr);

                        }
                    }

                    DataRow[] objdr = userRows.Select("EktronUserName is null");
                    foreach (DataRow removerow in objdr)
                        userRows.Rows.Remove(removerow);
                }
                else
                {
                    userRows = CreateuserRowsTable(userRows, users1);
                }
                DataTable dtFillGrid = ConvertToDatatable(users1);
                FillGridView(dtFillGrid);
            }
            else
            {
                userRows = CreateuserRowsTable(userRows, users1);
                gridDataBind(userRows);
            }
        }
    }

    private void FillGridView(DataTable users)
    {
        DataTable userRows = new DataTable();
        userRows = FreewayDB.GetAllUserRow();
        System.Data.DataColumn newColumn = new System.Data.DataColumn("EktronUserName", typeof(System.String));
        userRows.Columns.Add(newColumn);

        if (users.Rows.Count > 0)
        {
            for (int i = 0; i < users.Rows.Count; i++)
            {
                if (userRows.Rows.Count > 0)
                {
                    for (int j = 0; j < userRows.Rows.Count; j++)
                    {
                        if (Convert.ToInt64(Convert.ToString(users.Rows[i]["Id"]).Trim()) == long.Parse(Convert.ToString(userRows.Rows[j]["EktronUserId"])))
                        {
                            string ektronUserName = Convert.ToString(users.Rows[i]["Username"]).Trim();
                            userRows.Rows[j]["EktronUserName"] = ektronUserName;
                        }
                    }
                }
            }
        }
        gridDataBind(userRows);
        DataTable dtEk = new DataTable();
        dtEk = ConvertToDatatable((List<UserData>)ViewState["EktronUserList"]);
        RefreshEktronDropdown(dtEk);
    }

    private void RefreshEktronDropdown(DataTable viewstateEktronUsers)
    {
        DataTable dtUserRows = new DataTable();
        DataTable dtnew = new DataTable();
        dtnew.Columns.Add("EkUsers");
        DataTable userRows = new DataTable();
        userRows = FreewayDB.GetAllUserRow();
        userRows.Columns.Add("EktronUserName");
        dtUserRows = viewstateEktronUsers.Copy();
        if (userRows != null)
        {
            for (int i = 0; i <= userRows.Rows.Count - 1; i++)
            {

                if (viewstateEktronUsers.Rows.Count > 0)
                {
                    for (int k = 0; k <= viewstateEktronUsers.Rows.Count - 1; k++)
                    {
                        if (Convert.ToInt64(Convert.ToString(viewstateEktronUsers.Rows[k]["Id"]).Trim()) == Convert.ToInt64(Convert.ToString(userRows.Rows[i]["EktronUserId"]).Trim()))
                        {
                            //DataRow row = viewstateEktronUsers.Rows[k];
                            //viewstateEktronUsers.Rows.Remove(row);
                            viewstateEktronUsers.Rows[k].Delete();
                            k = k - 1;
                        }
                    }
                }
            }
        }



        FillEktronDropdown(viewstateEktronUsers);
    }

    private void FillEktronDropdown(DataTable userRows)
    {
        ViewState["EktronUserData"] = userRows;
        DataView dvEk = (userRows.DefaultView);
        dvEk.Sort = "Username ASC";
        ddlAvailEktronUser.Items.Clear();
        ddlAvailEktronUser.DataSource = dvEk;
        ddlAvailEktronUser.DataValueField = "Username";
        ddlAvailEktronUser.DataBind();
        ddlAvailEktronUser.Items.Insert(0, "--Select--");
    }

    private void gridDataBind(DataTable userRows)
    {
        for (int i = 0; i <= userRows.Rows.Count - 1; i++)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(userRows.Rows[i]["FreewayUsername"])))
            {
                userRows.Rows[i]["FreewayPassword"] = "******";
            }
        }

        //ViewState["GridDataTable"] = userRows;
        DataView dv = (userRows.DefaultView);
        dv.Sort = "EktronUsername ASC";
        gvUserDetail.DataSource = dv;
        gvUserDetail.DataBind();
        ViewState["GridDataTable"] = dv.ToTable();

    }
    private DataTable CreateuserRowsTable(DataTable userRows, List<UserData> users)// UserData[] users)
    {
        try
        {
            userRows = new DataTable();
            userRows.Columns.Add("EktronUserName");
            userRows.Columns.Add("EktronUserId");
            userRows.Columns.Add("FreewayUsername");
            userRows.Columns.Add("FreewayPassword");
            userRows.Columns.Add("SubmitWhere");
            userRows.Columns.Add("UseSsl");
            userRows.Columns.Add("CanSubmit");
            userRows.Columns.Add("ForceGlobalSettings");
            userRows.Columns.Add("CurrentEnvironment");
        }
        catch (Exception ex)
        {

        }
        return userRows;
    }
    private string CheckForValidCredentials(string freewayUser, string freewayPassword, string url)
    {
        LB.FreewayLib.VojoAuthService.FreewayAuth obj = new LB.FreewayLib.VojoAuthService.FreewayAuth();
        string ticket = string.Empty;
        try
        {
            ticket = GetFreewayTicket(freewayUser, freewayPassword, url);
            return ticket;
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    public string GetFreewayTicket(string freewayUser, string freewayPassword, string url)
    {
        string freewayTicket = string.Empty;
        try
        {
            var auth = new LB.FreewayLib.VojoAuthService.FreewayAuth
            {
                Url = url + "/vojo/FreewayAuth.asmx"

            };


            freewayTicket = auth.Logon(freewayUser, freewayPassword);
        }
        catch (Exception ex)
        {
            throw;
        }
        return freewayTicket;
    }
    public static LB.FreewayLib.Freeway.VojoIdentity CreateFreewayIdentity(string freewayUser, string freewayPassword, string submitwhere, bool useSsl)
    {
        return new LB.FreewayLib.Freeway.VojoIdentity(freewayUser,
            freewayPassword, (LB.FreewayLib.Freeway.FreewayServers)Enum.Parse(typeof(LB.FreewayLib.Freeway.FreewayServers),
            submitwhere), FreewayConfiguration.GetProxy(), useSsl);
    }
    private string getEktronUserIdFromEktronUserName(string ektronUserName)
    {
        //List<UserData> lst = (List<UserData>)ViewState["EktronUserData"];
        DataTable dt = ConvertToDatatable((List<UserData>)ViewState["EktronUserList"]);
        //DataTable dt = (DataTable)ViewState["EktronUserList"];
        string ektronUserId = string.Empty;
        try
        {
            //DataTable dt = (DataTable)ViewState["GridDataTable"];
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (ektronUserName.Trim() == Convert.ToString(dt.Rows[i]["Username"]).Trim())
                {
                    ektronUserId = Convert.ToString(dt.Rows[i]["Id"]).Trim();
                    break;
                }
            }
        }
        catch (Exception ex)
        {

        }
        return ektronUserId;
    }
    private void SaveUserRow(long ektronUserId, string freewayUserName, string freewayPassword, string freewayServerUrl, bool isGlobal, string OrigionalFreewayUserName, string OrigionalFreewayPassword)
    {

        //string demo = FreewayConfiguration.FreewayServer;
        FreewayDB.CreateUserRow(ektronUserId, freewayUserName, freewayPassword, freewayServerUrl, isGlobal, OrigionalFreewayUserName, OrigionalFreewayPassword);
    }

    private void SaveUserRowForGlobal(long ektronUserId, string freewayUserName, string freewayPassword, string freewayServerUrl, bool isGlobal)
    {

        //string demo = FreewayConfiguration.FreewayServer;
        FreewayDB.CreateUserRowForGlobal(ektronUserId, freewayUserName, freewayPassword, freewayServerUrl, isGlobal);
    }

    private void RemoveForceGlobalSetting(bool globalSetting)
    {
        FreewayDB.RemoveForceGlobalSetting(globalSetting);
    }

    private void deleteUserRow(long ektronUserId, string freewayUserName, bool isGlobal)
    {
        FreewayDB.DeleteUserRow(ektronUserId, freewayUserName, isGlobal);
    }
    private void AddNewRowToGrid(string ektronUserName)
    {
        try
        {
            if (ViewState["GridDataTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["GridDataTable"];
                DataRow drCurrentRow = null;

                drCurrentRow = dtCurrentTable.NewRow();
                drCurrentRow["EktronUsername"] = ektronUserName;

                dtCurrentTable.Rows.Add(drCurrentRow);
                gridDataBind(dtCurrentTable);
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        foreach (GridViewRow row in gvUserDetail.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow &&
                row.RowState.HasFlag(DataControlRowState.Edit) == false)
            {
                // enable click on row to enter edit mode
                if (gvUserDetail.Enabled)
                {
                    row.Attributes["ondblclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvUserDetail, "Edit$" + row.DataItemIndex, true);
                }
            }
        }
        base.Render(writer);
    }


    protected void gvUserDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gvUserDetail.Enabled)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 0; i <= gvUserDetail.Columns.Count - 1; i++)
                {
                    if (gvUserDetail.Columns[i].HeaderText != string.Empty)
                    {
                        if (e.Row.RowState != (DataControlRowState.Edit))
                        {

                            //e.Row.Attributes["ondblclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvUserDetail, "Edit$" + e.Row.RowIndex);
                            //e.Row.Attributes["style"] = "cursor:pointer";
                            e.Row.Cells[i].ToolTip = "Double-click here to edit.";

                        }
                    }
                }              
            }
        }
    }
    protected void gvUserDetail_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript: return window.confirm('Are you sure you want to delete this item?')", true);
    }
    protected void gvUserDetail_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // GridViewRow row = e.Row;        
        //Label lbl = (Label) row.Cells[2].Controls[0];
        //lbl.Text = string.Empty;
    }




    protected void btnMappedTab_Click(object sender, EventArgs e)
    {
        btnMappedTab.CssClass = "Clicked";
        btnGlobalTab.CssClass = "Initial";
        //btnMappedTab.BackColor = Color.LightGray;
        MainView.ActiveViewIndex = 0;
        checkMode = true;

    }
    protected void btnGlobalTab_Click(object sender, EventArgs e)
    {
        btnMappedTab.CssClass = "Initial";
        btnGlobalTab.CssClass = "Clicked";
        //btnGlobalTab.BackColor = Color.DimGray;
        MainView.ActiveViewIndex = 1;
        checkMode = false;

    }

    
    protected void imgChangeMode_Click(object sender, ImageClickEventArgs e)
    {
        if (lblFlipUserMode.Text.Trim().Contains("Global"))
        {           
                //Flip to mapped user mode
                if (!isGLobalExists)
                {
                    if (validateUserMode())
                    {                        
                        SaveUserRowForGlobal(userApi.UserId, txtUserName.Text.Trim(), txtPwd.Text.Trim(), txtFreeewayUrl.Text.Trim(), true);
                        //string strMessage = "Connector mode successfully set for Global User mode.";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
                       // imgChangeMode.ImageUrl = "~/images/global.jpg";
                        lblFlipUserMode.Text = "Click toggle switch above to switch the connector mode to Mapped Users Mode.";
                        string strMessageSuccess = "Connector mode switch to Global User.";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessageSuccess + "')", true);
                    }
                }          
        }
        else
        {
           
            //Flip to global user mode
            long ektronUserId = userApi.UserId;
            deleteUserRow(ektronUserId, txtUserName.Text.Trim(), true);
            txtUserName.Text = string.Empty;
            txtPwd.Text = string.Empty;
            string strMessage = "Connector mode successfully set for Mapped Users Mode.";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
           // imgChangeMode.ImageUrl = "~/images/mapped.jpg";
            lblFlipUserMode.Text = "Click toggle switch above to switch the connector mode to Global Users Mode.";          
        }
    }
    protected void gvUserDetail_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        
        if ((bool)rdbListItem.Items[0].Selected)
        {
            MainView.ActiveViewIndex = 1;
            //Flip to mapped user mode
            if (!isGLobalExists)
            {
                if (validateUserMode())
                {
                    SaveUserRowForGlobal(userApi.UserId, txtUserName.Text.Trim(), txtPwd.Text.Trim(), txtFreeewayUrl.Text.Trim(), true);
                    //string strMessage = "Connector mode successfully set for Global User mode.";
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
                    // imgChangeMode.ImageUrl = "~/images/global.jpg";
                    rdbListItem.Items[0].Selected = true;
                    lblFlipUserMode.Text = "The connector is currently running in: GLOBAL USER MODE";
                    string strMessageSuccess = "Connector mode switched to Global User Mode.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessageSuccess + "')", true);
                }
            }
            else
            {
                string strMessageSuccess = "The connector is already running in Global User Mode.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessageSuccess + "')", true);
            }
        }
        else
        {
            if (isGLobalExists)
            {
                MainView.ActiveViewIndex = 0;
                //Flip to global user mode
                long ektronUserId = userApi.UserId;
                deleteUserRow(ektronUserId, txtUserName.Text.Trim(), true);
                txtUserName.Text = string.Empty;
                txtPwd.Text = string.Empty;
                string strMessage = "Connector mode switched to Mapped Users Mode.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessage + "')", true);
                // imgChangeMode.ImageUrl = "~/images/mapped.jpg";
                rdbListItem.Items[1].Selected = true;
                lblFlipUserMode.Text = "The connector is currently running in: MAPPED USER MODE";
            }
            else
            {
                string strMessageSuccess = "The connector is already running in Mapped User Mode.";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Click", "javascript:alert('" + strMessageSuccess + "')", true);
            }
           
        }
    }
}
#endregion