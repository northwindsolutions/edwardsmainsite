﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="workarea" CodeFile="freewayworkarea.aspx.vb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title></title>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
        <script type="text/javascript">
            var PerReadOnlyLib = <asp:Literal ID="litPerReadOnlyLib" runat="server"/>;
            var PerContentTreeLang = '<asp:Literal ID="litLanguageId1" runat="server"/>';
            var PerLibraryTreeLang = '<asp:Literal ID="litLanguageId2" runat="server"/>';
            var PerMainPage = '<asp:Literal ID="litMainPage" runat="server"/>';
            var PerWorkareaPrefix = '<asp:Literal ID="litWorkareaPrefix" runat="server"/>';
            
            $ektron("#ek_nav_bottom").resize(function(){
                NavTreeResizing();
            });

            onload = function kickTabSwitch() {
                if (PerMainPage != '') {
                    // kick off timer switch to Content tab
                    setTimeout(switchContentTab, 250);
                }
            }
        </script>

        <script type="text/javascript" src="../../Workarea/java/workareawindow.js"></script>
        <script type="text/javascript">
            if (document.layers) {
                onresize = function reDo() {top.ek_nav_bottom.document.location.href="reloadworkareatree.htm";}
            }
        </script>
    </head>

    <frameset rows="59,*" cols="100%" border="0">
		<frame id="workareatop" name="workareatop" src="workareatop.aspx" scrolling="no" noresize="noresize" frameborder="0" />
		<frameset id="BottomFrameSet" cols="0,*" rows="100%" border="0">
			<frame name="ek_nav_bottom" id="ek_nav_bottom" src="workareanavigationtree.aspx" scrolling="no" frameborder="0" runat="server" />
			<frameset id="BottomRightFrame" rows="*,1" cols="100%" border="0">
				<frame id="ek_main" name="ek_main" src="content.aspx" scrolling="no" frameborder="0" runat="server" />
		</frameset>
	</frameset><noframes></noframes>
</html>