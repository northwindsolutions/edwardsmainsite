#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : This page will be used to perform workarea function
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports Ektron.Cms
Imports Ektron.Cms.UI.CommonUI
#End Region
#Region "workarea class"
Partial Class workarea
    Inherits System.Web.UI.Page

#Region "Object"
    Protected m_refApi As New CommonApi
#End Region

#Region "Page Event"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'register page components
        Me.registerJS()
        Me.registerCSS()

        'set javascript strings
        Me.setJavascriptStrings()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1
        'Put user code to initialize the page here
        Dim m_refMsg As Common.EkMessageHelper
        m_refMsg = m_refApi.EkMsgRef
        Dim strUserName As String = Server.UrlDecode(m_refApi.GetCookieValue("Username"))
        If m_refApi.RequestInformationRef.IsMembershipUser Or m_refApi.RequestInformationRef.UserId = 0 Then
            Response.Write("Please login as a cms user.")
            ek_nav_bottom.Attributes("src") = "blank.htm"
            ek_main.Attributes("src") = "blank.htm"
        Else
            Page.Title = m_refApi.AppName & " " & m_refMsg.GetMessage("workarea page html title") & " " & Request.Cookies("ecm")("username")
            ek_nav_bottom.Attributes("src") = ek_nav_bottom.Attributes("src") & "?" & Request.ServerVariables("query_string")
            If (Not (IsNothing(Request.QueryString("page")))) Then
                If (Request.QueryString("page") <> "") Then
                    litMainPage.Text = Request.QueryString("page") & "?" & Request.ServerVariables("query_string")
                    ek_main.Attributes("src") = litMainPage.Text
                End If
            Else
                If strUserName = "builtin" Then
                    ek_main.Attributes("src") = "blank.htm"
                Else
                    ek_main.Attributes("src") = ek_main.Attributes("src") & "?" & Request.ServerVariables("query_string")
                End If
            End If
        End If
        'litWorkareaPrefix.Text = m_refApi.ApplicationPath
    End Sub
#End Region

#Region "Methods"
    ''' <summary>
    ''' To create concatenated string 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function userstring() As String
        Return "groupid=2&grouptype=0&LangType=" & IIf(m_refApi.ContentLanguage > 0, m_refApi.ContentLanguage, m_refApi.DefaultContentLanguage) & "&id=" & m_refApi.UserId & "&FromUsers="
    End Function

    ''' <summary>
    ''' To set Javascript strings
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setJavascriptStrings()
        Dim AppUI As New ApplicationAPI
        Dim objResult, SiteObj As Object

        SiteObj = AppUI.EkSiteRef
        objResult = SiteObj.GetPermissions(0, 0, "folder")

        litPerReadOnlyLib.Text = LCase(objResult("ReadOnlyLib"))
        litLanguageId1.Text = AppUI.ContentLanguage
        litLanguageId2.Text = AppUI.ContentLanguage
    End Sub
    ''' <summary>
    ''' To register Javascript
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub registerJS()
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronJS)
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronWorkareaJS)
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronThickBoxJS)
    End Sub
    ''' <summary>
    ''' To register CSS 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub registerCSS()
        Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaCss)
        Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaIeCss, API.Css.BrowserTarget.LessThanEqualToIE7)
        Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronThickBoxCss)
    End Sub
#End Region
End Class
#End Region