<%@ Page Language="C#" AutoEventWireup="true" CodeFile="helpMain.aspx.cs" Inherits="Workarea_Freeway_helpMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Freeway CMS Connector help</title>
     <asp:literal id="jsStyleSheet" runat="server"/>
     <link href="../../Workarea/Freeway/freewaypopcalendar.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
            #dvUserGroups p {padding:0em 1em 1em 1em;margin:0;font-weight:bold;color:#1d5987;}
            #dvUserGroups ul.userGroups {list-style:none;margin:0 0 0 1em;padding:0;border:1px solid #d5e7f5;}
            #dvUserGroups ul.userGroups li {display:block;padding:.25em;border-bottom:none;}
            #dvUserGroups ul.userGroups li.stripe {background-color:#e7f0f7;}
            #dvWorkpage td.label {width:auto;}
        </style>
</head>

<body>
    <form id="form1" runat="server">
    <div>
    <table class="ektronGrid">    
    <tr>
    <td>
    <img src="images/helpIcon.JPG"  alt="" />    
    </td>
    </tr>
     <tr>
    <td class="label">
     <b>Following are the Module wise steps  to use the Freeway CMS Connector.</b>
    </td>
    </tr>    
     <tr>
    <td class="label">
    <ul>
    <li>If you are a freeway user then you can have the access of Project creation ,Content selection and Project Dashboard module.
      </li>
      <li>
      If you do not have access to the Project creation ,Content selection and Project Dashboard module ,then visit 
     <a href="helpUsersettings.aspx" > User Settings </a>
     module create your Login credentials for freeway.     
      </li>
      <li>If you have access to all modules then create project using 
     <a href="helpProjectCreation.aspx" >Project Creation</a>
      module</li>
      <li>
      4)Once Project is created successfully it need to send to freeway using  
     <a href="helpContentSelection.aspx" >Content Selection</a>
      module,so that this project will be able to see in 
       <a href="helpProjectDashboard.aspx" >Project Dashboard </a>
        </li>
      <li>Using 
     <a href="helpProjectDashboard.aspx" >Project Dashboard </a>
      module you can chsnge the status of Project and retrieve the files translated.
      </li>
      </ul>
          
    </td>
    </tr>
     <tr>
    <td class="label">
    
    </td>
    </tr>
     <tr>
    <td class="label">
    
    </td>
    </tr>
    
     <tr>
    <td class="label">
     
    </td>
    </tr>
    
     <tr>
    <td class="label">
    
    </td>
    </tr>
    
    </table>
    
    
   
    </div>
    </form>
</body>
</html>
