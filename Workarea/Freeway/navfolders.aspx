<%@ Page Language="vb" AutoEventWireup="false" Inherits="navfolders" CodeFile="navfolders.aspx.vb" %>

<!-- do *NOT* change this doctype or it'll break the workarea accordion layout! -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>navfolders</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
		var m_loaded = false;

		function Startup() {
			setTimeout("m_loaded = true;", 1);
		}

		function IsLoaded() {
			return (m_loaded);
		}

		//--><!]]>
	</script>
	<style type="text/css">
	    html, body {margin: 0; padding: 0;}
	</style>
</head>
<body class="UiNavigation" onload="Startup();" >
    <iframe id="ContentTree" name="ContentTree" frameborder="0" marginheight="0" marginwidth="0"
        width="100%" height="100%" scrolling="auto"></iframe>
    <iframe style="display: none" id="AdminTree" name="AdminTree" src="" frameborder="0"
        marginheight="0" marginwidth="0" width="100%" height="100%" scrolling="no"></iframe>
    <iframe style="display: none" id="LibraryTree" name="LibraryTree" src="" frameborder="0"
        marginheight="0" marginwidth="0" width="100%" height="100%" scrolling="auto"></iframe>
    <iframe style="display: none" id="ReportTree" name="ReportTree" src="" frameborder="0"
        marginheight="0" marginwidth="0" width="100%" height="100%" scrolling="no"></iframe>
</body>
</html>
