#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : 
'---------------------------------
'*/
#End Region
#Region "Imports"
Imports Ektron.Cms
#End Region
#Region "naviconbar Class"

Partial Class navtoolbar
    Inherits System.Web.UI.Page
#Region "Protected Objects"
    Protected m_refAPI As New CommonApi
    Protected m_msgHelper As Ektron.Cms.Common.EkMessageHelper
#End Region
#Region "Page event"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objStyle As StyleHelper
        Try
            RegisterResources()
            If m_refAPI.RequestInformationRef.IsMembershipUser Or m_refAPI.RequestInformationRef.UserId = 0 Then
                Response.Redirect("blank.htm", False)
                Exit Sub
            End If
            objStyle = New StyleHelper
            StyleSheetJS.Text = objStyle.GetClientScript()
            jsAppImgPath.Text = m_refAPI.AppImgPath
            HelpButton.Text = objStyle.GetHelpButton("navtoolbar", "navtoolbar_aspx")
            m_msgHelper = m_refAPI.EkMsgRef
            'Inner code of the Ektron so dont know how to trace it
        Catch ex As Exception

        Finally
            objStyle = Nothing
        End Try
    End Sub
#End Region
#Region "Method"
    Private Sub RegisterResources()
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronToolBarRollJS)
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronStyleHelperJS)
    End Sub
#End Region
End Class
#End Region