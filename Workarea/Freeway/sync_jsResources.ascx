<%@ Control Language="VB" AutoEventWireup="false" CodeFile="sync_jsResources.ascx.vb" Inherits="Sync_jsResources" %>
<script type="text/javascript" id="EktronSyncResourceTextJS">
<!--
    // the following JavaScript defines the resource text values
    // used by the Sync scripts on this page

    // establish Resource Text object for JS string references
    if (Ektron.ResourceText == undefined)
    {
        Ektron.ResourceText = {}
    }

    // define resource text strings as properties
    Ektron.ResourceText.databaseName = "<asp:literal id="jsDatabaseName" runat="server"/>";
    Ektron.ResourceText.deleteProfile = "<asp:literal id="jsDeleteProfile" runat="server" />";
    Ektron.ResourceText.deleteProfileMessage = "<asp:literal id="jsDeleteProfileMessage" runat="server" />";
    Ektron.ResourceText.deleteRelationship = "<asp:literal id="jsDeleteRelationship" runat="server" />";
    Ektron.ResourceText.deleteRelationshipMessage = "<asp:literal id="jsDeleteRelationshipMessage" runat="server" />";
    Ektron.ResourceText.enterCertificate = "<asp:literal id="jsEnterCertificate" runat="server" />";
    Ektron.ResourceText.enterRemoteServer = "<asp:literal id="jsEnterServerName" runat="server"/>";
    Ektron.ResourceText.enterPortNumber = "<asp:literal id="jsEnterPortNumber" runat="server"/>";
    Ektron.ResourceText.exceptionOccurred = "<asp:literal id="jsExceptionOccurred" runat="server"/>";
    Ektron.ResourceText.integratedSecurity = "<asp:literal id="jsIntegratedSecurity" runat="server"/>";
    Ektron.ResourceText.local = "<asp:literal id="jsLocal" runat="server"/>";
    Ektron.ResourceText.moveTo = "<asp:literal id="btnMoveTo" runat="server"/>";
    Ektron.ResourceText.noConfigFound = "<asp:literal id="jsNoConfigFound" runat="server"/>";
    Ektron.ResourceText.noCmsSitesFound = "<asp:literal id="jsNoCmsSitesFound" runat="server" />";
    Ektron.ResourceText.noProfileName = "<asp:literal id="jsNoSyncName" runat="server" />"
    Ektron.ResourceText.orderBy = "<asp:literal id="jsOrderBy" runat="server" />";
    Ektron.ResourceText.password = "<asp:literal id="jsPassword" runat="server"/>";
    Ektron.ResourceText.pickConfig = "<asp:literal id="jspickconfig" runat="server"/>";
    Ektron.ResourceText.portNumberInteger = "<asp:literal id="jsPortNumberInteger" runat="server"/>";
    Ektron.ResourceText.profileValidationError = "<asp:literal id="jsProfileValidationError" runat="server"/>"
    Ektron.ResourceText.pullFrom = "<asp:literal id="btnPullFrom" runat="server"/>";
    Ektron.ResourceText.remote = "<asp:literal id="jsRemote" runat="server"/>";
    Ektron.ResourceText.selectRemoteSite = "<asp:literal id="jsSelectCMSSite" runat="server"/>";
    Ektron.ResourceText.selectSyncProfile = "<asp:literal id="jsSelectSyncProfile" runat="server"/>";
    Ektron.ResourceText.serverName = "<asp:literal id="jsServerName" runat="server"/>";
    Ektron.ResourceText.securityKeyWarning = "<asp:literal id="jsSecurityKeyWarning" runat="server" />";
    Ektron.ResourceText.securityKeyWarningCaption = "<asp:literal id="jsSecurityKeyWarningCaption" runat="server" />";
    Ektron.ResourceText.securityKeyInvalid = "<asp:literal id="jsSecurityKeysInvalid" runat="server" />";
    Ektron.ResourceText.securityKeysLabel = "<asp:literal id="jsSecurityKeysLabel" runat="server" />";
    Ektron.ResourceText.securityKeyMatchInvalid = "<asp:literal id="jsSecurityKeysMatchInvalid" runat="server" />";
    Ektron.ResourceText.syncCanceled = "<asp:Literal id="jsSyncCanceled" runat="server" />";
    Ektron.ResourceText.syncComplete = "<asp:Literal id="jsSyncComplete" runat="server" />";
    Ektron.ResourceText.syncCompleteWithWarning = "<asp:Literal id="synccompletewithwarning" runat="server" />";
    Ektron.ResourceText.syncConfirm = "<asp:Literal id="jsSyncConfirm" runat="server" />";
    Ektron.ResourceText.syncConfirmInProgress = "<asp:Literal id="jsSyncConfirmInProgress" runat="server" />";
    Ektron.ResourceText.syncConfirmSync = "<asp:Literal id="jsSyncConfirmSync" runat="server" />";
    Ektron.ResourceText.syncConfirmSyncCaption = "<asp:Literal id="jsSyncConfirmSyncCaption" runat="server" />";
    Ektron.ResourceText.syncFailed = "<asp:Literal id="jsSyncFailed" runat="server" />";
    Ektron.ResourceText.syncInProgress = "<asp:Literal id="jsSyncInProgress" runat="server" />";
	Ektron.ResourceText.syncRunningConfirm = "<asp:Literal id="jsSyncRunningConfirm" runat="server" />";
    Ektron.ResourceText.syncRunningConfirmHeader = "<asp:Literal id="jsSyncRunningConfirmHeader" runat="server" />";
    Ektron.ResourceText.syncLogOutWarning = "<asp:Literal id="jsSyncLogOutWarning" runat="server" />";
    Ektron.ResourceText.syncRelationActivated = "<asp:literal id="jsSyncRelationActivated" runat="server"/>";
    Ektron.ResourceText.syncStatusErrorMsg = "<asp:literal id="jsSyncStatusErrorMsg" runat="server"/>";
    Ektron.ResourceText.syncStatusHeaderErrorMsg = "<asp:literal id="jsSyncStatusHeaderErrorMsg" runat="server"/>";
    Ektron.ResourceText.username = "<asp:literal id="jsUsername" runat="server"/>";

    Ektron.ResourceText.LocalMultiSite = "<asp:literal id="jsLocalMultiSite" runat="server"/>";
    Ektron.ResourceText.LocalMultiSitePathErrorMsg = "<asp:literal id="jsLocalMultiSitePathErrorMsg" runat="server"/>";
    Ektron.ResourceText.DatabaseTemplMsg = "<asp:literal id="jsDatabaseTemplMsg" runat="server"/>";
    Ektron.ResourceText.forceSyncTurnKeyOff = "<asp:literal id="jsforceSyncTurnKeyOff" runat="server"/>";
    Ektron.ResourceText.forceSyncTurnKeyOffTitle = "<asp:literal id="jsforceSyncTurnKeyOffTitle" runat="server"/>";
    Ektron.ResourceText.syncForceConfirm = "<asp:literal id="jssyncForceConfirm" runat="server"/>";
    Ektron.ResourceText.syncForceConfirmMessage = "<asp:literal id="jssyncForceConfirmMessage" runat="server"/>";
    Ektron.ResourceText.TemplBinErrorMsg = "<asp:literal id="jsTemplBinErrorMsg" runat="server"/>";
    Ektron.ResourceText.ContentSyncHeaderMsg = "<asp:literal id="jsContentSyncHeaderMsg" runat="server"/>";
    Ektron.ResourceText.FolderSyncHeaderMsg = "<asp:literal id="jsFolderSyncHeaderMsg" runat="server"/>";
    Ektron.ResourceText.jserrorgettingstatus = "<asp:literal id="jserrorgettingstatus" runat="server"/>";
    Ektron.ResourceText.jsLastSyncStatus = "<asp:literal id="jsLastSyncStatus" runat="server"/>";
    Ektron.ResourceText.jsLastSyncStatusErrorMsg = "<asp:literal id="jslastsyncstatuserrormsg" runat="server"/>";

    $ektron(document).ready(function()
    {
        Ektron.Sync.sitePath = "<asp:literal id="jsSitePath" runat="server"/>";
        Ektron.Sync.syncPath = "<asp:literal id="jsSyncPath" runat="server"/>";
        Ektron.Sync.isFileSyncAction = <asp:literal id="jsIsFileSyncAction" runat="server"/>;
    });
//-->
</script>
