Imports Ektron.Cms
Imports Ektron.Cms.Site
Imports Ektron.Cms.Common

Partial Class Sync_jsResources
    Inherits System.Web.UI.UserControl

    Protected m_refMsg As Common.EkMessageHelper

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        m_refMsg = (New CommonApi).EkMsgRef

        btnMoveTo.Text = m_refMsg.GetMessage("btn move to")
        btnPullFrom.Text = m_refMsg.GetMessage("btn pull from")

        jsDatabaseName.Text = m_refMsg.GetMessage("js database name")
        jsDeleteProfile.Text = m_refMsg.GetMessage("js delete sync profile")
        jsDeleteProfileMessage.Text = m_refMsg.GetMessage("js delete sync profile message")
        jsDeleteRelationship.Text = m_refMsg.GetMessage("js delete sync relationship")
        jsDeleteRelationshipMessage.Text = m_refMsg.GetMessage("js delete sync relationship message")
		jsEnterCertificate.Text = m_refMsg.GetMessage("js enter certificate")
        jsEnterPortNumber.Text = m_refMsg.GetMessage("js enter port number")
        jsEnterServerName.Text = m_refMsg.GetMessage("js enter server name")

        jsIntegratedSecurity.Text = m_refMsg.GetMessage("js integrated security")
        jsLocal.Text = m_refMsg.GetMessage("js local")
        jsNoConfigFound.Text = m_refMsg.GetMessage("js no sync config found")
        jsNoCmsSitesFound.Text() = m_refMsg.GetMessage("js no sync cms sites found")
        jsNoSyncName.Text() = m_refMsg.GetMessage("jsNoSyncName")
        If (Request.QueryString("orderby") IsNot Nothing) Then
            jsOrderBy.Text = Request.QueryString("orderby").ToLower()
        Else
            jsOrderBy.Text = ""
        End If
        jsPassword.Text = m_refMsg.GetMessage("lbl password")
        jspickconfig.Text = m_refMsg.GetMessage("jspickconfig")
        jsPortNumberInteger.Text = m_refMsg.GetMessage("js port number numeric")
        jsProfileValidationError.Text = m_refMsg.GetMessage("js sync profile validation error")
        jsRemote.Text = m_refMsg.GetMessage("js remote")
        jsSelectCMSSite.Text = m_refMsg.GetMessage("js select cms site")
        jsSelectSyncProfile.Text = m_refMsg.GetMessage("js sync select profile")
        jsServerName.Text = m_refMsg.GetMessage("js server name")
        If (Session("LocalSitePath") IsNot Nothing) AndAlso (Session("LocalSitePath").ToString().Length > 0) Then
            jsSitePath.Text = Session("LocalSitePath")
        Else
            jsSitePath.Text = HttpContext.Current.Request.PhysicalApplicationPath.TrimEnd("\").Replace("\", "\\")
            Session.Remove("LocalSitePath")
            Session.Add("LocalSitePath", jsSitePath.Text)
        End If
        jsSyncFailed.Text = m_refMsg.GetMessage("syncfailed")
        jsSyncCanceled.Text = m_refMsg.GetMessage("jssynccancel")
        jsSyncComplete.Text = m_refMsg.GetMessage("lbl syncended")
        synccompletewithwarning.Text = m_refMsg.GetMessage("lbl syncended with warning")
        jsSyncConfirmInProgress.Text = m_refMsg.GetMessage("lbl sync confirm in progress view status")
        jsSyncConfirm.Text = m_refMsg.GetMessage("lbl sync confirm")
        jsSyncConfirmSync.Text = m_refMsg.GetMessage("lbl sync confirm synchronization")
        jsSyncConfirmSyncCaption.Text = m_refMsg.GetMessage("lbl sync confirm synchronization caption")
	    jsSyncRunningConfirm.Text = m_refMsg.GetMessage("lbl sync running confirm")
        jsSyncRunningConfirmHeader.Text = m_refMsg.GetMessage("lbl sync running confirm header")
        '46206 - The first time messages for workarea and template sync being bidrectional should be removed
        'jsSyncDirectionMessageWorkarea.Text = m_refMsg.GetMessage("lbl first time sync workarea")
        'jsSyncDirectionMessageTemplates.Text = m_refMsg.GetMessage("lbl first time sync templates")
        jsSyncInProgress.Text = m_refMsg.GetMessage("lbl sync in progress")
        jsSyncLogOutWarning.Text = m_refMsg.GetMessage("lbl sync logoutwarning")
        jsSyncRelationActivated.Text = m_refMsg.GetMessage("jssyncrelationactivated")
        jsSyncStatusErrorMsg.Text = m_refMsg.GetMessage("jssyncstatuserrormsg")
        jsSyncStatusHeaderErrorMsg.Text = m_refMsg.GetMessage("jssyncstatusheadererrormsg")
        jsUsername.Text = m_refMsg.GetMessage("generic username")
        jsLocalMultiSite.Text = m_refMsg.GetMessage("jslocalmultisite")
        jsLocalMultiSitePathErrorMsg.Text = m_refMsg.GetMessage("local multisites fail")
        jsDatabaseTemplMsg.Text = m_refMsg.GetMessage("jsdatabasetemplmsg")
        jsforceSyncTurnKeyOff.Text = m_refMsg.GetMessage("jsforcesyncturnkeyoff")
        jsforceSyncTurnKeyOffTitle.Text = m_refMsg.GetMessage("jsforcesyncturnkeyofftitle")
        jssyncForceConfirm.Text = m_refMsg.GetMessage("jssyncforceconfirm")
        jssyncForceConfirmMessage.Text = m_refMsg.GetMessage("jssyncforceconfirmmessage")
        jsTemplBinErrorMsg.Text = m_refMsg.GetMessage("jstemplbinerrormsg")

        jsContentSyncHeaderMsg.Text = m_refMsg.GetMessage("jscontentsyncheadermsg")
        jsFolderSyncHeaderMsg.Text = m_refMsg.GetMessage("lbl select sync configuration")
        jserrorgettingstatus.Text = m_refMsg.GetMessage("jserrorgettingstatus")
        jsLastSyncStatus.Text = m_refMsg.GetMessage("lbl sync status")
        jslastsyncstatuserrormsg.Text = m_refMsg.GetMessage("jslastsyncstatuserrormsg")
        jsIsFileSyncAction.Text = "false"
        If (Request.QueryString("action") IsNot Nothing) Then
            If (Request.QueryString("action") = "syncfiles") Then
                jsIsFileSyncAction.Text = "true"
            End If
        End If

        Dim contentApi As ContentAPI = New ContentAPI()
        jsSyncPath.Text = (contentApi.SitePath & "workarea/sync/")
    End Sub
End Class