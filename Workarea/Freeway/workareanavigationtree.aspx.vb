#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : workarea navigation tree
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports Ektron.Cms
#End Region
#Region "workareanavigationtree class"
Partial Class workareanavigationtree
    Inherits System.Web.UI.Page

#Region "Protected variable"
    Protected m_bAjaxTree As Boolean = False
#End Region
#Region "Page Event"

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init

        'register page components
        Me.registerJS()
        Me.registerCSS()

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1
        Dim m_refApi As New CommonApi
        If (m_refApi.TreeModel = 1) Then
            m_bAjaxTree = True
        End If
        m_refApi = Nothing

        'set javascript strings
        Me.setJavascriptStrings()
    End Sub
#End Region

#Region "Methods"
    ''' <summary>
    ''' Set Javascript Strings
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setJavascriptStrings()

        litszContentNav.Text = Replace(Request.QueryString("ContentNav"), "\", "\\")
        litszFormsNav.Text = Replace(Request.QueryString("FormsNav"), "\", "\\")
        litszLibNav.Text = Replace(Request.QueryString("LibNav"), "\", "\\")
        litszAdminNav.Text = Replace(Request.QueryString("AdminNav"), "\", "\\")
        litszReportNav.Text = Replace(Request.QueryString("ReportNav"), "\", "\\")
        litszVisibleStartTree.Text = Request.QueryString("TreeVisible")

        If m_bAjaxTree = True Then
            litLinkFileName.Text = "WorkAreaTrees.aspx?" & "TreeVisible=" & litszVisibleStartTree.Text & "&tree="
        Else
            litLinkFileName.Text = "workareanavigationtrees.aspx?tree="
        End If

    End Sub

    ''' <summary>
    ''' RegisterJS files
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub registerJS()
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronJS)
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronWorkareaJS)
        Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronThickBoxJS)
    End Sub

    ''' <summary>
    ''' RegisterCSS files
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub registerCSS()
        Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaCss)
        Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaIeCss, API.Css.BrowserTarget.LessThanEqualToIE7)
        Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronThickBoxCss)

    End Sub
#End Region
End Class
#End Region