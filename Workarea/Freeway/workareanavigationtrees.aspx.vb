#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : workarea navigation trees
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports Ektron.Cms
Imports Ektron.Cms.Common
Imports Ektron.Cms.DataIO.LicenseManager
#End Region
#Region "workareanavigationtrees Class"
Partial Class workareanavigationtrees
    Inherits System.Web.UI.Page

#Region "Private Members"
    Private m_hashAllFolders As Collection
    Private m_refAPI As New ContentAPI
    Protected TreeJS As String = ""
    Private PerReadOnlyLib As Boolean = False
    Private IsAdmin As Boolean = False
    Private PerReadOnly As Boolean = False
    Private currentUserID As Long = 1
    Protected m_refContentApi As New ContentAPI
    Private IsSystemAccount As Boolean = False
#End Region

#Region "Page Event"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Response.CacheControl = "no-cache"
            Response.AddHeader("Pragma", "no-cache")
            Response.Expires = -1
            currentUserID = m_refAPI.UserId
            treeJsOutput.Text = getClientScript()
            ' register CSS
            Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaCss)
            Ektron.Cms.API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaIeCss, API.Css.BrowserTarget.AllIE)

            ' register JS
            Ektron.Cms.API.JS.RegisterJS(Me, API.JS.ManagedScript.EktronJS)
            Ektron.Cms.API.JS.RegisterJS(Me, m_refAPI.AppPath & "java/ekfoldercontrol.js", "EktronFolderControlJS")
            'Inner code of the Ektron so dont know how to trace it
        Catch ex As Exception
        Finally
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    End Sub
#End Region

#Region "Methods"
    ''' <summary>
    ''' Add Comment here.
    ''' </summary>
    ''' <param name="level"></param>
    ''' <param name="parent"></param>
    ''' <param name="FolderType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function outputFolders(ByVal level As System.Int32, ByVal parent As System.Int64, ByVal FolderType As Common.EkEnumeration.FolderDestinationType) As System.String
        Dim DestType(0) As Common.EkEnumeration.FolderDestinationType
        Dim Link(0) As String
        Dim DestName(0) As String
        Dim ExtParams(0) As String
        Dim result As System.String = ""
        Dim objContentRef As Ektron.Cms.Content.EkContent
        Try
            objContentRef = m_refAPI.EkContentRef
            If (FolderType = EkEnumeration.FolderTreeType.Content) Then
                DestType(0) = EkEnumeration.FolderDestinationType.Frame 'EkContent.FolderDestinationType.Frame
                Link(0) = "content.aspx?action=ViewContentByCategory&id="
                DestName(0) = "ek_main"
                ExtParams(0) = ""
                result = objContentRef.OutputFolders(level, parent, DestType, Link, DestName, ExtParams, m_hashAllFolders, EkEnumeration.FolderTreeType.Content)

            ElseIf (FolderType = EkEnumeration.FolderTreeType.Library) Then
                DestType(0) = EkEnumeration.FolderDestinationType.Frame
                Link(0) = "library.aspx?action=ViewLibraryByCategory&id="
                DestName(0) = "ek_main"
                ExtParams(0) = ""
                result = objContentRef.OutputFolders(level, parent, DestType, Link, DestName, ExtParams, m_hashAllFolders, EkEnumeration.FolderTreeType.Library)
            End If
            'Inner code of the Ektron so dont know how to trace it
        Catch ex As Exception
            result = ""
        End Try
        Return (result)
    End Function
    ''' <summary>
    ''' Get Client Script
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getClientScript() As System.String
        Dim result As System.Text.StringBuilder
        Dim TreeRequested As String = "content"
        Dim security_data As PermissionData
        Dim objMessage As Common.EkMessageHelper
        Dim objContentRef As Ektron.Cms.Content.EkContent
        Dim m_urlAliasSettings As New UrlAliasing.UrlAliasSettingsApi
        result = New System.Text.StringBuilder

        Try
            objMessage = m_refAPI.EkMsgRef
            security_data = m_refAPI.LoadPermissions(0, "folder")
            If (Not (security_data.IsLoggedIn)) Then
                showLoginError()
                Exit Try
            End If
            If (Not (IsNothing(security_data))) Then
                IsAdmin = security_data.IsAdmin
                PerReadOnly = security_data.IsReadOnly
                PerReadOnlyLib = security_data.IsReadOnlyLib
            End If
            IsSystemAccount = (m_refAPI.UserId = Ektron.Cms.Common.EkConstants.BuiltIn)
            If (Not (IsNothing(Request.QueryString("tree")))) Then
                TreeRequested = Request.QueryString("tree").ToLower.Trim
            End If
            TreeRequested = LCase(Request.QueryString("tree"))
            objContentRef = m_refAPI.EkContentRef
            result.AppendLine("<script type=""text/javascript"">")
            result.AppendLine("<!--")
            Select Case (TreeRequested)

                Case ("content")
                    If (PerReadOnly) Then
                        result.Append("var urlInfoArray = new Array(""frame"", ""content.aspx?action=ViewContentByCategory&id=0"", ""ek_main"");" & vbCrLf)
                        result.Append("TopTreeLevel = CreateFolderInstance(""" & objMessage.GetMessage("generic content title") & """, urlInfoArray);" & vbCrLf)
                    Else
                        result.Append("TopTreeLevel = CreateFolderInstance(""" & objMessage.GetMessage("generic content title") & """, """");" & vbCrLf)
                    End If
                    m_hashAllFolders = objContentRef.GetFolderTreeForUserID(0)
                    result.Append(OutputFolders(0, 0, Common.EkEnumeration.FolderTreeType.Content) & vbCrLf)
                Case ("library")
                    If (PerReadOnlyLib) Then
                        result.Append("var urlInfoArray = new Array(""frame"", ""library.aspx?action=ViewLibraryByCategory&id=0"", ""ek_main"");")
                        result.Append("TopTreeLevel = CreateFolderInstance(""" & objMessage.GetMessage("generic Library title") & """, urlInfoArray);")
                    Else
                        result.Append("TopTreeLevel = CreateFolderInstance(""" & objMessage.GetMessage("generic Library title") & """, """");")
                    End If

                    m_hashAllFolders = objContentRef.GetFolderTreeForUserID(0)
                    result.Append(OutputFolders(0, 0, Common.EkEnumeration.FolderTreeType.Library) & vbCrLf)


                Case ("admin")
                    'Merging Module and Settings under Settings tab for Version 8.0.
                    result.Append("TopTreeLevel = CreateFolderInstance(""" & objMessage.GetMessage("administrate button text") & """, """");")
                    If (Not IsSystemAccount) Then

                        'Commerce
                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.eCommerce) _
                            AndAlso (IsAdmin Or m_refAPI.IsARoleMember(Common.EkEnumeration.CmsRoleIds.CommerceAdmin))) Then

                            result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl commerce") & """, """"));")

                            result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl commerce catalog") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/producttypes.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl product types") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/Coupons/List/List.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl coupons") & """, urlInfoArray));")

                            result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl commerce config") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/locale/country.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa countries") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/currency.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa currency") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/creditcardtypes.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl cc wa") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""subscriptionmessages.aspx?mode=commerce"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl messages") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/paymentgateway.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl payment options") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/locale/region.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa regions") & """, urlInfoArray));")

                            result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl fulfillment") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/fulfillment.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl orders") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/fulfillment/workflow.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl order workflow") & """, urlInfoArray));")

                            result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("generic reports title") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/CustomerReports.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl customer reports") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/KeyPerformanceIndicators.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl key performance indicators") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/PaymentReports.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl payment reports") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/SalesTrend.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl sales trend") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/TopProducts.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl top products") & """, urlInfoArray));")

                            result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl wa shipping") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/shipping/shippingmethods.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa shipping methods") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/shipping/shippingsource.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa warehouses") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/shipping/packagesize.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa packagesize") & """, urlInfoArray));")

                            result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl wa tax") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/tax/taxclass.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa tax classes") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/tax/postaltaxtables.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa postal tax tables") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/tax/taxtables.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa tax tables") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/tax/countrytaxtables.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl wa country tax tables") & """, urlInfoArray));")

                            ' commerce folder links
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/audit/audit.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl audit") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/customers.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl customers") & """, urlInfoArray));")
                        End If
                        'Commerce Ends




                        'Community Management
                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl community management") & """, """"));")

                        ' flagging
                        result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl flagging") & """, """"));")
                        If (IsAdmin) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""contentflagging/flagsets.aspx?communityonly=true"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("wa tree community flag def") & """, urlInfoArray));")
                        End If
                        result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ContentFlags"", ""ek_main"");")
                        result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl flagged content") & """, urlInfoArray));")
                        If (IsAdmin) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""contentflagging/flagsets.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("wa tree flag def") & """, urlInfoArray));")
                        End If


                        ' memberships
                        If (IsAdmin OrElse m_refAPI.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers)) Then
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl memberships") & """, """"));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?backaction=viewallusers&action=viewallusers&grouptype=1&groupid=888888"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic Users") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?action=viewallgroups&grouptype=1"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic User Groups") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?backaction=viewallusers&action=viewallusers&grouptype=1&groupid=888888&ty=nonverify"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic Users not verified") & """, urlInfoArray));")
                        End If
                    End If

                    If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.SocialNetworking)) Then
                        If ((IsAdmin Or m_refAPI.IsARoleMember(Common.EkEnumeration.CmsRoleIds.CommunityAdmin)) And Not IsSystemAccount) Then
                            'Notifications
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl notifications") & """, """"));")
                            result.Append("level3 = InsertFolder(level2, CreateFolderInstance(""" & objMessage.GetMessage("lbl default preferences") & """, """"));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/DefaultNotificationPreferences.aspx?mode=colleagues"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl friends") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/DefaultNotificationPreferences.aspx?mode=groups"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl groups") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/DefaultNotificationPreferences.aspx?mode=privacy"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl privacy") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/ActivityTypes.aspx?mode=viewgrid"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl activity types") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/agents.aspx?mode=viewgrid"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl agent") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/notificationmessages.aspx?mode=viewnotificationmsggrid"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl messages") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""notifications/settings.aspx?mode=view"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("administrate button text") & """, urlInfoArray));")
                        End If
                    End If

                    If (Not IsSystemAccount) Then

                        ' tags
                        result.Append("var urlInfoArray = new Array(""frame"", ""Community/personaltags.aspx"", ""ek_main"");")
                        result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl tags") & """, urlInfoArray));")
                        If (IsAdmin) Then
                            result.Append("var urlInfoArray = new Array(""frame"",""Community/personaltags.aspx?action=viewdefaulttags&objectType=" & CInt(EkEnumeration.CMSObjectTypes.Content) & """, ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl default content tags") & """, urlInfoArray));")
                            If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.SocialNetworking)) Then
                                result.Append("var urlInfoArray = new Array(""frame"", ""Community/personaltags.aspx?action=viewdefaulttags&objectType=" & CInt(EkEnumeration.CMSObjectTypes.CommunityGroup) & """, ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl default group tags") & """, urlInfoArray));")
                            End If


                            result.Append("var urlInfoArray = new Array(""frame"",""Community/personaltags.aspx?action=viewdefaulttags&objectType=" & CInt(EkEnumeration.CMSObjectTypes.Library) & """, ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl default library tags") & """, urlInfoArray));")
                            If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.SocialNetworking)) Then
                                result.Append("var urlInfoArray = new Array(""frame"",""Community/personaltags.aspx?action=viewdefaulttags&objectType=" & CInt(EkEnumeration.CMSObjectTypes.User) & """, ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl default user tags") & """, urlInfoArray));")
                            End If
                        End If

                        ' community groups
                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.SocialNetworking)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""Community/groups.aspx?action=viewallgroups"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl groups") & """, urlInfoArray));")
                        End If

                        If (IsAdmin) Then
                            ' messages
                            result.Append("var urlInfoArray = new Array(""frame"", ""subscriptionmessages.aspx?mode=userprop"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl messages") & """, urlInfoArray));")
                        End If

                        ' reviews
                        result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ContentReviews"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl reviews") & """, urlInfoArray));")

                        ' templates
                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.SocialNetworking)) Then
                            If (IsAdmin) Then
                                result.Append("var urlInfoArray = new Array(""frame"",""Community/communitytemplates.aspx?action=view"", ""ek_main"");")
                                result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl templates") & """, urlInfoArray));")
                            End If
                        End If
                        'Community Management Ends
                    End If

                    'Configuration
                    If (IsAdmin) Then
                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("config page html title") & """, """"));")
                        Dim m_refSiteApi As New SiteAPI
                        Dim settings_data As SettingsData
                        settings_data = m_refSiteApi.GetSiteVariables(m_refSiteApi.UserId)
                        If (Not (IsNothing(settings_data))) Then
                            If (settings_data.IsAdInstalled) Then
                                result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("generic Active Directory") & """, """"));")
                                If m_refSiteApi.RequestInformationRef.ADAdvancedConfig = True Then
                                    result.Append("var urlInfoArray = new Array(""frame"", ""AD/ADDomains.aspx"", ""ek_main"");")
                                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic ad domains") & """, urlInfoArray));")
                                End If
                                result.Append("var urlInfoArray = new Array(""frame"", ""adconfigure.aspx"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic Setup") & """, urlInfoArray));")
                                result.Append("var urlInfoArray = new Array(""frame"", ""adreports.aspx?action=ViewAllReportTypes"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic Status") & """, urlInfoArray));")
                            End If
                        End If
                        If (Not IsSystemAccount) Then

                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl web alert properties") & """, """"));")
                            ' email from list
                            result.Append("var urlInfoArray = new Array(""frame"", ""subscriptionemailfromlist.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl subscription emailfrom properties") & """, urlInfoArray));")
                            ' messages
                            result.Append("var urlInfoArray = new Array(""frame"", ""subscriptionmessages.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl subscription message properties") & """, urlInfoArray));")
                            ' subscriptions
                            result.Append("var urlInfoArray = new Array(""frame"", ""subscriptions.aspx?action=ViewAllSubscriptions"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl subscription properties") & """, urlInfoArray));")

                            'search properties
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("generic search") & """, """"));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""search/suggestedresults.aspx?action=ViewSuggestedResults"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl suggested results") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""search/synonyms.aspx?action=ViewSynonyms"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl synonyms") & """, urlInfoArray));")

                            'forum properties
                            result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl disc boards") & """, """"));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""threadeddisc/replacewords.aspx?isemoticon=1"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl emoticons") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""subscriptionmessages.aspx?mode=forum"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl subscription message properties") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""threadeddisc/replacewords.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl replace words") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""threadeddisc/restrictIP.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl restricted ips") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""threadeddisc/userranks.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl user ranks") & """, urlInfoArray));")

                            'Url Aliasing 7.6
                            'Licensing For 7.6
                            If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Ektron.Cms.DataIO.LicenseManager.Feature.UrlAliasing, False)) Then

                                result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl navtree urlaliasing") & """, """"));")
                                ' automatic
                                result.Append("var urlInfoArray = new Array(""frame"", ""urlmanualaliaslistmaint.aspx?mode=auto"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl tree url automatic aliasing") & """, urlInfoArray));")
                                ' manual
                                result.Append("var urlInfoArray = new Array(""frame"", ""urlmanualaliaslistmaint.aspx"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl tree url manual aliasing") & """, urlInfoArray));")
                                ' regex
                                result.Append("var urlInfoArray = new Array(""frame"", ""urlRegExAliaslistMaint.aspx"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl tree url regex aliasing") & """, urlInfoArray));")
                                ' settings
                                result.Append("var urlInfoArray = new Array(""frame"", ""urlaliassettings.aspx?action=view"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("administrate button text") & """, urlInfoArray));")
                                'End If
                            End If
                            If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Ektron.Cms.DataIO.LicenseManager.Feature.Personalization, False)) Then
                                result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl personalizations") & """, """"));")
                                result.Append("var urlInfoArray = new Array(""frame"", ""widgetsettings.aspx?action=widgetspace"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl widgets space") & """, urlInfoArray));")
                                result.Append("var urlInfoArray = new Array(""frame"", ""widgetsettings.aspx?action=widgetsync"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl widgets") & """, urlInfoArray));")
                            End If

                            If (m_refAPI.EnableReplication) Then
                                result.Append("var urlInfoArray = new Array(""frame"", ""DynReplication.aspx"", ""ek_main"");")
                                result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl navtree quickdeploy") & """, urlInfoArray));")
                            End If
                        End If

                        ' asset server setup
                        result.Append("var urlInfoArray = new Array(""frame"", ""assetconfig.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl asset server type") & """, urlInfoArray));")
                        If (Not IsSystemAccount) Then
                            ' fonts
                            result.Append("var urlInfoArray = new Array(""frame"", ""font.aspx?action=ViewFontsByGroup"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("generic Fonts") & """, urlInfoArray));")
                            'integrated search folder
                            result.Append("var urlInfoArray = new Array(""frame"", ""IntegratedSearch.aspx?action=ViewAllIntegratedFolders"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl Integrated Search Folder") & """, urlInfoArray));")

                            'indexing services
                            result.Append("var urlInfoArray = new Array(""frame"", ""indexingservice.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl Indexing Services") & """, urlInfoArray));")
                            ' language settings
                            result.Append("var urlInfoArray = new Array(""frame"", ""language.aspx?action=Viewlanguage"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl Language Settings") & """, urlInfoArray));")
                            ' metadata definition
                            result.Append("var urlInfoArray = new Array(""frame"", ""meta_data50.aspx?action=ViewAllMetaDefinitions"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("meta_data page html title") & """, urlInfoArray));")
                        End If
                        ' setup
                        result.Append("var urlInfoArray = new Array(""frame"", ""configure.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("generic Setup") & """, urlInfoArray));")
                        If (Not IsSystemAccount) Then
                            'smart form configurations
                            result.Append("var urlInfoArray = new Array(""frame"", ""xml_config.aspx?action=ViewAllXmlConfigurations"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("generic XML Configurations") & """, urlInfoArray));")
                            ' synchronization
                            If (LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.eSync)) Then
                                result.Append("var urlInfoArray = new Array(""frame"", ""sync/Sync.aspx?action=viewallsync"", ""ek_main"");")
                                result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl sync") & """, urlInfoArray));")
                            End If
                            ' task types
                            result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTaskType"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl task types") & """, urlInfoArray));")
                            ' template configuration
                            result.Append("var urlInfoArray = new Array(""frame"", ""template_config.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & "" & objMessage.GetMessage("lbl Template Configuration") & "" & """, urlInfoArray));")
                            'user property
                            result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?action=ViewCustomProp"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl custom user properties") & """, urlInfoArray));")
                        End If
                    Else
                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("config page html title") & """, """"));")
                        If (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin, currentUserID)) Then
                            If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Ektron.Cms.DataIO.LicenseManager.Feature.UrlAliasing, False)) Then
                                result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl navtree urlaliasing") & """, """"));")
                                result.Append("var urlInfoArray = new Array(""frame"", ""urlaliassettings.aspx?action=view"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("administrate button text") & """, urlInfoArray));")
                                'If (m_urlAliasSettings.IsManualAliasEnabled) Then
                                result.Append("var urlInfoArray = new Array(""frame"", ""urlmanualaliaslistmaint.aspx"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl tree url manual aliasing") & """, urlInfoArray));")

                                result.Append("var urlInfoArray = new Array(""frame"", ""urlmanualaliaslistmaint.aspx?mode=auto"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl tree url automatic aliasing") & """, urlInfoArray));")

                                result.Append("var urlInfoArray = new Array(""frame"", ""urlRegExAliaslistMaint.aspx"", ""ek_main"");")
                                result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl tree url regex aliasing") & """, urlInfoArray));")
                                'End If
                            End If
                        End If
                        If (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminMetadata, currentUserID)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""meta_data50.aspx?action=ViewAllMetaDefinitions"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("meta_data page html title") & """, urlInfoArray));")
                        End If


                        If (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminXmlConfig, currentUserID)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""xml_config.aspx?action=ViewAllXmlConfigurations"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("generic XML Configurations") & """, urlInfoArray));")
                        End If
                        If (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.TemplateConfigurations, currentUserID)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""template_config.aspx"", ""ek_main"");")
                            result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl Template Configuration") & """, urlInfoArray));")
                        End If
                        If (LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.eSync)) Then
                            If (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.SyncAdmin, currentUserID)) Then
                                result.Append("var urlInfoArray = new Array(""frame"", ""sync/Sync.aspx?action=viewallsync"", ""ek_main"");")
                                result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl sync") & """, urlInfoArray));")
                            End If
                        End If
                    End If
                    'Configuration Ends.

                    'Roles
                    If (IsAdmin And Not IsSystemAccount) Then
                        ' Add roles:

                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl Roles") & """, """"));")

                        result.Append("level2 = InsertFolder(level1, CreateFolderInstance(""" & objMessage.GetMessage("lbl Built-In") & """, """"));")

                        result.Append("level3 = InsertFolder(level2, CreateFolderInstance(""" & objMessage.GetMessage("lbl System-Wide") & """, """"));")
                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.UrlAliasing)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=aliasedit"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Alias-Edit") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=aliasadmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Alias-Admin") & """, urlInfoArray));")
                        End If

                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.WebSiteAnalytics)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=analyticsviewer"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl role analytics-viewer") & """, urlInfoArray));")
                        End If

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=ruleedit"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Business Rule Editor") & """, urlInfoArray));")

                        Dim EnableClassicCalendar As Boolean
                        Boolean.TryParse(ConfigurationManager.AppSettings.Item("ek_enableClassicCalendar"), EnableClassicCalendar)
                        If EnableClassicCalendar Then
                            'Calendar
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=calendaradmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Calendar-Admin") & """, urlInfoArray));")
                            'Calendar Ends
                        End If

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=collectionmenuadmin"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Collection and Menu Admin") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=collectionapprovers"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Collection Approver") & """, urlInfoArray));")
                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.eCommerce)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=commerceadmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl role commerce-admin") & """, urlInfoArray));")
                        End If
                        If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.SocialNetworking)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=communityadmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl role community-admin") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=communitygroupadmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl role communitygroup-admin") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=communitygroupcreate"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl role communitygroup-create") & """, urlInfoArray));")
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=messageboardadmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Messageboard-Admin") & """, urlInfoArray));")
                        End If
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=metadataadmin"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Metadata-Admin") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=xmlconfigadmin"", ""ek_main"");")

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=multivariatetester"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("alt multivariate tester") & """, urlInfoArray));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=xmlconfigadmin&LangType=" & m_refAPI.RequestInformationRef.ContentLanguage & """, ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Smart Forms Admin") & """, urlInfoArray));")
                        If (LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.eSync)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=syncadmin"", ""ek_main"");")
                            result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl sync admin") & """, urlInfoArray));")
                        End If
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=taskcreate"", ""ek_main"");")

                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Task-Create") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=taskdelete"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Task-Delete") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=taskredirect"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Task-Redirect") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=taxonomyadministrator"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Taxonomy Administrator") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=templateconfig"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Template Configuration") & """, urlInfoArray));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=useradmin"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl User Admin") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=xliffadmin"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl XLIFF admin") & """, urlInfoArray));")
                        'Personalization

                        result.Append("level4 = InsertFolder(level3, CreateFolderInstance(""" & objMessage.GetMessage("lbl personalizations") & """, """"));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=personalizationadmin"", ""ek_main"");")
                        result.Append("InsertFile(level4, CreateLink(""" & objMessage.GetMessage("lbl Admins") & """, urlInfoArray));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=personalizationaddonly"", ""ek_main"");")
                        result.Append("InsertFile(level4, CreateLink(""" & objMessage.GetMessage("alt add web parts") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=personalizationeditonly"", ""ek_main"");")
                        result.Append("InsertFile(level4, CreateLink(""" & objMessage.GetMessage("alt edit web parts properties") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=personalizationmoveonly"", ""ek_main"");")
                        result.Append("InsertFile(level4, CreateLink(""" & objMessage.GetMessage("alt move web parts") & """, urlInfoArray));")

                        result.Append("level3 = InsertFolder(level2, CreateFolderInstance(""" & objMessage.GetMessage("lbl folder specific") & """, """"));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=folderuseradmin"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl Folder User Admin") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=moveorcopy"", ""ek_main"");")
                        result.Append("InsertFile(level3, CreateLink(""" & objMessage.GetMessage("lbl move or copy") & """, urlInfoArray));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""roles.aspx?action=managecustompermissions"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("lbl custom permissions") & """, urlInfoArray));")

                    End If
                    'Roles Ends
                    If (Not IsSystemAccount) Then
                        'Business Rules
                        result.Append("var urlInfoArray = new Array(""frame"", ""businessrules/ruleset.aspx"", ""ek_main"");")
                        result.Append("InsertFile(TopTreeLevel, CreateLink(""" & objMessage.GetMessage("lbl Business Rules") & """, urlInfoArray));")
                        'Business Rules Ends

                        Dim EnableClassicCalendar As Boolean = False
                        Boolean.TryParse(ConfigurationManager.AppSettings.Item("ek_enableClassicCalendar"), EnableClassicCalendar)
                        If EnableClassicCalendar Then
                            'Calendar
                            result.Append("var urlInfoArray = new Array(""frame"", ""cmscalendar.aspx?action=ViewAllCalendars"", ""ek_main"");")
                            result.Append("InsertFile(TopTreeLevel, CreateLink(""" & objMessage.GetMessage("calendar lbl") & """, urlInfoArray));")
                            'Calendar Ends
                        End If

                        'Import XLIFF Files
                        If (m_refAPI.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminXliff) AndAlso Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.Xliff)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""localization.aspx"", ""ek_main"");")
                            result.Append("InsertFile(TopTreeLevel, CreateLink(""" & objMessage.GetMessage("lbl Import XLIFF files") & """, urlInfoArray));")
                        End If
                        'Import XLIFF Files Ends
                    End If

                    'User Groups
                    If (IsAdmin) Then
                        result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?action=viewallgroups&grouptype=0"", ""ek_main"");")
                        result.Append("InsertFile(TopTreeLevel, CreateLink(""" & objMessage.GetMessage("generic User Groups") & """, urlInfoArray));")
                    Else
                        If (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminUsers, currentUserID)) Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?action=viewallgroups&grouptype=0"", ""ek_main"");")
                            result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("generic User Groups") & """, urlInfoArray));")
                        End If
                    End If
                    'User Groups Ends

                    'Users
                    If (IsAdmin Or (objContentRef.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminUsers, currentUserID))) Then
                        result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?backaction=viewallusers&action=viewallusers&LangType=" & m_refAPI.RequestInformationRef.ContentLanguage & "&grouptype=0&groupid=2&id=2&FromUsers=1"", ""ek_main"");")
                        result.Append("InsertFile(TopTreeLevel, CreateLink(""" & objMessage.GetMessage("generic Users") & """, urlInfoArray));")
                    Else
                        result.Append("var urlInfoArray = new Array(""frame"", ""users.aspx?action=View&grouptype=0&LangType=" & m_refAPI.RequestInformationRef.ContentLanguage & "&groupid=2&id=" & currentUserID & """, ""ek_main"");")
                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("generic User Profile") & """, urlInfoArray));")
                    End If
                    'Users Ends


                    ' Start: Module Tab


                    ' End: Module Tab

                    ' Lionbridge start 

                    Dim userApi As UserAPI

                    userApi = New UserAPI

                    ' To perform the Freeway functionality User must have ADminXliff privilage

                    'If (userApi.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminUsers, currentUserID)) And (m_refAPI.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminXliff)) Then

                    If (m_refAPI.IsARoleMember(Common.EkEnumeration.CmsRoleIds.AdminXliff)) Then

                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & "Freeway" & """, """"));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""Freeway/freewaysettings.aspx?usertype=all&mode=view"", ""ek_main"");")

                        result.Append("level2 = InsertFile(level1, CreateLink(""" & "User Settings" & """, urlInfoArray));")



                        result.Append("var urlInfoArray = new Array(""frame"", ""Freeway/freewayprojectcreation.aspx"", ""ek_main"");")

                        result.Append("level2 = InsertFile(level1, CreateLink(""" & "Project Creation" & """, urlInfoArray));")









                        result.Append("var urlInfoArray = new Array(""window"",""Freeway/freewaysendtofreewaywindow.aspx?Lang_value=" & m_refAPI.RequestInformationRef.ContentLanguage & """,""window"");")

                        result.Append("level2 = InsertFile(level1, CreateLink(""" & "Send To Freeway" & """, urlInfoArray));")









                        result.Append("var urlInfoArray = new Array(""frame"", ""Freeway/freewaydashboard.aspx"", ""ek_main"");")

                        result.Append("level2 = InsertFile(level1, CreateLink(""" & "Dashboard" & """, urlInfoArray));")

                    End If





                    ' Lionbridge End




                Case ("report")
                    result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                    result.Append("TopTreeLevel = CreateFolderInstance(""" & objMessage.GetMessage("generic reports title") & """, urlInfoArray);")

                    'Commerce
                    If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.eCommerce) _
                                           AndAlso (IsAdmin Or m_refAPI.IsARoleMember(Common.EkEnumeration.CmsRoleIds.CommerceAdmin))) Then

                        result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                        result.Append("level2 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl commerce") & """, """"));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/CustomerReports.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl customer reports") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/KeyPerformanceIndicators.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl key performance indicators") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/PaymentReports.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl payment reports") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/SalesTrend.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl sales trend") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""Commerce/reporting/TopProducts.aspx"", ""ek_main"");")
                        result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl top products") & """, urlInfoArray));")

                    End If

                    'Add a New folder for Content reports
                    result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                    result.Append("level2 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl web alert contents") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""approval.aspx?action=viewApprovalList"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("generic Approvals") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewCheckedIn"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports checked in title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewCheckedOut"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports checked out title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewNewContent"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports new title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewSubmitted"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports submitted title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewPending"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports pending title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewRefreshReport"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports refresh title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewExpired"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("content reports expired title") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewToExpire&interval=10"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl expire all smrtdesk") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=SiteUpdateActivity"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Site Update Activity Content Report") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewAsynchLogFile"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Asynchronous Log File Report") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewSearchPhraseReport"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Search Phrase Report") & """, urlInfoArray));")

                    If (PerReadOnly) Then
                        Dim m_refSiteApi As New SiteAPI
                        Dim setting_data As New SettingsData
                        setting_data = m_refSiteApi.GetSiteVariables(m_refSiteApi.UserId)
                        If setting_data.EnablePreApproval Then
                            result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewPreapproval"", ""ek_main"");")
                            result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Preapproval Groups") & """, urlInfoArray));")
                        End If
                    End If

                    result.Append("var urlInfoArray = new Array(""frame"", ""BadLinkCheck.aspx"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Bad Link Report") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ContentFlags"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl flag report") & """, urlInfoArray));")

                    result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ContentReviews"", ""ek_main"");")
                    result.Append("InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl review report") & """, urlInfoArray));")

                    'Site Analytics 
                    Dim dataManager As Ektron.Cms.Analytics.IAnalytics = ObjectFactory.GetAnalytics()
                    If (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(m_refAPI.RequestInformationRef, Feature.WebSiteAnalytics) AndAlso dataManager.IsAnalyticsViewer()) Then
                        result.AppendLine("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                        result.AppendLine("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & m_refAPI.EkMsgRef.GetMessage("lbl site analytics") & """, urlInfoArray));")

                        appendTreeItems(result, SiteAnalyticsContainer.TreeContainer, 1)


                        result.Append("var urlInfoArray = new Array(""frame"", ""Analytics/reporting/reports.aspx?report=CmsSearchTerms"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & objMessage.GetMessage("analytics searches") & """, urlInfoArray));")

                    End If
                    'Site Analytics Ends

                    'Pre Version 8 Analytics
                    Dim EnableClassicAnalytics As Boolean
                    Boolean.TryParse(ConfigurationManager.AppSettings.Item("ek_enableClassicAnalytics"), EnableClassicAnalytics)
                    If EnableClassicAnalytics Then
                        result.Append("var urlInfoArray = new Array(""frame"", ""ContentAnalytics.aspx"", ""ek_main"");")
                        result.Append("level1 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & m_refAPI.EkMsgRef.GetMessage("lbl pre8 site analytics") & """, urlInfoArray));")

                        result.Append("var urlInfoArray = new Array(""frame"", """", ""ek_main"");")
                        result.Append("level2 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl commerce") & """, """"));")

                        result.Append("var urlInfoArray = new Array(""frame"", ""ContentAnalytics.aspx?type=content"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & m_refAPI.EkMsgRef.GetMessage("top content") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""ContentAnalytics.aspx?type=page"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & m_refAPI.EkMsgRef.GetMessage("lbl top templates") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""ContentAnalytics.aspx?type=referring"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & m_refAPI.EkMsgRef.GetMessage("top referrers") & """, urlInfoArray));")
                        result.Append("var urlInfoArray = new Array(""frame"", ""reports.aspx?action=ViewSearchPhraseReport"", ""ek_main"");")
                        result.Append("InsertFile(level1, CreateLink(""" & m_refAPI.EkMsgRef.GetMessage("lbl Search Phrase Report") & """, urlInfoArray));")
                    End If
                    'Pre Version 8 Analytics Ends

                    'new tasks folder
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=both"", ""ek_main"");")
                    result.Append("level2 = InsertFolder(TopTreeLevel, CreateFolderInstance(""" & objMessage.GetMessage("lbl Tasks") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=all"", ""ek_main"");")
                    If (IsAdmin) Then
                        result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl All Open Tasks") & """, urlInfoArray));")
                    End If
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=to"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Assigned to me") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=by"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Assigned by me") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=created"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Created by me") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=touser"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Assigned to User") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=notstarted"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Not Started") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=active"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("Active label") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=awaitingdata"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Awaiting Data") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=onhold"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl On hold") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=pending"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Pending") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=reopened"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Reopened") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=completed"", ""ek_main"");")
                    result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Completed") & """, urlInfoArray));")
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=archived"", ""ek_main"");")
                    If (IsAdmin) Then
                        result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Archived") & """, urlInfoArray));")
                    End If
                    result.Append("var urlInfoArray = new Array(""frame"", ""tasks.aspx?action=ViewTasks&ty=deleted"", ""ek_main"");")
                    If (IsAdmin) Then
                        result.Append("level3 = InsertFile(level2, CreateLink(""" & objMessage.GetMessage("lbl Deleted") & """, urlInfoArray));")
                    End If



            End Select
            If (TreeRequested <> "smartdesktop" AndAlso TreeRequested <> "help") Then
                result.Append("if (IsValid())" & vbCrLf)
                result.Append("InitializeFolderControl();" & vbCrLf)
            End If

            result.AppendLine("//-->")
            result.AppendLine("</script>")
            result.Append("<script type=""text/javascript"">" & vbCrLf)
            result.Append("<!--" & vbCrLf)
            result.Append("function OpenWorkareaFolder() {" & vbCrLf)
            If (Request.QueryString("autonav") <> "") Then
                result.Append("OpenFolder(""" & Replace(Request.QueryString("autonav"), "\", "\\") & """, false);" & vbCrLf)
            End If
            result.Append("}" & vbCrLf)
            result.Append("OpenWorkareaFolder();" & vbCrLf)
            result.Append("//--></script>" & vbCrLf)
            'Inner code of the Ektron so dont know how to trace it
        Catch ex As Exception
            result.Length = 0
            result.Append(ex.ToString)
        Finally
        End Try
        Return (result.ToString)
    End Function
    ''' <summary>
    ''' To Append Tree Items
    ''' </summary>
    ''' <param name="result"></param>
    ''' <param name="ctlItem"></param>
    ''' <param name="level"></param>
    ''' <remarks></remarks>
    Private Sub appendTreeItems(ByVal result As StringBuilder, ByVal ctlItem As HtmlGenericControl, ByVal level As Integer)
        Try
            For Each ctlChild As Control In ctlItem.Controls
                If TypeOf ctlChild Is LiteralControl Then
                    Dim litCaption As LiteralControl = CType(ctlChild, LiteralControl)
                    Dim strCaption As String = Trim(litCaption.Text.Replace(vbCr, "").Replace(vbLf, "").Replace(vbTab, ""))
                    If Not String.IsNullOrEmpty(strCaption) Then
                        strCaption = m_refAPI.EkMsgRef.GetMessage(strCaption)
                        strCaption = Ektron.Cms.API.JS.Escape(strCaption)
                        If ctlItem.Controls.Count > 1 Then
                            result.AppendLine("level" & (level + 1) & " = InsertFolder(level" & level & ", CreateFolderInstance(""" & strCaption & """, []));")
                            level += 1
                        Else
                            Dim strHref As String = ctlItem.Attributes.Item("href")
                            strHref = Ektron.Cms.API.JS.Escape(strHref)
                            result.AppendLine("InsertFile(level" & level & ", CreateLink(""" & strCaption & """, [""frame"", """ & strHref & """, ""ek_main""]));")
                        End If
                    End If
                End If
                If TypeOf ctlChild Is HtmlGenericControl Then
                    appendTreeItems(result, ctlChild, level)
                End If
            Next
            'Inner code of the Ektron so dont know how to trace it
        Catch ex As Exception

        End Try
    End Sub
    ''' <summary>
    ''' To Show Login Error
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub showLoginError()
        Response.Write("<table><tr><td class=""exception"" ><b>Login Error: Process terminated.</b>  <br>User may not be logged in or logged out from another system.  To continue this process close this workarea and log-in back.</td></tr></table>")
    End Sub
#End Region

End Class
#End Region