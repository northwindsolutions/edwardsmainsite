<%@ Page Language="vb" AutoEventWireup="false" Inherits="workareatop" CodeFile="workareatop.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        var selectSettingsTab = <asp:Literal id="SelectSettingsTab" runat="server" text="false"/>;
        
        function Initialize()
        {
            self.focus();
            if (selectSettingsTab) {
                top.switchSettingsTab();
                
            }
            ChangePage("http://localhost/EktronTech/WorkArea/Freeway/workareatop.aspx#", "ContentTree");
        }

		function ChangePage(tab, pageName)
		{
		
			if (typeof(top.CanNavigate) == "function" && !top.CanNavigate())
			{
				return;
			}

			// Load the appropriate page on the right side
		    top.SelectMainWindow(pageName);

            // Update the tree on the left side
			top.MakeNavTreeVisible(pageName);

			$ektron("#tabs .selected").removeClass("selected");
			$ektron(tab).addClass("selected");
		}
    </script>
</head>
<body onload="Initialize();">
    <div class="ektronWorkAreaHeader" >
        <h1 class="logo" title="Ektron CMS400.NET" >Ektron CMS400.NET</h1>
        <div class="dvVersion" >
            <label class="version" >
                <asp:Literal runat="Server" ID="ltrVersion" />
            </label>
        </div>
        <div class="userInfo" visible = "false">
            <asp:Label ID="lblUser" CssClass="userName" runat="server" visible = "false"/>
            
            <asp:Label ID="userUnreadMessages" CssClass="userMessages" runat="server" visible = "false"/>
        </div>
        <div visible = "false">
            <div id="tabs" visible = "false">
                <ul class="ui-tabs-nav ui-helper-reset" visible = "false">
                    <li class="left" visible = "false" >&nbsp;</li>
                    <li id="Desktop" visible = "false">
                        <a href="#" id="DesktopLink"
                            onclick="ChangePage(this, 'SmartDesktopTree')" runat="server" visible = "false">Desktop</a>
                    </li>
                    <li class="divider" visible = "false"></li>
                    <li id="Content" visible = "false">
                        <a href="#" id="ContentLink" onclick="ChangePage(this, 'ContentTree')" runat="server" visible = "false">Content</a>
                    </li>
                    <li class="divider" visible = "false"></li>
                    <li id="Library" visible = "false">
                        <a href="#" id="LibraryLink" onclick="ChangePage(this, 'LibraryTree')" runat="server" visible = "false">Library</a>
                    </li>
                    <li class="divider" visible = "false"></li>
                    <li id="Settings" visible = "false">
                        <a href="#" id="SettingsLink" onclick="ChangePage(this, 'AdminTree')" runat="server" visible = "false">Settings</a>
                    </li>
                    <li class="divider" visible = "false"></li>
                    <li id="Reports" visible = "false">
                        <a href="#" id="ReportsLink" onclick="ChangePage(this, 'ReportTree')" runat="server" visible = "false">Reports</a>
                    </li>
                    <li class="divider" visible = "false"></li>
                    <li id="Help" visible = "false">
                        <a href="#" id="HelpLink" onclick="ChangePage(this, 'Help')" runat="server" visible = "false">Help</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
