#Region "Header Comment"
'/*
'---------------------------------
'Created By :    Adwait Churi
'Created Date :  24 May 2010
'Details : workarea top
'---------------------------------
'*/
#End Region

#Region "Imports"
Imports Ektron.Cms
Imports Ektron.Cms.API
Imports Ektron.Cms.Common
Imports Ektron.Cms.ContentAPI
#End Region

#Region "workareatop Class"
Partial Class workareatop
    Inherits System.Web.UI.Page
#Region "Protected Objects"
    Protected m_refMsg As EkMessageHelper
    Protected m_refApi As New UserAPI
    Protected contentAPI As New ContentAPI()
    Protected commonAPI As New CommonApi()
#End Region
#Region "Page Event"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        registerCSS()
        registerJS()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.Expires = -1
        m_refMsg = m_refApi.EkMsgRef
        ltrVersion.Text = m_refMsg.GetMessage("version") & "&nbsp;" & contentAPI.Version & "&nbsp;" & contentAPI.ServicePack & "<i>(" & m_refMsg.GetMessage("build") & "&nbsp;" & contentAPI.BuildNumber & ")</i>"
        Try
            Dim culture As New System.Globalization.CultureInfo(m_refApi.RequestInformationRef.UserCulture)
        Catch ex As Exception
            Dim culture As New System.Globalization.CultureInfo("en-us")
        End Try

        DesktopLink.InnerText = m_refMsg.GetMessage("lbl desktop")
        ContentLink.InnerText = m_refMsg.GetMessage("lbl content")
        LibraryLink.InnerText = m_refMsg.GetMessage("generic library title")
        SettingsLink.InnerText = m_refMsg.GetMessage("administrate button text")
        ReportsLink.InnerText = m_refMsg.GetMessage("lbl wa mkt reports")
        HelpLink.InnerText = m_refMsg.GetMessage("generic help")

        Dim strUserName As String = ""
        If (m_refApi.UserId > 0) Then
            strUserName = Server.UrlDecode(m_refApi.GetCookieValue("Username"))
            If strUserName = "builtin" Then
                ContentLink.Visible = False
                LibraryLink.Visible = False
                DesktopLink.Visible = False
                ReportsLink.Visible = False
                SelectSettingsTab.Text = "true"
            End If
            ' trim to max length and add elipsis if needed:
            If (Len(strUserName) > 20) Then
                strUserName = Left(strUserName, 20) & "..."
            End If
        End If

        lblUser.Text = String.Format("{0} {1}", m_refMsg.GetMessage("user"), strUserName)
        ' notify user of any messages
        Dim iUnread As Integer = contentAPI.GetUnreadMessageCountForUser(commonAPI.UserId)
        Dim unreadAnchor As String = "<a href=""" & m_refApi.AppPath.ToString() & "CommunityMessaging.aspx?action=viewall"" target=""ek_main"" onclick=""ChangePage(this, 'UserMessages'); return false;"">{0} {1}</a>"
        If (iUnread = 1) Then
            userUnreadMessages.Text = String.Format(m_refMsg.GetMessage("lbl user you have messages"), String.Format(unreadAnchor, iUnread, m_refMsg.GetMessage("lbl user message")))
        Else
            userUnreadMessages.Text = String.Format(m_refMsg.GetMessage("lbl user you have messages"), String.Format(unreadAnchor, iUnread, m_refMsg.GetMessage("lbl user messages")))
        End If


        If (Request.QueryString("tab") = "content") Then
            ContentLink.Attributes("class") = "selected"
        Else
            If strUserName = "builtin" Then
                SettingsLink.Attributes("class") = "selected"
            Else
                DesktopLink.Attributes("class") = "selected"
            End If
        End If

    End Sub

#End Region
#Region "CSS/JS"

    Private Sub registerCSS()
        API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronWorkareaCss)
        API.Css.RegisterCss(Me, API.Css.ManagedStyleSheet.EktronUITabsCss)
    End Sub

    Private Sub registerJS()
        JS.RegisterJS(Me, JS.ManagedScript.EktronJS)
    End Sub

#End Region

End Class

#End Region