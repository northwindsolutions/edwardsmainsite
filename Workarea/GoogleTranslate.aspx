﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GoogleTranslate.aspx.cs" Inherits="Workarea_GoogleTranslate" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title id="pageTitle" runat="server"></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <meta name="google-translate-customization" content="1d2b02835011bcee-c4d93ad00a7fba5b-g871f12675c9ef857-20"/>
        <script type="text/javascript" src="ewebeditpro/eweputil.js"></script>
        <script type="text/javascript" src="java/toolbar_roll.js"></script>
        <script type="text/javascript"  src="ContentDesigner/RadWindow.js" ></script>
        <asp:Literal ID="stylesheetjs" runat="server" ></asp:Literal> 
        <script src="https://www.google.com/jsapi" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="FrameworkUI/js/jQuery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="java/plugins/blockui/ektron.blockui.js"></script>
    <style type="text/css">
        .ektronErrorText {
            display: none;
        }
    </style>
</head>
<body style="background-color:InfoBackground;">
 
    <script language="javascript" type="text/javascript">
        google.load("language", "1");
        $ektron(window).bind("load", function () {
            if ($ektron('#selSourceLanguage option').size() == 0) {
                loadLanguages();
                if ($ektron("#divBranding").innerHTML == "") {
                    google.language.getBranding('divBranding');
                }
            }
            $ektron('#btnDetect').click(function (e) {
                e.preventDefault();
                $ektron.blockUI({ css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
                });
                $ektron.unblockUI();
            });

            $ektron('#btnTranslate').click(function (e) {
                e.preventDefault();
                var text = $ektron.trim($ektron('#txtOrgText').val());
                $ektron('#divTranslated').html("");
                if (text.length > 0) {
                    var langSource = $ektron('#selSourceLanguage').val();
                    var langTarget = $ektron('#selTargetLanguage').val();

                    if (langSource === "" || langTarget === "") {
                        alert('<%=jsManualSelLangAlert%>');
                    }
                    else if (langSource === langTarget) {
                        alert('<%=jsManualSTlangAlert %>');
                    }
                    else {
                        $ektron.blockUI({ css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                        });

                        text = text.replace(/&#160;/g, '');
                        var indx = 0;
                        var processtextArr = text.split('</p>');

                        var interstr = '';
                        for (var i = 0; i < processtextArr.length; i++) {
                            if (processtextArr[i] == '')
                                continue;
                            interstr = processtextArr[i] + '</p>';
                            indx = 0;
                            while (indx < interstr.length) {
                                processtext = interstr.substring(indx, indx + 999);

                                var apiurl = '<%=_apiUrl%>' + '<%=_apikey%>' + "&source=" + langSource + "&target=" + langTarget + "&q=";
                                $ektron.ajax({
                                    url: apiurl + encodeURIComponent(processtext),
                                    dataType: 'jsonp',
                                    async: false,
                                    cache: false,
                                    success: function (data) {
                                        try {
                                            $ektron('#divTranslated').append(data.data.translations[0].translatedText);
                                        }
                                        catch (e) {
                                            $ektron('#divTranslated').html(processtext);
                                        }
                                        $ektron('#divTranslated').css({ "border": "1px solid #7F9DB9" });
                                        $ektron('#divTranslated').css({ "padding": "4 4 4 4" });

                                        $ektron('#lblTranslation').css({ "color": "black" });
                                        $ektron('#btn').removeAttr('disabled');
                                        $ektron.unblockUI();
                                    },
                                    error: function (x, e) {
                                        alert('<%=jsManualErrTranslangAlert %>');
                                        $ektron('#btn').attr('disabled', 'disabled');
                                        $ektron.unblockUI();
                                    }
                                });
                                indx = indx + 1000;
                            }

                        }
                    }
                }
                else {
                    alert('<%=jsManualNoTextAlert %>');
                    $ektron.unblockUI();
                }
            });
            $ektron.unblockUI();
        });

        function loadLanguages() {
            var apiurl = "https://www.googleapis.com/language/translate/v2/languages?key=" + '<%=_apikey%>' + "&target=en";
            $ektron.ajax({
                url: apiurl,
                dataType: 'jsonp',
                success: function (data) {
                    if (typeof (data.data) != "undefined") {
                        var languages = data.data.languages;
                        $ektron.each(languages, function (index, language) {
                            $ektron('#selSourceLanguage').append('<option value="' + language.language + '">' + language.name + '</option>');
                            $ektron('#selTargetLanguage').append('<option value="' + language.language + '">' + language.name + '</option>');
                        });
                        $ektron("select#selSourceLanguage").val('en');
                        $ektron("select#selTargetLanguage").val('en');
                    }
                    else {
                        $ektron('.ektronErrorText').show();
                        alert('<%=jsManualInvalidApiorurlAlert %>');
                        $ektron('#btnTranslate').attr('disabled', 'disabled');
                    }
                },
                error: function (x, e) {
                    alert('<%=jsManualloadgooglelangAlert %>');
                }
            });

        }
          
    </script>

     <script type="text/javascript">
        <!--         //--><![CDATA[//><!--
         InitializeRadWindow();
         var editorcontent;
         var isContentDesigner = false;
         function loadcontent() {
             editorcontent = "";
             try {
                 var args = GetDialogArguments();
                 if (args) {
                     if (typeof (args.content) != "undefined") {
                         editorcontent = args.content;
                         isContentDesigner = true;
                     }
                 }
             }
             catch (e) {
                 isContentDesigner = false;
             }

             if (Ektron.Namespace.Exists("window.parent.Ektron.Translate.WorldLingo.GetContent")) {
                 editorcontent = window.parent.Ektron.Translate.WorldLingo.GetContent();
                 isContentDesigner = false;
             }

             if (null == document.getElementById("txtOrgText")) return;

             if (isContentDesigner || Ektron.Namespace.Exists("window.parent.Ektron.Translate.WorldLingo.GetContent")) {
                 if (editorcontent.length > 0) {
                     document.getElementById("txtOrgText").value = editorcontent;
                     document.getElementById("origContent").innerHTML = editorcontent;
                 }
                 else {
                     alert('<%=jsManualNoContentTransAlert %>');
                 }
             }
             else if (top.opener && top.opener.eWebEditPro) {
                 var objInstance = top.opener.eWebEditPro.instances["<%= htmleditor%>"];
                 if (objInstance) {
                     var objElem = objInstance.linkedElement();
                     if (objElem) {
                         editorcontent = objElem.value;
                         if (editorcontent.length > 0) {
                             document.mylanguage.txtOrgText.innerHTML = editorcontent;
                             document.writeln(editorcontent);
                         }
                         else {
                             alert('<%=jsManualNoContentTransAlert %>');
                         }
                     }
                     else {
                         alert('<%=jsManualUnableleAlert %>');
                     }
                 }
                 else {
                     document.writeln('<%=lblNotFindEditor %>' + '<%= htmleditor%>.');
                 }
             }
             else {

                 document.writeln('<%=lblWebpageEditor %>');

             }

         }
         function pasteContent() {
             if (Ektron.Namespace.Exists("window.parent.Ektron.Translate.WorldLingo.AcceptInsert")) {
                 window.parent.Ektron.Translate.WorldLingo.AcceptInsert({ "content": $ektron('#divTranslated').html(), "contentlanguage": document.getElementById("selTargetLanguage").value });
             }
             else if (isContentDesigner) {
                 var retValue = $ektron('#divTranslated').html();
                 retValue = retValue.replace(/<br>/g, '<br/>');
                 CloseDlg(retValue);
             }
             else if (top.opener && top.opener.eWebEditPro) {
                 var objInstance = top.opener.eWebEditPro.instances["<%= htmleditor %>"];
                 if (objInstance) {
                     var translatedContent = $ektron('#divTranslated').html();
                     translatedContent = translatedContent.replace(/<br>/g, '<br/>');
                     objInstance.load(translatedContent);
                     top.close();
                 }
                 else {
                     alert('<%=lblNotFindEditor %>' + '<%= htmleditor%>.');
                 }
             }
             else {
                 alert('<%=lblWebpageEditor %>');
             }
         }

         function validate() {
             if (document.getElementById("wl_srclang").value == document.getElementById("wl_trglang").value) {
                 alert('<%=jsManualSTlangAlert %>');
                 return false;
             }
             document.getElementById("btnTranslate").value = '<%=lblLoadingmsg %>'
             return true;
         }

         Ektron.ready(function (event, eventName) {
             loadcontent();
             $ektron("table.ektronGrid").show();
         });
         //--><!]]>
    </script>


    <div class="ektronPageHeader" style="padding-top: 0px;">
        <table width="100%" class="translation-toolbar">
            <tr class="ektronTitlebar">
                <td>
                    <asp:Label ID="TransTitle" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ektronToolbar" id="tblButton" runat="server">
                </td>
            </tr>
        </table>
    </div>

    <form id="form1" runat="server" method="get">
          
      <div class="ektronPageContainer ektronPageInfo" id="formpage" runat="server">
                <ektronUI:Message ID="lblErrorMessage" DisplayMode="Error" runat="server" CssClass="ektronErrorText"></ektronUI:Message>
                <table width="100%" class="ektronGrid" border="1">
                    <asp:Panel runat="server" ID="PanelActionLangSelector">
                    <tr>
                        <td class="label">
                             <asp:label ID="lblsrc" runat="server">Source Language : </asp:label>
                        </td>
                        <td colspan="3">
                            <select id="selSourceLanguage" name="selSourceLanguage" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                             <asp:label ID="lblTar" runat="server">Target Language : </asp:label>
                        </td>
                        <td colspan="3"> 
                            <select id="selTargetLanguage" name="selTargetLanguage" >
                            </select>
                        </td>
                    </tr>
                        </asp:Panel>
                    <tr>
                        <td class="label">
                            <asp:label ID="lblOriginal" runat="server">Original Text : </asp:label>
                        </td>
                        <td class="style2" colspan="3">
                            <div id="origContent" runat="server">
                            </div>
                            <textarea title="Enter Origional Content Text here" style="display: none;" id="txtOrgText" runat="server"></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="style1">
                        </td>
                        <td colspan="3">
                            <div id="divDetectedLanguage" style="font-size: 8pt; color: blue;">
                            </div>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="PanelActionButtons">

                    <tr>
                        <td>&nbsp;</td>
                        <td class="style1" colspan="3" align="center">
                                <input title="Paste Content" id="btn" name="btn" type="button" value="Paste Content" style="width: 110px;" disabled="disabled"  onclick="pasteContent()" />
                                <input id="btnTranslate" type="button" value="Translate" name="btnTranslate" style="width: 94px;" />
                        </td>
                    </tr>
                    </asp:Panel>
                    <tr>
                        <td id="lblTranslation" style="color: white; padding-top: 4px;" valign="top" class="style1">
                            Translated Text:&nbsp;
                        </td>
                        <td colspan="3">
                            <div id="Div2"></div>
                            <script type="text/javascript">
                                function googleTranslateElementInit() {
                                    new google.translate.TranslateElement({ pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE }, 'google_translate_element');
                                }
                            </script>
                            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" colspan="4">
                            <div id="divTranslated" style="font-size: 12pt">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    
   
    </form>
</body>
</html>
