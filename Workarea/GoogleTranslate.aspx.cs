﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.UI.CommonUI;
using Microsoft.VisualBasic;
using System.Configuration;
using System.Configuration.Provider;

public partial class Workarea_GoogleTranslate : Ektron.Cms.Workarea.Page
{
    protected string htmleditor = "";
    protected string htmcontent = "";
    protected StyleHelper m_refStyle = new StyleHelper();
    protected string TargetLanguage;
    protected string SourceLanguage;
    protected const int ALL_CONTENT_LANGUAGES = -1;
    protected string _apikey = "";
    protected string _apiUrl = "";
    protected string jsManualSelLangAlert = "";
    protected string jsManualSTlangAlert = "";
    protected string jsManualErrTranslangAlert = "";
    protected string jsManualNoTextAlert = "";
    protected string jsManualInvalidApiorurlAlert = "";
    protected string jsManualloadgooglelangAlert = "";
    protected string jsManualNoContentTransAlert = "";
    protected string jsManualUnableleAlert = "";
    protected string lblNotFindEditor = "";
    protected string lblWebpageEditor = "";
    protected string lblLoadingmsg = "";
    public ApplicationAPI AppUI = new ApplicationAPI();
    public Ektron.Cms.Common.EkMessageHelper MsgHelper;

    public Workarea_GoogleTranslate()
    {
        MsgHelper = AppUI.EkMsgRef;
    }
    private void Page_Init(System.Object sender, System.EventArgs e)
    {
        string TranslationKeyValue="";
        string [] TranslationKeyArray;
        System.Collections.Specialized.NameValueCollection _translateNameValue = new System.Collections.Specialized.NameValueCollection();
        _translateNameValue = (System.Collections.Specialized.NameValueCollection)ConfigurationManager.GetSection("machineTranslation");
        if (_translateNameValue != null && _translateNameValue["GoogleTranslator"] != null)
        {
            TranslationKeyValue = _translateNameValue["GoogleTranslator"].ToString();
            TranslationKeyArray = TranslationKeyValue.Split(',');
            if (TranslationKeyArray.Length == 2)
            {
                _apikey = TranslationKeyArray[0];
                _apiUrl = TranslationKeyArray[1];
            }
        }
        else
        {
            PanelActionButtons.Visible = false;
            PanelActionLangSelector.Visible = false;
            lblsrc.Visible = false;
            lblTar.Visible = false;
            
        }
        lblErrorMessage.Text = MsgHelper.GetMessage("lbl google translation is not configured");
        SetJSLabel();
    }
    protected void SetJSLabel()
    {
        jsManualSelLangAlert = MsgHelper.GetMessage("js:alert select source language and target language");
        jsManualSTlangAlert = MsgHelper.GetMessage("js:alert source language and target language cannot be same");
        jsManualErrTranslangAlert = MsgHelper.GetMessage("js:alert error occured while translating the text");
        jsManualNoTextAlert = MsgHelper.GetMessage("js:alert nothing was entered to translate");
        jsManualInvalidApiorurlAlert = MsgHelper.GetMessage("js:alert Error occured while loading the google supported languages because of invalid google api Key and/or url");
        jsManualloadgooglelangAlert = MsgHelper.GetMessage("js:alert Error occured while loading the google supported languages");
        jsManualNoContentTransAlert = MsgHelper.GetMessage("js:alert there is no content to translate");
        jsManualUnableleAlert = MsgHelper.GetMessage("js:alert unable to find element with content");
        lblNotFindEditor = MsgHelper.GetMessage("lbl could not find editor");
        lblWebpageEditor = MsgHelper.GetMessage("lbl this page must be opened by a web page that contains the editor");
        lblLoadingmsg = MsgHelper.GetMessage("lbl sync loading");
    }
    private void Page_Load(System.Object sender, System.EventArgs e)
    {
        
        RegisterResources();
        Response.CacheControl = "no-cache";
        Response.AddHeader("Pragma", "no-cache");
        Response.Expires = -1;
        stylesheetjs.Text = m_refStyle.GetClientScript();
        
        
        long currentUserID = AppUI.UserId;
        int EnableMultilingual = AppUI.EnableMultilingual;

        string AppPath = AppUI.AppPath;
        string AppImgPath = AppUI.AppImgPath;
        string SelectedEditControl = "";
        string sitePath = AppUI.SitePath;
        string AppName = AppUI.AppName;
        string AppeWebPath = AppUI.AppeWebPath;
        if (AppUI.RequestInformationRef.IsMembershipUser == 1 || AppUI.RequestInformationRef.UserId == 0)
        {
            Response.Redirect("blank.htm", false);
            return;
        }

        this.pageTitle.Text = MsgHelper.GetMessage("ektron translation");
        TransTitle.Text = m_refStyle.GetTitleBar(MsgHelper.GetMessage("machine translation title"));
        TransTitle.ToolTip = TransTitle.Text;
        lblsrc.Text = MsgHelper.GetMessage("lbl source language");
        lblsrc.ToolTip = lblsrc.Text;
        lblTar.Text = MsgHelper.GetMessage("lbl target language");
        lblTar.ToolTip = lblTar.Text;
        StringBuilder sbButton = new StringBuilder();
        sbButton.AppendLine("<table width=\"100%\">");
        sbButton.AppendLine("<tr>");
        SelectedEditControl = Utilities.GetEditorPreference(Request);
        if ("ContentDesigner" == SelectedEditControl)
        {
            sbButton.AppendLine(m_refStyle.GetButtonEventsWCaption(AppPath + "images/UI/Icons/cancel.png", "#", MsgHelper.GetMessage("generic cancel"), MsgHelper.GetMessage("btn cancel"), "Onclick=\"CloseDlg();\"", StyleHelper.CancelButtonCssClass, true));
        }
        else if ("Aloha" == SelectedEditControl)
        {
            sbButton.AppendLine(m_refStyle.GetButtonEventsWCaption(AppPath + "images/UI/Icons/cancel.png", "#", MsgHelper.GetMessage("generic cancel"), MsgHelper.GetMessage("btn cancel"), "Onclick=\"if (Ektron.Namespace.Exists('window.parent.Ektron.Translate.WorldLingo.CloseDialog')){window.parent.Ektron.Translate.WorldLingo.CloseDialog();}\"", StyleHelper.CancelButtonCssClass, true));
        }
        else
        {
            sbButton.AppendLine(m_refStyle.GetButtonEventsWCaption(AppPath + "images/UI/Icons/cancel.png", "#", MsgHelper.GetMessage("generic cancel"), MsgHelper.GetMessage("btn cancel"), "Onclick=\"window.close();\"", StyleHelper.CancelButtonCssClass, true));
        }
        sbButton.AppendLine("</tr>");
        sbButton.AppendLine("</table>");
        tblButton.InnerHtml = sbButton.ToString();

        if (IsPostBack)
        {
            
        }
        else
        {
            ListItem selLang;
            string lang;
            SourceLanguage = "0";
            lang = Request.QueryString["DefaultContentLanguage"];
            if (Information.IsNumeric(lang))
            {
                SourceLanguage = lang;
            }
            if (Convert.ToInt32(SourceLanguage) <= 0)
            {
                SourceLanguage = "1033"; // English
            }
            htmleditor = Request.QueryString["htmleditor"];
            TargetLanguage = "0";
            lang = Request.QueryString["LangType"];
            if (Information.IsNumeric(lang))
            {
                TargetLanguage = lang;
            }
            if (Convert.ToInt32(TargetLanguage) <= 0)
            {
                TargetLanguage = "1033";
            }
        }

    }

    protected void RegisterResources()
    {
        Packages.Ektron.Workarea.Core.Register(this);
        Packages.Ektron.Namespace.Register(this);
    }

}