using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Security.Application;
//namespace Ektron.Cms.Commerce.Workarea.CatalogEntry
//{
    public partial class Taxonomy_Tree_A_aspx : Ektron.Cms.Workarea.Page
    {
        #region Init

        protected override void OnInit(EventArgs e)
        {
            this.Context.Response.Charset = "utf-8";
            this.Context.Response.ContentType = "application/javascript";

            //set js server variables
            this.SetJsServerVariables();

            base.OnInit(e);
        }

        #endregion

        #region Methods - set js server variables

        private void SetJsServerVariables()
        {
            litFolderId.Text = AntiXss.HtmlEncode(Request.QueryString["folderId"]);
            litTaxonomyOverrideId.Text = AntiXss.HtmlEncode(Request.QueryString["taxonomyOverrideId"]);
            litTaxonomyTreeIdList.Text = AntiXss.HtmlEncode(Server.UrlDecode(Request.QueryString["taxonomyTreeIdList"]));
            litTaxonomyTreeParentIdList.Text = AntiXss.HtmlEncode(Server.UrlDecode(Request.QueryString["taxonomyTreeParentIdList"]));
        }

        #endregion
    }
//}
