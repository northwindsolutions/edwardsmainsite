﻿namespace Workarea.MobileDevicePreview
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Net;
    using System.IO;
    using System.Text.RegularExpressions;

    public partial class DeviceViewProxy : Ektron.Cms.Workarea.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string URL = Request.QueryString["targetURL"];
            string width = Request.QueryString["width"];
            string height = Request.QueryString["height"];
            string model = Request.QueryString["model"];
            string strCC = Request.Cookies["ecm"].Value;
            
            HttpCookie ccOrgCopy = new HttpCookie("ecm", strCC);
            
            //Inject the cookie values
            ccOrgCopy["dvcResWidth"] = width;
            ccOrgCopy["dvcResHeight"] = height;
            ccOrgCopy["dvcMdl"] = model;
            WebRequest request = WebRequest.Create(URL);
            request.Headers.Add("Cookie", "ecm=" + ccOrgCopy.Value);
            //((HttpWebRequest)request).UserAgent = Request.UserAgent;//Modify UA here


            WebResponse rsp = request.GetResponse();

            if (((HttpWebResponse)rsp).StatusCode == HttpStatusCode.OK)
            {

                Stream dataStream = rsp.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();

               
                reader.Close();
                rsp.Close();

                Response.Clear();
                Response.Write(ProcessHTML(responseFromServer));
            }
        }
        private bool ContainsImage(string s)
        {
            if (s.ToLower().Contains(".jpg") ||
                s.ToLower().Contains(".gif") ||
                s.ToLower().Contains(".png") ||
                s.ToLower().Contains(".jpeg"))
                return true;
            else
                return false;
        }
        private string replaceUrls(string s,bool isSrc)
        {
            Random r=new Random(DateTime.Now.Millisecond);
            string strRnd=r.Next(1000).ToString();
            bool constainsQuery = s.Contains("?");
            string retVal=s;

            if (ContainsImage(s))
            {
                if (isSrc)
                {
                    if (constainsQuery)
                    {
                        retVal = s + "&rnd=" + strRnd;
                    }
                    else
                    {
                        retVal = s + "?rnd=" + strRnd;
                    }
                }
                else
                {
                    if (retVal.Contains(".jpg"))
                        retVal = retVal.Replace(".jpg", ".jpg?rnd=" + strRnd);
                    if (retVal.Contains(".gif"))
                        retVal = retVal.Replace(".gif", ".jpg?rnd=" + strRnd);
                    if (retVal.Contains(".png"))
                        retVal = retVal.Replace(".png", ".jpg?rnd=" + strRnd);
                    if (retVal.Contains(".jpeg"))
                        retVal = retVal.Replace(".jpeg", ".jpg?rnd=" + strRnd);
                }
            }
            return retVal;
        }

        private string ProcessHTML(string responseFromServer)
        {
            string pattern = @"(?<name>src|href|style)=""(?<value>[^""]*)""";
            var matchEvaluator = new MatchEvaluator(
                match =>
                {
                    string value = match.Groups["value"].Value;
                    if (ContainsImage(value as string))//Uri.TryCreate(baseUri, value, out uri))
                    {
                        string name = match.Groups["name"].Value;
                        bool isSrc = string.Compare(name, "style", StringComparison.OrdinalIgnoreCase) != 0;
                        string replacedStr = replaceUrls(value.ToLower(), isSrc);
                        return string.Format("{0}=\"{1}\"", name, replacedStr);
                    }

                    return match.Value;
                });
            return Regex.Replace(responseFromServer, pattern, matchEvaluator, RegexOptions.IgnoreCase);
        }
    }
}