﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.API;

public partial class Workarea_PageBuilder_DroidX : Ektron.Cms.Workarea.Page
{
    protected ContentAPI _ContentApi = new ContentAPI();
    protected EkMessageHelper _MessageHelper;

    protected void Page_Load(object sender, EventArgs e)
    {
        //string[] urlSegments = this.Page.Request.Url.Segments;

        //url.Value = urlSegments[urlSegments.Count() - 1];
        _MessageHelper = _ContentApi.EkMsgRef;
        rotate.Attributes["title"] = _MessageHelper.GetMessage("lbl click to rotate");
        RegisterResources();

        url.Value = this.Page.Request.Url.AbsoluteUri;

        frame.Attributes["src"] = Request.QueryString["postbackurl"].ToString();

        if (this.Page.Request.Url.Query.Length == 0)
        {
            string myUrl = "http://www.ektron.com";

            if (this.Page.Request.UrlReferrer != null)
            {
                myUrl = this.Page.Request.UrlReferrer.ToString();
            }

            Response.Redirect(this.Page.Request.Url.AbsoluteUri + "?url=" + myUrl);


        }

    }

    private void RegisterResources()
    {
        // CSS        
        Ektron.Cms.API.Css.RegisterCss(this, _ContentApi.AppPath + "MobileDevicePreview/iPad_files/style.css", "EktroniPadstyleCSS");
        Ektron.Cms.API.Css.RegisterCss(this, _ContentApi.AppPath + "MobileDevicePreview/iPad_files/styleIE8.css", "EktroniPadstyleIE8CSS", Css.BrowserTarget.LessThanEqualToIE8);

        //JS
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronUICoreJS);
        
    }
}