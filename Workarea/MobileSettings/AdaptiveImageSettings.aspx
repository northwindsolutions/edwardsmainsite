﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Workarea.MobileSettings.AdaptiveImageSettings" CodeFile="AdaptiveImageSettings.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server">
    <title>Adaptive Image Settings</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <style>
        #left, #right {  
          background: white;  
        }  
 
        #left  { float:left;  width:480px;} 
        #right { float:right; position: absolute; top:80px; left:510px;}  
        #breakpointMain { white-space:nowrap; width:100%;}
        #uxSettings {border-bottom: none !important;}
        .subsetNone { border: none;}
        .subset {border:1px solid #D4D4D4;}
        input.ui-spinner-input {text-align:right; width:80px;}
        .greytext {font-size:0.8em; color:Gray;}
        .italictext { font-style:italic; font-size:0.8em;}
        .hidden {display:none;}
        .buttonAlignRight { float:right; }  
        ul.noBullet {list-style-type:none;}
        td.textAlighRight {text-align:right;}
        .indent {margin: 1.5em;}
        div.breakpoints, div.breakpoints > ul { margin: 0; padding 0;}
        div.breakpoints > ul > li { margin: 1em 0; }
        li.strip {background-color: #F4F4F4 !important; }
        span.breakpoint {margin:0.5em 0 0.5em 1em;}
        td.topGap {padding:0.8em 0 0 0;}
        span.groupHeader {font-weight : bold;}
        fieldset {margin: 0 0 1em;}
        .smallertext {font-size:0.8em; line-height: 3em;}
        .uxAdvancedSettings > span{line-height:3em;}
        div#uxSettings > ul.ui-widget-header {background:none; border-bottom:1px solid #D4D4D4;}
        div.ektronPageContainer > div.ektron-ui-message { margin-top: 1em; }
        label.uxValidationMessage { position: absolute; left: 7em;}
    </style>
    <script type="text/javascript">
        Ektron.ready(function () {
            // hide and show the breakpoint image settings at loading time.
            $ektron("span.breakpoint > input:checkbox").each(function () {
                var eImgSpec = $ektron(this).closest("li").children("div#adaptiveImgSpec");
                var eInputFileLabel = $ektron("span.fileLabel > input", eImgSpec);
                if (this.checked) {
                    eImgSpec.show(); 
                    if ("" == eInputFileLabel.val()) {
                        eInputFileLabel.removeAttr("disabled");
                    }
                    else {
                        eInputFileLabel.attr("disabled", "disabled");
                    }
                }
                else {
                    eImgSpec.hide();
                }
            });
            // hide and show the breakpoint image settings at on click
            $ektron("span.breakpoint > input:checkbox").click(function () {
                var eImgSpec = $ektron(this).closest("li").children("div#adaptiveImgSpec").toggle();
                var eInputFileLabel = $ektron("span.fileLabel > input", eImgSpec);
                eInputFileLabel.removeClass("ektron-ui-invalid");
                if ($ektron(this).is(":checked") && "" == eInputFileLabel.val()) {
                    eInputFileLabel.removeAttr("disabled").removeClass("ui-state-disabled");
                    $ektron("label.ektron-ui-validationMessage").hide();
                }
                else {
                    eInputFileLabel.attr("disabled", "disabled");
                }
            });
            // hide and show the level 2 caching settings
            var eL2Caching = $ektron("span.rlstEnableL2Caching");
            var eL2Fieldset = eL2Caching.siblings("fieldset");
            if (true == eL2Caching.children("input:checkbox").prop("checked")) {
                eL2Fieldset.show();
            }
            else {
                eL2Fieldset.hide();
            }
            eL2Caching.children("input:checkbox").click(function () {
                eL2Fieldset.toggle();
            });
        });
        // filelabel onsubmit validation test 
        function RequiredWhenSelected(value, element, errMsg, contextInfo) {
            var isSelected = $ektron(element).closest("#adaptiveImgSpec").siblings(".breakpoint").children("input:checkbox").is(":checked");
            if (isSelected) {
                var fileLabelVal = $ektron.trim(value);
                var alphaNumericRegEx = new RegExp("^[a-zA-Z0-9]+$");
                var isAlphaNumericNoSpace = alphaNumericRegEx.test(fileLabelVal);
                
                var pass = fileLabelVal.length > 0;
                if (!(pass && isAlphaNumericNoSpace)) {
                    $ektron("div.ektron-ui-success").hide();
                    return false;
                }
            }
            return true;
        }
    </script>
</head>
<body>
    <form runat="server">
    <div>
         <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar"></span>
            </div>
            <div class="ektronToolbar" id="htmToolBar" runat="server">
                <table>
                    <tr> 
                        <td>
                        <asp:LinkButton runat="server" ID="btnSave" OnClick="btnSave_Click" Text="<%$Resources: btnSave%>" CssClass="primary editButton" />
                        </td>
                        <td>
                            <a id="aHelp" runat="server" class="help" rel="nofollow">
								<img title="<%$Resources:Help %>" runat="server" alt="<%$Resources:Help %>" id="imgHelp" src="../images/UI/Icons/help.png" />
							</a>
                        </td>
                    </tr>
                  </table>
            </div>
            <div class="ektronPageContainer">
            <ektronUI:Message ID="uxSuccessMessage" DisplayMode="Success" Visible="false" runat="server">
            <ContentTemplate><asp:Label ID="success" runat="server" Text="<%$Resources:updateSuccess %>"></asp:Label></ContentTemplate>
            </ektronUI:Message>
            <ektronUI:Tabs ID="uxSettings" runat="server">
                <ektronUI:Tab ID="uxBreakpointSettings" runat="server" Text="<%$Resources: BreakpointSettings%>">
                    <ContentTemplate>
                    <asp:Label ID="uxBreakpointDescription" runat="server" Text="<%$Resources:BreakpointDescription %>"></asp:Label>
                    <%--<ektronUI:Button ID="uxAddNewBreakpoint" runat="server" CssClass="buttonAlignRight" Text="<%$Resources:AddNewBreakpoint %>" PrimaryIcon="CirclePlus"></ektronUI:Button>--%>
                                                                                                                                                                                                                                                                                                                                <div id="breakpointMain">
                    <div id="left" class="breakpoints subset">
                    <asp:Repeater runat="server" ID="rptBreakPoint">
                        <HeaderTemplate>
                            <% this.ItemIndex = 0; %>
                            <ul class="noBullet">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li class="<%# this.ItemIndex++ % 2 == 0 ? "" : "strip" %>">
                            <asp:HiddenField ID="breakpointId" runat="server" Value='<%# Eval("Id") %>' />
                            <asp:CheckBox ID="breakpointEnbaled" CssClass="breakpoint groupHeader" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.AdaptiveImageData.Enabled")  %>' Text='<%# Eval("Name") %>' />
                            <div id="adaptiveImgSpec">
                                <fieldset class="subsetNone">
                                    <asp:Label ID="uxAdaptiveImgDescription" runat="server" CssClass="smallertext" Text="<%$Resources:AdaptiveImgDescription %>"></asp:Label> <br />
                                    <table>
                                        <tr>
                                            <td class="textAlighRight"><ektronUI:Label ID="uxlblMaxImgWidth" runat="server" Text="<%$Resources:MaxImgWidth %>" AssociatedControlID="uxMaxImgWidth"></ektronUI:Label><span class="ektron-ui-required">*</span></td>
                                            <td>
                                            <ektronUI:IntegerField ID="uxMaxImgWidth" runat="server" ErrorMessage="Please enter a valid integer." MinValue="1" Value='<%# GetWidth(DataBinder.Eval(Container, "DataItem.AdaptiveImageData.Width"), DataBinder.Eval(Container, "DataItem.Width")) %>'>
                                            </ektronUI:IntegerField>
                                            <asp:Label ID="Label1" runat='server' Text="<%$Resources:Pixels %>"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="textAlighRight"><ektronUI:Label ID="uxlblMaxImgHeight" runat="server" Text="<%$Resources:MaxImgHeight %>" AssociatedControlID="uxMaxImgHeight"></ektronUI:Label></td>
                                            <td>
                                            <ektronUI:IntegerField ID="uxMaxImgHeight" runat="server" ErrorMessage="Please enter a valid integer." Value='<%# GetHeight(Container.DataItem) %>'>
                                            </ektronUI:IntegerField>
                                            <asp:Label ID="HeightPixel" runat='server' Text="<%$Resources:Pixels %>"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="topGap">
                                                <ektronUI:ValidationMessage ID="uxValidationMessage" runat="server" AssociatedControlID="uxFileLabel" CssClass="uxValidationMessage" /><br />
                                                <ektronUI:Label ID="uxlblFileLabel" runat="server" Text="<%$Resources:FileLabel %>" AssociatedControlID="uxFileLabel"></ektronUI:Label><span class="ektron-ui-required">*</span>
                                                <ektronUI:TextField ID="uxFileLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AdaptiveImageData.FileLabel") %>' CssClass="fileLabel">
                                                    <ValidationRules>
                                                          <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="RequiredWhenSelected" ContextInfo=""  ErrorMessage="<%$Resources:FileLabelRequired  %>" />                                                        
                                                    </ValidationRules>
                                                </ektronUI:TextField>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                    </div>
                    <div id="right">
                        <asp:Label ID="uxPathListDescription" runat="server" Text="<%$Resources:PathListDescription %>"></asp:Label>
                        <fieldset>
                            <asp:CheckBox ID="uxChkLibrary" runat="server" Text="<%$Resources:Library %>" /><br />
                            <asp:CheckBox ID="uxChkDms" runat="server" Text="<%$Resources:DMS %>" /><br />
                        </fieldset>
                        <div id="dvCustomPath" runat="server" visible="false">
                            <asp:Label ID="uxCustomPath" runat="server" Text="<%$Resources:CustomPath %>"></asp:Label><br />
                            <asp:Label ID="uxCustomPathWarning" runat="server" CssClass="important italictext indent" Text="<%$Resources:CustomPathWarning %>"></asp:Label><br />
                            <asp:ListView ID="rptCustomPath" runat="server" OnDataBound="rptCustomPath_OnDataBind" ItemPlaceholderID="itemContainer" OnItemCommand="DeletePath">
                                <LayoutTemplate>
                                <ul class="noBullet">
                                    <asp:PlaceHolder ID="itemContainer" runat="server"></asp:PlaceHolder>
                                </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                <li><asp:ImageButton ID="DeletePath" runat="server" CausesValidation="false" ImageUrl="../images/UI/Icons/delete.png" AlternateText="<%$Resources:deleteCustomPath %>" CssClass="delete" OnClick="DeletePath" CommandArgument="<%#Container.DataItem %>" /><%#Container.DataItem %></li>    
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                    </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxAdvancedSettings" CssClass="uxAdvancedSettings" runat="server" Text="<%$Resources: AdvancedSettings%>">
                        <ContentTemplate>
                            <asp:Label ID="uxMinFileSize" runat="server" CssClass="groupHeader" Text="<%$Resources:MinFileSize %>"></asp:Label>
                            <fieldset>
                                <asp:Label ID="uxMinFileSizeDescription" runat="server" CssClass="smallertext" Text="<%$Resources:MinFileSizeDescription %>"></asp:Label>
                                <br />
                                <ektronUI:Label ID="uxMinImgSizeThreshold" runat="server" Text="<%$Resources:MinImgSizeThreshold %>" AssociatedControlID="tfImageSizeThresholdKB"></ektronUI:Label>
                                <ektronUI:IntegerField ID="tfImageSizeThresholdKB" ErrorMessage="Please enter a valid integer." runat="server" MinValue="0" >
                                </ektronUI:IntegerField>KB
                            </fieldset> 
                            <asp:Label ID="uxCaching" runat="server" CssClass="groupHeader" Text="<%$Resources:Caching %>"></asp:Label>
                            <fieldset>
                                <asp:Label ID="uxLevel1CachingDescription" runat="server" CssClass="smallertext" Text="<%$Resources:Level1CachingDescription %>"></asp:Label>
                                <ektronUI:Button ID="uxClearCacheButton" DisplayMode="Button" CssClass="buttonAlignRight" Text="<%$Resources:ClearCacheButton %>" PrimaryIcon="Refresh" runat="server" />
                                <br />
                                <ektronUI:Label ID="uxLevel1CacheLabel" runat="server" Text="<%$Resources:Level1CacheLabel %>" AssociatedControlID="tfLevel1CacheSize"></ektronUI:Label>
                                <ektronUI:IntegerField ID="tfLevel1CacheSize" ErrorMessage="Please enter a valid integer." runat="server" MinValue="0" >
                                </ektronUI:IntegerField>
                                <asp:Label ID="lblImg" runat="server" Text="<%$Resources:images %>"></asp:Label>
                                <br />
                                <asp:CheckBox ID="rlstEnableL2Caching" runat="server" Text="<%$Resources:CacheImgContents %>" class="rlstEnableL2Caching" />
                                <ektronUI:Infotip ID="uxInfo" runat="server"></ektronUI:Infotip>
                                <br />
                                <fieldset class="subsetNone">
                                    <%--<asp:Label ID="uxLevel2CachingDescription" runat="server" CssClass="smallertext" Text="<%$Resources:Level2CachingDescription %>"></asp:Label>
                                    <br />--%>
                                    <ektronUI:Label ID="uxLevel2CacheLabel" runat="server" Text="<%$Resources:Level2CacheLabel %>" AssociatedControlID="tfLevel2CacheSize"></ektronUI:Label>
                                    <ektronUI:IntegerField ID="tfLevel2CacheSize" ErrorMessage="Please enter a valid integer." runat="server" MinValue="0" >
                                    </ektronUI:IntegerField>
                                    <asp:Label ID="lblImg2" runat="server" Text="<%$Resources:images %>"></asp:Label>
                                    <br />
                                    <asp:Label ID="uxLevel2MaxSizeDescription" runat="server" CssClass="smallertext" Text="<%$Resources:Level2MaxSizeDescription %>"></asp:Label>
                                    <br />
                                    <ektronUI:Label ID="uxMaxImgSizeThreshold" runat="server" Text="<%$Resources:MaxImgSizeThreshold %>" AssociatedControlID="tfLevel2CacheItemThresholdKB"></ektronUI:Label>
                                    <ektronUI:IntegerField ID="tfLevel2CacheItemThresholdKB" ErrorMessage="Please enter a valid integer." runat="server" MinValue="0" >
                                    </ektronUI:IntegerField>KB
                                </fieldset>
                            </fieldset> 
                            <asp:Label ID="uxDirectAccessQueryString" runat="server" CssClass="groupHeader" Text="<%$Resources:DirectAccessQueryString %>"></asp:Label>
                            <fieldset>
                                <asp:Label ID="uxDirectAccessDescription" runat="server" CssClass="smallertext" Text="<%$Resources:DirectAccessDescription %>"></asp:Label>
                                <br />
                                <ektronUI:Label ID="uxDirectAccessKey" runat="server" Text="<%$Resources:DirectAccessKey %>" AssociatedControlID="tfDirectAccessQueryString"></ektronUI:Label>
                                <ektronUI:TextField ID="tfDirectAccessQueryString" runat="server" Text="<%$Resources:DirectAccessKeyText %>">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule />
                                        <ektronUI:AlphaOnlyRule />
                                    </ValidationRules>
                                </ektronUI:TextField>
                            </fieldset>
                        </ContentTemplate>
                    </ektronUI:Tab>
                </ektronUI:Tabs>                                        
            </div>
        </div>
    </div>
    </form>
</body>
</html>
