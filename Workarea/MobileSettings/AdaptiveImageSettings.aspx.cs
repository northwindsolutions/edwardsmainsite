﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.Settings.Mobile;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Settings.Mobile;

    public partial class AdaptiveImageSettings : Ektron.Cms.Workarea.Page
    {
        protected Ektron.Cms.ContentAPI _ContentApi;
        protected Int32 ItemIndex;
        private AdaptiveImageSettingManager settingMgr = null;
        private List<string> customPath = null;
        private AdaptiveImageSettingData SettingData = null;
        private List<string> deleteCustomPath
        {
            get { return ViewState["DeleteCustomPath"] as List<string>; }
            set { ViewState["DeleteCustomPath"] = value; }
        }

        private void Page_Load(System.Object sender, System.EventArgs e)
        {
            _ContentApi = new Ektron.Cms.ContentAPI();
            RegisterResources();
            Utilities.ValidateUserLogin();
            divTitleBar.InnerHtml = GetLocalResourceObject("PageTitle").ToString();

            //set help link  
            aHelp.HRef = "#Help";
            aHelp.Attributes.Add("onclick", "window.open('" + _ContentApi.fetchhelpLink("AdaptiveImageTopic") + "', 'SitePreview', 'width=800,height=500,resizable=yes,toolbar=no,scrollbars=1,location=no,directories=no,status=no,menubar=no,copyhistory=no');return false;");
            // dry info msg
            uxInfo.Text = GetLocalResourceObject("MoreInformation").ToString();
            
            settingMgr = new AdaptiveImageSettingManager();

            if (!Page.IsPostBack)
            {
                SettingData = settingMgr.GetSettings();
                customPath = LoadPathList(SettingData.IncludePathList);
                if (customPath != null)
                {
                    rptCustomPath.DataSource = customPath;
                    rptCustomPath.DataBind();
                }
                tfImageSizeThresholdKB.Text = SettingData.ImageSizeThresholdKB.ToString();
                tfLevel1CacheSize.Text = SettingData.Level1CacheSize.ToString();
                tfLevel2CacheItemThresholdKB.Text = SettingData.Level2CacheItemThresholdKB.ToString();
                tfLevel2CacheSize.Text = SettingData.Level2CacheSize.ToString();

                tfDirectAccessQueryString.Text = SettingData.DirectAccessQueryString;
                rlstEnableL2Caching.Checked = (SettingData.EnableL2Caching.ToString().ToLower() == "true");

                ReloadBreakpointImageData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DeviceBreakpointManager manager = new DeviceBreakpointManager();

            for (int idx = 0; idx < rptBreakPoint.Items.Count; idx++)
            {
                DeviceBreakpointData toAddEdit = new DeviceBreakpointData();
                toAddEdit.Id = Int64.Parse(((HiddenField)rptBreakPoint.Items[idx].FindControl("breakpointId")).Value);
                DeviceBreakpointManager m = new DeviceBreakpointManager();
                DeviceBreakpointData data = m.GetItem(toAddEdit.Id);
                toAddEdit.Name = data.Name;
                toAddEdit.FileLabel = data.FileLabel;
                toAddEdit.Description = data.Description;
                toAddEdit.Height = data.Height;
                toAddEdit.Width = data.Width;

                DeviceBreakpointImageSizeData imageData = new DeviceBreakpointImageSizeData();
                var container = rptBreakPoint.Items[idx].FindControl("uxMaxImgWidth") as Ektron.Cms.Framework.UI.Controls.EktronUI.IntegerField;
                imageData.Width = container.Value;
                container = rptBreakPoint.Items[idx].FindControl("uxMaxImgHeight") as Ektron.Cms.Framework.UI.Controls.EktronUI.IntegerField;
                imageData.Height = (container.Text.Length > 0 ? container.Value : imageData.Width);
                var txtcontainer = rptBreakPoint.Items[idx].FindControl("uxFileLabel") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField;
                imageData.FileLabel = txtcontainer.Text;
                if (0 == imageData.FileLabel.Length && data.AdaptiveImageData != null)
                {
                    imageData.FileLabel = data.AdaptiveImageData.FileLabel;
                }
                if (((CheckBox)rptBreakPoint.Items[idx].FindControl("breakpointEnbaled")).Checked == true)
                {
                    imageData.Enabled = true;
                }
                else
                {
                    imageData.Enabled = false;
                }
                toAddEdit.AdaptiveImageData = imageData; 
                manager.Update(toAddEdit);
            }
            ReloadBreakpointImageData();

            AdaptiveImageSettingData SettingData = settingMgr.GetSettings();
            SettingData.ImageSizeThresholdKB = tfImageSizeThresholdKB.Value;
            SettingData.Level1CacheSize = tfLevel1CacheSize.Value;
            SettingData.Level2CacheItemThresholdKB = tfLevel2CacheItemThresholdKB.Value;
            SettingData.Level2CacheSize = tfLevel2CacheSize.Value;
            SettingData.DirectAccessQueryString = tfDirectAccessQueryString.Text;
            SettingData.EnableL2Caching = rlstEnableL2Caching.Checked;
            if (this.deleteCustomPath != null && this.deleteCustomPath.Count > 0)
            {
                string[] toDelete = this.deleteCustomPath.ToArray();
                SettingData.IncludePathList.RemoveAll(x => toDelete.Contains(x));
            }
            List<string> includePathList = new List<string>();
            if (true == uxChkLibrary.Checked)
            {
                includePathList.Add(uxChkLibrary.Attributes["path"]);
            }
            if (true == uxChkDms.Checked)
            {
                includePathList.Add(uxChkDms.Attributes["path"]);
            }
            SettingData.IncludePathList = includePathList;

            settingMgr.SaveSettings(SettingData);

            //Reload the settings for the singleton object.
            Ektron.ASM.EkHttpDavHandler.AdaptiveImageProcessor.Instance.BreakPoints.Clear();
            Ektron.ASM.EkHttpDavHandler.AdaptiveImageProcessor.Instance.BreakPoints.AddRange(manager.GetList(new DeviceBreakpointCriteria()));
            Ektron.ASM.EkHttpDavHandler.AdaptiveImageProcessor.Instance.ReloadSettings();
            Ektron.Cms.UserContext.ReloadDeviceBreakpointCache();

            uxSuccessMessage.Visible = true;

        }

        private void ReloadBreakpointImageData()
        {
            DeviceBreakpointManager m = new DeviceBreakpointManager();
            DeviceBreakpointCriteria c = new DeviceBreakpointCriteria();
            c.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            c.OrderByField = DeviceBreakpointProperty.Width;

            List<DeviceBreakpointData> lst = m.GetList(c);
            foreach (DeviceBreakpointData data in lst)
            {
                if (data.AdaptiveImageData == null)
                {
                    DeviceBreakpointImageSizeData i = new DeviceBreakpointImageSizeData();
                    i.Enabled = false;
                    data.AdaptiveImageData = i;
                }
            }

            rptBreakPoint.DataSource = lst;
            rptBreakPoint.DataBind();
        }

        private List<string> LoadPathList(List<string> pathList)
        {
            List<string> customPathList = new List<string>();
            uxChkLibrary.Attributes.Add("path", "~/uploadedimages");
            uxChkDms.Attributes.Add("path", "~/assets");
            if (pathList.Count > 0)
            {
                for (int idx = 0; idx < pathList.Count; idx++)
                {
                    if (pathList[idx] != null)
                    {
                        string displayText = pathList[idx].ToString();
                        switch (displayText)
                        {
                            case "~/uploadedimages":
                                uxChkLibrary.Checked = true;
                                break;
                            case "~/assets":
                                uxChkDms.Checked = true;
                                break;
                            default:
                                customPathList.Add(displayText);
                                break;
                        }
                    }
                }
            }
            if (customPathList.Count > 0)
            {
                return customPathList;
            }
            return null;
        }
        protected void rptCustomPath_OnDataBind(object sender, EventArgs e)
        {
            if (rptCustomPath.Items.Count > 0)
            {
                dvCustomPath.Visible = true;
            }
        }
        protected void DeletePath(object sender, EventArgs e)
        {
            ImageButton btnDelete = (ImageButton)sender;
            if (this.deleteCustomPath == null)
            {
                this.deleteCustomPath = new List<string>();
            }
            this.deleteCustomPath.Add(btnDelete.CommandArgument);
            customPath.RemoveAll(x => this.deleteCustomPath.ToArray().Contains(x));
            rptCustomPath.DataSource = customPath;
            rptCustomPath.DataBind();
        }
        protected int GetWidth(object itemValue, object defaultWidth)
        {
            int itemWidth = 0;
            int.TryParse(itemValue.ToString(), out itemWidth);
            return (itemWidth > 0 ? itemWidth : int.Parse(defaultWidth.ToString()));
        }

        protected int GetHeight(object item)
        {
            DeviceBreakpointData DataItem = item as DeviceBreakpointData;
            if (DataItem == null)
                return 0;
            if (DataItem.AdaptiveImageData.Height <= 0)
                return DataItem.Width;
            else
                return DataItem.AdaptiveImageData.Height;
        }

        private void RegisterResources()
        {
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
        }
    }
}