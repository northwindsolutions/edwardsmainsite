﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditBreakPoint.aspx.cs" Inherits="Workarea.MobileSettings.AddEditBreakPoint" %>

<%@ Reference Control="~/Workarea/MobileSettings/DeviceGroupTemplateListingCtrl.ascx" %>
<%@ Register TagPrefix="ektronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:Css Path="css/addEditBreakpoint.css" runat="server" />
        <ektronUI:JavaScriptBlock ExecutionMode="OnParse" runat="server">
            <ScriptTemplate>
                var jsDeleteDlgId = '<%= diagDelete.Selector %>';
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <ektronUI:JavaScript Path="js/addEditBreakpoint.js" runat="server" />
        <div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="divTitleBar" runat="server">
                    <span id="WorkareaTitleBar">
                        <asp:Label ID="pageTitle" runat="server" Text="<%$Resources: pageTitle %>"></asp:Label></span>
                </div>
                <div class="ektronToolbar" id="htmToolBar" runat="server">
                    <table style="" cellpadding="1px" cellspacing="1px">
                        <tr>
                            <td><a style="cursor: default; margin-right: .5em;" class="primary backButton" href="DeviceGroupingView.aspx"></a></td>
                            <td>
                                <asp:LinkButton runat="server" ID="uxSave" Text="<%$Resources: btnSave%>" ValidationGroup="Save" CausesValidation="true" CssClass="primary editButton" OnClick="Save_Click" /></td>
                            <td>
                                <a id="aHelp" runat="server" class="help" rel="nofollow">
                                    <img title="<%$Resources:Help %>" runat="server" alt="<%$Resources:Help %>" id="imgHelp" src="../images/UI/Icons/help.png" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ektronPageContainer">
                    <p>
                        <asp:Label ID="DeviceGroupDescription" runat="server" Text="<%$Resources:DeviceGroupDescription %>"></asp:Label>
                    </p>
                    <p>
                        <ektronUI:Message ID="uxGroupDevicesMessage" runat="server" DisplayMode="Success" Visible="false"></ektronUI:Message>
                        <ektronUI:Blueprint ID="validationStyling" RegisterFormsCss="true" runat="server" />
                        <asp:CustomValidator ID="uxEnableDeviceValidation" ClientValidationFunction-="EnableDeviceValidation" Display="Dynamic" OnServerValidate="EnableDeviceValidation_ServerValidate" ValidationGroup="Save" runat="server" />
                        <asp:Panel ID="uxEnableDeviceErrorMessagePanel" CssClass="enableDeviceErrorMessage" runat="server">
                            <ektronUI:Message ID="uxEnableDeviceErrorMessage" DisplayMode="Error" Text="<%$Resources:EnableDeviceErrorMessage %>" runat="server" />
                        </asp:Panel>
                        <br />
                        <ektronUI:Button ID="uxAddBreakpoint" runat="server" Text="<%$Resources:AddBreakpoint %>" PrimaryIcon="CirclePlus"></ektronUI:Button>
                    </p>
                    <div class="alignRight">
                        <asp:Label ID="ImagePreviewLabel" runat="server" Text="<%$Resources:ImagePreview %>"></asp:Label>
                        <ektronUI:Infotip ID="uxInfo" runat="server" PositionCollision="flip" PositionAt="right center" Hide="fadeOut" PositionMy="left top"></ektronUI:Infotip>
                    </div>
                    <asp:HiddenField ID="targetDeleteId" runat="server" />
                    <div style="margin-top: 4em;">
                        <asp:ListView runat="server" ID="uxBreakPoint" ClientIDMode="AutoID" OnItemDataBound="BreakPoint_OnItemDataBound" OnItemCommand="BreakPoint_ItemCommand">
                            <LayoutTemplate>
                                <table class="breakpointTbl">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr class="row">
                                    <td id="left" class="breakpoints">
                                        <asp:HiddenField ID="DeviceGroupId" runat="server" Value='<%#Eval("Id") %>' />
                                        <asp:LinkButton ID="DeviceGroupName" runat="server" Text='<%#Eval("GroupName") %>' CssClass="DeviceGroupName"></asp:LinkButton><ektronUI:TextField ID="txtGroupName" runat="server" Text='<%#Eval("GroupName") %>' CssClass="txtGroupName"><ValidationRules><ektronUI:RequiredRule ClientValidationEnabled="true" ServerValidationEnabled="true" /></ValidationRules></ektronUI:TextField>
                                        <span id="BreakpointRange">
                                            <asp:Label ID="DeviceGroupInfo" runat="server" Text='<%#GetPrePendWidthLabel(Eval("Width")) %>'></asp:Label>
                                            <ektronUI:TextField ID="uxWidth" runat="server" Text='<%#Eval("Width") %>' CssClass="shortFields widthValue">
                                                <ValidationRules>
                                                    <ektronUI:RequiredRule ClientValidationEnabled="true" ServerValidationEnabled="true" />
                                                    <ektronUI:DigitsRule ClientValidationEnabled="true" ServerValidationEnabled="true" />
                                                </ValidationRules>
                                            </ektronUI:TextField>
                                            <asp:Label ID="lblPixels" runat="server" Text="<%$Resources:Pixels %>"></asp:Label>
                                            <asp:ImageButton ID="uxDelete" runat="server" CausesValidation="false" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/delete.png" CommandName="delItem" CommandArgument='<%# Eval("Id") %>' />
                                        </span>
                                        <br />
                                        <img alt="lineBarStart" width="2px" height="18px" class="bkgd" src="../images/spacer.gif" /><asp:Image ID="lineBar" runat="server" AlternateText='<%#Eval("GroupInfo") %>' ImageUrl="~/Workarea/images/spacer.gif" Height="2px" Width='<%#GetLineBarWidth(Eval("Width")) %>' CssClass="bkgd alighMiddle" /><img alt="lineBarEnd" width="2px" height="18px" class="bkgd endbar" src="../images/spacer.gif" />
                                        <br />
                                        <asp:ImageButton ID="uxRefresh" runat="server" ImageUrl="~/Workarea/images/UI/Icons/refresh.png" AlternateText="<%$Resources:btnRefresh %>" CssClass="refresh" OnClick="Refresh_Click" CausesValidation="false" />
                                        <asp:Label ID="phoneList" runat="server" Text="List..." CssClass="offSetLeft"></asp:Label>
                                    </td>
                                    <td id="right">
                                        <asp:ListView ID="phoneImgList" runat="server">
                                            <LayoutTemplate>
                                                <ul id="firstCarousel" class="phonepreview-carousel jcarousel-skin-tango" runat="server">
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                                </ul>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <div class="enablePreviewImage" style="background: url(<%#GetPreviewImg(DataBinder.Eval(Container.DataItem, "ThumbnailPath")) %>) no-repeat;">
                                                        <asp:Label ID="chkPreviewEnabledImageLabel" AssociatedControlID="chkPreviewEnabled" CssClass="enablePreviewImage" runat="server"></asp:Label>
                                                    </div>
                                                    <asp:HiddenField ID="devicePreviewId" Value='<%#DataBinder.Eval(Container.DataItem, "Id") %>' runat="server" />
                                                    <asp:HiddenField ID="devicePreviewEnabled" Value='<%#DataBinder.Eval(Container.DataItem, "EnabledPreview") %>' runat="server" />
                                                    <asp:CheckBox ID="chkPreviewEnabled" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem, "EnabledPreview") %>' ToolTip='<%#DataBinder.Eval(Container.DataItem, "Model") %>' />
                                                    <asp:Label ID="lblPreview" AssociatedControlID="chkPreviewEnabled" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Model") %>' ToolTip='<%#DataBinder.Eval(Container.DataItem, "Model") %>' CssClass="deviceModelLabel" />
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
        </div>

        <ektronUI:Dialog ID="uxIFrameDialog" CssClass="uxIFrameDialog" Title="<%$Resources:AddDevice %>" runat="server" Width="630" Height="250" AutoOpen="false" Modal="true" Resizable="true" Draggable="true">
            <ContentTemplate>
                <h2>
                    <asp:Label ID="addByBreakpoint" runat="server" Text="<%$Resources:AddByBreakPoint %>"></asp:Label></h2>
                <br />
                <table>
                    <tr>
                        <td class="textAlighRight">
                            <asp:Label ID="lblName" runat="server" Text="<%$Resources:lblName %>"></asp:Label><span class="ektron-ui-required">*</span></td>
                        <td>
                            <ektronUI:ValidationMessage ID="uxValidationMessage" runat="server" AssociatedControlID="uxName" CssClass="uxValidationMessage" /><br />
                            <ektronUI:TextField ID="uxName" runat="server"><ValidationRules><ektronUI:RequiredRule ErrorMessage="<%$Resources:NameRequried%>" /></ValidationRules></ektronUI:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td class="textAlighRight">
                            <asp:Label ID="lblWidth" runat="server" Text="<%$Resources:lblWidth %>"></asp:Label><span class="ektron-ui-required">*</span>
                        </td>
                        <td>
                            <ektronUI:IntegerField ID="uxWidth" runat="server" CssClass="shortFields" MinValue="1">
                        <ValidationRules>
                            <ektronUI:RequiredRule ClientValidationEnabled="true" ServerValidationEnabled="false" ErrorMessage="<%$Resources:IntegerRequired %>" />
                        </ValidationRules>
                            </ektronUI:IntegerField>
                            <asp:Label ID="widthUnit" runat="server" Text="<%$Resources:Pixels %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&#160;</td>
                        <td><%--<asp:Label ID="widthNotes" runat="server" Text="<%$Resources:widthNotes %>" CssClass="smallertext"></asp:Label>--%></td>
                    </tr>
                </table>
                <asp:Panel runat="server" ID="pnlOK" CssClass="buttonRight">
                    <ektronUI:Button runat="server" ID="uxCreate" OnClick="Create_Click" Text="<% $Resources: btnCreate %>" ToolTip="<% $Resources: btnCreate %>" CloseDialog="true"></ektronUI:Button>
                    <ektronUI:Button runat="server" ID="uxCancel" Text="<% $Resources: btnCancel %>" ToolTip="<% $Resources: btnCancel %>"></ektronUI:Button>
                </asp:Panel>
            </ContentTemplate>
        </ektronUI:Dialog>

        <ektronUI:Dialog runat="server" ID="diagDelete" Modal="true" AutoOpen="false" Title="<%$Resources:diagDel_title %>" Width="600" Height="300">
            <ContentTemplate>
                <h1>
                    <asp:Literal ID="ltrdiagDeleteHeader" runat="server" /></h1>
                <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:diagDel_body %>" />
                <asp:Panel runat="server" ID="pnlDeleteListHolder" />
                <b>
                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:diagDel_confirm %>" /></b>
                <div style="text-align: right">
                    <ektronUI:Button runat="server" ID="Delete" CausesValidation="false" DisplayMode="Anchor" Text="<%$Resources:diagDel_btnDelete %>" ToolTip="<% $Resources: diagDel_btnDelete %>" OnClick="Delete_Click" CloseDialog="true"></ektronUI:Button>
                    <ektronUI:Button runat="server" ID="DelteCancel" Text="<%$Resources:diag_Cancel %>" ToolTip="<% $Resources: diag_Cancel %>" OnClientClick="closeDeleteDialog();"></ektronUI:Button>
                </div>
            </ContentTemplate>
        </ektronUI:Dialog>

    </form>
</body>
</html>
