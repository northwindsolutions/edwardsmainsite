﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Device;
    using Ektron.Cms.Framework.Settings.Mobile;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Mobile;
    using Ektron.Cms.Settings.Mobile;

    public partial class AddEditBreakPoint : Ektron.Cms.Workarea.Page
    {
        internal class DeviceGroupingViewData
        {
            public enum GroupTypes
            {
                Model = 0,
                Os = 1,
                Size = 2
            }

            public long Id { get; set; }

            public GroupTypes GroupType { get; set; }

            public string GroupTypeText
            {
                get
                {
                    return this.GroupType.ToDescriptionString();
                }
            }

            public string GroupName { get; set; }

            public string GroupInfo { get; set; }

            public int NumOfTemplates { get; set; }

            public int Width { get; set; }

            public DeviceGroupingViewData(DeviceBreakpointData src)
            {
                this.GroupType = GroupTypes.Size;
                this.GroupName = src.Name;
                this.GroupInfo = "";
                this.Id = src.Id;
                this.NumOfTemplates = 0;
                this.Width = 0;
            }

            public DeviceGroupingViewData(CmsDeviceConfigurationData src)
            {
                if (src.DisplayFor == 0)
                {
                    this.GroupType = GroupTypes.Model;
                }
                else if (src.DisplayFor == 1)
                {
                    this.GroupType = GroupTypes.Os;
                }

                this.GroupName = src.Name;
                this.GroupInfo = string.Join(",", src.Models.ToArray());
                this.Id = src.Id;
                this.NumOfTemplates = 0;
                this.Width = src.Width.HasValue ? int.Parse(src.Width.ToString()) : 0;
            }
        }

        /// <summary>
        /// Device information access, instantiated during OnInit.
        /// </summary>
        private IDevicePreviewManager previews = null;

        /// <summary>
        /// Breakpoint access, instantiated during OnInit.
        /// </summary>
        private DeviceBreakpointManager breakpoints = null;

        /// <summary>
        /// Content access, instantiated during OnInit.
        /// </summary>
        private ContentAPI content = null;

        private List<int> WidthPixelSpec
        {
            get
            {
                if (null == ViewState["WidthPixelSpec"])
                {
                    ViewState["WidthPixelSpec"] = new List<int>();
                }

                return ViewState["WidthPixelSpec"] as List<int>;
            }

            set
            {
                ViewState["WidthPixelSpec"] = value;
            }
        }

        #region Page Events

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Instantiate frequently-accessed, global APIs
            breakpoints = new DeviceBreakpointManager();
            content = new Ektron.Cms.ContentAPI();
            previews = Ektron.Cms.ObjectFactory.Get<IDevicePreviewManager>();

            RegisterResources();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Set default messaging visibility to hidden
            uxGroupDevicesMessage.Visible = false;
            this.uxEnableDeviceErrorMessagePanel.Style.Add("display", "none");

            uxAddBreakpoint.OnClientClick = "$ektron('" + uxIFrameDialog.Selector + "').dialog('open'); return false;";
            uxCancel.OnClientClick = "$ektron('" + uxIFrameDialog.Selector + "').dialog('close'); return false;";

            //set help link  
            aHelp.HRef = "#Help";
            aHelp.Attributes.Add("onclick", "window.open('" + content.fetchhelpLink("AddEditBreakpoint") + "', 'SitePreview', 'width=800,height=500,resizable=yes,toolbar=no,scrollbars=1,location=no,directories=no,status=no,menubar=no,copyhistory=no');return false;");

            // set info message
            uxInfo.Text = GetLocalResourceObject("ImagePreviewHelp").ToString();

            if (!Page.IsPostBack)
            {
                LoadValues();
            }
        }

        #endregion

        #region Form Events

        protected void Create_Click(object sender, EventArgs e)
        {
            DeviceBreakpointData toAdd = new DeviceBreakpointData();
            toAdd.Name = uxName.Text;
            toAdd.FileLabel = uxName.Text;
            toAdd.Description = uxName.Text;
            toAdd.Height = int.Parse(uxWidth.Value.ToString());
            toAdd.Width = int.Parse(uxWidth.Value.ToString());

            breakpoints.Add(toAdd);

            Response.Redirect("AddEditBreakPoint.aspx");
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            if (ViewState["DeleteItem"] != null)
            {
                string[] pair = ViewState["DeleteItem"].ToString().Split('|');
                if (pair.Length == 2)
                {
                    if (pair[0] == "breakpoint")
                    {

                        breakpoints.Delete(long.Parse(pair[1]));
                    }
                }
            }

            Response.Redirect("AddEditBreakPoint.aspx");
        }

        /// <summary>
        /// This marks the entire page as invalid if any breakpoint has no 
        /// device enabled, noting the particular breakpoint for attention, and
        /// displays a page-wide error message.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="ServerValidateEventArgs" /> instance containing the event data.</param>
        protected void EnableDeviceValidation_ServerValidate(object source, ServerValidateEventArgs args)
        {
            foreach (ListViewItem item in uxBreakPoint.Items)
            {
                ListView phoneImgList = (ListView)item.FindControl("phoneImgList");

                bool valid = false;
                foreach (ListViewDataItem phoneImg in phoneImgList.Items)
                {
                    CheckBox chkPreviewEnabled = (CheckBox)phoneImg.FindControl("chkPreviewEnabled");

                    // Valid if any one checkbox is checked
                    valid = valid | chkPreviewEnabled.Checked;
                }

                HtmlGenericControl firstCarousel = (HtmlGenericControl)phoneImgList.FindControl("firstCarousel");
                // If there are no devices in the given range of breakpoint, there won't be any devices for that group. Hence there won't be any carousel.
                if (firstCarousel != null)
                {
                    // Reset the carousel's style classes
                    firstCarousel.Attributes["class"] = firstCarousel.Attributes["class"].Replace(" error", string.Empty);

                    if (!valid)
                    {
                        firstCarousel.Attributes["class"] += " error";

                        // Return that the page is invalid
                        args.IsValid = false;

                        // Allow the error message to be displayed
                        this.uxEnableDeviceErrorMessagePanel.Style.Remove("display");
                    }
                }
            }
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            if (ValidateBreakpointWidths())
            {
                List<DeviceBreakpointData> tempList =
                    (from item in uxBreakPoint.Items
                     select new DeviceBreakpointData()
                      {
                          Id = long.Parse(((HiddenField)item.FindControl("DeviceGroupId")).Value),
                          Name = (item.FindControl("txtGroupName") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField).Value,
                          Width = int.Parse((item.FindControl("uxWidth") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField).Value)
                      }
                    ).OrderBy(b => b.Width).ToList();

                WidthPixelSpec.Clear();

                DataBindBreakPointsTable(this.GetDeviceGroups(tempList));
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ValidateBreakpointWidths())
            {
                foreach (ListViewItem item in uxBreakPoint.Items)
                {
                    long id = long.Parse(((HiddenField)item.FindControl("DeviceGroupId")).Value);
                    bool breakpointChanged = false;

                    // Get the existing breakpoint to maintain values not changed in the UI
                    DeviceBreakpointData breakpoint = breakpoints.GetItem(id);

                    var txtGroupName = item.FindControl("txtGroupName") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField;
                    if (breakpoint.Name != txtGroupName.Value)
                    {
                        breakpoint.Name = txtGroupName.Value;
                        breakpointChanged = true;
                    }

                    var uxWidth = item.FindControl("uxWidth") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField;
                    if (breakpoint.Width != int.Parse(uxWidth.Value))
                    {
                        breakpoint.Width = int.Parse(uxWidth.Value);

                        if (breakpoint.Height == 0)
                        {
                            breakpoint.Height = breakpoint.Width;
                        }

                        breakpointChanged = true;
                    }

                    if (breakpointChanged)
                    {
                        breakpoints.Update(breakpoint);
                    }

                    //saving mobile preview settings
                    ListView phoneImgList = (ListView)item.FindControl("phoneImgList");
                    for (int j = 0; j < phoneImgList.Items.Count; j++)
                    {
                        CheckBox chkPreviewEnabled = (CheckBox)phoneImgList.Items[j].FindControl("chkPreviewEnabled");
                        HiddenField devicePreviewEnabled = (HiddenField)phoneImgList.Items[j].FindControl("devicePreviewEnabled");

                        // Only update device preview when it changed
                        if (chkPreviewEnabled.Checked.ToString() != devicePreviewEnabled.Value)
                        {
                            HiddenField devicePreviewId = (HiddenField)phoneImgList.Items[j].FindControl("devicePreviewId");
                            previews.Update(long.Parse(devicePreviewId.Value), chkPreviewEnabled.Checked);
                        }
                    }
                }

                uxGroupDevicesMessage.Text = GetLocalResourceObject("SaveSuccess").ToString();
                uxGroupDevicesMessage.Visible = true;

                LoadValues();
            }
        }

        #endregion

        #region Data-binding Events

        protected void BreakPoint_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "delItem")
            {
                long breakpointId = 0;

                if (long.TryParse(e.CommandArgument.ToString(), out breakpointId))
                {
                    DeviceGroupTemplateListingCtrl templateListing = LoadControl("DeviceGroupTemplateListingCtrl.ascx") as DeviceGroupTemplateListingCtrl;
                    templateListing.deleteID = breakpointId;
                    templateListing.isBreakpoint = true;
                    templateListing.LoadData();

                    List<DeviceGroupingViewData> ds = GetViewDataForBreakpoint() as List<DeviceGroupingViewData>;
                    string ItemName = ds.Find(x => x.Id == templateListing.deleteID).GroupName;
                    ltrdiagDeleteHeader.Text = string.Format(GetLocalResourceObject("diagDel_header").ToString(), ItemName);

                    ViewState["DeleteItem"] = "breakpoint|" + breakpointId.ToString();
                    pnlDeleteListHolder.Controls.Add(templateListing);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "openDeleteDiag", "Ektron.ready(function(event, eventName){openDeleteDialog();});", true);
                }
            }
        }

        protected void BreakPoint_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item != null)
            {
                ListViewItem item = e.Item;

                int widthBegin = 0;

                // Get the lower bound device width 
                Label breakpointInfo = item.FindControl("DeviceGroupInfo") as Label;
                string widthInfo = breakpointInfo.Text;
                if (widthInfo.Length > 0)
                {
                    widthInfo = widthInfo.Substring(0, widthInfo.IndexOf(" <"));
                    Int32.TryParse(widthInfo, out widthBegin);
                }

                // Get preview devices for the breakpoint's width
                var uxWidth = item.FindControl("uxWidth") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField;

                Criteria<DevicePreviewProperty> previewCriteria = new Criteria<DevicePreviewProperty>();
                previewCriteria.AddFilter(DevicePreviewProperty.ViewportWidth, CriteriaFilterOperator.LessThanOrEqualTo, Int32.Parse(uxWidth.Text));
                previewCriteria.AddFilter(DevicePreviewProperty.ViewportWidth, CriteriaFilterOperator.GreaterThan, widthBegin);
                previewCriteria.OrderByField = DevicePreviewProperty.Model;

                List<DevicePreviewData> previews =
                    (from preview in this.previews.GetList(previewCriteria)
                     orderby preview.Brand == "Ektron" descending
                     select preview).ToList();

                // Populate the list of example devices
                Label phoneList = item.FindControl("phoneList") as Label;
                phoneList.Text = this.GetExampleDeviceList(previews.SkipWhile(d => d.Brand == "Ektron").ToList());

                // Bind the image list to preview devices (with a thumbnail)
                ListView phoneImgList = (ListView)item.FindControl("phoneImgList");
                List<DevicePreviewData> imgList = previews.FindAll(x => x.ThumbnailPath != "");
                if (imgList.Count > 0)
                {
                    phoneImgList.DataSource = imgList;
                    phoneImgList.DataBind();
                }
                else 
                {
                    string genericImgPath = "../MobileDevicePreview/Models/generic-sm.png";
                    LinkButton breakpointModel = item.FindControl("DeviceGroupName") as LinkButton;
                    DevicePreviewData defaultDevicePreviewData = new DevicePreviewData();
                    defaultDevicePreviewData.ThumbnailPath = genericImgPath;
                    defaultDevicePreviewData.EnabledPreview = true;
                    defaultDevicePreviewData.Model = breakpointModel.Text.ToString();

                    imgList.Add(defaultDevicePreviewData);
                    phoneImgList.DataSource = imgList;
                    phoneImgList.DataBind();
                }
            }
        }

        #endregion

        protected Unit GetLineBarWidth(object itemValue)
        {
            float ret = 0;

            if (this.WidthPixelSpec.Count > 0)
            {
                this.WidthPixelSpec.Sort();
                ret = float.Parse(itemValue.ToString()) / this.WidthPixelSpec[this.WidthPixelSpec.Count - 1] * 80; //scale it down
            }

            return Unit.Percentage(ret);
        }

        protected string GetPrePendWidthLabel(object itemValue)
        {
            string ret = string.Empty;

            if (this.WidthPixelSpec.Count > 0)
            {
                this.WidthPixelSpec.Sort();
                int idx = this.WidthPixelSpec.FindIndex(x => x == int.Parse(itemValue.ToString()));

                if (0 == idx)
                {
                    ret = "0 < ";
                }
                else if (idx > 0)
                {
                    ret = (this.WidthPixelSpec[idx - 1] + 1).ToString() + " < ";
                }
            }

            return ret;
        }

        protected string GetPreviewImg(object thumbnailPath)
        {
            string previewImagPath = thumbnailPath.ToString();

            if (0 == previewImagPath.IndexOf("WorkArea/"))
            {
                previewImagPath = "../" + previewImagPath.Substring(9);
            }

            return previewImagPath;
        }

        private void DataBindBreakPointsTable(List<DeviceGroupingViewData> lst)
        {
            uxBreakPoint.DataSource = lst;
            uxBreakPoint.DataBind();
        }

        private List<DeviceGroupingViewData> GetDeviceGroups(IEnumerable<DeviceBreakpointData> deviceBreakpoints)
        {
            List<DeviceGroupingViewData> deviceGroups = new List<DeviceGroupingViewData>();

            // Instantiate template-to-device mapping manager for template stats
            TemplateToDeviceGroupManager deviceTemplates = new TemplateToDeviceGroupManager();

            // Used to display the pixel range as previous to current (e.g. 0 to 320px)
            int previousBreakpointWidth = -1;

            foreach (DeviceBreakpointData breakpoint in deviceBreakpoints)
            {
                DeviceGroupingViewData group = new DeviceGroupingViewData(breakpoint);
                WidthPixelSpec.Add(breakpoint.Width);
                group.Width = breakpoint.Width;

                TemplateToDeviceGroupCriteria forBreakpointId = new TemplateToDeviceGroupCriteria();
                forBreakpointId.AddFilter(TemplateToDeviceGroupProperty.DeviceGroupID, CriteriaFilterOperator.EqualTo, group.Id);
                List<TemplateToDeviceGroupData> breakpointTemplates = deviceTemplates.GetList(forBreakpointId);

                group.NumOfTemplates = breakpointTemplates.Count;

                group.GroupInfo = string.Format("{0} {1} {2}px", previousBreakpointWidth + 1, GetLocalResourceObject("PixelRangeSeparator").ToString(), breakpoint.Width);
                previousBreakpointWidth = breakpoint.Width;

                deviceGroups.Add(group);
            }

            return deviceGroups;
        }

        private string GetExampleDeviceList(List<DevicePreviewData> devices)
        {
            if (devices.Count > 0)
            {
                return string.Format("{0} {1}",
                    GetLocalResourceObject("ExamplesPrefix").ToString(),
                    string.Join(", ",
                        (from device in devices
                         select device.Model)));
            }

            return string.Empty;
        }

        private List<DeviceGroupingViewData> GetViewDataForBreakpoint()
        {
            // Get all breakpoints ordered by their width
            DeviceBreakpointCriteria allByWidthAscending = new DeviceBreakpointCriteria();
            allByWidthAscending.OrderByField = DeviceBreakpointProperty.Width;
            allByWidthAscending.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            List<DeviceBreakpointData> allBreakPoints = breakpoints.GetList(allByWidthAscending);

            return this.GetDeviceGroups(allBreakPoints);
        }

        /// <summary>
        /// Validates the breakpoint widths for unique widths and updates the
        /// UI to reflect an error on duplicative widths.
        /// </summary>
        /// <returns>true if all breakpoint widths are valid; otherwise false.</returns>
        private bool ValidateBreakpointWidths()
        {
            List<DeviceBreakpointData> breakpoints = new List<DeviceBreakpointData>();

            foreach (ListViewItem item in uxBreakPoint.Items)
            {
                var uxWidth = item.FindControl("uxWidth") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField;
                uxWidth.CssClass = ResetValidation(uxWidth.CssClass);

                DeviceBreakpointData data = new DeviceBreakpointData()
                {
                    Id = Int64.Parse(((HiddenField)item.FindControl("DeviceGroupId")).Value),
                    Name = (item.FindControl("txtGroupName") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField).Value,
                    Height = int.Parse(uxWidth.Text),
                    Width = int.Parse(uxWidth.Text)
                };

                if (breakpoints.Count > 0 && breakpoints.Exists(x => x.Width == data.Width))
                {
                    // indicate the invalid width textboxes
                    uxWidth.CssClass += " ektron-ui-invalid";
                    for (int j = breakpoints.Count - 1; j >= 0; j--)
                    {
                        var uiDupWidth = uxBreakPoint.Items[j].FindControl("uxWidth") as Ektron.Cms.Framework.UI.Controls.EktronUI.TextField;
                        if (uiDupWidth.Text == data.Width.ToString())
                        {
                            uiDupWidth.CssClass += " ektron-ui-invalid";
                            break;
                        }
                    }

                    uxGroupDevicesMessage.Text = GetLocalResourceObject("SaveFail").ToString();
                    uxGroupDevicesMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    uxGroupDevicesMessage.Visible = true;

                    return false;
                }

                breakpoints.Add(data);
            }

            return true;
        }

        private void LoadValues()
        {
            this.WidthPixelSpec.Clear();

            List<DeviceGroupingViewData> lst = GetViewDataForBreakpoint();
            DataBindBreakPointsTable(lst);
        }

        private void RegisterResources()
        {
            Package BreakPointPackage = new Package()
            {
                Components = new List<Ektron.Cms.Framework.UI.Component>()
                {
                    Packages.Ektron.Namespace,
                    Packages.Ektron.CssFrameworkBase,
                    Packages.Ektron.Workarea.Core,
                    Ektron.Cms.Framework.UI.JavaScript.Create("js/jquery.jcarousel.min.js"),
                    Ektron.Cms.Framework.UI.Css.Create("css/jCarousel.css")
                }
            };

            BreakPointPackage.Register(this);
        }

        private string ResetValidation(string cssClass)
        {
            return cssClass.Replace("ektron-ui-invalid", "");
        }
    }
}