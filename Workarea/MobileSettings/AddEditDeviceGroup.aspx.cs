﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml;
    using Ektron.Cms.Framework.Device;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Interfaces.Context;

    public partial class AddEditDeviceGroup : Ektron.Cms.Workarea.Page
    {
        private enum UIModes
        {
            Model,
            Os
        };

        private const string SessionKey = "AddEditDeviceGroup.aspx.cs_CurrentList";
        private UIModes UIMode;
        private long DevGroupID = 0;
        private CmsDeviceConfiguration cDeviceMgr = null;
        private Ektron.Cms.Device.CmsDeviceConfigurationData devConfig = null;
        private List<string> CurrentSelectedOS = null;
        private List<ModelGroupUIData> CurrentSelectedModels = null;
        private List<string> Manufactures = null;
        private Ektron.Cms.Mobile.IDeviceInfoProvider wurflManager = null;
        private bool runTreeSelectionEvent = false;
        private string itemToUnselect = "";
        private Ektron.Cms.ContentAPI _contentApi;

        protected Ektron.Cms.ContentAPI ContentApi
        {
            get { return _contentApi ?? (_contentApi = new Ektron.Cms.ContentAPI()); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);


            treeCtrl.Click += new EventHandler<EventArgs>(treeCtrl_Click);
            treeCtrl.ItemDataBound += new EventHandler<Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.TreeItemDataBoundEventArgs<Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode>>(treeCtrl_ItemDataBound);

            mainGrid.RowCommand += new GridViewCommandEventHandler(mainGrid_RowCommand);

            lbtnSave.Click += new EventHandler(lbtnSave_Click);
            cDeviceMgr = new CmsDeviceConfiguration();
            wurflManager = Ektron.Cms.ObjectFactory.Get<Ektron.Cms.Mobile.IDeviceInfoProvider>();

            LoadManufactureList();

            if (!string.IsNullOrEmpty(Request.QueryString["DeviceGroupID"]))
            {
                long.TryParse(Request.QueryString["DeviceGroupID"], out DevGroupID);
                if (DevGroupID > 0)
                {
                    devConfig = cDeviceMgr.GetItem(DevGroupID);
                    if (devConfig.DisplayFor == 0)
                    {
                        UIMode = UIModes.Model;
                    }
                    else
                    {
                        UIMode = UIModes.Os;
                    }
                }
            }

            if (devConfig == null)
            {
                //Add group
                if (AddGroupType == "Model")
                {
                    UIMode = UIModes.Model;
                }
                else
                {
                    UIMode = UIModes.Os;
                }

                tfWidth.Text = "320";
                tfHeight.Text = "480";

            }
            SetUIText();
            LoadCurrent();
            LoadGridUI();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            LoadGridData();
            if (!Page.IsPostBack)
            {
                if (UIMode == UIModes.Os)
                {
                    populateTreeForOS();
                }
                LoadDetail();
            }
            SetHelpLink();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //populateTree(tfSearchBox.Text);

            mainGrid.DataBind();

            ICmsContextService cmsContext = ServiceFactory.CreateCmsContextService();
            Packages.jQuery.jQueryUI.ThemeRoller.Register(this);

            Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Common/commonTree.css");
            Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Folder/ektron-ui-folderTree.css");
        }

        protected void lbtnSave_Click(object sender, EventArgs e)
        {
            if (devConfig == null)
            {
                devConfig = new Ektron.Cms.Device.CmsDeviceConfigurationData();
            }
            devConfig.Name = tfName.Text;
            devConfig.Height = tfHeight.Value;
            devConfig.Width = tfWidth.Value;
            devConfig.Models = new List<string>();
            // TODO: Test with OS screen as well
            if (UIMode == UIModes.Model)
            {
                foreach (ModelGroupUIData selected in CurrentSelectedModels)
                {
                    devConfig.Models.Add(selected.Model);
                }
            }
            else
            {
                devConfig.Models = CurrentSelectedOS;
            }
            if (devConfig.Id > 0)
            {
                cDeviceMgr.Update(devConfig);
            }
            else
            {
                if (UIMode == UIModes.Os)
                {
                    devConfig.DisplayFor = 1;
                }
                cDeviceMgr.Add(devConfig);
            }
            Response.Redirect("DeviceGroupingView.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            populateTree(tfSearchBox.Text.Trim());
        }

        protected void treeCtrl_ItemDataBound(object sender, Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.TreeItemDataBoundEventArgs<Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode> e)
        {
            if (runTreeSelectionEvent && !string.IsNullOrEmpty(itemToUnselect))
            {
                string[] pair;
                pair = e.TreeNode.Id.Split('|');
                if (pair.Length == 3)
                {
                    if (pair[1] == itemToUnselect)
                    {
                        e.TreeNode.Selected = false;
                    }
                }
            }
            HideResultsGoHereMessage();
        }

        protected void treeCtrl_Click(object sender, EventArgs e)
        {

            Ektron.Cms.Framework.UI.Controls.EktronUI.Tree treeCtrl = (sender as Ektron.Cms.Framework.UI.Controls.EktronUI.Tree);
            if (treeCtrl.CurrentNode == null)
                return;
            bool selected = treeCtrl.CurrentNode.Selected;

            if (UIMode == UIModes.Model)
            {
                string[] deviceInfo;
                deviceInfo = treeCtrl.CurrentNode.Id.Split('|');
                if (deviceInfo.Length == 3)
                {
                    //Model clicked
                    if (selected)
                    {//add to grid if not exist
                        if (!CurrentSelectedModels.Exists(x => x.Model == deviceInfo[1]))
                        {
                            CurrentSelectedModels.Add(new ModelGroupUIData() { Manufacturer = deviceInfo[0], Model = deviceInfo[1], Name = deviceInfo[2] });
                        }
                    }
                    else
                    {//remove from grid if exist
                        ModelGroupUIData gridItem = CurrentSelectedModels.FirstOrDefault(x => x.Model == deviceInfo[1]);
                        if (gridItem != null)
                        {
                            CurrentSelectedModels.Remove(gridItem);
                        }
                    }
                    //Check parent
                    Ektron.Cms.Framework.UI.Tree.ITreeNode parent = treeCtrl.Items.Find(x => x.Id == treeCtrl.CurrentNode.ParentId);
                    if (parent != null && parent.Items != null)
                    {
                        bool isAllSelected = true;
                        foreach (Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode n in parent.Items)
                        {
                            if (!n.Selected)
                            {
                                isAllSelected = false;
                                break;
                            }
                        }
                        if (isAllSelected)
                        {
                            parent.Selected = true;
                        }
                        else
                        {
                            parent.Selected = false;
                        }
                    }


                }
                else if (deviceInfo.Length == 1)
                {
                    //Brand cliced
                    foreach (Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode n in treeCtrl.CurrentNode.Items)
                    {
                        n.Selected = selected;
                        string[] pairChildNode = n.Id.Split('|');
                        if (pairChildNode.Length == 3)
                        {
                            if (selected)
                            {
                                if (!CurrentSelectedModels.Exists(x => x.Model == pairChildNode[1]))
                                {
                                    CurrentSelectedModels.Add(new ModelGroupUIData() { Manufacturer = pairChildNode[0], Model = pairChildNode[1], Name = pairChildNode[2] });
                                }
                            }
                            else
                            {
                                ModelGroupUIData gridItem = CurrentSelectedModels.FirstOrDefault(x => x.Model == pairChildNode[1]);
                                if (gridItem != null)
                                {
                                    CurrentSelectedModels.Remove(gridItem);
                                }
                            }
                        }
                    }
                }


            }
            else
            {
                if (selected)
                {//add to grid if not exist
                    if (!CurrentSelectedOS.Exists(x => x == treeCtrl.CurrentNode.Id))
                    {
                        CurrentSelectedOS.Add(treeCtrl.CurrentNode.Id);
                    }
                }
                else
                {//remove from grid if exist
                    string gridItem = CurrentSelectedOS.FirstOrDefault(x => x == treeCtrl.CurrentNode.Id);
                    if (!string.IsNullOrEmpty(gridItem))
                    {
                        CurrentSelectedOS.Remove(gridItem);
                    }
                }
            }

        }

        protected void mainGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delItem")
            {
                if (UIMode == UIModes.Model)
                {
                    CurrentSelectedModels.Remove(CurrentSelectedModels.Find(x => x.Model == (string)e.CommandArgument));
                    //un-select from tree if selected
                    runTreeSelectionEvent = true;
                    itemToUnselect = (string)e.CommandArgument;
                }
                else
                {
                }
            }
        }

        protected string AddGroupType
        {
            get { return Request.QueryString["AddGroupType"]; }
        }

        protected void SetHelpLink()
        {
            var helpLink = AddGroupType.Equals("os", StringComparison.InvariantCultureIgnoreCase)
                               ? "CreateNewDeviceGroup_GroupDeviceByOS"
                               : "CreateNewDeviceGroup_GroupDeviceByModel";
            HelpLink.Attributes.Add("onclick", "window.open('" + ContentApi.fetchhelpLink(helpLink) + "', 'SitePreview', 'width=800,height=500,resizable=yes,toolbar=no,scrollbars=1,location=no,directories=no,status=no,menubar=no,copyhistory=no');return false;");
        }

        private void SetUIText()
        {
            if (UIMode == UIModes.Model)
            {
                pnlSearchArea.Visible = true;
                gridArea.Visible = true;
                uxTreeHeader.InnerText = GetLocalResourceObject("treeHeader_Model").ToString();
                ltrTitle.Text = GetLocalResourceObject("pageTitle_Model").ToString();
            }
            else
            {
                pnlSearchArea.Visible = false;
                gridArea.Visible = false;
                uxTreeHeader.InnerText = GetLocalResourceObject("treeHeader_Os").ToString();
                ltrTitle.Text = GetLocalResourceObject("pageTitle_OS").ToString();
            }

            uxGridHeader.InnerText = GetLocalResourceObject("gridtdHeader_Model").ToString();
        }

        private void LoadManufactureList()
        {
            Manufactures = new List<string>();
            XmlDocument xml = new XmlDocument();
            xml.Load(Server.MapPath("~/workarea/MobileSettings/ManagedLists/BrandsList.xml"));
            foreach (XmlNode n in xml.SelectNodes("/brands/brand"))
            {
                Manufactures.Add(n.Attributes["name"].Value);
            }
            Manufactures.Add("Other");
        }

        private void LoadCurrent()
        {
            if (Page.IsPostBack)
            {
                if (UIMode == UIModes.Model)
                {
                    CurrentSelectedModels = (List<ModelGroupUIData>)Session[SessionKey];
                }
                else
                {
                    CurrentSelectedOS = (List<string>)Session[SessionKey];
                }
            }
            else
            {
                Session[SessionKey] = null;
                if (UIMode == UIModes.Model)
                {
                    CurrentSelectedModels = new List<ModelGroupUIData>();
                    if (devConfig != null)
                    {
                        foreach (string Model in devConfig.Models)
                        {
                            ModelGroupUIData toAdd = new ModelGroupUIData();
                            toAdd.Model = Model;
                            Ektron.Cms.Mobile.EkDeviceInfo devInf = wurflManager.GetDeviceInfoByModel(Model);
                            if (devInf != null)
                            {
                                toAdd.Manufacturer = devInf.DeviceBrandName;
                                toAdd.Name = devInf.DeviceName;
                            }
                            CurrentSelectedModels.Add(toAdd);
                        }
                    }
                    Session[SessionKey] = CurrentSelectedModels;
                }
                else
                {
                    CurrentSelectedOS = new List<string>();
                    if (devConfig != null)
                    {
                        CurrentSelectedOS = devConfig.Models;
                    }
                    Session[SessionKey] = CurrentSelectedOS;
                }
            }
        }

        private void LoadDetail()
        {
            if (devConfig != null)
            {
                tfName.Text = devConfig.Name;
                tfWidth.Value = (int)devConfig.Width;
                tfHeight.Value = (int)devConfig.Height;
            }
        }

        private void LoadGridUI()
        {
            if (UIMode == UIModes.Model)
            {
                //Model
                BoundField colBrand = new BoundField();
                colBrand.DataField = "Manufacturer";
                colBrand.HeaderText = GetLocalResourceObject("gridHeader_Manufacture").ToString();
                BoundField colModel = new BoundField();
                colModel.DataField = "Name";
                colModel.HeaderText = GetLocalResourceObject("gridHeader_Model").ToString();

                mainGrid.Columns.Add(colBrand);
                mainGrid.Columns.Add(colModel);
            }
            else
            {
                //OS
            }
        }

        private void LoadGridData()
        {
            mainGrid.EktronUIPagingInfo = new Ektron.Cms.PagingInfo();
            mainGrid.EktronUIPagingInfo.CurrentPage = 1;
            if (UIMode == UIModes.Model)
            {
                mainGrid.DataSource = CurrentSelectedModels;
                mainGrid.EktronUIPagingInfo.TotalRecords = CurrentSelectedModels.Count;
            }
            else
            {
                //mainGrid.DataSource = CurrentSelectedOS;
                //mainGrid.EktronUIPagingInfo.TotalRecords = CurrentSelectedOS.Count;
            }
        }

        private void HideResultsGoHereMessage()
        {
            msgResultShowHere.Visible = false;
        }

        private void ShowNoDevicesFoundMessage()
        {
            msgNoResult.Visible = true;
            HideResultsGoHereMessage();
        }

        private void populateTree(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                treeCtrl.Visible = false;
                return;
            }

            List<Ektron.Cms.Mobile.EkDeviceInfo> DeviceSearchResult = wurflManager.SearchForDevices(searchText);
            DeviceSearchResult.RemoveAll(x => x.DeviceModel.ToLower().Contains("http://") || x.DeviceModel.ToLower().Contains("_"));

            //Manufactures = new List<string>();
            //Manufactures = DeviceSearchResult.ConvertAll<string>(new Converter<Ektron.Cms.Mobile.EkDeviceInfo, string>(delegate(Ektron.Cms.Mobile.EkDeviceInfo src) { return src.DeviceBrandName; })).Distinct().ToList();

            treeCtrl.Visible = true;
            if (DeviceSearchResult.Count == 0)
            {
                ShowNoDevicesFoundMessage();
            }

            var toBind = new List<Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode>();
            foreach (string brand in Manufactures)
            {
                var nodeBrand = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode();
                nodeBrand.Id = brand;
                nodeBrand.Text = brand;
                nodeBrand.CanHaveChildren = true;
                nodeBrand.SelectionMode = Ektron.Cms.Framework.UI.Tree.TreeSelectionMode.MultiForAll;
                //nodeBrand.ClickCausesPostback = true;
                List<Ektron.Cms.Mobile.EkDeviceInfo> devices = null;
                if (brand != "Other")
                {
                    devices = DeviceSearchResult.FindAll(x => string.Compare(x.DeviceBrandName, brand, StringComparison.OrdinalIgnoreCase) == 0);
                }
                else
                {
                    devices = DeviceSearchResult.FindAll(x => !Manufactures.Contains(x.DeviceBrandName));
                }
                nodeBrand.Type = "brand";
                if (devices.Count > 0)
                {
                    nodeBrand.HasChildren = true;
                    foreach (Ektron.Cms.Mobile.EkDeviceInfo dev in devices)
                    {
                        if (dev.DeviceModel.ToLower().StartsWith("http") || dev.DeviceModel.Contains("_") || string.IsNullOrEmpty(dev.DeviceModel.Trim()))
                        {
                            continue;
                        }

                        var nodeDevice = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode();
                        nodeDevice.Id = brand + "|" + dev.DeviceModel + "|" + dev.DeviceName;
                        nodeDevice.Text = dev.DeviceName;
                        nodeDevice.SelectionMode = Ektron.Cms.Framework.UI.Tree.TreeSelectionMode.MultiForAll;
                        nodeDevice.Type = "device";
                        nodeDevice.Selected = CurrentSelectedModels.Exists(x => string.Compare(x.Model, dev.DeviceModel, StringComparison.Ordinal) == 0);
                        nodeDevice.ParentId = nodeBrand.Id;
                        nodeBrand.Items.Add(nodeDevice);
                    }
                }
                if (nodeBrand.Items.Count > 0)
                    toBind.Add(nodeBrand);
            }
            treeCtrl.DataSource = toBind;
            treeCtrl.DataBind();
        }

        private void populateTreeForOS()
        {
            List<string> allOs = wurflManager.GetAvaliableOs();
            var toBind = new List<Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode>();
            foreach (string Os in allOs)
            {
                var nodeOs = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode();
                nodeOs.Id = Os;
                nodeOs.Text = Os;
                nodeOs.CanHaveChildren = true;
                nodeOs.SelectionMode = Ektron.Cms.Framework.UI.Tree.TreeSelectionMode.MultiForAll;
                nodeOs.ClickCausesPostback = true;
                nodeOs.Selected = CurrentSelectedOS.Exists(x => string.Compare(Os, x, StringComparison.OrdinalIgnoreCase) == 0);
                toBind.Add(nodeOs);
            }
            treeCtrl.DataSource = toBind;
            treeCtrl.DataBind();
        }

        internal class ModelGroupUIData
        {
            public string Manufacturer { get; set; }
            public string Model { get; set; }
            public string Name { get; set; }
        }
    }
}