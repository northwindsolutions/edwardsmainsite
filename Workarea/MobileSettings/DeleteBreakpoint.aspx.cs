﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.Settings.Mobile;
    using Ektron.Cms.Settings.Mobile;
    using Ektron.Cms;
    using Ektron.Cms.Common;

    public partial class DeleteBreakpoint : Ektron.Cms.Workarea.Page
    {
        private DeviceBreakpointManager manager = null;
        private DeviceBreakpointData toDelete = null;
        private TemplateToDeviceGroupManager templateBrkMgr = null;
        private ITemplateManager tmpMgr = null;
        
        long deleteID = 0;
        protected override void OnInit(EventArgs e)
        {
            
            base.OnInit(e);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            btnDelete.Click += new EventHandler(btnDelete_Click);
            manager = new DeviceBreakpointManager();
            templateBrkMgr = new TemplateToDeviceGroupManager();
            tmpMgr = ObjectFactory.GetTemplateManager();
            if (!string.IsNullOrEmpty(Request["Id"]))
            {
                deleteID = long.Parse(Request["Id"]);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            toDelete = manager.GetItem(deleteID);
            TemplateToDeviceGroupCriteria criteria = new TemplateToDeviceGroupCriteria();
            criteria.AddFilter(TemplateToDeviceGroupProperty.DeviceGroupID, CriteriaFilterOperator.EqualTo, toDelete.Id);
            List<TemplateToDeviceGroupData> mapped = templateBrkMgr.GetList(criteria);
            rptTemplateMapping.DataSource = mapped.ConvertAll<BindingItem>(new Converter<TemplateToDeviceGroupData, BindingItem>(getBinding));

            ancBack.HRef = "AddEditBreakPoint.aspx?Id=" + Request["Id"];

            string msg = "Are you sure you want to delete the breakpoint: " + toDelete.Name + "<br/>Currently associated template(s) ";
            msgBox.ContentTemplate = new MessageTemplate(msg);
            msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Warning;

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            rptTemplateMapping.DataBind();
        }
        BindingItem getBinding(TemplateToDeviceGroupData s)
        {
            return new BindingItem(s, tmpMgr.GetItem(s.TemplateId).FileName);
        }
        void btnDelete_Click(object sender, EventArgs e)
        {
            manager.Delete(deleteID);
            Response.Redirect("ViewDeviceBreakpoint.aspx");
        }

        internal class BindingItem : TemplateToDeviceGroupData
        {
            public string TemplateSourceFile { get; set; }
            public BindingItem(TemplateToDeviceGroupData source, string TemplateSourceFile)
            {
                this.DeviceGroupID = source.DeviceGroupID;
                this.Enabled = source.Enabled;
                this.TemplateId = source.TemplateId;
                this.TemplateFileName = source.TemplateFileName;
                this.TemplateSourceFile = TemplateSourceFile;
            }
        }
        internal class MessageTemplate : ITemplate
        {
            public MessageTemplate() { }
            public MessageTemplate(string MsgText)
            {
                MessageTest = MsgText;
            }
            public string MessageTest { get; set; }
            public void InstantiateIn(Control container)
            {
                Literal ltr = new Literal();
                ltr.ID = "lterMsgText";
                ltr.Text = MessageTest;
                container.Controls.Add(ltr);
            }
        }
    }
}