﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceGroupTemplateListingCtrl.ascx.cs" Inherits="Workarea.MobileSettings.DeviceGroupTemplateListingCtrl" %>
<asp:Repeater runat="server" ID="rptTemplateMapping">
                    <HeaderTemplate>
                    <table  class="ektronGrid"  style="  margin:5px; border:1px solid #d4d4d4;">
                            <tr>
                                <td><asp:Literal runat="server" Text="<%$Resources:header_Id %>" /></td>
                                <td><asp:Literal runat="server" Text="<%$Resources:header_temp_filename %>" /></td>
                                <td><asp:Literal runat="server" Text="<%$Resources:header_group_target %>" /></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td><%#Eval("TemplateId")%></td>
                                <td>
                                    
                                    <asp:HyperLink runat="server" ID="hlViewTemplate" NavigateUrl='<%#string.Format("~/WorkArea/template_config.aspx?view=update&id={0}",Eval("TemplateId")) %>' Text='<%#Eval("TemplateSourceFile")%>' />
                                </td>
                                <td><%#Eval("TemplateFileName")%></td>
                            </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                    </table>
                    </FooterTemplate>
                </asp:Repeater>