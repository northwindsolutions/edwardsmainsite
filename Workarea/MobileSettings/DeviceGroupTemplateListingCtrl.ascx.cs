﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.Settings.Mobile;
    using Ektron.Cms.Settings.Mobile;
    using Ektron.Cms.Common;
    using Ektron.Cms;

    public partial class DeviceGroupTemplateListingCtrl : System.Web.UI.UserControl
    {
        private DeviceBreakpointManager manager = null;
        
        private TemplateToDeviceGroupManager templateBrkMgr = null;
        private ITemplateManager tmpMgr = null;

        public long deleteID
        {
            get;
            set;
        }
        public bool isBreakpoint { get; set; }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
        }
        BindingItem getBinding(TemplateToDeviceGroupData s)
        {
            return new BindingItem(s, tmpMgr.GetItem(s.TemplateId).FileName);
        }

        public void LoadData()
        {
            manager = new DeviceBreakpointManager();
            templateBrkMgr = new TemplateToDeviceGroupManager();
            tmpMgr = ObjectFactory.GetTemplateManager();
            TemplateToDeviceGroupCriteria criteria = new TemplateToDeviceGroupCriteria();
            criteria.AddFilter(TemplateToDeviceGroupProperty.DeviceGroupID, CriteriaFilterOperator.EqualTo, deleteID);
            List<TemplateToDeviceGroupData> mapped ;
            if (isBreakpoint)
                mapped = templateBrkMgr.GetList(criteria);
            else
                mapped = templateBrkMgr.GetListModelOs(criteria);
            rptTemplateMapping.DataSource = mapped.ConvertAll<BindingItem>(new Converter<TemplateToDeviceGroupData, BindingItem>(getBinding));
            rptTemplateMapping.DataBind();
        }
        internal class BindingItem : TemplateToDeviceGroupData
        {
            public string TemplateSourceFile { get; set; }
            public BindingItem(TemplateToDeviceGroupData source, string TemplateSourceFile)
            {
                this.DeviceGroupID = source.DeviceGroupID;
                this.Enabled = source.Enabled;
                this.TemplateId = source.TemplateId;
                this.TemplateFileName = source.TemplateFileName;
                this.TemplateSourceFile = TemplateSourceFile;
            }
        }
    }
}