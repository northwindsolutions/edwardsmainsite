﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.Device;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.Mobile;
    using Ektron.Cms.Settings.Mobile;
    using Ektron.Cms.Mobile;

    public partial class DeviceGroupingView : Ektron.Cms.Workarea.Page
    {
        private IDeviceInfoProvider deviceManager = null;
        private DeviceBreakpointManager breakpointMgr = null;
        private CmsDeviceConfiguration cDeviceMgr = null;
        private TemplateToDeviceGroupManager templateDevMgr = null;
        private Ektron.Cms.ContentAPI _ContentApi;
        private IDevicePreviewManager previewManager = null;

        protected Ektron.Cms.ContentAPI ContentApi
        {
            get { return _ContentApi ?? (_ContentApi = new Ektron.Cms.ContentAPI()); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            breakpointMgr = new DeviceBreakpointManager();
            cDeviceMgr = new CmsDeviceConfiguration();
            templateDevMgr = new TemplateToDeviceGroupManager();
            deviceManager = Ektron.Cms.ObjectFactory.Get<IDeviceInfoProvider>();
            previewManager = Ektron.Cms.ObjectFactory.Get<IDevicePreviewManager>();

            gvDeviceGroup.RowCommand += new GridViewCommandEventHandler(gvDeviceGroup_RowCommand);
            gvBreakpoint.RowCommand += new GridViewCommandEventHandler(gvBreakpoint_RowCommand);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Package MenuCss = new Package()
            {
                Components = new List<Component>()
                {
                    Css.Create(ResolveUrl("~/workarea/wamenu/css/com.ektron.ui.menu.css")),
                }
            };
            MenuCss.Register(this);
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            switch (rblAddNew.SelectedValue)
            {
                case "breakpoint":
                    Response.Redirect("AddEditBreakPoint.aspx");
                    break;
                case "os":
                    Response.Redirect("AddEditDeviceGroup.aspx?AddGroupType=Os");
                    break;
                case "model":
                    Response.Redirect("AddEditDeviceGroup.aspx?AddGroupType=Model");
                    break;
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ViewState["DeleteItem"] != null)
            {
                string[] pair = ViewState["DeleteItem"].ToString().Split('|');
                if (pair.Length == 2)
                {
                    if (pair[0] == "breakpoint")
                    {

                        breakpointMgr.Delete(long.Parse(pair[1]));
                    }
                    else
                    {
                        cDeviceMgr.Delete(long.Parse(pair[1]));
                    }

                }
            }
            LoadGrids();
        }
        protected void gvBreakpoint_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            DeviceGroupTemplateListingCtrl templateListing = LoadControl("DeviceGroupTemplateListingCtrl.ascx") as DeviceGroupTemplateListingCtrl;
            templateListing.deleteID = long.Parse(e.CommandArgument.ToString());
            templateListing.isBreakpoint = true;
            templateListing.LoadData();

            List<DeviceGroupingViewData> ds = gvBreakpoint.DataSource as List<DeviceGroupingViewData>;
            string ItemName = ds.Find(x => x.Id == templateListing.deleteID).GroupName;
            ltrdiagDeleteHeader.Text = string.Format(GetLocalResourceObject("diagDel_header").ToString(), ItemName);

            if (e.CommandName == "delItem")
            {
                ViewState["DeleteItem"] = "breakpoint|" + e.CommandArgument.ToString();
                pnlDeleteListHolder.Controls.Add(templateListing);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "openDeleteDiag", "Ektron.ready(function(event, eventName){openDeleteDialog();});", true);
            }
            if (e.CommandName == "viewTemplates")
            {
                pnlViewListHolder.Controls.Add(templateListing);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "openViewDiag", "Ektron.ready(function(event, eventName){openViewDialog();});", true);
            }
        }
        protected void gvDeviceGroup_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delItem" || e.CommandName == "viewTemplates")
            {
                DeviceGroupTemplateListingCtrl templateListing = LoadControl("DeviceGroupTemplateListingCtrl.ascx") as DeviceGroupTemplateListingCtrl;
                templateListing.deleteID = long.Parse(e.CommandArgument.ToString());
                templateListing.isBreakpoint = false;
                templateListing.LoadData();
                List<DeviceGroupingViewData> ds = gvDeviceGroup.DataSource as List<DeviceGroupingViewData>;
                string ItemName = ds.Find(x => x.Id == templateListing.deleteID).GroupName;
                ltrdiagDeleteHeader.Text = string.Format(GetLocalResourceObject("diagDel_header").ToString(), ItemName);

                if (e.CommandName == "delItem")
                {
                    ViewState["DeleteItem"] = "modelos|" + e.CommandArgument.ToString();
                    pnlDeleteListHolder.Controls.Add(templateListing);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "openDeleteDiag", "Ektron.ready(function(event, eventName){openDeleteDialog();});", true);
                }

                if (e.CommandName == "viewTemplates")
                {
                    pnlViewListHolder.Controls.Add(templateListing);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "openViewDiag", "Ektron.ready(function(event, eventName){openViewDialog();});", true);
                }
            }
            if (e.CommandName.StartsWith("reorder"))
            {
                Ektron.Cms.Device.CmsDeviceConfigurationCriteria criteria = new Ektron.Cms.Device.CmsDeviceConfigurationCriteria();
                criteria.AddFilter(Ektron.Cms.Device.CmsDeviceConfigurationProperty.Order, CriteriaFilterOperator.GreaterThan, -1);//Exclude the two defaults
                List<Ektron.Cms.Device.CmsDeviceConfigurationData> cDeviceList;
                criteria.OrderByField = Ektron.Cms.Device.CmsDeviceConfigurationProperty.Order;
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                cDeviceList = cDeviceMgr.GetList(criteria);

                Ektron.Cms.Device.CmsDeviceConfigurationData itemMoved = cDeviceList.Find(x => x.Id == long.Parse(e.CommandArgument.ToString()));
                int Itemindex = cDeviceList.IndexOf(itemMoved);
                if (e.CommandName == "reorderUP")
                {
                    int targetOrder = (int)cDeviceList[Itemindex - 1].Order;
                    cDeviceList[Itemindex - 1].Order = itemMoved.Order;
                    itemMoved.Order = itemMoved.Order-1;
                }
                if (e.CommandName == "reorderDOWN")
                {
                    int targetOrder = (int)cDeviceList[Itemindex + 1].Order;
                    cDeviceList[Itemindex + 1].Order = itemMoved.Order;
                    itemMoved.Order = itemMoved.Order+1;
                }
                cDeviceMgr.Reorder(cDeviceList);
                LoadGrids();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            LoadGrids();
            SetHelpLink();
        }

        protected void SetHelpLink()
        {
            HelpLink.Attributes.Add("onclick", "window.open('" + ContentApi.fetchhelpLink("ViewAllDeviceGroups") + "', 'SitePreview', 'width=800,height=500,resizable=yes,toolbar=no,scrollbars=1,location=no,directories=no,status=no,menubar=no,copyhistory=no');return false;");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // Bind and show/hide the device group section
            gvDeviceGroup.DataBind();

            // Set custom mark up for Generic Mobile row.
            SetGenericMobileRowVal(gvDeviceGroup);
            
            this.uxDeviceGroups.Visible = gvDeviceGroup.Rows.Count > 0;

            // Bind and show/hide the breakpoint group section
            gvBreakpoint.DataBind();
            this.uxBreakpointGroups.Visible = gvBreakpoint.Rows.Count > 0;
        }
        private void LoadGrids()
        {
            List<DeviceGroupingViewData> toBindDevice = GetViewDataForDeviceGroup();
            gvDeviceGroup.DataSource = toBindDevice;
            gvDeviceGroup.EktronUIPagingInfo = new Ektron.Cms.PagingInfo();
            gvDeviceGroup.EktronUIPagingInfo.TotalPages = 1;
            gvDeviceGroup.EktronUIPagingInfo.CurrentPage = 1;
            gvDeviceGroup.EktronUIPagingInfo.TotalRecords = toBindDevice.Count;

            List<DeviceGroupingViewData> toBindBreakpoint = GetViewDataForBreakpoint();
            gvBreakpoint.DataSource = toBindBreakpoint;
            gvBreakpoint.EktronUIPagingInfo = new Ektron.Cms.PagingInfo();
            gvBreakpoint.EktronUIPagingInfo.TotalPages = 1;
            gvBreakpoint.EktronUIPagingInfo.CurrentPage = 1;
            gvBreakpoint.EktronUIPagingInfo.TotalRecords = toBindBreakpoint.Count;
        }
        private List<DeviceGroupingViewData> GetViewDataForDeviceGroup()
        {
            List<DeviceGroupingViewData> ret = new List<DeviceGroupingViewData>();
            Ektron.Cms.Device.CmsDeviceConfigurationCriteria criteria = new Ektron.Cms.Device.CmsDeviceConfigurationCriteria();
            List<Ektron.Cms.Device.CmsDeviceConfigurationData> cDeviceList;
            criteria.OrderByField = Ektron.Cms.Device.CmsDeviceConfigurationProperty.Order;
            criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            cDeviceList = cDeviceMgr.GetList(criteria);

            foreach (Ektron.Cms.Device.CmsDeviceConfigurationData item in cDeviceList)
            {
                if (item.Order < 0 || item.Name == "Generic Mobile")
                    continue;
                DeviceGroupingViewData toAdd = new DeviceGroupingViewData(item);

                TemplateToDeviceGroupCriteria tcCrti = new TemplateToDeviceGroupCriteria();
                tcCrti.AddFilter(TemplateToDeviceGroupProperty.DeviceGroupID, CriteriaFilterOperator.EqualTo, toAdd.Id);
                List<TemplateToDeviceGroupData> mapped = templateDevMgr.GetListModelOs(tcCrti);
                toAdd.NumOfTemplates = mapped.Count();
                ret.Add(toAdd);
            }

            // Bind Device configuration item separately because it needs to be in the bottom and not sortable.
            DeviceGroupingViewData genericModel = new DeviceGroupingViewData(cDeviceList.Find(item => item.Name == "Generic Mobile"));

            TemplateToDeviceGroupCriteria genericTempelateCri = new TemplateToDeviceGroupCriteria();
            genericTempelateCri.AddFilter(TemplateToDeviceGroupProperty.DeviceGroupID, CriteriaFilterOperator.EqualTo, genericModel.Id);
            List<TemplateToDeviceGroupData> genericMapped = templateDevMgr.GetListModelOs(genericTempelateCri);
            genericModel.NumOfTemplates = genericMapped.Count();
            ret.Add(genericModel);
            return ret;
            // Binding Generic mobile configuration ends.
        }
        private List<DeviceGroupingViewData> GetViewDataForBreakpoint()
        {
            List<DeviceGroupingViewData> ret = new List<DeviceGroupingViewData>();
            DeviceBreakpointCriteria breakpointCriteria = new DeviceBreakpointCriteria();
            breakpointCriteria.OrderByField = DeviceBreakpointProperty.Width;
            breakpointCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            List<DeviceBreakpointData> listBreakPoints = breakpointMgr.GetList(breakpointCriteria);


            foreach (DeviceBreakpointData item in listBreakPoints)
            {
                DeviceGroupingViewData toAdd = new DeviceGroupingViewData(item);

                TemplateToDeviceGroupCriteria tcCrti = new TemplateToDeviceGroupCriteria();
                tcCrti.AddFilter(TemplateToDeviceGroupProperty.DeviceGroupID, CriteriaFilterOperator.EqualTo, toAdd.Id);
                List<TemplateToDeviceGroupData> mapped = templateDevMgr.GetList(tcCrti);

                toAdd.NumOfTemplates = mapped.Count;
                int startWdith = 0;
                int endWidth = 0;
                if (listBreakPoints.IndexOf(item) == 0)
                {
                    startWdith = 0;
                    endWidth = item.Width;
                }
                else
                {

                    DeviceBreakpointData preItem = listBreakPoints[listBreakPoints.IndexOf(item) - 1];
                    startWdith = preItem.Width + 1;
                    endWidth = item.Width;
                }
                toAdd.GroupInfo = string.Format("{0} to {1}px", startWdith.ToString(), item.Width.ToString());
                List<string> samples = previewManager.GetList(item.Id).OrderBy(p => p.Model).ToList().FindAll(d => d.Brand != "Ektron").ConvertAll(new Converter<DevicePreviewData, string>(delegate(DevicePreviewData srcIn) { return srcIn.Model; }));
                toAdd.ExampleDevice = string.Join(", ", samples.ToArray());

                ret.Add(toAdd);
            }
            return ret;
        }

        /// <summary>
        /// Since the Default Generic Mobile item can not be deleted or edited, it needs special binding.
        /// </summary>
        /// <param name="deviceGroupView"></param>
        protected void SetGenericMobileRowVal(GridView deviceGroupView)
        {
            foreach (GridViewRow row in deviceGroupView.Rows)
            {
                if (deviceGroupView.Rows.Count - 1 == row.RowIndex)
                {
                    LinkButton orderUp = (LinkButton)row.FindControl("lbreOrderUp");
                    ImageButton delImage = (ImageButton)row.FindControl("lbDeleteItem");
                    delImage.Visible = false;
                    orderUp.Visible = false;

                    // Since the generic device group name is default and not a device model, 
                    //its value is default and not the model.
                    row.Cells[3].Text = "Default";

                    // Since the Device Group Name for Generic Mobile can not be edited or deleted, 
                    //it should not be a link, hence it is static text.
                    row.Cells[4].Text = "Generic Mobile";
                }
                if (deviceGroupView.Rows.Count - 2 == row.RowIndex)
                {
                    LinkButton orderDown = (LinkButton)row.FindControl("lbreOrderDown");
                    orderDown.Visible = false;
                }
            }
        }
        internal class DeviceGroupingViewData
        {
            public enum GroupTypes
            {
                Model = 0,
                OS = 1,
                Size = 2
            }

            public long Id { get; set; }
            public GroupTypes GroupType { get; set; }
            public string GroupTypeText
            {
                get
                {
                    return this.GroupType.ToDescriptionString();
                }
            }
            public string GroupName { get; set; }
            public string GroupInfo { get; set; }
            public int NumOfTemplates { get; set; }
            public string ExampleDevice { get; set; }
            public DeviceGroupingViewData(DeviceBreakpointData src)
            {
                this.GroupType = GroupTypes.Size;
                this.GroupName = src.Name;
                this.GroupInfo = "";
                this.Id = src.Id;
                this.NumOfTemplates = 0;

            }
            public DeviceGroupingViewData(Ektron.Cms.Device.CmsDeviceConfigurationData src)
            {
                if (src.DisplayFor == 0)
                    this.GroupType = GroupTypes.Model;
                else if (src.DisplayFor == 1)
                    this.GroupType = GroupTypes.OS;

                this.GroupName = src.Name;
                this.GroupInfo = string.Join(", ", src.Models.ToArray());
                this.Id = src.Id;
                this.NumOfTemplates = 0;
            }


        }
    }
}