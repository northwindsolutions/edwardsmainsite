﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.Settings.Mobile;
    using Ektron.Cms.Settings.Mobile;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

    public partial class ViewDeviceBreakpoint : Ektron.Cms.Workarea.Page
    {
        DeviceBreakpointManager m=null;
        DeviceBreakpointCriteria c=null;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Package MenuCss = new Package()
            {
                Components = new List<Component>()
                {
                    
                    // CSS
                    Css.Create(ResolveUrl("~/workarea/wamenu/css/com.ektron.ui.menu.css")),
                }
            };
            MenuCss.Register(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            m = new DeviceBreakpointManager();
            c = new DeviceBreakpointCriteria();
            if (!Page.IsPostBack)
            {
                c.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending;
                c.OrderByField = DeviceBreakpointProperty.Id;

                List<DeviceBreakpointData> lst = m.GetList(c);
                mainGrid.EktronUIOrderByFieldText = c.OrderByField.ToString();
                mainGrid.EktronUIOrderByDirection = c.OrderByDirection;
                mainGrid.EktronUIPagingInfo = c.PagingInfo;
                mainGrid.DataSource = lst;
            }
            
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            mainGrid.DataBind();
        }
        protected void EkGrid_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            mainGrid.EktronUIPagingInfo = c.PagingInfo = e.PagingInfo;
            mainGrid.DataSource = m.GetList(c);
        }
        protected void EkGrid_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            c.OrderByField = (DeviceBreakpointProperty)Enum.Parse(typeof(DeviceBreakpointProperty), e.OrderByFieldText);
            c.OrderByDirection = e.OrderByDirection;
            
            mainGrid.DataSource = m.GetList(c);
            mainGrid.EktronUIOrderByFieldText = e.OrderByFieldText;
            mainGrid.EktronUIPagingInfo = c.PagingInfo;
        }
    }
}