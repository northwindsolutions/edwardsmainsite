﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WURFLUpdate.ascx.cs" Inherits="Workarea.MobileSettings.WURFLUpdate" %>
<div class="WURFLUpdateDiv">
    <asp:Panel runat="server" ID="pnlUpdateMSG">
        <div id="wurflUpdateCtrl_msgUpdateMessage" class="ektron-ui-control ui-corner-all ektron-ui-message ui-state-highlight ektron-ui-warning ektron-ui-clearfix">
                        <span id="wurflUpdateCtrl_msgUpdateMessage_Message_aspIcon" class="ui-icon ektron-ui-sprite ui-icon-alert ektron-ui-sprite-warning"></span>
                        <div class="ektron-ui-clearfix ektron-ui-messageBody">
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:updateMessage %>" />
                            <asp:Button runat="server" ID="Button1" OnClick="btnUpdate_Click" Text="<%$Resources:btnUpdate %>" />
                        </div>
                    </div>
        

    </asp:Panel>
    <div class="wurflUpdateStatus" style="text-align: right;">
        <asp:Literal ID="ltrDateUpdated" runat="server" />
    </div>
</div>
