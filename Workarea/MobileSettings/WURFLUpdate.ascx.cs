﻿namespace Workarea.MobileSettings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Common;
    using Ektron.Cms;
    using System.IO;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Interfaces.Context;

    public partial class WURFLUpdate : System.Web.UI.UserControl
    {
        protected EkMessageHelper _MessageHelper;
        protected ContentAPI _ContentApi = new ContentAPI();
        protected CommonApi _CommonApi = new CommonApi();
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();    
            Package interactionPackage = new Package()
            {
                Components = new List<Ektron.Cms.Framework.UI.Component>()
                {
                    Packages.jQuery.jQueryUI.ThemeRoller,
                    Packages.Ektron.CssFrameworkBase,
                    Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-message.css")
                }
            };
            interactionPackage.Register(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _MessageHelper = _ContentApi.EkMsgRef;
            LoadLocalFileDT();
            View();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string result = _CommonApi.UpdateWurflFile();
            if (result == "sucessful")
            {
                pnlUpdateMSG.Visible = false;
            }
            else if (result == "already updated")
            {
                pnlUpdateMSG.Visible = false;
            }
            else
            {

            }
            LoadLocalFileDT();
        }
        private void View()
        {
            
            
            //if (fInfo.Exists)
            if(_CommonApi.WURFLCheckUpdate()==0)
            {
                pnlUpdateMSG.Visible = true;
            }
            else if (_CommonApi.WURFLCheckUpdate() == 1)
            {
                pnlUpdateMSG.Visible = false;
            }
            else
            {
                pnlUpdateMSG.Visible = false;
            }
            //hlUpdate.NavigateUrl = _ContentApi.ApplicationPath + "updatewurflfile.aspx?action=update";
            //mvUpdateFile.SetActiveView(vView);
        }

        private void LoadLocalFileDT()
        {
            FileInfo fInfo = new FileInfo(Server.MapPath("~/App_Data/wurfl.zip"));
            ltrDateUpdated.Text = _MessageHelper.GetMessage("lbl device list update") + fInfo.LastWriteTime + ". ";
        }
        internal class MessageTempalte : ITemplate
        {
            private string StrMessage { get; set; }
            private string BtnText { get; set; }
            public delegate void ButtonEventHanlder(object sender, EventArgs e);
            public ButtonEventHanlder theHandler { get; set; }
            public MessageTempalte(string message,string btnText)
            {
                StrMessage = message;
                BtnText = btnText;
            }
            public void InstantiateIn(Control container)
            {
                Literal ltr = new Literal();
                ltr.Text = StrMessage;
                container.Controls.Add(ltr);

                Ektron.Cms.Framework.UI.Controls.EktronUI.Button btn = new Ektron.Cms.Framework.UI.Controls.EktronUI.Button();
                btn.ID = "btnUpdate";
                btn.Click += new EventHandler(btn_Click);
                btn.Text = BtnText;
                container.Controls.Add(btn);
            }

            void btn_Click(object sender, EventArgs e)
            {
                theHandler(sender, e);
            }
        }
    }
}