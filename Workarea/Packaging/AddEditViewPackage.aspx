﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditViewPackage.aspx.cs"
    Inherits="Ektron.Workarea.Packaging.AddEditViewPackage" meta:resourcekey="page" %>

<%@ Register Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI"
    TagPrefix="ektronUI" %>
<%@ Register Src="usercontrols/FolderAndContentSelector.ascx" TagName="FolderAndContentSelector"
    TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/LibraryFilesSelector.ascx" TagName="LibraryFilesSelector"
    TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/FileSystemSelector.ascx" TagName="FileSystemSelector"
    TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/PackageDefinitionSelector.ascx" TagName="PackageDefinitionSelector"
    TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/TaxonomySelector.ascx" TagName="TaxonomySelector"
    TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/MenuSelector.ascx" TagName="MenuSelector" TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/CollectionSelector.ascx" TagName="CollectionSelector"
    TagPrefix="ektronUC" %>
<%@ Register Src="usercontrols/PackageSummary.ascx" TagName="PackageSummary" TagPrefix="ektronUC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:JavaScriptBlock ID="folderSelectorJS" ExecutionMode="OnEktronReady" runat="server">
        <ScriptTemplate>
            Ektron.Workarea.Packaging.initEditPackage();
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    <asp:ScriptManager ID="uxPackageScriptManager" runat="server">
    </asp:ScriptManager>
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="uxAddHeaderLiteral" runat="server" meta:resourcekey="uxAddHeaderLiteral" />
                    <asp:Literal ID="uxEditHeaderLiteral" runat="server" meta:resourcekey="uxEditHeaderLiteral" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSaveButton"
                                class="primary saveButton" ValidationGroup="createpackage" OnClick="uxSaveButton_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageGrid packageConfigContainer">
            <ektronUI:Message ID="uxMessage" runat="server" Visible="false">
            </ektronUI:Message>
            <div class="packageMeta">
                <table>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxPackageNameLabel" runat="server" meta:resourcekey="uxPackageNameLabel" />
                        </td>
                        <td class="value">
                            <ektronUI:TextField ID="uxPackageNameValue" runat="server" ValidationGroup="createpackage"
                                meta:resourcekey="uxPackageNameValue">
                                <ValidationRules>
                                    <ektronUI:RequiredRule ErrorMessage="<%$ Resources:uxPackageNameValueRequiredMessage %>" />
                                    <ektronUI:RegexRule ClientRegex="/^[a-z0-9 ]+$/i"  ErrorMessage="<%$ Resources:uxPackageNameValueCharacterMessage %>" />
                                </ValidationRules>
                            </ektronUI:TextField>
                            <ektronUI:ValidationMessage ID="uxPackageNameValueValidatorMessage" AssociatedControlID="uxPackageNameValue"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxPackageDescriptionLabel" runat="server" meta:resourcekey="uxPackageDescriptionLabel" />
                        </td>
                        <td class="value">
                            <ektronUI:TextField ID="uxPackageDescriptionValue" runat="server" ValidationGroup="createpackage"
                                meta:resourcekey="uxPackageDescriptionValue" TextMode="MultiLine">
                            </ektronUI:TextField>
                            <ektronUI:ValidationMessage ID="uxPackageDescriptionValueValidation" AssociatedControlID="uxPackageDescriptionValue"
                                runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="contentsHeaderContainer">
                <h3>
                    <asp:Literal ID="uxPackageContentsLabel" runat="server" meta:resourcekey="uxPackageContentsLabel"></asp:Literal></h3>
            </div>
            <div class="packageContents">
                <ektronUI:Tabs ID="uxContentsTab" runat="server">
                    <ektronUI:Tab ID="uxSummaryTab" runat="server" meta:resourcekey="uxSummaryTab">
                        <ContentTemplate>
                            <ektronUC:PackageSummary ID="uxPackageSummary" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxFolderContentTab" runat="server" CssClass="uxFolderContentTab"
                        meta:resourcekey="uxFolderContentTab">
                        <ContentTemplate>
                            <ektronUC:FolderAndContentSelector ID="uxFolderContentSelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxLibraryTab" runat="server" CssClass="uxLibraryTab" meta:resourcekey="uxLibraryTab">
                        <ContentTemplate>
                            <ektronUC:LibraryFilesSelector ID="uxLibraryFilesSelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxFilesTab" runat="server" CssClass="uxFilesTab" meta:resourcekey="uxFilesTab">
                        <ContentTemplate>
                            <ektronUC:FileSystemSelector ID="uxFileSystemSelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxTaxonomyTab" runat="server" meta:resourcekey="uxTaxonomyTab">
                        <ContentTemplate>
                            <ektronUC:TaxonomySelector ID="uxTaxonomySelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxMenuTab" runat="server" meta:resourcekey="uxMenusTab">
                        <ContentTemplate>
                            <ektronUC:MenuSelector ID="uxMenuSelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxCollectionTab" runat="server" meta:resourcekey="uxCollectionsTab">
                        <ContentTemplate>
                            <ektronUC:CollectionSelector ID="uxCollectionSelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                    <ektronUI:Tab ID="uxPackageDefinitionSelectorTab" runat="server" CssClass="uxPackageDefinitionSelectorTab"
                        meta:resourcekey="uxPackageDefinitionSelectorTab">
                        <ContentTemplate>
                            <ektronUC:PackageDefinitionSelector ID="uxPackageDefinitionSelector" runat="server" />
                        </ContentTemplate>
                    </ektronUI:Tab>
                </ektronUI:Tabs>
            </div>
        </div>
    </div>
    <div class="hdnFields">
        <input type="hidden" id="uxPackageJSON" runat="server" class="uxPackageJSON" />
    </div>
    </form>
</body>
</html>
