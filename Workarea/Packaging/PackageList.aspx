﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackageList.aspx.cs" Inherits="Ektron.Workarea.Packaging.PackageList"
    meta:resourcekey="page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ektronUI:JavaScriptBlock ID="initJS" ExecutionMode="OnEktronReady" runat="server">
            <ScriptTemplate>
                Ektron.Workarea.Packaging.initPackageList();
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <td class="column-AddButton">
                            <div>
                                <asp:HyperLink ID="uxAddButton" runat="server" CssClass="primary editButton" meta:resourcekey="uxAddButton"
                                    NavigateUrl="AddEditViewPackage.aspx"></asp:HyperLink>
                            </div>
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageGrid packageConfigListContainer">
            <ektronUI:Message ID="uxMessage" runat="server" Visible="false">
            </ektronUI:Message>
            <ektronUI:GridView ID="uxPackageListGridView" runat="Server" AutoGenerateColumns="false"
                EktronUIEnabled="true" OnEktronUIPageChanged="uxPackageListGridView_EktronUIThemePageChanged"
                OnRowCommand="uxPackageListGridView_RowCommand">
                <Columns>
                    <asp:HyperLinkField DataTextField="Name" DataNavigateUrlFormatString="AddEditViewPackage.aspx?id={0}"
                        DataNavigateUrlFields="Id" meta:resourceKey="columnHeaderName" />
                    <asp:BoundField DataField="Id" meta:resourcekey="columnHeaderId" />
                    <asp:BoundField DataField="Description" meta:resourcekey="columnHeaderDescription" />
                    <asp:BoundField DataField="DateModified" meta:resourcekey="columnHeaderDateModified" />
                    <asp:TemplateField meta:resourceKey="columnHeaderExample">
                        <ItemTemplate>
                            <asp:LinkButton ID="uxDeleteRow" runat="server" CommandName="deletepackage" CssClass="deletePackageLink"
                                CommandArgument='<%# Eval("Id") %>' meta:resourcekey="uxDeleteRow"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </ektronUI:GridView>
        </div>
    </div>
    </form>
</body>
</html>
