﻿/*
* Ektron Workarea Packaging
*
* Copyright 2012
*
* Depends:
*	jQuery
*   Ektron.Namespace.js
*/

Ektron.Namespace.Register("Ektron.Workarea.Packaging");
Ektron.Workarea.Packaging = {
    /* properties */
    selectall: false,
    unselectall: false,
    Package: {},

    /* methods */

    initEditPackage: function () {
        Ektron.Workarea.Packaging.Package = Ektron.Workarea.Packaging.getPackageObject();

        // Content Tab:
        Ektron.Workarea.Packaging.initContentTab();

        // Summary Tab:
        Ektron.Workarea.Packaging.initSummaryTab();

        // Library Tab:
        Ektron.Workarea.Packaging.initLibraryTab();

        // Taxonomy Tab
        Ektron.Workarea.Packaging.initTaxonomyTab();

        // Menu Tab
        Ektron.Workarea.Packaging.initMenuTab();

        // Collection Tab
        Ektron.Workarea.Packaging.initCollectionTab();

        // FileSystem Tab:
        Ektron.Workarea.Packaging.initFileSystemTab();

        // Package Definition Tab
        Ektron.Workarea.Packaging.initPackageDefinitionTab();

        $ektron('.saveButton').click(function () {
            return Ektron.Workarea.Packaging.preProcessSavePackage();
        });
    },

    initContentTab: function () {
        $ektron('.contentFolderSelectorWrapper .folderWrapper .ektron-ui-tree-root').on('click', 'span.textWrapper', function () {
            Ektron.Workarea.Packaging.loadChildContent(this);
            return false;
        });

        $ektron('.contentFolderSelectorWrapper .folderWrapper .ektron-ui-tree-root').on('click', '.selectionLocation', function (event) {
            if ($ektron(event.target).is('.selectionLocation')) {
                Ektron.Workarea.Packaging.folderItemSelected(this);
            }
            return true;
        });

        $ektron('.contentFolderSelectorWrapper .contentWrapper').on('change', 'input[type=checkbox]', function (e) {
            Ektron.Workarea.Packaging.contentItemSelected(this);
        });
    },

    initSummaryTab: function () {

        $ektron('#uxContentsTab_uxSummaryTab #uxSummaryContentsContainer table').on('click', '.deleteItem', function (e) {
            Ektron.Workarea.Packaging.deleteItemViaSummary(this);
        });

        $ektron('#uxContentsTab_uxSummaryTab_uxPackageSummary_uxViewOrphanListButton_Button').on('click', function () {
            $ektron('#uxOrphanSummaryContainer').toggle();
            return false;
        });

        $ektron('#uxSummaryHeaderContainer .summaryFilterList').on('change', function () {
            Ektron.Workarea.Packaging.filterSummaryItemsTab();
        });

        $ektron('#uxContentsTab a[href=#uxContentsTab_uxSummaryTab]').click(function () {
            Ektron.Workarea.Packaging.filterSummaryItemsTab();
        });

        // Missing Records Active CheckBox Change
        $ektron('#uxOrphanSummaryContainer #uxOrphantable').on('click', 'input[type=checkbox]', function (e) {
            try {
                Ektron.Workarea.Packaging.modifyPackage(this);
            } catch (err) { }
        });
    },

    initLibraryTab: function () {
        $ektron('.librarySelectorWrapper .folderWrapper .ektron-ui-tree-root').on('click', '.textWrapper', function () {
            Ektron.Workarea.Packaging.loadChildLibraryContent(this);
            return false;
        });

        $ektron('.librarySelectorWrapper .libraryWrapper').on('change', 'input[type=checkbox]', function (e) {
            Ektron.Workarea.Packaging.libraryItemSelected(this);
        });
    },

    initTaxonomyTab: function () {
        $ektron('#uxContentsTab a[href=#uxContentsTab_uxTaxonomyTab]').click(function () {
            Ektron.Workarea.Packaging.syncTaxonomySelections();
        });

        $ektron('.taxonomySelectorWrapper ').on('change', 'input[type=checkbox]', function () {
            Ektron.Workarea.Packaging.taxonomySelected(this);
        });
    },

    initMenuTab: function () {
        $ektron('#uxContentsTab a[href=#uxContentsTab_uxMenuTab]').click(function () {
            Ektron.Workarea.Packaging.syncMenuSelections();
        });

        $ektron('.menuSelectorWrapper ').on('change', 'input[type=checkbox]', function () {
            Ektron.Workarea.Packaging.menuSelected(this);
        });
    },

    initCollectionTab: function () {
        $ektron('#uxContentsTab a[href=#uxContentsTab_uxCollectionTab]').click(function () {
            Ektron.Workarea.Packaging.syncCollectionSelections();
        });

        $ektron('.collectionSelectorWrapper ').on('change', 'input[type=checkbox]', function () {
            Ektron.Workarea.Packaging.collectionSelected(this);
        });
    },

    initFileSystemTab: function () {
        $ektron('.filesystemSelectorWrapper .foldertreeWrapper .folderTree .EktronFiletree').on('click', '.folder', function () {
            Ektron.Workarea.Packaging.loadChildFileSystemContent(this);
            return false;
        });

        $ektron('.filesystemSelectorWrapper .filesystemWrapper').on('change', 'input[type=checkbox]', function (e) {
            Ektron.Workarea.Packaging.fileSystemItemSelected(this);
        });
    },

    initPackageDefinitionTab: function () {
        $ektron('.packageDefinitionSelectorWrapper .uxPackageList input[type=checkbox]').on('change', function () {
            Ektron.Workarea.Packaging.packageDefinitionSelected(this);
        });

        $ektron('#uxContentsTab a[href=#uxContentsTab_uxPackageDefinitionSelectorTab]').click(function () {
            Ektron.Workarea.Packaging.updatePackageGridRows();
            Ektron.Workarea.Packaging.updatePackageGridRows($ektron(".packageDefinitionSelectorWrapper .uxcurrentPackageID").val());
        });
    },

    // Add or Remove Orphan or missing Records from the Package.
    modifyPackage: function (checkedItem) {
        var PackageObject = new Object();

        var keys = $ektron(checkedItem).attr('data-ektron-id').split("_"); //*_30_1033
        if (keys[0] == "O" || keys[0] == "o") {
            PackageObject.ContentID = PackageObject.PackageID = PackageObject.MenuID = PackageObject.CollectionID = PackageObject.TaxonomyID = PackageObject.ID = keys[2];

            if (keys[3] != "")
                PackageObject.LanguageID = keys[3];

            if (keys[1] == "Content") // Content
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addContentToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeContentFromPackage(PackageObject);
            }
            else if (keys[1] == "Folder") // Folder
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addFolderToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeFolderFromPackage(PackageObject);
            }
            else if (keys[1] == "Library") // Library
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addLibraryItemToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeLibraryFileFromPackage(PackageObject);
            }
            else if (keys[1] == "Files") // File System
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addFileSystemItemToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeFileSystemFileFromPackage(PackageObject);
            }
            else if (keys[1] == "Taxonomy") // Taxonomy
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addTaxonomyToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeTaxonomyFromPackage(PackageObject);
            }
            else if (keys[1] == "LocalPackage") // Package Definition
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addPackageToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removePackageFromPackage(PackageObject);
            }
            else if (keys[1] == "Menu") // Menus
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addMenuToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeMenuFromPackage(PackageObject);
            }
            else if (keys[1] == "Collection") // Collections
            {
                if (checkedItem.checked)
                    Ektron.Workarea.Packaging.addCollectionToPackage(PackageObject);
                else
                    Ektron.Workarea.Packaging.removeCollectionFromPackage(PackageObject);
            }
        }
    },

    updatePackageGridRows: function (currentpackageid) {
        var rows = $ektron('.packageDefinitionSelectorWrapper .uxPackageList input[type=checkbox]');
        for (i = 0; i < rows.length; i++) {
            var row = $ektron(rows[i]);
            var found = false;
            if (typeof (currentpackageid) != "undefined" && currentpackageid != "" && currentpackageid != null) {
                if (currentpackageid == $ektron(row).val()) {
                    found = true;
                    row.prop('checked', true);
                    break;
                }
            }
            else {
                for (j = 0; j < Ektron.Workarea.Packaging.Package.PackageDefinitions.length; j++) {
                    var packageid = $ektron(row).val(); //2da0ec40-34cc-e111-b994-00238b64e83c               
                    if (typeof (Ektron.Workarea.Packaging.Package.PackageDefinitions[j].PackageID) != "undefined") {
                        if (Ektron.Workarea.Packaging.Package.PackageDefinitions[j].PackageID.toString() == packageid) {
                            found = true;
                            break;
                        }
                    }
                }
                if (found) {
                    row.prop('checked', true);
                } else {
                    row.prop('checked', false);
                }
            }

        }
    },
    initPackageList: function () {
        $ektron('.deletePackageLink').click(function () {
            return Ektron.Workarea.Packaging.ProcessDeleteClick();
        });
    },

    filterSummaryItemsTab: function () {
        // Hide all Rows First.
        $ektron("#uxSummaryContentsContainer #uxTableHead tr").hide();

        var selectedfilter = $ektron(".summaryFilterList option:selected").val();
        // Display Selected Filter Rows
        if (typeof (selectedfilter) != "undefined") {
            // Show Header
            $ektron("#uxSummaryContentsContainer #uxTableHead #trHeader").show();
            if (selectedfilter == "0") // All
                $ektron("#uxSummaryContentsContainer #uxTableHead tr").show();
            else if (selectedfilter == "1") // Content
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemcontent").show();
            else if (selectedfilter == "2") // Folder
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemfolder").show();
            else if (selectedfilter == "3") // Library
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemlibrary").show();
            else if (selectedfilter == "4") // File System
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemFilesystem").show();
            else if (selectedfilter == "5") // Taxonomy
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemTaxonomy").show();
            else if (selectedfilter == "6") // Package Definition
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemPackageDef").show();
            else if (selectedfilter == "7") // Menus
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemMenu").show();
            else if (selectedfilter == "8") // Collections
                $ektron("#uxSummaryContentsContainer #uxTableHead tr.summaryitemCollection").show();
        }
    },

    ProcessDeleteClick: function () {
        return confirm("Are you sure you want to delete?");
    },

    preProcessSavePackage: function () {
        Ektron.Workarea.Packaging.savePackageJSON();
        return true;
    },

    loadChildContent: function (selectedItem) {
        var selectedFolderID = $ektron(selectedItem).attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().attr('data-ektron-id')
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().parent().attr('data-ektron-id')


        var currentFolder = $ektron('.contentWrapper .hdnSourceFolder').val();
        if (selectedFolderID != currentFolder) {
            $ektron('.contentWrapper .hdnSourceFolder').val(selectedFolderID);
            $ektron('.contentWrapper .uxTriggerGetContent').click();
        }
    },

    loadChildLibraryContent: function (selectedItem) {
        var selectedFolderID = $ektron(selectedItem).attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().attr('data-ektron-id')
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().parent().attr('data-ektron-id')


        var currentFolder = $ektron('.libraryWrapper .hdnSourceLibraryFolder').val();
        if (selectedFolderID != currentFolder) {
            $ektron('.libraryWrapper .hdnSourceLibraryFolder').val(selectedFolderID);
            $ektron('.libraryWrapper .uxTriggerGetLibraryContent').click();
        }
    },

    loadChildFileSystemContent: function (selectedItem) {
        var selectedFolderPath = $ektron(selectedItem).next().attr('data-ektron-path');
        if (typeof (selectedFolderPath) == "undefined")
            selectedFolderPath = $ektron(selectedItem).parent().attr('data-ektron-path');
        if (typeof (selectedFolderPath) == "undefined")
            selectedFolderPath = $ektron(selectedItem).parent().parent().attr('data-ektron-path')
        if (typeof (selectedFolderPath) == "undefined")
            selectedFolderPath = $ektron(selectedItem).parent().parent().parent().attr('data-ektron-path')


        var currentFolder = $ektron('.filesystemWrapper .hdnSourceFileSystemFolder').val();
        if (selectedFolderPath != currentFolder) {
            $ektron('.filesystemWrapper .hdnSourceFileSystemFolder').val(selectedFolderPath);
            $ektron('.filesystemWrapper .uxTriggerGetFileSystemItems').click();
        }
    },

    contentselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        //  $ektron('.contentFolderSelectorWrapper .contentWrapper .ektron-ui-gridview input:not(:checked).contentselectorcheckbox').click();
        $ektron('.contentFolderSelectorWrapper .contentWrapper .ektron-ui-gridview input:not(:checked).contentselectorcheckbox').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    contentdeselectAllSummary: function () {
        //  $ektron('.contentFolderSelectorWrapper .contentWrapper .ektron-ui-gridview input:checked.contentselectorcheckbox').click();
        if (!$ektron('.contentFolderSelectorWrapper .contentWrapper .ektron-ui-gridview input:checked.contentselectorcheckbox').is(':disabled')) {
            Ektron.Workarea.Packaging.unselectall = true;
            $ektron('.contentFolderSelectorWrapper .contentWrapper .ektron-ui-gridview input:checked.contentselectorcheckbox').attr('checked', false).change();
            Ektron.Workarea.Packaging.unselectall = false;
        }
        return false;
    },

    libraryselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        $ektron('.librarySelectorWrapper .libraryWrapper .ektron-ui-gridview input:not(:checked).libraryselectorcheckbox').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    librarydeselectAllSummary: function () {
        Ektron.Workarea.Packaging.unselectall = true;
        $ektron('.librarySelectorWrapper .libraryWrapper .ektron-ui-gridview input:checked.libraryselectorcheckbox').attr('checked', false).change();
        Ektron.Workarea.Packaging.unselectall = false;
        return false;
    },

    fileselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        $ektron('.filesystemSelectorWrapper .filesystemWrapper .ektron-ui-gridview input:not(:checked).filesystemselectorcheckbox').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    filedeselectAllSummary: function () {
        Ektron.Workarea.Packaging.unselectall = true;
        $ektron('.filesystemSelectorWrapper .filesystemWrapper .ektron-ui-gridview input:checked.filesystemselectorcheckbox').attr('checked', false).change();
        Ektron.Workarea.Packaging.unselectall = false;
        return false;
    },

    packageselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        $ektron('.packageDefinitionSelectorWrapper .uxPackageList .ektron-ui-gridview input:not(:checked).itemPackageDef').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    packagedeselectAllSummary: function () {
        Ektron.Workarea.Packaging.unselectall = true;
        $ektron('.packageDefinitionSelectorWrapper .uxPackageList .ektron-ui-gridview input:checked.itemPackageDef').attr('checked', false).change();
        Ektron.Workarea.Packaging.unselectall = false;
        return false;
    },

    taxonomyselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        $ektron('.taxonomySelectorWrapper .uxTaxonomyLists .ektron-ui-gridview input:not(:checked).uxTaxonomyitem').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    taxonomydeselectAllSummary: function () {
        Ektron.Workarea.Packaging.unselectall = true;
        $ektron('.taxonomySelectorWrapper .uxTaxonomyLists .ektron-ui-gridview input:checked.uxTaxonomyitem').attr('checked', false).change();
        Ektron.Workarea.Packaging.unselectall = false;
        return false;
    },

    menuselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        $ektron('.menuSelectorWrapper .uxMenuLists .ektron-ui-gridview input:not(:checked).uxMenuitem').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    menudeselectAllSummary: function () {
        Ektron.Workarea.Packaging.unselectall = true;
        $ektron('.menuSelectorWrapper .uxMenuLists .ektron-ui-gridview input:checked.uxMenuitem').attr('checked', false).change();
        Ektron.Workarea.Packaging.unselectall = false;
        return false;
    },

    collectionselectAllSummary: function () {
        Ektron.Workarea.Packaging.selectall = true;
        $ektron('.collectionSelectorWrapper .uxCollectionLists .ektron-ui-gridview input:not(:checked).uxCollectionitem').attr('checked', true).change();
        Ektron.Workarea.Packaging.selectall = false;
        return false;
    },

    collectiondeselectAllSummary: function () {
        Ektron.Workarea.Packaging.unselectall = true;
        $ektron('.collectionSelectorWrapper .uxCollectionLists .ektron-ui-gridview input:checked.uxCollectionitem').attr('checked', false).change();
        Ektron.Workarea.Packaging.unselectall = false;
        return false;
    },

    deleteItemViaSummary: function (deletedItem) {
        var keys = $ektron(deletedItem).attr('data-ektron-id').split("_"); //C_30_1033
        switch (keys[0]) {
            case "C":
                var ContentIdentifier = new Object();
                ContentIdentifier.ID = keys[1];
                ContentIdentifier.ContentID = keys[1];
                ContentIdentifier.LanguageID = keys[2];
                Ektron.Workarea.Packaging.removeContentFromPackage(ContentIdentifier);
                Ektron.Workarea.Packaging.syncContentSelections();
                break;
            case "F":
                var FolderIdentifier = new Object();
                FolderIdentifier.ID = keys[1];
                Ektron.Workarea.Packaging.removeFolderFromPackage(FolderIdentifier);
                var idKey = ".folderWrapper li[data-ektron-id='" + keys[1] + "'] .hitLocation";
                Ektron.Workarea.Packaging.folderItemSelected($ektron(idKey));
                break;
            case "L":
                var LibraryIdentifier = new Object();
                LibraryIdentifier.ContentID = keys[1];
                LibraryIdentifier.LanguageID = keys[2];
                Ektron.Workarea.Packaging.removeLibraryFileFromPackage(LibraryIdentifier);
                Ektron.Workarea.Packaging.syncLibrarySelections();
                break;
            case "T":
                var TaxonomyIdentifier = new Object();
                TaxonomyIdentifier.TaxonomyID = keys[1];
                TaxonomyIdentifier.LanguageID = keys[2];
                Ektron.Workarea.Packaging.removeTaxonomyFromPackage(TaxonomyIdentifier);
                Ektron.Workarea.Packaging.syncTaxonomySelections();
                break;
            case "M":
                var MenuIdentifier = new Object();
                MenuIdentifier.MenuID = keys[1];
                MenuIdentifier.LanguageID = keys[2];
                Ektron.Workarea.Packaging.removeMenuFromPackage(MenuIdentifier);
                Ektron.Workarea.Packaging.syncMenuSelections();
                break;
            case "CO":
                var CollectionIdentifier = new Object();
                CollectionIdentifier.CollectionID = keys[1];
                CollectionIdentifier.LanguageID = keys[2];
                Ektron.Workarea.Packaging.removeCollectionFromPackage(CollectionIdentifier);
                Ektron.Workarea.Packaging.syncCollectionSelections();
                break;
            case "FS":
                var FileIdentifier = new Object();
                FileIdentifier.FilePath = $ektron(deletedItem).attr('data-ektron-id').substring(3); // file name might have _ in it
                Ektron.Workarea.Packaging.removeFileSystemFileFromPackage(FileIdentifier);
                Ektron.Workarea.Packaging.syncFileSystemSelections();
                break;
            case "P":
                var PackageIdentifier = new Object();
                PackageIdentifier.PackageID = keys[1];
                Ektron.Workarea.Packaging.removePackageFromPackage(PackageIdentifier);
                break;
        }
        $ektron(deletedItem).closest('tr').remove();
    },

    folderItemSelected: function (checkedItem) {
        var currentViewingContentInFolder = $ektron('.contentWrapper .hdnSourceFolder').val();
        var selectedFolderID = $ektron(checkedItem).attr('data-ektron-id');
        var isSelected = !$ektron(checkedItem).hasClass('selected');
        if (typeof (selectedFolderID) == "undefined") {
            selectedFolderID = $ektron(checkedItem).parent().attr('data-ektron-id');
            isSelected = !$ektron(checkedItem).parent().hasClass('selected');
        }
        if (typeof (selectedFolderID) == "undefined") {
            selectedFolderID = $ektron(checkedItem).parent().parent().attr('data-ektron-id');
            isSelected = !$ektron(checkedItem).parent().parent().hasClass('selected');
        }
        if (typeof (selectedFolderID) == "undefined") {
            selectedFolderID = $ektron(checkedItem).parent().parent().parent().attr('data-ektron-id');
            isSelected = !$ektron(checkedItem).parent().parent().parent().hasClass('selected');
        }

 
        if (typeof (selectedFolderID) != "undefined") {
            var FolderIdentifier = new Object();
            FolderIdentifier.ID = selectedFolderID;
            if (typeof ($ektron(checkedItem).tmplItem()) != "undefined" && typeof ($ektron(checkedItem).tmplItem()) != null && $ektron(checkedItem).tmplItem() != "")
                FolderIdentifier.Path = $ektron(checkedItem).tmplItem().data.NameWithPath;
            else
                FolderIdentifier.Path = $ektron(checkedItem).find('span.textWrapper').text();
            FolderIdentifier.Title = $ektron(checkedItem).find('span.textWrapper').text();
            if (isSelected) {
                Ektron.Workarea.Packaging.addFolderToPackage(FolderIdentifier);
                if (currentViewingContentInFolder == FolderIdentifier.ID) {
                    Ektron.Workarea.Packaging.disableAllContentInGrid();
                }
                Ektron.Workarea.Packaging.removeChildContentFromPackage(FolderIdentifier);
            } else {
                Ektron.Workarea.Packaging.removeFolderFromPackage(FolderIdentifier);
                Ektron.Workarea.Packaging.removeFolderFromSummary(FolderIdentifier);
                if (currentViewingContentInFolder == FolderIdentifier.ID) {
                    Ektron.Workarea.Packaging.enableAllContentInGrid();
                }
            }
        }
    },

    contentItemSelected: function (checkedItem) {
        var ContentIdentifier = new Object();
        var keys = $ektron(checkedItem).attr('data-ektron-id').split("_"); //C_30_1033
        ContentIdentifier.ID = keys[1];
        ContentIdentifier.ContentID = keys[1];
        ContentIdentifier.LanguageID = keys[2];
        ContentIdentifier.Path = $ektron(checkedItem).next().val();
        ContentIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addContentToPackage(ContentIdentifier);
            Ektron.Workarea.Packaging.addContentToSummary(ContentIdentifier);
        } else {
            Ektron.Workarea.Packaging.removeContentFromPackage(ContentIdentifier);
            Ektron.Workarea.Packaging.removeContentFromSummary(ContentIdentifier);
        }

    },

    libraryItemSelected: function (checkedItem) {
        var LibraryIdentifier = new Object();
        var keys = $ektron(checkedItem).attr('data-ektron-id').split("_"); //L_30_1033

        LibraryIdentifier.ContentID = keys[1];
        LibraryIdentifier.LanguageID = keys[2];
        LibraryIdentifier.LibraryType = Ektron.Workarea.Packaging.getLibraryType();
        LibraryIdentifier.Path = $ektron(checkedItem).next().val();
        LibraryIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addLibraryItemToPackage(LibraryIdentifier);
            Ektron.Workarea.Packaging.addLibraryItemToSummary(LibraryIdentifier);
        } else {
            Ektron.Workarea.Packaging.removeLibraryFileFromPackage(LibraryIdentifier);
            Ektron.Workarea.Packaging.removeLibraryFileFromSummary(LibraryIdentifier);
        }
    },

    fileSystemItemSelected: function (checkedItem) {
        var FileIdentifier = new Object();
        //var keys = $ektron(checkedItem).attr('data-ektron-path').split("_"); //FS_CMSLogin.aspx

        FileIdentifier.FilePath = $ektron(checkedItem).attr('data-ektron-path').substring(3);
        FileIdentifier.Path = $ektron(checkedItem).next().val();
        FileIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addFileSystemItemToPackage(FileIdentifier);
            Ektron.Workarea.Packaging.addFileSystemItemToSummary(FileIdentifier);
        } else {
            Ektron.Workarea.Packaging.removeFileSystemFileFromPackage(FileIdentifier);
            Ektron.Workarea.Packaging.removeFileSystemFileFromSummary(FileIdentifier);
        }
    },

    getLibraryType: function () {
        var type = $ektron('#uxContentsTab_uxLibraryTab_uxLibraryFilesSelector_uxLibraryTypes option:selected').val();
        if (typeof (type) != "undefined") {
            if (type == "1")
                return 1;
            else
                return 2;
        }
    },

    packageDefinitionSelected: function (checkedItem) {
        var PackageIdentifier = new Object();
        PackageIdentifier.PackageID = $ektron(checkedItem).val();
        PackageIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addPackageToPackage(PackageIdentifier);
            Ektron.Workarea.Packaging.addPackageToSummary(PackageIdentifier);
        } else {
            Ektron.Workarea.Packaging.removePackageFromPackage(PackageIdentifier);
            Ektron.Workarea.Packaging.removePackageFromSummary(PackageIdentifier);
        }
    },

    taxonomySelected: function (checkedItem) {
        var TaxonomyIdentifier = new Object();
        TaxonomyIdentifier.TaxonomyID = $ektron(checkedItem).val();
        TaxonomyIdentifier.ID = $ektron(checkedItem).val();
        TaxonomyIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        TaxonomyIdentifier.Path = $ektron(checkedItem).attr('data-ektron-path');
        TaxonomyIdentifier.LanguageID = $ektron(checkedItem).attr('data-ektron-lang');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addTaxonomyToPackage(TaxonomyIdentifier);
            Ektron.Workarea.Packaging.addTaxonomyToSummary(TaxonomyIdentifier);
            if (TaxonomyIdentifier.LanguageID == -1) {
                $ektron('.taxonomySelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + TaxonomyIdentifier.TaxonomyID + ']:checked').trigger('click', false);
                $ektron('.taxonomySelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + TaxonomyIdentifier.TaxonomyID + ']').prop('checked', true).prop("disabled", true);
            }
        } else {
            Ektron.Workarea.Packaging.removeTaxonomyFromPackage(TaxonomyIdentifier);
            Ektron.Workarea.Packaging.removeTaxonomyFromSummary(TaxonomyIdentifier);
            if (TaxonomyIdentifier.LanguageID == -1) {
                $ektron('.taxonomySelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + TaxonomyIdentifier.TaxonomyID + ']').prop('checked', false).prop("disabled", false);
            }
        }
    },

    menuSelected: function (checkedItem) {
        var MenuIdentifier = new Object();
        MenuIdentifier.MenuID = $ektron(checkedItem).val();
        MenuIdentifier.ID = $ektron(checkedItem).val();
        MenuIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        MenuIdentifier.LanguageID = $ektron(checkedItem).attr('data-ektron-lang');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addMenuToPackage(MenuIdentifier);
            Ektron.Workarea.Packaging.addMenuToSummary(MenuIdentifier);
            if (MenuIdentifier.LanguageID == -1) {
                $ektron('.menuSelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + MenuIdentifier.MenuID + ']:checked').trigger('click', false);
                $ektron('.menuSelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + MenuIdentifier.MenuID + ']').prop('checked', true).prop("disabled", true);
            }
        } else {
            Ektron.Workarea.Packaging.removeMenuFromPackage(MenuIdentifier);
            Ektron.Workarea.Packaging.removeMenuFromSummary(MenuIdentifier);
            if (MenuIdentifier.LanguageID == -1) {
                $ektron('.menuSelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + MenuIdentifier.MenuID + ']').prop('checked', false).prop("disabled", false);
            }
        }
    },

    collectionSelected: function (checkedItem) {
        var CollectionIdentifier = new Object();
        CollectionIdentifier.CollectionID = $ektron(checkedItem).val();
        CollectionIdentifier.ID = $ektron(checkedItem).val();
        CollectionIdentifier.Title = $ektron(checkedItem).attr('data-ektron-title');
        CollectionIdentifier.LanguageID = $ektron(checkedItem).attr('data-ektron-lang');
        if (checkedItem.checked) {
            Ektron.Workarea.Packaging.addCollectionToPackage(CollectionIdentifier);
            Ektron.Workarea.Packaging.addCollectionToSummary(CollectionIdentifier);
            if (CollectionIdentifier.LanguageID == -1) {
                $ektron('.menuSelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + CollectionIdentifier.CollectionID + ']:checked').trigger('click', false);
                $ektron('.menuSelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + CollectionIdentifier.CollectionID + ']').prop('checked', true).prop("disabled", true);
            }
        } else {
            Ektron.Workarea.Packaging.removeCollectionFromPackage(CollectionIdentifier);
            Ektron.Workarea.Packaging.removeCollectionFromSummary(CollectionIdentifier);
            if (CollectionIdentifier.LanguageID == -1) {
                $ektron('.menuSelectorWrapper div.taxlangwrap[data-ektron-id!=-1] input[value=' + CollectionIdentifier.CollectionID + ']').prop('checked', false).prop("disabled", false);
            }
        }
    },

    taxonomyLanguageSelected: function (selectedItem) {
        var selectedLanguage = $ektron(selectedItem).val();

        var currentlang = $ektron('.taxonomySelectorWrapper .hdnOldLanguage').val();
        if (selectedLanguage != currentlang) {
            $ektron('.taxonomySelectorWrapper .hdnOldLanguage').val(selectedLanguage);
            //$ektron('.taxonomySelectorWrapper .uxTriggerGetTaxonomy').click();
        }
    },

    menuLanguageSelected: function (selectedItem) {
        var selectedLanguage = $ektron(selectedItem).val();

        var currentlang = $ektron('.menuSelectorWrapper .hdnOldLanguage').val();
        if (selectedLanguage != currentlang) {
            $ektron('.menuSelectorWrapper .hdnOldLanguage').val(selectedLanguage);
            //$ektron('.taxonomySelectorWrapper .uxTriggerGetTaxonomy').click();
        }
    },

    addPackageToPackage: function (packageObject) {
        var found = Ektron.Workarea.Packaging.isPackageInPackage(packageObject);
        if (!found) {
            Ektron.Workarea.Packaging.Package.PackageDefinitions.push(packageObject);
        }
    },

    removePackageFromPackage: function (packageObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.PackageDefinitions.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.PackageDefinitions[i].PackageID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.PackageDefinitions[i].PackageID.toString() == packageObject.PackageID) {
                    theTarget = Ektron.Workarea.Packaging.Package.PackageDefinitions[i];
                    break;
                }
            }
        }
        Ektron.Workarea.Packaging.Package.PackageDefinitions.splice(i, 1);
    },

    addFolderToPackage: function (folderObject) {
        var found = Ektron.Workarea.Packaging.isFolderInPackage(folderObject);
        if (!found) {
            Ektron.Workarea.Packaging.Package.Folders.push(folderObject);
            Ektron.Workarea.Packaging.addFolderToSummary(folderObject);
        }
    },

    removeFolderFromPackage: function (folderObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Folders.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Folders[i].ID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Folders[i].ID.toString() == folderObject.ID) {
                    theTarget = Ektron.Workarea.Packaging.Package.Folders[i];
                    break;
                }
            }
        }

        Ektron.Workarea.Packaging.Package.Folders.splice(i, 1);
    },

    addContentToPackage: function (contentObject) {
        var found = false;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Content.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Content[i].ContentID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Content[i].ContentID.toString() == contentObject.ID
                    && Ektron.Workarea.Packaging.Package.Content[i].LanguageID.toString() == contentObject.LanguageID) {
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            Ektron.Workarea.Packaging.Package.Content.push(contentObject);
        }
    },

    addLibraryItemToPackage: function (contentObject) {
        var found = false;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.LibraryFiles.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.LibraryFiles[i].ContentID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.LibraryFiles[i].ContentID.toString() == contentObject.ContentID
                    && Ektron.Workarea.Packaging.Package.LibraryFiles[i].LanguageID.toString() == contentObject.LanguageID) {
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            Ektron.Workarea.Packaging.Package.LibraryFiles.push(contentObject);
        }
    },

    addFileSystemItemToPackage: function (contentObject) {
        var found = false;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Files.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Files[i].FilePath) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Files[i].FilePath.toString() == contentObject.FilePath) {
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            Ektron.Workarea.Packaging.Package.Files.push(contentObject);
        }
    },

    addTaxonomyToPackage: function (taxonomyObject) {
        var found = Ektron.Workarea.Packaging.isTaxonomyInPackage(taxonomyObject);
        if (!found) {
            Ektron.Workarea.Packaging.Package.Taxonomy.push(taxonomyObject);
        }
    },

    addMenuToPackage: function (menuObject) {
        var found = Ektron.Workarea.Packaging.isMenuInPackage(menuObject);
        if (!found) {
            Ektron.Workarea.Packaging.Package.Menu.push(menuObject);
        }
    },

    addCollectionToPackage: function (collectionObject) {
        var found = Ektron.Workarea.Packaging.isCollectionInPackage(collectionObject);
        if (!found) {
            Ektron.Workarea.Packaging.Package.Collection.push(collectionObject);
        }
    },

    removeContentFromPackage: function (contentObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Content.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Content[i].ContentID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Content[i].ContentID.toString() == contentObject.ID
                    && Ektron.Workarea.Packaging.Package.Content[i].LanguageID.toString() == contentObject.LanguageID) {
                    theTarget = Ektron.Workarea.Packaging.Package.Content[i];
                    break;
                }
            }
        }

        Ektron.Workarea.Packaging.Package.Content.splice(i, 1);
    },

    removeLibraryFileFromPackage: function (contentObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.LibraryFiles.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.LibraryFiles[i].ContentID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.LibraryFiles[i].ContentID.toString() == contentObject.ContentID
                    && Ektron.Workarea.Packaging.Package.LibraryFiles[i].LanguageID.toString() == contentObject.LanguageID) {
                    theTarget = Ektron.Workarea.Packaging.Package.LibraryFiles[i];
                    break;
                }
            }
        }

        Ektron.Workarea.Packaging.Package.LibraryFiles.splice(i, 1);
    },

    removeTaxonomyFromPackage: function (taxonomyObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Taxonomy.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Taxonomy[i].TaxonomyID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Taxonomy[i].TaxonomyID.toString() == taxonomyObject.TaxonomyID &&
                        Ektron.Workarea.Packaging.Package.Taxonomy[i].LanguageID.toString() == taxonomyObject.LanguageID) {
                    theTarget = Ektron.Workarea.Packaging.Package.Taxonomy[i];
                    break;
                }
            }
        }
        Ektron.Workarea.Packaging.Package.Taxonomy.splice(i, 1);
    },

    removeMenuFromPackage: function (menuObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Menu.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Menu[i].MenuID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Menu[i].MenuID.toString() == menuObject.MenuID &&
                        Ektron.Workarea.Packaging.Package.Menu[i].LanguageID.toString() == menuObject.LanguageID) {
                    theTarget = Ektron.Workarea.Packaging.Package.Menu[i];
                    break;
                }
            }
        }
        Ektron.Workarea.Packaging.Package.Menu.splice(i, 1);
    },

    removeCollectionFromPackage: function (collectionObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Collection.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Collection[i].CollectionID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Collection[i].CollectionID.toString() == collectionObject.CollectionID &&
                        Ektron.Workarea.Packaging.Package.Collection[i].LanguageID.toString() == collectionObject.LanguageID) {
                    theTarget = Ektron.Workarea.Packaging.Package.Collection[i];
                    break;
                }
            }
        }
        Ektron.Workarea.Packaging.Package.Collection.splice(i, 1);
    },

    removeFileSystemFileFromPackage: function (contentObject) {
        var theTarget = null;
        var i = 0;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Files.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Files[i].FilePath) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Files[i].FilePath.toString() == contentObject.FilePath) {
                    theTarget = Ektron.Workarea.Packaging.Package.Files[i];
                    break;
                }
            }
        }

        Ektron.Workarea.Packaging.Package.Files.splice(i, 1);
    },


    getPackageObject: function () {
        return Ektron.JSON.parse($ektron('#uxPackageJSON').val());
    },

    savePackageJSON: function () {

        $ektron('#uxPackageJSON').val(Ektron.JSON.stringify(Ektron.Workarea.Packaging.Package));
    },

    syncContentSelections: function () {

        var currentFolder = $ektron('.contentWrapper .hdnSourceFolder').val();
        var FolderIdentifier = new Object();
        FolderIdentifier.ID = currentFolder;
        var folderFound = Ektron.Workarea.Packaging.isFolderInPackage(FolderIdentifier);

        if (!folderFound) {
            var rows = $ektron('.contentBrowserContainer .ektron-ui-gridview input[type=checkbox]');
            for (i = 0; i < rows.length; i++) {
                var row = $ektron(rows[i]);
                var found = false;
                for (j = 0; j < Ektron.Workarea.Packaging.Package.Content.length; j++) {
                    var keys = $ektron(row).attr('data-ektron-id').split("_"); //C_30_1033
                    if (typeof (Ektron.Workarea.Packaging.Package.Content[j].ContentID) != "undefined") {
                        if (Ektron.Workarea.Packaging.Package.Content[j].ContentID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Content[j].LanguageID.toString() == keys[2]) {
                            found = true;
                            break;
                        }
                    }
                }
                if (found) {
                    row.prop('checked', true);
                } else {
                    row.prop('checked', false);
                }
            }
        } else {
            Ektron.Workarea.Packaging.disableAllContentInGrid();
            Ektron.Workarea.Packaging.removeChildContentFromPackage(FolderIdentifier);
        }
    },

    syncLibrarySelections: function () {

        var rows = $ektron('.libraryBrowserContainer .ektron-ui-gridview input[type=checkbox]');
        for (i = 0; i < rows.length; i++) {
            var row = $ektron(rows[i]);
            var found = false;
            for (j = 0; j < Ektron.Workarea.Packaging.Package.LibraryFiles.length; j++) {
                var keys = $ektron(row).attr('data-ektron-id').split("_"); //L_30_1033
                if (typeof (Ektron.Workarea.Packaging.Package.LibraryFiles[j].ContentID) != "undefined") {
                    if (Ektron.Workarea.Packaging.Package.LibraryFiles[j].ContentID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.LibraryFiles[j].LanguageID.toString() == keys[2]) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                row.prop('checked', true);
            } else {
                row.prop('checked', false);
            }
        }
    },

    syncFileSystemSelections: function () {
        var rows = $ektron('.FileSystemBrowserContainer .ektron-ui-gridview input[type=checkbox]');
        for (i = 0; i < rows.length; i++) {
            var row = $ektron(rows[i]);
            var found = false;
            for (j = 0; j < Ektron.Workarea.Packaging.Package.Files.length; j++) {
                //var keys = $ektron(row).attr('data-ektron-path').split("_"); //FS_cmslogin.aspx
                if (typeof (Ektron.Workarea.Packaging.Package.Files[j].FilePath) != "undefined") {
                    if (Ektron.Workarea.Packaging.Package.Files[j].FilePath.toString() == $ektron(row).attr('data-ektron-path').substring(3)) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                row.prop('checked', true);
            } else {
                row.prop('checked', false);
            }
        }
    },

    syncTaxonomySelections: function () {
        var rows = $ektron('.uxTaxonomyLists .ektron-ui-gridview input[type=checkbox]');
        if (typeof (rows) != "undefined") {
            for (i = 0; i < rows.length; i++) {
                var row = $ektron(rows[i]);
                var found = false;
                if (typeof (Ektron.Workarea.Packaging.Package.Taxonomy) != "undefined" && Ektron.Workarea.Packaging.Package.Taxonomy != null) {
                    for (j = 0; j < Ektron.Workarea.Packaging.Package.Taxonomy.length; j++) {
                        var keys = $ektron(row).attr('data-ektron-id').split("_"); //T_30_1033
                        if (typeof (Ektron.Workarea.Packaging.Package.Taxonomy[j].TaxonomyID) != "undefined") {
                            if (Ektron.Workarea.Packaging.Package.Taxonomy[j].TaxonomyID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Taxonomy[j].LanguageID.toString() == keys[2]) {
                                found = true;
                                break;
                            }
                            if (Ektron.Workarea.Packaging.Package.Taxonomy[j].TaxonomyID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Taxonomy[j].LanguageID.toString() == -1) {
                                found = true;
                                row.prop("disabled", true);
                                break;
                            }
                        }
                    }
                    if (found) {
                        row.prop('checked', true);
                    } else {
                        row.prop('checked', false);
                        row.prop("disabled", false);
                    }
                }
            }
        }
    },

    syncMenuSelections: function () {
        var rows = $ektron('.uxMenuLists .ektron-ui-gridview input[type=checkbox]');
        if (typeof (rows) != "undefined") {
            for (i = 0; i < rows.length; i++) {
                var row = $ektron(rows[i]);
                var found = false;
                if (typeof (Ektron.Workarea.Packaging.Package.Menu) != "undefined" && Ektron.Workarea.Packaging.Package.Menu != null) {
                    for (j = 0; j < Ektron.Workarea.Packaging.Package.Menu.length; j++) {
                        var keys = $ektron(row).attr('data-ektron-id').split("_"); //M_30_1033
                        if (typeof (Ektron.Workarea.Packaging.Package.Menu[j].MenuID) != "undefined") {
                            if (Ektron.Workarea.Packaging.Package.Menu[j].MenuID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Menu[j].LanguageID.toString() == keys[2]) {
                                found = true;
                                break;
                            }
                            if (Ektron.Workarea.Packaging.Package.Menu[j].MenuID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Menu[j].LanguageID.toString() == -1) {
                                found = true;
                                row.prop("disabled", true);
                                break;
                            }
                        }
                    }
                    if (found) {
                        row.prop('checked', true);
                    } else {
                        row.prop('checked', false);
                        row.prop("disabled", false);
                    }
                }
            }
        }
    },

    syncCollectionSelections: function () {
        var rows = $ektron('.uxCollectionLists .ektron-ui-gridview input[type=checkbox]');
        if (typeof (rows) != "undefined") {
            for (i = 0; i < rows.length; i++) {
                var row = $ektron(rows[i]);
                var found = false;
                if (typeof (Ektron.Workarea.Packaging.Package.Collection) != "undefined" && Ektron.Workarea.Packaging.Package.Collection != null) {
                    for (j = 0; j < Ektron.Workarea.Packaging.Package.Collection.length; j++) {
                        var keys = $ektron(row).attr('data-ektron-id').split("_"); //CO_30_1033
                        if (typeof (Ektron.Workarea.Packaging.Package.Collection[j].CollectionID) != "undefined") {
                            if (Ektron.Workarea.Packaging.Package.Collection[j].CollectionID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Collection[j].LanguageID.toString() == keys[2]) {
                                found = true;
                                break;
                            }
                            if (Ektron.Workarea.Packaging.Package.Collection[j].CollectionID.toString() == keys[1]
                    && Ektron.Workarea.Packaging.Package.Collection[j].LanguageID.toString() == -1) {
                                found = true;
                                row.prop("disabled", true);
                                break;
                            }
                        }
                    }
                    if (found) {
                        row.prop('checked', true);
                    } else {
                        row.prop('checked', false);
                        row.prop("disabled", false);
                    }
                }
            }
        }
    },

    disableAllContentInGrid: function () {
        var rows = $ektron('.contentBrowserContainer .ektron-ui-gridview input[type=checkbox]');
        for (i = 0; i < rows.length; i++) {
            var row = $ektron(rows[i]);
            row.prop('checked', true);
            row.prop("disabled", true);
        }
    },

    enableAllContentInGrid: function () {
        var rows = $ektron('.contentBrowserContainer .ektron-ui-gridview input[type=checkbox]');
        for (i = 0; i < rows.length; i++) {
            var row = $ektron(rows[i]);
            row.prop('checked', false);
            row.prop("disabled", false);
        }
    },

    removeChildContentFromPackage: function (FolderIdentifier) {

    },

    addContentToSummary: function (contentObject) {
        var idKey = "#uxTableHead input[data-ektron-id='C_" + contentObject.ID.toString() + "_" + contentObject.LanguageID.toString() + "']";
        var imgsrc = $ektron('#uximgsrc_' + contentObject.ID.toString() + "_" + contentObject.LanguageID.toString()).val();
        var imgicon = $ektron('#uxTypesrc_' + contentObject.ID.toString() + "_" + contentObject.LanguageID.toString()).val();
        var contentid = contentObject.ID.toString();
        var contentlanguage = contentObject.LanguageID.toString();
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}_{1}" class="summaryitemcontent"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="C_' + contentid + "_" + contentlanguage + '" /></td><td>' +
                        imgicon + '</td><td>' + contentObject.Title.toString() + '</td><td>{2}</td><td><img src=' + imgsrc + ' alt="{1}" title="Language Flag" /></td></tr>';
            template = template.replace("{0}", contentid);
            template = template.replace("{1}", contentlanguage);
            template = template.replace("{2}", contentObject.Path);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    addLibraryItemToSummary: function (contentObject) {
        var idKey = "#uxTableHead input[data-ektron-id='L_" + contentObject.ContentID.toString() + "_" + contentObject.LanguageID.toString() + "']";
        var contentid = contentObject.ContentID.toString();
        var contentlanguage = contentObject.LanguageID.toString();
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}_{1}" class="summaryitemlibrary"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="L_' + contentid + "_" + contentlanguage + '" /></td><td>' +
                           '<img src="../images/UI/icons/bookGreen.png"" alt="{1}" title="Library" /></td><td>' + contentObject.Title.toString() + '</td><td>{2}</td><td></td></tr>';
            template = template.replace("{0}", contentid);
            template = template.replace("{1}", contentlanguage);
            template = template.replace("{2}", contentObject.Path);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    addFileSystemItemToSummary: function (contentObject) {
        var idKey = "#uxTableHead input[data-ektron-id='FS_" + contentObject.FilePath.toString() + "']";
        var filepath = contentObject.FilePath.toString();
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}" class="summaryitemFilesystem"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="FS_' + filepath + '" /></td><td>' +
                           '<img src="../images/UI/icons/contentStack.png"" alt="contentStack.png" title="File" /></td><td>' + contentObject.Title.toString() + '</td><td>{2}</td><td></td></tr>';

            template = template.replace("{0}", filepath.replace(/\s/g, "_").replace(/\./g, "_").replace(/\//g, "-").replace(/\&/g, "-").toLowerCase());
            template = template.replace("{2}", contentObject.Path);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    addTaxonomyToSummary: function (packageObject) {
        var idKey = "#uxTableHead input[data-ektron-id='T_" + packageObject.TaxonomyID.toString() + "_" + packageObject.LanguageID.toString() + "']";
        var taxonomyid = packageObject.TaxonomyID.toString();
        var taxlanguage = packageObject.LanguageID.toString();
        var imgsrc = $ektron('#uximgsrc_' + taxonomyid + "_" + taxlanguage).val();
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}_{1}" class="summaryitemTaxonomy"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="T_' + taxonomyid + "_" + taxlanguage + '" /></td><td>' +
                           '<img src="../images/UI/icons/taxonomy.png"" alt="{1}" title="Taxonomy" /></td><td>' + packageObject.Title.toString() + '</td><td>{2}</td><td><img src=' + imgsrc + ' alt="{1}" title="All Language" /></td></tr>';
            template = template.replace("{0}", taxonomyid);
            template = template.replace("{1}", taxlanguage);
            template = template.replace("{2}", packageObject.Path);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    addMenuToSummary: function (packageObject) {
        var idKey = "#uxTableHead input[data-ektron-id='M_" + packageObject.MenuID.toString() + "_" + packageObject.LanguageID.toString() + "']";
        var menuid = packageObject.MenuID.toString();
        var menulanguage = packageObject.LanguageID.toString();
        var imgsrc = $ektron('#uximgsrc_' + menuid + "_" + menulanguage).val();
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}_{1}" class="summaryitemMenu"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="M_' + menuid + "_" + menulanguage + '" /></td><td>' +
                           '<img src="../images/UI/icons/menu.png"" alt="{1}" title="Menu" /></td><td>' + packageObject.Title.toString() + '</td><td></td><td><img src=' + imgsrc + ' alt="{1}" title="All Language" /></td></tr>';
            template = template.replace("{0}", menuid);
            template = template.replace("{1}", menulanguage);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    addCollectionToSummary: function (packageObject) {
        var idKey = "#uxTableHead input[data-ektron-id='CO_" + packageObject.CollectionID.toString() + "_" + packageObject.LanguageID.toString() + "']";
        var collectionid = packageObject.CollectionID.toString();
        var collectionlanguage = packageObject.LanguageID.toString();
        var imgsrc = $ektron('#uximgsrc_' + collectionid + "_" + collectionlanguage).val();
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}_{1}" class="summaryitemCollection"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="CO_' + collectionid + "_" + collectionlanguage + '" /></td><td>' +
                           '<img src="../images/UI/icons/collection.png"" alt="{1}" title="Collection" /></td><td>' + packageObject.Title.toString() + '</td><td></td><td><img src=' + imgsrc + ' alt="{1}" title="All Language" /></td></tr>';
            template = template.replace("{0}", collectionid);
            template = template.replace("{1}", collectionlanguage);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    removeContentFromSummary: function (contentObject) {
        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}_{1}";
        selectorTemplate = selectorTemplate.replace("{0}", contentObject.ID);
        selectorTemplate = selectorTemplate.replace("{1}", contentObject.LanguageID);
        $ektron(selectorTemplate).remove();
    },

    removeLibraryFileFromSummary: function (contentObject) {

        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}_{1}";
        selectorTemplate = selectorTemplate.replace("{0}", contentObject.ContentID);
        selectorTemplate = selectorTemplate.replace("{1}", contentObject.LanguageID);
        $ektron(selectorTemplate).remove();
    },

    removeFileSystemFileFromSummary: function (contentObject) {

        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}";
        selectorTemplate = selectorTemplate.replace("{0}", contentObject.FilePath.toString().replace(/\s/g, "_").replace(/\./g, "_").replace(/\//g, "-").replace(/\&/g, "-").toLowerCase());
        $ektron(selectorTemplate).remove();
    },

    addFolderToSummary: function (folderObject) {
        var idKey = "#uxTableHead input[data-ektron-id='F_" + folderObject.ID.toString() + "']";
        var folderid = folderObject.ID;
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}" class="summaryitemfolder"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="F_' + folderid + '" /></td><td>' +
                        '<img src="../images/UI/icons/folder.png"  title="Folder" alt="folderIcon"></td><td>' + folderObject.Title.toString() + '</td><td>{1}</td><td></td></tr>';

            template = template.replace("{0}", folderObject.ID);
            template = template.replace("{1}", folderObject.Path);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    removeFolderFromSummary: function (folderObject) {
        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}";
        selectorTemplate = selectorTemplate.replace("{0}", folderObject.ID);
        $ektron(selectorTemplate).remove();
    },

    addPackageToSummary: function (packageObject) {
        var idKey = "#uxTableHead input[data-ektron-id='P_" + packageObject.PackageID.toString() + "']";
        var packageid = packageObject.PackageID;
        if ($ektron(idKey).length == 0) {
            var template = '<tr id="tr_{0}" class="summaryitemPackageDef"><td><img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem" data-ektron-id="P_' + packageid + '" /></td><td>' +
                        '<img src="../images/UI/icons/package.png"  title="Local Package" alt="PackageDefIcon"></td><td>' + packageObject.Title.toString() + '</td><td>' + packageid + '</td><td></td></tr>';

            template = template.replace("{0}", packageid);
            $('#uxSummaryContentsContainer .contentsScroll #uxTableHead tr:last').after(template);
        } else {
            $ektron(idKey).attr('checked', 'true');
        }
    },

    removePackageFromSummary: function (packageObject) {
        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}";
        selectorTemplate = selectorTemplate.replace("{0}", packageObject.PackageID);
        $ektron(selectorTemplate).remove();
    },

    removeTaxonomyFromSummary: function (packageObject) {
        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}_{1}";
        selectorTemplate = selectorTemplate.replace("{0}", packageObject.TaxonomyID);
        selectorTemplate = selectorTemplate.replace("{1}", packageObject.LanguageID);
        $ektron(selectorTemplate).remove();
    },

    removeMenuFromSummary: function (packageObject) {
        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}_{1}";
        selectorTemplate = selectorTemplate.replace("{0}", packageObject.MenuID);
        selectorTemplate = selectorTemplate.replace("{1}", packageObject.LanguageID);
        $ektron(selectorTemplate).remove();
    },

    removeCollectionFromSummary: function (packageObject) {
        var selectorTemplate = "#uxSummaryContentsContainer #uxTableHead #tr_{0}_{1}";
        selectorTemplate = selectorTemplate.replace("{0}", packageObject.CollectionID);
        selectorTemplate = selectorTemplate.replace("{1}", packageObject.LanguageID);
        $ektron(selectorTemplate).remove();
    },

    isFolderInPackage: function (folderObject) {
        var found = false;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.Folders.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.Folders[i].ID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.Folders[i].ID.toString() == folderObject.ID) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    },

    isPackageInPackage: function (packageObject) {
        var found = false;
        for (i = 0; i < Ektron.Workarea.Packaging.Package.PackageDefinitions.length; i++) {
            if (typeof (Ektron.Workarea.Packaging.Package.PackageDefinitions[i].PackageID) != "undefined") {
                if (Ektron.Workarea.Packaging.Package.PackageDefinitions[i].PackageID.toString() == packageObject.PackageID) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    },

    isTaxonomyInPackage: function (taxonomyObject) {
        var found = false;
        if (typeof (Ektron.Workarea.Packaging.Package.Taxonomy) != "undefined" && Ektron.Workarea.Packaging.Package.Taxonomy != null) {
            for (i = 0; i < Ektron.Workarea.Packaging.Package.Taxonomy.length; i++) {
                if (typeof (Ektron.Workarea.Packaging.Package.Taxonomy[i].TaxonomyID) != "undefined") {
                    if (taxonomyObject.LanguageID == -1) {
                        if (Ektron.Workarea.Packaging.Package.Taxonomy[i].TaxonomyID.toString() == taxonomyObject.TaxonomyID &&
                        Ektron.Workarea.Packaging.Package.Taxonomy[i].LanguageID.toString() == taxonomyObject.LanguageID) {
                            found = true;
                        }
                        else if (Ektron.Workarea.Packaging.Package.Taxonomy[i].TaxonomyID.toString() == taxonomyObject.TaxonomyID &&
                        Ektron.Workarea.Packaging.Package.Taxonomy[i].LanguageID.toString() != taxonomyObject.LanguageID) {
                            Ektron.Workarea.Packaging.Package.Taxonomy.splice(i, 1);
                        }
                    }
                    else {
                        if (Ektron.Workarea.Packaging.Package.Taxonomy[i].TaxonomyID.toString() == taxonomyObject.TaxonomyID &&
                        Ektron.Workarea.Packaging.Package.Taxonomy[i].LanguageID.toString() == taxonomyObject.LanguageID) {
                            found = true;
                            break;
                        }
                    }
                }
            }
        }
        return found;
    },

    isMenuInPackage: function (menuObject) {
        var found = false;
        if (typeof (Ektron.Workarea.Packaging.Package.Menu) != "undefined" && Ektron.Workarea.Packaging.Package.Menu != null) {
            for (i = 0; i < Ektron.Workarea.Packaging.Package.Menu.length; i++) {
                if (typeof (Ektron.Workarea.Packaging.Package.Menu[i].MenuID) != "undefined") {
                    if (menuObject.LanguageID == -1) {
                        if (Ektron.Workarea.Packaging.Package.Menu[i].MenuID.toString() == menuObject.MenuID &&
                        Ektron.Workarea.Packaging.Package.Menu[i].LanguageID.toString() == menuObject.LanguageID) {
                            found = true;
                        }
                        else if (Ektron.Workarea.Packaging.Package.Menu[i].MenuID.toString() == menuObject.MenuID &&
                        Ektron.Workarea.Packaging.Package.Menu[i].LanguageID.toString() != menuObject.LanguageID) {
                            Ektron.Workarea.Packaging.Package.Menu.splice(i, 1);
                        }
                    }
                    else {
                        if (Ektron.Workarea.Packaging.Package.Menu[i].MenuID.toString() == menuObject.MenuID &&
                        Ektron.Workarea.Packaging.Package.Menu[i].LanguageID.toString() == menuObject.LanguageID) {
                            found = true;
                            break;
                        }
                    }
                }
            }
        }
        return found;
    },

    isCollectionInPackage: function (collectionObject) {
        var found = false;
        if (typeof (Ektron.Workarea.Packaging.Package.Collection) != "undefined" && Ektron.Workarea.Packaging.Package.Collection != null) {
            for (i = 0; i < Ektron.Workarea.Packaging.Package.Collection.length; i++) {
                if (typeof (Ektron.Workarea.Packaging.Package.Collection[i].CollectionID) != "undefined") {
                    if (collectionObject.LanguageID == -1) {
                        if (Ektron.Workarea.Packaging.Package.Collection[i].CollectionID.toString() == collectionObject.CollectionID &&
                        Ektron.Workarea.Packaging.Package.Collection[i].LanguageID.toString() == collectionObject.LanguageID) {
                            found = true;
                        }
                        else if (Ektron.Workarea.Packaging.Package.Collection[i].CollectionID.toString() == collectionObject.CollectionID &&
                        Ektron.Workarea.Packaging.Package.Collection[i].LanguageID.toString() != collectionObject.LanguageID) {
                            Ektron.Workarea.Packaging.Package.Collection.splice(i, 1);
                        }
                    }
                    else {
                        if (Ektron.Workarea.Packaging.Package.Collection[i].CollectionID.toString() == collectionObject.CollectionID &&
                        Ektron.Workarea.Packaging.Package.Collection[i].LanguageID.toString() == collectionObject.LanguageID) {
                            found = true;
                            break;
                        }
                    }
                }
            }
        }
        return found;
    }
};
