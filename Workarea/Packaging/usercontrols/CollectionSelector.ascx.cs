﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework;
    using Ektron.Cms.Organization;
    using Ektron.Cms.Content;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

    public partial class CollectionSelector : UserControl
    {
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private SiteAPI siteAPI = new SiteAPI();
        private CollectionCriteria criteria;

        protected void Page_Init(object sender, EventArgs e)
        {
            uxInfoMessage.Visible = false;
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;
            this.criteria = this.GetCollectionCriteria();
            this.BindCollectionList();
        }

        private void BindCollectionList()
        {
            var collectionManager = new CollectionManager(ApiAccessMode.Admin);
            var collItems = collectionManager.GetList(this.criteria);

            if (collItems != null && collItems.Any())
            {
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
                uxCollectionListsGrid.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxCollectionListsGrid.EktronUIOrderByFieldText = this.criteria.OrderByDirection.ToString();
                uxCollectionListsGrid.DataSource = collItems;
                uxCollectionListsGrid.DataBind();
            }
            else
            {
                uxInfoMessage.Visible = true;
                uxInfoMessage.Text = GetLocalResourceObject("messageCollectionSelectorInfo").ToString();
            }
        }

        protected void uxCollectionListsGrid_GridViewEktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria = this.GetCollectionCriteria();
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByField = ContentCollectionProperty.Title;
                this.criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                this.BindCollectionList();
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }

        private CollectionCriteria GetCollectionCriteria()
        {
            CollectionCriteria collCriteria = new CollectionCriteria();
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = siteAPI.RequestInformationRef.PagingSize;
            collCriteria.PagingInfo = pagingInfo;
            collCriteria.OrderByField = ContentCollectionProperty.Title;
            collCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            collCriteria.AddFilter(ContentCollectionProperty.Id, CriteriaFilterOperator.GreaterThan, 0);
            return collCriteria;
        }

      public string GetRowDataCollectionId(long collId)
        {
            return "CO_" + collId.ToString() + "_-1";
        }
    }
}
