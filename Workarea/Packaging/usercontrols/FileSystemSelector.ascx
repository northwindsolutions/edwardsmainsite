﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileSystemSelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.FileSystemSelector" %>
<%@ Register Src="FolderSystemTree.ascx" TagName="FolderSystemTree" TagPrefix="ektronUC" %>
<div class="filesystemSelectorWrapper clearfix">
    <div class="foldertreeWrapper">
        <asp:PlaceHolder ID="uxfoldersystemtreeholder" runat="server" Visible="False">
            <div class="folderTree">
                <ektronUC:FolderSystemTree ID="uxFolderSystemTree" runat="server" />
            </div>
        </asp:PlaceHolder>
    </div>
    <div class="filesystemWrapper">
        <asp:UpdatePanel ID="uxUpdatePanelFileSystem" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ektronUI:JavaScriptBlock ID="JavaScriptBlock1" ExecutionMode="OnEktronReady" runat="server">
                    <ScriptTemplate>
                        Ektron.Workarea.Packaging.syncFileSystemSelections();
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <div class="hdnFields">
                    <input type="hidden" id="uxOldSourceFileSystemFolder" runat="server" class="hdnOldSourceFileSystemFolder" />
                    <input type="hidden" id="uxSourceFileSystemFolder" runat="server" class="hdnSourceFileSystemFolder" />
                    <asp:Button ID="uxTriggerGetFileSystemItems" runat="server" CssClass="uxTriggerGetFileSystemItems"
                        Style="display: none;" />
                </div>
                <div class="fileFilters">
                    <span style="float: right;">
                        <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
                        <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.fileselectAllSummary();"
                            Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
                        <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.filedeselectAllSummary();"
                            Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
                    </span>
                </div>
                <div class="FileSystemBrowserContainer">
                    <ektronUI:Message ID="uxFileSystemMessage" runat="server">
                    </ektronUI:Message>
                    <ektronUI:GridView ID="uxFileSystemGrid" runat="server" AutoGenerateColumns="false"
                        EktronUIAllowResizableColumns="false" Visible="false">
                        <Columns>
                            <ektronUI:CheckBoxField ID="uxSelectColumn">
                                <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                                <CheckBoxFieldColumnStyle Visible="false" />
                            </ektronUI:CheckBoxField>
                            <asp:TemplateField meta:resourceKey="columnHeaderSelect">
                                <ItemTemplate>
                                    <input type="checkbox" id="selectColumn" class="filesystemselectorcheckbox" data-ektron-path='FS_<%#  Eval("FileSitePath") %>'
                                        data-ektron-title='<%#  Eval("FileName") %>' />
                                    <input type="hidden" id="hdnfilesystemPath" class="hdnfilesystemPath" value='<%#  Eval("FileSitePath") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="columnHeaderFileSystemTitle">
                                <ItemTemplate>
                                    <asp:Literal ID="uxFileName" runat="server" Text='<%# Eval("FileName") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ektronUI:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
