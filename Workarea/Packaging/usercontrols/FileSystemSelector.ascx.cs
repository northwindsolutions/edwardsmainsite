﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Content;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using System.IO;
    using System.Linq;

    public partial class FileSystemSelector : System.Web.UI.UserControl
    {
        protected string selectedfolderpath = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            uxFileSystemMessage.Visible = false;
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;

            if (Page.IsPostBack)
            {
                // Folder Change Event
                if (!String.IsNullOrEmpty(uxSourceFileSystemFolder.Value) && uxSourceFileSystemFolder.Value != uxOldSourceFileSystemFolder.Value)
                {
                    this.uxFolder_Change();
                }
            }
            else
            {
                uxfoldersystemtreeholder.Visible = true;
                uxFileSystemMessage.Visible = true;
                uxFileSystemMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxFileSystemMessage.Text = GetLocalResourceObject("messageSelectFolder").ToString();
            }
        }

        protected void uxFolder_Change()
        {
            this.bindData();
            uxfoldersystemtreeholder.Visible = false;
            uxOldSourceFileSystemFolder.Value = selectedfolderpath.ToString();
        }

        private PagingInfo GetDefaultPagingInfo(int count)
        {
            var filelistpaginginfo = new PagingInfo();
            filelistpaginginfo.CurrentPage = 1;
            filelistpaginginfo.TotalPages = count;
            filelistpaginginfo.TotalRecords = count;
            filelistpaginginfo.RecordsPerPage = 100000;

            return filelistpaginginfo;
        }

        private void bindData()
        {
            selectedfolderpath = uxSourceFileSystemFolder.Value;
            string sitePath = new Ektron.Cms.SiteAPI().SitePath;
            string rootPath = MapPath(sitePath);
            Converter<string, string> trimPath = delegate(string str) { return str.Substring(rootPath.Length); };

            List<FileSystemData> FilesList = new List<FileSystemData>();
            if (!string.IsNullOrEmpty(selectedfolderpath))
            {
                string path = MapPath(sitePath + selectedfolderpath);
                foreach (string s in Directory.GetFiles(path))
                {
                    string filename = trimPath(s).Substring(trimPath(s).LastIndexOf('\\') + 1);
                    string filesithpath = trimPath(s).Replace('\\', '/').ToLower();
                    if (!filesithpath.StartsWith("/"))
                        filesithpath = "/" + filesithpath;
                    if (IsvalidFile(filename))
                    {
                        FilesList.Add(new FileSystemData() { FileName = filename, FilePhysicalPath = s.ToLower(), FileSitePath = filesithpath });
                    }
                }
            }

            if (FilesList != null && FilesList.Any())
            {
                uxFileSystemGrid.EktronUIPagingInfo = GetDefaultPagingInfo(FilesList.Count);
                uxFileSystemGrid.DataSource = FilesList;
                uxFileSystemGrid.DataBind();
                uxFileSystemGrid.Visible = true;
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
            }
            else
            {
                uxFileSystemGrid.DataSource = null;
                uxFileSystemGrid.DataBind();
                uxFileSystemGrid.Visible = false;
                uxFileSystemMessage.Visible = true;
                uxFileSystemMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxFileSystemMessage.Text = GetLocalResourceObject("MessageNoContent").ToString();
            }
        }

        private bool IsvalidFile(string filename)
        {
            var excludeFiles = new List<string>("thumbs.db,.cer,.pfx,.pvk,.refresh,.ekt,.config,.dll,.exe,.metadata,.sln,.svn,.tekt,._svn,.log,metaconfig.doc".Split(','));
            if (excludeFiles.Contains(filename.ToLower()) || excludeFiles.Contains("." + filename.ToLower().Substring(filename.ToLower().LastIndexOf('.') + 1)) || filename.ToLower().EndsWith("metaconfig.doc"))
                return false;

            return true;
        }
    }

    [Serializable]
    public class FileSystemData
    {
        public string FileName { get; set; }

        public string FilePhysicalPath { get; set; }

        public string FileSitePath { get; set; }
    }
}