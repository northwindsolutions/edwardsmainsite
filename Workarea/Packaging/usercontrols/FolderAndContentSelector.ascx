﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderAndContentSelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.FolderAndContentSelector" %>
<div class="contentFolderSelectorWrapper clearfix">
    <div class="folderWrapper" style="overflow: auto; white-space: nowrap;">
        <ektronUI:FolderTree ID="uxFolderTree" runat="server" PageSize="50" PageDepth="1"
            RootId="0" SelectionMode="MultiForAll" UseInternalAdmin="true" ShowRoot="true"
            Visible="false" CssClass="showControls" OnPreSerializeData="PreSerializeHandler">
        </ektronUI:FolderTree>
    </div>
    <div class="contentWrapper">
        <asp:UpdatePanel ID="uxUpdatePanelContent" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ektronUI:JavaScriptBlock ID="JavaScriptBlock1" ExecutionMode="OnEktronReady" runat="server">
                    <ScriptTemplate>
                        Ektron.Workarea.Packaging.syncContentSelections();
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <div class="hdnFields">
                    <input type="hidden" id="uxOldSourceFolder" runat="server" class="hdnOldSourceFolder" />
                    <input type="hidden" id="uxSourceFolder" runat="server" class="hdnSourceFolder" />
                    <asp:Button ID="uxTriggerGetContent" runat="server" CssClass="uxTriggerGetContent"
                        Style="display: none;" />
                </div>
                <div class="contentFilters">
                    <asp:DropDownList ID="uxLanguageList" runat="server" AutoPostBack="true"
                        OnSelectedIndexChanged="uxLanguageList_Change" />
                    <span style="float: right;">
                        <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
                        <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.contentselectAllSummary();"
                            Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
                        <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.contentdeselectAllSummary();"
                            Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
                    </span>
                </div>
                <div class="contentBrowserContainer">
                    <ektronUI:Message ID="uxContentMessage" runat="server">
                    </ektronUI:Message>
                    <ektronUI:GridView ID="uxContentGrid" runat="server" AutoGenerateColumns="false"
                        EktronUIAllowResizableColumns="false" Visible="false" OnEktronUIPageChanged="uxContentGridPageChanged"
                        OnEktronUISortChanged="uxContentGridSortChanged">
                        <Columns>
                            <ektronUI:CheckBoxField ID="uxSelectColumn">
                                <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                                <CheckBoxFieldColumnStyle Visible="false" />
                            </ektronUI:CheckBoxField>
                            <asp:TemplateField meta:resourceKey="columnHeaderSelect">
                                <ItemTemplate>
                                    <input type="checkbox" id="selectColumn" class="contentselectorcheckbox" data-ektron-id='C_<%# Eval("ID") %>_<%# Eval("LanguageID") %>'
                                        data-ektron-title='<%#  Eval("Title") %>' />
                                    <input type="hidden" id="hdnPath" class="hdnPath" value='<%# Eval("ContentPath") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="columnHeaderContentTitle" SortExpression="Title">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="uximageicon" Text='<%#this.GetImageByContentType((Ektron.Cms.ContentData)Container.DataItem)%>' />
                                    <input type="hidden" id='uxTypesrc_<%# Eval("ID") %>_<%# Eval("LanguageID") %>' class="uxTypesrchidden"
                                        value='<%#this.GetImageByContentType((Ektron.Cms.ContentData)Container.DataItem)%>' />
                                    <asp:Literal runat="server" ID="uxContentType" Text='<%#  Eval("Title") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="columnHeaderContentType" SortExpression="Type">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="uxContentType" Text='<%#this.GetContentType((Ektron.Cms.ContentData)Container.DataItem)%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DateModified" SortExpression="DateModified" meta:resourceKey="columnHeaderContentDateModified" />
                            <asp:TemplateField meta:resourceKey="columnHeaderContentLanguage">
                                <ItemTemplate>
                                    <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageId")))%>'
                                        alt='<%# Eval("LanguageId") %>' title='<%# Eval("LanguageId") %>' />
                                    <input type="hidden" id='uximgsrc_<%# Eval("ID") %>_<%# Eval("LanguageID") %>' class="uximgsrchidden"
                                        value='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageId")))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ektronUI:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
