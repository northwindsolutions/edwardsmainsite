﻿namespace Ektron.Workarea.Packaging
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Content;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using Ektron.Cms.Framework.UI.Tree;

    public partial class FolderAndContentSelector : System.Web.UI.UserControl
    {
        protected long selectedFolderID = 0;
        protected int selectedLanguageID;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private ContentAPI _ContentApi;
        private EkMessageHelper _MessageHelper;
        private SiteAPI siteAPI = new SiteAPI();
        private ContentCriteria criteria;
        PackageManager packageManager;
        Package package;
        Guid packageId = Guid.Empty;
        public bool HasPackageFolders = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            this.selectedLanguageID = siteAPI.RequestInformationRef.ContentLanguage;
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;
            _ContentApi = new ContentAPI();
            _MessageHelper = _ContentApi.EkMsgRef;

            uxContentGrid.EktronUIOrderByFieldText = ContentProperty.Title.ToString();
            if (!Page.IsPostBack)
            {
                bindLanguageList();
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["uxContentsTab$uxFolderContentTab$uxFolderContentSelector$uxLanguageList"]))
                    int.TryParse(Request.Form["uxContentsTab$uxFolderContentTab$uxFolderContentSelector$uxLanguageList"], out this.selectedLanguageID);
            }

            packageManager = new PackageManager();
            if (Request.QueryString["id"] != null)
            {
                Guid.TryParse(Request.QueryString["id"].ToString(), out packageId);
                package = packageManager.GetItem(packageId);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.criteria = this.getContentCriteria();
            uxContentMessage.Visible = false;

            if (Page.IsPostBack)
            {
                // Folder Change Event
                if (!String.IsNullOrEmpty(uxSourceFolder.Value) && uxSourceFolder.Value != uxOldSourceFolder.Value)
                {
                    this.uxFolder_Change();
                }
            }
            else
            {
                uxFolderTree.Visible = true;
                uxContentMessage.Visible = true;
                uxContentMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxContentMessage.Text = GetLocalResourceObject("messageSelectFolder").ToString();
            }
        }

        protected void uxFolder_Change()
        {
            this.criteria = this.getContentCriteria();
            this.bindData();
            uxFolderTree.Visible = false;
            uxOldSourceFolder.Value = selectedFolderID.ToString();
        }

        protected void uxLanguageList_Change(object sender, EventArgs e)
        {
            int.TryParse(uxLanguageList.SelectedValue, out this.selectedLanguageID);
            this.criteria = this.getContentCriteria();
            this.bindData();
        }

        protected void uxContentGridPageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            this.criteria = this.getContentCriteria();
            this.criteria.PagingInfo = e.PagingInfo;
            this.criteria.OrderByField = (ContentProperty)Enum.Parse(typeof(ContentProperty), uxContentGrid.EktronUIOrderByFieldText);
            this.criteria.OrderByDirection = uxContentGrid.EktronUIOrderByDirection;
            this.bindData();
        }

        protected void uxContentGridSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            this.criteria = this.getContentCriteria();
            this.criteria.OrderByField = (ContentProperty)Enum.Parse(typeof(ContentProperty), e.OrderByFieldText);
            this.criteria.OrderByDirection = e.OrderByDirection;
            this.bindData();
        }

        protected void PreSerializeHandler(object sender, EventArgs e)
        {
            if (uxFolderTree != null && uxFolderTree.Items is Ektron.Cms.Framework.UI.Tree.TreeNodeCollection)
            {
                if (this.package != null && this.package.Folders != null && this.package.Folders.Count > 0)
                    HasPackageFolders = true;

                ModifyFolderTree(uxFolderTree.Items);
            }
        }

        /// <summary>
        /// Exclude Calendar and Commerce Folders from TREE, And Also Check the FolderSelected when It is in Package Folder list.
        /// </summary>
        /// <param name="nodeList"></param>
        private void ModifyFolderTree(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection nodeList)
        {
            for (int index = nodeList.Count - 1; index >= 0; index--)
            {
                var treeNode = (FolderTreeNode)nodeList[index];

                if (treeNode.FolderType == 8 || treeNode.FolderType == 9 || treeNode.FolderType == 3 || treeNode.FolderType == 1)
                {
                    // Exclude Ecommerce, Blogs, Forums (Discussion Board) and Calendar Folders from Tree                 
                    nodeList.RemoveAt(index);
                }
                else
                {
                    // Select the Folder when It is in Package Folders List.
                    if (HasPackageFolders)
                        treeNode.Selected = this.package.Folders.Contains(new FolderDataIdentifier { ID = long.Parse(treeNode.Id) });
                    if (treeNode.HasChildren)
                        this.ModifyFolderTree(nodeList[index].Items);
                }
            }
        }

        /// <summary>
        /// Get Content Related Image Icon
        /// </summary>
        /// <param name="cdata"></param>
        /// <returns></returns>
        protected string GetImageByContentType(ContentData cdata)
        {
            var image = string.Empty;
            if (cdata.ContType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentType.Content.GetHashCode() || cdata.ContType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentType.Archive_Content.GetHashCode())
            {
                if (cdata.SubType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderData.GetHashCode())
                {
                    image = "<img title=\"Content\" src=\"" + _ContentApi.AppImgPath + "layout_content.png" + "\" />";
                }
                else if (cdata.SubType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderMasterData.GetHashCode())
                {
                    image = "<img title=\"Content\" src=\"" + _ContentApi.AppImgPath + "layout_content.png" + "\"  />";
                }

                else
                {
                    if (cdata.ContType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentType.Archive_Content.GetHashCode())
                    {
                        image = "<img title=\"Content\" src=\"" + _ContentApi.ApplicationPath + "Images/ui/icons/contentArchived.png\" />";
                    }
                    else
                    {
                        image = "<img title=\"Content\" src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/contentHtml.png\"  />";
                    }
                }
            }
            else
            {
                image = "<span>" + cdata.AssetData.Icon + "</span>";
            }

            return image;
        }

        /// <summary>
        /// Get Content Type Text based on Enum.
        /// </summary>
        /// <param name="contenttype">EkEnumeration.CMSContentType</param>
        /// <returns>Text</returns>
        protected string GetContentType(ContentData cdata)
        {
            try
            {
                return GetContentTypeText(long.Parse(cdata.ContType.GetHashCode().ToString()), cdata.XmlConfiguration.Id, long.Parse(cdata.SubType.GetHashCode().ToString()), "");
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GetContentTypeText(long contentType, long xmlId, long contentSubType, string extension)
        {
            string result = "";
            switch (contentType)
            {
                case 1: // Content or Smart Form
                    if (xmlId > 0)
                    {
                        result = (string)(_MessageHelper.GetMessage("lbl smart form") + ": " + _ContentApi.GetXmlConfiguration(xmlId).Title.ToString());
                    }
                    else
                    {
                        switch (contentSubType)
                        {
                            case 1://Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderData
                                // this is a Page Layout
                                result = _MessageHelper.GetMessage("lbl pagebuilder layouts");
                                break;
                            case 3://Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderMasterData
                                // this is a Master Page Layout
                                result = _MessageHelper.GetMessage("lbl pagebuilder master layouts");
                                break;
                            case 2:// Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.WebEvent:
                                // this is a web event, which indicates this is a Calendar Event entry
                                result = _MessageHelper.GetMessage("calendar event");
                                break;
                            default:
                                result = _MessageHelper.GetMessage("lbl html content");
                                break;
                        }
                    }
                    break;
                case 2: // HTML Form/Survey
                    result = _MessageHelper.GetMessage("lbl html formsurvey");
                    break;
                case 3: // Archived Content
                    result = _MessageHelper.GetMessage("archive content");
                    break;
                case 4: // Archived Form/Survey
                    result = _MessageHelper.GetMessage("archive forms survey");
                    break;
                case 7: // Library Item
                    result = _MessageHelper.GetMessage("lbl library item");
                    break;
                case 8: //Asset
                    result = _MessageHelper.GetMessage("lbl asset");
                    break;
                case 9: // Non Image Library Item
                    result = _MessageHelper.GetMessage("nonimage library item");
                    break;
                case 10: // PDF
                    result = _MessageHelper.GetMessage("content:asset:pdf");
                    break;
                case 12: // Archived Media
                    result = _MessageHelper.GetMessage("lbl archived media");
                    break;
                case 13: // Blog Comment
                    result = _MessageHelper.GetMessage("lbl blog comment");
                    break;
                case 14: // Smart Form
                    if (xmlId > 0)
                    {
                        result = (string)(_MessageHelper.GetMessage("lbl smart form") + ": " + _ContentApi.GetXmlConfiguration(xmlId).Title.ToString());
                    }
                    break;
                case 98: // Non Library Form
                    result = _MessageHelper.GetMessage("nonlibrary form");
                    break;
                case 99: // Non Library Content
                    result = _MessageHelper.GetMessage("nonlibrary content");
                    break;
                case 101: // Microsoft Office Documents
                case 1101:
                    result = _MessageHelper.GetMessage("office document");
                    break;
                case 102: // Managed Assets (Non-office Documents - pdf, txt, etc.)
                case 1102:
                    result = _MessageHelper.GetMessage("managed asset");
                    break;
                case 106: //Image assets - jpg, tif, gif
                case 1106:
                    switch (extension.ToLower())
                    {
                        case ".gif":
                            result = _MessageHelper.GetMessage("content:asset:image:gif");
                            break;
                        case ".jpeg":
                            result = _MessageHelper.GetMessage("content:asset:image:jpeg");
                            break;
                        case ".jpg":
                            result = _MessageHelper.GetMessage("content:asset:image:jpg");
                            break;
                        case ".png":
                            result = _MessageHelper.GetMessage("content:asset:image:png");
                            break;
                        case ".bmp":
                            result = _MessageHelper.GetMessage("content:asset:image:bmp");
                            break;
                        default:
                            //generic Image Asset label will be displayed for other image file types.
                            result = _MessageHelper.GetMessage("content:asset:image");
                            break;
                    }
                    break;
                case 104: // Multi Media
                case 1104:
                    result = _MessageHelper.GetMessage("lbl multimedia");
                    break;
                case 1111: // Discussion Topic
                    result = _MessageHelper.GetMessage("discussion topic");
                    break;
                case 3333: // Catalog Entry
                    if (xmlId > 0)
                    {
                        result = (string)(_ContentApi.GetXmlConfiguration(xmlId).Title.ToString());
                    }
                    else
                    {
                        result = _MessageHelper.GetMessage("catalog entry");
                    }
                    break;
                default:
                    switch (extension.ToLower())
                    {
                        case ".pdf":
                            result = _MessageHelper.GetMessage("content:asset:pdf");
                            break;
                        case ".zip":
                            result = _MessageHelper.GetMessage("content:asset:zip");
                            break;
                        default:
                            result = _MessageHelper.GetMessage("unknown content type");
                            break;
                    }
                    break;

            }
            return result;
        }

        private ContentCriteria getContentCriteria()
        {
            long.TryParse(uxSourceFolder.Value, out selectedFolderID);

            ContentCriteria contentCriteria = new ContentCriteria();
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = siteAPI.RequestInformationRef.PagingSize;
            contentCriteria.PagingInfo = pagingInfo;
            contentCriteria.OrderByField = ContentProperty.Title;
            contentCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            contentCriteria.AddFilter(ContentProperty.FolderId, CriteriaFilterOperator.EqualTo, selectedFolderID);
            contentCriteria.AddFilter(ContentProperty.LanguageId, CriteriaFilterOperator.EqualTo, selectedLanguageID);
            return contentCriteria;
        }

        private void bindData()
        {
            IContentManager contentManager = ObjectFactory.GetContent(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());

            List<ContentData> contentList = contentManager.GetList(criteria);

            if (contentList != null && contentList.Count > 0)
            {
                uxContentGrid.EktronUIPagingInfo = criteria.PagingInfo;
                uxContentGrid.EktronUIOrderByFieldText = criteria.OrderByDirection.ToString();
                uxContentGrid.DataSource = contentList;
                uxContentGrid.DataBind();
                uxContentGrid.Visible = true;
                uxLanguageList.Visible = true;
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
            }
            else
            {
                uxContentGrid.DataSource = null;
                uxContentGrid.DataBind();
                uxContentGrid.Visible = false;
                uxContentMessage.Visible = true;
                uxContentMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxContentMessage.Text = GetLocalResourceObject("MessageNoContent").ToString();
            }

        }

        private void bindLanguageList()
        {
            LanguageData[] language_data = siteAPI.GetAllActiveLanguages();

            var languages = from LanguageData in language_data
                            select new
                            {
                                Name = LanguageData.Name,
                                value = LanguageData.Id
                            };
            uxLanguageList.DataSource = languages;
            uxLanguageList.DataTextField = "Name";
            uxLanguageList.DataValueField = "value";
            if (string.IsNullOrEmpty(selectedLanguageID.ToString()) || selectedLanguageID.ToString() == "-1")
                uxLanguageList.SelectedValue = CommonApi.Current.ContentLanguage.GetHashCode().ToString();
            else
                uxLanguageList.SelectedValue = selectedLanguageID.ToString();
            uxLanguageList.DataBind();
        }

    }
}