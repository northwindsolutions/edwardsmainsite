﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using Ektron.Cms;
    using Ektron.Cms.Framework.UI;

    public partial class FolderSystemTree : System.Web.UI.UserControl
    {
        public string Filter = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsCss();
        }

        protected void RegisterJsCss()
        {
            string sitePath = new CommonApi().SitePath;
            Ektron.Cms.API.Css.RegisterCss(this, string.Format("{0}/workarea/java/plugins/treeview/ektron.treeview.css", sitePath), "treeviewektronCSS");
            Ektron.Cms.API.JS.RegisterJS(this, string.Format("{0}/workarea/java/plugins/treeview/ektron.treeview.js", sitePath), "treeviewektronJS");
        }
    }
}