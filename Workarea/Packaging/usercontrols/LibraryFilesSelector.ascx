﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LibraryFilesSelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.LibraryFilesSelector" %>
<div class="librarySelectorWrapper clearfix">
    <div class="folderWrapper" style="overflow: auto; white-space: nowrap;">
        <ektronUI:FolderTree ID="uxFolderTree" runat="server" PageSize="0" PageDepth="1"
            OnPreSerializeData="PreSerializeHandler" RootId="0" SelectionMode="None" UseInternalAdmin="true"
            ShowRoot="true" Visible="false">
        </ektronUI:FolderTree>
    </div>
    <div class="libraryWrapper">
        <asp:UpdatePanel ID="uxUpdatePanelLibrary" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ektronUI:JavaScriptBlock ID="JavaScriptBlock1" ExecutionMode="OnEktronReady" runat="server">
                    <ScriptTemplate>
                        Ektron.Workarea.Packaging.syncLibrarySelections();
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <div class="hdnFields">
                    <input type="hidden" id="uxOldSourceLibraryFolder" runat="server" class="hdnOldSourceLibraryFolder" />
                    <input type="hidden" id="uxSourceLibraryFolder" runat="server" class="hdnSourceLibraryFolder" />
                    <asp:Button ID="uxTriggerGetLibraryContent" runat="server" CssClass="uxTriggerGetLibraryContent"
                        Style="display: none;" />
                </div>
                <div class="LibraryFilters">
                    <asp:DropDownList ID="uxLibraryTypes" runat="server" Visible="false" AutoPostBack="true"
                        OnSelectedIndexChanged="uxLibraryTypes_Change">
                        <asp:ListItem Selected="True" Text="<%$ Resources:libraryImages %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:libraryFiles %>" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                    <span style="float: right;">
                        <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
                        <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.libraryselectAllSummary();"
                            Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
                        <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.librarydeselectAllSummary();"
                            Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
                    </span>
                </div>
                <div class="libraryBrowserContainer">
                    <ektronUI:Message ID="uxLibraryMessage" runat="server">
                    </ektronUI:Message>
                    <ektronUI:GridView ID="uxLibraryGrid" runat="server" AutoGenerateColumns="false"
                        EktronUIAllowResizableColumns="false" Visible="false" OnEktronUIPageChanged="uxLibraryGridPageChanged"
                        OnEktronUISortChanged="uxLibraryGridSortChanged">
                        <Columns>
                            <ektronUI:CheckBoxField ID="uxSelectColumn">
                                <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                                <CheckBoxFieldColumnStyle Visible="false" />
                            </ektronUI:CheckBoxField>
                            <asp:TemplateField meta:resourceKey="columnHeaderSelect">
                                <ItemTemplate>
                                    <input type="checkbox" id="selectColumn" class="libraryselectorcheckbox" data-ektron-id='L_<%# Eval("ContentID") %>_<%# Eval("LanguageID") %>'
                                        data-ektron-title='<%#  Eval("Title") %>' />
                                    <input type="hidden" id="hdnLibPath" class="hdnLibPath" value='<%#  Eval("FileName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="columnHeaderLibraryTitle" SortExpression="Title">
                                <ItemTemplate>
                                    <asp:Literal ID="uxLibTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="columnHeaderLibraryFileName" SortExpression="FileName">
                                <ItemTemplate>
                                    <asp:Literal ID="uxFileName" runat="server" Text='<%# Eval("FileName") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ektronUI:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
