﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Content;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;

    public partial class LibraryFilesSelector : System.Web.UI.UserControl
    {
        protected long selectedFolderID = 0;
        protected LibraryType selectedLibraryType = LibraryType.images;
        private LibraryData[] library_data = new LibraryData[0];
        private SiteAPI siteAPI = new SiteAPI();
        private LibraryCriteria criteria;

        protected void Page_Init(object sender, EventArgs e)
        {
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;
            uxLibraryGrid.EktronUIOrderByFieldText = LibraryProperty.ContentId.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.criteria = this.GetLibraryCriteria();
            uxLibraryMessage.Visible = false;

            if (Page.IsPostBack)
            {
                // Folder Change Event
                if (!String.IsNullOrEmpty(uxSourceLibraryFolder.Value) && uxSourceLibraryFolder.Value != uxOldSourceLibraryFolder.Value)
                {
                    this.uxFolder_Change();
                }
            }
            else
            {
                uxFolderTree.Visible = true;
                uxLibraryMessage.Visible = true;
                uxLibraryMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxLibraryMessage.Text = GetLocalResourceObject("messageSelectFolder").ToString();
            }
        }

        protected void PreSerializeHandler(object sender, EventArgs e)
        {
            if (uxFolderTree != null && uxFolderTree.Items is Ektron.Cms.Framework.UI.Tree.TreeNodeCollection)
            {
                ModifyFolderTree(uxFolderTree.Items);
            }
        }

        /// <summary>
        /// Exclude Calendar and Commerce Folders from TREE.
        /// </summary>
        /// <param name="nodeList"></param>
        private void ModifyFolderTree(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection nodeList)
        {
            for (int index = nodeList.Count - 1; index >= 0; index--)
            {
                var treeNode = (FolderTreeNode)nodeList[index];

                if (treeNode.FolderType == 8 || treeNode.FolderType == 9 || treeNode.FolderType == 3 || treeNode.FolderType == 1)
                {
                    // Exclude Ecommerce, Blogs, Forums (Discussion Board) and Calendar Folders from Tree
                    nodeList.RemoveAt(index);
                }
                else
                {
                    if (treeNode.HasChildren)
                        this.ModifyFolderTree(nodeList[index].Items);
                }
            }
        }

        protected void uxFolder_Change()
        {
            this.GetSelectedLibraryType();
            this.criteria = this.GetLibraryCriteria();
            this.bindData();
            uxFolderTree.Visible = false;
            uxOldSourceLibraryFolder.Value = selectedFolderID.ToString();
        }

        protected void uxLibraryTypes_Change(object sender, EventArgs e)
        {
            this.GetSelectedLibraryType();
            this.criteria = this.GetLibraryCriteria();
            this.bindData();
        }

        private void GetSelectedLibraryType()
        {
            if (uxLibraryTypes.SelectedValue.ToString() == "1")
                this.selectedLibraryType = LibraryType.images;
            else if (uxLibraryTypes.SelectedValue.ToString() == "2")
                this.selectedLibraryType = LibraryType.files;
        }

        protected void uxLibraryGridPageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            this.criteria = this.GetLibraryCriteria();
            this.criteria.PagingInfo = e.PagingInfo;
            this.criteria.OrderByField = (LibraryProperty)Enum.Parse(typeof(LibraryProperty), uxLibraryGrid.EktronUIOrderByFieldText);
            this.criteria.OrderByDirection = uxLibraryGrid.EktronUIOrderByDirection;
            this.bindData();
        }

        protected void uxLibraryGridSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            this.criteria = this.GetLibraryCriteria();
            this.criteria.OrderByField = (LibraryProperty)Enum.Parse(typeof(LibraryProperty), e.OrderByFieldText);
            this.criteria.OrderByDirection = e.OrderByDirection;
            this.bindData();
        }

        private LibraryCriteria GetLibraryCriteria()
        {
            long.TryParse(uxSourceLibraryFolder.Value, out selectedFolderID);

            LibraryCriteria libraryCriteria = new LibraryCriteria();
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = siteAPI.RequestInformationRef.PagingSize;
            libraryCriteria.PagingInfo = pagingInfo;
            libraryCriteria.OrderByField = LibraryProperty.ContentId;
            libraryCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            libraryCriteria.AddFilter(LibraryProperty.ParentId, CriteriaFilterOperator.EqualTo, this.selectedFolderID);
            libraryCriteria.AddFilter(LibraryProperty.TypeId, CriteriaFilterOperator.EqualTo, this.selectedLibraryType);
            libraryCriteria.AddFilter(LibraryProperty.ContentType, CriteriaFilterOperator.EqualTo, EkEnumeration.CMSContentType.LibraryItem);

            return libraryCriteria;
        }

        private void bindData()
        {
            ILibraryManager libraryManager = ObjectFactory.GetLibraryManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());

            List<LibraryData> libraryList = libraryManager.GetList(criteria);

            if (libraryList != null && libraryList.Count > 0)
            {
                uxLibraryGrid.EktronUIPagingInfo = criteria.PagingInfo;
                uxLibraryGrid.EktronUIOrderByFieldText = criteria.OrderByDirection.ToString();
                uxLibraryGrid.DataSource = libraryList;
                uxLibraryGrid.DataBind();
                uxLibraryGrid.Visible = true;
                uxLibraryTypes.Visible = true;
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
            }
            else
            {
                uxLibraryGrid.DataSource = null;
                uxLibraryGrid.DataBind();
                uxLibraryGrid.Visible = false;
                uxLibraryMessage.Visible = true;
                uxLibraryMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxLibraryTypes.Visible = true;
                uxLibraryMessage.Text = GetLocalResourceObject("MessageNoContent").ToString();
            }

        }

    }
}