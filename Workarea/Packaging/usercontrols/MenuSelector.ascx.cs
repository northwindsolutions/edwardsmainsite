﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework;
    using Ektron.Cms.Organization;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

    public partial class MenuSelector : UserControl
    {
        List<LanguageModel> langList = new List<LanguageModel>();
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private SiteAPI siteAPI = new SiteAPI();
        private MenuCriteria criteria;
        protected string languageID;

        protected void Page_Init(object sender, EventArgs e)
        {
            languageID = SiteAPI.Current.ContentLanguage.GetHashCode().ToString();
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;
            if (!Page.IsPostBack)
            {
                bindLanguageList();
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["uxContentsTab$uxMenuTab$uxMenuSelector$uxLanguageList"]))
                    languageID = Request.Form["uxContentsTab$uxMenuTab$uxMenuSelector$uxLanguageList"];
            }

            this.criteria = this.GetMenuCriteria();
            this.BindMenuList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            uxInfoMessage.Text = GetLocalResourceObject("messageMenuSelectorInfo").ToString();
        }

        private void bindLanguageList()
        {
            SiteAPI siteAPI = new SiteAPI();
            LanguageData[] language_data = siteAPI.GetAllActiveLanguages();

            var languages = from LanguageData in language_data
                            select new LanguageModel
                            {
                                Name = LanguageData.Name,
                                LanguageID = LanguageData.Id
                            };

            langList = new List<LanguageModel>();
            langList.Add(new LanguageModel { Name = "All", LanguageID = -1 });
            langList.AddRange(languages);
            uxLanguageList.DataSource = langList;
            uxLanguageList.DataTextField = "Name";
            uxLanguageList.DataValueField = "LanguageID";
            languageID = uxLanguageList.SelectedValue = "-1";
            uxLanguageList.DataBind();
        }

        protected void uxLanguageList_Change(object sender, EventArgs e)
        {
            languageID = uxLanguageList.SelectedValue;
            this.BindMenuList();
        }

        private void BindMenuList()
        {
            var menuModel = new List<MenuModel>();
            var menuManager = new MenuManager(ApiAccessMode.Admin);
            var menuItems = menuManager.GetMenuList(this.criteria);
            if (menuItems != null && menuItems.Any())
            {
                menuItems = CleanMenuList(menuItems);
                List<MenuModelItem> clientMenuItems = menuItems.ConvertAll<MenuModelItem>(new Converter<Ektron.Cms.Organization.MenuData, MenuModelItem>(
                    delegate(Ektron.Cms.Organization.MenuData data)
                    {
                        return new MenuModelItem { Title = data.Text, MenuID = data.Id, LanguageID = int.Parse(languageID) };
                    }
                ));

                MenuModel model = new MenuModel();
                model.LanguageID = int.Parse(languageID);
                model.Items = clientMenuItems;
                menuModel.Add(model);
            }
            if (menuModel != null && menuModel.Any())
            {
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
                uxMenuListsGrid.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxMenuListsGrid.EktronUIOrderByFieldText = this.criteria.OrderByDirection.ToString();
                uxMenuListsGrid.DataSource = menuModel[0].Items;
                uxMenuListsGrid.DataBind();
            }
        }

        private List<Ektron.Cms.Organization.MenuData> CleanMenuList(List<Ektron.Cms.Organization.MenuData> list)
        {
            // Remove Duplicate Menu's.
            var filteredmenulist = new List<Ektron.Cms.Organization.MenuData>();
            filteredmenulist = list.GroupBy(x => x.Id).Select(z => z.First()).ToList();

            return filteredmenulist;
        }

        protected void uxMenuListsGrid_GridViewEktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria = this.GetMenuCriteria();
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByField = MenuProperty.Text;
                this.criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                this.BindMenuList();
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }

        private MenuCriteria GetMenuCriteria()
        {
            MenuCriteria menuCriteria = new MenuCriteria();
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = siteAPI.RequestInformationRef.PagingSize;
            menuCriteria.PagingInfo = pagingInfo;
            menuCriteria.OrderByField = MenuProperty.LanguageId;
            menuCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            menuCriteria.AddFilter(MenuProperty.Id, CriteriaFilterOperator.GreaterThan, 0);
            menuCriteria.AddFilter(MenuProperty.Type, CriteriaFilterOperator.EqualTo, 0);
            menuCriteria.AddFilter(MenuProperty.LanguageId, Cms.Common.CriteriaFilterOperator.GreaterThan, 0);
            return menuCriteria;
        }

        public string GetRowDataMenuId(long menuId, int langId)
        {
            return "M_" + menuId.ToString() + "_" + langId.ToString();
        }

        private class LanguageModel
        {
            public string Name { get; set; }
            public int LanguageID { get; set; }
        }

        private class MenuModel
        {
            public int LanguageID { get; set; }
            public List<MenuModelItem> Items { get; set; }
        }

        public class MenuModelItem
        {
            public long MenuID { get; set; }
            public int LanguageID { get; set; }
            public string Title { get; set; }
        }
    }
}
