﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using System.Web.UI.HtmlControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

    public partial class PackageDefinitionSelector : System.Web.UI.UserControl
    {
        PackageManager packageManager;
        Guid packageId;
        Package package;
        private SiteAPI siteAPI = new SiteAPI();
        private PackageCriteria criteria;

        protected void Page_Init(object sender, EventArgs e)
        {
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;
            packageManager = new PackageManager();
            if (Request.QueryString["id"] != null)
            {
                Guid.TryParse(Request.QueryString["id"].ToString(), out packageId);
                uxcurrentPackageID.Value = packageId.ToString();
                package = packageManager.GetItem(packageId);
            }
            uxPackageList.RowDataBound += new GridViewRowEventHandler(uxPackageList_RowDataBound);
            this.criteria = this.GetPackageCriteria();
            bindDefaultPackageList();
        }

        /// <summary>
        /// Disable Current Package Definition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void uxPackageList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                var packagedef = (Package)e.Row.DataItem;
                if (packagedef.Id == packageId)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        var packagedefcheckbox = (HtmlInputCheckBox)e.Row.FindControl("packagedefcheck");
                        if (packagedefcheckbox != null)
                        {
                            packagedefcheckbox.Checked = true;
                            packagedefcheckbox.Attributes.Add("checked", "checked");
                            packagedefcheckbox.Disabled = true;
                        }
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            uxInfoMessage.Text = GetLocalResourceObject("messagePackageSelectorInfo").ToString();
        }

        private void bindDefaultPackageList()
        {
            List<Package> packages = packageManager.GetList(this.criteria);
            if (packages != null && packages.Count > 0)
            {
                uxPackageList.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxPackageList.EktronUIOrderByFieldText = this.criteria.OrderByDirection.ToString();
                uxPackageList.DataSource = packages;
                uxPackageList.DataBind();
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
            }
        }

        protected void uxPackageList_GridViewEktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria = this.GetPackageCriteria();
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByField = PackageProperty.Name;
                this.criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                this.bindDefaultPackageList();
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }

        private PackageCriteria GetPackageCriteria()
        {
            PackageCriteria pkgCriteria = new PackageCriteria();
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = siteAPI.RequestInformationRef.PagingSize;
            pkgCriteria.PagingInfo = pagingInfo;
            pkgCriteria.OrderByField = PackageProperty.Name;
            pkgCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            pkgCriteria.AddFilter(PackageProperty.Id, CriteriaFilterOperator.IsNotNull, null);
            return pkgCriteria;
        }

        public string GetPackageDefinitionName(Package package)
        {
            if (package != null)
            {
                return package.Name + " - " + package.Id;
            }

            return string.Empty;
        }
    }
}