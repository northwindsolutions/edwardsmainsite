﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PackageSummary.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.PackageSummary" %>
<div class="clearfix" >
    <div id="uxSummaryHeaderContainer">
        <div class="leftFilters">
            <asp:Label ID="uxFilterLabel" runat="server" meta:resourcekey="uxFilterLabel"></asp:Label>
            <asp:DropDownList ID="uxFilterList" runat="server" CssClass="summaryFilterList">
                <asp:ListItem meta:resourceKey="uxFilterAllOption" Value="0"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterContentOption" Value="1"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterFolderOption" Value="2"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterLibraryOption" Value="3"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterFileSystemOption" Value="4"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterTaxonomyOption" Value="5"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterMenuOption" Value="7"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterCollectionOption" Value="8"></asp:ListItem>
                <asp:ListItem meta:resourceKey="uxFilterPackageDefinitionOption" Value="6"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="rightFilters">
            <ektronUI:Button ID="uxViewOrphanListButton" runat="server" Visible="false" meta:resourcekey="uxViewOrphanListButton"></ektronUI:Button>
        </div>
    </div>
    <div id="uxSummaryContentsContainer">
        <div class="contentsScroll">
            <table id="uxTableHead" class="ektronGrid">
                <tr id="trHeader">
                    <th class="column-type urlTypesLabel" id="uxDelete">
                        <asp:Label ID="uxDeleteLabel" runat="server" meta:resourcekey="uxDeleteLabel"></asp:Label>
                    </th>
                    <th class="column-type urlTypesLabel" id="uxType">
                        <asp:Label ID="uxTypeLabel" runat="server" meta:resourcekey="uxTypeLabel"></asp:Label>
                    </th>
                    <th class="column-type urlTypesLabel" id="uxTitle">
                        <asp:Label ID="uxTitleLabel" runat="server" meta:resourcekey="uxTitleLabel"></asp:Label>
                    </th>
                    <th class="column-type urlTypesLabel" id="uxPath">
                        <asp:Label ID="uxPathLabel" runat="server" meta:resourcekey="uxPathLabel"></asp:Label>
                    </th>
                    <th class="column-type urlTypesLabel" id="uxLanguage">
                        <asp:Label ID="uxLanguageLabel" runat="server" meta:resourcekey="uxLanguageLabel"></asp:Label>
                    </th>
                </tr>
                <asp:Repeater ID="uxPackageSummaryFolder" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("ID") %>' class="summaryitemfolder">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='F_<%# Eval("ID") %>' />
                            </td>
                            <td>
                                <img alt="folderIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.Folder %>'
                                    src="../images/UI/icons/folder.png" />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemTitle" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="uxItemPath" runat="server" Text='<%# Eval("NameWithPath") %>'></asp:Literal>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageSummaryContent" runat="server">
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("ID") %>_<%# Eval("LanguageID") %>' class="summaryitemcontent">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='C_<%# Eval("ID") %>_<%# Eval("LanguageID") %>' />
                            </td>
                            <td>
                                <asp:Literal runat="server" ID="uximageicon" Text='<%#this.GetImageByContentType((Ektron.Cms.ContentData)Container.DataItem)%>' />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="uxItemPath" runat="server" Text='<%# Eval("ContentPath") %>'></asp:Literal>
                            </td>
                            <td>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageId")))%>'
                                    alt='<%# Eval("LanguageId") %>' title='<%# Eval("LanguageId") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageSummaryLibrary" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("ContentId") %>_<%# Eval("LanguageID") %>' class="summaryitemlibrary">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='L_<%# Eval("ContentId") %>_<%# Eval("LanguageID") %>' />
                            </td>
                            <td>
                                <img alt="folderIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.Library %>'
                                    src="../images/UI/icons/bookGreen.png" />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("FileName") %>'></asp:Literal>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageFileSystem" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# GetValidFilePath(((string)Eval("FilePath"))) %>' class="summaryitemFilesystem">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='FS_<%# Eval("FilePath") %>' />
                            </td>
                            <td>
                                <img alt="folderIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.Files %>'
                                    src="../images/UI/icons/contentStack.png" />
                            </td>
                            <td>
                                <asp:Literal ID="Literal1" runat="server" Text='<%# GetFileName((string)Eval("FilePath")) %>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("FilePath") %>'></asp:Literal>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageDefinition" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("Id") %>' class="summaryitemPackageDef">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='P_<%# Eval("Id") %>' />
                            </td>
                            <td>
                                <img alt="packageIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.LocalPackage %>'
                                    src="../images/UI/icons/package.png" />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="uxItemID" runat="server" Text='<%# Eval("Id") %>'></asp:Literal>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageSummaryTaxonomy" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("TaxonomyId") %>_<%# Eval("TaxonomyLanguage") %>' class="summaryitemTaxonomy">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='T_<%# Eval("TaxonomyId") %>_<%# Eval("TaxonomyLanguage") %>' />
                            </td>
                            <td>
                                <img alt="packageIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.Taxonomy %>'
                                    src="../images/UI/icons/Taxonomy.png" />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                            </td>
                            <td>
                                <asp:Literal ID="uxItemPath" runat="server" Text='<%# Eval("Path") %>'></asp:Literal>
                            </td>
                            <td>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageId")))%>'
                                    alt='<%# Eval("LanguageId") %>' title="All Language" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageSummaryMenu" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("MenuID") %>_<%# Eval("LanguageID") %>' class="summaryitemMenu">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='M_<%# Eval("MenuID") %>_<%# Eval("LanguageID") %>' />
                            </td>
                            <td>
                                <img alt="packageIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.Menu %>'
                                    src="../images/UI/icons/menu.png" />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                            </td>
                            <td>
                            </td>
                            <td>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageID")))%>'
                                    alt='<%# Eval("LanguageID") %>' title="All Language" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="uxPackageSummaryCollection" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id='tr_<%# Eval("Id") %>_-1' class="summaryitemCollection">
                            <td>
                                <img alt="deleteIcon" title="Delete" src="../FrameworkUI/images/silk/icons/delete.png" class="deleteItem"
                                    data-ektron-id='CO_<%# Eval("Id") %>_-1' />
                            </td>
                            <td>
                                <img alt="packageIcon" title='<%# Ektron.Workarea.Packaging.PackageItemType.Collection %>'
                                    src="../images/UI/icons/collection.png" />
                            </td>
                            <td>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                            </td>
                            <td>
                            </td>
                            <td>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(-1))%>'
                                    alt='CollectionlanguageImage' title="All Language" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>
    <asp:PlaceHolder ID="uxOrphansection" runat="server" Visible="false">
        <div id="uxOrphanSummaryContainer" style="display: none;">
            <div class="contentsScroll">
                <ektronUI:Message ID="uxOrphanLabel" runat="server" meta:resourcekey="uxOrphanLabel"
                    DisplayMode="Warning">
                </ektronUI:Message>
                <table id="uxOrphantable" class="ektronGrid">
                    <tr id="tr1">
                        <th class="column-type urlTypesLabel" id="Th1">
                            <asp:Label ID="uxOrphanActivecheckboxLabel" runat="server" meta:resourcekey="uxActivecheckboxLabel"></asp:Label>
                        </th>
                        <th class="column-type urlTypesLabel" id="Th2">
                            <asp:Label ID="uxTypeHeaderLabel" runat="server" meta:resourcekey="uxTypeLabel"></asp:Label>
                        </th>
                        <th class="column-type urlTypesLabel" id="Th3">
                            <asp:Label ID="uxIdHeaderLabel" runat="server" meta:resourcekey="uxIDLabel"></asp:Label>
                        </th>
                        <th class="column-type urlTypesLabel" id="Th5">
                            <asp:Label ID="uxOrphanLanguageLabel" runat="server" meta:resourcekey="uxLanguageLabel"></asp:Label>
                        </th>
                    </tr>
                    <asp:Repeater ID="uxOrphanList" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <input type="checkbox" checked="checked" name="summarycheck" id="summarycheck" class="summaryitemCollection"
                                        data-ektron-id='O_<%# Eval("Type") %>_<%# Eval("Id") %>_<%# Eval("LanguageId") %>' />
                                </td>
                                <td>
                                    <img title='<%# Eval("Type") %>' alt="packageIcon" src='<%# GetTypeImage((Ektron.Workarea.Packaging.PackageItemType)Eval("Type")) %>' />
                                </td>
                                <td>
                                    <asp:Literal ID="uxOrphanID" runat="server" Text='<%# Eval("Id") %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:Literal runat="server" ID="uxlanguageimageicon" Text='<%#this.GetLanguageImage(System.Convert.ToInt32(Eval("LanguageId")))%>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </asp:PlaceHolder>
</div>
