﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TaxonomySelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.TaxonomySelector" %>
<div class="taxonomySelectorWrapper clearfix">
    <ektronUI:Message ID="uxInfoMessage" runat="server" DisplayMode="Information" Visible="true" />
    <asp:UpdatePanel ID="uxUpdatePanelTaxonomy" runat="server">
        <ContentTemplate>
            <ektronUI:JavaScriptBlock ID="uxTaxonomyJavaScriptBlock" ExecutionMode="OnEktronReady"
                runat="server">
                <ScriptTemplate>
                    Ektron.Workarea.Packaging.syncTaxonomySelections();
                </ScriptTemplate>
            </ektronUI:JavaScriptBlock>
            <div class="hdnFields">
                <input type="hidden" id="uxOldLanguage" runat="server" class="hdnOldLanguage" />
                <asp:Button ID="uxTriggerGetTaxonomy" runat="server" CssClass="uxTriggerGetTaxonomy"
                    Style="display: none;" />
            </div>
            <div class="taxonomyFilters">
                <asp:DropDownList ID="uxLanguageList" runat="server" CssClass="uxLanguageList" AutoPostBack="true" Visible="false"
                    OnSelectedIndexChanged="uxLanguageList_Change" />
                <span style="float: right;">
                    <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
                    <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.taxonomyselectAllSummary();"
                        Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
                    <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.taxonomydeselectAllSummary();"
                        Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
                </span>
            </div>
            <div class="uxTaxonomyLists">
                <ektronUI:GridView ID="uxTaxonomyListsGrid" runat="server" AutoGenerateColumns="false"
                    EktronUIAllowResizableColumns="false" Visible="true" OnEktronUIPageChanged="uxTaxonomyListsGrid_GridViewEktronUIThemePageChanged">
                    <Columns>
                        <ektronUI:CheckBoxField ID="uxSelectColumn">
                            <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                            <CheckBoxFieldColumnStyle Visible="false" />
                        </ektronUI:CheckBoxField>
                        <asp:TemplateField meta:resourceKey="uxIncludeResource">
                            <ItemTemplate>
                                <input type="checkbox" runat="server" name="uxTaxonomyitem" id="uxTaxonomyitem" class="uxTaxonomyitem"
                                    value='<%# Eval("TaxonomyID") %>' data-ektron-id='<%# GetRowDataTaxonomyId((long) Eval("TaxonomyID"),(int)Eval("LanguageID")) %>'
                                    data-ektron-title='<%#  Eval("Name") %>' data-ektron-path='<%#  Eval("Path") %>'
                                    data-ektron-lang='<%# Eval("LanguageID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="uxTaxonomyName">
                            <ItemTemplate>
                                <asp:Literal ID="uxItemID" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="uxTaxonomyPath">
                            <ItemTemplate>
                                <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("Path") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderContentLanguage">
                            <ItemTemplate>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageID")))%>'
                                    alt='<%# Eval("LanguageID") %>' title="All Language" />
                                <input type="hidden" id='uximgsrc_<%# Eval("TaxonomyID") %>_<%# Eval("LanguageID") %>'
                                    class="uximgsrchidden" value='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageID")))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </ektronUI:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
