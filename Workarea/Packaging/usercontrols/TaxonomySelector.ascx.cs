﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms.Organization;
    using Ektron.Cms;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

    public partial class TaxonomySelector : System.Web.UI.UserControl
    {
        List<LanguageModel> langList = new List<LanguageModel>();
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private SiteAPI siteAPI = new SiteAPI();
        protected string languageID;
        private TaxonomyCriteria criteria;
         
        protected void Page_Init(object sender, EventArgs e)
        {
            languageID = SiteAPI.Current.ContentLanguage.GetHashCode().ToString();
            uxSelectAllButton.Visible = false;
            uxDeselectAllButton.Visible = false;
            uxVisibleItems.Visible = false;
            if (!Page.IsPostBack)
            {
                bindLanguageList();
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["uxContentsTab$uxTaxonomyTab$uxTaxonomySelector$uxLanguageList"]))
                    languageID = Request.Form["uxContentsTab$uxTaxonomyTab$uxTaxonomySelector$uxLanguageList"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            uxInfoMessage.Text = GetLocalResourceObject("messageTaxonomySelectorInfo").ToString();
            this.criteria = this.GetTaxonomyCriteria();
            bindTaxonomyList();
        }

        private void bindLanguageList()
        {
            SiteAPI siteAPI = new SiteAPI();
            LanguageData[] language_data = siteAPI.GetAllActiveLanguages();

            var languages = from LanguageData in language_data
                            select new LanguageModel
                            {
                                Name = LanguageData.Name,
                                LanguageID = LanguageData.Id
                            };

            langList = new List<LanguageModel>();
            langList.Add(new LanguageModel { Name = "All", LanguageID = -1 });
            langList.AddRange(languages);
            uxLanguageList.DataSource = langList;
            uxLanguageList.DataTextField = "Name";
            uxLanguageList.DataValueField = "LanguageID";
            languageID = uxLanguageList.SelectedValue = "-1";
            uxLanguageList.DataBind();
        }

        protected void uxLanguageList_Change(object sender, EventArgs e)
        {
            languageID = uxLanguageList.SelectedValue;
            this.bindTaxonomyList();
        }

        private void bindTaxonomyList()
        {
            List<TaxonomyModel> taxModel = new List<TaxonomyModel>();

            TaxonomyManager taxonomyManager = new TaxonomyManager(Cms.Framework.ApiAccessMode.Admin);
            List<TaxonomyData> taxonomyItems = new List<TaxonomyData>();
            taxonomyItems = taxonomyManager.GetList(this.criteria);

            if (taxonomyItems != null && taxonomyItems.Count > 0)
            {
                List<TaxonomyModelItem> clientTaxonomyItems = taxonomyItems.ConvertAll<TaxonomyModelItem>(new Converter<TaxonomyData, TaxonomyModelItem>(
                    delegate(TaxonomyData data)
                    {
                        return new TaxonomyModelItem { Name = data.Name, Path = data.Path, TaxonomyID = data.Id, LanguageID = int.Parse(languageID) };
                    }
                ));

                TaxonomyModel model = new TaxonomyModel();
                model.LanguageID = int.Parse(languageID);
                model.Items = clientTaxonomyItems;
                taxModel.Add(model);
            }
            if (taxModel != null && taxModel.Count > 0)
            {
                uxSelectAllButton.Visible = true;
                uxDeselectAllButton.Visible = true;
                uxVisibleItems.Visible = true;
                uxTaxonomyListsGrid.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxTaxonomyListsGrid.EktronUIOrderByFieldText = this.criteria.OrderByDirection.ToString();
                uxTaxonomyListsGrid.DataSource = taxModel[0].Items;
                uxTaxonomyListsGrid.DataBind();
            }
        }

        protected void uxTaxonomyListsGrid_GridViewEktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria = this.GetTaxonomyCriteria();
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByField =  TaxonomyProperty.Name;
                this.criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                this.bindTaxonomyList();
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }

        private TaxonomyCriteria GetTaxonomyCriteria()
        {
            TaxonomyCriteria taxCriteria = new TaxonomyCriteria();
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = siteAPI.RequestInformationRef.PagingSize;
            taxCriteria.PagingInfo = pagingInfo;
            taxCriteria.OrderByField = TaxonomyProperty.Name;
            taxCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
            taxCriteria.AddFilter(TaxonomyProperty.Level, CriteriaFilterOperator.EqualTo, 1);
            taxCriteria.AddFilter(TaxonomyProperty.Type, CriteriaFilterOperator.EqualTo, 0);
            if (languageID != "-1")
            {
                taxCriteria.AddFilter(TaxonomyProperty.LanguageId, Cms.Common.CriteriaFilterOperator.EqualTo, languageID);
            }

            return taxCriteria;
        }

        public string GetRowDataTaxonomyId(long TaxoId, int TaxoLangId)
        {
            return "T_" + TaxoId.ToString() + "_" + TaxoLangId.ToString();
        }

        private class LanguageModel
        {
            public string Name { get; set; }
            public int LanguageID { get; set; }
        }

        private class TaxonomyModel
        {
            public int LanguageID { get; set; }
            public List<TaxonomyModelItem> Items { get; set; }
        }

        public class TaxonomyModelItem
        {
            public string Path { get; set; }
            public long TaxonomyID { get; set; }
            public int LanguageID { get; set; }
            public string Name { get; set; }
        }
    }
}