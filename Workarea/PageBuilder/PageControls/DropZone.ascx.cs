using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Ektron.Cms.PageBuilder;
using System.Collections.Generic;
using Ektron.Cms;
using Ektron.Newtonsoft.Json;
using System.ComponentModel;
using System.Security.Permissions;
using Ektron.Cms.Common;
using System.Linq;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.User;
using System.Web.Script.Serialization;
using Ektron.Newtonsoft.Json.Converters;
using System.Text;

namespace Ektron.Cms.PageBuilder.Controls
{
    #region JSON action classes
    [JsonObject]
    public class WidgetLocation : IWidgetDropInfo
    {
        private long _widgetTypeID;
        private string _dropZoneID;
        private long _ColumnID;
        private long _OrderID;
        private int _Width;
        private string _Unit;
        private bool _isNested;
        private long _nestedSortOrder;
        private Guid _columnGuid = Guid.Empty;
        private string _cssClass = string.Empty;
        private string _cssFramework = string.Empty;

        [JsonProperty]
        public Guid columnGuid { get { return _columnGuid; } set { _columnGuid = value; } }
        [JsonProperty]
        public long widgetTypeID { get { return _widgetTypeID; } set { _widgetTypeID = value; } }
        [JsonProperty]
        public string dropZoneID { get { return _dropZoneID; } set { _dropZoneID = value; } }
        [JsonProperty]
        public long ColumnID { get { return _ColumnID; } set { _ColumnID = value; } }
        [JsonProperty]
        public long OrderID { get { return _OrderID; } set { _OrderID = value; } }
        [JsonProperty]
        public int Width { get { return _Width; } set { _Width = value; } }
        [JsonProperty]
        public string Unit { get { return _Unit; } set { _Unit = value; } }
        [JsonProperty]
        public string CssClass { get { return _cssClass; } set { _cssClass = value; } }
        [JsonProperty]
        public string CssFramework { get { return _cssFramework; } set { _cssFramework = value; } }
        [JsonProperty]
        public bool isNested { get { return _isNested; } set { _isNested = value; } }
        [JsonProperty]
        public long nestedSortOrder { get { return _nestedSortOrder; } set { _nestedSortOrder = value; } }
    }

    [JsonObject]
    public class DropZoneInfo
    {
        private string _dropZoneID;
        private bool _isMaster;
        [JsonProperty]
        public string dropZoneID { get { return _dropZoneID; } set { _dropZoneID = value; } }
        [JsonProperty]
        public bool isMaster { get { return _isMaster; } set { _isMaster = value; } }
    }

    [JsonObject]
    public class JsonRequestDropZone
    {
        [JsonProperty]
        public string Action;
        [JsonProperty]
        public DropZoneInfo DropzoneInfo;
        [JsonProperty]
        public WidgetLocation OldWidgetLocation;
        [JsonProperty]
        public WidgetLocation NewWidgetLocation;
    }

    #region ux support

    public enum UXDropZoneActionType
    {
        SetMasterZone,
        UnsetMasterZone,
        RemoveColumn,
        AddColumn,
        ResizeColumn,
        MoveColumn
    }
    public class UXDropZoneData
    {
        public string ID { get; set; }
        public string MarkupID { get; set; }
        public bool IsDropZoneEditable { get; set; }
        public bool IsMasterZone { get; set; }
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class UXColumnData
    {
        public UXColumnData()
        {
            this.Actions = new List<UXDropZoneAction>();
            this.Index = 0;
        }
        public string ID { get; set; }
        public string ColumnGUID { get; set; }
        public string UnitName { get; set; }
        public string CssClass { get; set; }
        public string Width { get; set; }
        public string CssFramework { get; set; }
        public bool Visible { get; set; }
        public int Index { get; set; }
        public List<UXDropZoneAction> Actions { get; set; }
    }
    public class UXDropZoneAction
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public UXDropZoneActionType Action { get; set; }
        public string ConfirmationMessage { get; set; }
        public string Href { get; set; }
        public string Callback { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }

    #endregion


    #endregion

    [ToolboxData("<{0}:UCDropZone runat=\"server\"></{0}:UCDropZone>")]
    [ParseChildren(true)]
    public partial class UCDropZone : DropZone
    {
        private string appPath = "";
        protected EkRequestInformation requestInformation;

        protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
        //protected SiteAPI m_refSiteApi = new SiteAPI();

        public override UpdatePanel UpdatePanel
        {
            get
            {
                UpdatePanel up = updatepanel;
                if (this.IsUXEnabled)
                {
                    up = uxUpdatePanel;
                }
                return up;
            }
        }

        #region UX Support

        private IUser user;
        public bool IsUXUser
        {
            get
            {
                bool isUxUser = false;
                if (user == null)
                {
                    user = ObjectFactory.GetUser();
                }
                if (user.IsLoggedIn && user.IsCmsUser)
                {
                    isUxUser = true;
                }
                return isUxUser;
            }
        }
        public UXDropZoneData UXDropZoneData { get; set; }
        protected bool IsUXEnabled
        {
            get
            {
                ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
                return cmsContextService.IsDeviceHTML5 && cmsContextService.IsToolbarEnabledForTemplate;
            }
        }
        public void UXPostBackEvent(string eventArgument)
        {
            try
            {
                JsonRequestDropZone action = JsonConvert.DeserializeObject<JsonRequestDropZone>(eventArgument);
                switch (action.Action)
                {
                    case "AddColumn":
                        AddColumn_click(this, new EventArgs());
                        break;
                    case "MoveColumn":
                        MoveColumn(action);
                        break;
                    case "ResizeColumn":
                        ResizeColumn(action);
                        break;
                    case "RemoveColumn":
                        RemoveColumn(action);
                        break;
                    case "SetMasterZone":
                        SetMasterZone(action);
                        break;
                    case "AddWidget":
                        AddWidget(action);
                        break;
                    case "MoveWidget":
                        MoveWidget(action);
                        break;
                }
            }
            catch (Exception ex)
            {
                this.UXDropZoneData.HasError = true;
                this.UXDropZoneData.ErrorMessage = ex.Message;
            }
        }
        protected string GetColumnStyle(RepeaterItem e)
        {
            string data = String.Empty;
            IColumnData column = e.DataItem as IColumnData;

            if (column.width > 0)
            {
                data = " style='width:";
                string unit = (column.unit == Units.pixels) ? "px" : (column.unit == Units.em) ? "em" : "%";
                data += column.width.ToString() + unit + "'";
            }

            if (column.unit == Units.custom)
            {
                SizeType currentsize = getSizeRuleForColumn(column);
                data = " class='" + column.CssClass;
                if (currentsize != null && !string.IsNullOrEmpty(currentsize.ColumnClass))
                {
                    data += " " + currentsize.ColumnClass;
                }
                data += "'";
            }
            return data;
        }
        protected string GetColumnData(RepeaterItem e)
        {
            string data = String.Empty;
            IColumnData column = e.DataItem as IColumnData;

            if (this.IsUXEnabled && this.IsUXUser && this.isZoneEditable)
            {
                data = " data-ux-pagebuilder-column-data='";

                UXColumnData columnData = new UXColumnData();
                columnData.ID = column.columnID.ToString();
                columnData.CssClass = column.CssClass;
                columnData.ColumnGUID = column.Guid.ToString();
                columnData.UnitName = column.unit.ToString();

                string cssFramework = String.Empty;
                if (columnData.UnitName == "custom")
                {
                    List<SizeType> sizes = Ektron.Cms.PageBuilder.PageBuilderConfigReader.GetConfig().Sizes.FindAll(s => s.Enabled == true);
                    List<SizeType> NonDefaultSizes = sizes.FindAll(s => s.UnitType == Units.custom);
                    cssFramework = NonDefaultSizes.Count > 0 ? NonDefaultSizes[0].Name.ToString().ToLower() : cssFramework;
                }

                columnData.Width = column.width.ToString();
                columnData.CssFramework = cssFramework;
                columnData.Index = e.ItemIndex;

                if (this.isZoneEditable)
                {
                    UXDropZoneAction moveColumnAction = new UXDropZoneAction();
                    moveColumnAction.Action = UXDropZoneActionType.MoveColumn;
                    moveColumnAction.Text = this.GetLocalResourceObject("MoveColumn").ToString();
                    moveColumnAction.Title = this.GetLocalResourceObject("MoveColumn").ToString();
                    moveColumnAction.Href = "#MoveColumn";
                    moveColumnAction.Callback = "this.dropZoneService.move(this.pageData, this.sourceData, this.targetData);";
                    columnData.Actions.Add(moveColumnAction);

                    if (this.AllowColumnResize)
                    {
                        UXDropZoneAction resizeColumnAction = new UXDropZoneAction();
                        resizeColumnAction.Action = UXDropZoneActionType.ResizeColumn;
                        resizeColumnAction.Text = this.GetLocalResourceObject("Resize").ToString();
                        resizeColumnAction.Title = this.GetLocalResourceObject("Resize").ToString();
                        resizeColumnAction.Href = "#ResizeColumn";
                        resizeColumnAction.Callback = "this.panel.resize.show(event);";
                        columnData.Actions.Add(resizeColumnAction);

                        if (this.Columns.Count > 1)
                        {
                            UXDropZoneAction removeColumnAction = new UXDropZoneAction();
                            removeColumnAction.Action = UXDropZoneActionType.RemoveColumn;
                            removeColumnAction.Text = this.GetLocalResourceObject("Remove").ToString();
                            removeColumnAction.Title = this.GetLocalResourceObject("Remove").ToString();
                            removeColumnAction.Href = "#RemoveColumn";
                            removeColumnAction.Callback = "this.dropZoneService.remove(this.pageData, this.sourceData, this.targetData);";
                            columnData.Actions.Add(removeColumnAction);
                        }
                    }
                    if ((Page as PageBuilder).Pagedata.IsMasterLayout)
                    {
                        if (this.ZoneData.isMasterZone)
                        {
                            UXDropZoneAction setMasterZoneAction = new UXDropZoneAction();
                            setMasterZoneAction.Action = UXDropZoneActionType.SetMasterZone;
                            setMasterZoneAction.Text = this.GetLocalResourceObject("SetMasterZoneText").ToString();
                            setMasterZoneAction.Title = this.GetLocalResourceObject("SetMasterZoneText").ToString();
                            setMasterZoneAction.Href = "#SetMasterZone";
                            string setMasterZoneScript = @"
                                if (confirm('" + this.GetLocalResourceObject("LockMasterZoneConfirmationText").ToString() + @"')){
                                    this.dropZoneService.setMasterZone(this.pageData, this.sourceData, this.targetData);
                                }
                            ";
                            setMasterZoneAction.Callback = setMasterZoneScript;
                            columnData.Actions.Add(setMasterZoneAction);
                        }
                        else
                        {
                            UXDropZoneAction unsetMasterZoneAction = new UXDropZoneAction();
                            unsetMasterZoneAction.Action = UXDropZoneActionType.UnsetMasterZone;
                            unsetMasterZoneAction.Text = this.GetLocalResourceObject("UnsetMasterZoneText").ToString();
                            unsetMasterZoneAction.Title = this.GetLocalResourceObject("UnsetMasterZoneText").ToString();
                            unsetMasterZoneAction.Href = "#UnsetMasterZone";
                            string unsetMasterZoneScript = @"
                                if (confirm('" + this.GetLocalResourceObject("UnlockMasterZoneConfirmationText").ToString() + @"')){
                                    this.dropZoneService.setMasterZone(this.pageData, this.sourceData, this.targetData);
                                }
                            ";
                            unsetMasterZoneAction.Callback = unsetMasterZoneScript;
                            columnData.Actions.Add(unsetMasterZoneAction);
                        }
                    }
                    if (e.ItemIndex == ((IList)((Repeater)e.Parent).DataSource).Count - 1 && this.AllowAddColumn)
                    {
                        UXDropZoneAction addColumnAction = new UXDropZoneAction();
                        addColumnAction.Action = UXDropZoneActionType.AddColumn;
                        addColumnAction.Text = m_refMsg.GetMessage("lbl pagebuilder add column");
                        addColumnAction.Title = m_refMsg.GetMessage("lbl pagebuilder add column");
                        addColumnAction.Href = "#AddColumn";
                        addColumnAction.Callback = "this.dropZoneService.add(this.pageData, this.sourceData, this.targetData);";
                        columnData.Actions.Add(addColumnAction);
                    }
                }
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.DefaultValueHandling = DefaultValueHandling.Include;
                data += HttpUtility.HtmlAttributeEncode(JsonConvert.SerializeObject(columnData, Formatting.None, settings)) + "'";
            }
            return data;
        }
        protected override void Render(HtmlTextWriter writer)
        {
            this.Page.VerifyRenderingInServerForm(this);
            if (this.IsUXEnabled && !this.DesignMode && this.Visible && this.IsAjax && this.IsAjaxSafe() && this.isZoneEditable)
            {
                StringBuilder knockoutRebinding = new StringBuilder();
                knockoutRebinding.Append(@"Ektron.UX.apps.siteApp.rebindHandler({
                    ""type"":""column"",
                    ""selector"":""#" + uxUpdatePanel.ClientID + @"""
                });");
                JavaScript.RegisterJavaScriptBlock(this, knockoutRebinding.ToString());
            }
            base.Render(writer);
        }
        private bool IsAjax
        {
            get
            {
                bool isAjax = false;
                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                if (sm != null && sm.IsInAsyncPostBack)
                {
                    isAjax = true;
                }
                return isAjax;
            }
        }

        private bool IsAjaxSafe()
        {
            bool isAjaxSafe = true;
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm != null && sm.IsInAsyncPostBack)
            {
                isAjaxSafe = IsInUpdatingUpdatePanel();
            }
            return isAjaxSafe;
        }
        private bool IsInUpdatingUpdatePanel()
        {
            bool isInUpdatePanel = uxUpdatePanel.IsInPartialRendering;
            if (isInUpdatePanel)
            {
                return true;
            }
            Control parent = uxUpdatePanel.Parent;
            if (!(parent is HtmlForm))
            {
                do
                {
                    if ((parent as UpdatePanel) != null)
                    {
                        isInUpdatePanel = ((UpdatePanel)parent).IsInPartialRendering;
                    }
                    if (isInUpdatePanel)
                    {
                        break;
                    }
                    if (parent != null)
                        parent = parent.Parent;
                    else
                        break;
                } while (!(parent is System.Web.UI.HtmlControls.HtmlForm));
            }
            return isInUpdatePanel;
        }
        #endregion

        private List<Ektron.Cms.PageBuilder.ColumnData> _columndefs = new List<Ektron.Cms.PageBuilder.ColumnData>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public List<Ektron.Cms.PageBuilder.ColumnData> ColumnDefinitions
        {
            get { return _columndefs; }
            set { _columndefs = value; }
        }

        #region events

        public UCDropZone()
        {
            this.UXDropZoneData = new UXDropZoneData();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            requestInformation = ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
            m_refMsg = new EkMessageHelper(requestInformation);
            appPath = requestInformation.ApplicationPath;

            if (this.IsUXEnabled)
            {
                uxColumnDisplay.ItemDataBound += new RepeaterItemEventHandler(columnDisplay_ItemDataBound);
            }
            else
            {
                columnDisplay.ItemDataBound += new RepeaterItemEventHandler(columnDisplay_ItemDataBound);
            }

            (Page as PageBuilder).PageUpdated += new EventHandler(Page_Updated);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (this.IsUXEnabled && this.IsPostBack)
            {
                string target = Request.Form["__EVENTTARGET"];
                if (target == uxDropZone.UniqueID)
                {
                    string argument = Request.Form["__EVENTARGUMENT"];
                    if (argument != null && argument != "" && argument.StartsWith("{\"Action\""))
                    {
                        UXPostBackEvent(argument);
                    }
                }
            }
        }

        protected override void ZoneUpdated()
        {
            if (this.IsUXEnabled)
            {
                this.uxUXSwitch.SetActiveView(uxUXView);
                uxUpdatePanel.Visible = true;
                uxUpdatePanel.Update();
            }
            else
            {
                this.uxUXSwitch.SetActiveView(uxOriginalView);
                PBDropZoneError.Visible = false;
                updatepanel.Visible = true;
                updatepanel.Update();
            }

            if ((Page as PageBuilder).Pagedata.pageID == -1 && isZoneEditable)
            {
                if (this.IsUXEnabled)
                {
                    this.uxUpdatePanel.Visible = false;
                    this.UXDropZoneData.HasError = true;
                    this.UXDropZoneData.ErrorMessage = "Could not load content";
                }
                else
                {
                    PBDropZoneError.Visible = true;
                    updatepanel.Visible = false;
                    PBDropZoneError.InnerText = "Could not load content";
                }
            }
            else if ((Page as PageBuilder).Pagedata.pageID == 0 && isZoneEditable)
            {
                if (this.IsUXEnabled)
                {
                    this.uxUpdatePanel.Visible = false;
                    this.UXDropZoneData.HasError = true;
                    this.UXDropZoneData.ErrorMessage = "Could not restore content";
                }
                else
                {
                    PBDropZoneError.Visible = true;
                    updatepanel.Visible = false;
                    PBDropZoneError.InnerText = "Could not restore content";
                }
            }
            else
            {
                if (ZoneData == null)
                {
                    DropZoneData dzone = new DropZoneData();
                    dzone.Columns = new List<Ektron.Cms.PageBuilder.ColumnDataSerialize>();
                    dzone.DropZoneID = this.ID;
                    (Page as PageBuilder).Pagedata.Zones.Add(dzone);
                }
                if (ColumnDefinitions.Count > 0 && ZoneData.Columns.Count == 0)
                {
                    //ZoneData.Columns = ZoneData.Columns.FindAll(delegate(Ektron.Cms.PageBuilder.ColumnDataSerialize cd) { return cd.Guid != Guid.Empty; });
                    ZoneData.Columns.AddRange(ColumnDefinitions);
                }
                if (ZoneData.Columns.Count < 1)
                {
                    Ektron.Cms.PageBuilder.ColumnData col = new Ektron.Cms.PageBuilder.ColumnData();
                    col.columnID = 0;
                    ZoneData.Columns.Add(col);
                }
                //List<Ektron.Cms.PageBuilder.ColumnData> displayColumns = Columns.FindAll(delegate(Ektron.Cms.PageBuilder.ColumnData col) { return col.Guid == Guid.Empty; });
                //columnDisplay.DataSource = (ColumnDefinitions.Count > 0) ? ColumnDefinitions : columnDisplay.DataSource = displayColumns;
                if (this.IsUXEnabled)
                {
                    uxColumnDisplay.DataSource = ZoneData.Columns.FindAll(delegate(Ektron.Cms.PageBuilder.ColumnDataSerialize col) { return col.Guid == Guid.Empty; });
                    uxColumnDisplay.DataBind();
                }
                else
                {
                    columnDisplay.DataSource = ZoneData.Columns.FindAll(delegate(Ektron.Cms.PageBuilder.ColumnDataSerialize col) { return col.Guid == Guid.Empty; });
                    columnDisplay.DataBind();
                }
            }
        }
        #endregion

        #region actions

        private void AddWidget(JsonRequestDropZone action)
        {
            //add the control to the pagehi again
            PageFactory.GetDropZone(this).Add(action.NewWidgetLocation as IWidgetDropInfo);
            ZoneUpdated();
        }
        private void MoveWidget(JsonRequestDropZone action)
        {
            //remove control from old id, add to new, callback with instructions to update both?
            PageFactory.GetDropZone(this).Move(action.OldWidgetLocation as IWidgetDropInfo, action.NewWidgetLocation as IWidgetDropInfo);
            PageBuilder p = (Page as PageBuilder);
            p.View(p.Pagedata); //update all zones
        }
        private void RemoveColumn(JsonRequestDropZone action)
        {
            //move controls in >= column to column--, decrease column count by 1
            if (ZoneData.Columns.Count == 1)
            {
                if (ColumnDefinitions.Count > 0)
                {
                    long curColumn = ZoneData.Columns[0].columnID;
                    ZoneData.Columns[0] = ColumnDefinitions[0];
                    ZoneData.Columns[0].columnID = curColumn;
                }
                else
                {
                    Units unit; int width; string cssclass; string cssframework;
                    getFirstEnabledSize(out unit, out width, out cssclass, out cssframework);
                    ZoneData.Columns[0].width = width;
                    ZoneData.Columns[0].unit = unit;
                    ZoneData.Columns[0].CssClass = cssclass;
                    ZoneData.Columns[0].CssFramework = cssframework;
                }
                UpdateViewState();
            }
            else
            {
                PageFactory.GetDropZone(this).RemoveColumn(action.NewWidgetLocation as IWidgetDropInfo);
            }
            ZoneUpdated();
        }
        private void ResizeColumn(JsonRequestDropZone action)
        {
            //set width of column
            PageFactory.GetDropZone(this).ResizeColumn(action.NewWidgetLocation as IWidgetDropInfo);
            ZoneUpdated();
        }
        private void SetMasterZone(JsonRequestDropZone action)
        {
            //save new type of dropzone
            PageFactory.GetDropZone(this).SetDropZoneType(action.DropzoneInfo.isMaster);
            ZoneUpdated();
        }
        private void MoveColumn(JsonRequestDropZone action)
        {
            if (this.IsUXEnabled)
            {
                //moved column data
                long columnID = action.OldWidgetLocation.ColumnID;
                long sourceIndex = action.OldWidgetLocation.widgetTypeID;
                long targetIndex = action.NewWidgetLocation.widgetTypeID;

                var column = ZoneData.Columns.Where(c => c.columnID == columnID).First();

                ZoneData.Columns.RemoveAt(Int32.Parse(sourceIndex.ToString()));
                ZoneData.Columns.Insert(Int32.Parse(targetIndex.ToString()), column);
            }
            else
            {
                long prevColumnID = action.OldWidgetLocation.ColumnID;
                long thisColumnID = action.NewWidgetLocation.ColumnID;

                var thisColumn = ZoneData.Columns.Where(c => c.columnID == thisColumnID).First();
                var oldIndex = ZoneData.Columns.IndexOf(thisColumn);

                if (prevColumnID > -1)
                {
                    var afterColumn = ZoneData.Columns.Where(c => c.columnID == prevColumnID).First();
                    var afterIndex = ZoneData.Columns.IndexOf(afterColumn);
                    var newIndex = afterIndex + 1;
                    ZoneData.Columns.RemoveAt(oldIndex);
                    if (newIndex > oldIndex) newIndex--;
                    ZoneData.Columns.Insert(newIndex, thisColumn);
                }
                else
                {
                    ZoneData.Columns.RemoveAt(oldIndex);
                    ZoneData.Columns.Insert(0, thisColumn);
                }
            }
            UpdateViewState();
            ZoneUpdated();
        }

        protected void DropZonePanelLoad(object sender, EventArgs e)
        {
            if (!this.IsUXEnabled)
            {
                AddColumn.ImageUrl = appPath + "/PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/addcolumn_off.png";
                AddColumn.AlternateText = m_refMsg.GetMessage("lbl pagebuilder add column");
                AddColumn.ToolTip = m_refMsg.GetMessage("lbl pagebuilder add column");

                if (IsPostBack)
                {
                    string argument = Request.Form["__EVENTARGUMENT"];

                    if (argument != null && argument != "" && argument.StartsWith("{\"Action\""))
                    {
                        try
                        {
                            JsonRequestDropZone jsonRequest = JsonConvert.DeserializeObject<JsonRequestDropZone>(argument);

                            if (jsonRequest != null && ((jsonRequest.NewWidgetLocation != null && jsonRequest.NewWidgetLocation.dropZoneID == this.ID)
                                || (jsonRequest.DropzoneInfo != null && jsonRequest.DropzoneInfo.dropZoneID == this.ID)))
                            {
                                switch (jsonRequest.Action)
                                {
                                    case "add":
                                        {
                                            AddWidget(jsonRequest);
                                            break;
                                        }
                                    case "move":
                                        {
                                            MoveWidget(jsonRequest);
                                            break;
                                        }
                                    case "RemoveColumn":
                                        {
                                            RemoveColumn(jsonRequest);
                                            break;
                                        }
                                    case "ResizeColumn":
                                        {
                                            ResizeColumn(jsonRequest);
                                            break;
                                        }
                                    case "SaveDZType":
                                        {
                                            SetMasterZone(jsonRequest);
                                            break;
                                        }
                                    case "ReorderColumns":
                                        {
                                            MoveColumn(jsonRequest);
                                            break;
                                        }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                        }
                    }
                }
            }
        }

        private void getFirstEnabledSize(out Units unit, out int width, out string cssclass, out string cssframework)
        {
            unit = Units.pixels;
            width = 0;
            cssclass = "";
            cssframework = "";
            foreach (var size in PageBuilderConfigReader.GetConfig().Sizes)
            {
                if (size.Enabled)
                {
                    unit = size.UnitType;
                    if (size.UnitType == Units.custom && size.Groups.Count > 0)
                    {
                        cssframework = size.Name;
                        foreach (var group in size.Groups)
                        {
                            foreach (var field in group.Fields)
                            {
                                if (field.Type == "DropDown")
                                {
                                    DropDown f = field as DropDown;
                                    if (f != null && f.Classes != null && f.Classes.Count > 0)
                                    {
                                        cssclass += f.Classes[0].Class + " ";
                                    }
                                }
                            }
                        }
                        cssclass += size.ColumnClass;
                        cssclass = cssclass.Trim();
                    }
                    break;
                }
            }
            return;
        }

        protected void AddColumn_click(object sender, EventArgs e)
        {
            Ektron.Cms.PageBuilder.ColumnData col = new Ektron.Cms.PageBuilder.ColumnData();
            List<Ektron.Cms.PageBuilder.ColumnData> tmp = ColumnData.ConvertFromColumnDataSerializeList(ZoneData.Columns);
            tmp.Sort(delegate(Ektron.Cms.PageBuilder.ColumnData l, Ektron.Cms.PageBuilder.ColumnData r) { return l.columnID.CompareTo(r.columnID); });

            long newID = tmp.Max(c => c.columnID) + 1;
            col.columnID = newID;
            col.Guid = Guid.Empty;
            //default to first available config
            Units unit; int width; string cssclass; string cssframework;
            getFirstEnabledSize(out unit, out width, out cssclass, out cssframework);
            col.unit = unit; col.width = width; col.CssClass = cssclass; col.CssFramework = cssframework;

            ZoneData.Columns.Add(col);
            UpdateViewState();
            ZoneUpdated();
        }
        #endregion

        #region rendering

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.IsUXEnabled && this.IsUXUser && this.isZoneEditable)
            {
                //this.UXDropZoneData.ID = this.UniqueID;
                this.UXDropZoneData.ID = this.ID;
                this.UXDropZoneData.MarkupID = uxDropZone.UniqueID;
                this.UXDropZoneData.IsDropZoneEditable = this.isZoneEditable;
                this.UXDropZoneData.IsMasterZone = this.ZoneData.isMasterZone;
                uxDropZone.Attributes.Clear();
                uxDropZone.Attributes.Add("data-ux-pagebuilder-dropzone-data", JsonConvert.SerializeObject(this.UXDropZoneData));
            }
            else
            {
                dzcontainer.Attributes.Clear();
                dzcontainer.Attributes["class"] = "dropzone PBClear";

                //update column links (add / remove)
                if (isZoneEditable)
                {
                    if (Request["thumbnail"] == null)
                    {
                        blockuiCall.Visible = true;
                        dzcontainer.Attributes.Add("EditMode", "true");
                    }
                    AddColumn.Visible = AllowAddColumn;
                    masterzoneselect.Visible = (Page as PageBuilder).Pagedata.IsMasterLayout;
                    if (AllowAddColumn || (Page as PageBuilder).Pagedata.IsMasterLayout)
                    {
                        imgsetmasterzone.Src = appPath + "/PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/lock_off.png";
                        imgsetmasterzone.Alt = "Lock - Set as Layout Zone";

                        dzcontainer.Attributes["class"] = "dropzone PBClear PBDZhasHeader";
                        dzheader.Visible = true;
                    }
                    if (ZoneData.isMasterZone)
                    {
                        dzcontainer.Attributes["class"] += " isMasterZone";
                    }
                }
                else
                {
                    dzheader.Visible = false;
                }

                for (int i = 0; i < columnDisplay.Items.Count; i++)
                {
                    LinkButton l = (columnDisplay.Items[i].FindControl("RemColumn") as LinkButton);
                    if (l != null) l.Visible = isZoneEditable;
                }

                if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                {
                    //Ektron.Cms.API.JS.RegisterJSBlock(this, "$ektron('#" + updatepanel.ClientID + "').attr('class', '');", "updateDZClass" + updatepanel.ClientID + "clearClass");
                    Ektron.Cms.Framework.UI.JavaScript.RegisterJavaScriptBlock(this, "$ektron('#" + updatepanel.ClientID + "').attr('class', '');");
                }
            }

            ApplyCssClasses(ZoneData);

            if (Request["thumbnail"] != null && Request["thumbnail"] == "true")
            {
                if (!(Page as PageBuilder).Pagedata.IsMasterLayout || !ZoneData.isMasterZone)
                {
                    dzcontainer.Attributes["class"] += " dzthumbnail";
                }
            }

            if (this.IsUXEnabled)
            {
                this.uxUXSwitch.SetActiveView(uxUXView);
            }
            else
            {
                this.uxUXSwitch.SetActiveView(uxOriginalView);
            }
        }

        private void ApplyCssClasses(DropZoneData ZoneData)
        {
            HtmlGenericControl dropzone = this.IsUXEnabled ? uxDropZone : dzcontainer;
            UpdatePanel up = this.IsUXEnabled ? uxUpdatePanel : updatepanel;
            string dropzoneouterclass = "";
            foreach (var column in ZoneData.Columns)
            {
                if (!string.IsNullOrEmpty(column.CssClass) && column.unit == Units.custom)
                {
                    SizeType currentsize = getSizeRuleForColumn(column);
                    if (currentsize != null)
                    {
                        string dropZoneClass = String.IsNullOrEmpty(dropzone.Attributes["class"]) ? String.Empty : dropzone.Attributes["class"];
                        if (!string.IsNullOrEmpty(currentsize.DropZoneClass) && !dropZoneClass.Contains(currentsize.DropZoneClass))
                        {
                            dropzone.Attributes["class"] += " " + currentsize.DropZoneClass;
                        }

                        if (!string.IsNullOrEmpty(currentsize.DropZoneOuterClass))
                        {
                            if (!dropzoneouterclass.Contains(currentsize.DropZoneOuterClass))
                            {
                                dropzoneouterclass += " " + currentsize.DropZoneOuterClass;
                            }
                        }
                    }
                }
            }
            dropzoneouterclass = dropzoneouterclass.Trim();
            if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
            {
                Ektron.Cms.Framework.UI.JavaScript.RegisterJavaScriptBlock(this, "$ektron('#" + up.ClientID + "').attr('class', '').addClass('" + dropzoneouterclass + "')");
            }
            else
            {
                if (up.Attributes["class"] == null)
                {
                    up.Attributes.Add("class", dropzoneouterclass);
                }
                else
                {
                    up.Attributes["class"] = dropzoneouterclass;
                }
            }

        }

        private SizeType getSizeRuleForColumn(IColumnData thiscol)
        {
            if (!string.IsNullOrEmpty(thiscol.CssFramework))
            {
                foreach (var size in PageBuilderConfigReader.GetConfig().Sizes)
                {
                    if (size.UnitType == thiscol.unit)
                    {
                        if (thiscol.unit != Units.custom || size.Name.ToLower() == thiscol.CssFramework.ToLower())
                        {
                            return size;
                        }
                    }
                }
            }

            //get size rule
            foreach (var size in PageBuilderConfigReader.GetConfig().Sizes)
            {
                if (size.UnitType == thiscol.unit)
                {
                    if (thiscol.unit == Units.custom)
                    {
                        foreach (var group in size.Groups)
                        {
                            foreach (var field in group.Fields)
                            {
                                if (field.Type == "DropDown")
                                {
                                    DropDown f = field as DropDown;
                                    foreach (var i in f.Classes)
                                    {
                                        if (thiscol.CssClass == i.Class) { return size; }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return size;
                    }
                }
            }
            return null;
        }

        protected void columnDisplay_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                IColumnData thiscol = e.Item.DataItem as IColumnData;
                List<WidgetData> mywidgets = getColumnWidgets(thiscol.columnID);
                mywidgets.Sort(delegate(WidgetData left, WidgetData right) { return left.Order.CompareTo(right.Order); });

                //get size rule
                SizeType currentsize = getSizeRuleForColumn(thiscol);
                if (currentsize != null)
                {
                    foreach (var cssfile in currentsize.CssFiles)
                    {
                        Ektron.Cms.Framework.UI.Css.Register(this, cssfile);
                    }
                }

                HtmlControl columnheader = null;

                if (!this.IsUXEnabled)
                {
                    //image paths
                    //(e.Item.FindControl("imgleftcorner") as HtmlImage).Src = appPath + "/PageBuilder/PageControls/images/column_leftcorner.png";
                    //(e.Item.FindControl("imgrightcorner") as HtmlImage).Src = appPath + "/PageBuilder/PageControls/images/column_rightcorner.png";
                    (e.Item.FindControl("imgresizecolumn") as HtmlImage).Src = appPath + "/PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/edit_off.png";
                    (e.Item.FindControl("imgresizecolumn") as HtmlImage).Alt = (e.Item.FindControl("lbResizeColumn") as HtmlAnchor).Title = m_refMsg.GetMessage("lbl pagebuilder resize");
                    (e.Item.FindControl("imgremcolumn") as HtmlImage).Src = appPath + "/PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/icon_close.png";
                    (e.Item.FindControl("imgremcolumn") as HtmlImage).Alt = (e.Item.FindControl("lbDeleteColumn") as HtmlAnchor).Title = m_refMsg.GetMessage("generic delete title") + " Column";

                    (e.Item.FindControl("lbResizeColumn") as HtmlAnchor).Visible = AllowColumnResize;
                    (e.Item.FindControl("lbDeleteColumn") as HtmlAnchor).Visible = AllowColumnResize;
                    (e.Item.FindControl("lbResizeColumn") as HtmlAnchor).Title = (e.Item.FindControl("imgresizecolumn") as HtmlImage).Alt.ToString();
                    (e.Item.FindControl("lbDeleteColumn") as HtmlAnchor).Title = (e.Item.FindControl("imgremcolumn") as HtmlImage).Alt.ToString();

                    columnheader = (e.Item.FindControl("headerItem") as HtmlControl);
                    columnheader.Visible = isZoneEditable;

                    if ((Page as PageBuilder).Status != Mode.AnonViewing)
                    {
                        HtmlControl column = (e.Item.FindControl("column") as HtmlControl);
                        column.Attributes.Add("columnid", thiscol.columnID.ToString());
                        column.Attributes.Add("columnguid", thiscol.Guid.ToString());
                    }

                    HtmlControl zonediv = e.Item.FindControl("zone") as HtmlControl;
                    if (!isZoneEditable)
                    {
                        zonediv.Attributes["class"] = "PBViewing";
                    }
                    else
                    {
                        zonediv.Attributes.Add("dropzoneid", this.ID);
                        zonediv.Attributes["class"] = "PBColumn";
                        if (AllowColumnResize && Ektron.Cms.PageBuilder.PageBuilderConfigReader.GetConfig().ResizeDraggingEnabled)
                        {
                            zonediv.Attributes.Add("resizable", "true");
                        }
                        else
                        {
                            zonediv.Attributes["class"] += " PBUnsizable";
                        }
                        if (thiscol.width == 0)
                        {
                            zonediv.Attributes["class"] += " PBColumnUnsized";
                        }
                    }

                    // moved for more logical location for class assignment
                    if (thiscol.width > 0)
                    {
                        string unit = (thiscol.unit == Units.pixels) ? "px" : (thiscol.unit == Units.em) ? "em" : "%";
                        zonediv.Style.Add("width", thiscol.width.ToString() + unit);
                    }

                    if (thiscol.unit == Units.custom)
                    {
                        zonediv.Attributes["class"] += " " + thiscol.CssClass;
                        if (currentsize != null && !string.IsNullOrEmpty(currentsize.ColumnClass))
                        {
                            zonediv.Attributes["class"] += " " + currentsize.ColumnClass;
                        }
                    }
                    if (isZoneEditable && currentsize != null)
                    {
                        columnheader.Attributes.Add("unitname", currentsize.Name);
                        columnheader.Attributes.Add("cssclass", thiscol.CssClass);
                        columnheader.Attributes.Add("width", thiscol.width.ToString());
                    }
                }
                Repeater controlcolumn;
                if (this.IsUXEnabled)
                {
                    controlcolumn = (e.Item.FindControl("uxControlColumn") as Repeater);
                }
                else
                {
                    controlcolumn = (e.Item.FindControl("controlcolumn") as Repeater);
                }
                controlcolumn.ItemDataBound += new RepeaterItemEventHandler(controlcolumn_ItemDataBound);

                controlcolumn.DataSource = mywidgets;
                controlcolumn.DataBind();
            }
        }

        protected void controlcolumn_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            if (item.ItemType != ListItemType.Header && item.ItemType != ListItemType.Footer && (item.DataItem as WidgetData) != null)
            {
                string widgetHostId = "WidgetHost";
                if (this.IsUXEnabled)
                {
                    widgetHostId = "uxWidgetHost";
                }
                WidgetData w = item.DataItem as WidgetData;
                WidgetHostCtrl ctrl = (WidgetHostCtrl)item.FindControl(widgetHostId);
                ctrl.ColumnID = w.ColumnID;
                ctrl.SortOrder = w.Order;
                ctrl.WidgetHost_Load();
            }
        }
        #endregion
    }
}