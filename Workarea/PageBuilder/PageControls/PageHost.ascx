<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageHost.ascx.cs" Inherits="PageHost" %>
<%@ Register Src="WidgetTray.ascx" TagName="WidgetTray" TagPrefix="UCEktron" %>

<%@ Register TagPrefix="ektronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" %>
<asp:Literal ID="isMasterLayout" Visible="false" runat="server" EnableViewState="false">
    <script type="text/javascript">
        Ektron.PBMasterSettings = { 'isMasterLayout': <isMasterLayout>, 'pathToLock': '<pathToLock>', 'hasMasterLayout': <hasMasterLayout> }
    </script>
</asp:Literal>
<asp:Literal ID="dontAutoCloseMenu" Visible="false" runat="server" EnableViewState="false">
    <script type="text/javascript">
        Ektron.PBSettings = { 'dontClose': false }
    </script>
</asp:Literal>
<asp:Literal ID="sessionKeepalive" runat="server" Visible="false" EnableViewState="false">
    <script type="text/javascript">
        function sessionKeepAlive() {
            var wRequest = new Sys.Net.WebRequest();
            wRequest.set_url("<pagepostback>");
            wRequest.set_httpVerb("POST");
            wRequest.add_completed(sessionKeepAlive_Callback);
            wRequest.set_body("Message=keepalive");
            wRequest.get_headers()["Content-Length"] = 0;
            wRequest.invoke();
        }

        function sessionKeepAlive_Callback(executor, eventArgs){}
        window.setInterval( "sessionKeepAlive();", <millis>);
    </script>
</asp:Literal>

<asp:MultiView ID="uxUXSwitch" runat="server">
    <asp:View ID="uxOriginalView" runat="server">
        <div id="setSizeTemplate" runat="server" enableviewstate="false" class="setSizeTemplate" style="display: none;">
            <div class="Field">
                <label for="ColumnType">Type:</label>
                <select id="ColumnType" class="ColumnTypeDropDown" onchange='Ektron.PageBuilder.WidgetHost.ChangeResize(); return false;' runat="server"></select>
            </div>
            <asp:Repeater ID="DefaultSizeOptions" runat="server">
                <ItemTemplate>
                    <div id='<%# DataBinder.Eval(Container.DataItem, "Name")%>-_-<%# (int)DataBinder.Eval(Container.DataItem, "UnitType") %>' style="display: none;" class="framework">
                        <fieldset>
                            <legend>Size</legend>
                            <div class="Fields">
                                <div class="Field">
                                    <label for="<%# Container.FindControl("newwidth").ClientID%>"><%# DataBinder.Eval(Container.DataItem, "Label")%></label>
                                    <input id="newwidth" class="newwidth" type="text" runat="server" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <asp:Repeater ID="FrameworkTypes" runat="server">
                <ItemTemplate>
                    <div id='<%# DataBinder.Eval(Container.DataItem, "Name")%>-_-<%# (int)DataBinder.Eval(Container.DataItem, "UnitType") %>' style="display: none;" class="framework">
                        <asp:Repeater ID="FrameworkGroups" runat="server">
                            <ItemTemplate>
                                <fieldset>
                                    <legend><%# DataBinder.Eval(Container.DataItem, "Label")%></legend>
                                    <div class="Fields">
                                        <asp:Repeater ID="GroupFields" runat="server">
                                            <ItemTemplate>
                                                <div class="Field">
                                                    <label for="<%# Container.FindControl("FieldOptions").ClientID%>"><%# DataBinder.Eval(Container.DataItem, "Label")%></label>
                                                    <select id="FieldOptions" runat="server" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </fieldset>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="ColumnActions">
                <a href='#' onclick='Ektron.PageBuilder.WidgetHost.ManualResize(this, true); return false;'><%= SaveText%></a>
                <a href='#' onclick='Ektron.PageBuilder.WidgetHost.ManualResize(this, false); return false;'><%= CancelText%></a>
            </div>
        </div>
        <div id="EktronPersonalizationWrapper" runat="server" class="EktronPersonalizationWrapper">
            <div class="topmenu">
                <div class="topmenuitem">
                    <asp:Literal ID="lblFile" runat="server" />
                    <ul class="dropdown">
                        <li id="PBNew" class="dropdown">
                            <asp:LinkButton ID="lbNew" runat="server" Visible="true" OnClientClick="return false;" />
                        </li>
                        <li id="PBCopy" class="dropdown">
                            <asp:LinkButton ID="lbCopy" runat="server" Visible="true" OnClientClick="return false;" />
                        </li>
                        <li id="PBEdit" class="dropdown">
                            <asp:LinkButton CssClass="edit" ID="lbEdit" runat="server" OnClick="lbEdit_Click" Visible="true" />
                        </li>
                        <li id="PBSave" class="dropdown">
                            <asp:LinkButton ID="lbSave" runat="server" OnClick="lbSave_Click" Visible="true" />
                        </li>
                        <li id="PBCheckin" class="dropdown2">
                            <asp:LinkButton ID="lbCheckin" OnClick="lbCheckin_Click" runat="server" Visible="true" />
                        </li>
                        <li id="PBPublish" class="dropdown2">
                            <asp:LinkButton ID="lbPublish" runat="server" OnClick="lbPublish_Click" Visible="true" />
                        </li>
                        <li id="PBCancel" class="dropdown">
                            <asp:LinkButton ID="lbCancel" runat="server" OnClick="lbCancel_Click" Visible="true" />
                        </li>
                    </ul>
                </div>

                <div class="topmenuitem">
                    <%=m_refMsg.GetMessage("generic view")%>
                    <ul class="dropdown">
                        <li id="PBViewPublishedCheckedIn" class="dropdown">
                            <asp:LinkButton ID="lbViewPublishedCheckedIn" runat="server" Visible="true" OnClick="lbViewPublishedCheckedIn_Click" />
                        </li>
                        <li id="PBProperties" class="dropdown">
                            <asp:HyperLink NavigateUrl="#" ID="lbProperties" runat="server" Visible="true" />
                        </li>
                        <li id="PBPreview" class="dropdown">
                            <asp:LinkButton ID="lbPreview" runat="server" Visible="true" OnClick="lbPreview_Click" />
                        </li>
                        <li id="PBWorkarea" class="dropdown">
                            <asp:LinkButton ID="lbWorkarea" runat="server" Visible="true" />
                        </li>
                        <li id="PBAnalytics" class="dropdown">
                            <asp:LinkButton ID="lbAnalytics" runat="server" Visible="true" />
                        </li>
                        <li id="PBMobilePreview" class="dropdown">
                            <asp:LinkButton ID="lbMPreview" runat="server" Visible="false" />
                            <asp:LinkButton ID="lbDevicePreview" runat="server" Visible="true" />
                        </li>
                        <li id="PBIPhone" class="dropdown2">
                            <asp:LinkButton ID="lbIPhone" runat="server" Visible="false" />
                        </li>
                        <li id="PBIPad" class="dropdown2">
                            <asp:LinkButton ID="lbIPad" runat="server" Visible="false" />
                        </li>
                        <li id="PBDroid" class="dropdown2">
                            <asp:LinkButton ID="lbDroid" runat="server" Visible="false" />
                        </li>
                        <li id="PBDevDB" class="dropdown2"></li>
                    </ul>
                </div>

                <div class="topmenuitem" id="propsmenu" runat="server">
                    <%=m_refMsg.GetMessage("generic properties")%>
                    <ul class="dropdownProps">
                        <li id="Li1" class="dropdown">
                            <div>
                                <table id="PBPropsTable">
                                    <tr>
                                        <td><span id="spnMode" runat="server" class="label">Current Mode</span></td>
                                        <td>
                                            <asp:Label ID="lblMode" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnTitle" runat="server" class="label">Title</span></td>
                                        <td>
                                            <asp:Label ID="lblTitle" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnPageID" runat="server" class="label">Page ID</span></td>
                                        <td>
                                            <asp:Label ID="lblPageid" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnLanguage" runat="server" class="label">Language</span></td>
                                        <td>
                                            <asp:Label ID="lblLanguage" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnLastEditor" runat="server" class="label">Last User to Edit</span></td>
                                        <td>
                                            <asp:Label ID="lblLasteditor" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnLastEditDate" runat="server" class="label">Last Edit date</span></td>
                                        <td>
                                            <asp:Label ID="lblLasteditdate" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnDateCreated" runat="server" class="label">Date Created</span></td>
                                        <td>
                                            <asp:Label ID="lblDatecreated" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnWireframeFile" runat="server" class="label">Wireframe</span></td>
                                        <td>
                                            <asp:Label ID="lblWireframe" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnContentPath" runat="server" class="label">Path</span></td>
                                        <td>
                                            <asp:Label ID="lblPath" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td><span id="spnStatus" runat="server" class="label">Status</span></td>
                                        <td>
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr id="trCheckedOut" runat="server" visible="false">
                                        <td><span id="spnCurrentEditor" runat="server" class="label">Currently Checked out to</span></td>
                                        <td>
                                            <asp:Label ID="lblUserCheckedOut" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="topmenuitem">
                    <%=m_refMsg.GetMessage("generic help")%>
                    <ul class="dropdown">
                        <li id="PBLayoutHelp" class="dropdown">
                            <asp:HyperLink ID="lbCreatingLayouts" runat="server" Visible="true" />
                        </li>
                        <li id="PBLaunchHelp" class="dropdown">
                            <asp:HyperLink ID="lbLaunchHelp" runat="server" Visible="true" />
                        </li>
                    </ul>
                </div>

                <asp:Repeater ID="repWidgetMenus" runat="server">
                    <ItemTemplate>
                        <div class="topmenuitem">
                            <%# (Container.DataItem as Ektron.Cms.PageBuilder.PageBuilderMenu).Title %>
                            <ul class="dropdown">
                                <asp:Repeater ID="repWidgetMenuItems" runat="server">
                                    <ItemTemplate>
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lbBtn" runat="server" Visible="true" />
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <div class="MenuTack">
                    <a href="#" onclick="Ektron.PageBuilder.WidgetTray.menuTack(); return false;">
                        <asp:Image ID="MenuTack" CssClass="menuTackImg" runat="server" /></a>
                </div>
                <div class="MenuToggle">
                    <a href="#" onclick="Ektron.PageBuilder.WidgetTray.menuToggle(); return false;">
                        <asp:Image ID="MenuImg" class="menuToggleImg" runat="server" /></a>
                </div>

                <div class="topmenuitem_right">
                    <asp:LinkButton ID="lbLogout" runat="server" Visible="true">Logout</asp:LinkButton>
                </div>
                <asp:Panel ID="pnlSearchWidget" runat="server">
                    <div class="searchbox_alignright">
                        <div style="float: left;">
                            <input class="topmenuinputbox" id="topMenuInput" type="text" runat="server" />
                        </div>
                        <div style="float: right;">
                            <input class="searchbutton" type="button" />
                        </div>
                    </div>
                    <div class="topmenutext_right">
                        <asp:Label ID="lblFilterControlList" AssociatedControlID="topMenuInput" runat="server" />
                    </div>
                </asp:Panel>
                <br class="clear" />
            </div>

            <div id="widgetlist" class="controldashboard">
                <UCEktron:WidgetTray ID="tray" runat="server" Visible="false" />
            </div>

            <div class="controldashboardbottom">
                <!--<img id="imgdashbottom" runat="server" alt="" src="#" style="width:100%; height:5px;" />-->
            </div>
            <div class="pullchain">
                <a href="#" onclick="Ektron.PageBuilder.WidgetTray.toggleTray(); return false;">
                    <img id="imgpullchain" alt="pullchain" runat="server" src="#" /></a>
            </div>
        </div>

    </asp:View>
    <asp:View ID="uxUXView" runat="server">
        <div class="ektron-ux-UITheme ux-app-siteApppageBuilder-setSizeTemplate">
            <div id="uxsetSizeTemplate" runat="server" enableviewstate="false" class="ektron-ux-reset ux-app-siteApppageBuilder-setSizeTemplate ui-dialog ui-widget ui-widget-content ui-corner-all ui-front" style="display: none;">
                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                    <span id="ui-id-4" class="ui-dialog-title"><%= this.GetLocalResourceObject("ResizeHeaderText").ToString() %></span>
                </div>
                <div class="ui-dialog-content ui-widget-content">
                    <div class="inner-content-wrapper">
                        <div class="content-header">
                            <label for="<%= uxColumnType.ClientID %>"><%= this.GetLocalResourceObject("Type").ToString().ToLower() %></label>
                            <select id="uxColumnType" runat="server" class="ColumnTypeDropDown" onchange='Ektron.UX.apps.siteApp.onSelectChange();return false;'></select>
                        </div>
                        <asp:Repeater ID="uxDefaultSizeOptions" runat="server">
                            <ItemTemplate>
                                <div id='<%# DataBinder.Eval(Container.DataItem, "Name").ToString().ToLower() %>-_-<%# (int)DataBinder.Eval(Container.DataItem, "UnitType") %>' style="display: none;" class="unit value-section">
                                    <label for="<%# Container.FindControl("uxNewWidth").ClientID%>"><%= this.GetLocalResourceObject("Width").ToString() %></label>
                                    <input id="uxNewWidth" class="newwidth" type="text" runat="server" style="text-align:right;" /><abbr><%# GetUnitAbbreviation(Container) %></abbr>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Repeater ID="uxFrameworkTypes" runat="server">
                            <ItemTemplate>
                                <div id='<%# DataBinder.Eval(Container.DataItem, "Name").ToString().ToLower() %>-_-<%# (int)DataBinder.Eval(Container.DataItem, "UnitType") %>' style="display: none;" class="framework value-section">
                                    <asp:Repeater ID="FrameworkGroups" runat="server">
                                        <ItemTemplate>
                                            <h3><%# DataBinder.Eval(Container.DataItem, "Label")%></h3>
                                            <div class="Fields">
                                                <asp:Repeater ID="GroupFields" runat="server">
                                                    <ItemTemplate>
                                                        <div class="Field" style="margin-bottom:.25em;">
                                                            <label style="width:4em;text-align:right;display:inline-block;" for="<%# Container.FindControl("FieldOptions").ClientID%>"><%# DataBinder.Eval(Container.DataItem, "Label")%></label>
                                                            <select id="FieldOptions" runat="server" />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
                        <a class="primary" href='#<%= this.GetLocalResourceObject("Apply").ToString() %>' onclick='Ektron.UX.apps.siteApp.resize();return false;'><%= this.GetLocalResourceObject("Apply").ToString() %></a>
                        <a href='#<%= CancelText%>' onclick='Ektron.UX.apps.siteApp.hideResize();return false;'><%= CancelText%></a>
                    </div>
                </div>
            </div>
        </div>
        <ektronUI:JavaScriptBlock runat="server">
            <ScriptTemplate>
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate .ui-dialog-buttonpane a").button();
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate .framework").accordion({
                    heightStyle: "content",
                    activate: function(event, ui){
                        $ektron(ui).closest(".ui-accordion").accordion("refresh");
                    }
                });
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate").draggable({ handle: "div.ui-dialog-titlebar ", containment: $ektron("body") });
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <ektronUI:Css runat="server" Path="{UXPath}/applications/site/css/app.pagebuilder.css" />
        <asp:PlaceHolder ID="uxUXData" runat="server" Visible="<%# this.IsUxUserCmsAndLoggedIn %>">
            <input type="hidden" data-ektron-pagebuilder-page='column size data' value='<%= this.PageBuilderColumnSizeData %>' />
            <input type="hidden" data-ektron-pagebuilder-page='menu' value='<%= this.PageBuilderPageMenu %>' />
            <input type="hidden" data-ektron-pagebuilder-page='properties' value='<%= this.PageBuilderPageProperties %>' />
            <input type="hidden" data-ektron-pagebuilder-page="pagedata" value='<%= this.PageBuilderPageData %>' />
            <UCEktron:WidgetTray ID="uxWidgetData" runat="server" />
        </asp:PlaceHolder>
    </asp:View>
</asp:MultiView>