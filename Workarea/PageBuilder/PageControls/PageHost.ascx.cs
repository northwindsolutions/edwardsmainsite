using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Localization;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Workarea.Dms;
using Ektron.Cms.PageBuilder.UX;
using Ektron.Newtonsoft.Json;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.User;

public class UXPageBuilderPageData
{
    public string Mode { get; set; }
    public bool IsMasterLayout { get; set; }
    public long PageID { get; set; }
}

public partial class PageHost : System.Web.UI.UserControl, IPostBackEventHandler
{
    #region members and properties

    string appPath; //path to workarea
    ContentAPI _Capi;
    ContentAPI Capi
    {
        get
        {
            if (_Capi == null) _Capi = new ContentAPI();
            return _Capi;
        }
    }
    List<dmsMenuItem> _DmsOptions;
    protected Ektron.Cms.Common.EkMessageHelper m_refMsg
    {
        get
        {
            return Capi.EkMsgRef;
        }
    }

    public Double CacheInterval
    {
        get { return (Page as PageBuilder).CacheInterval; }
        set { (Page as PageBuilder).CacheInterval = value; }
    }

    private long _folder = -1;
    public long FolderID
    {
        get
        {
            if (PageID > 0)
            {
                // always verify folder ID unless user doesn't give us a page ID
                // because user can give us a bogus folder ID
                long pagefolderid = Capi.GetFolderIdForContentId(PageID);
                if (_folder != pagefolderid)
                {
                    _folder = pagefolderid;
                }
            }
            return _folder;
        }
        set { _folder = value; }
    }

    private long _defaulttaxid = -1;
    public long SelTaxonomyID
    {
        get { return _defaulttaxid; }
        set { _defaulttaxid = value; }
    }

    public long DefaultPageID
    {
        get { return (Page as PageBuilder).DefaultPageID; }
        set { (Page as PageBuilder).DefaultPageID = value; }
    }

    public long PageID
    {
        get { return (Page as PageBuilder).Pagedata.pageID; ; }
    }

    public int LangID
    {
        get { return Capi.ContentLanguage; }
    }

    private string _pagePath = "";
    public string PagePath
    {
        get
        {
            if (_pagePath == "")
            {
                _pagePath = Capi.GetPathByFolderID(FolderID);
            }
            return _pagePath;
        }
    }

    public string ThemeName
    {
        get { return (Page as PageBuilder).Theme; }
        set { (Page as PageBuilder).Theme = value; }
    }


    protected string SaveText
    {
        get { return m_refMsg.GetMessage("generic save"); }
    }
    protected string CancelText
    {
        get { return m_refMsg.GetMessage("generic cancel"); }
    }

    #endregion

    #region ux support
    public string GetUnitAbbreviation(RepeaterItem e)
    {
        string abbrev = String.Empty;
        SizeType sizeData = e.DataItem as SizeType;
        if (sizeData != null)
        {
            switch (sizeData.UnitType)
            {
                case Units.pixels:
                    abbrev = "px";
                    break;
                case Units.em:
                    abbrev = "em";
                    break;
                case Units.percent:
                    abbrev = "%";
                    break;
                default:
                    abbrev = String.Empty;
                    break;
            }
        }
        return abbrev;
    }
    private JavaScriptSerializer jsonSerializer;
    private JavaScriptSerializer JsonSerializer
    {
        get
        {
            if (this.jsonSerializer == null)
            {
                this.jsonSerializer = new JavaScriptSerializer();
            }
            return this.jsonSerializer;
        }
    }
    protected bool IsUxUserCmsAndLoggedIn
    {
        get
        {
            IUser user = ObjectFactory.GetUser();
            return user.IsLoggedIn && user.IsCmsUser;
        }
    }
    protected bool IsUXEnabled
    {
        get
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            return cmsContextService.IsDeviceHTML5 && cmsContextService.IsToolbarEnabledForTemplate;
        }
    }
    protected UXPageBuilderPageData UXPageData { get; set; }
    protected string PageBuilderPageData
    {
        get
        {
            string mode = String.Empty;
            switch ((Page as PageBuilder).Status)
            {
                case Mode.AuthorViewing:
                    if ((Page as PageBuilder).viewType == layoutVersion.Staged)
                        mode = "Staged";
                    else
                        mode = "Published";
                    break;
                case Mode.Editing:
                    mode = "Edit";
                    break;
                case Mode.Preview:
                    mode = "Preview";
                    break;
            }

            this.UXPageData.Mode = mode;
            this.UXPageData.IsMasterLayout = (Page as PageBuilder).Pagedata.IsMasterLayout;
            this.UXPageData.PageID = this.PageID;
            return this.jsonSerializer.Serialize(this.UXPageData);
        }
    }
    protected string PageBuilderPageProperties
    {
        get
        {
            Dictionary<string, string> pageBuilderProperties = new Dictionary<string, string>();
            //current mode
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("CurrentMode"), lblMode.Text);
            //title
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("Title"), lblTitle.Text);
            //page id
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("PageID"), lblPageid.Text);
            //language
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("Language"), lblLanguage.Text.Trim());
            //last user to edit
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("LastUserToEdit"), lblLasteditor.Text);
            //last edit date
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("LastEditDate"), lblLasteditdate.Text);
            //date created
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("DateCreated"), lblDatecreated.Text);
            //wireframe
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("Wireframe"), lblWireframe.Text);
            //path
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("Path"), lblPath.Text);
            //status
            pageBuilderProperties.Add((string)this.GetLocalResourceObject("Status"), lblStatus.Text);
            //currently checked out to
            if (this.PageID > -1)
            {
                ContentStateData state = Capi.GetContentState(PageID);
                if (state != null)
                {
                    if (state.Status == "O")
                    {
                        pageBuilderProperties.Add((string)this.GetLocalResourceObject("CurrentlyCheckedOutTo"), lblUserCheckedOut.Text);
                    }
                }
            }

            return this.JsonSerializer.Serialize(pageBuilderProperties);
        }
    }
    protected string PageBuilderPageMenu
    {
        get
        {
            UXMenuItem menu = new UXMenuItem();
            menu.Name = "PageBuilder";
            menu.Enabled = true;

            menu.Items.Add(this.GetUXMenuFile());
            menu.Items.Add(this.GetUXMenuView());
            menu.Items.Add(this.GetUXMenuHelp());


            return this.JsonSerializer.Serialize(menu);
        }
    }

    protected string PageBuilderColumnSizeData { get; set; }

    private UXMenuItem GetUXMenuFile()
    {
        //create file menu
        UXMenuItem fileMenu = UXMenuCreateItem((string)this.GetLocalResourceObject("File"), null, null, true, new List<UXMenuItem>());
        //new
        fileMenu.Items.Add(UXMenuCreateItem(lbNew.Text, null, SafeOnClick(lbNew.OnClientClick), lbNew.Enabled, null));
        //copy
        fileMenu.Items.Add(UXMenuCreateItem(lbCopy.Text, null, SafeOnClick(lbCopy.OnClientClick), lbCopy.Enabled, null));
        //edit
        string editEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbEdit_Click");
        fileMenu.Items.Add(UXMenuCreateItem(lbEdit.Text, null, editEvent, lbEdit.Enabled, null));
        //save
        string saveEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbSave_Click");
        UXMenuItem saveMenu = UXMenuCreateItem(lbSave.Text, null, saveEvent, lbSave.Enabled, new List<UXMenuItem>());
        //checkin
        string checkinEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbCheckin_Click");
        saveMenu.Items.Add(UXMenuCreateItem(lbCheckin.Text, null, checkinEvent, lbCheckin.Enabled, null));
        //publish
        string publishEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbPublish_Click");
        saveMenu.Items.Add(UXMenuCreateItem(lbPublish.Text, null, publishEvent, lbPublish.Enabled, null));
        fileMenu.Items.Add(saveMenu);
        //cancel
        string cancelEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbCancel_Click");
        fileMenu.Items.Add(UXMenuCreateItem(lbCancel.Text, null, cancelEvent, lbCancel.Enabled, null));

        return fileMenu;
    }
    private string SafeOnClick(string onclick)
    {
        return onclick.TrimEnd(new char[] { ';' }) + ";";
    }

    private UXMenuItem GetUXMenuView()
    {
        //create view menu
        UXMenuItem viewMenu = UXMenuCreateItem((string)this.GetLocalResourceObject("View"), String.Empty, String.Empty, true, new List<UXMenuItem>());
        //published checked in
        string publishedCheckedInEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbViewPublishedCheckedIn_Click");
        viewMenu.Items.Add(UXMenuCreateItem(lbViewPublishedCheckedIn.Text, null, publishedCheckedInEvent, lbViewPublishedCheckedIn.Enabled, null));
        //properties
        viewMenu.Items.Add(UXMenuCreateItem(lbProperties.Text, lbProperties.NavigateUrl, @"window.open(this.href);return false;", lbProperties.Enabled, null));
        //preview
        string previewEvent = (Page as PageBuilder).GetUXPostBackEventReference(this, "lbPreview_Click");
        viewMenu.Items.Add(UXMenuCreateItem(lbPreview.Text, null, previewEvent, lbPreview.Enabled, null));
        //workarea
        string workareaPath = String.Format("{0}/workarea.aspx?LangType={1}", this.appPath.ToString(), this.LangID);
        viewMenu.Items.Add(UXMenuCreateItem(lbWorkarea.Text, workareaPath, @"window.open(this.href);return false;", lbWorkarea.Enabled, null));
        //analytics
        string analyticsClick = String.Format("{0}/analytics/seo.aspx?tab=traffic&uri={1};", this.appPath.ToString(), Request.RawUrl);
        viewMenu.Items.Add(UXMenuCreateItem(lbAnalytics.Text, analyticsClick, @"window.open(this.href);return false;", lbAnalytics.Enabled, null));
        return viewMenu;
    }
    private UXMenuItem GetUXMenuHelp()
    {
        //create help menu
        UXMenuItem helpMenu = UXMenuCreateItem((string)this.GetLocalResourceObject("Help"), null, null, true, new List<UXMenuItem>());
        //layout management
        helpMenu.Items.Add(UXMenuCreateItem(lbCreatingLayouts.Text, lbCreatingLayouts.NavigateUrl, @"window.open(this.href);return false;", lbCreatingLayouts.Enabled, null));
        //launch help
        helpMenu.Items.Add(UXMenuCreateItem(lbLaunchHelp.Text, lbLaunchHelp.NavigateUrl, @"window.open(this.href);return false;", lbLaunchHelp.Enabled, null));

        return helpMenu;
    }
    UXMenuItem UXMenuCreateItem(String name, String href, String onclick, Boolean enabled, List<UXMenuItem> items)
    {
        UXMenuItem item = new UXMenuItem();
        if (!String.IsNullOrEmpty(name))
        {
            item.Name = name;
        }
        if (!String.IsNullOrEmpty(href))
        {
            item.Href = href;
        }
        if (!String.IsNullOrEmpty(onclick))
        {
            item.OnClick = onclick;
        }
        item.Enabled = enabled;
        if (items != null)
        {
            item.Items = items;
        }
        return item;
    }
    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "lbEdit_Click":
                lbEdit_Click(null, null);
                break;
            case "lbSave_Click":
                lbSave_Click(null, null);
                break;
            case "lbCheckin_Click":
                lbCheckin_Click(null, null);
                break;
            case "lbPublish_Click":
                lbPublish_Click(null, null);
                break;
            case "lbCancel_Click":
                lbCancel_Click(null, null);
                break;
            case "lbViewPublishedCheckedIn_Click":
                lbViewPublishedCheckedIn_Click(null, null);
                break;
            case "lbPreview_Click":
                lbPreview_Click(null, null);
                break;
        }
    }


    #endregion

    #region Init, Load

    protected void Page_Init(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager sMgr = new ScriptManager();
            this.Controls.AddAt(0, sMgr);
        }

        appPath = Capi.ApplicationPath.TrimEnd(new char[] { '/' });

        if (this.IsUXEnabled)
        {
            this.UXPageData = new UXPageBuilderPageData();
        }
        else
        {

            Packages.EktronCoreJS.Register(this);
            JavaScript.Register(this, appPath + "/java/webkitFix.js");
            Ektron.Cms.Framework.UI.Css.Register(this, appPath + "/PageBuilder/PageControls/" + ThemeName + "CSS/PageHost.css");
            if (Request["thumbnail"] == null || Request["thumbnail"] != "true")
            {
                Ektron.Cms.Framework.UI.Css.Register(this, appPath + "/PageBuilder/PageControls/" + ThemeName + "CSS/PageHostIE.css", BrowserTarget.AllIE);
                Ektron.Cms.Framework.UI.Css.Register(this, appPath + "/PageBuilder/PageControls/" + ThemeName + "CSS/PageHost.ie.7.css", BrowserTarget.LessThanEqualToIE7);
            }
        }
        /*repWidgetMenus.ItemDataBound += new RepeaterItemEventHandler(repWidgetMenus_ItemDataBound);
        repWidgetMenus.DataSource = (Page as PageBuilder).WidgetMenus;
        repWidgetMenus.DataBind();*/
        this.EnableViewState = false;

        (Page as PageBuilder).WidgetMenuRepeater = repWidgetMenus;

        lbLogout.OnClientClick = "window.open('" + appPath + "/login.aspx?action=logout', ";
        lbLogout.OnClientClick += "'Login', 'toolbar=0,location=0,scrollbars=0,width=250,height=190');return false;";

        sessionKeepalive.Visible = false;

        Ektron.Cms.API.AnalyticsTrackingCode.Register(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["thumbnail"] != null && Request["thumbnail"] == "true")
        {
            Ektron.Cms.Framework.UI.Css.Register(this, appPath + "/PageBuilder/PageControls/" + ThemeName + "CSS/Thumbnail.css");
            EktronPersonalizationWrapper.Visible = false;
        }
        else
        {
            dontAutoCloseMenu.Visible = false;
            PageBuilder p = (Page as PageBuilder);
            if (p.Status != Mode.AnonViewing)
            {
                setLabels();
                isActionAllowed(dmsMenuItemMenuItemType.DmsMenuView);
            }
            if (p.Status != Mode.AnonViewing && _DmsOptions.Count == 0)
            {
                //user do not have permissiosn
                //if they are an author and there is no layout then they still should see the menu though
                if (Capi.RequestInformationRef.IsMembershipUser == 1 || PageID > -1)
                {
                    p.Status = Mode.AnonViewing;
                }
            }
            if (p.Status != Mode.AnonViewing)
            {
                if (!IsPostBack && Request.RawUrl.Contains("ektronPageBuilderEdit=true") && Capi.IsLoggedIn) //we do it this way because aliasing apparently eats querystrings
                {
                    //if I have edit permissions then edit.
                    if (isActionAllowed(dmsMenuItemMenuItemType.DmsMenuEditLayout))
                    {
                        //note:this applies to to lbEdit
                        lbEdit_Click(null, null);
                        if (!this.IsUXEnabled)
                        {
                            dontAutoCloseMenu.Visible = true;
                        }
                    }
                    else
                    {
                        SwitchMode(Mode.AuthorViewing, layoutVersion.Staged);
                        if (!this.IsUXEnabled)
                        {
                            dontAutoCloseMenu.Visible = true;
                        }
                    }
                    //now redirect to the page without the querystring
                    string newUrl = Request.RawUrl.Replace('?', '&').Replace("&ektronPageBuilderEdit=true", "");
                    int first = newUrl.IndexOf('&');
                    if (first > -1) newUrl = newUrl.Remove(first, 1).Insert(first, "?");
                    //now redirect to it
                    Response.Redirect(newUrl);
                }
                else if (!IsPostBack && Request.RawUrl.Contains("view=staged") && Capi.IsLoggedIn && p.Status != Mode.AnonViewing)
                {
                    SwitchMode(Mode.AuthorViewing, layoutVersion.Staged);
                    string newUrl = Request.RawUrl.Replace('?', '&').Replace("&view=staged", "");
                    int first = newUrl.IndexOf('&');
                    if (first > -1) newUrl = newUrl.Remove(first, 1).Insert(first, "?");
                    //now redirect to it
                    Response.Redirect(newUrl);
                }
                else if (!IsPostBack && Request.RawUrl.Contains("view=published") && Capi.IsLoggedIn && p.Status != Mode.AnonViewing)
                {
                    SwitchMode(Mode.AuthorViewing, layoutVersion.Published);
                    string newUrl = Request.RawUrl.Replace('?', '&').Replace("&view=published", "");
                    int first = newUrl.IndexOf('&');
                    if (first > -1) newUrl = newUrl.Remove(first, 1).Insert(first, "?");
                    //now redirect to it
                    Response.Redirect(newUrl);
                }
            }
            setButtons();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        uxsetSizeTemplate.Visible = setSizeTemplate.Visible = false;
        displayProperties();
        if ((Page as PageBuilder).Status == Mode.AuthorViewing)
        {
            makeCopyandNew();
        }

        if ((Page as PageBuilder).Status == Mode.Editing)
        {
            //keep alive
            sessionKeepalive.Text = sessionKeepalive.Text.Replace("<pagepostback>", appPath + "/PageBuilder/SessionKeepAlive.aspx");
            sessionKeepalive.Text = sessionKeepalive.Text.Replace("<millis>", ((Session.Timeout * .75) * 60 * 1000).ToString());

            //resizemenu
            buildResizeMenu();
        }
        if ((Page as PageBuilder).Status != Mode.AnonViewing)
        {
            isMasterLayout.Visible = true;
            dontAutoCloseMenu.Visible = true;
            isMasterLayout.Text = isMasterLayout.Text.Replace("<isMasterLayout>", (Page as PageBuilder).Pagedata.IsMasterLayout.ToString().ToLower());
            isMasterLayout.Text = isMasterLayout.Text.Replace("<pathToLock>", appPath + "/PageBuilder/PageControls/Themes/TrueBlue/images/lock.png");
            isMasterLayout.Text = isMasterLayout.Text.Replace("<hasMasterLayout>", ((Page as PageBuilder).Pagedata.masterID > 0).ToString().ToLower());
        }

        //enable ux view if necessary
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        if (this.IsUXEnabled)
        {
            this.uxUXSwitch.SetActiveView(uxUXView);
            Packages.jQuery.jQueryUI.Accordion.Register(this);
            uxUXData.DataBind();
        }
        else
        {
            this.uxUXSwitch.SetActiveView(uxOriginalView);
        }

    }

    #endregion

    protected void SwitchMode(Mode newmode, layoutVersion newversion)
    {
        _DmsOptions = null;
        Mode oldmode = (Page as PageBuilder).Status;
        layoutVersion oldversion = (Page as PageBuilder).viewType;
        (Page as PageBuilder).Status = newmode;
        (Page as PageBuilder).viewType = newversion;

        switch (newmode)
        {
            case Mode.AnonViewing:
                Session["EkWidgetDirty"] = null;
                Session["EkWidgetBag"] = null;
                (Page as PageBuilder).Pagedata = null;
                (Page as PageBuilder).ClearView(newversion, true);
                break;
            case Mode.AuthorViewing:
                if (newversion == layoutVersion.Published)
                {
                    lbViewPublishedCheckedIn.Text = m_refMsg.GetMessage("lbl pagebuilder view staged");
                    lbViewPublishedCheckedIn.ToolTip = lbViewPublishedCheckedIn.Text;
                }
                else
                {
                    lbViewPublishedCheckedIn.Text = m_refMsg.GetMessage("lbl pagebuilder view published");
                    lbViewPublishedCheckedIn.ToolTip = lbViewPublishedCheckedIn.Text;
                }

                //clear session
                Session["EkWidgetDirty"] = null;
                Session["EkWidgetBag"] = null;
                (Page as PageBuilder).Pagedata = null;
                (Page as PageBuilder).ClearView(newversion, true);
                break;
            case Mode.Editing:
                if (oldmode != Mode.Preview || Session["EkWidgetDirty"] == null || Session["EkWidgetBag"] == null)
                {//if not the load from session
                    (Page as PageBuilder).ClearView(newversion, true);
                }
                else
                {
                    (Page as PageBuilder).View((Page as PageBuilder).Pagedata);
                }
                break;
            case Mode.Preview:
                //load from session
                (Page as PageBuilder).View((Page as PageBuilder).Pagedata);
                break;
        }

        setButtons();
    }

    protected void makeCopyandNew()
    {
        if (FolderID > -1)
        {
            if ((Page as PageBuilder).Pagedata.IsMasterLayout)
            {
                if (Capi.IsAdmin() || Capi.IsARoleMember(EkEnumeration.CmsRoleIds.CreateMasterLayout) && Capi.EkContentRef.IsAllowed(FolderID, LangID, "folder", "add", Capi.RequestInformationRef.UserId))
                {
                    Packages.EktronCoreJS.Register(this);
                    Packages.Ektron.Xml.Register(this);
                    Ektron.Cms.API.JS.RegisterJS(this, JS.ManagedScript.EktronModalJS);
                    Packages.jQuery.jQueryUI.Core.Register(this);
                    Packages.jQuery.jQueryUI.Draggable.Register(this);
                    Packages.jQuery.jQueryUI.Dialog.Register(this);
                    JS.RegisterJS(this, appPath + "/PageBuilder/Wizards/js/ektron.pagebuilder.wizards.js", "EktronPageBuilderWizardsJS");
                    JS.RegisterJS(this, appPath + "/PageBuilder/Wizards/js/wizardResources.aspx", "EktronPageBuilderWizardResourcesJS");

                    Packages.jQuery.jQueryUI.ThemeRoller.Register(this);

                    Ektron.Cms.API.Css.RegisterCss(this, appPath + "/PageBuilder/Wizards/css/ektron.pagebuilder.wizards.css", "EktronPageBuilderWizardsCSS");

                    //new
                    lbNew.OnClientClick = "return Ektron.PageBuilder.Wizards.showAddMasterPage({mode: 'add', folderId: " + FolderID.ToString() + ", language: " + LangID.ToString() + ", defaulttaxid: " + SelTaxonomyID.ToString() + "})";
                    enableButton(lbNew, true);
                    //copy
                    enableButton(lbCopy, false);
                }
                else
                {
                    //new
                    enableButton(lbNew, false);
                    //copy
                    enableButton(lbCopy, false);
                }
            }
            else
            {
                if (Capi.EkContentRef.IsAllowed(FolderID, LangID, "folder", "add", Capi.RequestInformationRef.UserId))
                {
                    Packages.EktronCoreJS.Register(this);
                    Packages.Ektron.Xml.Register(this);
                    JS.RegisterJS(this, JS.ManagedScript.EktronModalJS);
                    Packages.jQuery.jQueryUI.Core.Register(this);
                    Packages.jQuery.jQueryUI.Draggable.Register(this);
                    Packages.jQuery.jQueryUI.Dialog.Register(this);
                    JS.RegisterJS(this, appPath + "/PageBuilder/Wizards/js/ektron.pagebuilder.wizards.js", "EktronPageBuilderWizardsJS");
                    JS.RegisterJS(this, appPath + "/PageBuilder/Wizards/js/wizardResources.aspx", "EktronPageBuilderWizardResourcesJS");

                    Ektron.Cms.API.Css.RegisterCss(this, appPath + "/PageBuilder/Wizards/css/ektron.pagebuilder.wizards.css", "EktronPageBuilderWizardsCSS");

                    //new
                    lbNew.OnClientClick = "return Ektron.PageBuilder.Wizards.showAddPage({mode: 'add', folderId: " + FolderID.ToString() + ", language: " + LangID.ToString() + ", defaulttaxid: " + SelTaxonomyID.ToString() + "})";
                    enableButton(lbNew, true);

                    if (PageID > -1)
                    {
                        lbCopy.OnClientClick = "return Ektron.PageBuilder.Wizards.showAddPage({mode: 'saveAs', folderId: " + FolderID.ToString() + ", language: " + LangID.ToString() + ", pageid: " + PageID.ToString() + ", defaulttaxid: " + SelTaxonomyID.ToString() + " })";
                        enableButton(lbCopy, true);
                    }
                }
            }
        }
    }

    protected string getActionString(dmsMenuItemMenuItemType action)
    {
        try
        {
            if (_DmsOptions == null)
            {
                _DmsOptions = new List<dmsMenuItem>();

                if (PageID > -1)
                {
                    long UserID = Capi.UserId;
                    DmsMenu mnu = new DmsMenu(ektronDmsMenuMenuType.Workarea, PageID, UserID, LangID, FolderID, EkConstants.CMSContentType_Content, false);
                    dmsMenuItem[] items = mnu.GetDmsMenuData().dmsMenu.dmsContentItem.dmsMenuItem;
                    if (items != null)
                    {
                        _DmsOptions.AddRange(items);
                    }
                }
            }

            dmsMenuItem myitem = _DmsOptions.Find(delegate(dmsMenuItem i) { return i.menuItemType == action; });
            if (myitem == null) return "";
            return myitem.label;
        }
        catch (Exception ex)
        {
            string error = ex.ToString();
            return "";
        }
    }

    protected bool isActionAllowed(dmsMenuItemMenuItemType action)
    {
        try
        {
            if (_DmsOptions == null)
            {
                _DmsOptions = new List<dmsMenuItem>();

                if (PageID > -1)
                {
                    long UserID = new ContentAPI().UserId;
                    DmsMenu mnu = new DmsMenu(ektronDmsMenuMenuType.Workarea, PageID, UserID, LangID, FolderID, EkConstants.CMSContentType_Content, false);
                    dmsMenuItem[] items = mnu.GetDmsMenuData().dmsMenu.dmsContentItem.dmsMenuItem;
                    if (items != null)
                    {
                        _DmsOptions.AddRange(items);
                    }
                }
            }

            dmsMenuItem myitem = _DmsOptions.Find(delegate(dmsMenuItem i) { return i.menuItemType == action; });
            if (myitem == null) return false;
            return true;
        }
        catch (Exception ex)
        {
            string error = ex.ToString();
            return false;
        }
    }

    protected void enableButton(LinkButton lb, bool enabled) { enableButton(lb as WebControl, enabled); }
    protected void enableButton(HyperLink lb, bool enabled) { enableButton(lb as WebControl, enabled); }
    protected void enableButton(WebControl lb, bool enabled)
    {
        lb.CssClass = (enabled) ? "valid" : "invalid";
        lb.Enabled = enabled;
        string actionName = lb.ID.Replace("lb", "");
        HtmlImage img = (HtmlImage)this.FindControl("img" + actionName);
    }

    protected void setLabels()
    {
        // assign translated text values
        lblFile.Text = m_refMsg.GetMessage("lbl generic file");

        //new
        lbNew.Text = m_refMsg.GetMessage("lbl pagebuilder new page");
        lbNew.ToolTip = lbNew.Text;
        //copy
        lbCopy.Text = m_refMsg.GetMessage("lbl pagebuilder copy save as");
        lbCopy.ToolTip = lbCopy.Text;
        //edit
        lbEdit.Text = m_refMsg.GetMessage("generic edit title");
        lbEdit.ToolTip = lbEdit.Text;
        //save
        lbSave.Text = m_refMsg.GetMessage("generic save");
        lbSave.ToolTip = lbSave.Text;
        lbSave.PostBackUrl = Request.RawUrl; //Sometimes it just throws 404 error
        //checkin
        lbCheckin.Text = m_refMsg.GetMessage("btn checkin");
        lbCheckin.ToolTip = lbCheckin.Text;
        lbCheckin.PostBackUrl = Request.RawUrl; //Sometimes it just throws 404 error
        //publish
        lbPublish.Text = m_refMsg.GetMessage("generic publish");
        lbPublish.ToolTip = lbPublish.Text;
        lbPublish.PostBackUrl = Request.RawUrl; //Sometimes it just throws 404 error
        //cancel
        lbCancel.Text = m_refMsg.GetMessage("generic cancel");
        lbCancel.ToolTip = lbCancel.Text;
        //properties
        lbProperties.Text = m_refMsg.GetMessage("generic properties");
        lbProperties.ToolTip = lbProperties.Text;
        //preview
        lbPreview.Text = m_refMsg.GetMessage("lbl pagebuilder preview layout");
        lbPreview.ToolTip = lbPreview.Text;
        //logout
        lbLogout.Text = m_refMsg.GetMessage("generic logout msg");
        lbLogout.ToolTip = lbLogout.Text;
        //filter control list
        lblFilterControlList.Text = m_refMsg.GetMessage("lbl pagebuilder filter control list");
        lblFilterControlList.ToolTip = lblFilterControlList.Text;
        //view published checked in
        lbViewPublishedCheckedIn.Text = m_refMsg.GetMessage("lbl pagebuilder view published");
        lbViewPublishedCheckedIn.ToolTip = lbViewPublishedCheckedIn.Text;

        topMenuInput.Value = "";

        //set up img paths
        MenuImg.ImageUrl = appPath + "/PageBuilder/PageControls/" + ThemeName + "images/menuhandled_putback_hover.png";
        MenuImg.ToolTip = m_refMsg.GetMessage("generic openclose");
        MenuImg.AlternateText = m_refMsg.GetMessage("generic openclose");
        MenuTack.ImageUrl = appPath + "/PageBuilder/PageControls/" + ThemeName + "images/thumbtack_out.png";
        MenuTack.ToolTip = m_refMsg.GetMessage("generic thumbtack");
        MenuTack.AlternateText = m_refMsg.GetMessage("generic thumbtack");
        imgdashbottom.Src = appPath + "/PageBuilder/PageControls/" + ThemeName + "images/dashboardbottom.png";
        imgpullchain.Src = appPath + "/PageBuilder/PageControls/" + ThemeName + "images/pullchain.png";

        //workarea
        lbWorkarea.Text = m_refMsg.GetMessage("workarea page title");
        lbWorkarea.ToolTip = lbWorkarea.Text;
        lbWorkarea.Attributes.Add("onclick", String.Format("window.open('{0}/workarea.aspx?LangType={1}', 'Admin400', 'width=900,height=580,scrollable=1,resizable=1');return false;", this.appPath.ToString(), LangID));
        enableButton(lbWorkarea, true);

        //analytics
        lbAnalytics.Text = m_refMsg.GetMessage("lbl entry analytics");
        lbAnalytics.ToolTip = lbAnalytics.Text;
        lbAnalytics.Attributes.Add("onclick", String.Format("window.open('{0}/analytics/seo.aspx?tab=traffic&uri={1}', 'Analytics400', 'width=900,height=580,scrollable=1,resizable=1');return false;", appPath.ToString(), Request.RawUrl));
        enableButton(lbAnalytics, true);

        //mobile preview link
        string strPreviewLink = Request.Url.ToString() + ((Request.Url.ToString().IndexOf("?") == -1) ? "?" : "&") + "cmsMode=Preview";
        strPreviewLink = System.Web.HttpUtility.UrlEncode(strPreviewLink);

        //mobile preview
        lbMPreview.Text = m_refMsg.GetMessage("pb menu view mobile preview");
        lbMPreview.ToolTip = lbMPreview.Text;
        //iphone
        lbIPhone.Text = m_refMsg.GetMessage("pb menu view mobile preview iphone");
        lbIPhone.ToolTip = lbIPhone.Text;
        lbIPhone.Attributes.Add("onclick", String.Format("window.open('{0}/MobileDevicePreview/iPhone.aspx?LangType={1}&cmsMode=Preview&postbackurl={2}', 'Preview', 'width=900,height=580,scrollable=1,resizable=1');return false;", this.appPath.ToString(), LangID, strPreviewLink));
        enableButton(lbIPhone, true);
        //ipad
        lbIPad.Text = m_refMsg.GetMessage("pb menu view mobile preview ipad");
        lbIPad.ToolTip = lbIPad.Text;
        lbIPad.Attributes.Add("onclick", String.Format("window.open('{0}/MobileDevicePreview/iPad.aspx?LangType={1}&cmsMode=Preview&postbackurl={2}', 'Preview2', 'width=1200,height=650,scrollbars=yes,resizable=yes');return false;", this.appPath.ToString(), LangID, strPreviewLink));
        enableButton(lbIPad, true);
        //droid
        lbDroid.Text = m_refMsg.GetMessage("pb menu view mobile preview droid");
        lbDroid.ToolTip = lbDroid.Text;
        lbDroid.Attributes.Add("onclick", String.Format("window.open('{0}/MobileDevicePreview/DroidX.aspx?LangType={1}&cmsMode=Preview&postbackurl={2}', 'Preview3', 'width=700,height=620,scrollbars=yes,resizable=yes');return false;", this.appPath.ToString(), LangID, strPreviewLink));
        enableButton(lbDroid, true);
        //device preview
        lbDevicePreview.Text = (string)this.GetLocalResourceObject("DevicePreview");
        lbDevicePreview.ToolTip = (string)this.GetLocalResourceObject("DevicePreviewBreakPoint");
        lbDevicePreview.Attributes.Add("onclick", String.Format("window.open('{0}/MobileDevicePreview/DevicePreview.aspx?LangType={1}&cmsMode=Preview&postbackurl={2}', 'Preview3', 'width=700,height=620,scrollbars=yes,resizable=yes');return false;", this.appPath.ToString(), LangID, strPreviewLink));
        //help - overview       
        lbLaunchHelp.Text = m_refMsg.GetMessage("lbl launch pagebuilder help");
        lbLaunchHelp.ToolTip = lbLaunchHelp.Text;
        lbLaunchHelp.NavigateUrl = Capi.fetchhelpLink("pagebuilder_overview");
        lbLaunchHelp.Attributes.Add("onclick", "window.open(this.href);return false;");
        enableButton(lbLaunchHelp, true);

        //help - creating layouts     
        lbCreatingLayouts.Text = (string)this.GetLocalResourceObject("CreatingLayouts");
        lbCreatingLayouts.ToolTip = lbCreatingLayouts.Text;
        lbCreatingLayouts.NavigateUrl = Capi.fetchhelpLink("pagebuilder_layout");
        lbCreatingLayouts.Attributes.Add("onclick", "window.open(this.href);return false;");
        enableButton(lbCreatingLayouts, true);
    }

    protected void getHistory(PermissionData myPermissionData, out bool hasPublished, out bool hasStaged)
    {
        ContentHistoryData[] history = null;
        hasPublished = false;
        hasStaged = false;

        if (myPermissionData.CanHistory)
        {
            history = Capi.GetHistoryList(PageID);
        }
        if (history != null)
        {
            for (int i = 0; i < history.Length; i++)
            {
                if (history[i].Status == "I")
                {
                    hasStaged = true;
                }
                if (history[i].Status == "A")
                {
                    hasPublished = true;
                    break;
                }
            }
        }
    }

    protected void setButtons()
    {
        PermissionData myPermissionData = new PermissionData();
        bool hasStaged = false, hasPublished = false;

        //new
        enableButton(lbNew, false);
        lbNew.OnClientClick = "return false;";
        //copy
        enableButton(lbCopy, false);
        lbCopy.OnClientClick = "return false;";
        //cancel
        enableButton(lbCancel, false);
        //preview
        enableButton(lbPreview, false);
        lbPreview.Text = m_refMsg.GetMessage("lbl pagebuilder preview layout");
        lbPreview.ToolTip = lbPreview.Text;
        //save
        enableButton(lbSave, false);
        //workarea
        enableButton(lbWorkarea, false);
        //analytics
        enableButton(lbAnalytics, false);
        //iphone
        enableButton(lbIPhone, true);
        enableButton(lbIPhone, false); //note: prior to refactoring code set enabled to false then true for unknown reasons.  I'm leaving it as is.
        //ipad
        enableButton(lbIPad, true);
        enableButton(lbIPad, false); //note: prior to refactoring code set enabled to false then true for unknown reasons.  I'm leaving it as is.
        //droid
        enableButton(lbDroid, true);
        enableButton(lbDroid, false); //note: prior to refactoring code set enabled to false then true for unknown reasons.  I'm leaving it as is.
        //view publish checkin
        enableButton(lbViewPublishedCheckedIn, false);
        //preview
        enableButton(lbMPreview, false);

        dontAutoCloseMenu.Visible = true;
        tray.Visible = false;
        pnlSearchWidget.Visible = false;
        EktronPersonalizationWrapper.Visible = true;

        layoutVersion version = (Page as PageBuilder).viewType;

        if ((Page as PageBuilder).Status != Mode.AnonViewing)
        {
            if (!Capi.EkContentRef.IsAllowed(0, 0, "users", "IsLoggedIn", 0) || Capi.RequestInformationRef.UserId == EkConstants.BuiltIn)
            {
                EktronPersonalizationWrapper.Visible = false;
                return;
            }

            //fix for when another user forces checkin and starts editing. it kicks us out here.
            if ((isActionAllowed(dmsMenuItemMenuItemType.DmsMenuForceCheckIn) || isActionAllowed(dmsMenuItemMenuItemType.DmsMenuRequestCheckIn)) && (Page as PageBuilder).Status == Mode.Editing)
            {
                SwitchMode(Mode.AuthorViewing, (Page as PageBuilder).viewType);
                return;
            }

            myPermissionData = Capi.LoadPermissions(PageID, "content", ContentAPI.PermissionResultType.All);
            getHistory(myPermissionData, out hasPublished, out hasStaged);
            if (version == layoutVersion.Published && !hasPublished && hasStaged &&
                ((Page as PageBuilder).Status == Mode.AuthorViewing || (Page as PageBuilder).Status == Mode.Preview))
            {
                SwitchMode(Mode.AuthorViewing, layoutVersion.Staged);
                return;
            }

            if (!myPermissionData.CanPublish)
            {
                if (myPermissionData.CanApprove)
                {
                    lbPublish.Text = m_refMsg.GetMessage("generic approve title");
                    lbPublish.ToolTip = lbPublish.Text;
                }
                else
                {
                    lbPublish.Text = m_refMsg.GetMessage("generic submit");
                    lbPublish.ToolTip = lbPublish.Text;
                }
            }
            lbPublish.ToolTip = lbPublish.Text;

            RegisterFiles();

            string force = getActionString(dmsMenuItemMenuItemType.DmsMenuForceCheckIn);
            if (force != "")
            {
                lbCheckin.Text = force;
                lbCheckin.ToolTip = force;
            }
            string noforce = getActionString(dmsMenuItemMenuItemType.DmsMenuCheckIn);
            if (noforce != "")
            {
                lbCheckin.Text = noforce;
                lbCheckin.ToolTip = noforce;
            }

            enableButton(lbEdit, UserCanEditPage());

            if (isActionAllowed(dmsMenuItemMenuItemType.DmsMenuViewProperties))
            {
                enableButton(lbProperties, true);
                lbProperties.NavigateUrl = Capi.AppPath + "workarea.aspx?page=content.aspx&folder_id=" + FolderID.ToString();
                lbProperties.NavigateUrl += "&ContentNav=" + PagePath;
                lbProperties.NavigateUrl += "&TreeVisible=Content&action=view&id=" + PageID.ToString();
                lbProperties.NavigateUrl += "&LangType=" + LangID.ToString();
                lbProperties.NavigateUrl += "&menuItemType=viewproperties";
                string onclick = "window.open(this.href, 'Admin400', 'location=0,status=1,scrollbars=1, width:900, height:580');return false;";
                lbProperties.Attributes.Add("onclick", onclick);
            }
            else
            {
                enableButton(lbProperties, false);
            }
            //checkin
            enableButton(lbCheckin, isActionAllowed(dmsMenuItemMenuItemType.DmsMenuCheckIn) || isActionAllowed(dmsMenuItemMenuItemType.DmsMenuForceCheckIn));
            //publish
            enableButton(lbPublish, (isActionAllowed(dmsMenuItemMenuItemType.DmsMenuPublish) || isActionAllowed(dmsMenuItemMenuItemType.DmsMenuCheckIn) || isActionAllowed(dmsMenuItemMenuItemType.DmsMenuSubmit) || isActionAllowed(dmsMenuItemMenuItemType.DmsMenuApprove)) && version == layoutVersion.Staged);
        }

        switch ((Page as PageBuilder).Status)
        {
            case Mode.AnonViewing:
                EktronPersonalizationWrapper.Visible = false;
                break;
            case Mode.AuthorViewing:
                bool showSwitchView = false;
                if (PageID > -1)
                {
                    ContentHistoryData[] history = null;
                    //get permissions for history

                    showSwitchView = hasPublished && hasStaged;

                    if (!showSwitchView)
                    {
                        ContentStateData state = Capi.GetContentState(PageID);
                        DateTime golive = new DateTime();
                        if (state.CurrentGoLive == "" || !DateTime.TryParse(state.CurrentGoLive, out golive))
                        {
                            golive = DateTime.MinValue;
                        }
                        if (golive > DateTime.Now)
                            showSwitchView = true;
                    }
                }

                dontAutoCloseMenu.Visible = false;
                //workarea
                enableButton(lbWorkarea, true);
                //analytics
                enableButton(lbAnalytics, true);
                //iphone
                enableButton(lbIPhone, true);
                //ipad
                enableButton(lbIPad, true);
                //droid
                enableButton(lbDroid, true);
                //view published checked in
                enableButton(lbViewPublishedCheckedIn, showSwitchView);
                break;
            case Mode.Preview:
                //edit
                enableButton(lbEdit, false);
                //cancel
                enableButton(lbCancel, true);
                //workarea
                enableButton(lbWorkarea, true);
                //analytics
                enableButton(lbAnalytics, true);
                //iphone
                enableButton(lbIPhone, true);
                //ipad
                enableButton(lbIPad, true);
                //droid
                enableButton(lbDroid, true);
                //preview
                enableButton(lbPreview, true);
                lbPreview.Text = m_refMsg.GetMessage("lbl pagebuilder return to edit");
                lbPreview.ToolTip = lbPreview.Text;

                dontAutoCloseMenu.Visible = false;
                break;
            case Mode.Editing:
                if (!isActionAllowed(dmsMenuItemMenuItemType.DmsMenuForceCheckIn))
                {
                    (Page as PageBuilder).viewType = layoutVersion.Staged;
                    //edit
                    enableButton(lbEdit, false);
                    //cancel
                    enableButton(lbCancel, true);
                    //preview
                    enableButton(lbPreview, true);
                    //save
                    enableButton(lbSave, true);
                    //workarea
                    enableButton(lbWorkarea, true);
                    //analytics
                    enableButton(lbAnalytics, true);
                    //iphone
                    enableButton(lbIPhone, true);
                    enableButton(lbIPhone, true); //note: prior to refactoring code set enabled to false then true for unknown reasons.  I'm leaving it as is.
                    //ipad
                    enableButton(lbIPad, true);
                    enableButton(lbIPad, true); //note: prior to refactoring code set enabled to false then true for unknown reasons.  I'm leaving it as is.
                    //droid
                    enableButton(lbDroid, true);
                    enableButton(lbDroid, true); //note: prior to refactoring code set enabled to false then true for unknown reasons.  I'm leaving it as is.
                    //mobile preview
                    enableButton(lbMPreview, true);

                    tray.Visible = true;
                    pnlSearchWidget.Visible = true;
                    sessionKeepalive.Visible = true;
                    //register JS
                    Packages.Ektron.JSON.Register(this);
                    Packages.jQuery.jQueryUI.Core.Register(this);
                    Packages.jQuery.jQueryUI.Widget.Register(this);
                    Packages.jQuery.jQueryUI.Sortable.Register(this);
                    Packages.jQuery.jQueryUI.Effects.All.Register(this);
                    JS.RegisterJS(this, JS.ManagedScript.EktronBlockUiJS);
                    Packages.jQuery.jQueryUI.Resizable.Register(this);
                    Packages.jQuery.jQueryUI.Draggable.Register(this);
                    Packages.jQuery.jQueryUI.Dialog.Register(this);
                }
                break;
        }

        if (Capi.RequestInformationRef.PreviewMode == true)
            EktronPersonalizationWrapper.Visible = false;

    }

    protected void displayProperties()
    {
        if (PageID == -1 || (Page as PageBuilder).Status == Mode.AnonViewing)
        {
            propsmenu.Visible = false;
        }
        else
        {
            propsmenu.Visible = true;
            try
            {
                PageBuilder mypage = (Page as PageBuilder);
                WireframeModel wfm = new WireframeModel();
                ContentBase baseData = mypage.Basedata;
                if (baseData == null) baseData = Capi.EkContentRef.LoadContent(PageID, true, EkEnumeration.CMSContentSubtype.PageBuilderData);

                Ektron.Cms.Framework.Localization.LocaleManager _locApi = new Ektron.Cms.Framework.Localization.LocaleManager();

                LocaleData langData = null;
                langData = _locApi.GetItem(baseData.Language);
                ContentStateData state = null;
                WireframeData wireframe = null;
                if (PageID > -1)
                {
                    state = Capi.GetContentState(PageID);
                    wireframe = wfm.FindByPageID(PageID);
                }

                spnContentPath.InnerText = m_refMsg.GetMessage("generic path");
                spnCurrentEditor.InnerText = m_refMsg.GetMessage("status:checked out by");
                spnDateCreated.InnerText = m_refMsg.GetMessage("generic datecreated");
                spnLanguage.InnerText = m_refMsg.GetMessage("lbl language");
                spnLastEditDate.InnerText = m_refMsg.GetMessage("generic date modified");
                spnLastEditor.InnerText = m_refMsg.GetMessage("generic last editor");
                spnMode.InnerText = m_refMsg.GetMessage("lbl pagebuilder current mode");
                spnPageID.InnerText = m_refMsg.GetMessage("lbl pagebuilder page id");
                spnStatus.InnerText = m_refMsg.GetMessage("generic status");
                spnTitle.InnerText = m_refMsg.GetMessage("generic title");
                spnWireframeFile.InnerText = m_refMsg.GetMessage("lbl pagebuilder wireframe template path");

                trCheckedOut.Visible = false;

                //date created
                lblDatecreated.Text = baseData.DisplayDateCreated;
                //language
                lblLanguage.Text = langData.NativeName + " (" + baseData.Language.ToString() + ")";
                //last edit date
                lblLasteditdate.Text = baseData.DisplayDateModified;
                //last editor
                lblLasteditor.Text = baseData.LastEditorFname + " " + baseData.LastEditorLname;
                lblPath.Text = PagePath;

                if (wireframe != null)
                {
                    lblWireframe.Text = wireframe.Path;
                }
                //page id
                lblPageid.Text = PageID.ToString();
                //title
                lblTitle.Text = state.Title;

                if (state != null)
                {
                    switch (state.Status)
                    {
                        case "A":
                            lblStatus.Text = m_refMsg.GetMessage("status:approved (published)");
                            break;
                        case "S":
                            lblStatus.Text = m_refMsg.GetMessage("status:submitted for approval");
                            break;
                        case "I":
                            lblStatus.Text = m_refMsg.GetMessage("status:checked in");
                            break;
                        case "M":
                            lblStatus.Text = m_refMsg.GetMessage("status:submitted for deletion");
                            break;
                        case "P":
                            lblStatus.Text = m_refMsg.GetMessage("status:approved (pgld)");
                            break;
                        case "O":
                            trCheckedOut.Visible = true;
                            lblStatus.Text = m_refMsg.GetMessage("status:checked out");
                            if (state != null)
                            {
                                lblUserCheckedOut.Text = state.CurrentEditorFirstName + " " + state.CurrentEditorLastName;
                                lblUserCheckedOut.ToolTip = lblUserCheckedOut.Text;
                            }
                            break;
                        case "T":
                            lblStatus.Text = m_refMsg.GetMessage("lbl pagebuilder pending tasks");
                            break;
                        default:
                            lblStatus.Text = "";
                            break;
                    }
                    if (state.ContType == 3)
                    {
                        lblStatus.Text = m_refMsg.GetMessage("lbl pagebuilder expired");
                    }
                }

                switch (mypage.Status)
                {
                    case Mode.AuthorViewing:
                        if ((Page as PageBuilder).viewType == layoutVersion.Staged)
                            lblMode.Text = m_refMsg.GetMessage("pagebuilder viewing staged");
                        else
                            lblMode.Text = m_refMsg.GetMessage("pagebuilder viewing published");
                        break;
                    case Mode.Editing:
                        lblMode.Text = m_refMsg.GetMessage("pagebuilder editing");
                        break;
                    case Mode.Preview:
                        lblMode.Text = m_refMsg.GetMessage("pagebuilder preview");
                        break;
                    default:
                        lblMode.Text = "";
                        break;
                }
            }
            catch (Exception e)
            {
                string error = e.ToString();
            }
        }
    }

    protected void RegisterFiles()
    {
        if (!this.IsUXEnabled)
        {
            JS.RegisterJS(this, JS.ManagedScript.EktronJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronCookieJS);
            JS.RegisterJS(this, appPath + "/PageBuilder/PageControls/JS/widgetTrayResources.aspx", "EktronWidgetTrayResourcesJS");
            JS.RegisterJS(this, appPath + "/PageBuilder/PageControls/JS/WidgetTray.js", "EktronWidgetTrayJS");
        }
    }

    protected void lbPublish_Click(object sender, EventArgs e)
    {
        IPageController _controller = PageFactory.GetController(Page as PageBuilder);
        PageData savestate = null;
        if (Session["EkWidgetDirty"] != null && (bool)Session["EkWidgetDirty"] == true && Session["EkWidgetBag"] != null)
        {
            savestate = (PageData)Session["EkWidgetBag"];
        }
        else
        {
            _controller.Get(PageID, out savestate, true);
        }
        if (savestate != null)
        {
            if (savestate.pageID == PageID)
            {
                if (!_controller.Publish(savestate))
                {
                    (Page as PageBuilder).Error("Could not save state");
                }
            }
            else
            {
                (Page as PageBuilder).Error("State Mismatch");
            }
            // check the state of the content and set the mode correctly as appropriate
            ContentStateData state = Capi.GetContentState(PageID);
            DateTime golive = new DateTime();
            if (state.CurrentGoLive == "" || !DateTime.TryParse(state.CurrentGoLive, out golive))
            {
                golive = DateTime.MinValue;
            }
            if (state.Status == "A" && golive < DateTime.Now)
            {
                SwitchMode(Mode.AuthorViewing, layoutVersion.Published);
            }
            else
            {
                SwitchMode(Mode.AuthorViewing, layoutVersion.Staged);
            }
        }
        Response.Redirect(Request.RawUrl); //use post-redirect-get method to avoid viewstate issues
    }

    protected void lbSave_Click(object sender, EventArgs e)
    {
        PageData savestate = null;
        if (Session["EkWidgetDirty"] != null && (bool)Session["EkWidgetDirty"] == true && Session["EkWidgetBag"] != null)
        {
            savestate = (PageData)Session["EkWidgetBag"];
        }
        else
        {
            savestate = (Page as PageBuilder).Pagedata;
        }
        if (savestate != null)
        {
            IPageController _controller = PageFactory.GetController(Page as PageBuilder);
            if (savestate.pageID == PageID)
            {
                if (!_controller.Save(savestate))
                {
                    (Page as PageBuilder).Error("Could not save state");
                }
            }
            else
            {
                (Page as PageBuilder).Error("State Mismatch");
            }
        }
        Response.Redirect(Request.RawUrl); //use post-redirect-get method to avoid viewstate issues
    }

    protected void lbCheckin_Click(object sender, EventArgs e)
    {
        PageData savestate = null;
        if (Session["EkWidgetDirty"] != null && (bool)Session["EkWidgetDirty"] == true && Session["EkWidgetBag"] != null)
        {
            savestate = (PageData)Session["EkWidgetBag"];
        }
        else
        {
            savestate = (Page as PageBuilder).Pagedata;
        }
        if (savestate != null)
        {
            IPageController _controller = PageFactory.GetController(Page as PageBuilder);
            if (savestate.pageID == PageID)
            {
                if (!_controller.CheckIn(savestate))
                {
                    (Page as PageBuilder).Error("Could not save state");
                }
            }
            else
            {
                (Page as PageBuilder).Error("State Mismatch");
            }
        }
        SwitchMode(Mode.AuthorViewing, layoutVersion.Staged);
        Response.Redirect(Request.RawUrl); //use post-redirect-get method to avoid viewstate issues
    }

    protected bool UserCanEditPage()
    {
        return (((Page as PageBuilder).Pagedata.languageID == Capi.ContentLanguage && isActionAllowed(dmsMenuItemMenuItemType.DmsMenuEditLayout)) || Capi.RequestInformationRef.WorkAreaOperationMode == true);
    }

    protected void lbEdit_Click(object sender, EventArgs e)
    {
        Ektron.Cms.ContentStateData pageStateData = Capi.GetContentState((Page as PageBuilder).Pagedata.pageID);
        long contentStateUserId = pageStateData.CurrentUserId;
        if (UserCanEditPage())
        {
            IPageController _controller = PageFactory.GetController(Page as PageBuilder);
            _controller.CheckOut((Page as PageBuilder).Pagedata);
            SwitchMode(Mode.Editing, layoutVersion.Staged);

            string newUrl = Request.RawUrl.Replace('?', '&').Replace("&ektronPageBuilderEdit=true", "");
            int first = newUrl.IndexOf('&');
            if (first > -1) newUrl = newUrl.Remove(first, 1).Insert(first, "?");

            Response.Redirect(newUrl); //use post-redirect-get method to avoid viewstate issues
        }
        else
        {
            lblStatus.Text = m_refMsg.GetMessage("status:checked out");
        }
    }

    protected void lbCancel_Click(object sender, EventArgs e)
    {
        Session["EkWidgetDirty"] = false;
        Session["EkWidgetBag"] = null;
        IPageController _controller = PageFactory.GetController(Page as PageBuilder);
        _controller.Revert((Page as PageBuilder).Pagedata);
        SwitchMode(Mode.AuthorViewing, layoutVersion.Staged);
        Response.Redirect(Request.RawUrl); //use post-redirect-get method to avoid viewstate issues
    }

    protected void lbPreview_Click(object sender, EventArgs e)
    {
        //toggle editing
        if ((Page as PageBuilder).Status == Mode.Editing)
            SwitchMode(Mode.Preview, (Page as PageBuilder).viewType);
        else
            SwitchMode(Mode.Editing, (Page as PageBuilder).viewType);
        Response.Redirect(Request.RawUrl); //use post-redirect-get method to avoid viewstate issues
    }

    protected void lbViewPublishedCheckedIn_Click(object sender, EventArgs e)
    {
        //toggle staged / published
        if ((Page as PageBuilder).viewType == layoutVersion.Published)
            SwitchMode((Page as PageBuilder).Status, layoutVersion.Staged);
        else
            SwitchMode((Page as PageBuilder).Status, layoutVersion.Published);
        //Response.Redirect(Request.RawUrl); //use post-redirect-get method to avoid viewstate issues
    }

    protected void buildResizeMenu()
    {
        if (this.IsUXEnabled)
        {
            uxsetSizeTemplate.Visible = true;
        }
        else
        {
            setSizeTemplate.Visible = true;
        }
        List<SizeType> sizes = Ektron.Cms.PageBuilder.PageBuilderConfigReader.GetConfig().Sizes.FindAll(s => s.Enabled == true);
        List<SizeType> NonDefaultSizes = sizes.FindAll(s => s.UnitType == Units.custom);
        List<SizeType> DefaultSizes = sizes.FindAll(s => s.UnitType != Units.custom);

        foreach (var size in sizes)
        {
            if (this.IsUXEnabled)
            {
                uxColumnType.Items.Add(new ListItem(size.Name, size.Name.ToLower() + "-_-" + ((int)size.UnitType).ToString()));
            }
            else
            {
                ColumnType.Items.Add(new ListItem(size.Name, size.Name + "-_-" + ((int)size.UnitType).ToString()));
            }
        }
        if (this.IsUXEnabled)
        {
            //this.PageBuilderColumnSizeData = this.JsonSerializer.Serialize(sizes).ToString();
            uxDefaultSizeOptions.DataSource = DefaultSizes;
            uxDefaultSizeOptions.DataBind();
            uxFrameworkTypes.DataSource = NonDefaultSizes;
            uxFrameworkTypes.ItemDataBound += FrameworkTypes_ItemDataBound;
            uxFrameworkTypes.DataBind();
        }
        else
        {
            DefaultSizeOptions.DataSource = DefaultSizes;
            DefaultSizeOptions.DataBind();
            FrameworkTypes.DataSource = NonDefaultSizes;
            FrameworkTypes.ItemDataBound += FrameworkTypes_ItemDataBound;
            FrameworkTypes.DataBind();
        }

    }

    void FrameworkTypes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
        {
            Repeater FrameworkGroups = (Repeater)item.FindControl("FrameworkGroups");
            SizeType size = (SizeType)item.DataItem;
            FrameworkGroups.DataSource = size.Groups;
            FrameworkGroups.ItemDataBound += FrameworkGroups_ItemDataBound;
            FrameworkGroups.DataBind();
        }
    }

    void FrameworkGroups_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
        {
            Repeater GroupFields = (Repeater)item.FindControl("GroupFields");
            FieldGroup group = (FieldGroup)item.DataItem;
            GroupFields.DataSource = group.Fields;
            GroupFields.ItemDataBound += GroupFields_ItemDataBound;
            GroupFields.DataBind();
        }
    }

    void GroupFields_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
        {
            HtmlSelect Classes = (HtmlSelect)item.FindControl("FieldOptions");
            IField field = (IField)item.DataItem;
            if (field.Type == "DropDown")
            {
                DropDown typedfield = field as DropDown;
                foreach (var css in typedfield.Classes)
                {
                    Classes.Items.Add(new ListItem(css.Label, css.Class));
                }
            }
        }
    }
}
