<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetHost.ascx.cs" Inherits="WidgetHostCtrl" %>
<asp:MultiView ID="uxUXSwitch" runat="server">
    <asp:View ID="uxOriginalView" runat="server">
        <div class="widget" id="dropcontainer" runat="server">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" EnableViewState="false"></asp:Label>
            <asp:UpdatePanel ID="updatepanel" runat="server" OnLoad="widgetPanelLoad" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="header" id="toolbar" runat="server" enableviewstate="false">
                        <div class="buttons">
                            <span><asp:Literal ID="lblTitle" runat="server" Text="Widget"></asp:Literal>&#160;</span>
                    
                            <a href="#" class="help" id="lbHelpWidget" target="_blank" runat="server" visible="false" rel="nofollow">
                                <img id="imgPBhelpbutton" runat="server" alt="Help" class="PBhelpbutton PB-UI-icon" src="#" />
                            </a>
                            <a href="#" class="expand" id="lbExpandWidget" runat="server" onclick="Ektron.PageBuilder.WidgetHost.openAsModal(this); return false;" visible="false">
                                <img id="imgPBexpandbutton" runat="server" alt="Expand" class="PBexpandbutton PB-UI-icon" src="#" />
                            </a>
                            <asp:LinkButton CssClass="edit" ID="lbEditWidget" runat="server" OnClick="lbEdit_Click" Visible="false">
                                <img id="imgPBeditbutton" runat="server" alt="Edit" class="PBeditbutton PB-UI-icon" src="#" />
                            </asp:LinkButton>
                            <asp:LinkButton CssClass="delete" ID="lbDeleteWidget" runat="server" OnClick="lbDelete_Click" Visible="false">
                                <img id="imgPBclosebutton" runat="server" alt="Delete" class="PBclosebutton PB-UI-icon" src="#" />
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="content" id="dvContent" runat="server">
                        <asp:PlaceHolder ID="phWidgetContent" runat="server"></asp:PlaceHolder>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:View>
    <asp:View ID="uxUXView" runat="server">
        <asp:UpdatePanel ID="uxUpdatePanel" runat="server" OnLoad="widgetPanelLoad" UpdateMode="Conditional">
            <ContentTemplate>
                <div data-ux-pagebuilder="Widget"<%= this.UXWidgetDataJson %><%= this.UXIsWidgetEditMode %>>
                    <asp:PlaceHolder runat="server" id="widgetHeaderPlaceholder" Visible="false">
                        <div class="ektron-ux-reset widgetHeader">
                            <h3 class="ux-app-siteApp-widgetHeader" data-bind="click: select">
                                <span data-bind="text: Name || labels.widget, attr: {title: Name || labels.widget}" class="widgetName"></span>                                
                                <script id="ux-app-siteApp-widgetActions" type="text/html">
                                    <!-- ko if: 'Move' == Action -->
                                    <a data-bind="attr: {href: Href, title: Title, 'class': $parent.moveClass }, html: icon, click: fireAction"></a>
                                    <!-- /ko -->
                                    <!-- ko ifnot: 'Move' == Action -->
                                    <a data-bind="attr: {href: Href, title: Title, 'class': Action.toLowerCase() }, html: icon, click: fireAction"></a>
                                    <!-- /ko -->
                                </script>
                                <span id="uxAction" runat="server" class="actions">
                                    <span data-bind="template: {name: 'ux-app-siteApp-widgetActions', foreach: Actions}"></span>
                                </span>
                            </h3>
                        </div>
                    </asp:PlaceHolder>
                    <div class="widgetBody"<%= this.UXIsWidgetEditModeStyle %>>
                        <asp:PlaceHolder ID="uxWidgetContent" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:View>
</asp:MultiView>