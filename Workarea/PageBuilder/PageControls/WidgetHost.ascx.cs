using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Generic;
using Ektron.Cms;
//using Ektron.Cms.API;
using Ektron.Cms.Content;

using Ektron.Cms.PageBuilder;
using Ektron.Cms.Common;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Converters;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.User;

public enum UXWidgetActionType
{
    LaunchHelp,
    Edit,
    Remove,
    Move
}
public class UXWidgetData
{
    public UXWidgetData()
    {
        this.Actions = new List<UXWidgetAction>();
    }
    public string ID { get; set; }
    public string Name { get; set; }
    public string ClientID { get; set; }
    public bool HasError { get; set; }
    public long Index { get; set; }
    public bool IsPageEditable { get; set; }
    public string ErrorMessage { get; set; }
    public List<UXWidgetAction> Actions { get; set; }
}

public class UXWidgetAction
{
    [JsonConverter(typeof(StringEnumConverter))]
    public UXWidgetActionType Action { get; set; }
    public string ConfirmationMessage { get; set; }
    public string Href { get; set; }
    public string Callback { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
}

public partial class WidgetHostCtrl : Ektron.Cms.PageBuilder.WidgetHost
{
    private string appPath = "";
    private bool editing = false;

    protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
    protected EkRequestInformation requestInformation;

    #region ux support

    public UXWidgetData UXWidgetData { get; set; }
    public string UXIsWidgetEditMode
    {
        get
        {
            string attribute = String.Empty;
            if (this.UserInfo.IsLoggedIn && this.UserInfo.IsCmsUser && this.editing)
            {
                attribute = " data-ux-pagebuilder-widget-edit-mode='true'";
            }
            return attribute;
        }
    }
    public string UXIsWidgetEditModeStyle
    {
        get
        {
            string attribute = String.Empty;
            if (this.UserInfo.IsLoggedIn && this.UserInfo.IsCmsUser && this.editing)
            {
                attribute = " style='display:none;'";
            }
            return attribute;
        }
    }
    private IUser userInfo;
    public IUser UserInfo
    {
        get
        {
            if (this.userInfo == null)
            {
                userInfo = ObjectFactory.GetUser();
            }
            return this.userInfo;
        }
    }
    public string UXWidgetDataJson
    {
        get
        {
            string attribute = String.Empty;
            if (this.UserInfo.IsLoggedIn && this.UserInfo.IsCmsUser && this.IsEditable)
            {
                attribute = " data-ux-pagebuilder-widget-data='" + HttpUtility.HtmlAttributeEncode(JsonConvert.SerializeObject(this.UXWidgetData)) + "'";
            }
            return attribute;
        }
    }
    private ICmsContextService cmsContextService;
    protected ICmsContextService CmsContextService
    {
        get
        {
            if (this.cmsContextService == null)
            {
                this.cmsContextService = ServiceFactory.CreateCmsContextService();
            }
            return this.cmsContextService;
        }
    }
    protected bool IsUXEnabled
    {
        get
        {
            return this.CmsContextService.IsDeviceHTML5 && this.CmsContextService.IsToolbarEnabledForTemplate;
        }
    }
    protected override void Render(HtmlTextWriter writer)
    {
        this.Page.VerifyRenderingInServerForm(this);
        if (this.IsUXEnabled && !this.DesignMode && this.Visible && this.IsAjax && this.IsAjaxSafe() && this.IsEditable)
        {
            StringBuilder knockoutRebinding = new StringBuilder();
            knockoutRebinding.Append(@"Ektron.UX.apps.siteApp.rebindHandler({
                ""type"":""widget"",
                ""selector"":""#" + uxUpdatePanel.ClientID + @"""
            });");
            JavaScript.RegisterJavaScriptBlock(this, knockoutRebinding.ToString());
        }
        base.Render(writer);
    }
    private bool IsAjax
    {
        get
        {
            bool isAjax = false;
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm != null && sm.IsInAsyncPostBack)
            {
                isAjax = true;
            }
            return isAjax;
        }
    }
    private bool IsAjaxSafe()
    {
        bool isAjaxSafe = true;
        ScriptManager sm = ScriptManager.GetCurrent(this.Page);
        if (sm != null && sm.IsInAsyncPostBack)
        {
            isAjaxSafe = IsInUpdatingUpdatePanel();
        }
        return isAjaxSafe;
    }
    private bool IsInUpdatingUpdatePanel()
    {
        bool isInUpdatePanel = uxUpdatePanel.IsInPartialRendering;
        if (isInUpdatePanel)
        {
            return true;
        }
        Control parent = uxUpdatePanel.Parent;
        if (!(parent is HtmlForm))
        {
            do
            {
                if ((parent as UpdatePanel) != null)
                {
                    isInUpdatePanel = ((UpdatePanel)parent).IsInPartialRendering;
                }
                if (isInUpdatePanel)
                {
                    break;
                }
                if (parent != null)
                    parent = parent.Parent;
                else
                    break;
            } while (!(parent is System.Web.UI.HtmlControls.HtmlForm));
        }
        return isInUpdatePanel;
    }
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (this.IsUXEnabled)
        {
            this.Page.LoadComplete += Page_LoadComplete;
        }
    }
    void Page_LoadComplete(object sender, EventArgs e)
    {
        if (this.IsUXEnabled && this.IsPostBack)
        {
            string target = Request.Form["__EVENTTARGET"];
            if (target == uxAction.UniqueID)
            {
                string argument = Request.Form["__EVENTARGUMENT"];
                switch (argument)
                {
                    case "Edit":
                        lbEdit_Click(this, new EventArgs());
                        break;
                    case "Remove":
                        lbDelete_Click(this, new EventArgs());
                        break;
                }
            }
        }
    }
    protected void UXOpenWidgetInModal()
    {
        string script = @"Ektron.UX.apps.siteApp.PageBuilder.Widget.toggleEditMode({""title"":""" + this.Title + @"""});return false;";
        JavaScript.RegisterJavaScriptBlock(this, script);
    }

    #endregion

    #region Constructor

    public WidgetHostCtrl()
    {
        requestInformation = ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
        m_refMsg = new EkMessageHelper(requestInformation);
        appPath = requestInformation.ApplicationPath;
        this.UXWidgetData = new UXWidgetData();
    }

    #endregion

    #region Event Handlers

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!this.IsUXEnabled)
        {
            imgPBclosebutton.Src = appPath + "PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/icon_close.png";
            imgPBeditbutton.Src = appPath + "PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/edit_off.png";
            imgPBexpandbutton.Src = appPath + "PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/icon_expand.png";
            imgPBhelpbutton.Src = appPath + "PageBuilder/PageControls/" + (Page as PageBuilder).Theme + "images/icon_help.png";
            // provide translations
            lbEditWidget.ToolTip = m_refMsg.GetMessage("generic edit title");
            imgPBeditbutton.Alt = lbEditWidget.ToolTip;
            lbDeleteWidget.ToolTip = m_refMsg.GetMessage("generic delete title");
            imgPBclosebutton.Alt = lbDeleteWidget.ToolTip;
            lbExpandWidget.Title = imgPBexpandbutton.Alt;
            imgPBexpandbutton.Alt = m_refMsg.GetMessage("lbl expand");
            lbExpandWidget.Attributes.Add("title", imgPBexpandbutton.Alt);
            lbHelpWidget.Title = imgPBhelpbutton.Alt;
            imgPBhelpbutton.Alt = m_refMsg.GetMessage("generic help");
            lbHelpWidget.Attributes.Add("title", imgPBhelpbutton.Alt);
        }

        (this as Ektron.Cms.Widget.IWidgetHost).Load += new Ektron.Cms.Widget.LoadDelegate(WidgetHost_Load);

        if (!this.IsUXEnabled)
        {
            Ektron.Cms.Framework.Settings.LocaleManager lcmanager = new Ektron.Cms.Framework.Settings.LocaleManager();
            Ektron.Cms.Localization.LocaleData locale_data;
            locale_data = lcmanager.GetEnabledLocale(requestInformation.ContentLanguage);
            if (!string.IsNullOrEmpty(locale_data.Direction) && locale_data.Direction.Equals("rtl", StringComparison.CurrentCultureIgnoreCase))
            {
                dvContent.Attributes["style"] = "text-align: right !important";
            }
        }
    }

    protected void buttonSetup()
    {
        lbEditWidget.CssClass = "edit";
        lbDeleteWidget.Visible = false;
        lbEditWidget.Visible = false;
        lbExpandWidget.Visible = false;
        lbHelpWidget.Visible = false;

        if (phWidgetContent.Controls.Count > 0 && IsEditable)
        {
            lbEditWidget.Visible = EditSupported();
            lbDeleteWidget.Visible = true;
            lbHelpWidget.Visible = (HelpFile != "");
            if (lbHelpWidget.Visible)
            {
                string path = "";
                if (HelpFile.Contains(requestInformation.SitePath) || HelpFile.StartsWith("http://"))
                {
                    path = HelpFile;
                }
                else
                {
                    path = requestInformation.SitePath + ((HelpFile.Contains("~")) ? path += ResolveClientUrl(HelpFile) : path += HelpFile);
                }
                lbHelpWidget.HRef = path;
                lbHelpWidget.Attributes.Add("onclick", "window.open('" + path + "','widgetHelp','status=1,height=400,width=400,resizable=1,scrollbars=1'); return false;");
            }
            if (ExpandOptions == Ektron.Cms.Widget.Expandable.ExpandOnExpand)
            {
                lbExpandWidget.Visible = true;
            }
            if (ExpandOptions == Ektron.Cms.Widget.Expandable.ExpandOnEdit)
            {
                if (editing)
                {
                    lbEditWidget.CssClass += " OpeninModal";
                }
            }
        }
    }

    public void WidgetHost_Load()
    {
        try
        {
            if (base.InternalWidget == null) LoadWidget();
            if (base.InternalWidget != null)
            {
                if (this.IsUXEnabled)
                {
                    this.uxUXSwitch.SetActiveView(uxUXView);
                    uxWidgetContent.Controls.Clear();
                    base.InternalWidget.ID = this.ID + "_widget";
                    uxWidgetContent.Controls.Add(base.InternalWidget);
                }
                else
                {
                    this.uxUXSwitch.SetActiveView(uxOriginalView);
                    phWidgetContent.Controls.Clear();
                    base.InternalWidget.ID = this.ID + "_widget";
                    phWidgetContent.Controls.Add(base.InternalWidget);
                    if (IsEditable)
                    {
                        toolbar.Attributes.Add("widget-type-id", PBWidgetInfo.ID.ToString());
                    }
                }
            }
            else
            {
                if (this.IsUXEnabled)
                {
                    this.UXWidgetData.HasError = true;
                    this.UXWidgetData.ErrorMessage = m_refMsg.GetMessage("lbl widget not found");
                }
                else
                {
                    lblErrorMessage.Text = m_refMsg.GetMessage("lbl widget not found");
                }
            }
        }
        catch (Exception ex)
        {
            // Handle widgets that have been deleted or are missing. 
            if (ex.InnerException is HttpException)
            {
                phWidgetContent.Controls.Add(lblErrorMessage);
                lblErrorMessage.Text = m_refMsg.GetMessage("lbl widget not found");
            }
            else
            {
                throw;
            }
        }

        buttonSetup();
        lblTitle.Text = base.Title;
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.IsUXEnabled)
        {
            buttonSetup();
            toolbar.Visible = IsEditable;
        }
        base.OnPreRender(e);
        lblTitle.Text = base.Title;

        if (this.IsUXEnabled)
        {
            this.UXWidgetData.ClientID = this.ClientID;
            this.UXWidgetData.Name = base.Title;
            this.UXWidgetData.IsPageEditable = (Page as PageBuilder).Status == Mode.Editing ? true : false;
            widgetHeaderPlaceholder.Visible = (Page as PageBuilder).Status == Mode.Editing;
            this.UXWidgetData.ID = this.PBWidgetInfo.ID.ToString();
            this.UXWidgetData.Index = this.PBWidgetInfo.Order;

            if (uxWidgetContent.Controls.Count > 0 && this.IsEditable)
            {
                //help
                if (!String.IsNullOrEmpty(this.HelpFile))
                {
                    UXWidgetAction launchHelpAction = new UXWidgetAction();
                    launchHelpAction.Action = UXWidgetActionType.LaunchHelp;
                    launchHelpAction.Text = m_refMsg.GetMessage("generic help");
                    launchHelpAction.Title = m_refMsg.GetMessage("generic help");
                    string path = String.Empty;
                    if (HelpFile.Contains(requestInformation.SitePath) || HelpFile.StartsWith("http://"))
                    {
                        path = HelpFile;
                    }
                    else
                    {
                        path = requestInformation.SitePath + ((HelpFile.Contains("~")) ? path += ResolveClientUrl(HelpFile) : path += HelpFile);
                    }
                    launchHelpAction.Href = "#Help";
                    launchHelpAction.Callback = "window.open('" + path + "');";
                    this.UXWidgetData.Actions.Add(launchHelpAction);
                }

                //move
                UXWidgetAction moveAction = new UXWidgetAction();
                moveAction.Action = UXWidgetActionType.Move;
                moveAction.Href = "#Move";
                moveAction.Callback = "null";
                moveAction.Title = this.GetLocalResourceObject("MoveWidget").ToString();
                moveAction.Text = this.GetLocalResourceObject("MoveWidget").ToString();
                this.UXWidgetData.Actions.Add(moveAction);

                //edit
                if (uxWidgetContent.Controls.Count > 0 && this.IsEditable && EditSupported())
                {
                    UXWidgetAction editAction = new UXWidgetAction();
                    editAction.Action = UXWidgetActionType.Edit;
                    editAction.Href = "#Edit";
                    editAction.Callback = (Page as PageBuilder).GetUXPostBackEventReference(uxAction, "Edit").TrimEnd(new char[] { ';' }) + ";";
                    editAction.Title = m_refMsg.GetMessage("generic edit title");
                    editAction.Text = m_refMsg.GetMessage("generic edit title");
                    this.UXWidgetData.Actions.Add(editAction);
                }

                //remove
                if (uxWidgetContent.Controls.Count > 0 && this.IsEditable)
                {
                    UXWidgetAction removeAction = new UXWidgetAction();
                    removeAction.Action = UXWidgetActionType.Remove;
                    removeAction.Href = "#Remove";
                    removeAction.Callback = (Page as PageBuilder).GetUXPostBackEventReference(uxAction, "Remove").TrimEnd(new char[] { ';' }) + ";";
                    removeAction.Title = m_refMsg.GetMessage("generic delete title");
                    removeAction.Text = m_refMsg.GetMessage("generic delete title");
                    this.UXWidgetData.Actions.Add(removeAction);
                }
            }
            this.uxUXSwitch.SetActiveView(uxUXView);
        }
        else
        {
            this.uxUXSwitch.SetActiveView(uxOriginalView);
        }
    }

    protected void lbEdit_Click(object sender, EventArgs e)
    {
        this.OnEdit();
    }

    public override void OnEdit()
    {
        lbEditWidget.Attributes.Add("data-ektron-editMode", "true");
        if (this.IsUXEnabled)
        {
            UXOpenWidgetInModal();
        }
        editing = true;
        base.OnEdit();
    }

    protected void lbDelete_Click(object sender, EventArgs e)
    {
        Control tmp = this;
        PageFactory.GetDropZone(this).UpdatePanel.Update();
        base.Delete();
    }

    protected void widgetPanelLoad(object sender, EventArgs e)
    {
        //WidgetHost_Load();
        //toolbar.Attributes.Remove("Deleted");
    }
    #endregion
}