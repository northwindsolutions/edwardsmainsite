Ektron.ready(function () {
    if ("undefined" === typeof Ektron.PageBuilder) {
        Ektron.PageBuilder = {};
    }

    if ("undefined" === typeof Ektron.PageBuilder.Wizards) {
        Ektron.PageBuilder.Wizards =
        {
            // PROPERTIES
            mode: "add",
            language: 1033,
            currentStep: 1,
            finishClicked: false,

            // METHODS
            centerModal: function (selector) {
                $ektron(selector).position('center center');
            },

            checkAliasName: function (options) {
                var returnValue = false,
                    params = {
                        aliasName: "",
                        extension: ".aspx",
                        language: 1033,
                        folderid: 0
                    };
                $ektron.extend(params, options);
                $ektron.ajax({
                    url: Ektron.ResourceText.PageBuilder.Wizards.appPath + "urlaliasdialoghandler.ashx?action=checkaliasname&aliasname=" + params.aliasName + "&fileextension=" + params.extension + "&langtype=" + params.language + "&folderid=" + params.folderid,
                    cache: false,
                    async: false,
                    success: function (html) {
                        if (html.indexOf("<aliasname>") != -1) {
                            returnValue = true;
                            return true;
                        }
                        else {
                            returnValue = html;
                            return false;
                        }
                    }
                });
                return returnValue;
            },

            init: function () {
                // add the Markup to the page
                Ektron.PageBuilder.Wizards.Markup.init();

                // initialize Buttons
                Ektron.PageBuilder.Wizards.Buttons.init();

                // initialize Modals
                Ektron.PageBuilder.Wizards.Modals.init();
            },

            parseAliasExtension: function (alias) {
                var result = "/"
                // get everything after the last "/"
                alias = alias.substring(alias.lastIndexOf("\/") + 1);
                // now lop off anything that might be a query string
                if (alias.indexOf("?") !== -1) {
                    alias = alias.substring(0, alias.indexOf("?"));
                }
                // check for a period in what's left
                if (alias.lastIndexOf(".") > -1) {
                    // get everythign after the last period
                    result = alias.substring(alias.lastIndexOf(".") + 1);
                }
                if (result.length === 0) {
                    result = "/";
                }
                return result
            },

            redirectIframe: function (iframeSelector, newUrl) {
                var iframe = $ektron(iframeSelector);
                $ektron(iframeSelector).attr("src", newUrl);
            },

            redirectPage: function () {
                var iframe = $ektron(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe"),
                    iframeContents = iframe.contents(),
                    fullUrlAlias = iframeContents.find(".redirectMessage input[type='hidden']").val();

                if (location.href.toLowerCase().indexOf(Ektron.ResourceText.PageBuilder.Wizards.appPath.toLowerCase()) != -1) {
                    $ektron(this).parents().filter(".ektronPageBuilderWizard").dialog("close");
                    window.open(fullUrlAlias, "_blank");
                    location.href = location.href;
                }
                else {
                    window.location = fullUrlAlias;
                }
                return false;
            },

            showAddMasterPage: function (options) {
                Ektron.PageBuilder.Wizards.currentStep = 1;
                Ektron.PageBuilder.Wizards.finishClicked = false;
                var addPageDialog = $ektron(".ektronPageBuilderWizard"),
                    pageTitle = addPageDialog.find(".ektronModalHeader h3 span.addPageTitle"),
                    iframe = addPageDialog.find("iframe.ektronPageBuilderAddPageIframe"),
                    buttons = addPageDialog.find(".ektronPageBuilderWizardButtons");

                addPageDialog.addClass("MasterLayoutWizard");

                //reset buttons shown
                buttons.find(".button").hide();
                buttons.find(".nextButton").show();
                buttons.find(".cancelButton").show();

                params = {
                    mode: "add",
                    language: "",
                    folderId: "",
                    pageid: "",
                    defaulttaxid: "-1",
                    animateRedirect: false,
                    isChangeFolder: false
                };
                // if an options object is provided,
                // extend the params with the options
                $ektron.extend(params, options);

                Ektron.PageBuilder.Wizards.language = params.language;
                if (params.mode == "add") {
                    Ektron.PageBuilder.Wizards.mode = "add";
                    addPageDialog.dialog("option", "title", Ektron.ResourceText.PageBuilder.Wizards.addMasterLayout);
                    iframe.css("height", "30.5em");
                }
                else {
                    Ektron.PageBuilder.Wizards.mode = "saveAs";
                    addPageDialog.dialog("option", "title", Ektron.ResourceText.PageBuilder.Wizards.savePageAs);
                    Ektron.PageBuilder.Wizards.currentStep++;
                }
                // show the modal
                if (!params.isChangeFolder) {
                    addPageDialog.dialog("open");
                }

                // alter UI based on mode paramter
                Ektron.PageBuilder.Wizards.Status.loading();
                if (params.animateRedirect) {
                    var iframe = $ektron(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe");
                    iframe.fadeTo(500, .01, function () {
                        Ektron.PageBuilder.Wizards.redirectIframe(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe", Ektron.ResourceText.PageBuilder.Wizards.path + "addmasterpage.aspx?folderid=" + params.folderId + "&language=" + params.language + "&LangType=" + params.language + "&mode=" + params.mode + "&pageid=" + params.pageid);
                    });
                } else {
                    Ektron.PageBuilder.Wizards.redirectIframe(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe", Ektron.ResourceText.PageBuilder.Wizards.path + "addmasterpage.aspx?folderid=" + params.folderId + "&language=" + params.language + "&LangType=" + params.language + "&mode=" + params.mode + "&pageid=" + params.pageid);
                }

                return false;
            },

            showAddPage: function (options) {
                Ektron.PageBuilder.Wizards.currentStep = 1;
                Ektron.PageBuilder.Wizards.finishClicked = false;
                var addPageDialog = $ektron(".ektronPageBuilderWizard"),
                    pageTitle = addPageDialog.find(".ektronModalHeader h3 span.addPageTitle"),
                    iframe = addPageDialog.find("iframe.ektronPageBuilderAddPageIframe"),
                    buttons = addPageDialog.find(".ektronPageBuilderWizardButtons");

                addPageDialog.removeClass("MasterLayoutWizard");

                //reset buttons shown
                buttons.find(".button").hide();
                buttons.find(".nextButton").show();
                buttons.find(".cancelButton").show();

                params = {
                    mode: "add",
                    language: "",
                    folderId: "",
                    pageid: "",
                    defaulttaxid: "-1",
                    animateRedirect: false,
                    isChangeFolder: false
                };
                // if an options object is provided,
                // extend the params with the options
                $ektron.extend(params, options);

                Ektron.PageBuilder.Wizards.language = params.language;
                if (params.mode == "add") {
                    Ektron.PageBuilder.Wizards.mode = "add";
                    addPageDialog.dialog("option", "title", Ektron.ResourceText.PageBuilder.Wizards.addPage);
                    iframe.css("height", "30.5em");
                }
                else {
                    Ektron.PageBuilder.Wizards.mode = "saveAs";
                    addPageDialog.dialog("option", "title", Ektron.ResourceText.PageBuilder.Wizards.savePageAs);
                    Ektron.PageBuilder.Wizards.currentStep++;
                }
                if (!params.isChangeFolder) {
                    // show the modal
                    addPageDialog.dialog("open");
                }

                // alter UI based on mode paramter
                Ektron.PageBuilder.Wizards.Status.loading();
                if (params.animateRedirect) {
                    var iframe = $ektron(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe");
                    iframe.fadeTo(500, .01, function () {
                        Ektron.PageBuilder.Wizards.redirectIframe(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe", Ektron.ResourceText.PageBuilder.Wizards.path + "addpage.aspx?folderid=" + params.folderId + "&language=" + params.language + "&LangType=" + params.language + "&mode=" + params.mode + "&pageid=" + params.pageid + "&taxonomyid=" + params.defaulttaxid);
                    });
                } else {
                    Ektron.PageBuilder.Wizards.redirectIframe(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe", Ektron.ResourceText.PageBuilder.Wizards.path + "addpage.aspx?folderid=" + params.folderId + "&language=" + params.language + "&LangType=" + params.language + "&mode=" + params.mode + "&pageid=" + params.pageid + "&taxonomyid=" + params.defaulttaxid);
                }

                return false;
            },

            stepNext: function (clickedButton) {
                var wizardFrame = $ektron(".ektronPageBuilderWizard");
                if (Ektron.PageBuilder.Wizards.currentStep == 1) {
                    //check if folder selector is out and something new has been selected without being confirmed. if so, ask user if they want to switch
                    var iframe = $ektron(".ektronPageBuilderIframe");
                    var findercontainer = iframe.contents().find("div.ektronPageBuilderPageLayoutsFolderSelector > div");
                    if (findercontainer.length > 0) {
                        //it's open
                        var selections = findercontainer.find(".ui-finder-list-item-active");
                        if (selections.length > 0) {
                            //they've selected something
                            //now get the path to the selected folder, and confirm
                            var selectedfolder = "";
                            var startindex = 0;
                            var selectedel;
                            if (selections.length > 2) {
                                startindex = selections.length - 2;
                                selectedfolder += "...";
                            }
                            for (var i = startindex; i < selections.length; i++) {
                                selectedel = $ektron(selections[i]).children("a");
                                selectedfolder += "/" + selectedel.text();

                            }
                            selectedfolder += "/";
                            //selectedfolder now contains the path. make sure it's a valid choice
                            if ($ektron(selections[selections.length - 1]).hasClass("hasWireframe")) {
                                var response = confirm('You have not confirmed changing folders to ' + selectedfolder + '. Would you like to do so now?');
                                if (response) {
                                    var ismasterwizard = false;
                                    var params =
                                    {
                                        mode: "add",
                                        language: "",
                                        folderId: "",
                                        pageid: "",
                                        defaulttaxid: "-1",
                                        animateRedirect: false,
                                        isChangeFolder: true
                                    };
                                    var url = iframe.attr("src");
                                    if (url.toLowerCase().indexOf("addmasterpage.aspx") > -1) {
                                        ismasterwizard = true;
                                    }
                                    url = url.split("?")[1];
                                    url = url.split("&");
                                    for (var j = 0; j < url.length; j++) {
                                        var pair = url[j].split("=");
                                        pair[0] = pair[0].toLowerCase();
                                        if (pair[0] == "folderid") {
                                            params.folderId = pair[1];
                                        } else if (pair[0] == "language") {
                                            params.language = pair[1];
                                        } else if (pair[0] == "mode") {
                                            params.mode = pair[1];
                                        } else if (pair[0] == "pageid") {
                                            params.pageid = pair[1];
                                        } else if (pair[0] == "taxonomyid") {
                                            params.defaulttaxid = pair[1];
                                        }
                                    }

                                    //get new folderid
                                    params.folderId = selectedel.attr("href").split("=")[1];

                                    if (ismasterwizard) {
                                        //update iframe to new folder location
                                        Ektron.PageBuilder.Wizards.showAddMasterPage(params);
                                    } else {
                                        Ektron.PageBuilder.Wizards.showAddPage(params);
                                    }
                                }
                                return false;
                            } else {
                                alert('The folder you have selected (' + selectedfolder + ') does not have any wireframes associated with it, and cannot be used for pagebuilder content.');
                                return false;
                            }
                        }
                    }
                }
                if (!wizardFrame.hasClass("MasterLayoutWizard")) {
                    var retval = true;
                    Ektron.PageBuilder.Wizards.currentStep++;
                    if (Ektron.PageBuilder.Wizards.currentStep == 1) {
                        retval = Ektron.PageBuilder.Wizards.stepOne(clickedButton);
                    } else if (Ektron.PageBuilder.Wizards.currentStep == 2) {
                        retval = Ektron.PageBuilder.Wizards.stepTwo(clickedButton);
                    } else if (Ektron.PageBuilder.Wizards.currentStep == 3) {
                        retval = Ektron.PageBuilder.Wizards.stepThree(clickedButton);
                    }
                    if (!retval) Ektron.PageBuilder.Wizards.currentStep--;
                    return false;
                } else {
                    var retval = true;
                    Ektron.PageBuilder.Wizards.currentStep++;
                    if (Ektron.PageBuilder.Wizards.currentStep == 1) {
                        retval = Ektron.PageBuilder.Wizards.stepOne(clickedButton);
                    } else if (Ektron.PageBuilder.Wizards.currentStep == 2) {
                        retval = Ektron.PageBuilder.Wizards.stepThree(clickedButton);
                    }
                    if (!retval) Ektron.PageBuilder.Wizards.currentStep--;
                    return false;
                }
                return false;
            },
            stepBack: function (clickedButton) {
                var wizardFrame = $ektron(".ektronPageBuilderWizard");

                if (!wizardFrame.hasClass("MasterLayoutWizard")) {
                    var retval = true;
                    Ektron.PageBuilder.Wizards.currentStep--;
                    if (Ektron.PageBuilder.Wizards.currentStep == 1) {
                        retval = Ektron.PageBuilder.Wizards.stepOne(clickedButton);
                    } else if (Ektron.PageBuilder.Wizards.currentStep == 2) {
                        retval = Ektron.PageBuilder.Wizards.stepTwo(clickedButton);
                    } else if (Ektron.PageBuilder.Wizards.currentStep == 3) {
                        retval = Ektron.PageBuilder.Wizards.stepThree(clickedButton);
                    }
                    if (!retval) Ektron.PageBuilder.Wizards.currentStep++;
                    return false;
                } else {
                    var retval = true;
                    Ektron.PageBuilder.Wizards.currentStep--;
                    if (Ektron.PageBuilder.Wizards.currentStep == 1) {
                        retval = Ektron.PageBuilder.Wizards.stepOne(clickedButton);
                    } else if (Ektron.PageBuilder.Wizards.currentStep == 2) {
                        retval = Ektron.PageBuilder.Wizards.stepThree(clickedButton);
                    }
                    if (!retval) Ektron.PageBuilder.Wizards.currentStep++;
                    return false;
                }
                return false;
            },
            stepOne: function (clickedButton) {
                clickedButton = $ektron(clickedButton);
                var clickedButtonGrandParent = clickedButton.parent().parent(),
                    modal = clickedButtonGrandParent.parent().parent(),
                    iframe = $ektron(".ektronPageBuilderIframe"),
                    iframeContents = iframe.contents(),
                    messages = modal.find(".messages"),
                    finishButton = clickedButtonGrandParent.find(".finishButton"),
                    nextButton = clickedButtonGrandParent.find(".nextButton"),
                    cancelButton = clickedButtonGrandParent.find(".cancelButton"),
                    backButton = clickedButtonGrandParent.find(".backButton"),
                    okButton = clickedButtonGrandParent.find(".okButton"),
                    step1 = iframeContents.find("#step1"),
                    step2 = iframeContents.find("#step2"),
                    step3 = iframeContents.find("#step3");

                modal.fadeOut(200);
                window.setTimeout(function () {
                    finishButton.hide();
                    nextButton.show();
                    cancelButton.show();
                    backButton.hide();
                    okButton.hide();
                    messages.empty();
                    step2.hide();
                    step3.hide();
                    if (Ektron.PageBuilder.Wizards.mode == "saveAs") {
                        iframe.css("height", "6em");
                    }
                    step1.find(".messages").empty();
                    step1.css("display", "block");
                    Ektron.PageBuilder.Wizards.centerModal(".ektronPageBuilderAddPage");
                    modal.fadeIn("slow");
                }, 200);
                return true;
            },


            stepTwo: function (clickedButton) {
                if (Ektron.PageBuilder.Wizards.verifyTemplateInfo() === false) {
                    return false;
                }

                clickedButton = $ektron(clickedButton);

                var clickedButtonGrandParent = clickedButton.parent().parent(),
                    modal = clickedButtonGrandParent.parent().parent(),
                    iframe = $ektron(".ektronPageBuilderIframe"),
                    iframeContents = iframe.contents(),
                    finishButton = clickedButtonGrandParent.find(".finishButton"),
                    nextButton = clickedButtonGrandParent.find(".nextButton"),
                    cancelButton = clickedButtonGrandParent.find(".cancelButton"),
                    backButton = clickedButtonGrandParent.find(".backButton"),
                    okButton = clickedButtonGrandParent.find(".okButton"),
                    messages = $ektron(".ektronPageBuilderAddPage .messages"),
                    step1 = iframeContents.find("#step1"),
                    step2 = iframeContents.find("#step2"),
                    step3 = iframeContents.find("#step3");

                modal.fadeOut(200);
                window.setTimeout(function () {
                    finishButton.hide();
                    nextButton.show();
                    cancelButton.show();
                    if (Ektron.PageBuilder.Wizards.mode !== "saveAs") {
                        backButton.show();
                    }
                    else {
                        backButton.hide();
                    }
                    okButton.hide();
                    step1.hide();
                    step3.hide();
                    iframe.css("height", "30.5em");
                    messages.empty();
                    step2.css("display", "block");
                    Ektron.PageBuilder.Wizards.centerModal(".ektronPageBuilderAddPage");
                    modal.fadeIn("slow");
                }, 200);
                return true;
            },

            stepThree: function (clickedButton) {
                var wizardFrame = $ektron(".ektronPageBuilderWizard");
                if (!wizardFrame.hasClass("MasterLayoutWizard")) {
                    if (Ektron.PageBuilder.Wizards.verifyTaxonomy() === false) {
                        return false;
                    }
                } else {
                    if (Ektron.PageBuilder.Wizards.verifyTemplateInfo() === false) {
                        return false;
                    }
                }

                //PageLayout Title Validation
                var iframe = $ektron(".ektronPageBuilderIframe"),
                    iframeContents = iframe.contents(),
                    pageTitle = iframeContents.find(".pageBuilderWizardPageTitle").val(),
                    messageContainer = $ektron(".ektronPageBuilderAddPage .messages");

                if (($ektron.trim(pageTitle)).length == 0) {
                    Ektron.PageBuilder.Wizards.updateMessage(
                        messageContainer,
                        Ektron.ResourceText.PageBuilder.Wizards.errorPageTitle
                    );
                    return false;
                }
                if (($ektron.trim(pageTitle)).length > 200) {
                    Ektron.PageBuilder.Wizards.updateMessage(
                        messageContainer,
                        "Title is too long. Please enter a title less than 200 characters."
                    );
                    return false;
                }
                /////
                if (!wizardFrame.hasClass("MasterLayoutWizard")) {
                    var manualaliaschecked = iframeContents.find(".createManualAlias input:checked");
                    if (manualaliaschecked.length > 0) {
                        var urlAlias = $ektron.trim(iframeContents.find(".pageBuilderWizardAlias").val()),
                            extension = $ektron.trim(iframeContents.find(".manualContainer select").val()),
                            folid = $ektron.trim(iframeContents.find(".manualContainer .Folid input").val());
                        var re = new RegExp(extension + "$", "g");
                        urlAlias = urlAlias.replace(re, ""); //strip extension if necessary

                        if (urlAlias.length == 0) {
                            Ektron.PageBuilder.Wizards.updateMessage(
                            messageContainer, Ektron.ResourceText.PageBuilder.Wizards.errorUrlAlias);
                            return false;
                        } else {
                            // verify the url/alias is valid.
                            // parse out the file extension to use
                            var aliasInUse = Ektron.PageBuilder.Wizards.checkAliasName({
                                aliasName: urlAlias,
                                extension: extension,
                                language: Ektron.PageBuilder.Wizards.language,
                                folderid: folid
                            });
                            if (aliasInUse !== true) {
                                //aliasInUse == true means alias already exists and should NOT be used.
                                // we've got an error
                                Ektron.PageBuilder.Wizards.updateMessage(messageContainer, "Alias already exists.");

                                return false;
                            }
                        }
                    }
                }
                
                clickedButton = $ektron(clickedButton);
                var clickedButtonGradparent = clickedButton.parent().parent(),
                    modal = clickedButtonGradparent.parent().parent(),
                    iframe = $ektron(".ektronPageBuilderIframe"),
                    buttonsToShow = clickedButtonGradparent.find(".backButton, .finishButton"),
                    finishButton = clickedButtonGradparent.find(".finishButton"),
                    nextButton = clickedButtonGradparent.find(".nextButton"),
                    cancelButton = clickedButtonGradparent.find(".cancelButton"),
                    backButton = clickedButtonGradparent.find(".backButton"),
                    okButton = clickedButtonGradparent.find(".okButton"),
                    messages = $ektron(".ektronPageBuilderAddPage .messages"),
                    iframeContents = $ektron(iframe).contents(),
                    step1 = iframeContents.find("#step1"),
                    step2 = iframeContents.find("#step2"),
                    step3 = iframeContents.find("#step3");

                modal.fadeOut(200);
                window.setTimeout(function () {
                    finishButton.show();
                    backButton.show();
                    nextButton.hide();
                    cancelButton.show();
                    okButton.hide();
                    step1.hide();
                    step2.hide();
                    iframe.css("height", "30.5em");
                    messages.empty();
                    step3.css("display", "block");
                    Ektron.PageBuilder.Wizards.centerModal(".ektronPageBuilderAddPage");
                    modal.fadeIn("slow");
                }, 200);
                return true;
            },

            updateMessage: function (messageContainer, message, messageType) {
                // search the message for any URLs and wrap them.
                message = message.replace(Ektron.RegExp.PageBuilderURL, ' <span class="EkForceWrap">$2</span>');
                message = message.replace(Ektron.RegExp.PageBuilderObjectName, function ($0_match) { return $0_match.replace(".", '<span class="EkForceWrap">.</span>'); });
                if (typeof (messageType) == "undefined") {
                    messageType = "error";
                }
                // NOTE: the messageContainer expected is an Ektron Library object
                if (messageContainer.length != 1) {
                    // message DIV to update was not found, or there is more than one
                    // DIV that matches the request
                    return false;
                }
                else {
                    messageContainer.empty();
                    messageContainer.html('<span class="' + messageType + '">' + message + "</span>").fadeIn("slow");
                }
                Ektron.PageBuilder.Wizards.centerModal(".ektronPageBuilderAddPage");
                return true;
            },

            verifyTemplateInfo: function () {
                var result = true,
                    iframe = $ektron(".ektronPageBuilderAddPage .ektronPageBuilderIframe"),
                    iframeContents = iframe.contents(),
                    iframeBodyHeight = iframeContents.find("body").outerHeight(),
                    pageTitle = iframeContents.find(".pageBuilderWizardPageTitle").val(),
                    urlAlias = iframeContents.find(".pageBuilderWizardAlias"),
                    AliasExt = iframeContents.find("#ExtensionDropdown").val(),
                    AliasOpts = iframeContents.find("#ExtensionDropdown > option"),
                    template = iframeContents.find("#ektronSelectedTemplate").val(),
                    messageContainer = $ektron(".ektronPageBuilderAddPage .messages");

                if (Ektron.PageBuilder.Wizards.mode === "add") {
                    // need to verify the template data
                    if (($ektron.trim(template)).length == 0) {
                        // template has not been selected
                        Ektron.PageBuilder.Wizards.updateMessage(messageContainer, Ektron.ResourceText.PageBuilder.Wizards.errorSelectLayout);
                        result = false;
                        return result;
                    }
                }
                return result;
            },

            verifyMetadataInfo: function () {
                var result = true;
                var iframe = $ektron(".ektronPageBuilderAddPage .ektronPageBuilderIframe");
                var messageContainer = $ektron(".ektronPageBuilderAddPage .messages");
                var reqfield = iframe.contents().find("[name='req_fields']");

                if (reqfield.length > 0) {
                    reqfield = reqfield.val();

                    var reqfield = reqfield.split(',');
                    for (var i = 0; i < reqfield.length; i++) {
                        if (reqfield[i] != "") {
                            var value = iframe.contents().find("#" + reqfield[i] + ", input[name='" + reqfield[i] + "']").val();
                            if (value == null || value == "") {
                                result = false;

                                Ektron.PageBuilder.Wizards.updateMessage(
                                    messageContainer,
                                    Ektron.ResourceText.PageBuilder.Wizards.errorMetadata
                                );

                                break;
                            }
                        }
                    }
                }
                return result;
            },

            verifyTaxonomy: function () {
                var result = true;
                var iframe = $ektron(".ektronPageBuilderAddPage .ektronPageBuilderIframe");
                var messageContainer = $ektron(".ektronPageBuilderAddPage .messages");
                var req = iframe.contents().find(".TaxRequiredBool");
                if (req.length > 0) {
                    if (req.html() == "true") {
                        //verify that at least one is checked
                        var checked = iframe.contents().find("div.treecontainer input.categoryCheck:checked");
                        if (checked.length == 0) checked = iframe.contents().find("div.treecontainer input.categoryCheck:checked"); //this is weird but ie doesn't necessarily return the correct set the first time.
                        if (checked.length == 0) {
                            Ektron.PageBuilder.Wizards.updateMessage(
                                messageContainer,
                                Ektron.ResourceText.PageBuilder.Wizards.errorTaxonomy
                            );
                            return false;
                        }
                    }
                }

                return result;
            },

            // CLASSES
            Buttons:
            {
                // PROPERTIES
                initialized: false,

                init: function () {
                    if (Ektron.PageBuilder.Wizards.Buttons.initialized == true) {
                        return;
                    }

                    var wizardButtons = $ektron(".ektronPageBuilderAddPage .ektronPageBuilderWizardButtons");
                    // bind button handlers
                    wizardButtons.find(".nextButton").bind("click", function (e) {
                        return Ektron.PageBuilder.Wizards.stepNext(this);
                    }
                    );

                    wizardButtons.find(".backButton").bind("click", function (e) {
                        return Ektron.PageBuilder.Wizards.stepBack(this);
                    }
                    );

                    wizardButtons.find(".cancelButton").bind("click", function (e) {
                        $ektron(this).closest(".ektronPageBuilderWizard").dialog("close");
                        if (location.href.indexOf(Ektron.ResourceText.PageBuilder.Wizards.appPath) != -1) {
                            location.href = location.href;
                        }
                        return false;
                    });

                    wizardButtons.find(".finishButton").on("click", function (e) {
                        if (!Ektron.PageBuilder.Wizards.finishClicked) {
                            var iframe = $ektron(".ektronPageBuilderAddPage .ektronPageBuilderIframe"),
                                iframeContents = iframe.contents(),
                                messageContainer = $ektron(".ektronPageBuilderAddPage .messages"),
                                wizardFrame = $ektron(".ektronPageBuilderWizard"),
                                submitButton = iframeContents.find("#btnFinish"),
                                pageTitle,
                                template;

                            messageContainer.empty();

                            if (wizardFrame.hasClass("MasterLayoutWizard")) {
                                //verify that template is selected and that title is valid
                                messageContainer = $ektron(".ektronPageBuilderAddPage .messages");

                                template = iframeContents.find("#ektronSelectedTemplate").val();
                                if (template == null || template == "") {
                                    Ektron.PageBuilder.Wizards.updateMessage(messageContainer, "You must select a wireframe to base this master layout on.");
                                    return false;
                                }
                                pageTitle = iframeContents.find(".pageBuilderWizardPageTitle").val();

                                if (($ektron.trim(pageTitle)).length == 0) {
                                    Ektron.PageBuilder.Wizards.updateMessage(messageContainer, Ektron.ResourceText.PageBuilder.Wizards.errorPageTitle);
                                    return false;
                                }
                                //check metadata + taxonomy
                                if (Ektron.PageBuilder.Wizards.verifyMetadataInfo() === false) {
                                    return false;
                                }
                                if (Ektron.PageBuilder.Wizards.verifyTaxonomy() === false) {
                                    return false;
                                }
                            }
                            else {
                                //check metadata + taxonomy
                                if (Ektron.PageBuilder.Wizards.verifyMetadataInfo() === false) {
                                    return false;
                                }
                            }
                            Ektron.PageBuilder.Wizards.finishClicked = true;
                            submitButton.click();
                        }
                        return false;
                    });

                    wizardButtons.find(".okButton").on("click", function (e) {
                        return Ektron.PageBuilder.Wizards.redirectPage();
                    });

                    Ektron.PageBuilder.Wizards.Buttons.initialized = true;
                },

                showPromptButtons: function (wizardButtons) {
                    wizardButtons.find(".button").hide();
                    wizardButtons.find(".cancelButton").show();
                    wizardButtons.find(".okButton").css('display', 'block');
                }
            },

            Markup:
            {
                // PROPERTIES
                initialized: false,

                init: function () {
                    if (Ektron.PageBuilder.Wizards.Markup.initialized == true) {
                        return;
                    }
                    if ($ektron("#PageBuilderAddPageModal").length > 0) {
                        // prevent the markup from being added twice
                        Ektron.PageBuilder.Wizards.Markup.initialized = true;
                        return;
                    }

                    var modal = new String();
                    modal = '<div class="ektronPageBuilderWizard ektronPageBuilderAddPage ektronPageBuilderDialog" id="PageBuilderAddPageModal">\n';
                    modal += '  <div>\n';
                    modal += '    <div class="messages"></div>\n';
                    modal += '    <iframe noresize="noresize" frameborder="0" border="0"  marginwidth="0" marginheight="0" id="ektronPageBuilderAddPageIframe" class="ektronPageBuilderIframe ektronPageBuilderAddPageIframe" scrolling="auto"></iframe>\n';
                    modal += '    <ul class="ektronPageBuilderWizardButtons clearfix">\n';
                    modal += '      <li><div class="wizardStatus"><p><span></span><b>' + Ektron.ResourceText.PageBuilder.Wizards.loading + '</b></p></div></li>\n';
                    modal += '      <li><a id="ektronPageBuilderFinish" title="' + Ektron.ResourceText.PageBuilder.Wizards.finish + '" class="button finishButton buttonRight" href="#' + Ektron.ResourceText.PageBuilder.Wizards.finish + '">' + Ektron.ResourceText.PageBuilder.Wizards.finish + '</a></li>\n';
                    modal += '      <li><a id="ektronPageBuilderFinish" title="' + Ektron.ResourceText.PageBuilder.Wizards.ok + '" class="button okButton buttonRight" href="#' + Ektron.ResourceText.PageBuilder.Wizards.ok + '">' + Ektron.ResourceText.PageBuilder.Wizards.ok + '</a></li>\n';
                    modal += '      <li><a id="ektronPageBuilderNext" title="' + Ektron.ResourceText.PageBuilder.Wizards.next + '" class="button nextButton buttonRight" href="#' + Ektron.ResourceText.PageBuilder.Wizards.next + '">' + Ektron.ResourceText.PageBuilder.Wizards.next + '</a></li>\n';
                    modal += '      <li><a id="ektronPageBuilderCancel" title="' + Ektron.ResourceText.PageBuilder.Wizards.cancel + '" class="button cancelButton buttonRight" href="#' + Ektron.ResourceText.PageBuilder.Wizards.cancel + '">' + Ektron.ResourceText.PageBuilder.Wizards.cancel + '</a></li>\n';
                    modal += '      <li><a id="ektronPageBuilderBack" title="' + Ektron.ResourceText.PageBuilder.Wizards.back + '" class="button backButton buttonRight" href="#' + Ektron.ResourceText.PageBuilder.Wizards.back + '">' + Ektron.ResourceText.PageBuilder.Wizards.back + '</a></li>\n';
                    modal += '    </ul>\n';
                    modal += '  </div>\n';
                    modal += '</div>\n';

                    $ektron("body").append(modal);

                    Ektron.PageBuilder.Wizards.Markup.initialized;
                }
            },

            Modals:
            {
                // PROPERTIES
                initialized: false,

                init: function () {
                    if (Ektron.PageBuilder.Wizards.Modals.initialized == true) {
                        return;
                    }

                    var addPage = $ektron(".ektronPageBuilderAddPage"),
                        messages = addPage.find(".messages"),
                        iframe = addPage.find("iframe.ektronPageBuilderAddPageIframe"),
                        appendToSelector = $ektron('.ux-app-siteApp-dialogs').length > 1 ? '.ux-app-siteApp-dialogs' : 'body';

                    addPage.dialog({
                        appendTo: appendToSelector,
                        autoOpen: false,
                        close: function (event, ui) {
                            messages.empty();
                            iframe.attr('src', 'javascript:""');
                        },
                        hide: 'fade',
                        modal: false,
                        minWidth: 670,
                        open: function(event, ui) {
                            $ektron(this).closest('.ui-dialog').css('zIndex', '100000001');
                        },
                        dragStart: function (event, ui) {
                            $ektron(this).closest('.ui-dialog').css('zIndex', '100000001');
                        },
                        dragStop: function (event, ui) {
                            $ektron(this).closest('.ui-dialog').css('zIndex', '100000001');
                        },
                        focus: function (event, ui) {
                            $ektron(this).closest('.ui-dialog').css('zIndex', '100000001');
                        },
                        position: 'center center',
                        overlay: 0,
                        show: 'fade'
                    });
                    Ektron.PageBuilder.Wizards.Modals.initialized;
                }
            },

            Status:
            {
                loading: function () {
                    $ektron("div.wizardStatus").fadeIn(1000);
                },
                doneLoading: function () {
                    $ektron(".ektronPageBuilderWizard iframe.ektronPageBuilderAddPageIframe").fadeTo(500, 1)
                    $ektron("div.wizardStatus").fadeOut(1000);
                }
            }
        };
    }

    // initialize the Wizards
    Ektron.PageBuilder.Wizards.init();
    // Define Reular Expressions for later use
    Ektron.RegExp.PageBuilderURL = new RegExp("(^|[ \t\r\n])((ftp|http|https|file)://(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?(([A-Za-z0-9$_+!*();/?:~-])|%[A-Fa-f0-9]{2}))", "gi");
    Ektron.RegExp.PageBuilderObjectName = new RegExp("\\w+(\\.\\w+)+", "g");
});
