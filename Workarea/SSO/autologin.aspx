<%@ Page Language="C#" AutoEventWireup="true" CodeFile="autologin.aspx.cs" Inherits="autologin" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>autologin</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <meta http-equiv="Pragma" content="no-cache" />
    <link rel="stylesheet" href="../csslib/ektron.workarea.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../java/jfunct.js">
    </script>

</head>
<body class="Login">
    <form id="LoginRequestForm" method="post" runat="server">
    <asp:Literal ID="ltrredirect" runat="server"></asp:Literal>
    </form>
</body>
</html>
