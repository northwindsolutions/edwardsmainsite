﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;

public partial class Workarea_STSVerselect : Ektron.Cms.Workarea.Page
{
    private string DMSCookieName = "DMS_Office_ver";
    protected Ektron.Cms.ContentAPI contentAPI = new Ektron.Cms.ContentAPI();
    protected StyleHelper m_refStyle = new StyleHelper();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        
        if (!string.IsNullOrEmpty(Request.QueryString["action"]))
        {
            string newVal="";
            if (Request.QueryString["action"] == "switchversion")
            {
                if (Request.Cookies["DMS_Office_ver"].Value == "2010")
                    newVal = "2003";
                else
                    newVal = "2010";
            }
            HttpCookie c = new HttpCookie(DMSCookieName, newVal);
            c.Expires = DateTime.Now.AddYears(50);
            Response.Cookies.Remove(c.Name);
            Response.Cookies.Add(c);
            Response.Redirect(Request.UrlReferrer.AbsoluteUri);
        }
        else
        {

            lit_VerionSelect.Text = contentAPI.EkMsgRef.GetMessage("lbl dms office ver sel header");
            foreach (ListItem li in rbl_OfficeVersion.Items)
            {
                if (li.Value == "2010")
                    li.Text = contentAPI.EkMsgRef.GetMessage("li text office 2010 name");
                else
                    li.Text = contentAPI.EkMsgRef.GetMessage("li text other office ver name");
            }
        }
        RegisterResources();
       
    }

    protected void RegisterResources()
    {
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss);        
    }

    protected void btn_VersionSelect_Click(object sender, EventArgs e)
    {
        //Create cookie
        HttpCookie c = new HttpCookie(DMSCookieName, rbl_OfficeVersion.SelectedValue);
        c.Expires = DateTime.Now.AddYears(50);
        Response.Cookies.Remove(c.Name);
        Response.Cookies.Add(c);
        if (!string.IsNullOrEmpty(Request.QueryString["NextUrl"]))
        {

            string redriectStr = ("parent.location='" + Request.QueryString["NextUrl"] + Request.Url.Query + "';");
            redriectStr = redriectStr.Replace("ContType=9875", "ContType=9876");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", redriectStr, true);
        }
        else
        {
            string RetURL = string.Format("{0}?action={1}&id={2}&treeViewId=0", Request.QueryString["back_file"], Request.QueryString["back_action"], Request.QueryString["back_id"]);
            Response.Redirect(RetURL);
        }
    }
}