﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.VisualBasic;
using Ektron.Cms;
using Ektron.Cms.Common;

public partial class Workarea_Subscriptionuserlist : Ektron.Cms.Workarea.Page
{
    protected SiteAPI m_refApi = new SiteAPI();
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected UserAPI m_refUserApi = new UserAPI();
    protected EkMessageHelper m_refMsg;
    protected int ContentLanguage;
    protected int m_intCurrentPage = 1;
    protected int m_intTotalPages = 1;
    protected string m_strSelectedItem = "-1";
    protected StyleHelper m_refStyle = new StyleHelper();
    const string PAGE_NAME = "subscriptions.aspx";
    protected UserData[] user_list;
    protected string imagePath = "";
    protected string m_strSearchText = "";
    protected string m_strKeyWords = "";
    protected string OrderBy = "";
    protected string m_strDirection = "asc";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            jsStyleSheet.Text = (new StyleHelper()).GetClientScript();
            m_refMsg = (new CommonApi()).EkMsgRef;            
            if ((m_refContentApi.EkContentRef).IsAllowed(0, 0, "users", "IsLoggedIn", m_refContentApi.UserId) == false)
            {
                Response.Redirect("login.aspx?fromLnkPg=1", false);
                return;
            }
            if (m_refContentApi.RequestInformationRef.IsMembershipUser > 0 || m_refContentApi.RequestInformationRef.UserId == 0)
            {
                Response.Redirect("reterror.aspx?info=Please login as cms user", false);
                return;
            }
            if (!(Request.QueryString["LangType"] == null))
            {
                if (Request.QueryString["LangType"] != "")
                {
                    ContentLanguage = Convert.ToInt32(Request.QueryString["LangType"]);
                    m_refApi.SetCookieValue("LastValidLanguageID", ContentLanguage.ToString());
                }
                else
                {
                    if (m_refApi.GetCookieValue("LastValidLanguageID") != "")
                    {
                        ContentLanguage = Convert.ToInt32(m_refApi.GetCookieValue("LastValidLanguageID"));
                    }
                }
            }
            else
            {
                if (m_refApi.GetCookieValue("LastValidLanguageID") != "")
                {
                    ContentLanguage = Convert.ToInt32(m_refApi.GetCookieValue("LastValidLanguageID"));
                }
            }
            this.uxPaging.Visible = false;

            Utilities.SetLanguage(m_refUserApi);
        
            imagePath = m_refContentApi.AppPath + "images/ui/icons/";
            RegisterResources();
            if (!(Request.QueryString["action"] == null))
            {
                if (Request.QueryString["action"].Equals("ViewSubscribeuser", StringComparison.CurrentCultureIgnoreCase))
                {
                    long id = 0;
                    id = Convert.ToInt64(Request.QueryString["id"]);
                    string strName = Request.QueryString["SubName"];
                    txtTitleBar.InnerHtml = m_refStyle.GetTitleBar((string)(m_refMsg.GetMessage("view subscribers of msg") + " \"" + strName + "" + "\""));
                    DisplaySubscriptionUsers(id);
                    
                }
            }
        }
        catch (Exception ex)
        {
            Utilities.ShowError(EkFunctions.UrlEncode(ex.Message));
        }
    }
    private void DisplaySubscriptionUsers(long id)
    {
        if (Page.IsPostBack && Request.Form[isPostData.UniqueID] != "")
        {
            if (Request.Form[isSearchPostData.UniqueID] != "")
            {
                CollectSearchText();                
            }
        }
        UserRequestData req = new UserRequestData();
        m_intCurrentPage = System.Convert.ToInt32(this.uxPaging.SelectedPage);
        if (Request.QueryString["OrderBy"] == "" || Request.QueryString["OrderBy"] == string.Empty || Request.QueryString["OrderBy"] == null)
        {
            OrderBy = "user_name";
        }
        else
        {
            OrderBy = Request.QueryString["OrderBy"];
        }
        req.Group = id;
        req.OrderDirection = m_strDirection;
        req.OrderBy = OrderBy;
        req.SearchText = m_strSearchText;
        req.PageSize = m_refContentApi.RequestInformationRef.PagingSize;
        req.CurrentPage = m_intCurrentPage + 1;
        user_list = m_refUserApi.GetAllSubscriptionUserListBySubscriptionID(ref req);
        m_intTotalPages = req.TotalPages;
        Populate_ViewCMSSubscriptionUsersGrid(user_list);
        ViewSubscriptionuserToolBar();
        isPostData.Value = "true";
    }
    private void Populate_ViewCMSSubscriptionUsersGrid(UserData[] data)
    {
        System.Web.UI.WebControls.BoundColumn colBound;

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "USERNAME";
        colBound.HeaderText = m_refMsg.GetMessage("generic Username");
        colBound.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(23);
        colBound.ItemStyle.Width = Unit.Percentage(23);
        MapCMSSubUserToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "LASTNAME";
        colBound.HeaderText = m_refMsg.GetMessage("generic lastname");
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(23);
        colBound.ItemStyle.Width = Unit.Percentage(23);
        MapCMSSubUserToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "FirstName";
        colBound.HeaderText = m_refMsg.GetMessage("generic firstname");
        colBound.HeaderStyle.Width = Unit.Percentage(23);
        colBound.ItemStyle.Width = Unit.Percentage(23);
        colBound.ItemStyle.Wrap = false;
        MapCMSSubUserToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "LANGUAGE";
        colBound.HeaderText = m_refMsg.GetMessage("generic Language"); 
        colBound.ItemStyle.Wrap = false;
        MapCMSSubUserToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "LASTLOGINDATE";
        colBound.HeaderText = m_refMsg.GetMessage("generic lastlogindate"); 
        colBound.ItemStyle.Wrap = false;
        MapCMSSubUserToADGrid.Columns.Add(colBound);

        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add(new DataColumn("USERNAME", typeof(string)));
        dt.Columns.Add(new DataColumn("LASTNAME", typeof(string)));
        dt.Columns.Add(new DataColumn("FIRSTNAME", typeof(string)));
        dt.Columns.Add(new DataColumn("LANGUAGE", typeof(string)));
        dt.Columns.Add(new DataColumn("LASTLOGINDATE", typeof(string)));
        int i = 0;
        if (!(data  == null))
        {
            for (i = 0; i <= data.Length - 1; i++)
            {
                dr = dt.NewRow();
                dr["USERNAME"] = data[i].Username;
                dr["LASTNAME"] = data[i].LastName;
                dr["FIRSTNAME"] = data[i].FirstName;
                dr["LANGUAGE"] = data[i].LanguageName;
                dr["LASTLOGINDATE"] = data[i].LastLoginDate;
                dt.Rows.Add(dr);
            }
        }
        DataView dv = new DataView(dt);
        MapCMSSubUserToADGrid.PageSize = this.m_refContentApi.RequestInformationRef.PagingSize;
        MapCMSSubUserToADGrid.DataSource = dv;
        MapCMSSubUserToADGrid.CurrentPageIndex = m_intCurrentPage;
        MapCMSSubUserToADGrid.DataBind();
        if (m_intTotalPages > 1)
        {
            this.uxPaging.Visible = true;
            this.uxPaging.TotalPages = m_intTotalPages;
            this.uxPaging.CurrentPageIndex = m_intCurrentPage;
        }
        else
        {
            this.uxPaging.Visible = false;
        }
    }
    private void ViewSubscriptionuserToolBar()
    {
        System.Text.StringBuilder result = new System.Text.StringBuilder();
        int i = 0;
        result.Append("<table><tr>");
        result.Append(m_refStyle.GetButtonEventsWCaption(imagePath + "back.png", System.Convert.ToString(PAGE_NAME + "?action=ViewAllSubscriptions"), m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
        result.Append(StyleHelper.ActionBarDivider);
        result.Append("<td width=\"100%\" align=\"right\"><label for=\"txtSearch\">" + m_refMsg.GetMessage("generic search") + "</label><input type=text class=\"ektronTextMedium\" id=txtSearch name=txtSearch value=\"" + m_strKeyWords + "\" onkeydown=\"CheckForReturn(event)\" />");
        result.Append(" <select id=searchlist name=searchlist>");
        result.Append("     <option value=-1" + IsSelected("-1") + ">" + m_refMsg.GetMessage("generic all") + "</option>");
        result.Append("     <option value=\"last_name\"" + IsSelected("last_name") + ">" + m_refMsg.GetMessage("generic lastname") + "</option>");
        result.Append("     <option value=\"first_name\"" + IsSelected("first_name") + ">" + m_refMsg.GetMessage("generic firstname") + "</option>");
        result.Append("     <option value=\"user_name\"" + IsSelected("user_name") + ">" + m_refMsg.GetMessage("generic username") + "</option>");
        result.Append(" </select>");
        result.Append(" <input type=button value=" + m_refMsg.GetMessage("btn search") + " id=btnSearch name=btnSearch onclick=\"searchuser();\" class=\"ektronWorkareaSearch\" title=\"" + m_refMsg.GetMessage("lbl Search Users") + "\" />");

        result.Append("</td>");
        result.Append(StyleHelper.ActionBarDivider);
        result.Append("<td>" + m_refStyle.GetHelpButton("viewsubscriptions", "") + "</td>");
        result.Append("</tr></table>");
        htmToolBar.InnerHtml = result.ToString();
    }
    private void CollectSearchText()
    {
        m_strKeyWords = Request.Form["txtSearch"];
        m_strSelectedItem = Request.Form["searchlist"];
        if (m_strSelectedItem == "-1")
        {
            m_strSearchText = " (first_name like \'%" + Quote(m_strKeyWords) + "%\' OR last_name like \'%" + Quote(m_strKeyWords) + "%\' OR user_name like \'%" + Quote(m_strKeyWords) + "%\')";
        }
        else if (m_strSelectedItem == "last_name")
        {
            m_strSearchText = " (last_name like \'%" + Quote(m_strKeyWords) + "%\')";
        }
        else if (m_strSelectedItem == "first_name")
        {
            m_strSearchText = " (first_name like \'%" + Quote(m_strKeyWords) + "%\')";
        }
        else if (m_strSelectedItem == "user_name")
        {
            m_strSearchText = " (user_name like \'%" + Quote(m_strKeyWords) + "%\')";
        }
    }
    private string Quote(string KeyWords)
    {
        string result = KeyWords;
        if (KeyWords.Length > 0)
        {
            result = KeyWords.Replace("\'", "\'\'");
        }
        return result;
    }
    private string IsSelected(string val)
    {
        if (val == m_strSelectedItem)
        {
            return (" selected ");
        }
        else
        {
            return ("");
        }
    }
    protected void RegisterResources()
    {
        // register JS
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronUICoreJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronUITabsJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronInputLabelJS);
        Ektron.Cms.API.JS.RegisterJS(this, m_refContentApi.AppPath + "java/ektron.workarea.searchBox.inputLabelInit.js", "EktronWorkareaSearchBoxInputLabelInitJS");
        Ektron.Cms.API.JS.RegisterJS(this, m_refContentApi.AppPath + "/wamenu/includes/com.ektron.ui.menu.js", "EktronWamenuJs");
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronWorkareaHelperJS);

        // register CSS
        Ektron.Cms.API.Css.RegisterCss(this, m_refContentApi.AppPath + "csslib/ActivityStream/activityStream.css", "ActivityStream");
        Ektron.Cms.API.Css.RegisterCss(this, m_refContentApi.AppPath + "/wamenu/css/com.ektron.ui.menu.css", "EktronWamenuCss");

        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronToolBarRollJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronModalJS);
    }
}