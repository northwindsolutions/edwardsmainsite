﻿<%@ WebHandler Language="C#" Class="Tasks" %>

using System;
using System.Web;
using Ektron.Cms.UI.CommonUI;
using Ektron.Cms.Content;
using Microsoft.VisualBasic;
using System.Collections.Generic;

public class Tasks : IHttpHandler
{

    #region Members
   
   
    private System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
    private Ektron.Cms.Framework.Settings.TaskCategoryManager tcmanager = new Ektron.Cms.Framework.Settings.TaskCategoryManager();
    private Ektron.Cms.Framework.Settings.TaskManager tmanager = new Ektron.Cms.Framework.Settings.TaskManager();
    private long categoryId = -1;
    #endregion

    public void ProcessRequest (HttpContext context) {

        context.Response.ContentType = "application/json";
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;
        if(long.TryParse(context.Request.QueryString["categoryId"],out categoryId))
        {
            categoryId = Convert.ToInt64(context.Request.QueryString["categoryId"]);
        }
        long outvalue;
        if (context.Request.QueryString["get"] != null && long.TryParse(context.Request.QueryString["get"],out outvalue) != true)
        {
            object d;
            switch (context.Request.QueryString["get"])
            {
                
                case "allTasksByCategory":
                    if(categoryId >=0)
                    {
                        d = SelectTasksByCategory(categoryId);
                        data = new { d };
                        context.Response.Write(jss.Serialize(data));
                    }
                break;
                default:
                    d = SelectAllCategoryData();
                    data = new { d };
                    context.Response.Write(jss.Serialize(data));
                break;

            }

        }
       
    }

    protected List<Ektron.Cms.TaskData> SelectTasksByCategory(long categoryId)
    {
        try
        {
            Ektron.Cms.TaskCriteria tcrit = new Ektron.Cms.TaskCriteria();
            tcrit.AddFilter(Ektron.Cms.Common.TaskProperty.AssignedToGroupId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, categoryId);
            return tmanager.GetList(tcrit);
        }
        catch (Exception ex)
        {
            Ektron.Cms.EkException.LogException("There was an error retrieving the tasks collection: " + ex.Message.ToString() + " Exception: " + ex.InnerException.Message.ToString());
            List < Ektron.Cms.TaskData > tasks = new List<Ektron.Cms.TaskData>();
            return tasks;
        }
    }

    protected List<Ektron.Cms.TaskCategoryData> SelectAllCategoryData()
    {

        
        try
        {
            
            Ektron.Cms.TaskCategoryCriteria tcrit = new Ektron.Cms.TaskCategoryCriteria();
            tcrit.AddFilter(Ektron.Cms.Common.TaskCategoryProperty.Active, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, true);
            return tcmanager.GetList(tcrit);
            
            
        }
        catch (Exception ex)
        {
            Ektron.Cms.EkException.LogException("There was an error retrieving task categories and associated tasks: " + ex.Message.ToString() + " Exception: " + ex.InnerException.Message.ToString());
            List < Ektron.Cms.TaskCategoryData > tasks = new List<Ektron.Cms.TaskCategoryData>();
            return tasks;
        }

        

        
    }

    public object data
    {
        get;
        set;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}

