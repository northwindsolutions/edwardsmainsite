using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.User;
using Microsoft.VisualBasic;
using Ektron.Storage;
using System.Web;

public partial class Workarea_Upload : Ektron.Cms.Workarea.Page
{
    protected Ektron.Cms.CommonApi api = new Ektron.Cms.CommonApi();
    protected Ektron.Cms.ContentAPI m_refContApi = new Ektron.Cms.ContentAPI();
    protected string sTarget = "ekavatarpath";
    protected string pageAction = string.Empty;
    protected UserManager _userManager = new UserManager();

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if ((!Page.IsPostBack) && (m_refContApi.RequestInformationRef.UserId == 0))
        {
            System.Uri url = Context.Request.UrlReferrer;
            if ((url == null) && (m_refContApi.RequestInformationRef.UserId == 0))
            {
                Response.Redirect((string)("reterror.aspx?info=" + "You are not authorised to view this page!"), false);
            }
        }

        //Put user code to initialize the page here
        uploadButton.Text = api.EkMsgRef.GetMessage("upload txt");
        cancelButtonText.Text = api.EkMsgRef.GetMessage("btn cancel");
        string qString = Request.QueryString["addedit"];

        //Register JavaScript and StyleSheet resources
        RegisterResources();
        SetServerJSVariables();
        if (!string.IsNullOrEmpty(Request.QueryString["action"]))
        {
            pageAction = Request.QueryString["action"];
        }
        // defect:63021 For uploading avatar you need not have user validation for membership users.
        Utilities.ValidateUserLogin();
        if (qString == null)
        {
            if (Convert.ToBoolean(api.RequestInformationRef.IsMembershipUser) || api.RequestInformationRef.UserId == 0)
            {
                Response.Redirect(api.SitePath + "login.aspx", true);
                return;
            }
        }
        if (!string.IsNullOrEmpty(Request.QueryString["returntarget"])) // just say no to cross site scripting!
        {
            sTarget = (string)(Utilities.StripHTML(Request.QueryString["returntarget"]).Replace("\'", "\\\'"));
        }

        uploadButton.Attributes.Add("onclick", "return CheckUpload();");
    }

    public void UploadImage()
    {
        if (fileupload1.PostedFile != null) //Check to make sure we actually have a file to upload
        {
            string uploadedFileName = Path.GetFileName(fileupload1.PostedFile.FileName);

            if (!CheckUploadImage.IsImage(fileupload1.PostedFile) || !IsExtensionWhitelisted(Path.GetExtension(uploadedFileName))) //Make sure we are getting a valid JPG/gif image 
            {
                //Not a valid jpeg/gif image
                lbStatus.Text = api.EkMsgRef.GetMessage("lbl err avatar not valid extension");
                SetStatusScript(false, uploadedFileName);
                return;
            }
            if (fileupload1.PostedFile.ContentLength / 1024 > 200)
            {
                lbStatus.Text = string.Format(api.EkMsgRef.GetMessage("lbl avatar filesize exceeded"), uploadedFileName, fileupload1.PostedFile.ContentLength / 1024);
                SetStatusScript(false, uploadedFileName);
                return;
            }

            string strFileName = string.Empty;
            UserData uData = _userManager.GetItem(m_refContApi.UserId);
            bool shouldDelete = false;
            string oldFileName = uData.Avatar;//

            if (!string.IsNullOrEmpty(uData.Avatar))
            {
                oldFileName = Path.GetFileName(uData.Avatar);
                if (String.Equals(Path.GetExtension(uData.Avatar), Path.GetExtension(uploadedFileName), StringComparison.CurrentCultureIgnoreCase))
                {
                    strFileName = Path.GetFileName(uData.Avatar).ToLower().Replace("thumb_", ""); ;
                }
                else
                {
                    shouldDelete = true;
                }
            }
            if (string.IsNullOrEmpty(strFileName))
            {
                strFileName = Guid.NewGuid().ToString().Substring(0, 5) + "_u_" + uploadedFileName;// fix for defect 32686 two users with the same image and same name for avatar.
            }

            string thumbnailPath = Server.MapPath(api.SitePath + "uploadedimages");
            try
            {
                StorageClient.Context.File.UploadStream(fileupload1.PostedFile.InputStream, thumbnailPath + "\\" + strFileName);
                Utilities.ProcessThumbnail(thumbnailPath, strFileName);

                //save new avatar path
                uData.Avatar = strFileName;
                _userManager.Update(uData);

                if (shouldDelete)
                {
                    File.Delete(Server.MapPath(api.SitePath + "uploadedimages/" + oldFileName.TrimStart('/')));
                    File.Delete(Server.MapPath(api.SitePath + "uploadedimages/thumb_" + oldFileName.TrimStart('/')));
                }
            }
            catch (Exception ex)
            {
                Response.Redirect((string)("reterror.aspx?info=" + ex.Message.ToString()), false);
            }

            lbStatus.Text = string.Format(api.EkMsgRef.GetMessage("lbl success avatar uploaded"), strFileName, api.SitePath + "uploadedimages/" + strFileName);
            SetStatusScript(true, strFileName);
            return;
        }
    }

    protected void SetStatusScript(bool success, string strFileName)
    {
        var script = new StringBuilder();
        script.Append("<script language=\'javascript\'>").AppendLine();
        if (success)
        {
            script.Append("CheckUpHelper_ShowControls(false);").AppendLine();
            script.Append("self.parent.document.getElementById(\'" + sTarget + "\').value=\'" + api.SitePath + "uploadedimages/" + "thumb_" + Utilities.GetCorrectThumbnailFileWithExtn(strFileName) + "\';setTimeout(\'DialogClose()\',2000);").AppendLine();
        }
        else
        {
            script.Append("CheckUpHelper_ShowControls(true);").AppendLine();
        }
        script.Append("</script>").AppendLine();
        litScript.Text = script.ToString();
    }

    protected void uploadButton_Click(object sender, System.EventArgs e)
    {
        UploadImage();
    }

    protected void RegisterResources()
    {
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronThickBoxJS);
    }

    protected void SetServerJSVariables()
    {
        litErrSelAvatarUpload.Text = api.EkMsgRef.GetMessage("js err select avatar upload");
        litErrAvatarNotValidExtn.Text = api.EkMsgRef.GetMessage("lbl err avatar not valid extension");
    }

    public static class CheckUploadImage
    {
        public const int ImageMinimumBytes = 512;

        public static bool IsImage(HttpPostedFile postedFile)
        {
            //  Check the image mime types
            if (postedFile.ContentType.ToLower() != "image/jpg" &&
                        postedFile.ContentType.ToLower() != "image/jpeg" &&
                        postedFile.ContentType.ToLower() != "image/pjpeg" &&
                        postedFile.ContentType.ToLower() != "image/gif" &&
                        postedFile.ContentType.ToLower() != "image/x-png" &&
                        postedFile.ContentType.ToLower() != "image/png")
            {
                return false;
            }

            //  Check the image extension
            if (Path.GetExtension(postedFile.FileName).ToLower() != ".jpg"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".png"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".gif"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".jpeg")
            {
                return false;
            }

            //  Attempt to read the file and check the first bytes
            try
            {
                if (!postedFile.InputStream.CanRead)
                {
                    return false;
                }

                if (postedFile.ContentLength < ImageMinimumBytes)
                {
                    return false;
                }

                byte[] buffer = new byte[512];
                postedFile.InputStream.Read(buffer, 0, 512);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image

            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream)) { }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }

    private bool IsExtensionWhitelisted(string extension)
    {
        // retrieve the whitelist extensions from the web.config
        var whiteListText = ConfigurationManager.AppSettings.Get("ek_DMSFileTypeWhiteList");

        // if the whitelist doesn't exist, then populate it with default extensions
        if (string.IsNullOrWhiteSpace(whiteListText))
        {
            whiteListText = ".odb,*.ods,*.odg,*.odp,*.odf,*.odt,*.doc,*.xls,*.ppt,*.pdf,*.gif,*.jpg,*.jpeg,*.log,*.vsd,*.dot,*.zip,*.swf,*.wma,*.wav,*.avi,*.mp3,*.mp4,*.rm,*.wmv,*.ra,*.mov,*.png,*.docx,*.xlsx,*.pptx,*.vsdx,*.wmf,*.xml,*.htm,*.html,*.flv,*.pot,*.potx";
        }

        // split the whitelist text into an array of extensions
        // then trim each extension in the array of extentions
        var extensionWhiteList = Array.ConvertAll<string, string>(whiteListText.Split(','), s => s.Trim(' ', '\t', '*'));

        // check if the extension whitelist contains the extension passed into the function
        return Array.Exists(extensionWhiteList, s => s == extension.ToLower());
    }

}


