﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditViewAlias.aspx.cs"
    Inherits="Workarea_UrlAliasing_AddEditViewAlias" meta:resourcekey="PageResource1"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:Css ID="aliasCSS" Path="CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <ektronUI:JavaScript ID="aliasJS" runat="server" Path="js/ektron.workarea.urlaliasing.js" />
    <ektronUI:JavaScriptBlock ID="aliasJSLauncher" runat="server">
        <ScriptTemplate>
            Ektron.Workarea.UrlAliasing.initAddManual();
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSaveButton"
                                class="primary saveButton" ValidationGroup="saveAlias" OnClick="uxSaveButton_Click" />
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" meta:resourcekey="uxEditButton"
                                OnClick="uxEditButton_Click" Visible="false" />
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="uxDelete" class="primary deleteButton" meta:resourcekey="uxDelete"
                                OnClick="uxDelete_Click" Visible="false" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <ektronUI:Message ID="uxMessage" runat="server" Visible="<%# this.showMessage %>">
        </ektronUI:Message>
        <table class="ektronForm">
            <tr>
                <td class="label">
                    <asp:Label ID="uxActiveLabel" runat="server" meta:resourcekey="uxActiveLabel" Enabled='<%#this.IsEditable %>' />
                </td>
                <td class="value">
                    <asp:CheckBox ID="uxActiveCheckBox" runat="server" meta:resourcekey="uxActiveCheckBox"
                        Enabled='<%#this.IsEditable %>' />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="uxIsDefaultLabel" runat="server" meta:resourcekey="uxIsDefaultLabel"
                        Enabled='<%#this.IsEditable %>' />
                </td>
                <td class="value">
                    <asp:CheckBox ID="uxIsDefaultCheckBox" runat="server" meta:resourcekey="uxIsDefaultCheckBox"
                        Enabled='<%#this.IsEditable %>' />
                </td>
            </tr>
            <tr id="uxSiteRow" runat="server">
                <td class="label">
                    <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxSiteList" runat="server" meta:resourcekey="uxSiteList">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="langRow" runat="server">
                <td class="label">
                    <asp:Label ID="uxLanguageLabel" runat="server" meta:resourcekey="uxLanguageLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxLanguageList" runat="server" meta:resourcekey="uxLanguageList"
                        AppendDataBoundItems="true" Enabled="false" CssClass="languageChange">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="uxTypeRow" runat="server">
                <td class="label">
                    <asp:Label ID="uxSourceTypeLabel" runat="server" meta:resourcekey="uxSourceTypeLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxSourceTypeDropDownList" runat="server" meta:resourcekey="uxSourceTypeDropDownList" />
                </td>
            </tr>
            <tr id="uxRuleRow" runat="server">
                <td class="label">
                    <asp:Label ID="uxRuleLabel" runat="server" meta:resourcekey="uxRuleLabel" />
                </td>
                <td class="value">
                    <asp:TextBox ID="uxRuleTitle" runat="server" meta:resourcekey="uxRuleTitle" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div class="ektronTopSpace">
        </div>
        <fieldset>
            <legend>
                <asp:Literal ID="uxAliasDetailTitle" runat="server" meta:resourcekey="uxAliasDetailTitle" /></legend>
            <table class="ektronForm">
                <tr>
                    <td class="label">
                        <asp:Label ID="uxAliasLabel" runat="server" meta:resourcekey="uxAliasLabel" />
                    </td>
                    <td class="value">
                        <asp:Literal ID="uxSitePrefix" runat="server"></asp:Literal>
                        <ektronUI:TextField ID="uxAliasName" runat="server" meta:resourcekey="uxAliasName"
                            onmouseup="Ektron.Workarea.UrlAliasing.validateAliasName();" Enabled='<%#this.IsEditable %>'
                            Width="275px" ValidationGroup="saveAlias">
                            <ValidationRules>
                                <ektronUI:RequiredRule ErrorMessage="<%$ Resources:ErrorRequireduxAliasName %>" />
                                <ektronUI:MaxLengthRule ErrorMessage="<%$ Resources:uxAliasNameTooLong %>" MaxLength="250"  ClientValidationEnabled="true"  ServerValidationEnabled="true" />
                                <ektronUI:CustomRule ErrorMessage="<%$ Resources:uxUrlAliasErrorMessageInvalid %>" ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.UrlAliasing.validateManualAliasLocally" />
                            </ValidationRules>
                        </ektronUI:TextField>
                        <asp:DropDownList ID="uxExtensionDropDownList" meta:resourcekey="uxExtensionDropDownList"
                            Enabled='<%#this.IsEditable %>' runat="server" />
                        <ektronUI:ValidationMessage ID="uxValidationMessageuxAliasName" runat="server" AssociatedControlID="uxAliasName" />
                        <p>
                        </p>
                        <div id="uxUrlAliasError" style="display: none;">
                            <ektronUI:Message ID="uxUrlAliasErrorMessage" runat="server" DisplayMode="Error" />
                        </div>
                        <div id="uxUrlAliasErrorInvalid" style="display: none;">
                            <ektronUI:Message ID="uxUrlAliasErrorMessageInvalid" runat="server" DisplayMode="Error" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="uxQueryStringActionLabel" runat="server" meta:resourcekey="uxQueryStringActionLabel" />
                    </td>
                    <td class="value">
                        <asp:DropDownList ID="uxQueryStringAction" meta:resourcekey="uxQueryStringAction"
                            runat="server" Enabled='<%#this.IsEditable %>' />
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="uxAdditionalVariablesLabel" runat="server" meta:resourcekey="uxAdditionalVariablesLabel" />
                    </td>
                    <td class="value">
                        <asp:TextBox ID="uxAdditionalVariables" runat="server" meta:resourcekey="uxAdditionalVariables"
                            Enabled='<%#this.IsEditable %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset id="uxContentBlockContainer" runat="server">
            <legend>
                <asp:Literal ID="uxAliasTargetTitle" runat="server" meta:resourcekey="uxAliasTargetTitle"></asp:Literal></legend>
            <table class="ektronForm">
                <tr>
                    <td class="label">
                        <asp:Label ID="uxContentLabel" runat="server" meta:resourcekey="uxContentLabel" />
                    </td>
                    <td class="value">
                        <ektronUI:TextField ID="uxContentSelector" runat="server" meta:resourcekey="uxContentSelector"
                            Enabled='false'></ektronUI:TextField>
                        <asp:LinkButton ID="uxContentSelectButton" runat="server" OnClientClick="Ektron.Workarea.UrlAliasing.openContentDialog(); return false;"
                            Visible='<%#this.IsEditable %>' CssClass="button greenHover buttonCheckAll viewFolderList"
                            meta:resourcekey="uxContentSelectButton"></asp:LinkButton>
                        <div style="display: none;">
                            <ektronUI:TextField ID="frm_content_id_validation_box" runat="server" Value="<%# this.contentIDString %>"
                                Enabled='true' ValidationGroup="saveAlias">
                            <ValidationRules>
                                <ektronUI:RequiredRule ErrorMessage="<%$ Resources:ErrorContentRequired %>" />
                            </ValidationRules>
                            </ektronUI:TextField>
                        </div>
                        <ektronUI:ValidationMessage ID="uxValidationMessageUxContentSelector" runat="server"
                            AssociatedControlID="frm_content_id_validation_box" />
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="uxQuickLinkLabel" runat="server" meta:resourcekey="uxQuickLinkLabel" />
                    </td>
                    <td class="value">
                        <asp:DropDownList ID="uxQuicklinkSelector" meta:resourcekey="uxQuicklinkSelector"
                            runat="server" Enabled='<%#this.IsEditable %>' />
                    </td>
                </tr>
            </table>
        </fieldset>
        <ektronUI:Dialog ID="uxContentSelectorDialog" runat="server" AutoOpen="false" meta:resourcekey="uxContentSelectorDialog"
            Modal="true" Width="500" Height="500">
            <ContentTemplate>
                <ektronUI:Iframe ID="uxContentSelectIframe" CssClass="modalIframe" Height="450" Width="460"
                    runat="server" Src="<%# this.QuickLinkSelectorSRC %>">
                </ektronUI:Iframe>
            </ContentTemplate>
        </ektronUI:Dialog>
        <input type="hidden" value="<%# this.contentID %>" name="frm_content_id" id="frm_content_id"
            class="frm_content_id" runat="server" />
        <input type="hidden" value="<%# this.contentLangID %>" name="frm_content_langid"
            class="frm_content_langid" id="frm_content_langid" />
        <input type="hidden" value="<%# this.quicklink %>" name="frm_qlink" id="frm_qlink"
            class="frm_qlink" />
        <input type="hidden" value="0" name="uxaliasId" id="uxaliasId" runat="server" />
    </div>
    </form>
</body>
</html>
