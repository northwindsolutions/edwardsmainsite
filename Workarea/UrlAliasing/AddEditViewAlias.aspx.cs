using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Settings;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.Content;

public partial class Workarea_UrlAliasing_AddEditViewAlias : Ektron.Cms.Workarea.Page
{
    #region variables
    private SiteAPI siteAPI = new SiteAPI();
    private ContentAPI refContentApi = new ContentAPI();
    private ISite site;
    private const string querystringKey = "id";
    private const string querystringlangkey = "lang";
    private AliasData alias;
    private AliasRuleData rule;
    private AliasManager manager = new AliasManager();
    private AliasRuleManager ruleManager = new AliasRuleManager();
    private DisplayMode displayMode;
    private ContentManager contentManager = null;
    private string returnpath = "";
    private bool isvalid = true;
    public string QuickLinkSelectorSRC { get; set; }
    public string LanguageID { get; set; }
    public bool showMessage { get; set; }
    [System.ComponentModel.DefaultValue(false)]
    public bool IsEditable { get; set; }
    // client variables
    public long contentID { get; set; }
    public string contentIDString
    {
        get
        {
            return (contentID > 0) ? contentID.ToString() : "";
        }
        set
        {
            if (!string.IsNullOrEmpty(value)) contentID = long.Parse(value);
            else contentID = 0;
        }
    }
    public long contentLangID { get; set; }
    public string quicklink { get; set; }
    #endregion

    #region page Events

    protected void Page_Init(object sender, EventArgs e)
    {
        this.QuickLinkSelectorSRC = "../QuickLinkSelect.aspx?folderid=0&formName=frm_urlalias&titleFormElem=uxContentSelector_TextField_aspInput&useQLinkCheck=0&SetBrowserState=1&forcetemplate=1&disAllowAddContent=1";
        uxUrlAliasErrorMessage.Text = GetLocalResourceObject("errorAliasAlreadyExists").ToString();
        uxUrlAliasErrorMessageInvalid.Text = GetLocalResourceObject("uxUrlAliasErrorMessageInvalid").ToString();

        contentManager = new ContentManager();
        if (this.HasViewPermission())
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString[querystringKey]))
                {
                    alias = manager.GetItem(long.Parse(Request.QueryString[querystringKey]));
                    if (alias.Id == 0)
                    {
                        alias = null;
                    }
                    else
                    {
                        uxaliasId.Value = alias.Id.ToString();
                        rule = ruleManager.GetItem(alias.ConfigurationId);
                    }
                }
                this.IsEditable = ((alias != null && Page.IsPostBack) || (alias == null && !Page.IsPostBack));
                this.showMessage = false;
                this.displayMode = this.getDisplayMode();
                this.initWorkareaUI();
                this.setUI();
                this.QuickLinkSelectorSRC = this.QuickLinkSelectorSRC + "&LangType=" + LanguageID.ToString();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                //Utilities.ShowError(exc.Message);
                uxMessage.Visible = true;
                uxMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxMessage.Text = exc.Message;
                return;
            }
        }
        else
        {
            Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (isvalid)
        {
            // for inline databinding
            this.DataBind();
        }
    }

    protected void uxSaveButton_Click(object sender, EventArgs e)
    {
        if (this.validateAliasData())
        {
            try
            {
                if (alias == null)
                {
                    alias = new AliasData();
                    alias.Type = EkEnumeration.AliasRuleType.Manual;
                    alias.TargetType = EkEnumeration.TargetType.Content;
                    alias.SiteId = long.Parse(uxSiteList.SelectedValue);
                    alias.LanguageId = int.Parse(uxLanguageList.SelectedValue.ToString());
                    if (alias.LanguageId < 0) alias.LanguageId = 0;
                }
                var newalias = uxAliasName.Text + uxExtensionDropDownList.SelectedValue;
                alias.Alias = newalias;
                alias.IsEnabled = uxActiveCheckBox.Checked;
                alias.IsDefault = uxIsDefaultCheckBox.Checked;

                alias.QueryStringAction = (EkEnumeration.QueryStringActionType)Enum.Parse(typeof(EkEnumeration.QueryStringActionType), uxQueryStringAction.SelectedValue);
                alias.QueryString = uxAdditionalVariables.Text;
                alias.TargetId = long.Parse(frm_content_id.Value);
                if (uxQuicklinkSelector.SelectedItem == null)
                {
                    populateQuicklinkSelector(alias.TargetId, alias.LanguageId, alias.TargetURL);
                }

                alias.TargetURL = Request["frm_qlink"];
                alias.NodeId = long.Parse(Request["frm_content_id"]);
                    if (alias.TargetURL.IndexOf("downloadAsset.aspx", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        alias.TargetURL = alias.TargetURL.Substring(alias.TargetURL.IndexOf("downloadAsset.aspx", StringComparison.OrdinalIgnoreCase));
                    }

                alias = (alias.Id != 0) ? manager.Update(alias) : manager.Add(alias);
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                //Utilities.ShowError(exc.Message);
                isvalid = false;
                uxMessage.Visible = true;
                uxMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxMessage.Text = exc.Message;
                return;
            }

            if (this.displayMode == DisplayMode.EditContent || this.displayMode == DisplayMode.AddContent)
            {
                Ektron.Cms.API.JS.RegisterJSBlock(this, "window.parent.Ektron.Workarea.Content.UrlAliasing.modelComplete();", "closeAliasDialog");
            }
            else
            {
                Response.Redirect("AddEditViewAlias.aspx?" + querystringKey + "=" + alias.Id.ToString());
            }
        }
        else
        {
            this.IsEditable = true;
            this.displayMode = DisplayMode.Edit;
            this.setUI();
        }
    }

    private bool validateAliasData()
    {
        isvalid = true;
        Page.Validate("saveAlias");
        isvalid = this.Page.IsValid;

        if (string.IsNullOrWhiteSpace(uxAliasName.Text))
        {
            isvalid = false;
            this.showMessage = true;
            uxMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
            uxMessage.Text = GetLocalResourceObject("ErrorRequireduxAliasName").ToString();
        }

        if (string.IsNullOrWhiteSpace(frm_content_id.Value) || frm_content_id.Value == "0")
        {
            isvalid = false;
            this.showMessage = true;
            uxMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
            uxMessage.Text = GetLocalResourceObject("ErrorContentRequired").ToString();
        }

        return isvalid;
    }

    protected void uxEditButton_Click(object sender, EventArgs e)
    {
        this.IsEditable = true;
        this.displayMode = DisplayMode.Edit;
        this.setUI();
    }

    protected void uxDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (alias.IsDefault)
            {
                this.showMessage = true;
                uxMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxMessage.Text = "This is a primary alias and cannot be deleted.  Please choose an alternate primary alias before deleting this alias.";
                this.IsEditable = false;
                return;
            }
            else
            {
                manager.Delete(alias.Id);
            }
        }
        catch (Exception exc)
        {
            //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
            //Utilities.ShowError(exc.Message);
            uxMessage.Visible = true;
            uxMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
            uxMessage.Text = exc.Message;
            return;
        }
        if (this.displayMode == DisplayMode.EditContent)
        {
            Ektron.Cms.API.JS.RegisterJSBlock(this, "window.parent.Ektron.Workarea.Content.UrlAliasing.modelComplete();", "closeAliasDialog");
        }
        else
        {
            if (!this.showMessage)
                Response.Redirect("aliases.aspx", true);
        }
    }

    #endregion

    #region internal methods

    private void initWorkareaUI()
    {
        this.RegisterResources();

        EkMessageHelper msgHelper = refContentApi.EkMsgRef;
        StyleHelper styleHelper = new StyleHelper();
        uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
        this.aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_aliases_detail", string.Empty);
    }

    private void bindUIOptions()
    {
        bindLanguageList(siteAPI.ContentLanguage);
        site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
        bindSiteList();
        bindTypeList();
        bindExtensions();
        bindQueryStringAction();
    }

    private void bindQueryStringAction()
    {
        uxQueryStringAction.Items.Add(new ListItem(getLocalQueryName(EkEnumeration.QueryStringActionType.None), EkEnumeration.QueryStringActionType.None.GetHashCode().ToString()));
        uxQueryStringAction.Items.Add(new ListItem(getLocalQueryName(EkEnumeration.QueryStringActionType.Append), EkEnumeration.QueryStringActionType.Append.GetHashCode().ToString()));
        uxQueryStringAction.Items.Add(new ListItem(getLocalQueryName(EkEnumeration.QueryStringActionType.Replace), EkEnumeration.QueryStringActionType.Replace.GetHashCode().ToString()));
        uxQueryStringAction.Items.Add(new ListItem(getLocalQueryName(EkEnumeration.QueryStringActionType.Resolve), EkEnumeration.QueryStringActionType.Resolve.GetHashCode().ToString()));
        uxQueryStringAction.DataBind();
    }

    private void bindExtensions()
    {
        AliasSettingsManager settingsManager = new AliasSettingsManager();
        List<FileExtension> extensions = settingsManager.GetAllExtensions();
        uxExtensionDropDownList.DataSource = extensions;
        uxExtensionDropDownList.DataValueField = "Extension";
        uxExtensionDropDownList.DataTextField = "Extension";
        uxExtensionDropDownList.DataBind();
    }

    private void bindTypeList()
    {
        uxSourceTypeDropDownList.Items.Add(new ListItem("Select", "Select"));
        uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Taxonomy), EkEnumeration.AliasRuleType.Taxonomy.GetHashCode().ToString()));
        uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Folder), EkEnumeration.AliasRuleType.Folder.GetHashCode().ToString()));
        uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Manual), EkEnumeration.AliasRuleType.Manual.GetHashCode().ToString()));
        uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.User), EkEnumeration.AliasRuleType.User.GetHashCode().ToString()));
        uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Group), EkEnumeration.AliasRuleType.Group.GetHashCode().ToString()));
        uxSourceTypeDropDownList.DataBind();
    }

    private void setUI()
    {
        if (!Page.IsPostBack)
        {
            this.bindUIOptions();
        }

        // View Mode
        if (this.displayMode == DisplayMode.View)
        {
            if (this.HasEditPermission())
            {
                uxEditButton.Visible = true;
                uxDelete.Visible = true;
            }
            uxSaveButton.Visible = false;
            uxSourceTypeDropDownList.Enabled = false;
            uxSiteList.Enabled = false;
            uxBackButton.Visible = true;

            if (alias.Type != EkEnumeration.AliasRuleType.Manual)
            {
                uxEditButton.Visible = false;
                uxSaveButton.Visible = false;
                uxDelete.Visible = false;
                uxSiteRow.Visible = false;
                uxTypeRow.Visible = false;
                uxRuleRow.Visible = false;
            }
            this.bindData(alias);
        }

        // Edit mode from content tab
        if (this.displayMode == DisplayMode.EditContent && HasEditPermission())
        {
            uxContentBlockContainer.Visible = false;
            uxContentSelectorDialog.Visible = false;
            uxBackButton.Visible = false;
            uxEditButton.Visible = false;
            uxSiteList.Visible = false;
            if (alias.Type == EkEnumeration.AliasRuleType.Manual)
            {
                uxSaveButton.Visible = true;
                uxDelete.Visible = true;
                this.IsEditable = true;
                uxSiteRow.Visible = false;
                uxTypeRow.Visible = false;
                uxRuleRow.Visible = false;
            }
            else
            {
                uxSaveButton.Visible = false;
                uxDelete.Visible = false;
                this.IsEditable = false;
            }
            this.bindData(alias);
        }

        // Edit mode
        if (this.displayMode == DisplayMode.Edit && HasEditPermission())
        {
            this.bindData(alias);
            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxDelete.Visible = false;
        }

        // Add from content tab
        if (this.displayMode == DisplayMode.AddContent && HasEditPermission())
        {
            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxDelete.Visible = false;
            uxSourceTypeDropDownList.SelectedValue = EkEnumeration.AliasRuleType.Manual.GetHashCode().ToString();
            uxSourceTypeDropDownList.Enabled = false;
            uxSiteList.Visible = false; // need default select
            uxLanguageList.Enabled = false; // need defauilt select
            uxBackButton.Visible = false;
            uxContentBlockContainer.Visible = false;
            uxSiteRow.Visible = false;
            uxTypeRow.Visible = false;
            uxRuleRow.Visible = false;
            contentID = long.Parse(Request.QueryString["cid"].ToString());
            contentLangID = int.Parse(Request.QueryString[querystringlangkey].ToString());
            LanguageID = uxLanguageList.SelectedValue = contentLangID.ToString();
        }

        // Add Mode
        if (this.displayMode == DisplayMode.Add && HasEditPermission())
        {
            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxDelete.Visible = false;
            uxSourceTypeDropDownList.SelectedValue = EkEnumeration.AliasRuleType.Manual.GetHashCode().ToString();
            uxSourceTypeDropDownList.Enabled = false;
            uxSiteList.Enabled = true;
            uxLanguageList.Enabled = true;
            uxBackButton.Visible = true;
            uxSiteRow.Visible = false;
            uxTypeRow.Visible = false;
            uxRuleRow.Visible = false;
            long contentid = 0;
            if (!string.IsNullOrEmpty(frm_content_id.Value) && long.TryParse(frm_content_id.Value, out contentid) && contentid > 0)
            {
                populateQuicklinkSelector(contentid, int.Parse(uxLanguageList.SelectedValue), "");
            }
        }
    }

    private void setEditUI()
    {
        uxEditButton.Visible = false;
        uxSaveButton.Visible = true;
        uxDelete.Visible = false;
        uxBackButton.Visible = true;
    }

    private string getLocalQueryName(EkEnumeration.QueryStringActionType queryAction)
    {
        string returnVal;
        try
        {
            returnVal = GetLocalResourceObject("queryType" + queryAction.GetHashCode().ToString()) as string;
        }
        catch
        {
            returnVal = queryAction.ToDescriptionString();
        }
        return returnVal;
    }


    private string getLocalTypeName(EkEnumeration.AliasRuleType configType)
    {
        string returnVal;
        try
        {
            returnVal = GetLocalResourceObject("configType" + configType.GetHashCode().ToString()) as string;
        }
        catch
        {
            returnVal = configType.ToDescriptionString();
        }
        return returnVal;
    }

    private void bindLanguageList(long defaultlanguage)
    {
        LanguageData[] language_data = this.siteAPI.GetAllActiveLanguages();

        var languages = from LanguageData in language_data
                        select new
                        {
                            Name = LanguageData.Name,
                            value = LanguageData.Id
                        };

        uxLanguageList.Items.Clear();
        uxLanguageList.DataSource = languages;
        uxLanguageList.DataTextField = "Name";
        uxLanguageList.DataValueField = "value";
        if (!string.IsNullOrEmpty(defaultlanguage.ToString()) && defaultlanguage.ToString() != "-1")
        {
            LanguageID = uxLanguageList.SelectedValue = defaultlanguage.ToString();
        }
        else
        {
            LanguageID = uxLanguageList.SelectedValue = this.siteAPI.DefaultContentLanguage.ToString();
        }
    }

    private void bindSiteList()
    {
        Dictionary<long, string> siteList = site.GetSiteList();
        var displaySites = from sites in siteList
                           select new { Label = sites.Value, ID = sites.Key };
        uxSiteList.DataSource = displaySites;
        uxSiteList.DataTextField = "Label";
        uxSiteList.DataValueField = "ID";
        uxSiteList.DataBind();
    }

    private void populateQuicklinkSelector(long TargetId, int LanguageId, string selectedTargetURL)
    {
        selectedTargetURL = selectedTargetURL.TrimStart('/');
        uxQuicklinkSelector.Items.Clear();
        contentManager.ContentLanguage = LanguageId;
        ContentData c = contentManager.GetItem(TargetId);
        string selectedlibId = "0";
        if (!string.IsNullOrEmpty(Request["uxQuicklinkSelector"]))
        {
            selectedlibId = Request["uxQuicklinkSelector"].ToString();
        }
        if (c != null)
        {
            uxContentSelector.Text = c.Title;
        }
        var alias_templates = refContentApi.EkLibraryRef.GetQuicklinksForContent(TargetId, LanguageId);
        foreach (var item in alias_templates)
        {
            item.FileName = item.FileName.TrimStart('/');
            if (long.Parse(selectedlibId) > 0)
            {
                var li = new ListItem(item.FileName, item.Id.ToString());
                if (selectedlibId == item.Id.ToString()) { li.Selected = true; }
                uxQuicklinkSelector.Items.Add(li);
            }
            else
            {
                if (item.FileName.ToLower().IndexOf("downloadasset.aspx?") != -1)
                {
                    var li = new ListItem(refContentApi.RequestInformationRef.WorkAreaDir.ToLower() + item.FileName.ToLower(), item.Id.ToString());
                    if (selectedTargetURL.ToLower() == refContentApi.RequestInformationRef.WorkAreaDir.ToLower() + item.FileName.ToLower()) li.Selected = true;
                    uxQuicklinkSelector.Items.Add(li);
                }
                else
                {
                    var li = new ListItem(item.FileName, item.Id.ToString());
                    if (selectedTargetURL == item.FileName) { li.Selected = true; }
                    uxQuicklinkSelector.Items.Add(li);
                }
            }
        }
        if (!string.IsNullOrEmpty(selectedTargetURL) && !FindByText(uxQuicklinkSelector, selectedTargetURL.ToLower()))
        {
            var li = new ListItem(selectedTargetURL);
            li.Selected = true;
            uxQuicklinkSelector.Items.Add(li);
        }
    }

    private bool FindByText(DropDownList lstBox, string value)
    {
        bool found = false;
        foreach (ListItem li in lstBox.Items)
        {
            if (li.Text.ToLower() == value.ToLower())
            {
                found = true;
                break;
            }
        }
        return found;
    }
    private void bindData(AliasData alias)
    {
        uxActiveCheckBox.Checked = alias.IsEnabled;
        uxIsDefaultCheckBox.Checked = alias.IsDefault;
        uxSiteList.SelectedValue = alias.SiteId.ToString();
        if (alias.LanguageId > 0)
        {
            LanguageID = uxLanguageList.SelectedValue = alias.LanguageId.ToString();
        }

        uxSourceTypeDropDownList.SelectedValue = alias.Type.GetHashCode().ToString();
        uxSitePrefix.Text = siteAPI.SitePath;
        string aliasname = "";
        string extension = "";
        this.splitAliasName(alias.Alias, out aliasname, out extension);
        uxAliasName.Text = aliasname;
        if (uxExtensionDropDownList.Items.Count > 0)
        {
            var ext = uxExtensionDropDownList.Items.FindByValue(extension);
            if (ext != null)
                uxExtensionDropDownList.SelectedValue = extension;
        }

        uxQueryStringAction.SelectedValue = alias.QueryStringAction.GetHashCode().ToString();
        uxAdditionalVariables.Text = alias.QueryString;
        //uxContentSelector.Text = alias.TargetId.ToString();
        populateQuicklinkSelector(alias.TargetId, alias.LanguageId, alias.TargetURL);
        contentID = alias.TargetId;
        contentLangID = alias.LanguageId;
        quicklink = alias.TargetURL;
        if (rule != null)
        {
            uxRuleTitle.Text = this.rule.Name;
        }
    }

    private void splitAliasName(string fullAlias, out string aliasName, out string aliasExtension)
    {
        aliasName = fullAlias;
        aliasExtension = "";
        if (aliasName.EndsWith("/"))
        {
            aliasName = aliasName.TrimEnd("/".ToCharArray());
            aliasExtension = "/";
        }
        else if (aliasName.Contains("."))
        {
            aliasName = aliasName.Substring(0, aliasName.LastIndexOf('.'));
            aliasExtension = fullAlias.Substring(fullAlias.LastIndexOf('.'));
        }
    }

    private bool HasViewPermission()
    {
        return
            (
                !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                ContentAPI.Current.RequestInformationRef.UserId != 0
            );
    }

    private bool HasEditPermission()
    {
        return
        (
            (
                !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                ContentAPI.Current.RequestInformationRef.UserId != 0
            ) &&
            (
                ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0) ||
                ContentAPI.Current.IsARoleMember((long)EkEnumeration.CmsRoleIds.UrlAliasingAdmin, ContentAPI.Current.UserId, false) ||
                (ContentAPI.Current.IsARoleMember((long)EkEnumeration.CmsRoleIds.EditAlias, ContentAPI.Current.UserId, false) &&
                        (this.displayMode == DisplayMode.AddContent ||
                           this.displayMode == DisplayMode.EditContent
                        )
                )
            )
        );
    }

    private DisplayMode getDisplayMode()
    {
        DisplayMode mode = DisplayMode.None;

        if (Request.QueryString["mode"] != null)
        {
            if (Request.QueryString["mode"].ToString() == "edit") { mode = DisplayMode.EditContent; }
            if (Request.QueryString["mode"].ToString() == "add") { mode = DisplayMode.AddContent; }
        }
        else
        {
            if (alias != null && !Page.IsPostBack)
            {
                mode = DisplayMode.View;
                returnpath = siteAPI.RequestInformationRef.ApplicationPath + "UrlAliasing/aliases.aspx";
            }

            if (alias != null && Page.IsPostBack)
            {
                mode = DisplayMode.Edit;
                returnpath = siteAPI.RequestInformationRef.ApplicationPath + "UrlAliasing/AddEditViewAlias.aspx?" + querystringKey + "=" + Request.QueryString[querystringKey];
                //go back to viewing the alias
            }

            if (alias == null && mode != DisplayMode.AddContent)
            {
                mode = DisplayMode.Add;
                returnpath = siteAPI.RequestInformationRef.ApplicationPath + "UrlAliasing/aliases.aspx";
            }
        }
        return mode;
    }

    #endregion

    #region JavaScript/CSS

    /// <summary>
    /// Register Javascripts and CSS packages
    /// </summary>
    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        Package resources = new Package()
        {
            Components = new List<Component>()
                {
                    Packages.EktronCoreJS,
                    Packages.Ektron.Namespace,
                    Packages.Ektron.Workarea.Core, 
                    Packages.Ektron.JSON,
                    JavaScript.Create(cmsContextService.WorkareaPath + "/UrlAliasing/js/ektron.workarea.urlaliasing.js")
                }
        };
        resources.Register(this);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);

    }


    #endregion

    public enum DisplayMode
    {
        None = 0,
        View = 1,
        Edit = 2,
        Add = 3,
        EditContent = 4,
        AddContent = 5
    }
}
