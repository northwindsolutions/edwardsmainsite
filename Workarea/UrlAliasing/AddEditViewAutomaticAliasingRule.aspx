﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditViewAutomaticAliasingRule.aspx.cs"
    Inherits="Ektron.Workarea.UrlAliasing.AddAutomaticAliasingRule" 
    meta:resourcekey="PageResource1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
        function NOTagValidation(Fieldval, FieldName) {
            var re = /<(.|\n)*?>/g;
            if (re.test(Fieldval)) {
                return false;
            }
            else
                return true;

        }
    </script>
</head>
<body>
    <form id="frm_autoalias" runat="server">
    <ektronUI:JavaScript ID="aliasJS" runat="server" Path="js/ektron.workarea.urlaliasing.js" />
    <ektronUI:JavaScriptBlock ID="autoRuleInitJS" ExecutionMode="OnEktronReady" runat="server">
        <ScriptTemplate>
            Ektron.Workarea.UrlAliasing.initAutoConfig();
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    <ektronUI:JavaScriptBlock ID="addAutoInitJS" ExecutionMode="OnEktronReady" runat="server">
        <ScriptTemplate>
            Ektron.Workarea.UrlAliasing.initAddAuto();
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    <ektronUI:Css ID="aliasCSS" Path="CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextIdAdd"
                        Visible="<%# addTitleVisible %>" />
                    <asp:Literal ID="aspHeaderTextId2" runat="server" meta:resourcekey="aspHeaderTextIdEdit"
                        Visible="<%# editTitleVisible %>" />
                    <asp:Literal ID="aspHeaderTextId3" runat="server" meta:resourcekey="aspHeaderTextIdView"
                        Visible="<%# viewTitleVisible %>" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSavetxt_labelBase"
                                class="primary saveButton" ValidationGroup="ValidateData" OnClick="uxSaveButton_Click" />
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" meta:resourcekey="uxEditButton"
                                OnClick="uxEditButton_Click" />
                        </td>
                        <td class="column-PrimaryButton">
                            <asp:HyperLink ID="uxViewAssociatesButton" runat="server" CssClass="primary editButton"
                                Visible="false" meta:resourcekey="uxViewAssociatesButton"></asp:HyperLink>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="uxDelete" class="primary deleteButton" meta:resourcekey="uxDelete"
                                OnClick="uxDelete_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <ektronUI:Message ID="uxMessage" runat="server" Visible="<%# this.showMessage %>">
        </ektronUI:Message>
        <table class="ektronForm">
            <tr>
                <td class="label">
                    <asp:Label ID="uxNameLabel" runat="server" meta:resourcekey="uxNameLabel" />
                </td>
                <td class="value">
                    <ektronUI:TextField ID="uxNameTextBox" runat="server" meta:resourcekey="uxNameTextBox"
                        ValidationGroup="ValidateData" Enabled='<%#this.IsEditable %>'>
                    <ValidationRules>
                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources:AlertNoName %>" />
                        <ektronUI:CustomRule JavascriptFunctionName="NOTagValidation" ErrorMessage="<%$ Resources:NoTagValidationText %>" />
                    </ValidationRules>
                    </ektronUI:TextField>
                    <ektronUI:ValidationMessage AssociatedControlID="uxNameTextBox" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="uxActiveLabel" runat="server" meta:resourcekey="uxActiveLabel" />
                </td>
                <td class="value">
                    <asp:CheckBox ID="uxActiveCheckBox" runat="server" meta:resourcekey="uxActiveCheckBox"
                        Checked="true" Enabled='<%#this.IsEditable %>' />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxSiteList" runat="server" meta:resourcekey="uxSiteList">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="uxSourceTypeLabel" runat="server" meta:resourcekey="uxSourceTypeLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxSourceTypeDropDownList" runat="server" meta:resourcekey="uxSourceTypeDropDownList" />
                </td>
            </tr>
            <tr id="langRow" runat="server" visible="false">
                <td class="label">
                    <asp:Label ID="uxLanguageLabel" runat="server" meta:resourcekey="uxLanguageLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxLanguageList" runat="server" meta:resourcekey="uxLanguageList"
                        AppendDataBoundItems="true" Enabled="false">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="priorityRow" runat="server" visible="true">
                <td class="label">
                    <asp:Label ID="uxPriorityFieldLabel" AssociatedControlID="uxPriorityField" runat="server" meta:resourcekey="uxPriorityLabel"></asp:Label>
                </td>
                <td class="value">
                    <ektronUI:IntegerField ID="uxPriorityField" MinValue="1" MaxValue="2147483647" ErrorMessage="Please enter a valid positive integer (1 - 2147483647)"
                        runat="server"></ektronUI:IntegerField>
                    <ektronUI:ValidationMessage ID="uxValidationMessageForPriority" AssociatedControlID="uxPriorityField"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="uxNextButton" runat="server" meta:resourcekey="uxNextButton" OnClick="uxNextButton_Click" />
                </td>
            </tr>
        </table>
        <div class="ektronTopSpace">
        </div>
        <div class="ektronTopSpace">
        </div>
        <asp:Panel ID="uxDefineFolderRulePanel" runat="server" Visible="false">
            <fieldset id="DefineURL">
                <legend><strong title="Define URL">
                    <asp:Literal ID="uxDefineURLLiteral" runat="server" meta:resourcekey="uxDefineURLLiteral" /></strong></legend>
                <div>
                    <div class="ektronPageContainer">
                        <table class="ektronForm">
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxAliasRootLabel" runat="server" meta:resourcekey="uxAliasRootLabel" />
                                </td>
                                <td class="validate">
                                    <%=sitePath%>
                                    <ektronUI:TextField ID="uxFolderPath" runat="server" ValidationGroup="ValidateData"
                                        Enabled='<%#this.IsEditable %>'>
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources:AlertNoAliasRoot %>" />
                                    </ValidationRules>
                                    </ektronUI:TextField>
                                    <asp:LinkButton ID="uxFolderSelect" runat="server" OnClientClick="Ektron.Workarea.UrlAliasing.openFolderDialog(); return false;"
                                        Visible='<%#this.IsEditable %>' CssClass="button greenHover buttonCheckAll viewFolderList"
                                        meta:resourcekey="uxFolderSelect"></asp:LinkButton>
                                    <ektronUI:ValidationMessage ID="ValidationMessage1" AssociatedControlID="uxFolderPath"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxExcludeFromPathLabel" runat="server" meta:resourcekey="uxExcludeFromPathLabel" />
                                </td>
                                <td class="value">
                                    <select id="uxExcludeFromPathList" name="uxExcludeFromPathList" <%#disabledString %>>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxAliasFormatLabel" runat="server" meta:resourcekey="uxAliasFormatLabel" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="uxAliasFormatDropDownList" Enabled='<%#this.IsEditable %>'
                                        runat="server" meta:resourcekey="uxAliasFormatDropDownList" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxExtensionLabel" runat="server" meta:resourcekey="uxExtensionLabel" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="uxExtensionDropDownList" meta:resourcekey="uxExtensionDropDownList"
                                        Enabled='<%#this.IsEditable %>' runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxQueryStringParamLabel" runat="server" meta:resourcekey="uxQueryStringParamLabel" />
                                </td>
                                <td ontextchanged="uxQueryStringParam_TextChanged">
                                    <asp:TextBox ID="uxQueryStringParam" runat="server" Enabled='<%#this.IsEditable %>' />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="uxDefineTaxonomyRulePanel" runat="server" Visible="false">
            <fieldset id="Fieldset2">
                <legend><strong title="Define URL">
                    <asp:Literal ID="uxDefineTaxURLLiteral" runat="server" meta:resourcekey="uxDefineURLLiteral" /></strong></legend>
                <div>
                    <div class="ektronPageContainer">
                        <table class="ektronForm">
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxAliasTaxRootLabel" runat="server" meta:resourcekey="uxAliasRootLabel" />
                                </td>
                                <td class="validate">
                                    <%=sitePath%>
                                    <ektronUI:TextField ID="uxTaxonomyPath" runat="server" ValidationGroup="ValidateData"
                                        CssClass="uxTaxonomyPath" Enabled='<%#this.IsEditable %>'>
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources:AlertNoAliasRoot %>" />
                                    </ValidationRules>
                                    </ektronUI:TextField>
                                    <asp:LinkButton ID="uxTaxonomySelect" runat="server" CssClass="button greenHover buttonCheckAll viewTaxonomyList"
                                        meta:resourcekey="uxFolderSelect" Visible='<%#this.IsEditable %>' OnClientClick="Ektron.Workarea.UrlAliasing.openTaxonomyDialog(); return false;"></asp:LinkButton>
                                    <ektronUI:ValidationMessage ID="ValidationMessage2" AssociatedControlID="uxFolderPath"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxExcludeFromTaxPathLabel" runat="server" meta:resourcekey="uxExcludeFromPathLabel" />
                                </td>
                                <td class="value">
                                    <select id="uxExcludeFromTaxPathList" name="uxExcludeFromTaxPathList" meta:resourcekey="uxPathList"
                                        disabled='<%#!this.IsEditable %>' runat="server">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxAliasTaxFormatLabel" runat="server" meta:resourcekey="uxAliasFormatLabel" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="uxAliasTaxFormatDropDownList" Enabled='<%#this.IsEditable %>'
                                        runat="server" meta:resourcekey="uxAliasFormatDropDownList" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxTaxExtensionLabel" runat="server" meta:resourcekey="uxExtensionLabel" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="uxTaxExtensionDropDownList" meta:resourcekey="uxExtensionDropDownList"
                                        Enabled='<%#this.IsEditable %>' runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxAliasTemplateSourceLabel" runat="server" meta:resourcekey="uxAliasTemplateSourceLabel" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="uxAliasTemplateSource" meta:resourcekey="uxAliasTemplateSource"
                                        Enabled='<%#this.IsEditable %>' runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxTaxQueryStringParamLabel" runat="server" meta:resourcekey="uxQueryStringParamLabel" />
                                </td>
                                <td ontextchanged="uxQueryStringParam_TextChanged">
                                    <asp:TextBox ID="uxTaxQueryStringParam" runat="server" Enabled='<%#this.IsEditable %>' />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="uxDefineUserRulePanel" runat="server" Visible="false">
            <fieldset id="Fieldset1">
                <legend><strong title="Define URL">
                    <asp:Literal ID="uxDefineUserURLLabel" runat="server" meta:resourcekey="uxDefineURLLiteral" /></strong></legend>
                <div>
                    <table class="ektronForm">
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxUserAliasPathLabel" runat="server" meta:resourcekey="uxUserAliasPathLabel" />
                            </td>
                            <td class="validate">
                                <%=sitePath%><ektronUI:TextField ID="uxUserAliasPath" runat="server" ValidationGroup="ValidateData"
                                    CssClass="ektronTextMedium uxUserAliasPath" Enabled='<%#this.IsEditable %>'>
                                    <ValidationRules>
                                        <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.UrlAliasing.UserFolderValidation" ErrorMessage="<%$ Resources:AlertInvalidCharacters %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <ektronUI:ValidationMessage ID="uxUserAliasPathValidationMessage" AssociatedControlID="uxUserAliasPath"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxUserExtensionLabel" runat="server" meta:resourcekey="uxExtensionLabel" />
                            </td>
                            <td>
                                <asp:DropDownList ID="uxExtensionsForUserDropDownList" meta:resourcekey="uxExtensionDropDownList"
                                    Enabled='<%#this.IsEditable %>' runat="server" />
                            </td>
                        </tr>
                       <%-- <tr>
                            <td class="label">
                                <asp:Label ID="uxQueryStringParamForUserLabel" runat="server" meta:resourcekey="uxQueryStringParamLabel" />
                            </td>
                            <td>
                                <asp:TextBox ID="uxQueryStringParamForUser" runat="server" Enabled='<%#this.IsEditable %>' />
                            </td>
                        </tr>--%>
                    </table>
                </div>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="uxDefineGroupRulePanel" runat="server" Visible="false">
            <fieldset id="Fieldset3">
                <legend><strong title="Define URL">
                    <asp:Literal ID="uxDefineGroupURLLabel" runat="server" meta:resourcekey="uxDefineURLLiteral" /></strong></legend>
                <div>
                    <table class="ektronForm">
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxGroupAliasPathLabel" runat="server" meta:resourcekey="uxGroupAliasPathLabel" />
                            </td>
                            <td class="validate">
                                <%=sitePath%><ektronUI:TextField ID="uxGroupAliasPath" runat="server" ValidationGroup="ValidateData"
                                    CssClass="ektronTextMedium uxGroupAliasPath" Enabled='<%#this.IsEditable %>'>
                                    <ValidationRules>
                                        <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.UrlAliasing.GroupFolderValidation" ErrorMessage="<%$ Resources:AlertInvalidCharacters %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <ektronUI:ValidationMessage ID="ValidationMessage3" AssociatedControlID="uxGroupAliasPath"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxGroupExtensionLabel" runat="server" meta:resourcekey="uxExtensionLabel" />
                            </td>
                            <td>
                                <asp:DropDownList ID="uxExtensionsForGroupDropDownList" meta:resourcekey="uxExtensionsForGroupDropDownList"
                                    Enabled='<%#this.IsEditable %>' runat="server" />
                            </td>
                        </tr>
                       <%-- <tr>
                            <td class="label">
                                <asp:Label ID="uxQueryStringParamForGroupLabel" runat="server" meta:resourcekey="uxQueryStringParamLabel" />
                            </td>
                            <td>
                                <asp:TextBox ID="uxQueryStringParamForGroup" runat="server" Enabled='<%#this.IsEditable %>' />
                            </td>
                        </tr>--%>
                    </table>
                </div>
            </fieldset>
        </asp:Panel>
        <div class="ektronTopSpace">
        </div>
        <asp:Panel ID="uxPreviewPanel" runat="server" Visible="false">
            <fieldset id="PreviewAlias">
                <legend><strong title="Preview Alias">
                    <asp:Literal ID="uxPreviewAliasLiteral" runat="server" meta:resourcekey="uxPreviewAliasLiteral" /></strong></legend>
                <div class="ektronPageContainer">
                    <asp:Panel ID="PreviewAliasPanel" runat="server" meta:resourcekey="Panel1Resource1">
                        <table class="ektronForm">
                            <tr>
                                <td class="label">
                                    <asp:Label ID="uxExamplePreviewLabel" runat="server" meta:resourcekey="uxExamplePreviewLabel" />
                                </td>
                                <td class="value">
                                    <asp:Label ID="uxExamplePreviewForUser" runat="server" Text='<%#this.sitePath%>&nbsp;' />
                                    <asp:Label ID="uxExamplePreview" runat="server" ReadOnly="true" Enabled='false'></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </fieldset>
        </asp:Panel>
        <div class="ektronTopSpace">
        </div>
        <asp:Panel ID="uxReplacementCharactersPanel" runat="server" Visible="false">
            <fieldset id="replacementCharForm">
                <legend>
                    <asp:Literal ID="uxReplacementCharLabel" runat="server" meta:resourcekey="uxReplacementCharLabel"></asp:Literal></legend>
                <asp:ListView ID="uxReplacementCharListView" runat="server" InsertItemPosition="None">
                    <LayoutTemplate>
                        <div class="replacementlistview1">
                            <table id="uxTableHead" class="ektronGrid urlAliasSettings">
                                <tr id="trHeader">
                                    <th class="column-type urlTypesLabel" id="uxHeadCharacter">
                                        <asp:Label ID="uxHeaderCharacterLabel" runat="server" meta:resourcekey="uxHeaderCharacterLabel"></asp:Label>
                                    </th>
                                    <th class="column-type urlTypesLabel" id="uxHeadReplacement">
                                        <asp:Label ID="uxHeaderReplacementLabel" runat="server" meta:resourcekey="uxHeaderReplacementLabel"></asp:Label>
                                    </th>
                                </tr>
                                <tr runat="server" id="itemPlaceholder">
                                </tr>
                            </table>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="urlTypesLabel">
                                <%#Eval("MatchCharacter")%>
                            </td>
                            <td>
                                <%#Eval("ReplaceCharacter")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <asp:ListView ID="uxReplacementCharListEdit" runat="server" InsertItemPosition="None">
                    <LayoutTemplate>
                        <div class="replacementlistview1 replacementlistedit">
                            <table id="uxTableHead" class="ektronGrid urlAliasSettings">
                                <tr class="title-header" id="trHeader">
                                    <th class="column-type urlTypesLabel" id="uxHeadCharacter">
                                        <asp:Label ID="uxHeaderCharacterLabel" runat="server" meta:resourcekey="uxHeaderCharacterLabel"></asp:Label>
                                    </th>
                                    <th class="column-type urlTypesLabel" id="uxHeadReplacement">
                                        <asp:Label ID="uxHeaderReplacementLabel" runat="server" meta:resourcekey="uxHeaderReplacementLabel"></asp:Label>
                                    </th>
                                    <th class="column-type urlTypesLabel">
                                        <%-- <asp:Label ID="uxDeleteLabel" runat="server" meta:resourcekey="uxDeleteLabel"></asp:Label>--%>
                                    </th>
                                </tr>
                                <tr runat="server" id="itemPlaceholder">
                                </tr>
                            </table>
                            <div class="replacementbutton">
                                <a id="uxAddReplacementButton" runat="server" class="uxAddNewCharacter button greenHover buttonCheckAll replacementbutton"
                                    onclick="return false" meta:resourcekey="uxAddReplacementButton" href="#">
                                    <asp:Label ID="uxaddreplacelink" runat="server" meta:resourcekey="uxaddreplacelink"></asp:Label></a><%--<img src="../Images/ui/icons/add.png" alt="add" class="uxAddNewCharacter" />--%>
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="urlTypesLabel">
                                <input type="hidden" id="uxHiddenCharacterID" value='<%# Eval("Id")%>' runat="server"
                                    class="uxHiddenCharacterID" />
                                <input type="hidden" id="uxHiddenConfigID" value='<%# Eval("ConfigId")%>' runat="server"
                                    class="uxHiddenConfigID" />
                                <input type="hidden" id="uxHiddenIsDefault" value='<%# Eval("IsDefault")%>' runat="server"
                                    class="uxHiddenIsDefault" />
                                <asp:TextBox ID="uxMatchCharacter" runat="server" Text='<%# Eval("MatchCharacter")%>'
                                    Width="20px" MaxLength="1" Enabled='<%# !(bool)Eval("IsDefault") %>' CssClass="uxMatchCharacter"></asp:TextBox>
                            </td>
                            <td class="urlTypesLabel">
                                <asp:TextBox ID="uxReplacementCharacter" runat="server" Text='<%# Eval("ReplaceCharacter")%>'
                                    Width="20px" CssClass="uxReplacementCharacter"></asp:TextBox>
                            </td>
                            <td>
                                <div <%# (Boolean.Parse(Eval("IsDefault").ToString())) ? @" class=""uxDeleteCharacterDefault""" : @" class=""uxDeleteCharacter""" %>>
                                    <img src="../Images/ui/icons/delete.png" alt="delete" id="uxDeleteCharacterButton"
                                        runat="server" />
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </fieldset>
            <input type="hidden" id="uxCharacterMapHidden" runat="server" />
        </asp:Panel>
        <div class="ektronTopSpace">
        </div>
        <ektronUI:Dialog ID="uxSelectFolderDialog" runat="server" AutoOpen="false" meta:resourcekey="uxSelectFolderDialog"
            Modal="true" Width="500" Height="500">
            <ContentTemplate>
                <ektronUI:Iframe ID="uxFolderPathSelectIframe" runat="server" Height="450" Width="460"
                    CssClass="modalIframe folderIframe" Src="">
                </ektronUI:Iframe>
            </ContentTemplate>
        </ektronUI:Dialog>
        <ektronUI:Dialog ID="uxTaxonomyDialog" runat="server" AutoOpen="false" meta:resourcekey="uxTaxonomyDialog"
            Modal="true" Width="500" Height="500">
            <ContentTemplate>
                <ektronUI:Iframe ID="uxTaxonomyPathSelectIframe" CssClass="modalIframe" Height="450"
                    Width="460" runat="server" Src="">
                </ektronUI:Iframe>
            </ContentTemplate>
        </ektronUI:Dialog>
    </div>
    <asp:PlaceHolder runat="server">
        <input type="hidden" runat="server" name="frm_hdnSourceId" id="frm_hdnSourceId" />
        <input type="hidden" runat="server" name="uxPleaseSelect" id="uxPleaseSelect" meta:resourcekey="uxPleaseSelect" />
        <input type="hidden" runat="server" name="uxselectedExcludePath" id="uxselectedExcludePath" />
        <input type="hidden" runat="server" name="uxRuleId" id="uxRuleId" />
    </asp:PlaceHolder>
    </form>
</body>
</html>
