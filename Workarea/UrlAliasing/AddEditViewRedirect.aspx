﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditViewRedirect.aspx.cs"
    Inherits="Ektron.Workarea.UrlAliasing.AddEditViewRedirect" meta:resourcekey="PageResource1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
</head>
<body>
    <form id="frm_redirectalias" runat="server">
    <ektronUI:JavaScript ID="aliasJS" runat="server" Path="js/ektron.workarea.urlaliasing.js" />
    <ektronUI:Css ID="aliasCSS" Path="CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <div class="ektronPageHeader">
        <div class="ektronTitlebar" id="divTitleBar" runat="server">
            <span id="WorkareaTitleBar">
                <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextIdResource1" />
            </span><span id="_WorkareaTitleBar" style="display: none;"></span>
        </div>
        <div class="ektronToolbar" id="divToolBar" runat="server">
            <table>
                <tr>
                    <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                    <td class="column-PrimaryButton">
                        <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSavetxt_labelBase"
                            OnClick="uxSaveButton_Click" Visible='<%#this.IsEditable %>' class="primary saveButton"
                            OnClientClick="return Ektron.Workarea.UrlAliasing.RedirectURLValidation();" ValidationGroup="uxSaveValidateRequired" />
                        <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" Visible="false"
                            meta:resourcekey="uxEditButton" OnClick="uxEditButton_Click" />
                    </td>
                    <td>
                        <asp:LinkButton runat="server" ID="uxDelete" class="primary deleteButton" meta:resourcekey="uxDelete"
                            OnClick="uxDelete_Click" Visible="false" />
                    </td>
                    <td>
                        <div class="actionbarDivider">
                        </div>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ektronPageContainer ektronPageGrid">
        <ektronUI:Message ID="uxMessage" runat="server" DisplayMode="Error" Visible="false">
        </ektronUI:Message>
        <table class="ektronForm">
            <tr>
                <td class="label">
                    <asp:Label ID="uxActiveLabel" runat="server" meta:resourcekey="uxActiveLabel" />
                </td>
                <td class="value">
                    <asp:CheckBox ID="uxActiveCheckBox" runat="server" meta:resourcekey="uxActiveCheckBox"
                        Checked="True" Enabled='<%#this.IsEditable %>' />
                </td>
            </tr>
        </table>
        <div class="ektronTopSpace">
        </div>
        <div class="ektronTopSpace">
        </div>
        <fieldset id="DefineRedirect">
            <legend><strong title="Define Redirect">
                <asp:Literal ID="uxDefineURLLiteral" runat="server" meta:resourcekey="uxDefineURLLiteral" /></strong></legend>
            <div>
                <table class="ektronForm">
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                        </td>
                        <td class="value">
                            <asp:DropDownList ID="uxSiteDropDownList" runat="server" meta:resourcekey="uxSiteDropDownList" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxRedirectCodeLabel" runat="server" meta:resourcekey="uxRedirectCodeLabel" />
                        </td>
                        <td>
                            <asp:DropDownList ID="uxRedirectCodeDropDownList" runat="server" CssClass="uxRedirectCodeDropDownList"
                                meta:resourcekey="uxTypeListuxRedirectCodeDropDownList" Enabled='<%#this.IsEditable %>'>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxOriginalURLLabel" runat="server" meta:resourcekey="uxOriginalURLLabel" />
                            <%=sitePath%>
                        </td>
                        <td class="value validate">
                            <div style="height: auto; width: 460px; overflow-x: scroll; overflow-y: hidden">
                                <asp:TextBox ID="uxOriginalURL" runat="server" meta:resourcekey="uxOriginalURL" TextMode="SingleLine"
                                    CssClass="uxOriginalURL" MaxLength="250" Width="1850px" Enabled='<%#this.IsEditable %>' />
                            </div>
                        </td>
                    </tr>
                    <tr class="newurlclass">
                        <td class="label">
                            <asp:Label ID="uxNewURLLabel" runat="server" meta:resourcekey="uxNewURLLabel" CssClass="newurlclass" />
                        </td>
                        <td class="value validate">
                            <div style="height: auto; width: 460px; overflow-x: scroll; overflow-y: hidden">
                                <asp:TextBox ID="uxNewURL" runat="server" meta:resourcekey="uxNewURL" TextMode="SingleLine"
                                    MaxLength="250" Width="1850px" Enabled='<%#this.IsEditable %>' ValidationGroup="uxSaveValidateRequired"
                                    CssClass="newurlclass uxNewURL" />
                            </div>
                            <%--  <asp:RequiredFieldValidator ID="uxNewURLRequired" runat="server" ControlToValidate="uxNewURL"
                                meta:resourcekey="uxNewURLRequired" ValidationGroup="uxSaveValidateRequired"></asp:RequiredFieldValidator>--%>
                            <div id="uxnewurlerrormessage" style="display: none;">
                                <ektronUI:Message ID="uxnewurlUIerrormessage" runat="server" DisplayMode="Error">
                                </ektronUI:Message>
                            </div>
                            <div id="uxORGurlerrormessage" style="display: none;">
                                <ektronUI:Message ID="uxORGurlUIerrormessage" runat="server" DisplayMode="Error">
                                </ektronUI:Message>
                            </div>
                            <div id="optionalmessage" style="display: none;">
                                <ektronUI:Message ID="uxoptionalmessage" runat="server" DisplayMode="Information">
                                </ektronUI:Message>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <ektronUI:JavaScriptBlock ID="uxredirectInitJS" ExecutionMode="OnEktronReady" runat="server">
        <ScriptTemplate>
            Ektron.Workarea.UrlAliasing.initRedirect();
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    </form>
</body>
</html>
