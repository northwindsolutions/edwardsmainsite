﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using Ektron.Cms;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;
    using Ektron.Cms.Settings;
    using System.Web.UI.WebControls;
    using System.Net;
    using Ektron.Cms.Common;
    using System.Threading;

    public partial class AddEditViewRedirect : Ektron.Cms.Workarea.Page
    {
        #region member variables

        private SiteAPI siteAPI = new SiteAPI();
        private ISite site;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private const string querystringKey = "id";
        private RedirectData redirectData;
        private RedirectManager manager = new RedirectManager();
        string returnpath = "";
        private bool isvalid = true;
        public string sitePath;

        [System.ComponentModel.DefaultValue(false)]
        public bool IsEditable { get; set; }

        #endregion

        #region page events

        protected void Page_Init(object sender, EventArgs e)
        {
            uxnewurlUIerrormessage.Text = GetLocalResourceObject("errNewURLMessage").ToString();
            uxORGurlUIerrormessage.Text = GetLocalResourceObject("uxORGurlerrormessage").ToString();
            uxoptionalmessage.Text = GetLocalResourceObject("uxoptionalmessage").ToString();

            if (this.HasViewPermission())
            {
                sitePath = siteAPI.SitePath;
                try
                {
                    returnpath = ContentAPI.Current.ApplicationPath + "UrlAliasing/urlaliasingredirects.aspx";
                    if (!String.IsNullOrEmpty(Request.QueryString[querystringKey]))
                    {
                        redirectData = manager.GetItem(long.Parse(Request.QueryString[querystringKey]));
                        if (redirectData.Id == 0)
                        {
                            redirectData = null;
                        }
                    }
                    this.IsEditable = ((redirectData != null && Page.IsPostBack) || (redirectData == null && !Page.IsPostBack));

                    this.initWorkareaUI();
                    this.setUI();
                }
                catch (Exception exc)
                {
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (isvalid)
            {
                uxMessage.Visible = false;
                // for inline databinding
                this.DataBind();
            }
        }

        protected void uxSaveButton_Click(object sender, EventArgs e)
        {
            if (redirectData == null)
            {
                redirectData = new RedirectData();
            }
            bool isUpdate = (redirectData.Id > 0);
            try
            {
                redirectData.Active = uxActiveCheckBox.Checked;
                redirectData.StatusCode = (HttpStatusCode)int.Parse(uxRedirectCodeDropDownList.SelectedValue);
                var siteId = long.Parse(uxSiteDropDownList.SelectedValue);
                if (!IsValidOrigUrl(uxOriginalURL.Text))
                {
                    uxMessage.Text = GetLocalResourceObject("errOriginalURLMessage").ToString();
                    uxMessage.Visible = true;
                    isvalid = false;
                    return;
                }

                if (origUrlAlreadyExists(uxOriginalURL.Text, redirectData.Id, siteId))
                {
                    uxMessage.Text = GetLocalResourceObject("errOriginalURLExists").ToString();
                    uxMessage.Visible = true;
                    isvalid = false;
                    return;
                }

                if (uxOriginalURL.Text.TrimStart('/').ToLower().StartsWith(siteAPI.RequestInformationRef.ApplicationPath.ToLower().Trim('/')) ||
                        uxNewURL.Text.TrimStart('/').ToLower().StartsWith(siteAPI.RequestInformationRef.ApplicationPath.ToLower().Trim('/')))
                {
                    uxMessage.Text = GetLocalResourceObject("errNotWorkarea").ToString();
                    uxMessage.Visible = true;
                    isvalid = false;
                    return;
                }

                var orgurl = uxOriginalURL.Text.TrimStart('/');
                redirectData.SourceURL = orgurl;

                if (!string.IsNullOrWhiteSpace(uxNewURL.Text) && !uxNewURL.Text.Contains(" ") && uxNewURL.Text.Length > 0)
                {
                    var newurl = uxNewURL.Text;
                    if (!uxNewURL.Text.Contains("://") && !uxNewURL.Text.StartsWith("/"))
                    {
                        newurl = "/" + uxNewURL.Text;
                    }
                    if (IsvalidNewURL(newurl))
                    {
                        redirectData.TargetURL = newurl;
                    }
                    else
                    {
                        uxMessage.Visible = true;
                        isvalid = false;
                        return;
                    }
                }
                else
                {
                    if ((HttpStatusCode)int.Parse(uxRedirectCodeDropDownList.SelectedValue) == HttpStatusCode.NotFound)
                    {
                        redirectData.TargetURL = string.Empty;
                    }
                    else
                    {
                        uxMessage.Text = GetLocalResourceObject("errNewURLMessage").ToString();
                        uxMessage.Visible = true;
                        isvalid = false;
                        return;
                    }
                }
                if (isUpdate)
                {
                    manager.Update(redirectData);
                }
                else
                {
                    redirectData.SiteId = siteId;
                    redirectData = manager.Add(redirectData);
                    Response.Redirect("AddEditViewRedirect.aspx?" + querystringKey + "=" + redirectData.Id.ToString(), true);
                }
                isvalid = true;
            }
            catch (ThreadAbortException)
            {
                //saved and redirected
            }
            catch (Exception exc)
            {
                uxMessage.Text = exc.Message;
                uxMessage.Visible = true;
                isvalid = false;
                return;
            }

            this.IsEditable = false;
            this.bindData(redirectData);
            this.setUI();
            uxSiteDropDownList.Enabled = false;
        }

        private bool origUrlAlreadyExists(string origurl, long origId, long siteId)
        {
            origurl = origurl.TrimStart('/');
            RedirectCriteria crit = new RedirectCriteria();
            crit.Condition = LogicalOperation.And;
            crit.AddFilter(RedirectProperty.SourceURL, CriteriaFilterOperator.EqualTo, origurl);
            crit.AddFilter(RedirectProperty.Id, CriteriaFilterOperator.NotEqualTo, origId);
            crit.AddFilter(RedirectProperty.SiteId, CriteriaFilterOperator.EqualTo, siteId);
            List<RedirectData> redirects = manager.GetList(crit);
            return redirects.Count > 0;
        }

        private bool IsValidOrigUrl(string origurl)
        {
            if (origurl == null) return false;
            if (uxOriginalURL.Text.Contains(" ")) return false;
            if (uxOriginalURL.Text.Contains(":/")) return false;
            return true;
        }

        /// <summary>
        /// Check the New URL valid or NOT
        /// </summary>
        /// <param name="newurl">URL</param>
        /// <returns>Bool </returns>
        private bool IsvalidNewURL(string newurl)
        {
            uxMessage.Text = GetLocalResourceObject("errNewURLMessage").ToString();
            bool isvalid = false;
            if (!string.IsNullOrEmpty(newurl))
            {
                // 200 Always required both URL's relative
                if ((HttpStatusCode)int.Parse(uxRedirectCodeDropDownList.SelectedValue) == HttpStatusCode.OK)
                {
                    if (newurl.StartsWith("/"))
                    {
                        return isvalid = true;
                    }
                    else
                    {
                        uxMessage.Text = GetLocalResourceObject("errAddHTTPOKRedirect").ToString();
                        return isvalid = false;
                    }
                }

                // If Target URL is relative
                if (newurl.StartsWith("/"))
                {
                    return isvalid = true;
                }

                // Validation for External URLS.
                Uri result;
                if (Uri.TryCreate(newurl, UriKind.RelativeOrAbsolute, out result))
                {
                    return isvalid = true;
                }
            }

            return isvalid;
        }
        protected void uxEditButton_Click(object sender, EventArgs e)
        {
            this.setEditUI();
        }

        protected void uxDelete_Click(object sender, EventArgs e)
        {
            try
            {
                manager.Delete(redirectData.Id);
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
            Response.Redirect("UrlAliasingRedirects.aspx", true);
        }

        #endregion

        #region private methods

        private void initWorkareaUI()
        {
            this.RegisterResources();
            StyleHelper styleHelper = new StyleHelper();
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
            this.aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_redirects_detail", string.Empty);
        }

        private void bindUIOptions()
        {
            site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
            bindSiteList();
            bindTypeList();
        }

        private void bindSiteList()
        {
            Dictionary<long, string> siteList = site.GetSiteList();
            var displaySites = from sites in siteList
                               select new { Label = sites.Value, ID = sites.Key };
            uxSiteDropDownList.DataSource = displaySites;
            uxSiteDropDownList.DataTextField = "Label";
            uxSiteDropDownList.DataValueField = "ID";
            uxSiteDropDownList.DataBind();
        }

        private void bindTypeList()
        {
            uxRedirectCodeDropDownList.Items.Add(new ListItem(HttpStatusCode.OK.GetHashCode().ToString() + " - " + GetLocalResourceObject("Oklabel"), HttpStatusCode.OK.GetHashCode().ToString()));
            uxRedirectCodeDropDownList.Items.Add(new ListItem(HttpStatusCode.MovedPermanently.GetHashCode().ToString() + " - " + GetLocalResourceObject("MovedPermanentlylabel"), HttpStatusCode.MovedPermanently.GetHashCode().ToString()));
            uxRedirectCodeDropDownList.Items.Add(new ListItem(HttpStatusCode.Redirect.GetHashCode().ToString() + " - " + GetLocalResourceObject("TempRedirectlabel"), HttpStatusCode.Redirect.GetHashCode().ToString()));
            uxRedirectCodeDropDownList.Items.Add(new ListItem(HttpStatusCode.NotFound.GetHashCode().ToString() + " - " + GetLocalResourceObject("NotFoundlabel"), HttpStatusCode.NotFound.GetHashCode().ToString()));
            uxRedirectCodeDropDownList.SelectedValue = HttpStatusCode.MovedPermanently.GetHashCode().ToString();
        }

        private void setEditUI()
        {
            returnpath = ContentAPI.Current.ApplicationPath + "UrlAliasing/AddEditViewRedirect.aspx?" + querystringKey + "=" + Request.QueryString[querystringKey];
            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxDelete.Visible = false;
            uxBackButton.Visible = true;
        }

        private void setUI()
        {
            if (!Page.IsPostBack)
            {
                this.bindUIOptions();
            }
            this.RegisterResources();

            // View Mode
            if (redirectData != null && !Page.IsPostBack)
            {
                uxEditButton.Visible = HasEditPermission();
                uxSaveButton.Visible = false;
                uxSiteDropDownList.Enabled = false;
                uxBackButton.Visible = true;
                this.bindData(redirectData);
            }

            // Edit mode
            if (redirectData != null && Page.IsPostBack)
            {
                // moved to click event handler
                uxEditButton.Visible = HasEditPermission();
            }

            // Add Mode
            if (redirectData == null)
            {
                uxEditButton.Visible = false;
                uxSaveButton.Visible = true;
                uxSiteDropDownList.Enabled = true;
                uxBackButton.Visible = true;
            }
        }

        private void bindData(RedirectData redirectData)
        {
            uxActiveCheckBox.Checked = redirectData.Active;
            uxSiteDropDownList.SelectedValue = redirectData.SiteId.ToString();
            uxRedirectCodeDropDownList.SelectedValue = redirectData.StatusCode.GetHashCode().ToString();
            uxOriginalURL.Text = redirectData.SourceURL;
            uxNewURL.Text = redirectData.TargetURL;
        }

        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                );
        }

        private bool HasEditPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
                );
        }


        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
        }

        #endregion

    }
}