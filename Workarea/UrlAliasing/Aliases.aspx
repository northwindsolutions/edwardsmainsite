﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Aliases.aspx.cs" Inherits="Ektron.Workarea.UrlAliasing.Aliases"
    meta:resourcekey="PageResource1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
            function RollOver(MyObj) {
                $ektron(MyObj).parent().addClass("button-over");
                $ektron(MyObj).parent().removeClass("button");
            }

            function RollOut(MyObj) {
                $ektron(MyObj).parent().addClass("button");
                $ektron(MyObj).parent().removeClass("button-over");
            }
            function disableCheckboxes() {
                var uxgridrows = $ektron("table#uxAliasListGridView tr");
                uxgridrows.each(function (index, item) {
                    if ($ektron(item).find("span.EditNotAllowed").length > 0) {
                        $ektron(item).find("input[type='checkbox']").attr("disabled", true);
                    }
                });
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:JavaScript ID="aliasJS" runat="server" Path="js/ektron.workarea.urlaliasing.js" />
    <ektronUI:Css ID="aliasCSS" Path="CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                    <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:HyperLink runat="server" ID="uxAddLink" class="primary editButton" NavigateUrl="AddEditViewAlias.aspx"
                                meta:resourcekey="uxAddLink" />
                        </td>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" OnClick="EditButton_Click"
                                meta:resourcekey="uxedittxt_labelBase" />
                            <asp:LinkButton runat="server" ID="uxSaveButton" Visible="False" meta:resourcekey="uxSavetxt_labelBase"
                                class="primary saveButton" OnClick="SaveButton_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td class="column-Type">
                            <asp:Label ID="uxTypeLabel" runat="server" meta:resourcekey="uxTypeLabel" />
                        </td>
                        <td class="column-Type">
                            <asp:DropDownList ID="uxTypeList" runat="server" meta:resourcekey="uxTypeList" AutoPostBack="true"
                                OnSelectedIndexChanged="filterChange">
                                <asp:ListItem Text="All" Value="0" Selected="True" />
                            </asp:DropDownList>
                        </td>
                        <td class="column-Language">
                            <asp:Label ID="uxLanguageLabel" runat="server" meta:resourcekey="uxLanguageLabel" />
                        </td>
                        <td class="column-Language">
                            <asp:DropDownList ID="uxLanguageList" runat="server" meta:resourcekey="uxLanguageList"
                                AutoPostBack="true" OnSelectedIndexChanged="filterChange" />
                        </td>
                        <td class="column-Site">
                            <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                        </td>
                        <td class="column-Site">
                            <asp:DropDownList ID="uxSiteList" runat="server" meta:resourcekey="uxSiteList" AutoPostBack="true"
                                OnSelectedIndexChanged="filterChange" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td class="column-Search">
                        </td>
                        <td class="column-Search">
                            <asp:TextBox ID="uxSearchTextBox" runat="server" meta:resourcekey="uxSearchTextBox"
                                Text="Search" CssClass="searchTextCss" Width="100px" 
                                onfocus="if(this.value=='Search')this.value=''" onblur="if(this.value=='')this.value='Search'" />
                        </td>
                        <td class="column-Search">
                            <asp:ImageButton runat="server" ID="uxSearchButton" meta:resourcekey="uxSearchButton"
                                class="deleteButton" OnClick="filterChange" ImageUrl="../images/ui/icons/magnifier.png" />
                            <asp:HyperLink ID="uxSearchReset" runat="server" NavigateUrl="aliases.aspx"
                                CssClass="resetButton" meta:resourcekey="uxSearchReset">
                                <img src="../images/ui/icons/restore.png" />
                            </asp:HyperLink>
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageGrid aliasListContainer">
            <ektronUI:Message ID="uxMessage" runat="server" Visible='<%# this.showMessage %>' />
            <ektronUI:GridView ID="uxAliasListGridView" runat="server" AutoGenerateColumns="false" EktronUIAllowResizableColumns="false"
                OnEktronUISortChanged="uxAliasListGridView_EktronUIThemeSortChanged" EktronUIEnabled="true"
                OnEktronUIPageChanged="uxAliasListGridView_EktronUIThemePageChanged">
                <Columns>
                    <ektronUI:CheckBoxField ID="uxDeleteField">
                        <CheckBoxFieldColumnStyle Visible="<%# this.IsEditable %>" />
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderDelete %>" />
                        <CheckBoxFieldItemStyle Enabled="<%# this.IsEditable %>" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <ektronUI:CheckBoxField ID="uxActiveField" >
                        <CheckBoxFieldColumnStyle Width="100px" />
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderActive %>" HeaderCheckBoxEnabled="<%# this.IsEditable %>" />
                        <%--<CheckBoxFieldItemStyle Enabled='<%# this.IsEditableType((Ektron.Cms.Common.EkEnumeration.AliasRuleType)Eval("Type")) %>' CheckedBoundField="IsEnabled" />--%>
                        <CheckBoxFieldItemStyle Enabled='<%# this.IsEditable %>' CheckedBoundField="IsEnabled" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <asp:TemplateField meta:resourceKey="columnHeaderType" SortExpression="Type">
                        <ItemTemplate>
                            <asp:Literal runat="server" ID="Type" Text='<%#this.getTypeImage((Ektron.Cms.Common.EkEnumeration.AliasRuleType)Eval("Type"))%>' />
                            <asp:Literal runat="server" ID="EditAllowed" Text='<%#this.getEditAllowed((Ektron.Cms.Common.EkEnumeration.AliasRuleType)Eval("Type"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField meta:resourceKey="columnHeaderLanguage" SortExpression="LanguageId">
                        <ItemTemplate>
                            <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageId")))%>'
                                alt='<%# Eval("LanguageId") %>' title='<%# Eval("LanguageId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataTextField="Alias" SortExpression="Alias" DataNavigateUrlFormatString="AddEditViewAlias.aspx?id={0}"
                        DataNavigateUrlFields="Id" meta:resourceKey="columnHeaderAlias" />
                    <asp:BoundField DataField="TargetURL" SortExpression="Target" meta:resourceKey="columnHeaderTarget" />
                    <asp:TemplateField meta:resourceKey="columnHeaderTitle">
                        <ItemTemplate>
                            <asp:Literal ID="targetTitle" runat="server" Text='<%# this.getContentTitle((long)Eval("TargetId"), (int)Eval("LanguageId"), (Ektron.Cms.Common.EkEnumeration.TargetType)Eval("TargetType")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TargetId" SortExpression="TargetId" meta:resourceKey="columnHeaderTargetID" />
                </Columns>
            </ektronUI:GridView>
        </div>
    </div>
    </form>
</body>
</html>
