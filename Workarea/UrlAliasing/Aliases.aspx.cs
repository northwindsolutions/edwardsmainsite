﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Community;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.User;
    using Ektron.Cms.Settings;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;

    /// <summary>
    /// Automatic Aliasing Rules
    /// </summary>
    public partial class Aliases : Ektron.Cms.Workarea.Page
    {
        #region Variables

        private int pageSize = 20;
        private SiteAPI siteAPI = new SiteAPI();
        private ISite site;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private AliasManager aliasManager = new AliasManager();
        private AliasRuleManager configManager = new AliasRuleManager();
        private List<AliasData> aliasList;
        private AliasCriteria criteria;
        private AliasRuleData config;
        AliasSettings urlAliasingSettingsData;
        private long configID = 0;

        public bool IsEditable { get { return (ViewState["IsEditable"] != null) ? (bool)ViewState["IsEditable"] : false; } set { ViewState["IsEditable"] = value; } }
        [System.ComponentModel.DefaultValue(false)]
        public bool showMessage { get; set; }

        #endregion

        #region Page Events


        protected override void OnInit(EventArgs e)
        {
            if (this.HasViewPermission())
            {
                try
                {
                    base.OnInit(e);
                    this.pageSize = siteAPI.RequestInformationRef.PagingSize;
                    AliasSettingsManager urlAliasSettingsManager = new AliasSettingsManager();
                    urlAliasingSettingsData = urlAliasSettingsManager.Get();
                    this.initWorkareaUI();
                    bool isenabled = IsFeatureEnabled();
                    this.setViewUI(isenabled);

                    if (Request.QueryString["id"] != null && !String.IsNullOrEmpty(Request.QueryString["id"].ToString()))
                    {
                        long.TryParse(Request.QueryString["id"], out configID);
                    }

                    if (configID != 0)
                    {
                        config = configManager.GetItem(configID);
                        uxAddLink.Visible = false;
                        uxEditButton.Visible = false;
                        uxTypeList.Enabled = false;
                        uxLanguageList.Enabled = false;
                        uxSiteList.Enabled = false;
                        uxBackButton.Visible = true;
                        uxTypeList.SelectedValue = config.Type.GetHashCode().ToString();
                        uxSiteList.SelectedValue = config.SiteId.ToString();
                        uxLanguageList.SelectedValue = config.LanguageId.ToString();
                        this.showMessage = false;
                    }


                }
                catch (Exception exc)
                {
                    //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        /// <summary>
        /// Use the OnLoad event method to set properties in controls.
        /// </summary>
        /// <param name="e">EventArgs e</param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                criteria = this.getCriteria();
                if (!Page.IsPostBack && IsFeatureEnabled())
                {
                    this.bindData();
                }
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            this.DataBind();
            if (this.IsEditable)
            {
                Ektron.Cms.API.JS.RegisterJSBlock(this, "disableCheckboxes();", "disableCheckBoxes");
            }
        }

        protected void filterChange(object sender, EventArgs e)
        {
            try
            {
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void uxAliasListGridView_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            try
            {
                this.criteria.OrderByField = (AliasProperty)Enum.Parse(typeof(AliasProperty), e.OrderByFieldText);
                this.criteria.OrderByDirection = e.OrderByDirection;
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void uxAliasListGridView_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByDirection = uxAliasListGridView.EktronUIOrderByDirection;
                this.criteria.OrderByField = (AliasProperty)Enum.Parse(typeof(AliasProperty), uxAliasListGridView.EktronUIOrderByFieldText);

                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            this.setEditUI();
            this.bindData();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    // Activate
                    Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField enableColumn = uxAliasListGridView.Columns[1] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                    enableColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                    {
                        string originalState = dirtyField.OriginalState;
                        string dirtyState = dirtyField.DirtyState;
                        if (originalState != dirtyState)
                        {
                            string id = dirtyField.KeyFields["Id"];
                            long aliasID = long.Parse(id);
                            AliasData alias = aliasManager.GetItem(aliasID);
                            alias.IsEnabled = !alias.IsEnabled;
                            aliasManager.Update(alias);
                        }
                    });

                    // Delete
                    Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField deleteColumn = uxAliasListGridView.Columns[0] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                    deleteColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                    {
                        string originalState = dirtyField.OriginalState;
                        string dirtyState = dirtyField.DirtyState;
                        if (originalState != dirtyState)
                        {
                            string id = dirtyField.KeyFields["Id"];
                            long aliasID = long.Parse(id);
                            aliasManager.Delete(aliasID);
                        }
                    });

                    this.IsEditable = false;
                    this.setViewUI(true);
                    this.bindData();
                }
                catch (Exception exc)
                {
                    //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                    Utilities.ShowError(exc.Message);
                }

                uxBackButton.Visible = false;

            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        #endregion

        #region Private Methods

        private void setViewUI(bool isEnabled)
        {
            this.IsEditable = false;

            if (!Page.IsPostBack)
            {
                this.bindUIOptions();
            }

            uxEditButton.Visible = this.HasEditPermission();
            uxSaveButton.Visible = false;
            uxBackButton.Visible = false;
            uxTypeList.Enabled = true;
            uxLanguageList.Enabled = true;
            uxSiteList.Enabled = true;
            uxSearchTextBox.Enabled = true;
            uxSearchButton.Enabled = true;
            uxSearchReset.Enabled = true;
            uxAddLink.Visible = urlAliasingSettingsData.IsManualAliasingEnabled &&
                this.HasEditPermission();

            // If alias is turned off
            if (!isEnabled)
            {
                uxAddLink.Attributes["OnClick"] = "return false;";
                uxEditButton.Attributes["OnClick"] = "return false;";
                uxSiteList.Enabled = false;
                uxTypeList.Enabled = false;
                uxLanguageList.Enabled = false;
                uxSearchTextBox.Enabled = false;
                uxSearchButton.Enabled = false;
                uxSearchReset.Enabled = false;
                showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxMessage.Text = GetLocalResourceObject("FeatureDisabledMessage").ToString();
            }
        }

        private void setEditUI()
        {
            this.IsEditable = true;
            uxBackButton.Visible = true;
            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxAddLink.Visible = false;
            uxTypeList.Enabled = false;
            uxLanguageList.Enabled = false;
            uxSiteList.Enabled = false;
            uxSearchTextBox.Enabled = false;
            uxSearchButton.Enabled = false;
            uxSearchReset.Enabled = false;
        }

        private Dictionary<Tuple<EkEnumeration.TargetType, int, long>, string> titleDic = new Dictionary<Tuple<EkEnumeration.TargetType, int, long>, string>();
        private void populateTitleDic(List<AliasData> aliasList)
        {
            titleDic = new Dictionary<Tuple<EkEnumeration.TargetType, int, long>, string>();
            ContentAPI capi = new ContentAPI();
            CommunityGroupManager cm = new CommunityGroupManager(Cms.Framework.ApiAccessMode.Admin);
            UserManager usermanager = new UserManager(Cms.Framework.ApiAccessMode.Admin);
            foreach (AliasData item in aliasList)
            {
                var key = new Tuple<EkEnumeration.TargetType, int, long>(item.TargetType, item.LanguageId, item.TargetId);
                if (!titleDic.ContainsKey(key))
                {
                    string title = "";
                    if (item.LanguageId > 0)
                    {
                        capi.EkContentRef.RequestInformation.ContentLanguage = item.LanguageId;
                    }

                    switch (item.Type)
                    {
                        case EkEnumeration.AliasRuleType.Folder:
                        case EkEnumeration.AliasRuleType.Manual:
                        case EkEnumeration.AliasRuleType.Taxonomy:
                            title = capi.EkContentRef.GetContentTitle(item.TargetId);
                            break;
                        case EkEnumeration.AliasRuleType.Group:
                            title = cm.GetItem(item.TargetId).Name;
                            break;
                        case EkEnumeration.AliasRuleType.User:
                            title = usermanager.GetItem(item.TargetId, false,false).Username;
                            break;
                        default:
                            title = "Content Title";
                            break;
                    }
                    titleDic.Add(key, title);
                }
            }
        }
        private void bindData()
        {
            aliasList = aliasManager.GetList(this.criteria);
            if (aliasList != null && aliasList.Count > 0)
            {
                populateTitleDic(aliasList);
                uxAliasListGridView.DataSource = aliasList;
                uxAliasListGridView.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxAliasListGridView.EktronUIOrderByFieldText = this.criteria.OrderByField.ToString();
                uxAliasListGridView.DataBind();
            }
            else
            {
                uxAliasListGridView.DataSource = null;
                uxAliasListGridView.DataBind();
                uxEditButton.Visible = false;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxMessage.Text = GetLocalResourceObject("NoData").ToString();
            }
        }

        private bool IsFeatureEnabled()
        {
            bool returnVal = true;
            returnVal = urlAliasingSettingsData.IsAliasingEnabled && (
                        urlAliasingSettingsData.IsManualAliasingEnabled ||
                        urlAliasingSettingsData.IsTaxonomyAliasingEnabled ||
                        urlAliasingSettingsData.IsFolderAliasingEnabled ||
                        urlAliasingSettingsData.IsUserAliasingEnabled ||
                        urlAliasingSettingsData.IsGroupAliasingEnabled
                );

            return returnVal;
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            StyleHelper styleHelper = new StyleHelper();
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "UrlAliasing/aliases.aspx";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
            aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_aliases_list", string.Empty);
        }


        private void bindUIOptions()
        {
            bindLanguageList(siteAPI.DefaultContentLanguage);
            site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
            bindSiteList();
            bindTypeList();
        }

        private void bindLanguageList(long defaultlanguage)
        {
            LanguageData[] language_data = this.siteAPI.GetAllActiveLanguages();

            var languages = from LanguageData in language_data
                            select new
                            {
                                Name = LanguageData.Name,
                                value = LanguageData.Id
                            };

            uxLanguageList.DataSource = null;
            uxLanguageList.Items.Clear();
            uxLanguageList.DataSource = languages;
            uxLanguageList.DataTextField = "Name";
            uxLanguageList.DataValueField = "value";
            uxLanguageList.AppendDataBoundItems = true;
            uxLanguageList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
            uxLanguageList.SelectedValue = Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString();
        }

        private void bindSiteList()
        {
            Dictionary<long, string> siteList = site.GetSiteList();
            var displaySites = from sites in siteList
                               select new { Label = sites.Value, ID = sites.Key };
            uxSiteList.DataSource = displaySites;
            uxSiteList.DataTextField = "Label";
            uxSiteList.DataValueField = "ID";
            uxSiteList.DataBind();
            uxSiteList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
        }

        private void bindTypeList()
        {
            //See EkEnumeration.AliasType
            if (urlAliasingSettingsData.IsTaxonomyAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Taxonomy), EkEnumeration.AliasRuleType.Taxonomy.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsFolderAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Folder), EkEnumeration.AliasRuleType.Folder.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsManualAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Manual), EkEnumeration.AliasRuleType.Manual.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsUserAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.User), EkEnumeration.AliasRuleType.User.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsGroupAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Group), EkEnumeration.AliasRuleType.Group.GetHashCode().ToString()));
            }
        }

        private string getLocalTypeName(EkEnumeration.AliasRuleType configType)
        {
            string returnVal;

            try
            {
                returnVal = GetLocalResourceObject("configType" + configType.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = configType.ToDescriptionString();
            }

            return returnVal;
        }

        protected bool IsEditableType(EkEnumeration.AliasRuleType autoAliasType)
        {
            return (autoAliasType == EkEnumeration.AliasRuleType.Manual);
        }

        protected string getEditAllowed(EkEnumeration.AliasRuleType autoAliasType)
        {
            string allowEdit = "<span class=\"";
            if (autoAliasType == EkEnumeration.AliasRuleType.Manual)
                allowEdit += "EditAllowed";
            else
                allowEdit += "EditNotAllowed";
            allowEdit += "\">&nbsp;</span>";
            return allowEdit;
        }

        protected string getTypeImage(EkEnumeration.AliasRuleType autoAliasType)
        {
            string image = "";
            switch (autoAliasType)
            {
                case EkEnumeration.AliasRuleType.Taxonomy:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/taxonomy.png", getLocalTypeName(autoAliasType));
                    break;
                case EkEnumeration.AliasRuleType.Folder:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/folder.png", getLocalTypeName(autoAliasType));
                    break;
                case EkEnumeration.AliasRuleType.User:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/userMembership.png", getLocalTypeName(autoAliasType));
                    break;
                case EkEnumeration.AliasRuleType.Group:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/usersMemberGroups.png", getLocalTypeName(autoAliasType));
                    break;
            }
            return image;
        }

        protected string getContentTitle(long id, int languageID, EkEnumeration.TargetType targetType)
        {
            return titleDic[new Tuple<EkEnumeration.TargetType, int, long>(targetType, languageID, id)];
        }

        private AliasCriteria getCriteria()
        {
            AliasCriteria criteria = new AliasCriteria();

            long siteID = long.Parse(uxSiteList.SelectedValue);
            if (siteID != -1)
            {
                criteria.AddFilter(AliasProperty.SiteId, CriteriaFilterOperator.EqualTo, siteID);
            }

            int langID = int.Parse(uxLanguageList.SelectedValue);
            if (langID != -1)
            {
                CriteriaFilterGroup<AliasProperty> langGroup = new CriteriaFilterGroup<AliasProperty>();
                langGroup.Condition = LogicalOperation.Or;
                langGroup.AddFilter(AliasProperty.LanguageId, CriteriaFilterOperator.EqualTo, langID);
                langGroup.AddFilter(AliasProperty.LanguageId, CriteriaFilterOperator.EqualTo, 0);
                langGroup.AddFilter(AliasProperty.LanguageId, CriteriaFilterOperator.EqualTo, -1);
                criteria.FilterGroups.Add(langGroup);
            }

            EkEnumeration.AliasRuleType type = (EkEnumeration.AliasRuleType)(int.Parse(uxTypeList.SelectedValue));
            if (type != EkEnumeration.AliasRuleType.None && int.Parse(uxTypeList.SelectedValue) != -1)
            {
                criteria.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, type);
            }
            else
            {
                CriteriaFilterGroup<AliasProperty> typeGroup = new CriteriaFilterGroup<AliasProperty>();
                typeGroup.Condition = LogicalOperation.Or;
                // must be searching for all type, only those that are enabled.
                if (urlAliasingSettingsData.IsTaxonomyAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Taxonomy);
                }
                if (urlAliasingSettingsData.IsFolderAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Folder);
                }
                if (urlAliasingSettingsData.IsManualAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Manual);
                }
                if (urlAliasingSettingsData.IsUserAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.User);
                }
                if (urlAliasingSettingsData.IsGroupAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Group);
                }
                criteria.FilterGroups.Add(typeGroup);
            }

            string searchText = uxSearchTextBox.Text;
            if (!String.IsNullOrEmpty(searchText) && searchText != "Search")
            {
                criteria.AddFilter(AliasProperty.Alias, CriteriaFilterOperator.Contains, searchText);
            }

            if (this.configID != 0)
            {
                criteria.AddFilter(AliasProperty.ConfigurationId, CriteriaFilterOperator.EqualTo, configID);
            }

            criteria.OrderByDirection = uxAliasListGridView.EktronUIOrderByDirection;
            if (!string.IsNullOrEmpty(uxAliasListGridView.EktronUIOrderByFieldText))
            {
                criteria.OrderByField = (AliasProperty)Enum.Parse(typeof(AliasProperty), uxAliasListGridView.EktronUIOrderByFieldText);
            }
            else
            {
                uxAliasListGridView.EktronUIOrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByField = AliasProperty.Alias;
            }

            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = pageSize;
            criteria.PagingInfo = pagingInfo;

            return criteria;
        }

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access
        /// the functionality on this page, false otherwise.
        /// </summary>
        /// <returns>True if permissions are sufficient, false otherwise</returns>
        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                );
        }

        private bool HasEditPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
                );
        }

        #endregion


        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
        }

        #endregion

    }
}