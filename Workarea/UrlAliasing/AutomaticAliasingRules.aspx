﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AutomaticAliasingRules.aspx.cs"
    Inherits="Ektron.Workarea.UrlAliasing.AutomaticAliasingRules" 
    meta:resourcekey="PageResource1" %>
<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Cache-control" content="no-cache" />
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
        function ConfirmAliasDelete() {
            var uxDeleteFieldChk = 0;
            var uxDeleteFieldCnt = 0;
            for (i = 0; i < parseInt(document.getElementById("uxAutoRulesGridView").rows.length - 1); i++) {
                if (document.getElementById("uxAutoRulesGridView").rows[i].cells[uxDeleteFieldChk].children[0].checked == true) {
                    uxDeleteFieldCnt++;
                }
            }
            if (uxDeleteFieldCnt > 0) {
                if (!confirm(document.getElementById("spMsgDelAlias").innerHTML)) {
                    return false;
                }
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <span id="spMsgDelAlias" runat="server"/>
    <ektronUI:Css ID="aliasCSS" Path="CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <td class="column-AddButton">
                            <div>
                                <asp:HyperLink ID="uxAddButton" runat="server" CssClass="primary editButton" meta:resourcekey="uxAddButton"
                                    NavigateUrl="AddEditViewAutomaticAliasingRule.aspx"></asp:HyperLink>
                            </div>
                        </td>
                        <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" OnClick="EditButton_Click"
                                meta:resourcekey="uxedittxt_labelBase" Visible="false" />
                            <asp:LinkButton runat="server" ID="uxSaveButton" Visible="False" meta:resourcekey="uxSavetxt_labelBase"
                                class="primary saveButton" OnClick="SaveButton_Click" OnClientClick="javascript:return ConfirmAliasDelete();"/>
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td class="column-Type">
                            <asp:Label ID="uxTypeLabel" runat="server" meta:resourcekey="uxTypeLabel" />
                        </td>
                        <td class="column-Type">
                            <asp:DropDownList ID="uxTypeList" runat="server" meta:resourcekey="uxTypeList" AutoPostBack="true"
                                OnSelectedIndexChanged="filterChange">
                                <asp:ListItem Text="All" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td class="column-Language">
                            <asp:Label ID="uxLanguageLabel" runat="server" meta:resourcekey="uxLanguageLabel" />
                        </td>
                        <td class="column-Language">
                            <asp:DropDownList ID="uxLanguageList" runat="server" meta:resourcekey="uxLanguageList"
                                AutoPostBack="true" OnSelectedIndexChanged="filterChange" AppendDataBoundItems="true" />
                        </td>
                        <td class="column-Site">
                            <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                        </td>
                        <td class="column-Site">
                            <asp:DropDownList ID="uxSiteList" runat="server" meta:resourcekey="uxSiteList" AutoPostBack="true"
                                OnSelectedIndexChanged="filterChange" AppendDataBoundItems="true" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td class="column-Search">
                        </td>
                        <td class="column-Search">
                            <asp:TextBox ID="uxSearchTextBox" runat="server" meta:resourcekey="uxSearchTextBox"
                                Text="Search" CssClass="searchTextCss" Width="100px" onfocus="if(this.value=='Search')this.value=''"
                                onblur="if(this.value=='')this.value='Search'" />
                        </td>
                        <td class="column-Search">
                            <asp:ImageButton runat="server" ID="uxSearchButton" meta:resourcekey="uxSearchButton"
                                class="deleteButton" OnClick="filterChange" ImageUrl="../images/ui/icons/magnifier.png" />
                            <asp:HyperLink ID="uxSearchReset" runat="server" NavigateUrl="AutomaticAliasingRules.aspx"
                                CssClass="resetButton" meta:resourcekey="uxSearchReset">
                                <img src="../images/ui/icons/restore.png" />
                            </asp:HyperLink>
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageGrid aliasConfigListContainer">
            <ektronUI:Message ID="uxMessage" runat="server" Visible="<%# this.showMessage %>">
            </ektronUI:Message>
            <ektronUI:GridView ID="uxAutoRulesGridView" runat="Server" AutoGenerateColumns="false"
                EktronUIEnabled="true" OnEktronUISortChanged="uxAutoRulesGridView_EktronUIThemeSortChanged"
                OnEktronUIPageChanged="uxAutoRulesGridView_EktronUIThemePageChanged">
                <Columns>
                    <ektronUI:CheckBoxField ID="uxDeleteField">
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderDelete %>" />
                        <CheckBoxFieldColumnStyle Visible="<%# this.IsEditable %>" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <ektronUI:CheckBoxField ID="uxActiveField">
                        <CheckBoxFieldHeaderStyle HeaderCheckBoxEnabled="<%# this.IsEditable %>" HeaderText="<%$ Resources:columnHeaderActive %>" />
                        <CheckBoxFieldItemStyle Enabled="<%# this.IsEditable %>" CheckedBoundField="IsEnabled" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <asp:HyperLinkField DataTextField="Name" SortExpression="Name" DataNavigateUrlFormatString="AddEditViewAutomaticAliasingRule.aspx?id={0}"
                        DataNavigateUrlFields="Id" meta:resourceKey="columnHeaderName" />
                    <asp:TemplateField meta:resourceKey="columnHeaderType" SortExpression="Type">
                        <ItemTemplate>
                            <asp:Literal runat="server" ID="Type" Text='<%#this.getTypeImage((Ektron.Cms.Common.EkEnumeration.AliasRuleType)Eval("Type"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField meta:resourceKey="columnHeaderLanguage" SortExpression="LanguageId">
                        <ItemTemplate>
                            <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageId")))%>'
                                alt='<%# Eval("LanguageId") %>' title='<%# Eval("LanguageId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField meta:resourceKey="columnHeaderExample">
                        <ItemTemplate>
                            <asp:Literal ID="exampleAlias" runat="server" Text='<%# this.getExampleAlias((Ektron.Cms.Settings.UrlAliasing.DataObjects.AliasRuleData)Container.DataItem) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField meta:resourceKey="columnHeaderSite">
                        <ItemTemplate>
                            <asp:Literal ID="uxSiteID" runat="server" Text='<%# this.GetSiteNameById((Ektron.Cms.Settings.UrlAliasing.DataObjects.AliasRuleData)Container.DataItem) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField meta:resourceKey="columnHeaderPriority">
                        <ItemTemplate>
                            <asp:Literal ID="uxPriority" runat="server" Text='<%# ((Ektron.Cms.Settings.UrlAliasing.DataObjects.AliasRuleData)Container.DataItem).AliasPriority %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </ektronUI:GridView>
        </div>
    </div>
    </form>
</body>
</html>
