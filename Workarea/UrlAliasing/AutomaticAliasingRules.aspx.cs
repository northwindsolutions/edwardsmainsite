﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;
    using Ektron.Cms.Settings.UrlAliasing;
    using Ektron.Cms.Settings;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.Organization;

    /// <summary>
    /// Automatic Aliasing Rules
    /// </summary>
    public partial class AutomaticAliasingRules : Ektron.Cms.Workarea.Page
    {
        #region Local Variables

        private int pageSize = 20;
        private SiteAPI siteAPI = new SiteAPI();
        private ISite site;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private AliasRuleManager ruleManager = new AliasRuleManager();
        private List<AliasRuleData> aliasRules;
        private AliasRuleCriteria criteria;
        AliasSettings urlAliasingSettingsData;
        private AliasSettingsManager settingsManager = new AliasSettingsManager();
        private ContentAPI _capi = new ContentAPI();
        public Dictionary<long, string> siteList;
        public bool IsEditable { get { return (ViewState["IsEditable"] != null) ? (bool)ViewState["IsEditable"] : false; } set { ViewState["IsEditable"] = value; } }
        [System.ComponentModel.DefaultValue(false)]
        public bool showMessage { get; set; }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            if (this.HasViewPermission())
            {
                try
                {
                    base.OnInit(e);
                    this.initWorkareaUI();
                    this.pageSize = siteAPI.RequestInformationRef.PagingSize;
                    urlAliasingSettingsData = settingsManager.Get();
                    if (!Page.IsPostBack)
                    {
                        this.bindUIOptions();
                    }
                }
                catch (Exception exc)
                {
                    //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }


        /// <summary>
        /// Use the OnLoad event method to set properties in controls.
        /// </summary>
        /// <param name="e">EventArgs e</param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                ContentAPI refContentApi = new ContentAPI();
                EkMessageHelper msgHelper = refContentApi.EkMsgRef;
                spMsgDelAlias.InnerText = msgHelper.GetMessage("js:alert you sure you wish to delete this aliasing Continue?");
                criteria = this.getCriteria();
                if (!Page.IsPostBack)
                {
                    this.bindData();
                    this.setViewUI();
                }
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.DataBind();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Activate
                Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField enableColumn = uxAutoRulesGridView.Columns[1] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                enableColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                {
                    string originalState = dirtyField.OriginalState;
                    string dirtyState = dirtyField.DirtyState;
                    if (originalState != dirtyState)
                    {
                        string id = dirtyField.KeyFields["Id"];
                        long ruleID = long.Parse(id);
                        AliasRuleData rule = ruleManager.GetItem(ruleID);
                        rule.IsEnabled = !rule.IsEnabled;
                        ruleManager.Update(rule);
                    }
                });

                // Delete
                Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField deleteColumn = uxAutoRulesGridView.Columns[0] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                deleteColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                {
                    string originalState = dirtyField.OriginalState;
                    string dirtyState = dirtyField.DirtyState;
                    if (originalState != dirtyState)
                    {
                        string id = dirtyField.KeyFields["Id"];
                        long ruleID = long.Parse(id);
                        ruleManager.Delete(ruleID);
                    }
                });

                IsEditable = false;
                this.bindData();
                this.setViewUI();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }


        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            IsEditable = true;
            this.bindData();

            this.uxEditButton.Visible = false;
            this.uxSaveButton.Visible = true;
            this.uxAddButton.Visible = false;
            this.uxBackButton.Visible = true;
            this.uxTypeList.Enabled = false;
            this.uxLanguageList.Enabled = false;
            this.uxSiteList.Enabled = false;
            this.uxSearchTextBox.Enabled = false;
            this.uxSearchButton.Enabled = false;
            this.uxSearchReset.Enabled = false;
        }

        protected void filterChange(object sender, EventArgs e)
        {
            try
            {
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void uxAutoRulesGridView_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            try
            {
                this.criteria.OrderByField = (AliasRuleProperty)Enum.Parse(typeof(AliasRuleProperty), e.OrderByFieldText);
                this.criteria.OrderByDirection = e.OrderByDirection;
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void uxAutoRulesGridView_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByDirection = uxAutoRulesGridView.EktronUIOrderByDirection;
                this.criteria.OrderByField = (AliasRuleProperty)Enum.Parse(typeof(AliasRuleProperty), uxAutoRulesGridView.EktronUIOrderByFieldText);

                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        #endregion


        #region Private Members

        private void bindData()
        {
            aliasRules = ruleManager.GetList(this.criteria);
            if (aliasRules != null && aliasRules.Count > 0)
            {
                uxAutoRulesGridView.DataSource = aliasRules;
                uxAutoRulesGridView.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxAutoRulesGridView.EktronUIOrderByFieldText = this.criteria.OrderByField.ToString();
            }
            else
            {
                uxAutoRulesGridView.DataSource = null;
                //uxAutoRulesGridView.DataBind();

                showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Warning;
                uxMessage.Text = GetLocalResourceObject("NoData").ToString();
            }
        }

        private void setViewUI()
        {
            if (aliasRules == null || aliasRules.Count == 0)
            {
                uxEditButton.Visible = false;
            }
            else
            {
                this.uxEditButton.Visible = this.HasEditPermission();
            }
            this.uxAddButton.Visible = this.HasEditPermission();
            this.uxSaveButton.Visible = false;
            this.uxBackButton.Visible = false;
            this.uxTypeList.Enabled = true;
            this.uxLanguageList.Enabled = true;
            this.uxSiteList.Enabled = true;
            this.uxSearchTextBox.Enabled = true;
            this.uxSearchButton.Enabled = true;
            this.uxSearchReset.Enabled = true;

            // If alias is turned off
            if (!IsFeatureEnabled())
            {
                uxAddButton.Attributes["OnClick"] = "return false;";
                uxEditButton.Attributes["OnClick"] = "return false;";
                uxSiteList.Enabled = false;
                uxTypeList.Enabled = false;
                uxLanguageList.Enabled = false;
                uxSearchTextBox.Enabled = false;
                uxSearchButton.Enabled = false;
                uxSearchReset.Enabled = false;
                showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxMessage.Text = GetLocalResourceObject("FeatureDisabledMessage").ToString();
                uxAutoRulesGridView.Visible = false;
            }
        }

        private bool IsFeatureEnabled()
        {
            bool returnVal = true;
            returnVal = urlAliasingSettingsData.IsAliasingEnabled && (
                        urlAliasingSettingsData.IsTaxonomyAliasingEnabled ||
                        urlAliasingSettingsData.IsFolderAliasingEnabled ||
                        urlAliasingSettingsData.IsUserAliasingEnabled ||
                        urlAliasingSettingsData.IsGroupAliasingEnabled
                );

            return returnVal;
        }

        // General Workare related UI initialization
        private void initWorkareaUI()
        {
            this.RegisterResources();
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            StyleHelper styleHelper = new StyleHelper();
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "UrlAliasing/AutomaticAliasingRules.aspx?mode=auto";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
            aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_rules_automatic_list", string.Empty);
        }


        private void bindUIOptions()
        {
            bindLanguageList(siteAPI.DefaultContentLanguage);
            site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
            bindSiteList();
            bindTypeList();
        }

        private void bindLanguageList(long defaultlanguage)
        {
            LanguageData[] language_data = this.siteAPI.GetAllActiveLanguages();

            var languages = from LanguageData in language_data
                            select new
                            {
                                Name = LanguageData.Name,
                                value = LanguageData.Id
                            };
            uxLanguageList.Items.Clear();
            uxLanguageList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
            uxLanguageList.DataSource = languages;
            uxLanguageList.DataTextField = "Name";
            uxLanguageList.DataValueField = "value";
            uxLanguageList.SelectedValue = Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString();
        }

        private void bindSiteList()
        {
            siteList = site.GetSiteList();
            var displaySites = from sites in siteList
                               select new { Label = sites.Value, ID = sites.Key };
            uxSiteList.Items.Clear();
            uxSiteList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
            uxSiteList.DataSource = displaySites;
            uxSiteList.DataTextField = "Label";
            uxSiteList.DataValueField = "ID";
        }

        private void bindTypeList()
        {
            //See EkEnumeration.AliasType
            if (urlAliasingSettingsData.IsTaxonomyAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Taxonomy), EkEnumeration.AliasRuleType.Taxonomy.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsFolderAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Folder), EkEnumeration.AliasRuleType.Folder.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsUserAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.User), EkEnumeration.AliasRuleType.User.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsGroupAliasingEnabled)
            {
                uxTypeList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Group), EkEnumeration.AliasRuleType.Group.GetHashCode().ToString()));
            }
        }

        private string getLocalTypeName(EkEnumeration.AliasRuleType configType)
        {
            string returnVal;

            try
            {
                returnVal = GetLocalResourceObject("configType" + configType.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = configType.ToDescriptionString();
            }

            return returnVal;
        }

        private AliasRuleCriteria getCriteria()
        {
            AliasRuleCriteria criteria = new AliasRuleCriteria();

            long siteID = long.Parse(uxSiteList.SelectedValue);
            if (siteID != -1)
            {
                criteria.AddFilter(AliasRuleProperty.SiteId, CriteriaFilterOperator.EqualTo, siteID);
            }

            int langID = int.Parse(uxLanguageList.SelectedValue);
            if (langID != -1)
            {
                CriteriaFilterGroup<AliasRuleProperty> langGroup = new CriteriaFilterGroup<AliasRuleProperty>();
                langGroup.Condition = LogicalOperation.Or;
                langGroup.AddFilter(AliasRuleProperty.LanguageId, CriteriaFilterOperator.EqualTo, langID);
                langGroup.AddFilter(AliasRuleProperty.LanguageId, CriteriaFilterOperator.EqualTo, 0);
                langGroup.AddFilter(AliasRuleProperty.LanguageId, CriteriaFilterOperator.EqualTo, -1);
                criteria.FilterGroups.Add(langGroup);
            }

            EkEnumeration.AliasRuleType type = (EkEnumeration.AliasRuleType)(int.Parse(uxTypeList.SelectedValue));
            if (type != EkEnumeration.AliasRuleType.None && int.Parse(uxTypeList.SelectedValue) != -1)
            {
                criteria.AddFilter(AliasRuleProperty.Type, CriteriaFilterOperator.EqualTo, type);
            }
            else
            {
                CriteriaFilterGroup<AliasRuleProperty> typeGroup = new CriteriaFilterGroup<AliasRuleProperty>();
                typeGroup.Condition = LogicalOperation.Or;
                // must be searching for all type, only those that are enabled.
                if (urlAliasingSettingsData.IsTaxonomyAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasRuleProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Taxonomy);
                }
                if (urlAliasingSettingsData.IsFolderAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasRuleProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Folder);
                }
                if (urlAliasingSettingsData.IsUserAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasRuleProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.User);
                }
                if (urlAliasingSettingsData.IsGroupAliasingEnabled)
                {
                    typeGroup.AddFilter(AliasRuleProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Group);
                }
                criteria.FilterGroups.Add(typeGroup);
            }

            string searchText = uxSearchTextBox.Text;
            if (!String.IsNullOrEmpty(searchText) && searchText != "Search")
            {
                CriteriaFilterGroup<AliasRuleProperty> searchGroup = new CriteriaFilterGroup<AliasRuleProperty>();
                searchGroup.AddFilter(AliasRuleProperty.Name, CriteriaFilterOperator.Contains, searchText);
                criteria.FilterGroups.Add(searchGroup);
            }

            criteria.OrderByDirection = uxAutoRulesGridView.EktronUIOrderByDirection;
            if (!string.IsNullOrEmpty(uxAutoRulesGridView.EktronUIOrderByFieldText))
            {
                criteria.OrderByField = (AliasRuleProperty)Enum.Parse(typeof(AliasRuleProperty), uxAutoRulesGridView.EktronUIOrderByFieldText);
            }
            else
            {
                uxAutoRulesGridView.EktronUIOrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByField = AliasRuleProperty.Name;
            }

            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = pageSize;
            criteria.PagingInfo = pagingInfo;

            return criteria;
        }

        protected string getTypeImage(EkEnumeration.AliasRuleType autoAliasType)
        {
            string image = "";
            switch (autoAliasType)
            {
                case EkEnumeration.AliasRuleType.Taxonomy:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/taxonomy.png", getLocalTypeName(autoAliasType));
                    break;
                case EkEnumeration.AliasRuleType.Folder:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/folder.png", getLocalTypeName(autoAliasType));
                    break;
                case EkEnumeration.AliasRuleType.User:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/userMembership.png", getLocalTypeName(autoAliasType));
                    break;
                case EkEnumeration.AliasRuleType.Group:
                    image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/usersMemberGroups.png", getLocalTypeName(autoAliasType));
                    break;
            }
            return image;
        }

        private string getTaxonomyPathById(long id, int language)
        {
            string returnPath = "";
            TaxonomyManager tm = new TaxonomyManager(Cms.Framework.ApiAccessMode.Admin);
            if (language > 0) tm.ContentLanguage = language;
            TaxonomyData td = tm.GetItem(id);
            if (td != null)
            {
                returnPath = td.Path;
            }
            return returnPath;
        }

        private string getFormatString(EkEnumeration.AutoAliasNameType x)
        {
            switch (x)
            {
                case EkEnumeration.AutoAliasNameType.ContentId:
                    return "999";
                case EkEnumeration.AutoAliasNameType.ContentIdAndLanguage:
                    return "999/1033";
                case EkEnumeration.AutoAliasNameType.ContentTitle:
                    return "Content Title";
            }
            return x.ToDescriptionString();
        }

        protected string GetSiteNameById(AliasRuleData config)
        {
            if (siteList == null || !siteList.Any())
            {
                site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
                siteList = site.GetSiteList();
            }

            var list = (from x in siteList where x.Key == config.SiteId select x.Value).ToList();
            if (list != null && list.Any())
                return list[0].ToString();

            return "";
        }

        protected string getExampleAlias(AliasRuleData config)
        {
            _capi.ContentLanguage = config.LanguageId;

            string folderpath = "";
            string excludepath = "";
            string extension = (config.FileExtensionId != 0) ? settingsManager.GetExtension(config.FileExtensionId).Extension : "";
            string format = "";

            switch (config.Type)
            {
                case EkEnumeration.AliasRuleType.Folder:
                    folderpath = _capi.GetPathByFolderID(config.SourceId);
                    excludepath = _capi.GetPathByFolderID(config.ExcludedPathId);
                    if (config.SiteId > 0)
                    {
                        folderpath = folderpath.TrimStart('\\');
                        if (folderpath.IndexOf('\\') > 0) folderpath = folderpath.Substring(folderpath.IndexOf('\\'));
                        excludepath = excludepath.TrimStart('\\');
                        if (excludepath.IndexOf('\\') > 0) excludepath = excludepath.Substring(excludepath.IndexOf('\\'));
                    }

                    format = getFormatString(config.PageTypeId);
                    break;
                case EkEnumeration.AliasRuleType.Group:
                    format = "GroupName";
                    folderpath = config.Prefix;
                    break;
                case EkEnumeration.AliasRuleType.Taxonomy:
                    folderpath = getTaxonomyPathById(config.SourceId, config.LanguageId);
                    excludepath = getTaxonomyPathById(config.ExcludedPathId, config.LanguageId);
                    format = getFormatString(config.PageTypeId);
                    break;
                case EkEnumeration.AliasRuleType.User:
                    folderpath = config.Prefix;
                    format = "UserName";
                    break;
                default:
                    return "";
            }

            excludepath = excludepath.Trim('/');
            folderpath = folderpath.TrimStart('/');
            if (folderpath.IndexOf(excludepath) >= 0)
                folderpath = folderpath.Substring(excludepath.Length);
            if (!folderpath.EndsWith("/")) folderpath = folderpath + "/";
            folderpath = folderpath.Replace("\\", "/");

            string fullpath = siteAPI.SitePath + folderpath + format;
            fullpath = fullpath.Replace("//", "/");

            ReplacementCharacterManager rm = new ReplacementCharacterManager(Cms.Framework.ApiAccessMode.Admin);
            config.ReplacementCharacterMap = rm.GetList(config.Id);

            if (config.ReplacementCharacterMap != null)
            {
                foreach (var x in config.ReplacementCharacterMap)
                {
                    try
                    {
                        fullpath = fullpath.Replace(x.MatchCharacter, x.ReplaceCharacter);
                    }
                    catch (Exception e) { }
                }
            }

            return fullpath + extension;
        }


        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                );
        }

        private bool HasEditPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
                );
        }


        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        }

        #endregion

    }
}
