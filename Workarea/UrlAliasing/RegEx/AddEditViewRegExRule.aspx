﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditViewRegExRule.aspx.cs"
    Inherits="Workarea_UrlAliasing_RegEx_AddEditViewRegExRule" meta:resourcekey="PageResource1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <script type="text/javascript" language="javascript">
            function RollOver(MyObj) {
                $ektron(MyObj).parent().addClass("button-over");
                $ektron(MyObj).parent().removeClass("button");
            }

            function RollOut(MyObj) {
                $ektron(MyObj).parent().addClass("button");
                $ektron(MyObj).parent().removeClass("button-over");
            }

            Ektron.ready(function () {
                $ektron('#selExpDialog').modal({
                    trigger: '.viewSelectExpTrigger',
                    modal: true,
                    onShow: function (hash) {
                        hash.o.fadeTo("fast", 0.5, function () {
                            hash.w.fadeIn("fast");
                        });
                    },
                    onHide: function (hash) {
                        hash.w.fadeOut("fast");
                        hash.o.fadeOut("fast", function () {
                            if (hash.o)
                                hash.o.remove();
                        });
                    }
                });
            });

            function selectExpMap(expressionMap, expression, reqURL) {

                $ektron('#selExpDialog').modalHide();
                var obj = document.getElementById("uxExpressionMapValue");
                obj.value = expressionMap;
                var obj1 = document.getElementById("uxExpressionValue");
                obj1.value = expression;
                var obj3 = document.getElementById("uxExampleURLValue");
                obj3.value = reqURL;

            }
    </script>
    <style type="text/css" >
       div.ektronWindow 
       {
           width: 50em;
           margin-left:-25em;
       }
    div.ektronWindow h3 {
        background-color:#3163BD;
        background-image:url(../../images/application/darkblue_gradiant-nm.gif);
        background-position:0pt -2px;
        background-repeat:repeat-x;
        color:#FFFFFF;
        font-size:1em;
        margin:0pt;
        padding:0.6em 0.25em;
        position:relative;
    }
    div.ektronWindow h3 a.ektronModalClose {
        background-color:transparent;
        background-image:url(../../images/application/closeButton.gif);
        background-position:0px -23px;
        text-indent:-10000px;
        background-repeat:no-repeat;
        display:block;
        height:21px;
        overflow:hidden;
        position:absolute;
        right:0.25em;
        text-decoration:none;
        top:0.25em;
        width:21px;
        color:#FFFFFF;
}
    th.title-header 
    {
        font-weight:bold;
    }
</style>
</head>
<body>
    <form id="form_regexrule" runat="server">
            <div class="ektronWindow" style="overflow: auto;" id="selExpDialog">
            <div class="ektronModalHeader">
                <h3>
                    <asp:Literal ID="lblExpressionLib" runat="server" />
                    <asp:HyperLink ID="closeDialogLink" CssClass="ektronModalClose" runat="server" />
                </h3>
            </div>
            <div style="width: 100%; height: auto; float: left; overflow: auto;">
                <div class="ektronPageGrid">
                        <asp:GridView ID="regExPicker"
                        runat="server"
                        AutoGenerateColumns="False"
                        Width="100%"
                        EnableViewState="False"
                        GridLines="None">
                        <HeaderStyle CssClass="title-header" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    <ektronUI:JavaScript ID="aliasJS" runat="server" Path="../js/ektron.workarea.urlaliasing.js" />
    <ektronUI:Css ID="aliasCSS" Path="../CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                    <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSaveButton"
                                class="primary saveButton" ValidationGroup="ValidateData" OnClick="uxSaveButton_Click" />
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" meta:resourcekey="uxEditButton"
                                OnClick="uxEditButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="uxDelete" class="primary deleteButton" meta:resourcekey="uxDelete"
                                OnClick="uxDelete_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <table class="ektronForm">
            <tr>
                <td class="label">
                    <asp:Label ID="uxActiveLabel" runat="server" meta:resourcekey="uxActiveLabel" Enabled='<%#this.IsEditable %>' />
                </td>
                <td class="value">
                    <asp:CheckBox ID="uxActiveCheckBox" runat="server" meta:resourcekey="uxActiveCheckBox"
                        Enabled='<%#this.IsEditable %>' />
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                </td>
                <td class="value">
                    <asp:DropDownList ID="uxSiteList" runat="server" meta:resourcekey="uxSiteList">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <fieldset id="uxRuleInfoContainer" runat="server">
            <legend>
                <asp:Literal ID="uxDefineRuleTitle" runat="server" meta:resourcekey="uxDefineRuleTitle"></asp:Literal></legend>
            <table class="ektronForm urlAliasSettings">
                <tr>
                    <td class="label">
                        <asp:Label ID="uxExpressionNameLabel" runat="server" meta:resourcekey="uxExpressionNameLabel" />
                    </td>
                    <td class="value">
                        <asp:TextBox ID="uxExpressionNameValue" runat="server" meta:resourcekey="uxExpressionNameValue"
                            Enabled='<%#this.IsEditable %>' />
                    </td>
                </tr>
                 <tr>
                    <td class="label">
                        <asp:Label ID="uxExpressionLabel" runat="server" meta:resourcekey="uxExpressionLabel" />
                    </td>
                    <td class="value">
                        <asp:TextBox ID="uxExpressionValue" runat="server" meta:resourcekey="uxExpressionValue"
                            Enabled='<%#this.IsEditable %>' />
                            <a href="#" class="viewSelectExpTrigger" title="<%=msgHelper.GetMessage("lbl select exp")%>"><%=msgHelper.GetMessage("lbl select exp")%></a>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="uxExpressionMapLabel" runat="server" meta:resourcekey="uxExpressionMapLabel" />
                    </td>
                    <td class="value">
                        <asp:TextBox ID="uxExpressionMapValue" runat="server" meta:resourcekey="uxExpressionMapValue"
                            Enabled='<%#this.IsEditable %>' />
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="uxExampleURLLabel" runat="server" meta:resourcekey="uxExampleURLLabel" />
                    </td>
                    <td class="value">
                        <asp:TextBox ID="uxExampleURLValue" runat="server" meta:resourcekey="uxExampleURLValue"
                            Enabled='<%#this.IsEditable %>' />
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="uxPriorityLabel" runat="server" meta:resourcekey="uxPriorityLabel" />
                    </td>
                    <td class="value">
                        <asp:DropDownList ID="ddlPriority" runat="server" meta:resourcekey="uxPriorityValue"
                            Enabled='<%#this.IsEditable %>'></asp:DropDownList>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    </form>
</body>
</html>
