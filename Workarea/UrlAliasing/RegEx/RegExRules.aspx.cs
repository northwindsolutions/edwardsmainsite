﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Settings;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;
    using Ektron.Cms.UrlAliasing;

    public partial class RegExRules : Ektron.Cms.Workarea.Page
    {
        #region variables

        private int pageSize = 20;
        private SiteAPI siteAPI = new SiteAPI();
        private ISite site;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private List<UrlAliasRegExData> aliasList;
        private RegExAliasManager aliasManager = new RegExAliasManager();
        private RegExAliasCriteria criteria;

        public bool IsEditable { get { return (ViewState["IsEditable"] != null) ? (bool)ViewState["IsEditable"] : false; } set { ViewState["IsEditable"] = value; } }
        [System.ComponentModel.DefaultValue(false)]
        public bool showMessage { get; set; }

        #endregion

        #region page events

        protected override void OnInit(EventArgs e)
        {
            uxBackButton.Visible = false;
            if (this.HasViewPermission())
            {
                try
                {
                    base.OnInit(e);
                    this.pageSize = siteAPI.RequestInformationRef.PagingSize;
                    this.initWorkareaUI();
                    if (!Page.IsPostBack)
                    {
                        this.bindUIOptions();
                    }

                    if (IsFeatureEnabled())
                    {
                        uxAddButton.Visible = HasEditPermission();
                        uxEditButton.Visible = HasEditPermission();
                        criteria = this.getCriteria();
                        if (!Page.IsPostBack)
                        {
                            this.bindData();
                        }
                    }
                    else
                    {
                        uxAddButton.Visible = false;
                        uxEditButton.Visible = false;
                        uxRegExList.Visible = false;
                        showMessage = true;
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                        uxMessage.Text = GetLocalResourceObject("FeatureDisabledMessage").ToString();
                    }

                }
                catch (Exception Exc)
                {
                    //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                    Utilities.ShowError(Exc.Message + Exc.StackTrace);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        //protected override void OnLoad(EventArgs e)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception Exc)
        //    {
        //        //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
        //        Utilities.ShowError(Exc.Message + Exc.StackTrace);
        //    }
        //}


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.DataBind();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {

            // Activate
            Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField enableColumn = uxRegExList.Columns[1] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
            enableColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
            {
                string originalState = dirtyField.OriginalState;
                string dirtyState = dirtyField.DirtyState;
                if (originalState != dirtyState)
                {
                    string id = dirtyField.KeyFields["Id"];
                    long ruleID = long.Parse(id);
                    UrlAliasRegExData rule = aliasManager.GetItem(ruleID);
                    rule.IsEnabled = !rule.IsEnabled;
                    aliasManager.Update(rule);
                }
            });

            // Delete
            Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField deleteColumn = uxRegExList.Columns[0] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
            deleteColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
            {
                string originalState = dirtyField.OriginalState;
                string dirtyState = dirtyField.DirtyState;
                if (originalState != dirtyState)
                {
                    string id = dirtyField.KeyFields["Id"];
                    long ruleID = long.Parse(id);
                    aliasManager.Delete(ruleID);
                }
            });

            IsEditable = false;
            this.bindData();

            uxSaveButton.Visible = false;
            uxBackButton.Visible = false;
            uxEditButton.Visible = HasEditPermission();
            uxAddButton.Visible = HasEditPermission();

            uxSiteList.Enabled = true;
            uxSearchTextBox.Enabled = true;
            uxSearchButton.Enabled = true;
            uxSearchReset.Enabled = true;
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            IsEditable = true;
            this.criteria = this.getCriteria();
            this.bindData();

            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxBackButton.Visible = true;
            uxSiteList.Enabled = false;
            uxSearchTextBox.Enabled = false;
            uxSearchButton.Enabled = false;
            uxAddButton.Visible = false;
            uxSearchReset.Enabled = false;
        }

        protected void filterChange(object sender, EventArgs e)
        {
            try
            {	criteria = this.getCriteria();
                this.bindData();
            }
            catch (Exception Exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(Exc.Message + Exc.StackTrace);
            }
        }

        protected void uxAutoRulesGridView_EktronUISortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            try
            {
                this.criteria.OrderByField = (RegExAliasProperty)Enum.Parse(typeof(RegExAliasProperty), e.OrderByFieldText);
                this.criteria.OrderByDirection = e.OrderByDirection;
                this.bindData();
            }
            catch (Exception Exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(Exc.Message + Exc.StackTrace);
            }
        }

        protected void uxRegExRulesGridView_EktronUIPageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByDirection = uxRegExList.EktronUIOrderByDirection;
                this.criteria.OrderByField = (RegExAliasProperty)Enum.Parse(typeof(RegExAliasProperty), uxRegExList.EktronUIOrderByFieldText);

                this.bindData();
            }
            catch (Exception Exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(Exc.Message + Exc.StackTrace);
            }
        }

        #endregion

        #region internal methods

        private bool IsFeatureEnabled()
        {
            AliasSettingsManager urlAliasSettingsManager = new AliasSettingsManager();
            AliasSettings urlAliasingSettingsData = urlAliasSettingsManager.Get();
            urlAliasingSettingsData = urlAliasSettingsManager.Get();
            return urlAliasingSettingsData.IsRegExAliasingEnabled && urlAliasingSettingsData.IsAliasingEnabled;
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();
            StyleHelper styleHelper = new StyleHelper();
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "UrlAliasing/regex/regexrules.aspx";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
            aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_rules_regex_list", string.Empty);
        }

        private void bindUIOptions()
        {
            site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
            bindSiteList();
        }

        private void bindSiteList()
        {
            Dictionary<long, string> siteList = site.GetSiteList();
            var displaySites = from sites in siteList
                               select new { Label = sites.Value, ID = sites.Key };
            uxSiteList.Items.Clear();
            uxSiteList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
            uxSiteList.DataSource = displaySites;
            uxSiteList.DataTextField = "Label";
            uxSiteList.DataValueField = "ID";
        }

        private void bindData()
        {
            aliasList = aliasManager.GetList(this.criteria);
            if (aliasList != null && aliasList.Count > 0)
            {
                uxRegExList.DataSource = aliasList;
                uxRegExList.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxRegExList.EktronUIOrderByFieldText = this.criteria.OrderByField.ToString();
            }
            else
            {
                uxRegExList.DataSource = null;
                uxEditButton.Visible = false;
                this.showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Warning;
                uxMessage.Text = GetLocalResourceObject("NoData").ToString();
            }
        }

        private RegExAliasCriteria getCriteria()
        {
            RegExAliasCriteria regExCriteria = new RegExAliasCriteria();

            if (!String.IsNullOrEmpty(uxSiteList.SelectedValue))
            {
                long siteId = long.Parse(uxSiteList.SelectedValue);
                if (siteId != -1)
                {
                    regExCriteria.AddFilter(RegExAliasProperty.SiteId, CriteriaFilterOperator.EqualTo, siteId);
                }
            }

            string searchText = uxSearchTextBox.Text;
            if (!String.IsNullOrEmpty(searchText) && searchText != "Search")
            {
                regExCriteria.AddFilter(RegExAliasProperty.ExpressionName, CriteriaFilterOperator.Contains, searchText);
            }

            regExCriteria.OrderByDirection = uxRegExList.EktronUIOrderByDirection;
            if (!string.IsNullOrEmpty(uxRegExList.EktronUIOrderByFieldText))
            {
                regExCriteria.OrderByField = (RegExAliasProperty)Enum.Parse(typeof(RegExAliasProperty), uxRegExList.EktronUIOrderByFieldText);
            }
            else
            {
                uxRegExList.EktronUIOrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                regExCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                regExCriteria.OrderByField = RegExAliasProperty.ExpressionName;
            }

            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = pageSize;
            regExCriteria.PagingInfo = pagingInfo;

            return regExCriteria;
        }

        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 
                );
        }

        private bool HasEditPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
                );
        }

        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
        }
        #endregion

    }
}