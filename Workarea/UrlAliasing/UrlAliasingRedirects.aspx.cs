﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Settings;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;



    public partial class UrlAliasingRedirects : Ektron.Cms.Workarea.Page
    {
        #region Variables

        private int pageSize = 20;
        private SiteAPI siteAPI = new SiteAPI();
        private ISite site;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private RedirectManager redirectManager = new RedirectManager();
        private List<RedirectData> redirectList;
        private RedirectCriteria criteria;
        private bool isEnabled = true;

        public bool IsEditable { get { return (ViewState["IsEditable"] != null) ? (bool)ViewState["IsEditable"] : false; } set { ViewState["IsEditable"] = value; } }
        [System.ComponentModel.DefaultValue(false)]
        public bool showMessage { get; set; }

        #endregion

        #region page events


        protected override void OnInit(EventArgs e)
        {
            if (this.HasViewPermission())
            {
                base.OnInit(e);
                this.initWorkareaUI();
                if (!Page.IsPostBack)
                {
                    this.setViewUI(this.IsEnabled());
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                criteria = this.getCriteria();
                if (!Page.IsPostBack && IsEnabled())
                {
                    this.bindData();
                }
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.DataBind();
        }

        protected void filterChange(object sender, EventArgs e)
        {
            try
            {
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }


        protected void uxRedirectListGridView_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            try
            {
                this.criteria.OrderByField = (RedirectProperty)Enum.Parse(typeof(RedirectProperty), e.OrderByFieldText);
                this.criteria.OrderByDirection = e.OrderByDirection;
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void uxRedirectListGridView_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByDirection = uxRedirectListGridView.EktronUIOrderByDirection;
                this.criteria.OrderByField = (RedirectProperty)Enum.Parse(typeof(RedirectProperty), uxRedirectListGridView.EktronUIOrderByFieldText);

                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            this.setEditUI();
            this.bindData();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Activate
                Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField enableColumn = uxRedirectListGridView.Columns[1] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                enableColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                {
                    string originalState = dirtyField.OriginalState;
                    string dirtyState = dirtyField.DirtyState;
                    if (originalState != dirtyState)
                    {
                        string id = dirtyField.KeyFields["Id"];
                        long redirectID = long.Parse(id);
                        RedirectData redirect = redirectManager.GetItem(redirectID);
                        redirect.Active = !redirect.Active;
                        redirectManager.Update(redirect);
                    }
                });

                // Delete
                Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField deleteColumn = uxRedirectListGridView.Columns[0] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
                deleteColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                {
                    string originalState = dirtyField.OriginalState;
                    string dirtyState = dirtyField.DirtyState;
                    if (originalState != dirtyState)
                    {
                        string id = dirtyField.KeyFields["Id"];
                        long redirectID = long.Parse(id);
                        redirectManager.Delete(redirectID);
                    }
                });

                IsEditable = false;
                this.setViewUI(true);
                this.bindData();
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
            this.setViewUI(true);
        }

        #endregion

        #region methods

        private void bindData()
        {
            redirectList = redirectManager.GetList(this.criteria);
            if (redirectList != null && redirectList.Count > 0)
            {
                uxRedirectListGridView.DataSource = redirectList;
                uxRedirectListGridView.EktronUIPagingInfo = this.criteria.PagingInfo;
                uxRedirectListGridView.EktronUIOrderByFieldText = this.criteria.OrderByField.ToString();
                uxRedirectListGridView.DataBind();
            }
            else
            {
                uxRedirectListGridView.DataSource = null;
                uxRedirectListGridView.DataBind();
                uxEditButton.Visible = false;
                this.showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Warning;
                uxMessage.Text = GetLocalResourceObject("NoData").ToString();
            }
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            StyleHelper styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_redirects_list", string.Empty);
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "UrlAliasing/urlaliasingredirects.aspx";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
            this.showMessage = false;
        }

        private void setViewUI(bool isEnabled)
        {
            this.bindUIOptions();
            if (isEnabled)
            {
                uxEditButton.Visible = this.HasEditPermission();
                uxAddButton.Visible = this.HasEditPermission();
                uxSaveButton.Visible = false;
                uxBackButton.Visible = false;

                uxSiteList.Enabled = true;
                uxTypeList.Enabled = true;
                uxSearchTextBox.Enabled = true;
                uxSearchButton.Enabled = true;
                uxSearchReset.Enabled = true;
            }
            else
            {
                uxAddButton.Visible = false;
                uxSiteList.Enabled = false;
                uxTypeList.Enabled = false;
                uxSearchTextBox.Enabled = false;
                uxSearchButton.Enabled = false;
                uxSearchReset.Enabled = false;
                showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxMessage.Text = GetLocalResourceObject("FeatureDisabledMessage").ToString();
            }
        }

        private void setEditUI()
        {
            this.IsEditable = true;

            uxBackButton.Visible = true;
            uxAddButton.Visible = false;
            uxEditButton.Visible = false;
            uxSaveButton.Visible = true;
            uxSiteList.Enabled = false;
            uxTypeList.Enabled = false;
            uxSearchTextBox.Enabled = false;
            uxSearchButton.Enabled = false;
            uxSearchReset.Enabled = false;
        }


        private void bindUIOptions()
        {
            site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
            bindSiteList();
            bindTypeList();
        }

        private void bindTypeList()
        {
            uxTypeList.Items.Clear();
            uxTypeList.Items.Add(new ListItem("All", "-1"));
            uxTypeList.Items.Add(new ListItem(HttpStatusCode.OK.GetHashCode().ToString(), HttpStatusCode.OK.GetHashCode().ToString()));
            uxTypeList.Items.Add(new ListItem(HttpStatusCode.MovedPermanently.GetHashCode().ToString(), HttpStatusCode.MovedPermanently.GetHashCode().ToString()));
            uxTypeList.Items.Add(new ListItem(HttpStatusCode.Redirect.GetHashCode().ToString(), HttpStatusCode.Redirect.GetHashCode().ToString()));
            uxTypeList.Items.Add(new ListItem(HttpStatusCode.NotFound.GetHashCode().ToString(), HttpStatusCode.NotFound.GetHashCode().ToString()));
        }

        private void bindSiteList()
        {
            uxSiteList.Items.Clear();
            Dictionary<long, string> siteList = site.GetSiteList();
            var displaySites = from sites in siteList
                               select new { Label = sites.Value, ID = sites.Key };
            uxSiteList.DataSource = displaySites;
            uxSiteList.DataTextField = "Label";
            uxSiteList.DataValueField = "ID";
            uxSiteList.DataBind();
            uxSiteList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
        }

        private RedirectCriteria getCriteria()
        {
            RedirectCriteria criteria = new RedirectCriteria();
            this.pageSize = siteAPI.RequestInformationRef.PagingSize;
            long siteID = 0;
            long.TryParse(uxSiteList.SelectedValue, out siteID);
            if (siteID != -1)
            {
                criteria.AddFilter(RedirectProperty.SiteId, CriteriaFilterOperator.EqualTo, siteID);
            }

            long statusCode = 0;
            long.TryParse(uxTypeList.SelectedValue, out statusCode);
            if (statusCode != -1)
            {
                criteria.AddFilter(RedirectProperty.StatusCode, CriteriaFilterOperator.EqualTo, statusCode);
            }

            string searchText = uxSearchTextBox.Text;
            if (!String.IsNullOrEmpty(searchText) && searchText != "Search")
            {
                CriteriaFilterGroup<RedirectProperty> searchVars = new CriteriaFilterGroup<RedirectProperty>();
                searchVars.Condition = LogicalOperation.Or;
                searchVars.AddFilter(RedirectProperty.SourceURL, CriteriaFilterOperator.Contains, searchText);
                searchVars.AddFilter(RedirectProperty.Target, CriteriaFilterOperator.Contains, searchText);
                criteria.FilterGroups.Add(searchVars);
            }

            criteria.OrderByDirection = uxRedirectListGridView.EktronUIOrderByDirection;
            if (!string.IsNullOrEmpty(uxRedirectListGridView.EktronUIOrderByFieldText))
            {
                criteria.OrderByField = (RedirectProperty)Enum.Parse(typeof(RedirectProperty), uxRedirectListGridView.EktronUIOrderByFieldText);
            }
            else
            {
                uxRedirectListGridView.EktronUIOrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByField = RedirectProperty.SourceURL;
            }


            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = pageSize;
            criteria.PagingInfo = pagingInfo;

            return criteria;
        }

        private bool IsEnabled()
        {
            bool returnVal = true;

            AliasSettingsManager urlAliasSettingsManager = new AliasSettingsManager();
            AliasSettings urlAliasingSettingsData = urlAliasSettingsManager.Get();
            returnVal = urlAliasingSettingsData.IsURLRedirectEnabled;

            return returnVal;
        }

        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                );
        }

        private bool HasEditPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
                );
        }

        protected string CleanDisplayString(string url)
        {
            if (url.IsValueNullOrEmpty()) return "(Site Root)";
            return url;
        }

        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        }

        #endregion

    }
}