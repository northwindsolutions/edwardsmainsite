﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.Script.Serialization;
    using System.Linq;
    using Ektron.Cms;
    using Ektron.Cms.BusinessObjects.Caching.Settings;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;

    /// <summary>
    /// URL Aliasing Settings
    /// </summary>
    public partial class UrlAliasingSettings : Ektron.Cms.Workarea.Page
    {
        #region Variables

        private EkMessageHelper msgHelper;
        private int cb;
        private ContentAPI refContentApi;
        private AliasSettings urlAliasingSettingsData;
        private List<FileExtension> aliasExtensions;
        private AliasSettingsManager urlAliasSettingsManager;

        [DefaultValue(false)]
        public bool showMessage { get; set; }
        [DefaultValue(false)]
        public bool IsEditable { get; set; }
        [DefaultValue(false)]
        public bool IsAliasEditable { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Use this event to read or initialize control properties
        /// </summary>
        /// <param name="e"> EventArgs e</param>
        protected override void OnInit(EventArgs e)
        {
            if (this.HasPermission())
            {
                base.OnInit(e);
                refContentApi = new ContentAPI();
                urlAliasSettingsManager = new AliasSettingsManager();
                urlAliasingSettingsData = urlAliasSettingsManager.Get();
                aliasExtensions = urlAliasSettingsManager.GetAllExtensions();
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        /// <summary>
        /// Use the OnLoad event method to set properties in controls.
        /// </summary>
        /// <param name="e">EventArgs e</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.msgHelper = this.refContentApi.EkMsgRef;
            this.RegisterResources();
            StyleHelper styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_settings", string.Empty);
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "UrlAliasing/UrlAliasingSettings.aspx?action=view";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsPostBack)
            {
                try
                {
                    this.uxFileExtension.Attributes.Add("disabled", "");
                    this.ViewURLAliasingSettings();
                }
                catch (Exception exc)
                {
                    //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                    Utilities.ShowError(exc.Message);
                }
            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            this.IsEditable = true;
            if (!urlAliasingSettingsData.IsAliasingEnabled)
            {
                this.IsAliasEditable = false;
            }
            else
            {
                this.IsAliasEditable = true;
                this.uxFileExtension.Attributes.Remove("disabled");
            }
            this.DataBind();
            this.uxEditButton.Visible = false;
            this.uxSaveButton.Visible = this.uxextensionsec.Visible = true;
            this.clearCacheButton.Visible = false;
            this.uxBackButton.Visible = true;
            this.uxMessage.Visible = false;

        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (!uxGlobalEnableCheckBox.Checked || (uxGlobalEnableCheckBox.Checked && validateReplacementChar(uxDefaultReplacementChar.Text)))
                {
                    this.IsEditable = this.uxextensionsec.Visible = false;
                    this.IsAliasEditable = false;
                    this.DataBind();

                    try
                    {
                        this.UpdateURLAliasingSettings();
                        this.uxFileExtension.Attributes.Add("disabled", "");
                        urlAliasingSettingsData = urlAliasSettingsManager.Get();
                        aliasExtensions = urlAliasSettingsManager.GetAllExtensions();
                        this.showMessage = true;
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                        uxMessage.Text = GetLocalResourceObject("SettingsSaved").ToString();
                        JavaScript.RegisterJavaScriptBlock(this, "Ektron.Workarea.UrlAliasing.HideMessage();Ektron.Workarea.UrlAliasing.RefreshTreeAfterSettings();");
                        this.ViewURLAliasingSettings();
                    }
                    catch (Exception exc)
                    {
                        //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                        //Utilities.ShowError(exc.Message);
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                        this.showMessage = true;
                        uxMessage.Text = exc.Message;
                        this.IsEditable = this.uxextensionsec.Visible = true;
                        this.IsAliasEditable = true;
                        this.DataBind();
                        return;
                    }
                }
                else
                {
                    this.IsEditable = true;
                    this.showMessage = true;
                    this.IsAliasEditable = true;
                    uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    uxMessage.Text = GetLocalResourceObject("errorReplacementChar").ToString();
                    this.DataBind();
                }
            }

            // Clear Cache on UPDATE settings.
            urlAliasSettingsManager.ClearCache(CacheSegmentKeys.Ektron_UrlAliasing);
            urlAliasSettingsManager.ClearCache(CacheSegmentKeys.Ektron_UrlRedirects);
        }



        protected void ClearCacheButton_Click(object sender, EventArgs e)
        {
            urlAliasSettingsManager.ClearCache(CacheSegmentKeys.Ektron_UrlAliasing);
            urlAliasSettingsManager.ClearCache(CacheSegmentKeys.Ektron_UrlRedirects);
            this.showMessage = true;
            uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
            uxMessage.Text = GetLocalResourceObject("CacheCleared").ToString();
            JavaScript.RegisterJavaScriptBlock(this, "Ektron.Workarea.UrlAliasing.HideMessage();");
            this.ViewURLAliasingSettings();
        }

        #endregion

        #region Display

        /// <summary>
        /// Display View URL Aliasing Settings
        /// </summary>
        protected void ViewURLAliasingSettings()
        {

            this.IsEditable = false;
            this.IsAliasEditable = this.urlAliasingSettingsData.IsAliasingEnabled;

            this.uxEditButton.Visible = true;
            this.uxSaveButton.Visible = false;
            this.uxBackButton.Visible = false;
            this.clearCacheButton.Visible = true;

            // View Enabled Properties
            this.uxGlobalEnableCheckBox.Checked = this.urlAliasingSettingsData.IsAliasingEnabled;
            this.uxRedirectsEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsURLRedirectEnabled;
            this.uxManualAliasEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsManualAliasingEnabled;
            this.uxFolderAliasEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsFolderAliasingEnabled;
            this.uxTaxonomyAliasEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsTaxonomyAliasingEnabled;
            this.uxUserAliasEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsUserAliasingEnabled;
            this.uxGroupAliasEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsGroupAliasingEnabled;
            this.uxRegExAliasEnabledCheckBox.Checked = this.urlAliasingSettingsData.IsRegExAliasingEnabled;


            // View Advance Url Aliasing Settings.
            uxFileExtension.DataSource = this.aliasExtensions;
            this.uxOverrideTemplateCheckBox.Checked = this.urlAliasingSettingsData.OverrideTemplate;
            this.uxFallBackLangCheckBox.Checked = this.urlAliasingSettingsData.IsAliasFallbackEnabled;
            this.uxQueryStringActionList.SelectedValue = this.urlAliasingSettingsData.QueryStringAction.GetHashCode().ToString();
            this.uxDefaultReplacementChar.Text = this.urlAliasingSettingsData.DefaultReplacementCharacter;
            this.DataBind();
        }

        /// <summary>
        /// Update URL Aliasing Settings
        /// </summary>
        protected void UpdateURLAliasingSettings()
        {
            AliasSettings newSettings = this.urlAliasingSettingsData;
            // Update Enabled Properties
            newSettings.IsAliasingEnabled = this.uxGlobalEnableCheckBox.Checked;
            newSettings.IsURLRedirectEnabled = this.uxRedirectsEnabledCheckBox.Checked;

            if (newSettings.IsAliasingEnabled)
            {
                newSettings.IsManualAliasingEnabled = this.uxManualAliasEnabledCheckBox.Checked;
                newSettings.IsFolderAliasingEnabled = this.uxFolderAliasEnabledCheckBox.Checked;
                newSettings.IsTaxonomyAliasingEnabled = this.uxTaxonomyAliasEnabledCheckBox.Checked;
                newSettings.IsUserAliasingEnabled = this.uxUserAliasEnabledCheckBox.Checked;
                newSettings.IsGroupAliasingEnabled = this.uxGroupAliasEnabledCheckBox.Checked;
                newSettings.IsRegExAliasingEnabled = this.uxRegExAliasEnabledCheckBox.Checked;

                // Update Advance Url Aliasing Settings.
                newSettings.OverrideTemplate = this.uxOverrideTemplateCheckBox.Checked;
                newSettings.IsAliasFallbackEnabled = this.uxFallBackLangCheckBox.Checked;
                newSettings.QueryStringAction = (EkEnumeration.QueryStringActionType)Enum.Parse(typeof(EkEnumeration.QueryStringActionType), this.uxQueryStringActionList.SelectedValue.ToString());
                newSettings.DefaultReplacementCharacter = this.uxDefaultReplacementChar.Text;

                // Clone others
                //newSettings.DefaultReplaceCharList = this.urlAliasingSettingsData.DefaultReplaceCharList;
                //newSettings.DefaultReplaceCharString = this.urlAliasingSettingsData.DefaultReplaceCharString;
                //newSettings.SiteID = this.urlAliasingSettingsData.SiteID;
            }
            this.urlAliasSettingsManager.Update(newSettings);

            var aJsSerializer = new JavaScriptSerializer();
            List<FileExtension> clientExtensionData = aJsSerializer.Deserialize<List<FileExtension>>(uxExtensionsHidden.Value);
            // delete
            if (clientExtensionData != null)
            {
                var toDelete = (from ed in aliasExtensions
                                where !clientExtensionData.Contains(ed)
                                select ed).ToList();
                foreach (FileExtension ext in toDelete)
                {
                    urlAliasSettingsManager.DeleteExtension(ext.Id);
                    clientExtensionData.Remove(ext);
                }
                //add
                foreach (FileExtension extension in clientExtensionData)
                {
                    if (!aliasExtensions.Contains(extension))
                    {
                        if (!extension.Extension.StartsWith(".")) { extension.Extension = "." + extension.Extension; }

                        urlAliasSettingsManager.AddFileExtension(extension);
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access
        /// the functionality on this page, false otherwise.
        /// </summary>
        /// <returns>True if permissions are sufficient, false otherwise</returns>
        private bool HasPermission()
        {
            return
            (
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                ) &&
                (
                    ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0)
                    || ContentAPI.Current.EkUserRef.IsARoleMember((long)EkEnumeration.CmsRoleIds.UrlAliasingAdmin, ContentAPI.Current.UserId, false)
                )
            );
        }

        private bool validateReplacementChar(string defaultChar)
        {
            bool isvalid = false;
            string badChars = @"[\<>:|?]'%.#* &+";
            if (!String.IsNullOrEmpty(defaultChar))
            {
                isvalid = true;

                if (defaultChar.Length > 1)
                {
                    isvalid = false;
                }

                if (badChars.Contains(defaultChar))
                {
                    isvalid = false;
                }
            }

            return isvalid;
        }
        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Packages.Ektron.JSON.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        }

        #endregion
    }
}