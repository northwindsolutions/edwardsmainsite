﻿if ("undefined" == typeof(Ektron)) { Ektron = {}; }
if ("undefined" == typeof(Ektron.WebCalendar)) { Ektron.WebCalendar = {}; }
if (Ektron.WebCalendar.Popup === undefined) {
    Ektron.WebCalendar.Popup = {
        FixPopup: function (sender, eventArgs) {
            var popup = $ektron(sender.get_popupElement());
            var parent = $ektron(sender.get_targetControl());
            var newoffset = popup.offset();
            popup.mouseenter(function () { popup.addClass("mouseOver"); });
            popup.mouseleave(function () { popup.removeClass("mouseOver"); });
            newoffset.top = parent.offset().top + parent.height();
            popup.offset(newoffset);
        },
        OnClientBeforeHide: function (sender, eventArgs) {
            var popup = $ektron(sender.get_popupElement());
            var parent = $ektron(sender.get_targetControl());
            if (popup.hasClass("mouseOver")) { eventArgs.set_cancel(true); }
        }
    };
}
