<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumTopicsAndPosts.ascx.cs" Inherits="Workarea_Widgets_ForumTopicsAndPosts" %>

<style type="text/css">
    #forumEditLabel, #ForumTopicsAndPostsEditButtons
    {
    	margin: 10px;
    }
</style>

<div class="ForumTopicsAndPostsWidget">
    <asp:MultiView ID="ForumTopicsAndPostsViews" runat="server" ActiveViewIndex="0">
        <asp:View ID="EditView" runat="server">
            <div id="editContainer" class="ForumTopicsAndPostsWidget_Edit" runat="server" >
                <div id="forumEditLabel">Board ID: <asp:TextBox ID="BoardID" runat="server" /><br /></div>
                <div id="ForumTopicsAndPostsEditButtons">
                    <asp:Button ToolTip="Cancel" ID="uxCancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
                    &nbsp;&nbsp; 
                    <asp:Button ToolTip="Save" ID="uxSaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </asp:View>  
        <asp:View ID="DisplayView" runat="server">
            <asp:Label ToolTip="No Records" ID="lblNoRecords" Visible="false" runat="server">
            <asp:literal ID="ltrlNoRecords" runat="server" /></asp:Label>
            <asp:Literal ID="ltr_forum_mod" runat="Server" />
        </asp:View>
    </asp:MultiView>
</div>