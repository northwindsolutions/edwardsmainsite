#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : This page will dispaly the Freeway Dashboard in User Profile Dashboard.
---------------------------------
*/
#endregion
#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.DataIO.LicenseManager;
using System.Text;
using System.Data;
#endregion

public partial class widgets_FreewayDashboard : System.Web.UI.UserControl, IWidget
{


    #region Private & Protected variables
    StyleHelper m_refStyle = new StyleHelper();
    protected string StyleSheetJS = "";
    private const string UserDisplayFormat = "{0} ({1}, {2})";
    #endregion

    #region Public Properties
    /// <summary>
    /// Manages the sorting of project grid based on the Project ID's
    /// </summary>
    public string SortingField
    {
        get
        {
            if (ViewState["SortingField"] == null)
                return "Id";

            return ViewState["SortingField"] as string;
        }
        set { ViewState["SortingField"] = value; }
    }

    /// <summary>
    /// Manages the sorting of project grid based on the Project ID's
    /// </summary>
    public bool SortingOrder
    {
        get
        {
            if (ViewState["SortingOrder"] == null)
                return false;

            return (bool)ViewState["SortingOrder"];
        }
        set { ViewState["SortingOrder"] = value; }
    }
    #endregion



    #region Page Events

    /// <summary>
    /// Adds the users and projects in respective list  
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        UserAPI userApi = new UserAPI();
        if (userApi.IsARoleMember(EkEnumeration.CmsRoleIds.AdminXliff))
        {
            //Assigns the stylesheet to the header of the page
            StyleSheetJS = m_refStyle.GetClientScript();

            // If the page is first time visited then add the users and projects in respective list  
            if (!IsPostBack)
            {

                WidgetDataRefresh();
            }

            //WidgetDataRefresh();
        }
        else
        {
            StringBuilder sberror = new StringBuilder();
            sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
            sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Rights. <br> Please contact Freeway Administrator.</td>");

            sberror.Append("</tr></table>");
            lblMessage.Text = sberror.ToString();
            pnlDashboard.Visible = false;
        }
        return;
    }

    private void WidgetDataRefresh()
    {
        //Assigns the stylesheet to the header of the page
        //StyleSheetJS = m_refStyle.GetClientScript();

        // If the page is first time visited then add the users and projects in respective list  
        //if (!IsPostBack)
        //{


        //Clears User drop down list
        _userSelect.Items.Clear();

        // Clears all or the available project in list
        _projectCount.Items.Clear();
        _projectCount.Items.Clear();
        _projectCount.Items.Clear();
        _projectCount.Items.Clear();
        _projectCount.Items.Clear();


        // Displays all or the available project in list
        _projectCount.Items.Add(new ListItem("10", "10"));
        _projectCount.Items.Add(new ListItem("25", "25"));
        _projectCount.Items.Add(new ListItem("50", "50"));
        _projectCount.Items.Add(new ListItem("100", "100"));
        _projectCount.Items.Add(new ListItem("All", "All"));

        // Retrieves the users from Ektron and fill the user dropdown list
        UserAPI userApi = new UserAPI();

        try
        {

            if (userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers, userApi.UserId, false)
                || FreewayConfiguration.IsExecutiveUser)
            {
                try
                {
                    UserData[] users = userApi.GetAllCmsUsers("username");

                    foreach (UserData user in users)
                    {
                        _userSelect.Items.Add(new ListItem(string.Format(UserDisplayFormat, user.Username, user.LastName, user.FirstName, user.Id), user.Username));

                        if (userApi.UserId == user.Id)
                            _userSelect.SelectedValue = user.Username;
                    }

                }
                catch
                {
                    UserData currentUser = userApi.UserObject(userApi.UserId);

                    _userSelect.Items.Add(new ListItem(string.Format(UserDisplayFormat, currentUser.Username, currentUser.LastName, currentUser.FirstName, currentUser.Id), currentUser.Username));

                }

                _userSelect.Items.Add(new ListItem("All Users", string.Empty));

            }
            else
            {

                UserData currentUser = userApi.UserObject(userApi.UserId);

                _userSelect.Items.Add(new ListItem(string.Format(UserDisplayFormat, currentUser.Username, currentUser.LastName, currentUser.FirstName, currentUser.Id), currentUser.Username));

            }
        }
        catch
        {

            UserData currentUser = userApi.UserObject(userApi.UserId);

            _userSelect.Items.Add(new ListItem(string.Format(UserDisplayFormat, currentUser.Username, currentUser.LastName, currentUser.FirstName, currentUser.Id), currentUser.Username));

        }

        _projectStatusFilter.Items.Add(new ListItem("All", "All"));
        _projectStatusFilter.Items.Add(new ListItem("Ektron Only", "Ektron"));
        _projectStatusFilter.SelectedValue = "Ektron";

        // Binds the Project data to the grid
        doDataBinding(false);
        //}
    }


    /// <summary>
    /// Loads the Project data in the grid
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        // If the user is not authorised in Ektron then redirect to blank page.
        SiteAPI siteApi = new SiteAPI();

        if (siteApi.RequestInformationRef.IsMembershipUser != 0 || siteApi.RequestInformationRef.UserId == 0)
        {
            Response.Redirect("blank.htm", false);
            return;
        }

        base.OnPreRender(e);
        UserAPI userApi = new UserAPI();
        DataRow userRow = FreewayDB.GetUserRow(userApi.UserId, false);
        if (userRow != null)
        {
            bool IsGlobalUser = false;
            DataRow GlobaluserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            if (GlobaluserRow != null)
            {
                IsGlobalUser = (bool)GlobaluserRow["ForceGlobalSettings"];
            }
            //Modified by Supriya on 3rd Aug 2010 To change frame title from �Freeway Settings� to �Freeway User Settings�.
            if (IsGlobalUser)
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:85%'>Freeway Project Dashboard  &nbsp;<i>(Connector is running in Global User mode.)</i></td><td align='right' style='width:15%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");
            else
                txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("<table width='100%'><tr><td align='left' style='width:85%'>Freeway Project Dashboard  &nbsp;<i>(Connector is running in Mapped Users mode.)</i></td><td align='right' style='width:15%'><a href='#' onClick='showAbout();' style='color:white;' >About Ektron 8 Connector</a></td></tr></table>");

        }
        //txtTitleBar.InnerHtml = m_refStyle.GetTitleBar("Freeway Project Dashboard");

        if (Session["ForceDaskboardRefresh"] != null)
        {
            // Displays the project information in the grid
            foreach (DataListItem item in _dataList.Items)
            {
                HyperLink projectId = item.FindControl("_projectId") as HyperLink;
                Label retrievedWhen = item.FindControl("_retrievedWhen") as Label;

                if (projectId != null && retrievedWhen != null)
                {
                    EktronProject ektronProject = EktronProjectCollection.CurrentProjects[projectId.Text];

                    if (ektronProject != null)
                        retrievedWhen.Text = ektronProject.RetrievedWhen;
                }
            }

            Session["ForceDaskboardRefresh"] = null;
        }

        if (Session["ForceDaskboardVisibleRefresh"] != null)
        {
#if false
			foreach (DataListItem item in _dataList.Items)
			{
				if (!item.Visible)
					return;

				HyperLink projectId = item.FindControl("_projectId") as HyperLink;
				Label retrievedWhen = item.FindControl("_retrievedWhen") as Label;

				if (projectId != null && retrievedWhen != null)
				{
					EktronProject ektronProject = EktronProjectCollection.CurrentProjects[projectId.Text];

					if (ektronProject != null)
						item.Visible = ektronProject.Visible;
				}
			}

			Session["ForceDaskboardVisibleRefresh"] = null;
#endif
            // Retrieves the project data from the Ektron project database mapped with Freeway 
            // baed on the user authorisation
            _dataList.DataSource = EktronProjectCollection.CurrentProjects;

            _dataList.DataBind();

            Session["ForceDaskboardVisibleRefresh"] = null;
        }
        StringBuilder sb = new StringBuilder();
        if (userRow != null)
        {
            //sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append(userRow["CurrentEnvironment"]);
            //sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User Id :  ");
            //DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            //if (globalUserRow != null && Convert.ToBoolean(globalUserRow["ForceGlobalSettings"]) == true)
            //    sb.Append(globalUserRow["FreewayUsername"]);
            //else
            //    sb.Append(userRow["FreewayUsername"]);
            //sb.Append("&nbsp; &nbsp;</b></div>");
        }
        else
        {
            //sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append("Not selected");
            //sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User Id :  ");
            //sb.Append("Not available");
            //sb.Append("&nbsp; &nbsp;</b></div>");
        }
        lblFreewayDetails.Text = sb.ToString();
        return;
    }

    /// <summary>
    /// Adds the FreewayDashboardProjectControl control for each row of the project
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _dataList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem
            || e.Item.ItemType == ListItemType.SelectedItem)
        {
            Table table = e.Item.Controls[1] as Table;

            if (table != null)
            {
                table.Rows[0].Attributes.Add("onclick", this.Page.ClientScript.GetPostBackEventReference(table.Rows[0].Cells[0].Controls[0], string.Empty));
            }

        }
        else if (e.Item.ItemType == ListItemType.Header)
        {
            UpdateSortingHeader(e.Item);
        }

        if (e.Item.DataItem is EktronProject)
        {
            Control control = e.Item.FindControl("_projectId");

            try
            {

                if (control is HyperLink)
                {
                    UserAPI userApi = new UserAPI();
                    DataRow userRow = FreewayDB.GetUserRow(userApi.UserId, false);
                    switch (userRow["CurrentEnvironment"].ToString())
                    {
                        case "Demo": (control as HyperLink).NavigateUrl = LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl((e.Item.DataItem as EktronProject).Id,
        LB.FreewayLib.Freeway.FreewayServers.Demo, FreewayConfiguration.CreateFreewayIdentity().UseSsl);
                            break;
                        case "Staging": (control as HyperLink).NavigateUrl = LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl((e.Item.DataItem as EktronProject).Id,
                               LB.FreewayLib.Freeway.FreewayServers.Staging, FreewayConfiguration.CreateFreewayIdentity().UseSsl);
                            break;
                        case "Live": (control as HyperLink).NavigateUrl = LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl((e.Item.DataItem as EktronProject).Id,
   LB.FreewayLib.Freeway.FreewayServers.Live, FreewayConfiguration.CreateFreewayIdentity().UseSsl);
                            break;
                        case "Staging2": (control as HyperLink).NavigateUrl = LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl((e.Item.DataItem as EktronProject).Id,
                               LB.FreewayLib.Freeway.FreewayServers.Staging2, FreewayConfiguration.CreateFreewayIdentity().UseSsl);
                            break;

                    }
                }
                //    if (control is HyperLink)
                //        (control as HyperLink).NavigateUrl = LB.FreewayLib.Freeway.VojoServer.GetFreewayProjectUrl((e.Item.DataItem as EktronProject).Id,
                //            (e.Item.DataItem as EktronProject).FreewayServer, FreewayConfiguration.CreateFreewayIdentity().UseSsl);

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("User account is invalid"))
                {
                    StringBuilder sberror = new StringBuilder();
                    sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                    sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");

                    sberror.Append("</tr></table>");
                    Response.Write(sberror.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            control = e.Item.FindControl("_Freewaydashboardprojectcontrol");

            if (control is freewaydashboardprojectcontrolForWidget)
            {
                (control as freewaydashboardprojectcontrolForWidget).EktronProject = e.Item.DataItem as EktronProject;

                (control as freewaydashboardprojectcontrolForWidget).Visible = (e.Item.DataItem as EktronProject).FullStatus != null;

                //Panel dvfreewaydashboardprojectcontrol = ((Panel)e.Item.FindControl("dvfreewaydashboardprojectcontrol"));

                //             if (dvfreewaydashboardprojectcontrol.Style.Value.Contains("display:none"))
                //           {
                //             dvfreewaydashboardprojectcontrol.Style.Add(HtmlTextWriterStyle.Display, "block");
                //           }
                //          else
                //             dvfreewaydashboardprojectcontrol.Style.Add(HtmlTextWriterStyle.Display, "none");


            }

            control = e.Item.FindControl("_JobId");
            Control controlStatus = e.Item.FindControl("_projectStatus");
            if (control is Label && controlStatus is Label)
            {
                if ((Convert.ToInt64(((Label)control).Text)) != Int64.MinValue && ((Label)controlStatus).Text == "Draft")
                    ((freewaydashboardprojectcontrolForWidget)e.Item.FindControl("_Freewaydashboardprojectcontrol")).SubmitProjectVisible = true;
                else
                    ((freewaydashboardprojectcontrolForWidget)e.Item.FindControl("_Freewaydashboardprojectcontrol")).SubmitProjectVisible = false;


            }

        }

        return;
    }

    /// <summary>
    /// On refresh click the updated project information will be reflected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _reload_Click(object sender, EventArgs e)
    {
        EktronProjectCollection.Reset();

        // redo the bindings
        doDataBinding(true);

        return;
    }
    //protected void LinkButton_Onclick(object sender, EventArgs e)
    //{
    //    mpeNewProject.Show();
    //}
    //protected void CancelBtn_Click(object sender, EventArgs e)
    //{
    //    mpeNewProject.Hide();
    //}

    /// <summary>
    /// Adds the control to each row of the project
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _Freewaydashboardprojectcontrol_OnRetrieveClick(object sender, EventArgs e)
    {
        // redo the bindings
        doDataBinding(false);

        return;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _selectButton_Click(object sender, EventArgs e)
    {
        return;
    }

    /// <summary>
    /// On click of the project row, the row will be expanded showing the freewaydashboardprojectcontrol 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _dataList_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (_dataList.SelectedItem != null || _dataList.SelectedIndex >= 0)
        {
            EktronProject ektronProject = EktronProjectCollection.CurrentProjects[_dataList.SelectedItem.ItemIndex];

            if (ektronProject != null)
            {
                if (ektronProject.FullStatus == null)
                {
                    try
                    {
                        ektronProject.GetStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
                        ektronProject.GetFilesStatus(FreewayConfiguration.IsExecutiveUser ?
                                FreewayConfiguration.CreateExecutiveFreewayIdentity() : FreewayConfiguration.CreateFreewayIdentity());
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("User account is invalid"))
                        {
                            StringBuilder sberror = new StringBuilder();
                            sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                            sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");

                            sberror.Append("</tr></table>");
                            Response.Write(sberror.ToString());
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    ektronProject.FullStatus = null;
                }
            }

            Control control = _dataList.SelectedItem.FindControl("_Freewaydashboardprojectcontrol");

            if (control is freewaydashboardprojectcontrolForWidget)
            {
                (control as freewaydashboardprojectcontrolForWidget).EktronProject = ektronProject;

                (control as freewaydashboardprojectcontrolForWidget).Visible = ektronProject.FullStatus != null;
                //Panel dvfreewaydashboardprojectcontrol = ((Panel)_dataList.SelectedItem.FindControl("dvfreewaydashboardprojectcontrol"));

                //          if (dvfreewaydashboardprojectcontrol.Style.Value.Contains("display:none"))
                //         {
                //           dvfreewaydashboardprojectcontrol.Style.Add(HtmlTextWriterStyle.Display, "block");
                //        }
                //         else
                //             dvfreewaydashboardprojectcontrol.Style.Add(HtmlTextWriterStyle.Display, "none");


            }

        }

        return;
    }

    /// <summary>
    /// On click of the user dropdown list, the grid will be populated with projects authorised to the user
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _userSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        EktronProjectCollection.Reset();
        doDataBinding(true);

        return;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _projectStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        return;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _projectCount_SelectedIndexChanged(object sender, EventArgs e)
    {
        return;
    }

    /// <summary>
    /// Allows sorting the grid on header click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Header_Click(object sender, EventArgs e)
    {
        Control control = sender as Control;

        if (control.ID.Equals("_projectIdHeader"))
        {
            if (SortingField == "Id")
                SortingOrder = !SortingOrder;
            else
            {
                SortingField = "Id";
                SortingOrder = false;
            }
        }
        else if (control.ID.Equals("_projectNameHeader"))
        {
            if (SortingField == "Description")
                SortingOrder = !SortingOrder;
            else
            {
                SortingField = "Description";
                SortingOrder = false;
            }
        }
        else if (control.ID.Equals("_submittedByHeader"))
        {
            if (SortingField == "SubmittedBy")
                SortingOrder = !SortingOrder;
            else
            {
                SortingField = "SubmittedBy";
                SortingOrder = false;
            }
        }
        else if (control.ID.Equals("_submittedWhenHeader"))
        {
            if (SortingField == "SubmittedWhen")
                SortingOrder = !SortingOrder;
            else
            {
                SortingField = "SubmittedWhen";
                SortingOrder = false;
            }
        }
        else if (control.ID.Equals("_projectStatusHeader"))
        {
            if (SortingField == "Status")
                SortingOrder = !SortingOrder;
            else
            {
                SortingField = "Status";
                SortingOrder = false;
            }
        }

        doDataBinding(false);

        return;
    }

    #endregion

    #region Private Fuctions
    /// <summary>
    /// Manages up or down image based on the sorting direction
    /// </summary>
    /// <param name="fromControl"></param>
    private void UpdateSortingHeader(Control fromControl)
    {
        updateImage(fromControl, "_projectIdSortImage", SortingField.Equals("Id"),
            SortingOrder ? "~/Workarea/images/application/arrow_up_green.gif" : "~/Workarea/images/application/arrow_down_green.gif");
        updateImage(fromControl, "_projectNameSortImage", SortingField.Equals("Description"),
            SortingOrder ? "~/Workarea/images/application/arrow_up_green.gif" : "~/Workarea/images/application/arrow_down_green.gif");
        updateImage(fromControl, "_submittedBySortImage", SortingField.Equals("SubmittedBy"),
            SortingOrder ? "~/Workarea/images/application/arrow_up_green.gif" : "~/Workarea/images/application/arrow_down_green.gif");
        updateImage(fromControl, "_submittedWhenSortImage", SortingField.Equals("SubmittedWhen"),
            SortingOrder ? "~/Workarea/images/application/arrow_up_green.gif" : "~/Workarea/images/application/arrow_down_green.gif");
        updateImage(fromControl, "_projectStatusSortImage", SortingField.Equals("Status"),
            SortingOrder ? "~/Workarea/images/application/arrow_up_green.gif" : "~/Workarea/images/application/arrow_down_green.gif");

        return;
    }

    /// <summary>
    /// Manages image visibility based on the sorting direction 
    /// </summary>
    /// <param name="fromControl"></param>
    /// <param name="controlName"></param>
    /// <param name="visible"></param>
    /// <param name="imageUrl"></param>
    private void updateImage(Control fromControl, string controlName, bool visible, string imageUrl)
    {
        Image image = fromControl.FindControl(controlName) as Image;

        if (image != null)
        {
            image.Visible = visible;
            image.ImageUrl = imageUrl;
        }

        return;
    }

    #endregion

    #region Public Function
    /// <summary>
    /// Binds the project data to grid
    /// </summary>
    /// <param name="refresh"></param>
    public void doDataBinding(bool refresh)
    {
        try
        {
            _dataList.DataSource = EktronProjectCollection.GetProjects(_userSelect.SelectedValue, refresh);

            EktronProjectCollection.CurrentProjects.Sort(new EktronProjectComparer(SortingField, SortingOrder));

            _dataList.DataBind();
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("User account is invalid") || ex.Message.Contains("Object reference not set to an instance of an object."))
            {
                StringBuilder sberror = new StringBuilder();
                sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                //sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");
                sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">No mapping found for the Freeway connector. <br> Please navigate to the User Settings node to complete the mapping.</td>");

                sberror.Append("</tr></table>");
                Response.Write(sberror.ToString());
                Response.Flush();
                Response.End();
            }
            else if (ex.Message.Contains("A network-related"))
            {
                StringBuilder sberror = new StringBuilder();
                sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
               sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">Freeway Services are Unavailable. <br> Please contact the Administrator.</td>");

                sberror.Append("</tr></table>");
                Response.Write(sberror.ToString());
                Response.Flush();
                Response.End();
            }
        }
    }
    #endregion


}

