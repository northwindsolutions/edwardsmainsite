﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <ul class="EkActivityList">
      <xsl:for-each select="ActivityFeeddata/entry">
        <li class="EkActivityMessage">
          <div class="message">
            <div class="avatar">
              <xsl:choose>
                <xsl:when test="//ActivityFeeddata/TemplateUserProfile!=''">
                  <a title="Administrator">
                    <xsl:attribute name="href"><xsl:value-of select="//ActivityFeeddata/sitePath"/><xsl:value-of select="//ActivityFeeddata/TemplateUserProfile"/><xsl:value-of select="//ActivityFeeddata/userProfileParamName"/>=<xsl:value-of select="userid"/></xsl:attribute>
                    <xsl:attribute name="title"><xsl:value-of select="displayname"/></xsl:attribute>
                    <img class="avatar">
                      <xsl:attribute name="alt"><xsl:value-of select="displayname"/></xsl:attribute>
                      <xsl:attribute name="src"><xsl:value-of select="avatarPath"/></xsl:attribute>
                    </img>
                  </a>
                </xsl:when>
                <xsl:when test="userAlias!=''">
                  <a title="Administrator">
                    <xsl:attribute name="href"><xsl:value-of select="//ActivityFeeddata/sitePath"/><xsl:value-of select="userAlias"/></xsl:attribute>
                    <xsl:attribute name="title"><xsl:value-of select="displayname"/></xsl:attribute>
                    <img class="avatar">
                      <xsl:attribute name="alt"><xsl:value-of select="displayname"/></xsl:attribute>
                      <xsl:attribute name="src"><xsl:value-of select="avatarPath"/></xsl:attribute>
                    </img>
                  </a>
                </xsl:when>
                <xsl:when test="//ActivityFeeddata/workareaProfileTemplate!=''">
                  <a title="Administrator">
                    <xsl:attribute name="href"><xsl:value-of select="//ActivityFeeddata/sitePath"/><xsl:value-of select="//ActivityFeeddata/workareaProfileTemplate"/><xsl:value-of select="//ActivityFeeddata/workareaProfileParamName"/>=<xsl:value-of select="userid"/></xsl:attribute>
                    <xsl:attribute name="title"><xsl:value-of select="displayname"/></xsl:attribute>
                    <img class="avatar">
                      <xsl:attribute name="alt"><xsl:value-of select="displayname"/></xsl:attribute>
                      <xsl:attribute name="src"><xsl:value-of select="avatarPath"/></xsl:attribute>
                    </img>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <img class="avatar">
                    <xsl:attribute name="alt"><xsl:value-of select="displayname"/></xsl:attribute>
                    <xsl:attribute name="src"><xsl:value-of select="avatarPath"/></xsl:attribute>
                  </img>
                </xsl:otherwise>
              </xsl:choose>
            </div>
            <xsl:value-of select="activity" disable-output-escaping="yes"/>
            <ul class="activityStreamCommands" >
              <li class="shareMessage">
                <a title="Share" href="#"><xsl:attribute name="onclick">_ShareActivity<xsl:value-of select="//ActivityFeeddata/clientID"/>(IAjax.getArguements(),'control=<xsl:value-of select="//ActivityFeeddata/clientID"/>&amp;__ecmmsgaction<xsl:value-of select="//ActivityFeeddata/clientID"/>=shareactivity&amp;__ecmid=<xsl:value-of select="id"/>' );return false;</xsl:attribute>Share</a>
              </li>
              <li class="commandSeparator">.</li>
              <li class="commentMessage">
                <a title="Comment" href="#"><xsl:attribute name="onclick">_CommentActivity<xsl:value-of select="//ActivityFeeddata/clientID"/>(IAjax.getArguements(),'control=<xsl:value-of select="//ActivityFeeddata/clientID"/>&amp;__ecmmsgaction<xsl:value-of select="//ActivityFeeddata/clientID"/>=commentactivity&amp;__ecmid=<xsl:value-of select="id"/>' );return false;</xsl:attribute>Comment<xsl:if test="commentCount&gt;0"> (<xsl:value-of select="commentCount"/>)</xsl:if></a>
              </li>
            </ul>
            <span class="EkActivityTimeSpan">
              <xsl:choose>
                <xsl:when test="//ActivityFeeddata/templateActivity!=''">
                  <a title="Administrator">
                    <xsl:attribute name="href"><xsl:value-of select="//ActivityFeeddata/sitePath"/><xsl:value-of select="//ActivityFeeddata/templateActivity"/><xsl:value-of select="//ActivityFeeddata/activityProfileParamName"/>=<xsl:value-of select="id"/></xsl:attribute>
                    <xsl:value-of select="timeLapse"/>
                  </a>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="timeLapse"/></xsl:otherwise>
              </xsl:choose>
            </span>
            <xsl:if test="shareSuccess='true' and id=//ActivityFeeddata/activityId"><div class="shareSuccess">Activity has been added to your status update.</div></xsl:if>
          </div>
          
          <!-- comment box -->
          <xsl:if test="showActivityCommentBox='true' and id=//ActivityFeeddata/activityId">
            <xsl:if test="commentListCount&gt;0">
              <ul class="EkActivityComments">
                <xsl:for-each select="activityComments/activityComment">
                  <li class="CommentItem">
                    <div class="avatar">
                      <xsl:choose>
                        <xsl:when test="//ActivityFeeddata/TemplateUserProfile!=''">
                          <a title="Administrator">
                            <xsl:attribute name="href">
                              <xsl:value-of select="//ActivityFeeddata/sitePath"/><xsl:value-of select="//ActivityFeeddata/TemplateUserProfile"/><xsl:value-of select="//ActivityFeeddata/userProfileParamName"/>=<xsl:value-of select="userid"/>
                            </xsl:attribute>
                            <xsl:attribute name="title">Avatar</xsl:attribute>
                            <img class="avatar">
                              <xsl:attribute name="alt">Avatar</xsl:attribute>
                              <xsl:attribute name="src">
                                <xsl:value-of select="avatarPath"/>
                              </xsl:attribute>
                            </img>
                          </a>
                        </xsl:when>
                        <xsl:when test="userAlias!=''">
                          <a title="Administrator">
                            <xsl:attribute name="href">
                              <xsl:value-of select="//ActivityFeeddata/sitePath"/>
                              <xsl:value-of select="userAlias"/>
                            </xsl:attribute>
                            <xsl:attribute name="title">Avatar</xsl:attribute>
                            <img class="avatar" title="Avatar">
                              <xsl:attribute name="alt">Avatar</xsl:attribute>
                              <xsl:attribute name="src">
                                <xsl:value-of select="avatarPath"/>
                              </xsl:attribute>
                            </img>
                          </a>
                        </xsl:when>
                        <xsl:when test="//ActivityFeeddata/workareaProfileTemplate!=''">
                          <a title="Administrator">
                            <xsl:attribute name="href">
                              <xsl:value-of select="//ActivityFeeddata/sitePath"/><xsl:value-of select="//ActivityFeeddata/workareaProfileTemplate"/><xsl:value-of select="//ActivityFeeddata/workareaProfileParamName"/>=<xsl:value-of select="userid"/>
                            </xsl:attribute>
                            <xsl:attribute name="title">Avatar</xsl:attribute>
                            <img class="avatar">
                              <xsl:attribute name="alt">Avatar</xsl:attribute>
                              <xsl:attribute name="src">
                                <xsl:value-of select="avatarPath"/>
                              </xsl:attribute>
                            </img>
                          </a>
                        </xsl:when>
                        <xsl:otherwise>
                          <img class="avatar">
                            <xsl:attribute name="alt">Avatar</xsl:attribute>
                            <xsl:attribute name="src">
                              <xsl:value-of select="avatarPath"/>
                            </xsl:attribute>
                          </img>
                        </xsl:otherwise>
                      </xsl:choose>
                    </div>
                    <div class="comment">
                      <p class="body">
                        <span class="username">
                          <xsl:value-of select="displayname"/>
                          <span class="colon">:</span>
                        </span>
                        <xsl:value-of select="comment"/>
                      </p>
                      <div class="metaData">
                        <span class="time">
                          <xsl:value-of select="timeLapse"/>
                        </span>
                      </div>
                      <ul class="commands">
                        <xsl:if test="userId=//ActivityFeeddata/currentUserId and canDelete">
                          <li class="ekDeleteComment">
                            <a title="Delete" href="#">
                              <xsl:attribute name="onclick">
                                _DeleteActivityComment<xsl:value-of select="//ActivityFeeddata/clientID"/>(IAjax.getArguements(),'control=<xsl:value-of select="//ActivityFeeddata/clientID"/>&amp;__ecmmsgaction<xsl:value-of select="//ActivityFeeddata/clientID"/>=deletecomment&amp;__ecmid=<xsl:value-of select="activityId"/>&amp;__commentid=<xsl:value-of select="id"/>&amp;__noofviews=<xsl:value-of select="//ActivityFeeddata/numberOfViews"/>');return false;
                              </xsl:attribute>
                              Delete
                            </a>
                          </li>
                        </xsl:if>
                      </ul>
                    </div>
                  </li>
                </xsl:for-each>
              </ul>
              <xsl:if test="showViewMoreComments='true'">
                <div id="EkViewMoreComments" class="EkViewMoreComments">
                  <a title="View more Comments" href="#">
                    <xsl:attribute name="onclick">_ShowMoreComments<xsl:value-of select="//ActivityFeeddata/clientID"/>(IAjax.getArguements(),'control=<xsl:value-of select="//ActivityFeeddata/clientID"/>&amp;__ecmmsgaction<xsl:value-of select="//ActivityFeeddata/clientID"/>=showmorecomments&amp;__ecmid=<xsl:value-of select="id"/>&amp;__noofviews=<xsl:value-of select="//ActivityFeeddata/numberOfViews"/>');return false;</xsl:attribute>
                    View more Comments
                  </a>
                </div>
              </xsl:if>
            </xsl:if>
            <xsl:if test="canComment='true'">
              <div class="AddActivityComment">
                <xsl:attribute name="id">EkAddActivityComment<xsl:value-of select="//ActivityFeeddata/clientID"/></xsl:attribute>
                <xsl:attribute name="name">EkAddActivityComment<xsl:value-of select="//ActivityFeeddata/clientID"/></xsl:attribute>
                <div class="commentBox">
                  <textarea autocomplete="off" class="ContributionText" maxlength="1000"  rows="0" cols="0">
                     <xsl:attribute name="id">commentText<xsl:value-of select="//ActivityFeeddata/clientID"/></xsl:attribute>
                     <xsl:attribute name="name">commentText<xsl:value-of select="//ActivityFeeddata/clientID"/></xsl:attribute>
                     <xsl:attribute name="onKeyDown">if(event.keyCode ==13) document.getElementById('commentBtn<xsl:value-of select="//ActivityFeeddata/clientID"/>').click()</xsl:attribute>
                  </textarea>
                </div>
                <input type="button" value="Comment">
                  <xsl:attribute name="id">commentBtn<xsl:value-of select="//ActivityFeeddata/clientID"/></xsl:attribute>
                  <xsl:attribute name="onclick">_ValidateCommentMsg<xsl:value-of select="//ActivityFeeddata/clientID"/>(IAjax.getArguements(),'control=<xsl:value-of select="//ActivityFeeddata/clientID"/>&amp;__ecmmsgaction<xsl:value-of select="//ActivityFeeddata/clientID"/>=addcomment&amp;__ecmid=<xsl:value-of select="id"/>&amp;__noofviews=<xsl:value-of select="//ActivityFeeddata/numberOfViews"/>');return false;</xsl:attribute>
                </input>
                </div>
            </xsl:if>
          </xsl:if>
          
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>
</xsl:stylesheet>