﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Datepicker.aspx.cs" Inherits="Datepicker" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Date Picker</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ektronUI:Datepicker ID="uxDatepicker" DisplayMode="Default" runat="server" />
        <input type="button" id="uxDone" runat="server" title="Done" value="Done" onclick="doneDatePickerClick();" />
        <input type="button" id="uxCancel" runat="server" title="Cancel" value="Cancel" onclick="cancelClick();" />
        <%--Hidden Values--%>
        <input type="hidden" id="uxTargetDatehidden" runat="server" value="" />
        <input type="hidden" id="uxSpanIdHidden" runat="server" value="" />
        <input type="hidden" id="uxFormName" runat="server" value="" />
        <input type="hidden" id="uxsdatehidden" runat="server" value="" />
        <input type="hidden" id="uxformelement" runat="server" value="" />
    </div>
    <ektronUI:JavaScript ID="JavaScript1" runat="server" Path="../java/internCalendarDisplayFuncs.js"
        Aggregate="True" />
    </form>
</body>
</html>
