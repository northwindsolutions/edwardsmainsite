﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Ektron.Cms.Localization;
using Ektron.Cms.Common;

/// <summary>
/// This Page is for Form's CalendarField.
/// </summary>
public partial class Datepicker : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        try
        {
            // Set DatePicker Default Value as Current Date.
            if (!string.IsNullOrEmpty(Request.QueryString["sdate"]))
            {
                uxDatepicker.Value = DateTime.Parse(Request.QueryString["sdate"]);
                uxsdatehidden.Value = Request.QueryString["sdate"];
            }
            else
                uxDatepicker.Value = DateTime.Now.Date;

            // Set the FormName and SpanId to hidden fields.
            if (!string.IsNullOrEmpty(Request.QueryString["formname"]))
                uxFormName.Value = Request.QueryString["formname"];

            if (!string.IsNullOrEmpty(Request.QueryString["spanid"]))
                uxSpanIdHidden.Value = Request.QueryString["spanid"];

            if (!string.IsNullOrEmpty(Request.QueryString["targetdate"]))
                uxTargetDatehidden.Value = Request.QueryString["targetdate"];

            if (!string.IsNullOrEmpty(Request.QueryString["formelement"]))
                uxformelement.Value = Request.QueryString["formelement"];

            var lmanager = new Ektron.Cms.Framework.Localization.LocaleManager();
            if (lmanager.ContentLanguage > 0 && lmanager.ContentLanguage != -1)
            {
                var criteria = new Ektron.Cms.Common.Criteria<LocaleProperty>(
               LocaleProperty.EnglishName,
               EkEnumeration.OrderByDirection.Ascending);
                criteria.PagingInfo.RecordsPerPage = 2;
                criteria.AddFilter(LocaleProperty.Id, CriteriaFilterOperator.EqualTo, lmanager.ContentLanguage);

                var list = lmanager.GetList(criteria);

                if (list.FirstOrDefault() != null)
                {
                    // Change the DatePicker Default Culture based up on the Current Content Language.
                    uxDatepicker.OverrideDefaultCulture = CultureInfo.CreateSpecificCulture(list.FirstOrDefault().Culture);
                }
            }
        }
        catch
        {
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}