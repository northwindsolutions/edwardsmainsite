<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewAllTags.ascx.cs" Inherits="controls_Community_PersonalTags_ViewAllTags" %>
<%@ Reference Page="../../../Community/PersonalTags.aspx" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="../../paging/paging.ascx" %>

<script type="text/javascript">
	function resetPostback()
	{
	    document.getElementById("<asp:Literal ID='ltlIsPostDataId' runat='server'/>").value = "";
	}
</script>

<div id="dhtmltooltip"></div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
    <div class="ektronToolbar" id="htmToolBar" runat="server"></div>
</div>	
<div class="ektronPageContainer ektronPageGrid">
    <asp:DataGrid ID="_dg" 
        AutoGenerateColumns="false"
        Width="100%"
        GridLines="None" 
        cssclass="ektronGrid"
        runat="server">
        <HeaderStyle CssClass="title-header" />	
    </asp:DataGrid>
  <uxEktron:Paging ID="uxPaging" runat="server"  />

    <input type="hidden" id="PTagsCBHdn" name="PTagsSelCBHdn" value="" />
    <input type="hidden" runat="server" id="tags_isPostData" value="true" name="tags_isPostData" />
</div>

