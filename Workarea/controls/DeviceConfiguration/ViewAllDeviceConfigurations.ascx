<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewAllDeviceConfigurations.ascx.cs"
    Inherits="ViewAllDeviceConfigurations" %>
<script type="text/javascript">
    Ektron.ready(function () {
        $ektron("#dlgBrowse").modal({ height: 100, modal: true, onShow: function (h) {
            $ektron("#dlgBrowse").css("margin-top", -1 * Math.round($ektron("#dlgBrowse").outerHeight() / 2)); h.w.show();
        }
        });
    });
</script>
<div id="dhtmltooltip">
</div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="divTitleBar" runat="server">
    </div>
    <div class="ektronToolbar" id="divToolBar" runat="server">
    </div>
</div>
<div id="dlgBrowse" class="ektronWindow ektronModalStandard">
    <div class="ektronModalHeader">
        <h3>
            <asp:Literal ID="updateWurfl" runat="server" />
            <asp:HyperLink ID="closeDialogLink" CssClass="ektronModalClose" runat="server" />
        </h3>
    </div>
    <div class="ektronModalBody" style="height: 150px; width: auto;">
        <iframe id="iframeUpdateWurflFile" scrolling="no" width="100%" height="80%" runat="server"
            frameborder="0"></iframe>
        <div style="margin-top: -5px;">
            <p class="cancelAddItemModal clearfix">
                <a href="#Close" class="redHover button buttonRight ektronModalClose" onclick="return false;">
                    <asp:Image ID="imgCloseAddItemModal" runat="server" />
                    <span>
                        <%=this._MessageHelper.GetMessage("close title") %></span> </a>
            </p>
        </div>
        <div class="MainCentreBottom">
            <asp:Literal ID="ltrNotification" runat="server"></asp:Literal>
        </div>
    </div>
</div>
<div class="ektronPageContainer">
    <div visible="false" runat="server" id="dvMessage">
        <span class="required">
            <asp:Label ID="lblEnableDeviceDetection" runat="server"></asp:Label>
        </span>
    </div>
    <div title="Device Type List" id="dvDeviceList" class="ektronPageInfo">
        <asp:DataGrid ID="DeviceListGrid" runat="server" CssClass="ektronGrid" AutoGenerateColumns="False"
            EnableViewState="False">
            <HeaderStyle CssClass="title-header" />
        </asp:DataGrid>
    </div>
    <asp:Panel runat="server" ID="pnlAdpImg" CssClass="ektronPageInfo" Visible="false">
        <%=_MessageHelper.GetMessage("lbl adaptive img cahce heading") %>
        <br />
        <%=_MessageHelper.GetMessage("lbl adaptive img lv1") %>:<asp:Literal runat="server"
            ID="ltrLV1Cnt" />
        <br />
        <%=_MessageHelper.GetMessage("lbl adaptive img lv2") %>:<asp:Literal runat="server"
            ID="ltrLV2Cnt" />
        <br />
        <ektronUI:Button runat="server" ID="btnClearCache" Text="Clear Cahce" OnClick="clearCache_click"></ektronUI:Button>
    </asp:Panel>
</div>
