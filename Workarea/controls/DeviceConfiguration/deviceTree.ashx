<%@ WebHandler Language="C#" Class="deviceTree" %>

using System;
using System.Web;
using Ektron.Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Ektron.Cms.Mobile;

public class deviceTree : IHttpHandler
{
    private EkDeviceInfo device;

    private Ektron.Cms.ContentAPI c_api;
    private Ektron.Cms.Common.EkMessageHelper m_ref;
    
    public deviceTree()
    {
        c_api = new Ektron.Cms.ContentAPI();
        m_ref = c_api.EkMsgRef;
        
    }
    
    public void ProcessRequest (HttpContext context)
    {
        WURFL.Aspnet.Extensions.Config.ApplicationConfigurer configurer = new WURFL.Aspnet.Extensions.Config.ApplicationConfigurer();
        IDeviceInfoProvider wurflProvider = Ektron.Cms.ObjectFactory.Get<IDeviceInfoProvider>();
        context.Response.ContentType = "text/plain";
        if (!String.IsNullOrEmpty(context.Request["detail"]))
        {
            device = wurflProvider.GetDeviceInfo(context.Request["detail"].ToString());
            if (device != null)
            {
                context.Response.Write(getcontenttip(device));
            }
        }
        
        context.Response.End();
    }
 
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string getcontenttip(EkDeviceInfo device)
    {
      
        try
        {
           StringBuilder sb = new StringBuilder();
                    
            sb.Append("<div class=\"deviceDetails\">");
            sb.Append("    <span> " + m_ref.GetMessage("tooltip device detail") + "</span>");
            sb.Append("     <table>");
            sb.Append("         <tr>");
            sb.Append("             <td>");
            sb.Append("               " + m_ref.GetMessage("tooltip device model") + ":");
            sb.Append("             </td>");
            sb.Append("             <td>");
            sb.Append(                  device.DeviceModel);
            sb.Append("             </td>");
            sb.Append("         </tr>");
            sb.Append("         <tr>");
            sb.Append("             <td>");
            sb.Append("                " + m_ref.GetMessage("tooltip device manufacturer") + ":");
            sb.Append("             </td>");
            sb.Append("             <td>");
            sb.Append(                  device.DeviceBrandName);
            sb.Append("             </td>");
            sb.Append("         </tr>");
            sb.Append("</div>");
      
            return sb.ToString();
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

}