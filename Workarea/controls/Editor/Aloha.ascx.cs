namespace Ektron
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.Content;
    using Ektron.Cms.Framework;
    using System.Text;
    using Ektron.Cms.Interfaces.Context;
    using System.Threading;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using Ektron.Cms.Common;

    public partial class AlohaEditor : Ektron.Cms.Controls.EditorBase
    {
        protected ContentData content_data;
        protected string _text = string.Empty;
        public static string _clientid;
        private string _id = string.Empty;
        protected EkRequestInformation requestInformation;
        public string ID
        {
            get
            {
                if (string.IsNullOrEmpty(_id))
                {
                    return this.ClientID;
                }
                else
                {
                    return _id;
                }
            }
            set { _id = value; }
        }
        public string CID
        {
            get { return _clientid; }
        }

        public string Content 
        {
            get
            {
                if (string.IsNullOrEmpty(base.Content))
                {
                    return string.Empty;
                }
                else
                {
                    return base.Content;
                }
            } 
            set { 
                base.Content = CleanContent(value);
            } 
        }
        public string Text { get { return _text; } set { _text = value; } }

  
        protected override void OnPreRender(EventArgs e)
        {
            if (string.IsNullOrEmpty(Content))
            {
                JavaScript.RegisterJavaScriptBlock(this, @"
                    Aloha.ready(function(){
                        for (var x = 0; x < Aloha.editables.length; x++)
                        {
                            if (Aloha.editables[x].obj.html() == '')
                            {
                                if (true == Aloha.jQuery.browser.msie) 
                                {
                                    Aloha.editables[x].obj.html('<p></p>');
                                } 
                                else
                                {
                                Aloha.editables[x].initEmptyEditable();
                                }
                            }                   
                        }
                });");
            }
            else
            {
                string loadedhtml = (base.HtmlEncoded ? System.Net.WebUtility.HtmlDecode(base.Content) : base.Content);

                //EIC_Literal.Text = loadedhtml;
                textValue.Value = base.Content; 
                Content = loadedhtml;
                Text = StripTagsCharArray(loadedhtml);

                //Content = CleanContent(Content);
                EIC_Body.ContentControl.InnerHtml = Content;
            }

            if (this.Visible)
            {
                ContentManager c_manager = new ContentManager(ApiAccessMode.LoggedInUser);

                string updatedContentScript = @"if (typeof Aloha != 'undefined')
                {
                    Aloha.bind('submit aloha-editable-deactivated', function (event, myeditable) {  
                        if (Ektron.Namespace.Exists('Ektron.Controls.Editor.Aloha.UpdateAllContents')) {                       
                            Ektron.Controls.Editor.Aloha.UpdateAllContents();
                        }
                    });
                    if (Ektron.Namespace.Register) {
                        Ektron.Namespace.Register('Ektron.Controls.Editor.Aloha');
                        Ektron.Controls.Editor.Aloha.AllowScripts = " + AllowScripts.ToString().ToLower() + @";
                        Ektron.Controls.Editor.Aloha.HtmlEncoded = " + HtmlEncoded.ToString().ToLower() + @";
                        Ektron.Controls.Editor.Aloha.ResponsiveImageBreakPoints = null;
                        Ektron.Controls.Editor.Aloha.Fontfamilies = null;
                    }
                }";
                JavaScript.RegisterJavaScriptBlock(this, updatedContentScript);
            }
            base.OnPreRender(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            requestInformation = ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
            EIC_Body.ContentID = base.ContentId;
            EIC_Body.ContentLanguage = base.ContentLanguage;
            EIC_Body.DefaultContentLanguage = base.DefaultContentLanguage;
            EIC_Body.FolderID = base.FolderId;
            EIC_Body.TagName = "div";
            EIC_Body.ToolbarConfig = base.ToolbarConfig;
            EIC_Body.Stylesheet = base.Stylesheet;
            EIC_Body.CheckVisibility = base.CheckVisibility;
            Ektron.Cms.Framework.Settings.LocaleManager lcmanager = new Ektron.Cms.Framework.Settings.LocaleManager();
            Ektron.Cms.Localization.LocaleData locale_data;

            int contlang = requestInformation.ContentLanguage;
            if (contlang == Ektron.Cms.Common.EkConstants.CONTENT_LANGUAGES_UNDEFINED || contlang == Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES)
            {
                contlang = requestInformation.DefaultContentLanguage;
            }
            locale_data = lcmanager.GetEnabledLocale(contlang);
            if (!string.IsNullOrEmpty(locale_data.Direction) && locale_data.Direction.Equals("rtl", StringComparison.CurrentCultureIgnoreCase))
            {
                EIC_Body.ContentControl.Attributes["style"] = "text-align: right !important";
            }
            Packages.Ektron.Namespace.Register(this);
            base.OnLoad(e);
        }

        #region helpers
        public void textValue_TextChanged(object sender, EventArgs e)
        {
            // a full postback is required for the hidden field to trigger this event.
            base.Content = textValue.Value;
        }

        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static string CleanContent(string html)
        {
            Regex re = new Regex(@"(<SCRIPT[^>]*>)([\s\S]*?)(<\/SCRIPT[^>]*>)", RegexOptions.IgnoreCase);
            html = re.Replace(html, @"<textarea class=""data-ektron-placeholder-script"" readonly=""readonly"" contenteditable=""false"">$2</textarea>");
            // remove "return false" from A tag onclick;
            re = new Regex(@"(<a[^>]*)onclick=""((?<remove>return false;[\s]*)+)", RegexOptions.IgnoreCase);
            html = re.Replace(html, @"$1onclick=""");
            // add data-ektron-url attribute back to IMG and A element.
            re = new Regex(@"(<[^>]*)data-ektron-url\s*=\s*('|"")[^\2]*?\2([^>]*>)", RegexOptions.IgnoreCase);
            Match match = Regex.Match(html, @"(<[^>]*)data-ektron-url\s*=\s*('|"")[^\2]*?\2([^>]*>)", RegexOptions.IgnoreCase);
            while (match.Success)
            {
                html = Regex.Replace(html, @"(<[^>]*)data-ektron-url\s*=\s*('|"")[^\2]*?\2([^>]*>)", "$1$3", RegexOptions.IgnoreCase);
                match = Regex.Match(html, @"(<[^>]*)data-ektron-url\s*=\s*('|"")[^\2]*?\2([^>]*>)", RegexOptions.IgnoreCase);
            }
            re = new Regex(@"(<img[^>]+\w*src=)""([^""]*\w+)""([^>]*>)", RegexOptions.IgnoreCase);
            html = re.Replace(html, @"$1""$2"" data-ektron-url=""$2"" $3");
            re = new Regex(@"(<a[^>]+\w*)href=""([^""]*\w+)""([^>]*>[\s\S]*?<\/a[^>]*>)", RegexOptions.IgnoreCase);
            html = re.Replace(html, @"$1 href=""$2"" data-ektron-url=""$2"" $3");
            //// add "return false" to A Tag onclick
            //re = new Regex(@"(<a[^>]+\w*)onclick=""([^""]*\w\W+)(?:return false;)*""([^>]*>[\s\S]*?\w\W+[\s\S]*?<\/a[^>]*>)", RegexOptions.IgnoreCase);
            //html = re.Replace(html, @"$1onclick=""$2return false;"" $3");
            return html;
        }

        #endregion
    }
}