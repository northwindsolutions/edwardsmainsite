using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Data;
using System.Web.Caching;
using System.Xml.Linq;
using System.Web.UI;
using System.Diagnostics;
using System.Web.Security;
using System;
using System.Text;
using Microsoft.VisualBasic;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.Profile;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Web;
using Ektron.Cms.Common;
using Ektron.Cms;


	public partial class controls_analytics_ContentReports : AnalyticsBase
	{
		
		
		protected CommonApi common = new Ektron.Cms.CommonApi();
		protected ContentAPI m_refContentApi = new Ektron.Cms.ContentAPI();
        protected Ektron.Cms.Content.EkContent ekcontent = new Ektron.Cms.Content.EkContent(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
		
		public override void Initialize()
		{
            ErrMsg.Visible = false;
			Image1.Visible = false;
			stats_aggr.Visible = false;

            if (!Page.IsPostBack)
            {
                GridView1.PageSize = PageSize;
                auditContent.PageSize = PageSize;

                lbl_total_hits.Text = common.EkMsgRef.GetMessage("total views");
                lbl_total_hits.ToolTip = common.EkMsgRef.GetMessage("total views");
                lbl_total_visitors.Text = common.EkMsgRef.GetMessage("total visitors");
                lbl_total_visitors.ToolTip = common.EkMsgRef.GetMessage("total visitors");
                lbl_hits_per_visitor.Text = common.EkMsgRef.GetMessage("views to visitors");
                lbl_hits_per_visitor.ToolTip = common.EkMsgRef.GetMessage("views to visitors");
                lbl_new_visitors.Text = common.EkMsgRef.GetMessage("new visitors");
                lbl_new_visitors.ToolTip = common.EkMsgRef.GetMessage("new visitors");
                lbl_returning_visitors.Text = common.EkMsgRef.GetMessage("returning visitors");
                lbl_returning_visitors.ToolTip = common.EkMsgRef.GetMessage("returning visitors");
                lbl_hits_vs_visitors.Text = common.EkMsgRef.GetMessage("views to visitors");
                lbl_hits_vs_visitors.ToolTip = common.EkMsgRef.GetMessage("views to visitors");

                lbl_new_vs_returning_visitors.Text = common.EkMsgRef.GetMessage("new to returning visitors");
            }
			if (Request.QueryString["id"] == null)
			{
				Description = common.EkMsgRef.GetMessage("top content");
				AnalyticsData.Clear();
                AnalyticsData = ekcontent.BA_getContentGlobalAnalytic(DateClause, "content", common.DefaultContentLanguage);
				
				if (AnalyticsData.Tables[0].Rows.Count == 0)
				{
					ErrMsg.Visible = true;
					ErrMsg.Text = common.EkMsgRef.GetMessage("alt No Records for this range");
				}
				
				((HyperLinkField) (GridView1.Columns[0])).DataNavigateUrlFormatString = common.ApplicationPath + "ContentAnalytics.aspx?type=content&id={0}";
				((HyperLinkField) (GridView1.Columns[0])).SortExpression = "content_title";
				
				AnalyticsDataView.Table = AnalyticsData.Tables[0];
				
				GridView1.DataSource = AnalyticsDataView;
				GridView1.DataBind();
				
				this.Image1.Visible = false;
			}
			else
			{
				long content_id;
				
				try
				{
					content_id = Convert.ToInt64(Request.QueryString["id"]);
				}
				catch (Exception)
				{
					content_id = 0;
				}			
                
                pnlnavBarcont.Visible = true;
                
                // Report1
                Description = (string)("Statistics for ContentID=" + content_id);
                stats_aggr.Visible = true;
                InitStatsAggr(content_id);

                //Report2
                Description = (string)("Activity by time for ContentID=" + content_id);
                Image1.Visible = true;
                graph_key.Text = "       <table border=\"0\"><tr><td width=\"20px\" height=\"10px\" bgcolor=\"red\">&nbsp;</td><td>" + "Views" + "</td></tr><tr><td width=\"20px\" height=\"10px\" bgcolor=\"blue\">&nbsp;</td><td>" + "Visitors" + "</td></tr></table>";
                Image1.ImageUrl = common.ApplicationPath + "ContentRatingGraph.aspx?type=time&view=" + CurrentView + "&res_type=content&res=" + content_id + "&EndDate=" + EkFunctions.UrlEncode(EndDateTime.ToString());

                //Report3
                Description = (string)("User Views for ContentID=" + content_id);
                InitAuditContent(content_id);
			}
		}
		
		private void InitStatsAggr(long content_id)
        {
            int newVisitorCount = 0;
            int returningVisitorCount = 0;
            int totalNewReturningCount = 0;
            float newVisitorRatio = 0;
            float returningVisitorRatio = 0;

            int hitCount = 0;
            int visitCount = 0;
            int totalHitsVisits = 0;
            float hitRatio = 0;
            float visitRatio = 0;

            AnalyticsData = ekcontent.BA_getContentGlobalAnalytic(DateClause, "content", common.DefaultContentLanguage, content_id);
            
            if (AnalyticsData.Tables[1].Rows.Count == 0)
            {
                ErrMsg.Visible = true;
                ErrMsg.Text = common.EkMsgRef.GetMessage("alt No Records for this range");
                ErrMsg.ToolTip = common.EkMsgRef.GetMessage("lbl no records");
            }

            {   // hits and visits
                visitCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[1].Rows[0][1]);
                hitCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[1].Rows[0][2]);
                totalHitsVisits = hitCount + visitCount;

                hitRatio = EkFunctions.GetPercent(hitCount, totalHitsVisits);
                visitRatio = EkFunctions.GetPercent(visitCount, totalHitsVisits);

                num_total_hits.Text = hitCount.ToString();
                num_total_hits.ToolTip = hitCount.ToString();
                num_total_visitors.Text = visitCount.ToString();
                num_total_visitors.ToolTip = visitCount.ToString();
            }

            {   // new and returning                               
                newVisitorCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[2].Rows[0][0]);

                returningVisitorCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[3].Rows[0][0]);

                totalNewReturningCount = newVisitorCount + returningVisitorCount;

                newVisitorRatio = EkFunctions.GetPercent(newVisitorCount, totalNewReturningCount);
                returningVisitorRatio = EkFunctions.GetPercent(returningVisitorCount, totalNewReturningCount);

                num_new_visitors.Text = newVisitorCount.ToString();
                num_new_visitors.ToolTip = newVisitorCount.ToString();
                num_returning_visitors.Text = returningVisitorCount.ToString();
                num_returning_visitors.ToolTip = returningVisitorCount.ToString();
            }

            double ratio = 0;

            try
            {
                ratio = double.Parse((string)num_total_hits.Text);
                ratio = Math.Round(ratio / double.Parse((string)num_total_visitors.Text), 2);
                if (double.Parse((string)num_total_visitors.Text) == 0)
                {
                    ratio = 0;
                }
            }
            catch (Exception)
            {
                ratio = 0;
            }

            if (ratio == 0)
            {
                num_hits_per_visitor.Text = "N/A";
            }
            else
            {
                num_hits_per_visitor.Text = ratio.ToString();
            }

            {
                graph_hits_per_visitor.BriefDescription = common.EkMsgRef.GetMessage("views to visitors");
                graph_hits_per_visitor.LoadData(new List<float>() { hitRatio, visitRatio });
                graph_hits_per_visitor.LoadColors(new List<string>() { "FF0000", "0000FF" });
                graph_hits_per_visitor.LoadNames(new List<string>() { common.EkMsgRef.GetMessage("total views"), common.EkMsgRef.GetMessage("total visitors") });
            }
            {
                graph_new_vs_returning_visitors.BriefDescription = common.EkMsgRef.GetMessage("new to returning visitors");
                graph_new_vs_returning_visitors.LoadData(new List<float>() { newVisitorRatio, returningVisitorRatio });
                graph_new_vs_returning_visitors.LoadColors(new List<string>() { "FF0000", "0000FF" });
                graph_new_vs_returning_visitors.LoadNames(new List<string>() { common.EkMsgRef.GetMessage("new visitors"), common.EkMsgRef.GetMessage("returning visitors") });
            }
        }
		
		private void InitAuditContent(long content_id)
		{
            AnalyticsData = ekcontent.BA_getContentGlobalAnalytic(DateClause, "content", 0, content_id);
			
			AnalyticsDataView.Table = AnalyticsData.Tables[4];
			
			auditContent.DataSource = AnalyticsDataView;
			auditContent.DataBind();
		}
		
		protected void GridView1_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			if (SortOrder == SortDirection.Descending)
			{
				SortExpression = e.SortExpression + " DESC";
			}
			else
			{
				SortExpression = e.SortExpression + " ASC";
			}
			
			AnalyticsDataView.Sort = SortExpression;
			GridView1.PageIndex = PageIndex;
			
			GridView1.DataSource = AnalyticsDataView;
			GridView1.DataBind();
		}
		
		protected void GridView1_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			PageIndex = e.NewPageIndex;
			
			AnalyticsDataView.Sort = SortExpression;
			GridView1.PageIndex = PageIndex;
			
			GridView1.DataSource = AnalyticsDataView;
			GridView1.DataBind();
		}
		
		protected void GridView2_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			if (SortOrder == SortDirection.Descending)
			{
				SortExpression = e.SortExpression + " DESC";
			}
			else
			{
				SortExpression = e.SortExpression + " ASC";
			}
			
			AnalyticsDataView.Sort = SortExpression;
			auditContent.PageIndex = PageIndex;
			
			auditContent.DataSource = AnalyticsDataView;
			auditContent.DataBind();
		}
		
		protected void GridView2_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			PageIndex = e.NewPageIndex;
			
			AnalyticsDataView.Sort = SortExpression;
			auditContent.PageIndex = PageIndex;
			
			auditContent.DataSource = AnalyticsDataView;
			auditContent.DataBind();
		}

        protected void TabAuditContent_Click(object sender, EventArgs e)
        {
            contentTabs.SetActiveTab(TabAuditContent);
        }
}
	
