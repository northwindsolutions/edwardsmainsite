using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Data;
using System.Web.Caching;
using System.Xml.Linq;
using System.Web.UI;
using System.Diagnostics;
using System.Web.Security;
using System;
using System.Text;
using Microsoft.VisualBasic;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.Profile;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Web;
using Ektron.Cms.Common;
using Ektron.Cms;

	public partial class controls_analytics_Referring_url : AnalyticsBase
	{	
		
		protected CommonApi common = new Ektron.Cms.CommonApi();
		private string Domain = "";
		public override void Initialize()
		{
            ErrMsg.Visible = false;
            if (!Page.IsPostBack)
            {

			    this.GridView1.PageSize = PageSize;
			    this.GridView2.PageSize = PageSize;
			    this.GridView3.PageSize = PageSize;
			
			    lbl_total_hits.Text = common.EkMsgRef.GetMessage("total views");
			    lbl_total_hits.ToolTip = common.EkMsgRef.GetMessage("total views");
			    lbl_total_visitors.Text = common.EkMsgRef.GetMessage("total visitors");
			    lbl_total_visitors.ToolTip = common.EkMsgRef.GetMessage("total visitors");
			    lbl_hits_per_visitor.Text = common.EkMsgRef.GetMessage("views to visitors");
			    lbl_hits_per_visitor.ToolTip = common.EkMsgRef.GetMessage("views to visitors");
			    lbl_new_visitors.Text = common.EkMsgRef.GetMessage("new visitors");
			    lbl_new_visitors.ToolTip = common.EkMsgRef.GetMessage("new visitors");
			    lbl_returning_visitors.Text = common.EkMsgRef.GetMessage("returning visitors");
			    lbl_returning_visitors.ToolTip = common.EkMsgRef.GetMessage("returning visitors");
			    lbl_hits_vs_visitors.Text = common.EkMsgRef.GetMessage("views to visitors");
			    lbl_hits_vs_visitors.ToolTip = common.EkMsgRef.GetMessage("views to visitors");
			    lbl_new_vs_returning_visitors.Text = common.EkMsgRef.GetMessage("new to returning visitors");
			    lbl_new_vs_returning_visitors.ToolTip = common.EkMsgRef.GetMessage("new to returning visitors");
            }
			    this.stats_aggr.Visible = false;
			    this.Image1.Visible = false;
			
			    if (Request.QueryString["rid"] == null)
			    {
				    Description = common.EkMsgRef.GetMessage("lbl Referring Domains");
				    AnalyticsData.Clear();
                    Ektron.Cms.Content.EkContent ekcontent = new Ektron.Cms.Content.EkContent(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
                    AnalyticsData = ekcontent.BA_getContentGlobalAnalytic(DateClause, "referrers");

                    if (AnalyticsData.Tables[0].Rows.Count == 0)
                    {
                        ErrMsg.Visible = true;
                        ErrMsg.Text = common.EkMsgRef.GetMessage("alt No Records for this range");
                        ErrMsg.ToolTip = common.EkMsgRef.GetMessage("lbl no records");
                    }
                    

                    ((HyperLinkField)(GridView1.Columns[0])).DataNavigateUrlFormatString = common.ApplicationPath + "/ContentAnalytics.aspx?type=referring&rid={0}";
				
				    AnalyticsDataView.Table = AnalyticsData.Tables[0];
				
				    GridView1.DataSource = AnalyticsDataView;
				    GridView1.DataBind();
				
				    Image1.Visible = false;
			    }
			    else
			    {
				    Domain = Request.QueryString["rid"];

                    pnlnavBarcont.Visible = true;
                    // Report1
                    stats_aggr.Visible = true;
                    InitStatsAggr(Domain);

                    //Report2
                    Image1.Visible = true;
                    Image1.ImageUrl = common.ApplicationPath + "ContentRatingGraph.aspx?type=time&view=" + CurrentView + "&res_type=referring&res=" + Domain + "&EndDate=" + EkFunctions.UrlEncode(EndDateTime.ToString());

                    //Report3
                    if (AnalyticsData.Tables[1].Rows.Count == 0)
                    {
                        ErrMsg.Visible = true;
                        ErrMsg.Text = common.EkMsgRef.GetMessage("alt No Records for this range");
                        ErrMsg.ToolTip = common.EkMsgRef.GetMessage("lbl no records");
                    }
                    AnalyticsDataView2.Table = AnalyticsData.Tables[1];
                    GridView2.DataSource = AnalyticsDataView2;
                    GridView2.DataBind();

                    //Report4
                    if (AnalyticsData.Tables[2].Rows.Count == 0)
                    {
                        ErrMsg.Visible = true;
                        ErrMsg.Text = common.EkMsgRef.GetMessage("alt No Records for this range");
                        ErrMsg.Text = common.EkMsgRef.GetMessage("lbl no records");
                    }

                    AnalyticsDataView.Table = AnalyticsData.Tables[2];
                    GridView3.DataSource = AnalyticsDataView;
                    GridView3.DataBind();
			    }			
           
		}
		
		private void InitStatsAggr(string ref_url)
		{
            int newVisitorCount = 0;
            int returningVisitorCount = 0;
            int totalNewReturningCount = 0;
            float newVisitorRatio = 0;
            float returningVisitorRatio = 0;

            int hitCount = 0;
            int visitCount = 0;
            int totalHitsVisits = 0;
            float hitRatio = 0;
            float visitRatio = 0;
            AnalyticsData.Clear();
            Ektron.Cms.Content.EkContent ekcontent = new Ektron.Cms.Content.EkContent(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
            AnalyticsData = ekcontent.BA_getContentGlobalAnalytic(DateClause, "referrers", 0, 0, ref_url);

            {   // hits and visits
                hitCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[3].Rows[0][0]);
                visitCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[3].Rows[0][1]);
                totalHitsVisits = hitCount + visitCount;

                hitRatio = EkFunctions.GetPercent(hitCount, totalHitsVisits);
                visitRatio = EkFunctions.GetPercent(visitCount, totalHitsVisits);

                num_total_hits.Text = hitCount.ToString();
                num_total_hits.ToolTip = hitCount.ToString();
                num_total_visitors.Text = visitCount.ToString();
                num_total_visitors.ToolTip = visitCount.ToString();
            }

            {   // new and returning
                newVisitorCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[3].Rows[0][2]);
                returningVisitorCount = EkFunctions.ReadIntegerValue(AnalyticsData.Tables[3].Rows[0][3]);
                totalNewReturningCount = newVisitorCount + returningVisitorCount;

                newVisitorRatio = EkFunctions.GetPercent(newVisitorCount, totalNewReturningCount);
                returningVisitorRatio = EkFunctions.GetPercent(returningVisitorCount, totalNewReturningCount);

                num_new_visitors.Text = newVisitorCount.ToString();
                num_new_visitors.ToolTip = newVisitorCount.ToString();
                num_returning_visitors.Text = returningVisitorCount.ToString();
                num_returning_visitors.ToolTip = returningVisitorCount.ToString();
            }
			
			double ratio = 0;
			
			try
			{
				ratio = double.Parse((string) num_total_hits.Text);
				ratio = Math.Round(ratio / double.Parse((string) num_total_visitors.Text), 2);
				if (double.Parse((string) num_total_visitors.Text) == 0)
				{
					ratio = 0;
				}
			}
			catch (Exception)
			{
				ratio = 0;
			}
			
			if (ratio == 0)
			{
				num_hits_per_visitor.Text = "N/A";
			}
			else
			{
				num_hits_per_visitor.Text = ratio.ToString();
			}

            {
                graph_hits_per_visitor.BriefDescription = common.EkMsgRef.GetMessage("views to visitors");
                graph_hits_per_visitor.LoadData(new List<float>() { hitRatio, visitRatio });
                graph_hits_per_visitor.LoadColors(new List<string>() { "FF0000", "0000FF" });
                graph_hits_per_visitor.LoadNames(new List<string>() { common.EkMsgRef.GetMessage("total views"), common.EkMsgRef.GetMessage("total visitors") });
            }
            {
                graph_new_vs_returning_visitors.BriefDescription = common.EkMsgRef.GetMessage("new to returning visitors");
                graph_new_vs_returning_visitors.LoadData(new List<float>() { newVisitorRatio, returningVisitorRatio });
                graph_new_vs_returning_visitors.LoadColors(new List<string>() { "FF0000", "0000FF" });
                graph_new_vs_returning_visitors.LoadNames(new List<string>() { common.EkMsgRef.GetMessage("new visitors"), common.EkMsgRef.GetMessage("returning visitors") });
            }
			
		}
		
		protected void GridView1_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			if (SortOrder == SortDirection.Descending)
			{
				SortExpression = e.SortExpression + " DESC";
			}
			else
			{
				SortExpression = e.SortExpression + " ASC";
			}
			
			AnalyticsDataView.Sort = SortExpression;
			GridView1.PageIndex = PageIndex;
			
			GridView1.DataSource = AnalyticsDataView;
			GridView1.DataBind();
		}
		
		protected void GridView1_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			PageIndex = e.NewPageIndex;
			
			GridView1.PageIndex = PageIndex;
			AnalyticsDataView.Sort = SortExpression;
			
			GridView1.DataSource = AnalyticsDataView;
			GridView1.DataBind();
		}
		
		protected void GridView2_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			if (SortOrder == SortDirection.Descending)
			{
				SortExpression = e.SortExpression + " DESC";
			}
			else
			{
				SortExpression = e.SortExpression + " ASC";
			}
            
            AnalyticsDataView2.Sort = SortExpression;
			GridView2.PageIndex = PageIndex;

            GridView2.DataSource = AnalyticsDataView2;
			GridView2.DataBind();
		}
		
		protected void GridView2_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			PageIndex = e.NewPageIndex;
			
			GridView2.PageIndex = PageIndex;
            AnalyticsDataView2.Sort = SortExpression;
            GridView2.DataSource = AnalyticsDataView2;
			GridView2.DataBind();
		}
		
		protected void GridView3_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
		{
			if (SortOrder == SortDirection.Descending)
			{
				SortExpression = e.SortExpression + " DESC";
			}
			else
			{
				SortExpression = e.SortExpression + " ASC";
			}
			
			AnalyticsDataView.Sort = SortExpression;
			GridView3.PageIndex = PageIndex;
			
			GridView3.DataSource = AnalyticsDataView;
			GridView3.DataBind();
		}
		
		protected void GridView3_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			PageIndex = e.NewPageIndex;
			
			GridView3.PageIndex = PageIndex;
			AnalyticsDataView.Sort = SortExpression;
			
			GridView3.DataSource = AnalyticsDataView;
			GridView3.DataBind();
		}
        protected void TabPaths_Click(object sender, EventArgs e)
        {
            Tabs.SetActiveTab(TabPaths);
        }
        protected void TabTopLandingPages_Click(object sender, EventArgs e)
        {
            Tabs.SetActiveTab(TabTopLandingPages);
        }
}
	

