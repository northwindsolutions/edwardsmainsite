#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  23 Oct 2009
Details : Modified the existing functionality of one file submittion in a Freeway Project
          To multiple files subbmittion in a Freeway Project.          
---------------------------------
  
*/
#endregion

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using DS = Ektron.Cms.LocalizationJobDataSet;
using System.Globalization;
using System.Text;
using System.Web.Configuration;
using LB.FreewayLib.Freeway;

public partial class Freeway_SendCompletedJobToFreeway : System.Web.UI.Page
{
    SiteAPI siteApi = new SiteAPI();
    LocalizationAPI locApi = new LocalizationAPI();
    private bool _alertPageTrip;

    protected void Page_Load(object sender, EventArgs e)
    {
        // LB Mumbai 28 Oct 2009
        // Checks the AlerPageTrip to retrieve the Job & File information 
        // and displays the target languages respectively
        if (!IsPostBack || AlerPageTrip)
        {
            //_jobId.Text = JobId.ToString();
	
            LocalizationJobDataSet.LocalizationJobRow currentJobRow = locApi.GetJobByID(JobId);

            _jobTitle.Text = currentJobRow.JobTitle;
           // _jobId.Text = currentJobRow.JobID.ToString();


            LocalizationJobDataSet.LocalizationJobDataTable jobs = locApi.GetJobs(JobId);

            for (int job = 0; job < jobs.Rows.Count; job++)
            {
                try
                {
                    LocalizationJobDataSet.LocalizationJobRow jobRow = jobs[job];

                    LocalizationJobDataSet.LocalizationJobFileDataTable jobFiles = locApi.GetFilesByJob(jobRow.JobID);

                    for (int file = 0; file < jobFiles.Rows.Count; file++)
                    {
                        LocalizationJobDataSet.LocalizationJobFileRow fileRow = jobFiles[file];

                        CultureInfo sourceCulture = new CultureInfo(fileRow.SourceLanguage);
                        string s1 = new LanguageIDMappingUtil().getEquivalentFreewayLangID(sourceCulture.Name);
                        VojoIdentity identity = FreewayConfiguration.CreateFreewayIdentity();
                        
                        LB.FreewayLib.VojoService.Language[] targetLangs = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(new LanguageIDMappingUtil().getEquivalentFreewayLangID(sourceCulture.Name), FreewayConfiguration.CreateFreewayIdentity());
                        //LB.FreewayLib.VojoService.Language[] targetLangs = LB.FreewayLib.Freeway.VojoServer.GetTargetLanguages(s1, identity);
                        

                     
                

                            


                        if (targetLangs.Length == 0)
                            continue;

                        CultureInfo targetCulture = new CultureInfo(fileRow.TargetLanguage);

                        if (!IsLanguageSupported(targetLangs, targetCulture))
                            continue;

                       
                        ListItem item = new ListItem(string.Format("{0} to {1}", new LanguageIDMappingUtil().getEquivalentFreewayLangID(sourceCulture.DisplayName),  new LanguageIDMappingUtil().getEquivalentFreewayLangID(targetCulture.DisplayName)));

                        item.Value = fileRow.FileID.ToString();
                        item.Selected = true;
                        _fileList.Items.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("User account is invalid") || ex.Message.Contains("Object reference not set to an instance of an object."))
                    {
                        StringBuilder sberror = new StringBuilder();
                        sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                        //sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");
                        sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">No valid Freeway User Settings were found for the Freeway Connector. Please navigate to the User Settings node to complete this configuration.</td>");
                        sberror.Append("</tr></table>");
                        Response.Write(sberror.ToString());
                        Response.Flush();
                        Response.End();
                    }
                    else if (ex.Message.Contains("A network-related"))
                    {
                        System.Text.StringBuilder sberror = new System.Text.StringBuilder();
                        sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                        sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">Freeway Services are Unavailable. <br> Please contact the Administrator.</td>");

                        sberror.Append("</tr></table>");
                        Response.Write(sberror.ToString());
                        Response.Flush();
                        Response.End();
                    }
                    else if (ex.Message.Contains("Object reference not set to an instance of an object."))
                    {
                        //Do nothing since alert from Ektron while retrieving the content from Freeway
                        // No an issue related to Freeway
                        // But creating file at Upload folder in Ektron
                    }
                    else
                    {
                        string errromessage = "javascript:alert('" + "No target languages found for the specified source language" + "');";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", errromessage, true);
                    }
                }

            }


        }


        if (lblFRTerLang != null)
        {
            if (_fileList.Items.Count > 0)

                lblFRTerLang.Visible = true;
            else
                lblFRTerLang.Visible = false;
        }


        StringBuilder sb = new StringBuilder();
        UserAPI userApi = new UserAPI();
        DataRow userRow = FreewayDB.GetUserRow(userApi.UserId, false);
        if (userRow != null)
        {
            //sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append(userRow["CurrentEnvironment"]);
            //sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            //DataRow globalUserRow = FreewayDB.GetUserRowFreewayServerWise(-1, false, userRow["CurrentEnvironment"].ToString());
            //if (globalUserRow != null && Convert.ToBoolean(globalUserRow["ForceGlobalSettings"]) == true)
            //    sb.Append(globalUserRow["FreewayUsername"]);
            //else
            //    sb.Append(userRow["FreewayUsername"]);
            //sb.Append("&nbsp; &nbsp;</b></div>");
        }
        else
        {
            //sb.Append("<div style='text-align:right;'><b>&nbsp;Current Environment is ");
            //sb.Append("Not selected");
            //sb.Append("&nbsp;&nbsp; &nbsp; &nbsp;Freeway User :  ");
            //sb.Append("Not available");
            //sb.Append("&nbsp; &nbsp;</b></div>");
        }
        lblFreewayDetails.Text = sb.ToString();

        return;
    }



   

    private bool IsLanguageSupported(LB.FreewayLib.VojoService.Language[] targetLangs, CultureInfo targetCulture)
    {
        foreach (LB.FreewayLib.VojoService.Language language in targetLangs)

            

            if (string.Compare(new LanguageIDMappingUtil().getEquivalentEktronLangID(language.ID), targetCulture.Name, true) == 0)
                return true;

        return false;
    }

    public long JobId
    {
        get
        {
            try
            {
                return long.Parse(Page.Request.QueryString["jobid"]);
            }
            catch
            {
                return 0;
            }
        }
    }

    public string BackUrl
    {
        get
        {
            try
            {
                return Page.Request.QueryString["backurl"];
            }
            catch
            {
                return string.Empty;
            }
        }
    }


    /// <summary>
    /// LB Mumbai 28 Oct 2009
    /// Check the Page Roundtrip 
    /// is due to the alert message
    /// </summary>
    public bool AlerPageTrip
    {
        get
        {
            return _alertPageTrip;
        }
        set
        {
            _alertPageTrip = value;
        }


    }
    protected void _cancel_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(BackUrl))
            Response.Redirect(BackUrl);

        return;
    }


    /// <summary>
    /// LB Mumbai 23 Oct 2009
    /// Submits the File in Freeway Project
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _submitfile_Click(object sender, EventArgs e)
    {
        // Checking agaianst the Project selected else sends alert
        if (_sendToFreewayControl.ProjectID != null)
        {
            if (_sendToFreewayControl.ProjectID != "0")
            {
                bool fileSelected = false;

                // LB Mumbai 23 Oct 2009
                // If no file list is available , sends alert
                if (_fileList.Items.Count == 0)
                {
                    alertMsg("alert('No project language matching file available to submit.')");
                    return;
                }
                foreach (ListItem item in _fileList.Items)
                    if (item.Selected)
                    {
                        fileSelected = true;

                        break;
                    }


                if (fileSelected)
                {
#if true
                    // Creates the EktronProject object by passing ProjectId and JobId
                    // Allows to retrieve the Files for the Project from Ektron
                    EktronProject ektronProject = new EktronProject(_sendToFreewayControl.ProjectID, JobId);


                    try
                    {
                        // LB Mumbai 23 OCt 2009
                        // If File Add functionality failed, sends alert
                        if (_fileList.Items.Count > 0)
                        {
                            try
                            {
                                string msgreturn = ektronProject.AddFile(_fileList.Items);
                                //UserAPI usrApi = new UserAPI();
                                //FreewayDB.CreateProjectId(int.Parse(_sendToFreewayControl.ProjectID), JobId, usrApi.UserObject(usrApi.UserId).Username, string.Empty);
                                //alertMsg("alert('File added to the project successfully.')");
                                alertMsg("alert('" + msgreturn + "')");
                                return;
                            }
                            catch (System.Threading.ThreadAbortException ex)
                            {
                                // Don't do anything
                            }
                            catch (Exception ex)
                            {
                                // LB Mumbai 23 Oct 2009 
                                // If Project submission failed in Freeway, sends the alert
                                if (ex.Message.Contains("already submitted"))
                                    alertMsg("alert('Project is already submitted in Freeway.')");
                                else if (ex.Message.Contains("call the AddTaskToProject"))
                                    alertMsg("alert('No file available in the project to submit.')");
                                else
                                    alertMsg("alert('User doesnot have rights to submit the project in Freeway.')");

                                return;
                            }
                        }
                        else
                        {
                            alertMsg("alert('No file available to add to the project')");
                            return;
                        }

                    }
                    catch (Exception ex) { throw ex; }
#else
			List<LocalizationJobDataSet.LocalizationJobFileRow> files = new List<LocalizationJobDataSet.LocalizationJobFileRow>();

			foreach (ListItem item in _fileList.Items)
			{
				LocalizationJobDataSet.LocalizationJobFileRow fileRow = locApi.GetFileByID(int.Parse(item.Value));

				files.Add(fileRow);
			}            

			if (files.Count > 0)
			{
                string projectId = FreewayUtil.CreateProject(_sendToFreewayControl.ProjectDescription, _sendToFreewayControl.POReference,
					string.Empty, string.Empty, DateTime.Now, DateTime.Now.AddDays(7.0), null);

				FreewayDB.CreateProjectId(int.Parse(projectId), JobId, string.Empty, string.Empty);

				foreach (LocalizationJobDataSet.LocalizationJobFileRow file in files)
					FreewayUtil.AddFileToProject(projectId, file);

				FreewayUtil.SubmitProject(projectId);
			}
#endif
                }


                if (!string.IsNullOrEmpty(BackUrl))
                    Response.Redirect(BackUrl);
            }
            else
            {
                alertMsg("alert('No Project Selected.');");
            }
        }
        else
        {
            alertMsg("alert('No Project Selected.');");

        }

        return;
    }


    /// <summary>
    /// LB Mumbai 23 Oct 2009 
    /// sends the alert to the application
    /// </summary>
    /// <param name="alertMsg"></param>
    private void alertMsg(string alertMsg)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "File Add Information", alertMsg, true);
        AlerPageTrip = true;
        if (alertMsg == "alert('File added to the project successfully.')")
        {
            AlerPageTrip = false;
            Response.Redirect(BackUrl, false);
        }
    }


   

}
