#region Header Comment
/*
---------------------------------
Created By :    Benoit Morneau
Created Date : 
Details :
---------------------------------
Modified By :    Adwait Churi
Modified Date :  22 Oct 2009
Details : Modified the existing functionality of one file submittion in a Freeway Project
          To multiple files subbmittion in a Freeway Project.
          Design Changes - UI Design changed 
          Code Cahnges - Added new event handler
                         _resetProj_Click - Resets the project detail block
                         _createProj_Click - Creates the project in Freeway
                         ExportToFreewayVisible - Modified to add client side functionality of 
                                                  hiding and showing the control.
                         DoDataBinding - Refreshes the project displayed in the _ddlProj dropdownlist
---------------------------------
  
*/
#endregion

#region Imports
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
#endregion 

#region Freeway_SendToFreewayControl
public partial class Freeway_SendToFreewayControl : System.Web.UI.UserControl
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        // LB Mumbai 23 Oct 2009
        // The Project Id list will bind the _ddlProj control
        // on First time page visit only
        if (!IsPostBack)
        {
            doDataBinding(true);
        }
        return;
    }
    #endregion 
    #region Public variable
    private string projectID;
    public string ProjectID
    {
        get
        {
            return _ddlProj.SelectedValue;
        }
    }


    #endregion 
    #region control events
    /// <summary>
    /// To add task
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void _addTask_Click(object sender, EventArgs e)
    {
        return;
    }
   
    /// <summary>
    /// To set Project id from ddlProj dropdownlist.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void _ddlProj_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ProjectID = _ddlProj.SelectedValue;
    //}
    #endregion 
    #region Method 
    /// <summary>
    /// LB Mumbai 23 Oct 2009
    /// Refreshes the project displayed in the _ddlProj dropdownlist
    /// </summary>
    /// <param name="refresh"></param>
    public void doDataBinding(bool refresh)
    {
        if (refresh)
        {
            _ddlProj.Items.Clear();

            ListItem lstselect = new ListItem("Select Project", "0");
            // Retrieves the users from Ektron and fill the user dropdown list
            Ektron.Cms.UserAPI userApi = new Ektron.Cms.UserAPI();

            _ddlProj.Items.Insert(0, lstselect);
            Ektron.Cms.UserData currentUser = userApi.UserObject(userApi.UserId);
            try
            {
                EktronProjectCollection projectCollection = null;
                //if (userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers, currentUser.Id,false) || FreewayConfiguration.IsExecutiveUser)
                //    projectCollection = EktronProjectCollection.GetProjects(string.Empty, refresh);
                //else
                    projectCollection = EktronProjectCollection.GetProjects(currentUser.Username, refresh);

                foreach (EktronProject ektronProject in projectCollection)
                {
                    if (ektronProject.Visible && ektronProject.Status == LB.FreewayLib.VojoService.ProjectStatusCode.Draft)
                    {
                        //EktronProjectCollection.CurrentProjects.Sort(new EktronProjectComparer(SortingField, SortingOrder));
                        ListItem lstnew = new ListItem("(" + ektronProject.Id + ") " + ektronProject.Description, ektronProject.Id);
                        _ddlProj.Items.Add(lstnew);
                    }

                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("User account is invalid"))
                {
                    StringBuilder sberror = new StringBuilder();
                    sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                    sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway User Settings screen.</td>");

                    sberror.Append("</tr></table>");
                    Response.Write(sberror.ToString());
                    Response.Flush();
                    Response.End();
                }
            }

            _ddlProj.SelectedValue = "0";
        }
    }
    #endregion 
    protected void btnRefreshProjects_Click(object sender, EventArgs e)
    {
        doDataBinding(true);
    }

    //protected void CancelBtn_Click(object sender, EventArgs e)
    //{
    //    mpeNewProject.Hide();
    //}

    //protected void LinkButton_Onclick(object sender, EventArgs e)
    //{
    //    mpeNewProject.Show();
    //}

}
#endregion