<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="freewayprojectcreation.aspx.cs"
    Inherits="freewayprojectcreation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Freeway Project creation</title>
    <asp:literal id="jsStyleSheet" runat="server" />

    <script language="JavaScript" type="text/javascript" src="../java/jfunct.js"></script>

    <script language="JavaScript" type="text/javascript" src="../java/toolbar_roll.js"></script>

    <link href="../../../Freeway/freewaypopcalendar.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript" src="../../../Freeway/freewaycalendarnew.js"></script>

    <script language="javascript" type="text/javascript">
	     function showAbout()
     { 
    
     	window.open("aboutCMSConnector.aspx","About","width=455,height=370,left=280px,top=180px,resizable=no",false)
    
     }
     
     	
	
	    <!--
	    function Selectanalysiscode(control)
		{
//		debugger;
		    var controlid = document.getElementById(control.id);
		    var analysiscode = document.getElementById('_analysiscode');
		    var ddlanalysisCode1 = document.getElementById('_ddlanalysisCode1')!=null ?(document.getElementById('_ddlanalysisCode1')).value:0;
		    var ddlanalysisCode2 = document.getElementById('_ddlanalysisCode2')!=null ?(document.getElementById('_ddlanalysisCode2')).value:0;
		    var ddlanalysisCode3 = document.getElementById('_ddlanalysisCode3')!=null ?(document.getElementById('_ddlanalysisCode3')).value:0;
		    
		    if(ddlanalysisCode1 == '0')
		    {
		         ddlanalysisCode1 = '';
		    }
		    
		    if(ddlanalysisCode2 == '0')
		    {
		         ddlanalysisCode2 = '';
		    }
		    
		    if(ddlanalysisCode3 == '0')
		    {
		         ddlanalysisCode3 = '';
		    }
		    
		    analysiscode.value = ddlanalysisCode1 + "/"+ ddlanalysisCode2 + "/" + ddlanalysisCode3 ;
		    
		    if(analysiscode.value == '//')
            {		    
                analysiscode.value = '';
		    }
		    
		}
		
		
		function GoBackToCaller(){
			window.location.href = document.referrer;
			}
			
		function SubmitForm(FormName, Validate) {
			var go = true;
			if (Validate.length > 0) {
				if (eval(Validate)) {
					document.forms[0].submit();
					return false;
				}
				else {
					return false;
				}
			}
			else {
				document.forms[0].submit();
				return false;
			}
		}
			
    	function VerifyForm () {
    	   
    	   if(Trim(document.forms[0]._txtprojectName.value).length == 0) 
    	   {
    	        
    	        document.forms[0]._txtprojectName.focus();
    	        alert('Project Name is required');
    	        return false;
    	   }
    	   else if(Trim(document.forms[0]._txtprojectName.value).length > 50) 
    	   {
    	        
    	        document.forms[0]._txtprojectName.focus();
    	        alert('Project Name must be less than 50 characters');
    	        return false;
    	   }
//    	   else if(Trim(document.forms[0]._txtexpectedstartingdate.value).length == 0) 
//    	   {
//    	        alert('Expected Starting Date is required');
//    	        return false;
//           }
//           else if(Trim(document.forms[0]._txtexpecteddeliverydate.value).length == 0) 
//    	   {
//    	        alert('Expected Delivery Date is required');
//    	        return false;
//           }
//    	   else if(CheckDate())
//    	   {
//    	        alert('Expected Delivery Date must be after Expected Starting Date.\r\n Or Date format is not valid');
//    	        return false;
//           }
           else
    	        return true;
    	}
    	
    	// Start From and To Date Validation 
    	var monthNames = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
    	
//    	function CheckDate()
//        {  
//           var boolvalue = false;
//           var AssignTo = document.forms[0]._txtexpecteddeliverydate.value;  
//           var AssignFrom = document.forms[0]._txtexpectedstartingdate.value;

//           var strfrom = AssignFrom.split("-");   //31-Dec-2009  
//           var frommonth = getMonth(strfrom[1]);
//          
//           var strto = AssignTo.split("-");   //31-Dec-2009    
//           var tomonth = getMonth(strto[1]);
//           
//           if(frommonth == 0 || tomonth == 0)
//           {
//              boolvalue = true; 
//           }
//           else
//           {
//               var dateFrom=new Date(frommonth+"/"+strfrom[0]+"/"+strfrom[2]);  
//               var dateTo=new Date(tomonth+"/"+strto[0]+"/"+strto[2]);  
//               
//               var boolfrommonth = isDate(frommonth+"/"+strfrom[0]+"/"+strfrom[2]);
//               var booltomonth = isDate(tomonth+"/"+strto[0]+"/"+strto[2]);

//               if(boolfrommonth && booltomonth)
//               {                               
//                    if (dateFrom > dateTo)
//                        boolvalue = true; 
//                    else
//                        boolvalue = false; 
//               }
//                else
//                        boolvalue = true;
//           }
//           return boolvalue;
//        }  
  
//        function getMonth(month)
//        {   
//            
//            var i = 0;
//            if(typeof(month) != 'undefined')
//            {
//                switch(month.toUpperCase())
//                {
//                    case "JAN":
//                        i = 1;
//                    break;
//                    case "FEB":
//                        i = 2;
//                    break;
//                    case "MAR":
//                        i = 3;
//                    break;
//                    case "APR":
//                        i = 4;
//                    break;
//                    case "MAY":
//                        i = 5;
//                    break;
//                    case "JUN":
//                        i = 6;
//                    break;
//                    case "JUL":
//                        i = 7;
//                    break;
//                    case "AUG":
//                        i = 8;
//                    break;
//                    case "SEP":
//                        i = 9;
//                    break;
//                    case "OCT":
//                        i = 10;
//                    break;
//                    case "NOV":
//                        i = 11;                
//                    break;
//                    case "DEC":
//                        i = 12;
//                    break;
//                    default:
//                        i=0;
//                    break;
//                }
//            }
//            return i;
//        }
//		// End From and To Date Validation 
//		
//isDate check functionality
//Start
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		//alert("The date format should be : mm/dd/yyyy")-->
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		//alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		//alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		//alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		//alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateForm(){
	var dt=document.frmSample.txtDate
	if (isDate(dt.value)==false){
		dt.focus()
		return false
	}
    return true
 }
//isDate check functionality
//End

function selectDate(textboxcontrol,hiddencontrol)
{

    window.open("../../../Freeway/DatePicker.aspx?textboxcontrol=" + textboxcontrol+"&hiddencontrol="+hiddencontrol,"DatePicker","status = yes,resizable = no,top = 200, left = 300,height = 200, width = 250");

//        if(dv.style.display=="block")

//        dv.style.display="none";

//        else if(dv.style.display=="none")

//        dv.style.display="block";

}

			
	//-->

    </script>

    <style type="text/css">
            #dvUserGroups p {padding:0em 1em 1em 1em;margin:0;font-weight:bold;color:#1d5987;}
            #dvUserGroups ul.userGroups {list-style:none;margin:0 0 0 1em;padding:0;border:1px solid #d5e7f5;}
            #dvUserGroups ul.userGroups li {display:block;padding:.25em;border-bottom:none;}
            #dvUserGroups ul.userGroups li.stripe {background-color:#e7f0f7;}
            #dvWorkpage td.label {width:auto;}
        </style>
</head>
<body >
    <form id="projectcreationform" runat="server">
     <asp:Panel runat="server"  ID="pnl"  ScrollBars="auto">

        <%--<table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div id="dhtmltooltip">
                    </div>--%>

                    <script language="JavaScript" type="text/javascript" src="../java/workareahelper.js"></script>
                         <%--<table border="1" width = "120%" cellpadding="0" cellspacing="0">--%>
                        <table border="1" width = "100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                <%--<table class="ektronPageHeader" width ="120%">--%>
                                    <table class="ektronPageHeader" width ="100%">
                                        <tr>
                                            <td id="txtTitleBar" class="ektronTitlebar" runat="server" nowrap>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="htmToolBar" class="ektronToolbar" runat="server">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="_submittionDetails" runat="server">
                                                    <table class="ektronGrid" border="1" id="_projectDetailsDiv" runat="server" 
                                                        cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="label" style="width: 5%">
                                                                <asp:Label ID="_lblprojectname" runat="server" Text="Project Name:"></asp:Label></td>
                                                            <td class="value" style="width: 95%">
                                                                <asp:TextBox ID="_txtprojectName" Width="95%" MaxLength="50" runat="server" /><span
                                                                    style="color: #ff0000">*</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label" style="width: 5%">
                                                                <asp:Label ID="_lblporef" runat="server" Text="PO Reference:"> </asp:Label></td>
                                                            <td class="value" style="width: 95%">
                                                                <asp:TextBox ID="_txtpoReference" runat="server" MaxLength="30" Width="95%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr runat="server" id="trAnalyasisCode">
                                                            <td class="label" style="width: 5%">
                                                                <asp:Label ID="_lblanalysiscode" runat="server" Text="Analysis Code:"> </asp:Label></td>
                                                            <td class="value" style="width: 95%">
                                                                <table width="97%">
                                                                    <tr>
                                                                        <td class="label" style="width: 20%; padding: 1; text-align: left;">
                                                                            <asp:Label ID="_lblanalysisCode1" runat="server"> </asp:Label></td>
                                                                        <td class="label" style="width: 20%; padding: 1; text-align: left;">
                                                                            <asp:Label ID="_lblanalysisCode2" runat="server"> </asp:Label>
                                                                        </td>
                                                                        <td class="label" style="width: 20%; padding: 1; text-align: left;">
                                                                            <asp:Label ID="_lblanalysisCode3" runat="server"> </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="value" >
                                                                            <asp:DropDownList ID="_ddlanalysisCode1" Enabled="true" runat="server" Width="100%" 
                                                                                onchange="javascript:Selectanalysiscode(this);">
                                                                            </asp:DropDownList></td>
                                                                        <td class="value" >
                                                                            <asp:DropDownList ID="_ddlanalysisCode2" Enabled="true" runat="server"  Width="100%" 
                                                                                onchange="javascript:Selectanalysiscode(this);">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td class="value" >
                                                                            <asp:DropDownList ID="_ddlanalysisCode3" Enabled="true" runat="server" Width="100%" 
                                                                                onchange="javascript:Selectanalysiscode(this);">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label" style="width: 5%">
                                                                <asp:Label ID="_lblspecialinstruction" runat="server" Text="Special Instruction:"> </asp:Label></td>
                                                            <td class="value">
                                                                <asp:TextBox ID="_txtspecialInstruction" TextMode="MultiLine" Width="95%" Height="70px"
                                                                    runat="server"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label" style="width: 5%">
                                                                <asp:Label ID="_lblexpectedstartingdate" runat="server" Text="Expected Starting Date:&nbsp;&nbsp;"> </asp:Label></td>
                                                            <td class="value">
                                                                <asp:TextBox ID="_txtexpectedstartingdate" runat="server" Width="30%" ReadOnly="true"></asp:TextBox>
                                                                <input type="hidden" runat="server" id="hdnexpectedstartingdate" />
                                                                <%--<a id = "CalId1" href = "javascript:setVal(document.all.item('CalId1'),'_txtexpectedstartingdate','txt_Date',292,320);"><img  alt="Select Date" border="0" src="../../Workarea/images/freeeway_calendar.gif"></a>--%>
                                                                <%--<a id = "CalId1" href = "javascript:setVal(document.all.item('CalId1'),'_txtexpectedstartingdate','txt_Date',83,339);"><img  alt="Select Date" border="0" src="../../Workarea/freeway/images/freeway_calendar.gif"></a>--%>
                                                                <img alt="Select Date" style="cursor: pointer;" border="0" src="../../../Freeway/images/freeway_calendar.gif"
                                                                    onclick="javascript:selectDate('_txtexpectedstartingdate','hdnexpectedstartingdate');" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label" style="width: 5%">
                                                                <asp:Label ID="_lblexpecteddeliverydate" runat="server" Text="Expected Delivery Date:&nbsp;&nbsp;"></asp:Label></td>
                                                            <td class="value">
                                                                <asp:TextBox ID="_txtexpecteddeliverydate" ReadOnly="true" runat="server" Width="30%"></asp:TextBox>
                                                                <input type="hidden" runat="server" id="hdnexpecteddeliverydate" />
                                                                <%--<a id = "CalId2" href = "javascript:setVal(document.all.item('CalId2'),'_txtexpecteddeliverydate','txt_Date',292,348);"><img  alt="Select Date" border="0" src="../../Workarea/images/freeeway_calendar.gif"></a>--%>
                                                                <%--<a id = "CalId2" href = "javascript:setVal(document.all.item('CalId2'),'_txtexpecteddeliverydate','txt_Date',433,339);"><img  alt="Select Date" border="0" src="../../Workarea/freeway/images/freeway_calendar.gif"></a>--%>
                                                                <img alt="Select Date" style="cursor: pointer;" border="0" src="../../../Freeway/images/freeway_calendar.gif"
                                                                    onclick="javascript:selectDate('_txtexpecteddeliverydate','hdnexpecteddeliverydate');" />
                                                            </td>
                                                        </tr>                
                                                       
                                                        <tr>
                                                        <td colspan="2">
                                                         <asp:Label ID="lblNote" Text="Note :" ForeColor="gray" runat="server"></asp:Label>
                                                                &nbsp; 
                                                                 <font style="color: Red;">*</font>
                                                            <asp:Label ID="lblMandatory" Text="Indicates mandatory field." ForeColor="gray" runat="server"></asp:Label>
                                                           
                                                          
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                        <td colspan="2">
                                                                    <asp:TextBox BackColor="#e7f0f7" ForeColor="#e7f0f7" BorderColor="#e7f0f7" BorderStyle="None"
                                                                    ID="_analysiscode" MaxLength="50" runat="server" Width="10%"></asp:TextBox>
                                                    
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
        <%--<asp:Panel ID="Panel1" Width="120%" runat="server" Visible="false">--%>
        <asp:Panel ID="_progressPanel" Width="100%" runat="server" Visible="false">
        </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
