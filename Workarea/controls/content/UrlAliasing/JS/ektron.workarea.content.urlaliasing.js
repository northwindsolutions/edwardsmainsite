﻿/*
* Ektron Workarea URL Aliasing
*
* Copyright 2012
*
* Depends:
*	jQuery
*   Ektron.Namespace.js
*/

Ektron.Namespace.Register("Ektron.Workarea.Content.UrlAliasing");
Ektron.Workarea.Content.UrlAliasing = {
    /* properties */
    sitepath: "",
    nomanualalias: "",
    /* methods */
    initEditAliasTab: function () {
        $ektron('#dvAliases').on('click', '.editAliasTrigger', function () {
            var aliasID = $ektron(this).attr('aliasdata').substring(1);
            $ektron('#uxEditAliasIFrame').attr('src', Ektron.Workarea.Content.UrlAliasing.sitepath + 'workarea/urlaliasing/AddEditViewAlias.aspx?mode=edit&id=' + aliasID);
            $ektron(jsEditAliasedTabInnerDiaglogSel).dialog("open");
            return false;
        });
        $ektron('#uxAliasTabContent_uxAddNewAlias').click(function () {
            var linkParams = $ektron(this).attr('aliasdata').substring(1).split('_');
            var contentID = linkParams[0];
            var langID = linkParams[1];
            $ektron('#uxEditAliasIFrame').attr('src', Ektron.Workarea.Content.UrlAliasing.sitepath + 'workarea/urlaliasing/AddEditViewAlias.aspx?mode=add&cid=' + contentID + "&lang=" + langID);
            $ektron(jsEditAliasedTabInnerDiaglogSel).dialog("open");
            return false;
        });
        $ektron(jsEditAliaseTriggerSelector).click(function () {
            var linkParams = $ektron(this).attr('aliasdata').substring(1).split('_');
            var contentID = linkParams[0];
            var langID = linkParams[1];
            $ektron(jsEditAliaseIFrameSelector).attr('src', Ektron.Workarea.Content.UrlAliasing.sitepath + 'workarea/urlaliasing/AddEditViewAlias.aspx?mode=add&cid=' + contentID + "&lang=" + langID);
            $ektron(jsEditAliasedTabInnerDiaglogSel).dialog("open");
            return false;
        });

        $ektron(jsuxNoManualAliasClientId).val(Ektron.Workarea.Content.UrlAliasing.nomanualalias);
    },

    initialevents: function () {
        Ektron.Workarea.Content.UrlAliasing.sitepath = $ektron(jsuxsitepathhiddenclientID).val();

        $ektron(jsuxExtensionDropDownListclientId).change(function () {
            Ektron.Workarea.Content.UrlAliasing.aliasvalidate();
        });

        $ektron(jsuxAliasAddNameClienId).keyup(function () {
            delay(function () {
                Ektron.Workarea.Content.UrlAliasing.aliasvalidate();
            }, 400);

        });

        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
    },
    validateManualAliasEditPage: function () {
        var aliasname = $ektron(jsuxAliasAddNameClienId).val();
        var badChars = [',', '\\', '<', '>', ' ', ':', '|', '?', '\'', '^', '%', '$', '!', '*', '#'];
        for (var i = 0; i < badChars.length; i++) {
            if (aliasname.indexOf(badChars[i]) != -1) {
                $ektron(jsuxerrormessagehiddenclientid).val("errorInvalidCharacters");
                $ektron("#uxUrlAliasErrorInvalidCharacters").show();
                return false;
            }
        }
        return true;
    },
    CreateRequestObj: function (id, option, type) {
        request = {
            "Id": id,
            "Option": option,
            "Type": type
        };
        return Ektron.JSON.stringify(request);
    },
    aliasvalidate: function () {
        var folderid = $ektron(jsuxfolderIdhiddenclientId).val();
        if (typeof (folderid) != "undefined") {

        }
        else {
            folderid = "0";
        }

        var aliasname = $ektron(jsuxAliasAddNameClienId).val();
        $ektron("#uxUrlAliasError").hide();
        $ektron("#uxUrlAliasErrorInvalidCharacters").hide();
        $ektron(jsuxerrormessagehiddenclientid).val("");

        if (typeof (aliasname) != "undefined" && aliasname != "") {

            aliasname = aliasname + $ektron(jsuxExtensionDropDownListclientId + " option:selected").val();
            if (Ektron.Workarea.Content.UrlAliasing.validateManualAliasEditPage()) {
                var args = Ektron.Workarea.Content.UrlAliasing.CreateRequestObj(0, aliasname, "manual");

                $ektron.ajax({
                    type: "POST",
                    cache: false,
                    async: true,
                    url: Ektron.Workarea.Content.UrlAliasing.sitepath + "workarea/UrlAliasing/js/ektronworkareaurlaliasing.ashx?folderid=" + folderid,
                    data: { "request": args },
                    success: function (msg) {
                        if (msg == "error") {
                            $ektron("#uxUrlAliasError").show();
                            $ektron(jsuxerrormessagehiddenclientid).val(msg);
                        }
                        if (msg == "errorInvalidCharacters") {
                            $ektron("#uxUrlAliasErrorInvalid").show();
                            $ektron(jsuxerrormessagehiddenclientid).val(msg);
                        }
                    }
                });
            }
        }
    },
    modelComplete: function () {
        $ektron('#uxAliasTabContent_uxEditAlias_Dialog_uxDialogControl').dialog('close');
        $ektron(jsEditAliasedTabInnerDiaglogSel).dialog('close');
        $ektron('.uxPartialPostbackTrigger').click();




    }

};
