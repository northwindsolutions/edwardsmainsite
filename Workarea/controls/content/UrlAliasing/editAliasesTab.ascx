﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="editAliasesTab.ascx.cs"
    Inherits="Workarea_controls_content_editaliasesTab" %>
<div id="dvAliases">
    <div id="addWrapper" runat="server">
        <fieldset id="uxAddManual" runat="server">
            <legend>
                <asp:Literal ID="uxManualAddTitle" runat="server" meta:resourcekey="uxManualAddTitle" />
            </legend>
            <asp:Label ID="uxAliasAddNameLabel" runat="server" meta:resourcekey="uxAliasAddNameLabel"></asp:Label>
            <asp:TextBox ID="uxAliasAddName" runat="server" CssClass="uxaliasnamecontenttab"
                onmouseup="Ektron.Workarea.Content.UrlAliasing.aliasvalidate();" meta:resourcekey="uxAliasAddName"></asp:TextBox>
            <asp:DropDownList ID="uxExtensionDropDownList" meta:resourcekey="uxExtensionDropDownList"
                runat="server" />
            <p>
            </p>
            <div id="uxUrlAliasError" style="display: none;">
                <ektronUI:Message ID="uxUrlAliasErrorMessage" runat="server" DisplayMode="Error" />
            </div>
            <div id="uxUrlAliasErrorInvalidCharacters" style="display: none;">
                <ektronUI:Message ID="uxUrlAliasErrorMessageInvalidCharacter" runat="server" DisplayMode="Error">
                    <ContentTemplate>
                        <p>
                            Invalid characters in the alias.</p>
                    </ContentTemplate>
                </ektronUI:Message>
            </div>
        </fieldset>
    </div>
    <div id="editWrapper" runat="server">
        <asp:UpdatePanel ID="uxAliasListUpdatePanel" runat="server">
            <ContentTemplate>
                <ektronUI:JavaScriptBlock ID="aliasTab" ExecutionMode="OnEktronReady" runat="server">
                    <ScriptTemplate>
                        Ektron.Workarea.Content.UrlAliasing.nomanualalias='<%=this.nomanualalias.ToString()%>'
                        Ektron.Workarea.Content.UrlAliasing.initEditAliasTab();
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <asp:HyperLink ID="uxAddNewAlias" CssClass="primary" runat="server" meta:resourcekey="uxAddNewAlias"
                    NavigateUrl="#" Visible="false"></asp:HyperLink>
                <fieldset id="uxManualList" runat="server" class="fsManualAliasList">
                    <legend>
                        <asp:Literal ID="uxManualListTitle" runat="server" meta:resourcekey="uxManualListTitle"></asp:Literal>
                    </legend>
                    <ektronUI:Message ID="uxManualMessage" runat="server" Visible="false" />
                    <div class="ektronPageGrid manualAliasListContainer">
                        <ektronUI:GridView ID="uxManualAliasListGridView" runat="server" AutoGenerateColumns="false"
                            EktronUIAllowResizableColumns="false" EnableEktronUITheme="true">
                            <Columns>
                                <ektronUI:RadioButtonField ID="uxDefaultField">
                                    <RadioButtonFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderDefault %>" />
                                    <RadioButtonFieldColumnStyle Width="200px" />
                                    <RadioButtonFieldItemStyle CheckedBoundField="IsDefault" Enabled="<%# this.IsEditable %>" />
                                    <KeyFields>
                                        <ektronUI:KeyField DataField="Id" />
                                    </KeyFields>
                                </ektronUI:RadioButtonField>
                                <ektronUI:CheckBoxField meta:resourceKey="columnHeaderActive" ID="uxActiveField">
                                    <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderActive %>" HeaderCheckBoxEnabled="<%# this.IsEditable %>" />
                                    <CheckBoxFieldColumnStyle Width="200px" />
                                    <CheckBoxFieldItemStyle CheckedBoundField="IsEnabled" Enabled="<%# this.IsEditable %>" />
                                    <KeyFields>
                                        <ektronUI:KeyField DataField="Id" />
                                    </KeyFields>
                                </ektronUI:CheckBoxField>
                                <asp:TemplateField meta:resourceKey="columnHeaderAlias">
                                    <ItemTemplate>
                                        <asp:HyperLink Target="_blank" ID="uxaliaslink" Text='<%# Eval("Alias") %>' runat="server"
                                            CssClass="editAliasTrigger" aliasdata='<%#"/"+ Eval("Id") %>' NavigateUrl="#"
                                            meta:resourceKey="columnHeaderAlias" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </ektronUI:GridView>
                    </div>
                </fieldset>
                <fieldset id="uxAutoList" runat="server">
                    <legend>
                        <asp:Literal ID="uxAutomaticListTitle" runat="server" meta:resourcekey="uxAutomaticListTitle"></asp:Literal>
                    </legend>
                    <ektronUI:Message ID="uxAutoMessage" runat="server" Visible="false" />
                    <div class="ektronPageGrid manualAliasListContainer">
                        <ektronUI:GridView ID="uxAutoAliasListGridView" runat="server" AutoGenerateColumns="false"
                            EktronUIAllowResizableColumns="false" EnableEktronUITheme="true">
                            <Columns>
                                <ektronUI:CheckBoxField ID="uxActiveAutoField">
                                    <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderActive %>" HeaderCheckBoxEnabled="false" />
                                    <CheckBoxFieldColumnStyle Width="200px" />
                                    <CheckBoxFieldItemStyle CheckedBoundField="IsEnabled" Enabled="false" />
                                    <KeyFields>
                                        <ektronUI:KeyField DataField="Id" />
                                    </KeyFields>
                                </ektronUI:CheckBoxField>
                                <asp:TemplateField meta:resourceKey="columnHeaderType">
                                    <ItemTemplate>
                                        <asp:Literal runat="server" ID="Type" Text='<%#this.getTypeImage((Ektron.Cms.Common.EkEnumeration.AliasRuleType)Eval("Type"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField meta:resourceKey="columnHeaderAlias">
                                    <ItemTemplate>
                                        <asp:HyperLink Target="_blank" ID="uxautoaliaslink" Text='<%# Eval("Alias") %>' runat="server"
                                            NavigateUrl='<%# BuildAliasDest(Container.DataItem as Ektron.Cms.Settings.UrlAliasing.DataObjects.AliasData) %>'
                                            meta:resourceKey="columnHeaderAlias" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </ektronUI:GridView>
                    </div>
                </fieldset>
                <ektronUI:Dialog ID="uxEditAlias" runat="server" AutoOpen="false" meta:resourcekey="uxEditAliasDialog"
                    Modal="true" Width="600" Height="600">
                    <ContentTemplate>
                        <ektronUI:Iframe ID="uxEditAliasIFrame" runat="server" Height="550" Width="560" CssClass="modalIframe">
                        </ektronUI:Iframe>
                    </ContentTemplate>
                </ektronUI:Dialog>
                <asp:Button ID="uxPartialPostbackTrigger" runat="server" CssClass="uxPartialPostbackTrigger"
                    Style="display: none;" />
                <asp:Button ID="uxPartialPostbackTriggerWithSave" runat="server" OnClick="PartialPostbackTriggerWithSave"
                    CssClass="uxPartialPostbackTriggerWithSave" Style="display: none;" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<asp:PlaceHolder ID="uxHiddenplaceholder" runat="server">
    <input type="hidden" runat="server" class="uxerrormessage" name="uxerrormessage"
        id="uxerrormessage" />
    <input type="hidden" runat="server" value="/" class="uxsitepathhidden" name="uxsitepathhidden"
        id="uxsitepathhidden" />
    <input type="hidden" runat="server" value="false" class="uxNoManualAlias" name="uxNoManualAlias"
        id="uxNoManualAlias" />
    <input type="hidden" runat="server" value="" class="uxmodehidden" name="uxmodehidden"
        id="uxmodehidden" />
    <input type="hidden" runat="server" value="0" class="uxfolderIdhidden" name="uxfolderIdhidden"
        id="uxfolderIdhidden" />
</asp:PlaceHolder>
<ektronUI:JavaScriptBlock ID="JavaScriptBlock2" ExecutionMode="OnParse" runat="server">
    <ScriptTemplate>
        var jsEditAliasedTabInnerDiaglogSel = '<%=this.uxEditAlias.Selector %>';
        var jsEditAliaseTriggerSelector = '#<%=this.uxAddNewAlias.ClientID %>';
        var jsEditAliaseIFrameSelector = '#<%=this.uxEditAliasIFrame.ClientID %>';

        var jsuxNoManualAliasClientId = '#<%=this.uxNoManualAlias.ClientID %>';
        var jsuxAliasAddNameClienId = '#<%=this.uxAliasAddName.ClientID %>';
        var jsuxsitepathhiddenclientID = '#<%=this.uxsitepathhidden.ClientID %>';
        var jsuxerrormessagehiddenclientid = '#<%=this.uxerrormessage.ClientID %>';
        var jsuxExtensionDropDownListclientId = '#<%=this.uxExtensionDropDownList.ClientID %>';

        var jsuxfolderIdhiddenclientId = '#<%=this.uxfolderIdhidden.ClientID %>';
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>
<ektronUI:JavaScriptBlock ID="JavaScriptBlock1" ExecutionMode="OnEktronReady" runat="server">
    <ScriptTemplate>
        Ektron.Workarea.Content.UrlAliasing.initialevents();
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>
