﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
using Ektron.Cms.Common;
using Ektron.Cms;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Telerik.Web.UI;
using Ektron.Cms.Controls.CalendarProvider;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Framework.Content;

public partial class Workarea_controls_content_editaliasesTab : System.Web.UI.UserControl
{
    long folderId = 0;
    long contentId = 0;
    int langID;

    AliasSettings settings;
    AliasSettingsManager settingsManager = new AliasSettingsManager();
    AliasManager aliasManager = new AliasManager();
    List<AliasData> manualAliases;
    List<AliasData> autoAliases;
    public bool nomanualalias = false;
    public bool IsCurrentfolderId = false;
    public bool IsEditable { get; set; }
    private DisplayMode displayMode;
    public DisplayMode Mode { get { return displayMode; } set { displayMode = value; } }
    public string alias
    {
        get { return uxAliasAddName.Text; }
    }
    public string extension
    {
        get { return uxExtensionDropDownList.SelectedValue; }
    }

    #region events

    protected void Page_Init(object sender, EventArgs e)
    {
        this.IsEditable = true;
        this.displayMode = this.getDisplayMode();
        this.uxUrlAliasErrorMessage.Text = GetLocalResourceObject("uxUrlAliasErrorMessage").ToString();
        this.uxsitepathhidden.Value = Ektron.Cms.CommonApi.Current.SitePath;

        if (Request.QueryString["folderid"] != null)
        {
            long.TryParse(Request.QueryString["folderid"].ToString(), out folderId);
            IsCurrentfolderId = true;
        }
        if (Request.QueryString["folder_id"] != null)
        {
            long.TryParse(Request.QueryString["folder_id"].ToString(), out folderId);
            IsCurrentfolderId = true;
        }

        if (this.displayMode == DisplayMode.Edit)
        {
            if (Request.QueryString["id"] != null)
            {
                long.TryParse(Request.QueryString["id"].ToString(), out contentId);
            }
            if (Request.QueryString["content_id"] != null && contentId == 0)
            {
                long.TryParse(Request.QueryString["content_id"].ToString(), out contentId);
            }
            if (Request.QueryString["contentId"] != null && contentId == 0)
            {
                long.TryParse(Request.QueryString["contentId"].ToString(), out contentId);
            }
            if (Request.QueryString["langtype"] != null)
            {
                int.TryParse(Request.QueryString["langtype"].ToString(), out langID);
            }          

            addWrapper.Visible = false;
            editWrapper.Visible = true;
            uxmodehidden.Value = "edit";
        }
        else if (this.displayMode == DisplayMode.Add)
        {
            bindExtensions();
            addWrapper.Visible = true;
            editWrapper.Visible = false;
            uxmodehidden.Value = "add";
        }
        else if (this.displayMode == DisplayMode.EditEvent)
        {
            SchedulerFormContainer container = (SchedulerFormContainer)BindingContainer;
            if (container.Appointment.ID != null)
            {
                EventRadScheduleProvider.EventInfo.ExtractContentID(container.Appointment.ID.ToString(), out folderId, out contentId, out langID);
                addWrapper.Visible = false;
                editWrapper.Visible = true;
                uxmodehidden.Value = "edit";
            }
            else
            {
                this.displayMode = DisplayMode.AddEvent;
                bindExtensions();
                addWrapper.Visible = true;
                editWrapper.Visible = false;
                uxmodehidden.Value = "add";
            }
        }

        settings = settingsManager.Get();
        if (settings.IsManualAliasingEnabled)
        {
            if (langID > 0)
            {
                uxAddNewAlias.Attributes.Add("aliasdata", "/" + contentId.ToString() + "_" + langID.ToString());
            }
            else
            {
                uxAddNewAlias.Attributes.Add("aliasdata", "/" + contentId.ToString() + "_" + CommonApi.Current.RequestInformationRef.ContentLanguage);
            }
            uxAddNewAlias.Visible = true;
        }
        else
        {
            uxManualMessage.Visible = true;
            uxManualMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            uxManualMessage.Text = GetLocalResourceObject("manualDisabledMessage").ToString();
        }
        if (!settings.IsFolderAliasingEnabled && !settings.IsTaxonomyAliasingEnabled)
        {
            uxAutoMessage.Visible = true;
            uxAutoMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            uxAutoMessage.Text = GetLocalResourceObject("autoDisabledMessage").ToString();
        }

        if (contentId != 0 && folderId == 0 && !IsCurrentfolderId)
        {
            uxfolderIdhidden.Value = GetFolderIdByContentId(contentId).ToString();
        }
        else
        {
            uxfolderIdhidden.Value = folderId.ToString();
        }
    }

    /// <summary>
    /// Get FolderId from ContentId
    /// </summary>
    /// <param name="cId"></param>
    /// <returns></returns>
    public long GetFolderIdByContentId(long cId)
    {
        long currentfolderid = 0;
        ContentManager cm = new ContentManager(Ektron.Cms.Framework.ApiAccessMode.Admin);
       var cdata= cm.GetItem(cId);
       if (cdata != null)
       {
          currentfolderid = cdata.FolderId;
       }

        return currentfolderid;
    }

    public void UpdateManualAliasActivationAndDefaults()
    {
        // Activate
        try
        {
            Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField enableColumn = uxManualAliasListGridView.Columns[1] as Ektron.Cms.Framework.UI.Controls.EktronUI.CheckBoxField;
            if (enableColumn != null)
            {
                if (enableColumn.DirtyFields != null)
                {
                    enableColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                    {
                        string originalState = dirtyField.OriginalState;
                        string dirtyState = dirtyField.DirtyState;
                        if (originalState != dirtyState)
                        {
                            string id = dirtyField.KeyFields["Id"];
                            long ruleID = long.Parse(id);
                            AliasData rule = aliasManager.GetItem(ruleID);
                            rule.IsEnabled = !rule.IsEnabled;
                            aliasManager.Update(rule);
                        }
                    });
                }
            }

            Ektron.Cms.Framework.UI.Controls.EktronUI.RadioButtonField defaultColumn = uxManualAliasListGridView.Columns[0] as Ektron.Cms.Framework.UI.Controls.EktronUI.RadioButtonField;
            if (defaultColumn != null)
            {
                if (defaultColumn.DirtyFields != null)
                {
                    defaultColumn.DirtyFields.ForEach(delegate(GridViewDirtyField dirtyField)
                    {
                        string originalState = dirtyField.OriginalState;
                        string dirtyState = dirtyField.DirtyState;
                        if (originalState != dirtyState)
                        {
                            string id = dirtyField.KeyFields["Id"];
                            long ruleID = long.Parse(id);
                            AliasData rule = aliasManager.GetItem(ruleID);
                            rule.IsDefault = !rule.IsDefault;
                            aliasManager.Update(rule);
                        }
                    });
                }
            }
        }
        catch (Exception ex)
        { }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterResources();
        if (settings.IsManualAliasingEnabled)
        {
            AliasCriteria manualCriteria = new AliasCriteria();
            manualCriteria.AddFilter(AliasProperty.TargetId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, this.contentId);
            manualCriteria.AddFilter(AliasProperty.LanguageId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, aliasManager.RequestInformation.ContentLanguage);
            manualCriteria.AddFilter(AliasProperty.Type, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.Common.EkEnumeration.AliasRuleType.Manual);
            manualAliases = aliasManager.GetList(manualCriteria);

            if (manualAliases == null || manualAliases.Count == 0)
            {
                uxNoManualAlias.Value = "true";

                nomanualalias = uxManualMessage.Visible = true;
                uxManualMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxManualMessage.Text = GetLocalResourceObject("noManualAliasesMessage").ToString();
            }
            else
            {
                uxNoManualAlias.Value = "false";

                nomanualalias = uxManualMessage.Visible = false;
                uxManualAliasListGridView.DataSource = manualAliases;
                uxManualAliasListGridView.EktronUIPagingInfo = manualCriteria.PagingInfo;
                uxManualAliasListGridView.EktronUIOrderByFieldText = manualCriteria.OrderByField.ToString();
            }

            if (!editWrapper.Visible)
            {
                uxNoManualAlias.Value = "false";
                nomanualalias = false;
            }
        }
        if (settings.IsFolderAliasingEnabled || settings.IsTaxonomyAliasingEnabled)
        {
            AliasCriteria autoCriteria = new AliasCriteria();
            autoCriteria.Condition = LogicalOperation.And;
            autoCriteria.AddFilter(AliasProperty.TargetId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, this.contentId);
            autoCriteria.AddFilter(AliasProperty.LanguageId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, aliasManager.RequestInformation.ContentLanguage);
            CriteriaFilterGroup<AliasProperty> typeGroup = new CriteriaFilterGroup<AliasProperty>();
            typeGroup.Condition = LogicalOperation.Or;
            typeGroup.AddFilter(AliasProperty.Type, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.Common.EkEnumeration.AliasRuleType.Taxonomy);
            typeGroup.AddFilter(AliasProperty.Type, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.Common.EkEnumeration.AliasRuleType.Folder);
            autoCriteria.FilterGroups.Add(typeGroup);
            autoAliases = aliasManager.GetList(autoCriteria);

            if (autoAliases == null || autoAliases.Count == 0)
            {
                uxAutoMessage.Visible = true;
                uxAutoMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxAutoMessage.Text = GetLocalResourceObject("noAutoAliasesMessage").ToString();
            }
            else
            {
                uxAutoAliasListGridView.DataSource = autoAliases;
                uxAutoAliasListGridView.EktronUIPagingInfo = autoCriteria.PagingInfo;
                uxAutoAliasListGridView.EktronUIOrderByFieldText = autoCriteria.OrderByField.ToString();
            }
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        this.DataBind();
    }

    protected void PartialPostbackTriggerWithSave(object sender, EventArgs e)
    {
        UpdateManualAliasActivationAndDefaults();
    }

    protected string BuildAliasDest(AliasData alias)
    {
        string target = "";
        FolderData fdat = ContentAPI.Current.GetFolderById(alias.SiteId, false, false);
        if (ContentAPI.Current.RequestInformationRef.IsStaging && fdat.DomainStaging != string.Empty)
        {
            target = "http://" + fdat.DomainStaging + "/" + alias.Alias;
        }
        else if (fdat.IsDomainFolder)
        {
            target += "http://" + fdat.DomainProduction + "/" + alias.Alias;
        }
        else
        {
            target += CommonApi.Current.SitePath + alias.Alias;
        }
        return target;
    }

    #endregion

    #region private

    protected string getTypeImage(EkEnumeration.AliasRuleType autoAliasType)
    {
        SiteAPI siteAPI = new SiteAPI();
        string image = "";
        switch (autoAliasType)
        {
            case EkEnumeration.AliasRuleType.Taxonomy:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/taxonomy.png", getLocalTypeName(autoAliasType));
                break;
            case EkEnumeration.AliasRuleType.Folder:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/folder.png", getLocalTypeName(autoAliasType));
                break;
            case EkEnumeration.AliasRuleType.User:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/userMembership.png", getLocalTypeName(autoAliasType));
                break;
            case EkEnumeration.AliasRuleType.Group:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/usersMemberGroups.png", getLocalTypeName(autoAliasType));
                break;
        }
        return image;
    }

    private string getLocalTypeName(EkEnumeration.AliasRuleType configType)
    {
        string returnVal;

        try
        {
            returnVal = GetLocalResourceObject("configType" + configType.GetHashCode().ToString()) as string;
        }
        catch
        {
            returnVal = configType.ToDescriptionString();
        }

        return returnVal;
    }

    private DisplayMode getDisplayMode()
    {
        DisplayMode mode = DisplayMode.Edit;

        if (Mode == DisplayMode.EditEvent)
        {
            return DisplayMode.EditEvent;
        }

        if (Request.QueryString["type"] != null)
        {
            if (Request.QueryString["type"].ToString() == "add") { mode = DisplayMode.Add; };
            if (Request.QueryString["type"].ToString() == "update") { mode = DisplayMode.Edit; };
        }

        if (Request.QueryString["new"] != null)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["new"].ToString()) && Request.QueryString["new"].ToString().ToLower() == "true")
            {
                mode = DisplayMode.Add;
            }
        }
        return mode;
    }

    private void bindExtensions()
    {
        AliasSettingsManager settingsManager = new AliasSettingsManager();
        List<FileExtension> extensions = settingsManager.GetAllExtensions();
        uxExtensionDropDownList.DataSource = extensions;
        uxExtensionDropDownList.DataValueField = "Extension";
        uxExtensionDropDownList.DataTextField = "Extension";
        uxExtensionDropDownList.DataBind();
    }


    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        Package resources = new Package()
        {
            Components = new List<Component>()
                {
                    Packages.EktronCoreJS,
                    Packages.Ektron.Namespace,
                    Packages.Ektron.Workarea.Core,
                    Packages.Ektron.JSON,
                    JavaScript.Create(cmsContextService.WorkareaPath + "/controls/content/UrlAliasing/js/ektron.workarea.content.urlaliasing.js")
                }
        };
        resources.Register(this);
    }

    #endregion

    public enum DisplayMode
    {
        None = 0,
        Add = 1,
        Edit = 2,
        EditEvent = 3,
        AddEvent = 4
    }
}