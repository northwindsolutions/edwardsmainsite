﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewSharePointContent.ascx.cs" Inherits="Ektron.Workarea.Controls.Content.ViewSharePointContent"  %>
<%--Resrouce file: <%$ Resources:ViewSharePointContent,String1 %>--%>
<asp:Repeater runat="server" ID="rptDataList">
    <HeaderTemplate>
        <table class="ektronGrid"  style="filter:  ; zoom: 1; display: block;">
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td class="label"><%#Eval("Name")%>:</td>
            <td><%#Eval("Value")%></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="stripe">
            <td  class="label"><%#Eval("Name")%>:</td>
            <td><%#Eval("Value")%></td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
