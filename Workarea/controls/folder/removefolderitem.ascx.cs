using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Data;
using System.Web.Caching;
using System.Xml.Linq;
using System.Web.UI;
using System.Diagnostics;
using System.Web.Security;
using System;
using System.Text;
using Microsoft.VisualBasic;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.Profile;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Web;
using Ektron.Cms;
//using Ektron.Cms.Common.EkConstants;
using Ektron.Cms.Commerce;
//using Ektron.Cms.Common.EkEnumeration;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Content;


	public partial class removefolderitem : System.Web.UI.UserControl
	{
		
		
		#region Member Variables
		
		protected ContentAPI _ContentApi = new ContentAPI();
		protected StyleHelper _StyleHelper = new StyleHelper();
		protected EkMessageHelper _MessageHelper;
		protected long _Id = 0;
        protected long _ConfigId = 0;
		protected FolderData _FolderData;
		protected PermissionData _PermissionData;
		protected string _AppImgPath = "";
		protected int _ContentType = 1;
		protected long _CurrentUserId = 0;
		protected Collection _PageData;
		protected string _PageAction = "";
		protected string _OrderBy = "";
		protected int _ContentLanguage = -1;
		protected int _EnableMultilingual = 0;
		protected string _SitePath = "";
		protected EkContent _EkContent;
		protected long _FolderId = -1;
		protected int _CurrentPageId = 1;
		protected int _TotalPagesNumber = 1;
		protected int _TotalRecordsNumber = 0;
		protected bool _ShowArchive = false;
        private int _PagingCurrentPageNumber = 1;
        protected string direction = "";
		#endregion
		
		#region Events
		
		private void Page_Load(System.Object sender, System.EventArgs e)
		{
			_MessageHelper = _ContentApi.EkMsgRef;
			if (!(Request.QueryString["id"] == null))
			{
				_Id = Convert.ToInt64(Request.QueryString["id"]);
			}
            if (!(Request.QueryString["configid"] == null))
            {
                _ConfigId = Convert.ToInt64(Request.QueryString["configid"]);
            }
			if (!(Request.QueryString["action"] == null))
			{
				_PageAction = Convert.ToString(Request.QueryString["action"]).ToLower().Trim();
			}
			if (!(Request.QueryString["orderby"] == null))
			{
				_OrderBy = Convert.ToString(Request.QueryString["orderby"]);
			}
			if ((!(Request.QueryString["showarchive"] == null)) && (Request.QueryString["showarchive"].ToString().ToLower() == "true"))
			{
				_ShowArchive = true;
			}
			if (!(Request.QueryString["LangType"] == null))
			{
				if (Request.QueryString["LangType"] != "")
				{
					_ContentLanguage = Convert.ToInt32(Request.QueryString["LangType"]);
					_ContentApi.SetCookieValue("LastValidLanguageID", _ContentLanguage.ToString());
				}
				else
				{
					if (_ContentApi.GetCookieValue("LastValidLanguageID") != "")
					{
						_ContentLanguage = Convert.ToInt32(_ContentApi.GetCookieValue("LastValidLanguageID"));
					}
				}
			}
			else
			{
				if (_ContentApi.GetCookieValue("LastValidLanguageID") != "")
				{
					_ContentLanguage = Convert.ToInt32(_ContentApi.GetCookieValue("LastValidLanguageID"));
				}
			}
			
			if (Request.QueryString["currentpage"] != null)
			{
				_CurrentPageId = Convert.ToInt32(Request.QueryString["currentpage"]);
			}
			
			if (_ContentLanguage == Ektron.Cms.Common.EkConstants.CONTENT_LANGUAGES_UNDEFINED)
			{
				_ContentApi.ContentLanguage = Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES;
			}
			else
			{
				_ContentApi.ContentLanguage = _ContentLanguage;
			}
			_CurrentUserId = _ContentApi.UserId;
			_AppImgPath = _ContentApi.AppImgPath;
			_SitePath = _ContentApi.SitePath;
			_EkContent = _ContentApi.EkContentRef;
			_EnableMultilingual = _ContentApi.EnableMultilingual;
			contentids.Value = "";
			DeleteContentByCategory();
			isPostData.Value = "true";
			
			RegisterJS();
			
		}
		
		#endregion
		
		#region Helpers
		
		private void DeleteContentByCategory()
		{
			if (IsPostBack && Request.Form[isPostData.UniqueID] != "")
			{
				Process_submitMultiDelContAction();
			}
			else
			{
				if (IsPostBack== false || (Request.Form.Count > 0 && Request.Form[isPostData.UniqueID] != ""))
				{
					Display_Delete();
				}
			}
		}
		
		private void Process_submitMultiDelContAction()
		{
			string strContentIds = "";
			string[] arrArray;
			int i = 0;
			Ektron.Cms.Content.EkContent m_refContent;
            if (Request.QueryString["id"] != null)
            {
			    _FolderId = Convert.ToInt64(Request.QueryString["id"]);
            }
            else
            {
                _FolderId = Convert.ToInt64(folder_id.Value);
            }
			_CurrentPageId = Convert.ToInt32(Request.Form[page_id.UniqueID]);
			try
			{
				m_refContent = _ContentApi.EkContentRef;
				arrArray = Strings.Split(Request.Form[contentids.UniqueID], ",", -1, 0);
				for (i = 0; i <= (arrArray.Length - 1); i++)
				{
					if (Request.Form["id_" + arrArray[i]] == "on")
					{
						strContentIds = strContentIds + arrArray[i] + ",";
					}
				}
				if (strContentIds != "")
				{
					strContentIds = strContentIds.Substring(0, strContentIds.Length - 1);
					m_refContent.SubmitForDeletev2_0(strContentIds, _FolderId);
				}
                if (!string.IsNullOrEmpty(strContentIds))
                {
                    if ("deletecontentbyconfiguration" == _PageAction)
                    {
                        Response.Redirect((string)("xml_config.aspx?action=ViewXmlConfiguration&id=" + _ConfigId), false);
                    }                   
                    else
                    {
                        if (_ShowArchive)
                        {
                            Response.Redirect((string)("content.aspx?LangType=" + _ContentLanguage + "&action=ViewArchiveContentByCategory&id=" + _FolderId + (_CurrentPageId > 1 ? "&IsArchivedEvent=true&currentpage=" + _CurrentPageId : "")), false);
                        }
                        else
                        {
                            Response.Redirect((string)("content.aspx?LangType=" + _ContentLanguage + "&action=ViewContentByCategory&id=" + _FolderId + (_CurrentPageId > 1 ? "&currentpage=" + _CurrentPageId : "")), false);
                        }
                    }
                }
                else
                {
                    _PagingCurrentPageNumber = System.Convert.ToInt32(this.uxPaging.SelectedPage) + 1;
                    _CurrentPageId = _PagingCurrentPageNumber;
                    Display_Delete();
                }
			}
			catch (Exception ex)
			{
				Response.Redirect((string) ("reterror.aspx?info=" + EkFunctions.UrlEncode(ex.Message)), false);
			}
		}
		
		#endregion
		
		#region Display
		
		private void Display_Delete()
		{
            if (_ConfigId > 0)
            {
                Display_DeleteContentByConfiguration();
            }
            else if (_Id >= 0)
            {
                _FolderData = _ContentApi.GetFolderById(_Id);
                _PermissionData = _ContentApi.LoadPermissions(_Id, "folder", 0);

                switch (_FolderData.FolderType)
                {
                    case (int)Ektron.Cms.Common.EkEnumeration.FolderType.Catalog:
                        Display_DeleteEntries();
                        break;
                    default:
                        Display_DeleteContentByCategory();
                        break;
                }
            }
		}
		
		#region Entries

		
		private void Display_DeleteEntries()
		{
			
			CatalogEntry CatalogManager = new CatalogEntry(_ContentApi.RequestInformationRef);
			System.Collections.Generic.List<EntryData> entryList = new System.Collections.Generic.List<EntryData>();
			Ektron.Cms.Common.Criteria<EntryProperty> entryCriteria = new Ektron.Cms.Common.Criteria<EntryProperty>();
			
			entryCriteria.AddFilter(EntryProperty.CatalogId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, _Id);
			if (_ContentApi.RequestInformationRef.ContentLanguage > 0)
			{
				entryCriteria.AddFilter(EntryProperty.LanguageId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, _ContentApi.RequestInformationRef.ContentLanguage);
			}
			
			entryCriteria.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending;
			entryCriteria.OrderByField = EntryProperty.Title;
			
			entryCriteria.PagingInfo.RecordsPerPage = _ContentApi.RequestInformationRef.PagingSize;
			entryCriteria.PagingInfo.CurrentPage = _CurrentPageId;
			
			if (_ShowArchive == false)
			{
				entryCriteria.AddFilter(EntryProperty.IsArchived, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, false);
			}
			else
			{
				entryCriteria.AddFilter(EntryProperty.IsArchived, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, true);
			}
			
			entryList = CatalogManager.GetList(entryCriteria);
			
			_TotalPagesNumber = System.Convert.ToInt32(entryCriteria.PagingInfo.TotalPages);
			
			DeleteContentByCategoryToolBar();
			
			
			Populate_DeleteCatalogGrid(entryList);
			
			folder_id.Value =Convert.ToString(_Id);
			
		}
		private void Populate_DeleteCatalogGrid(System.Collections.Generic.List<EntryData> entryList)
		{
            _PagingCurrentPageNumber = System.Convert.ToInt32(this.uxPaging.SelectedPage);
			DeleteContentByGategoryGrid.Controls.Clear();
			contentids.Value = "";
            string strTag;
            string strtag1;
            string imageDirection = string.Empty;

            if (Request.QueryString["orderbydirection"] == null)
                direction = "asc";
            else if (Request.QueryString["orderbydirection"] == "desc")
            {
                imageDirection = "&nbsp;<img src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/arrowHeadDownGrey.png\" />";
                direction = "asc";
            }
            else if (Request.QueryString["orderbydirection"] == "asc")
            {
                imageDirection = "&nbsp;<img src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/arrowHeadUpGrey.png\" />";
                direction = "desc";
            }
            strTag = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByCategory&orderbydirection=" + direction + "&orderby=";
            strtag1 = "&id=" + _Id + "&LangType=" + _ContentLanguage + _MessageHelper.GetMessage("click to sort msg") + "\">";
            
			System.Web.UI.WebControls.BoundColumn colBound;
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "BOX";
			colBound.HeaderText = "<input type=\"checkbox\" name=\"all\" onclick=\"javascript:checkAll(document.forms[0].all.checked);\">";
			colBound.ItemStyle.Width = Unit.Parse("1");
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			colBound.HeaderStyle.CssClass = "title-header";
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "TITLE";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "Title" + strtag1 + _MessageHelper.GetMessage("generic title") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "Title" + strtag1 + _MessageHelper.GetMessage("generic title") + "</a>";
            }
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "ID";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "ID" + strtag1 + _MessageHelper.GetMessage("generic ID") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "ID" + strtag1 + _MessageHelper.GetMessage("generic ID") + "</a>";
            }
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "STATUS";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "status" + strtag1 + _MessageHelper.GetMessage("generic Status") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "status" + strtag1 + _MessageHelper.GetMessage("generic Status") + "</a>";
            }
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "DATEMODIFIED";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "datemodified")
                colBound.HeaderText = strTag + "DateModified" + strtag1 + _MessageHelper.GetMessage("generic Date Modified") + imageDirection + "</a>";
            else
                colBound.HeaderText = strTag + "DateModified" + strtag1 + _MessageHelper.GetMessage("generic Date Modified") + "</a>";
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "EDITORNAME";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "editor")
                colBound.HeaderText = strTag + "editor" + strtag1 + _MessageHelper.GetMessage("generic Last Editor") + imageDirection + "</a>";
            else
                colBound.HeaderText = strTag + "editor" + strtag1 + _MessageHelper.GetMessage("generic Last Editor") + "</a>";
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			
			DataTable dt = new DataTable();
			DataRow dr;
			
			dt.Columns.Add(new DataColumn("BOX", typeof(string)));
			dt.Columns.Add(new DataColumn("TITLE", typeof(string)));
			dt.Columns.Add(new DataColumn("ID", typeof(long)));
			dt.Columns.Add(new DataColumn("STATUS", typeof(string)));
			dt.Columns.Add(new DataColumn("DATEMODIFIED", typeof(string)));
			dt.Columns.Add(new DataColumn("EDITORNAME", typeof(string)));
			
			int i;
			for (i = 0; i <= (entryList.Count - 1); i++)
			{
				dr = dt.NewRow();
				dr[0] = "";
				if (entryList[i].ContentStatus == "A")
				{
					if (_EkContent.IsAllowed(entryList[i].Id, System.Convert.ToInt32(entryList[i].LanguageId), "content", "delete", 0))
					{
						if (contentids.Value == "")
						{
							contentids.Value = entryList[i].Id.ToString();
						}
						else
						{
							contentids.Value += "," + entryList[i].Id.ToString();
						}
						dr[0] = "<input type=\"checkbox\" onclick=\"checkAllFalse();\" name=\"id_" + entryList[i].Id + "\">";
					}
				}
				
				dr[1] = "<a href=\"content.aspx?LangType=" + entryList[i].LanguageId + "&action=View&id=" + entryList[i].Id + "\" title=\'" + _MessageHelper.GetMessage("generic View") + " \"" + Strings.Replace((string) (entryList[i].Title), "\'", "`", 1, -1, 0) + "\"" + "\'>" + entryList[i].Title + "</a>";
				dr[2] = entryList[i].Id;
				dr[3] = entryList[i].Status;
				dr[4] = entryList[i].DateModified;
				dr[5] = entryList[i].LastEditorLastName;
				dt.Rows.Add(dr);
			}
			
			DataView dv = new DataView(dt);
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && !string.IsNullOrEmpty(Request.QueryString["orderbydirection"]))
            {
                dv.Sort = Request.QueryString["orderby"] + " " + Request.QueryString["orderbydirection"];
            }
            DeleteContentByGategoryGrid.PageSize = this._ContentApi.RequestInformationRef.PagingSize;
			DeleteContentByGategoryGrid.DataSource = dv;
            DeleteContentByGategoryGrid.CurrentPageIndex = _PagingCurrentPageNumber;
			DeleteContentByGategoryGrid.DataBind();
            if (_TotalPagesNumber > 1)
            {
                this.uxPaging.Visible = true;
                this.uxPaging.TotalPages = _TotalPagesNumber;
                this.uxPaging.CurrentPageIndex = _PagingCurrentPageNumber;
            }
            else
            {
                this.uxPaging.Visible = false;
            }
		}
		#endregion
		
		
        #region Smart Form Content
        
        private void Display_DeleteContentByConfiguration()
        {

            List<Ektron.Cms.ContentData> contentResults = null;
            Ektron.Cms.Framework.Core.Content.Content contentManager = new Ektron.Cms.Framework.Core.Content.Content();
            Ektron.Cms.Common.Criteria<ContentProperty> entryCriteria = new Ektron.Cms.Common.Criteria<ContentProperty>();
            entryCriteria.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, _ConfigId);
            entryCriteria.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            entryCriteria.OrderByField = ContentProperty.Title;
            entryCriteria.PagingInfo.RecordsPerPage = _ContentApi.RequestInformationRef.PagingSize;
            entryCriteria.PagingInfo.CurrentPage = _CurrentPageId;
            if (_ShowArchive == false)
            {
                entryCriteria.AddFilter(ContentProperty.IsArchived, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, false);
            }
            else
            {
                entryCriteria.AddFilter(ContentProperty.IsArchived, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, true);
            }
            contentManager.InPreviewMode = true;
            contentResults = contentManager.GetList(entryCriteria);

            _TotalPagesNumber = System.Convert.ToInt32(entryCriteria.PagingInfo.TotalPages);

            DeleteContentByConfigurationToolBar();

            if (contentResults.Count > 0)
            {
                Populate_DeleteContentByConfiguration(contentResults);
                folder_id.Value = Convert.ToString(contentResults.ElementAt(0).FolderId);
            }

        }
        private void Populate_DeleteContentByConfiguration(List<Ektron.Cms.ContentData> contentdata)
        {
            _PagingCurrentPageNumber = System.Convert.ToInt32(this.uxPaging.SelectedPage);
            DeleteContentByGategoryGrid.Controls.Clear();
            contentids.Value = "";
            string strTag;
            string strtag1;
            string imageDirection = string.Empty;

            if (Request.QueryString["orderbydirection"] == null)
                direction = "asc";
            else if (Request.QueryString["orderbydirection"] == "desc")
            {
                imageDirection = "&nbsp;<img src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/arrowHeadDownGrey.png\" />";
                direction = "asc";
            }
            else if (Request.QueryString["orderbydirection"] == "asc")
            {
                imageDirection = "&nbsp;<img src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/arrowHeadUpGrey.png\" />";
                direction = "desc";
            }
            strTag = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderbydirection=" + direction + "&orderby=";
            strtag1 = "&configid=" + _ConfigId + "&LangType=" + _ContentLanguage +  "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">";
            
            System.Web.UI.WebControls.BoundColumn colBound;
            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "BOX";
            colBound.HeaderText = "<input type=\"checkbox\" name=\"all\" onclick=\"checkAll(document.forms[0].all.checked);\">";
            colBound.ItemStyle.Width = Unit.Parse("1");
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            colBound.HeaderStyle.CssClass = "title-header";
            DeleteContentByGategoryGrid.Columns.Add(colBound);

            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "TITLE";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "Title" + strtag1 + _MessageHelper.GetMessage("generic title") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "Title" + strtag1 + _MessageHelper.GetMessage("generic title") + "</a>";
            }
            //colBound.HeaderText = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderby=Title&configid=" + _ConfigId + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">" + _MessageHelper.GetMessage("generic Title") + "</a>";
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.HeaderStyle.CssClass = "title-header";
            colBound.ItemStyle.Wrap = false;
            DeleteContentByGategoryGrid.Columns.Add(colBound);

            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "ID";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "ID" + strtag1 + _MessageHelper.GetMessage("generic ID") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "ID" + strtag1 + _MessageHelper.GetMessage("generic ID") + "</a>";
            }
            //colBound.HeaderText = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderby=ID&configid=" + _ConfigId + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">" + _MessageHelper.GetMessage("generic ID") + "</a>";
            colBound.HeaderStyle.CssClass = "title-header";
            colBound.ItemStyle.Wrap = false;
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            DeleteContentByGategoryGrid.Columns.Add(colBound);

            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "LANGUAGEID";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "langid" + strtag1 + _MessageHelper.GetMessage("generic language") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "langid" + strtag1 + _MessageHelper.GetMessage("generic language") + "</a>";
            }
            //colBound.HeaderText = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderby=langid&configid=" + _ConfigId + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">" + _MessageHelper.GetMessage("generic language") + "</a>";
            colBound.HeaderStyle.CssClass = "title-header";
            colBound.ItemStyle.Wrap = false;
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            DeleteContentByGategoryGrid.Columns.Add(colBound);

            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "STATUS";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "status" + strtag1 + _MessageHelper.GetMessage("generic Status") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "status" + strtag1 + _MessageHelper.GetMessage("generic Status") + "</a>";
            }
            //colBound.HeaderText = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderby=status&configid=" + _ConfigId + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">" + _MessageHelper.GetMessage("generic Status") + "</a>";
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.HeaderStyle.CssClass = "title-header";
            colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            colBound.ItemStyle.Wrap = false;
            DeleteContentByGategoryGrid.Columns.Add(colBound);

            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "DATEMODIFIED";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "datemodified")
                colBound.HeaderText = strTag + "DateModified" + strtag1 + _MessageHelper.GetMessage("generic Date Modified") + imageDirection + "</a>";
            else
                colBound.HeaderText = strTag + "DateModified" + strtag1 + _MessageHelper.GetMessage("generic Date Modified") + "</a>";
            //colBound.HeaderText = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderby=DateModified&configid=" + _ConfigId + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">" + _MessageHelper.GetMessage("generic Date Modified") + "</a>";
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.HeaderStyle.CssClass = "title-header";
            colBound.ItemStyle.Wrap = false;
            DeleteContentByGategoryGrid.Columns.Add(colBound);

            colBound = new System.Web.UI.WebControls.BoundColumn();
            colBound.DataField = "EDITORNAME";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "editor")
                colBound.HeaderText = strTag + "editor" + strtag1 + _MessageHelper.GetMessage("generic Last Editor") + imageDirection + "</a>";
            else
                colBound.HeaderText = strTag + "editor" + strtag1 + _MessageHelper.GetMessage("generic Last Editor") + "</a>";
            //colBound.HeaderText = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByConfiguration&orderby=editor&configid=" + _ConfigId + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">" + _MessageHelper.GetMessage("generic Last Editor") + "</a>";
            colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
            colBound.HeaderStyle.CssClass = "title-header";
            colBound.ItemStyle.Wrap = false;
            DeleteContentByGategoryGrid.Columns.Add(colBound);


            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("BOX", typeof(string)));
            dt.Columns.Add(new DataColumn("TITLE", typeof(string)));
            dt.Columns.Add(new DataColumn("ID", typeof(long)));
            dt.Columns.Add(new DataColumn("LANGUAGEID", typeof(string)));
            dt.Columns.Add(new DataColumn("STATUS", typeof(string)));
            dt.Columns.Add(new DataColumn("DATEMODIFIED", typeof(string)));
            dt.Columns.Add(new DataColumn("EDITORNAME", typeof(string)));

            int i;
            for (i = 0; i <= contentdata.Count - 1; i++)
            {

                dr = dt.NewRow();
                dr[0] = "";
                if ((contentdata.ElementAt(i).Status == "A") || (contentdata.ElementAt(i).Status == "I"))
                {
                    if (_EkContent.IsAllowed(contentdata.ElementAt(i).Id, contentdata.ElementAt(i).LanguageId, "content", "delete", 0))
                    {
                        if (contentids.Value == "")
                        {
                            contentids.Value = contentdata.ElementAt(i).Id.ToString();
                        }
                        else
                        {
                            contentids.Value += "," + contentdata.ElementAt(i).Id;
                        }
                        dr[0] = "<input type=\"checkbox\" onclick=\"checkAllFalse();\" name=\"id_" + contentdata.ElementAt(i).Id + "\">";
                    }
                }

                LocalizationAPI localizationApi = new LocalizationAPI();
                string LanguageDescription = Ektron.Cms.API.JS.Escape(contentdata.ElementAt(i).LanguageDescription);
                dr[1] = "<a href=\"content.aspx?LangType=" + contentdata.ElementAt(i).LanguageId + "&action=View&id=" + contentdata.ElementAt(i).Id + "\" title=\'" + _MessageHelper.GetMessage("generic View") + " \"" + Strings.Replace(contentdata.ElementAt(i).Title, "\'", "`", 1, -1, 0) + "\"" + "\'>" + contentdata.ElementAt(i).Title + "</a>";
                dr[2] = contentdata.ElementAt(i).Id;
                dr[3] = "<a href=\"#ShowTip" + contentdata.ElementAt(i).LanguageDescription + "\" onmouseover=\"ddrivetip(\'" + LanguageDescription + "\',\'ADC5EF\', 100);\" onmouseout=\"hideddrivetip()\" style=\"text-decoration:none;\">" + "<img src=\'" + localizationApi.GetFlagUrlByLanguageID(contentdata.ElementAt(i).LanguageId) + "\' />" + "</a>";
                dr[4] = contentdata.ElementAt(i).Status;
                dr[5] = contentdata.ElementAt(i).DateModified.ToString();
                dr[6] = contentdata.ElementAt(i).EditorLastName;
                dt.Rows.Add(dr);
            }

            DataView dv = new DataView(dt);
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && !string.IsNullOrEmpty(Request.QueryString["orderbydirection"]))
            {
                dv.Sort = Request.QueryString["orderby"] + " " + Request.QueryString["orderbydirection"];
            }
            DeleteContentByGategoryGrid.PageSize = this._ContentApi.RequestInformationRef.PagingSize;
            DeleteContentByGategoryGrid.DataSource = dv;
            DeleteContentByGategoryGrid.CurrentPageIndex = _PagingCurrentPageNumber;
            DeleteContentByGategoryGrid.DataBind();
            if (_TotalPagesNumber > 1)
            {
                this.uxPaging.Visible = true;
                this.uxPaging.TotalPages = _TotalPagesNumber;
                this.uxPaging.CurrentPageIndex = _PagingCurrentPageNumber;
            }
            else
            {
                this.uxPaging.Visible = false;
            }
        }
        private void DeleteContentByConfigurationToolBar()
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder();

            string ConfigurationTitle = string.Empty;
            if (!(Request.QueryString["title"] == null))
            {
                ConfigurationTitle = EkFunctions.UrlDecode(Request.QueryString["title"]);
            }

            txtTitleBar.InnerHtml = _StyleHelper.GetTitleBar(_MessageHelper.GetMessage("lbl remove sf content header") + " " + " \"" + ConfigurationTitle + "\"");
            result.Append("<table></tr>");
            result.Append(_StyleHelper.GetButtonEventsWCaption(_AppImgPath + "../UI/Icons/back.png", (string)("xml_config.aspx?action=ViewXmlConfiguration&id=" + _ConfigId), _MessageHelper.GetMessage("alt back button text"), _MessageHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
            if (_EkContent.IsAllowed(0, 0, "users", "IsAdmin", 0) == true || _ContentApi.IsARoleMember(11, _EkContent.RequestInformation.UserId, false) == true)
            {
                result.Append(_StyleHelper.GetButtonEventsWCaption(_AppImgPath + "../UI/Icons/delete.png", "#", _MessageHelper.GetMessage("btn delete content"), _MessageHelper.GetMessage("btn delete content"), "OnClick=\"return checkDeleteForm();\"", StyleHelper.DeleteButtonCssClass, true));
            }
            result.Append(StyleHelper.ActionBarDivider);
            result.Append("<td>");
            result.Append(_StyleHelper.GetHelpButton( _PageAction, ""));
            result.Append("</td>");
            result.Append("</tr></table>");
            htmToolBar.InnerHtml = result.ToString();
        }
        #endregion
        #region Content
		
		
        private void Display_DeleteContentByCategory()
		{
			
			Ektron.Cms.Common.EkContentCol contentdataAll = new Ektron.Cms.Common.EkContentCol();
			
			DeleteContentByCategoryToolBar();
			
			_PageData = new Collection();
			_PageData.Add(_Id, "FolderID", null, null);
			_PageData.Add(_OrderBy, "OrderBy", null, null);
			if (_ShowArchive == false)
			{
				contentdataAll = _EkContent.GetAllViewableChildContentInfoV5_0(_PageData, _CurrentPageId, _ContentApi.RequestInformationRef.PagingSize, ref _TotalPagesNumber);
			}
			else
			{
				contentdataAll = _EkContent.GetAllViewArchiveContentInfov5_0(_PageData, _CurrentPageId, _ContentApi.RequestInformationRef.PagingSize, ref _TotalPagesNumber);
			}
			
			Ektron.Cms.Common.EkContentCol filteredcontentdata = new Ektron.Cms.Common.EkContentCol();
			foreach (Ektron.Cms.Common.ContentBase item in contentdataAll)
			{
				if (item.ContentSubType != Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderMasterData)
				{
					filteredcontentdata.Add(item);
				}
			}
			contentdataAll = filteredcontentdata;
			
			Populate_DeleteContentByCategory(contentdataAll);
			
			folder_id.Value =Convert.ToString(_Id);
			
		}
		private void Populate_DeleteContentByCategory(EkContentCol contentdata)
		{
            _PagingCurrentPageNumber = System.Convert.ToInt32(this.uxPaging.SelectedPage);
			DeleteContentByGategoryGrid.Controls.Clear();
			contentids.Value = "";
            string strTag;
            string strtag1;
            string imageDirection = string.Empty;

            if (Request.QueryString["orderbydirection"] == null)
                direction = "asc";
            else if (Request.QueryString["orderbydirection"] == "desc")
            {
                imageDirection = "&nbsp;<img src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/arrowHeadDownGrey.png\" />";
                direction = "asc";
            }
            else if (Request.QueryString["orderbydirection"] == "asc")
            {
                imageDirection = "&nbsp;<img src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/arrowHeadUpGrey.png\" />";
                direction = "desc";
            }
            strTag = "<a class=\"title-header\" href=\"content.aspx?action=DeleteContentByCategory&orderbydirection=" + direction + "&orderby=";
            strtag1 = "&id=" + _Id + "&LangType=" + _ContentLanguage + "&showarchive=" + _ShowArchive + "\" title=\"" + _MessageHelper.GetMessage("click to sort msg") + "\">";
            
			System.Web.UI.WebControls.BoundColumn colBound;
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "BOX";
			colBound.HeaderText = "<input type=\"checkbox\" name=\"all\" onclick=\"javascript:checkAll(document.forms[0].all.checked);\">";
			colBound.ItemStyle.Width = Unit.Parse("1");
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			colBound.HeaderStyle.CssClass = "title-header";
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "TITLE";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "Title" + strtag1 + _MessageHelper.GetMessage("generic title") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "Title" + strtag1 + _MessageHelper.GetMessage("generic title") + "</a>";
            }
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "ID";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "ID" + strtag1 + _MessageHelper.GetMessage("generic ID") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "ID" + strtag1 + _MessageHelper.GetMessage("generic ID") + "</a>";
            }
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "STATUS";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "title")
            {
                colBound.HeaderText = strTag + "status" + strtag1 + _MessageHelper.GetMessage("generic Status") + imageDirection + "</a>";
            }
            else
            {
                colBound.HeaderText = strTag + "status" + strtag1 + _MessageHelper.GetMessage("generic Status") + "</a>";
            }
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "DATEMODIFIED";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "datemodified")
                colBound.HeaderText = strTag + "DateModified" + strtag1 + _MessageHelper.GetMessage("generic Date Modified") + imageDirection + "</a>";
            else
                colBound.HeaderText = strTag + "DateModified" + strtag1 + _MessageHelper.GetMessage("generic Date Modified") + "</a>";
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			colBound = new System.Web.UI.WebControls.BoundColumn();
			colBound.DataField = "EDITORNAME";
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && Request.QueryString["orderby"].ToString().ToLower() == "editor")
                colBound.HeaderText = strTag + "editor" + strtag1 + _MessageHelper.GetMessage("generic Last Editor") + imageDirection + "</a>";
            else
                colBound.HeaderText = strTag + "editor" + strtag1 + _MessageHelper.GetMessage("generic Last Editor") + "</a>";
			colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
			colBound.HeaderStyle.CssClass = "title-header";
			colBound.ItemStyle.Wrap = false;
			DeleteContentByGategoryGrid.Columns.Add(colBound);
			
			
			DataTable dt = new DataTable();
			DataRow dr;
			
			dt.Columns.Add(new DataColumn("BOX", typeof(string)));
			dt.Columns.Add(new DataColumn("TITLE", typeof(string)));
			dt.Columns.Add(new DataColumn("ID", typeof(long)));
			dt.Columns.Add(new DataColumn("STATUS", typeof(string)));
			dt.Columns.Add(new DataColumn("DATEMODIFIED", typeof(string)));
			dt.Columns.Add(new DataColumn("EDITORNAME", typeof(string)));
			
			int i;
			for (i = 0; i <= contentdata.Count - 1; i++)
			{
                
				dr = dt.NewRow();
				dr[0] = "";
                if ((contentdata.get_Item(i).ContentStatus == "A") || (contentdata.get_Item(i).ContentStatus == "I"))
				{
                    if (_EkContent.IsAllowed(contentdata.get_Item(i).Id, contentdata.get_Item(i).Language, "content", "delete", 0))
					{
						if (contentids.Value == "")
						{
                            contentids.Value = contentdata.get_Item(i).Id.ToString();
						}
						else
						{
                            contentids.Value += "," + contentdata.get_Item(i).Id;
						}
                        dr[0] = "<input type=\"checkbox\" onclick=\"javascript:checkAllFalse();\" name=\"id_" + contentdata.get_Item(i).Id + "\">";
					}
				}

                dr[1] = "<a href=\"content.aspx?LangType=" + contentdata.get_Item(i).Language + "&action=View&id=" + contentdata.get_Item(i).Id + "\" title=\'" + _MessageHelper.GetMessage("generic View") + " \"" + Strings.Replace(contentdata.get_Item(i).Title, "\'", "`", 1, -1, 0) + "\"" + "\'>" + contentdata.get_Item(i).Title + "</a>";
                dr[2] = contentdata.get_Item(i).Id;
                dr[3] = contentdata.get_Item(i).Status;
                dr[4] = contentdata.get_Item(i).DisplayDateModified;
                dr[5] = contentdata.get_Item(i).LastEditorLname;
				dt.Rows.Add(dr);
			}
			
			DataView dv = new DataView(dt);
            if (!string.IsNullOrEmpty(Request.QueryString["orderby"]) && !string.IsNullOrEmpty(Request.QueryString["orderbydirection"]))
            {
                dv.Sort = Request.QueryString["orderby"] + " " + Request.QueryString["orderbydirection"];
            }
            DeleteContentByGategoryGrid.PageSize = this._ContentApi.RequestInformationRef.PagingSize;
			DeleteContentByGategoryGrid.DataSource = dv;
            DeleteContentByGategoryGrid.CurrentPageIndex = _PagingCurrentPageNumber;
            DeleteContentByGategoryGrid.DataBind();
            if (_TotalPagesNumber > 1)
            {
                this.uxPaging.Visible = true;
                this.uxPaging.TotalPages = _TotalPagesNumber;
                this.uxPaging.CurrentPageIndex = _PagingCurrentPageNumber;
            }
            else
            {
                this.uxPaging.Visible = false;
            }
		}
		private void DeleteContentByCategoryToolBar()
		{
			System.Text.StringBuilder result = new System.Text.StringBuilder();

            txtTitleBar.InnerHtml = _StyleHelper.GetTitleBar(_MessageHelper.GetMessage("lbl remove content header") + " " + " \"" + _FolderData.Name + "\"");
			result.Append("<table></tr>");
			result.Append(_StyleHelper.GetButtonEventsWCaption(_AppImgPath + "../UI/Icons/back.png", (string)("content.aspx?LangType=" + _ContentLanguage + "&action=ViewContentByCategory&id=" + _Id), _MessageHelper.GetMessage("alt back button text"), _MessageHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
			if (_PermissionData.IsAdmin || _ContentApi.IsARoleMemberForFolder_FolderUserAdmin(_FolderData.Id, 0, false) || _EkContent.IsAllowed(_FolderData.Id, _ContentApi.RequestInformationRef.ContentLanguage, "folder", "delete", 0))
			{
				result.Append(_StyleHelper.GetButtonEventsWCaption(_AppImgPath + "../UI/Icons/delete.png", "#", _MessageHelper.GetMessage("btn delete content"), _MessageHelper.GetMessage("btn delete content"), "OnClick=\"javascript:checkDeleteForm();\"", StyleHelper.DeleteButtonCssClass, true));
			}
			result.Append(StyleHelper.ActionBarDivider);
			result.Append("<td>");
			result.Append(_StyleHelper.GetHelpButton((string) (_StyleHelper.GetHelpAliasPrefix(_FolderData) + _PageAction), ""));
			result.Append("</td>");
			result.Append("</tr></table>");
			htmToolBar.InnerHtml = result.ToString();
		}
		
		

		#endregion
		
		#endregion
		
		#region Paging
		
		
		private void VisiblePageControls(bool flag)
		{
			//TotalPages.Visible = flag
			//CurrentPage.Visible = flag
			//ctrlPreviousPage.Visible = flag
			//ctrlNextPage.Visible = flag
			//ctrlLastPage.Visible = flag
			//ctrlFirstPage.Visible = flag
			//PageLabel.Visible = flag
			//OfLabel.Visible = flag
		}
		protected string Util_GetPageURL(int pageid)
		{
			
			return "content.aspx" + Ektron.Cms.Common.EkFunctions.GetUrl(new string[] {"currentpage"}, new string[] {"pageid"}, Request.QueryString).Replace("pageid", (string) (pageid == -1 ? "\' + pageid + \'" : pageid.ToString())).Replace("&amp;", "&");
			
		}
		
		#endregion
		
		#region JS/CSS
		
		private void RegisterJS()
		{
			
			JS.RegisterJS(this, JS.ManagedScript.EktronJS);
			JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaJS);
			JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaHelperJS);
            browseurljs.Text = "content.aspx?LangType=" + this._ContentLanguage + "&action=DeleteContentByCategory&id=" + this._Id + "&currentpage=";
			pagebetweenjs.Text = string.Format(_MessageHelper.GetMessage("js: err page must be between"), _TotalPagesNumber);
		}
		
		private void RegisterCSS()
		{
			Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaCss);
			Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaIeCss, Css.BrowserTarget.LessThanEqualToIE7);
		}
		
		#endregion
		
	}
	

