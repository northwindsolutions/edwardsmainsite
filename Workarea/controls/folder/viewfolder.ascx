<%@ Control Language="C#" AutoEventWireup="true" Inherits="viewfolder" CodeFile="viewfolder.ascx.cs" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="../paging/paging.ascx" %>
<%@ Reference Control="../../DxH/AddSharePointContent.ascx" %>

<%@ Register src="../generic/AjaxLoading.ascx" tagname="AjaxLoading" tagprefix="uc1" %>

<style type="text/css">
    div#EkTB_ajaxWindowTitle
    {
        padding: 0;
    }
</style>
<script type="text/javascript">
    <!--//--><![CDATA[//><!--

    var url_id="<asp:Literal ID="url_id" Runat="server"/>";
    var url_action="<asp:Literal ID="url_action" Runat="server"/>";
    var is_archived="<asp:Literal ID="is_archived" Runat="server"/>"
	    
    // Setting these JS variables in the user control, 
    // and ultimately setting them in Ektron.Workarea.FolderContext object, 
    // for accessing through out the content.aspx page.
    var pasteFolderType = "<asp:Literal runat="server" id="pasteFolderType"/>";
    var pasteFolderId = "<asp:Literal runat="server" id="pasteFolderId"/>";
    var pasteParentId = "<asp:Literal runat="server" id="pasteParentId"/>";
    <asp:literal id="ltr_js" runat="server" />
        
    function LoadLanguage(num){
        top.notifyLanguageSwitch(num, url_id);
        document.forms[0].action="content.aspx?id="+url_id+"&action="+url_action+"&LangType="+num+"&IsArchivedEvent="+is_archived;
        document.forms[0].submit();
        return false;
    }
    function AddNewEvent()
    {
        var contentLang = parseInt(folderjslanguage, 10);
        if(typeof(contentLang) != 'number'){
            contentLang = parseInt(jsContentLanguage, 10);
        }
        var multiSupport = jsEnableMultilingual;
        if ((contentLang < 1) && multiSupport)
        {
            bContinue = confirm("Do you wish to add an event in the default language?");
            if (bContinue){
                contentLang = jsDefaultContentLanguage;
                top.notifyLanguageSwitch(contentLang);
            }else{
                return;
            }
        }
        if(contentLang > 1){
            self.location.href = "content.aspx?id="+url_id+"&action="+url_action+"&LangType="+contentLang+"&showAddEventForm=true";
        }else{
            self.location.href = "content.aspx?id="+url_id+"&action="+url_action+"&LangType="+jsDefaultContentLanguage+"&showAddEventForm=true";
        }
    }
    function AddNewTopic()
    {
        var contentLang = parseInt(folderjslanguage, 10);
        var multiSupport = jsEnableMultilingual;
        if ((contentLang < 1) && multiSupport)
        {
            bContinue = confirm("Do you wish to add topic in the default language?");
            if (bContinue){
                contentLang = jsDefaultContentLanguage;
                top.notifyLanguageSwitch(contentLang);
            }
        }
        if(contentLang > 1){
            self.location.href = "threadeddisc/addedittopic.aspx?action=add&id="+url_id;
        }
    }
    function AddNewPage()
    {
        var contentLang = parseInt(jsContentLanguage, 10);
        var multiSupport = jsEnableMultilingual;
        if ((contentLang < 1) && multiSupport)
        {
            bContinue = confirm("Do you wish to add page in the default language?");
            if (bContinue){
                return jsDefaultContentLanguage;
            }
        }
        return contentLang;
    }
    function AddNewContent(payload, ContType) {
        var bContinue = true;
        if (typeof ContType != "undefined")
        {
            // add multiple
            payload = "<%= _ContentTypeUrlParam %>=" + ContType + "&" + payload;
		    }
		    else
		    {
		        // add single
		        if (null == objSelSupertype)
		        {
		            payload = "<%= _ContentTypeUrlParam %>=" + <%=_CMSContentType_AllTypes%> + "&" + payload;
			        }
			    else
			    {
			        ContType = objSelSupertype.value;
			        payload = "<%= _ContentTypeUrlParam %>=" + ContType + "&" + payload;
				    if (<%=_CMSContentType_AllTypes%> == ContType)
				    {
					    bContinue = confirm("Do you wish to add HTML content?");
				    }
            }
        }
        if (bContinue)
        {
            // when the workarea is first opened, jsContentLanguage is invalid, so we have to use this control's version
            var contentLang = parseInt(folderjslanguage, 10);
            var multiSupport = jsEnableMultilingual;
            if ((contentLang < 1) && multiSupport)
            {
                bContinue = confirm('<asp:literal id="addContentLanguageMessage" runat="server" />');
                if (bContinue){
                    // force language to default:
                    payload = replaceAll(payload, 'LangType=-1&', 'LangType=' + jsDefaultContentLanguage + '&');
                    payload = replaceAll(payload, 'LangType=0&', 'LangType=' + jsDefaultContentLanguage + '&');
                }
            }
        }

        if (bContinue)
        {
            if(ContType==2){
                // FireFox fix (the browser thinks it is already at the
                // target location. IE will obediently jump to the same
                // location but FF/NS won't, so we need to make them see
                // a difference. Only an issue with forms type content):

                var objTop=top.document.getElementById("ek_main");

                if (("object"==typeof(objTop)) && (objTop!= null))
                {
                    top.document.getElementById('ek_main').src = '';
                    top.document.getElementById('ek_main').src = 'cmsform.aspx?action=Addform&' + payload;
                }
                else
                {
                    self.location.href = 'cmsform.aspx?action=Addform&' + payload;
                }
            }
            else if(ContType == 9876)
            {
                var objTop=top.document.getElementById("ek_main");

                if (("object"==typeof(objTop)) && (objTop!= null))
                {
                    top.document.getElementById('ek_main').src = 'edit.aspx?close=false&type=multiple&' + payload;
                }
                else
                {
                    self.location.href =  'edit.aspx?close=false&type=multiple&' + payload;
                }
            }
            else if(ContType == 9875)
            {
                var objTop=top.document.getElementById("ek_main");

                if (("object"==typeof(objTop)) && (objTop!= null))
                {
                    $ektron(document).find(".uxAvatarUploadIframe").attr("src", "stsverselect.aspx?close=false&type=multiple&" + payload+"&NextUrl=edit.aspx");
                    Ektron.Workarea.OfficeVersionSelect.openDialog(); OfficeSelDialogInit();
                    //top.document.getElementById('ek_main').src = 'stsverselect.aspx?close=false&type=multiple&' + payload;
                }
                else
                {
                    self.location.href =  'edit.aspx?close=false&type=multiple&' + payload;
                }
            }
            else if(ContType == 3333){

                var objTop=top.document.getElementById("ek_main");

                if (("object"==typeof(objTop)) && (objTop!= null))
                {
                    top.document.getElementById('ek_main').src = 'commerce/CatalogEntry.aspx?close=false&' + payload;
                }
                else
                {
                    self.location.href =  'commerce/CatalogEntry.aspx?close=false&' + payload;
                }
            }
            else{

                var objTop=top.document.getElementById("ek_main");

                if (("object"==typeof(objTop)) && (objTop!= null))
                {
                    top.document.getElementById('ek_main').src = 'edit.aspx?close=false&' + payload;
                }
                else
                {
                    self.location.href =  'edit.aspx?close=false&' + payload;
                }
            }
        }
        return false;
        }

        function replaceAll(inStr, searchStr, replaceStr){
            var retStr = inStr;
            var index = retStr.indexOf(searchStr);
            while(index>=0){
                retStr = retStr.replace(searchStr, replaceStr);
                index = retStr.indexOf(searchStr);
            }
            return (retStr);
        }

        // Adjusts the navigation-tree frame (if function exists; ie workarea).
        // (True Shows the nav-tree, False hides it)
        function ResizeFrame(val) {
            if ((typeof(top.ResizeFrame) == "function") && top != self) {
                top.ResizeFrame(val);
            }
        }

        window.EditorCleanup = function(){
            try {
                if (window != null) {
                    if (typeof window.RadEditorGlobalArray != 'undefined') {
                        var length = window.RadEditorGlobalArray.length;
                        if (length > 0) {
                            for (var i = 0; i < length; i++) {
                                if (document.getElementById(window.RadEditorGlobalArray[i].Id) == null) {
                                    window.RadEditorGlobalArray.splice(i, 1);
                                    length--;
                                    i--;
                                }
                            }
                        }
                    }
                }
            } catch (ex) { }
        }
        //--><!]]>
        $ektron(document).ready(function()
        {
            $ektron('#DmsMenu').hover(function(){
                $("#file").css("visibility", "hidden");
                $("#view").css("visibility", "hidden");
                $("#delete").css("visibility", "hidden");
                $("#action").css("visibility", "hidden");
            });
        });	
</script>

<style type="text/css">
    </style>
<script type="text/javascript">
    Ektron.ready(function() 
    {
        $("#ReplyDesc" + " a").click(function() {
            this.blur();
            alert('<asp:Literal id="errorLinksDisabled" runat="server" />');
            return false;
        });
        
        //Start: Event Binding for Drag and Select various rows of the content screen.
        
        var container = $ektron("#viewfolder_FolderDataGrid");
      
        var urlPathName = window.location.href;
        
        if(urlPathName.indexOf("action=ViewContentByCategory") != -1 || urlPathName.indexOf("action=viewcontentbycategory") != -1)
        {
            container.bind("mousedown", function(e)
            {
                var ctrl = false;
                var meta = false;
                var clickType = 1;
                if (e.which)
                {
                    clickType = e.which;
                }
                else
                {
                    clickType = event.button;
                }
                ctrl = e.ctrlKey;
                meta = e.metaKey;
                //TODO: UX group to check highlighting is OK when selecting multiple row using mouse button down                
                //                document.onselectstart = function()
                //                {
                //                    return false;
                //                };

                //                document.onmousedown = function()
                //                {
                //                    return false;
                //                };
                // only bind to the left mouse button (clickType == 1)
                if (clickType == 1)
                {
                    //Drag and Select content items.
                    if(!ctrl && !meta)
                    {
                        Ektron.ContentContextMenu.SetClassSelected(e);
                    }
                        //Press + Hold Control key and select individual items.
                    else
                    {
                        Ektron.ContentContextMenu.ToggleClass(e);
                    }
                    var parentTR = $ektron(e.target).closest("tr:not(.title-header)");
                    parentTR.addClass("selected");
                }

            });

            container.bind("mouseup", function(e)
            {
                var clickType = 2;
                if (e.which)
                {
                    clickType = e.which;
                }
                else
                {
                    clickType = event.button;
                }
                var selectedRows = $ektron(Ektron.ContentContextMenu.Content.selectedRows);
                if (selectedRows.length > 0 && clickType == 1)
                {
                    //Context Menu Initialization.
                    Ektron.ContentContextMenu.Init();
                }
                $ektron("#viewfolder_FolderDataGrid").find("tr").unbind("mouseenter");
                Ektron.ContentContextMenu.lastSelected = null;
            });
        }
        //End: Event Binding for Drag and Select various rows of the content screen.
    });
    $ektron.addLoadEvent(function () {
        if ("undefined" !== typeof (top.Ektron.Workarea.FolderContext)) {
            top.Ektron.Workarea.FolderContext.folderType = pasteFolderType;
            top.Ektron.Workarea.FolderContext.folderId = pasteFolderId;
            top.Ektron.Workarea.FolderContext.folderParentId = pasteParentId;
        }
    });

    function OfficeSelDialogClose()
    {
        var jsUploadDlgId = "<asp:literal id="jsUxDialogSelectorTxt" runat="server"/>";
        $ektron(jsUploadDlgId).dialog('close'); 
        return false;
    }
    function OfficeSelDialogInit()
    {
        //place holder,in case needed
    }
    function CompleteDeleteAction(action,jsContentLanguage,jsFormId)
    {
        var bSuccess = false;
        var vFolderId= "<asp:literal id="jsvFolderId" runat="server"/>";
        if ("delete" == action.toLowerCase()) 
        {
            bSuccess = true;
            this.location.href = "/workarea/content.aspx?LangType=" + jsContentLanguage + "&action=submitDelContAction&delete_id=" + jsFormId + "&folder_id=" + vFolderId + "&form_id=" + jsFormId + "&callbackpage=content.aspx&parm1=action&value1=viewcontentbycategory&parm2=id&value2=" + vFolderId;
            window.location.href = this.location.href;
        }
        var jsConfirmDelMapDlgId = "<asp:literal id="jsDxHDeleteMapping" runat="server"/>";
        $ektron(jsConfirmDelMapDlgId).dialog('close'); 
        return false;
    }
</script>
<div id="dhtmltooltip"></div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
    <div class="ektronToolbar" id="htmToolBar" runat="server"></div>
</div>
<%--The class "ektronContextMenuSelect" has been added to only target the view folder grid for the drag and select various contents for cut/copy functionality.--%>
<div class="ektronPageContainer ektronPageGrid ektronContextMenuSelect">
    <CMS:WebCalendar runat="server" ID="calendardisplay" Visible="false" UseUpdatePanel="false" EnableViewState="false"></CMS:WebCalendar>
    <asp:DataGrid ID="FolderDataGrid"
        CssClass="ektronGrid"
        runat="server"
        AllowCustomPaging="True"
        AutoGenerateColumns="False"
        EnableViewState="False">
        <HeaderStyle CssClass="title-header" />
    </asp:DataGrid>
    <uxEktron:Paging ID="uxPaging" runat="server" Visible="false" />
    <asp:Panel ID="pnlThreadedDiscussions" runat="server" Visible="False">
        <table class="ektronGrid">
            <tbody>
                <tr class="title-header">
                    <td class="title-header left"><%=_MessageHelper.GetMessage("lbl Forum")%></td>
                    <td class="title-header"><%=_MessageHelper.GetMessage("lbl discussionforumtopics")%></td>
                    <td class="title-header"><%=_MessageHelper.GetMessage("lbl Posts")%></td>
                    <td class="title-header"><%=_MessageHelper.GetMessage("lbl Last Post")%></td>
                </tr>
                <asp:Repeater ID="CategoryList" runat="server" OnItemDataBound="CategoryList_ItemDataBound">
                    <ItemTemplate>
                        <tr class="ektronSubjectHeader">
                            <td colspan="5" class="left">
                                <%#DataBinder.Eval(Container.DataItem, "name")%>
                                <input type="hidden" runat="server" id="hdn_categoryid" value='<%#DataBinder.Eval(Container.DataItem, "id")%>' />
                            </td>
                        </tr>
                        <asp:Repeater ID="ForumList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 16px;">
                                        <img alt="" src="<%=_ContentApi.AppPath + "images/ui/icons/folderBoard.png" %>" /></td>
                                    <td class="left"><%#DataBinder.Eval(Container.DataItem, "Name")%><br />
                                        <%# DataBinder.Eval(Container.DataItem, "Description") %></td>
                                    <td align="center"><%#DataBinder.Eval(Container.DataItem, "Topics")%></td>
                                    <td align="center"><%#DataBinder.Eval(Container.DataItem, "Posts")%></td>
                                    <td align="center"><%#DataBinder.Eval(Container.DataItem, "LastPosted")%></td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="stripe">
                                    <td style="width: 16px;">
                                        <img alt="" src="<%=_ContentApi.AppPath + "images/ui/icons/folderBoard.png" %>" /></td>
                                    <td class="left"><%#DataBinder.Eval(Container.DataItem, "Name")%><br />
                                        <%# DataBinder.Eval(Container.DataItem, "Description") %></td>
                                    <td align="center"><%#DataBinder.Eval(Container.DataItem, "Topics")%></td>
                                    <td align="center"><%#DataBinder.Eval(Container.DataItem, "Posts")%></td>
                                    <td align="center"><%#DataBinder.Eval(Container.DataItem, "LastPosted")%></td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="addSharePointContent">
    </asp:Panel>
    <ektronUI:Dialog ID="uxDialog" CssClass="EktronAvatarUploadUI" Width="400" Height="300" Modal="true" Title="Select your Microsoft Office version" runat="server" Resizable="false">
        <ContentTemplate>
            <iframe class="uxAvatarUploadIframe xUploadUIControls" frameborder="0" border="0" id="uxAvatarUploadIframe" scrolling="no" runat="server" height="100%" width="100%" src="blank.htm"></iframe>
        </ContentTemplate>
    </ektronUI:Dialog>
    <input type="hidden" runat="server" id="hdnContentLanguage" />
    <input type="hidden" runat="server" id="hdnFolderId" />
    <input type="hidden" runat="server" id="hdnContentId" />
    <ektronUI:JavaScriptBlock ID="uxDialogOpenJavaScript" ExecutionMode="OnEktronReady" runat="server">
        <ScriptTemplate>
            var queryStringValues;
            Ektron.Namespace.Register("Ektron.Workarea.OfficeVersionSelect");
             Ektron.Workarea.OfficeVersionSelect.openDialog = function() {
                $ektron("<%= uxDialog.Selector %>").dialog("open");
            };
            DisplayConfirmDialog = function(queryString){ 
                // Set the querystring values to the server side hidden fields.

                var queryContentId = "&contentId=";
                var queryFolderId = "&folderId=";
                var queryLanguageId = "&LangType=";

                var contIdVal = queryString.split(queryContentId)[1];
                var folderIdVal = queryString.split(queryFolderId)[1];
                var langIdVal = queryString.split(queryLanguageId)[1];
                
                $ektron("#viewfolder_hdnContentId")[0].value = contIdVal.substring(0, contIdVal.indexOf("&"));
                $ektron("#viewfolder_hdnFolderId")[0].value = folderIdVal.substring(0, folderIdVal.indexOf("&"));
                $ektron("#viewfolder_hdnContentLanguage")[0].value = langIdVal.substring(0, langIdVal.indexOf("&"));

                $ektron("<% =uxUpdateContent.Selector%>").dialog('open');
                return false;
            };
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>

    <input type="hidden" runat="server" id="hdnIsPostData" value="true" class="isPostData" name="isPostData" />

    <asp:Literal ID="dropuploader" runat="server" />
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
    function UpdateViewwithSubtype(AssetType, SubType,IsArchived){
        var oForm = document.forms[0];
        var lang = jsContentLanguage;
        var objSelLang = document.getElementById('selLang');
        if (objSelLang != null)
        {
            lang = objSelLang.value;
        }
        var strAction = "content.aspx?id=<%=Request.QueryString["id"]%>" + "&action=<%=Request.QueryString["action"]%>" + "&LangType=" + lang;
	            strAction += "&ContType=" + AssetType;
	            strAction += "&SubType=" + SubType;
	            if(AssetType == 1101 || AssetType == 1102 || AssetType == 1104 || AssetType == 1106 || IsArchived == true)
	                strAction += "&IsArchivedEvent=true";
	            oForm.action = strAction;
	            oForm.submit();
	            return false;
            }
            
            function UpdateView(AssetType){
                return UpdateViewwithSubtype(AssetType, -1,false)
            }
            function UpdateArchiveView(AssetType,IsArchived){
                return UpdateViewwithSubtype(AssetType, -1,IsArchived)
            }


            function resetPostback()
            {
                $ektron("input.isPostData").attr("value", "");
            }

            function CheckWorkOfflineStatus(viewurl,editurl,assetid){
                var objTop=top.document.getElementById("ek_main");

                if (("object"==typeof(objTop)) && (objTop!= null))
                {

                    var dragDropFrame = top.GetEkDragDropObject();
                    if (dragDropFrame != null && dragDropFrame.IsInWorkOffline != undefined) {
                        if(dragDropFrame.IsInWorkOffline(assetid)){
                            if (!confirm("Click Ok to check-in your local file.\n Click Cancel to lose your changes.")) {
                                dragDropFrame.CancelWorkOffline(assetid)
                                document.forms[0].action=viewurl+"&cancelaction=undocheckout";
                            }else{
                                document.forms[0].action=editurl;
                            }
                        }else{
                            if(false == confirm("This content is already checked out to this user.\nIf you edit it at the same time in two locations, some edits may be lost or an error may occur.\nIf the document is open in another location, please close it before continuing.\nClick 'Ok' to Continue."))
                                return;
                            document.forms[0].action=viewurl;
                        }
                    }else{
                        if(false == confirm("This content is already checked out to this user.\nIf you edit it at the same time in two locations, some edits may be lost or an error may occur.\nIf the document is open in another location, please close it before continuing.\nClick 'Ok' to Continue."))
                            return;
                        document.forms[0].action=viewurl;
                    }
                    document.forms[0].__VIEWSTATE.name = 'NOVIEWSTATE';
                    document.forms[0].submit();

                }
            }
            ResizeFrame(1);
            //--><!]]>
    </script>
</div>

<ektronUI:Dialog runat="server" ID="dxhDeleteMappingDMS" AutoOpen="false" Modal="true" Width="550" Height="400" Resizable="false">
    <ContentTemplate>
        <ektronUI:Iframe Src="about:blank" runat="server" ID="iframeDxhDeleteMappingDMS" Width="500" Height="350" />
    </ContentTemplate>
</ektronUI:Dialog>
<ektronUI:Dialog runat="server" ID="uxUpdateContent" CssClass="uxUpdateContent" AutoOpen="false"
    Modal="true" Resizable="false" Width="600">
    <ContentTemplate>
        <table>
            <tr>
                <td style="text-align: left; vertical-align: middle; width: 20%;">
                    <asp:Image runat="server" ID="DXHLogo" ImageUrl="~/Workarea/DxH/images/dxh_logo_60x60.png" />
                </td>
                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;">
                    <asp:Literal ID="ltrConfirmTitle" runat="server" />
                </td>
            </tr>
        </table>
        <p>
            <asp:Literal runat="server" ID="ltrConfirmQuestion" />
        </p>
        <p>
            <asp:Literal runat="server" ID="ltrConfirmationDescription" />
        </p>
    </ContentTemplate>
    <Buttons>
        <ektronUI:DialogButton runat="server" ID="uxUpdateButton" OnClick="uxUpdateButton_Click"></ektronUI:DialogButton>
        <ektronUI:DialogButton runat="server" ID="uxCancel" CloseDialog="true"></ektronUI:DialogButton>
    </Buttons>
</ektronUI:Dialog>


<uc1:AjaxLoading ID="AjaxLoading1" runat="server" />



