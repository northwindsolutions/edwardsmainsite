<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MediaUploaderCommon.ascx.cs" Inherits="MediaUploaderCommon" %>

<script type="text/javascript"  language="JavaScript">
    var lbScope = '<asp:literal id="jsScope" runat="server"/>';
    var sMediaEditor = '<asp:literal id="jsEditorName" runat="server"/>';
    var lDEntryLink = '<asp:literal id="jsDEntrylink" runat="server"/>';
    var flagEnhancedMetaSelect = '';
    var metadataFormTagId = '';
    var separator = '';

    function InsertFunction(insertvalue, title, type, passedId, figureTag)
    {	
        var selectedText = "";
        if ((parent.frames[0].document) && (parent.frames[0].document.forms[0]))
        {
            if (parent.frames[0].document.forms[0].enhancedmetaselect != null)
                flagEnhancedMetaSelect = parent.frames[0].document.forms[0].enhancedmetaselect.value;
            if (parent.frames[0].document.forms[0].metadataformtagid != null)
                metadataFormTagId = parent.frames[0].document.forms[0].metadataformtagid.value;
            if (parent.frames[0].document.forms[0].separator != null)
                separator = parent.frames[0].document.forms[0].separator.value;
        }
		
        if (lbScope == '')
        {
            lbScope = "all";
        }
	
        var ephox = "false";
		
        if ((top.opener != null && typeof top.opener != "undefined" && !top.opener.closed) || "object" == typeof parent.window.radWindow)
        { 
            if (flagEnhancedMetaSelect == '')
            {
                var sEphoxFieldType = "undefined";
                try{
                    sEphoxFieldType = typeof(top.opener.document.forms[0].ephox);
                }
                catch(e){}
				
                if (sEphoxFieldType.toLowerCase() != "undefined")
                {
                    if (typeof(top.opener.document.forms[0].ephox.value) != "undefined"){
                        ephox = top.opener.document.forms[0].ephox.value.toLowerCase();
                    }
                    if (ephox == "true")
                    {
                        if (("undefined" != typeof top.opener.document.forms[0].selectedtext)
							&& ("undefined" != typeof top.opener.document.forms[0].selectedtext.value))
                        {
                            selectedText = top.opener.document.forms[0].selectedtext.value;
                        }
                    }
                }
                var bContentDesigner = false;
                try
                {
                    var args = parent.GetDialogArguments();
                    if(args)              
                        bContentDesigner = true;
                      
                }
                catch(e)
                {
                }

                if (!document.all && document.getElementById) {
                    var typename = "function";
                }
                else {
                    var typename = "object";
                }
                if (ephox != "true"){
                    if (sMediaEditor == "") {
                        var textsection = "content_html";
                    }
                    else {
                        var textsection = sMediaEditor;
                    }
                }
                if (type == "images")
                {
                    if (ephox == "true")
                    {	
                        parent.opener.insertImage(insertvalue, title);
                        parent.close();
                    }
                    else if (Ektron.Namespace.Exists("parent.window.parent.Ektron.Library.Media.AcceptInsert"))
                    {
                        // add image to the products' media tab
                        var imagePath = insertvalue, figure = '', id = ""; 
                        if (imagePath.indexOf("?") > 0)
                            imagePath = imagePath.substr(0, imagePath.indexOf("?"));
                        if (null != passedId && "undefined" != typeof passedId) {
                            id = passedId;
                        }
                        if (null != figureTag && "undefined" != typeof figureTag) {
                            figure = figureTag;
                        }
                        var newImageObj = {"id":id,"title":title,"altText":title,"path":imagePath,"width":"0","height":"0","figureTag":figure};

                        parent.window.parent.Ektron.Library.Media.AcceptInsert(newImageObj);
                    }
                    else if (parent.location.href.indexOf("&productmode=true") >= 0)
                    {
                        // add image to the products' media tab
                        var imagePath = insertvalue;
                        if (imagePath.indexOf("?") > 0)
                            imagePath = imagePath.substr(0, imagePath.indexOf("?"));
                        var id = ""; 
                        if (null != passedId && "undefined" != typeof passedId)
                            id = passedId;
                        var newImageObj = {"id":id,"title":title,"altText":title,"path":imagePath,"width":"0","height":"0"};
				        
                        var iframeLocation = "./Commerce/CatalogEntry/Media/AddLibraryImage.aspx?productTypeId=" + getQueryVariable("productTypeId") + "&imageId=" + newImageObj.id;
                        var iframe = $ektron("<iframe src=\"" + iframeLocation + "\" style=\"position:absolute;margin-left:-10000px;\" />");
                        $ektron("body").append(iframe);
                    }
                    else if (true == bContentDesigner)
                    {
                        parent.CloseRadDlg(insertvalue, title, type);	// content designer callback function will handle the rest. 
                    }
                    else 
                    {
                        parent.opener.eWebEditPro.instances[textsection].insertMediaFile(insertvalue,false,title,"IMAGE",0,0);
                        parent.close();
                    }
                }
                else if ((type == "files") && (lbScope != "all")) 
                {
                    if (ephox == "true")
                    {	
                        parent.opener.InsertHTMLAtCursor(escape("<a href=\"" + insertvalue + "\" alt=\"" + title + "\" title=\"" + title + "\">"));
                        parent.close();
                    }
                    else if (Ektron.Namespace.Exists("parent.window.parent.Ektron.Library.Media.AcceptLink"))
                    {
                        parent.window.parent.Ektron.Library.Media.AcceptLink({"href": insertvalue, "title": title, "type": type});
                    }
                    else if (true == bContentDesigner)
                    {
                        parent.CloseRadDlg(insertvalue, title, type);	// content designer callback function will handle the rest.
                    }
                    else 
                    {				
                        parent.opener.eWebEditProUseFileLink(textsection, insertvalue, title, true);
                        parent.close();
                    }
                }
                else if (type == "hyperlinks"){
                    if ((insertvalue.substring(0, 7) != "http://") && (insertvalue.substring(0, 8) != "https://") && (insertvalue.substring(0, 1) != "/")) {
                        insertvalue = "http://" + insertvalue; 
                    }
                    if (ephox == "true"){	
                        if (selectedText == "") {
                            parent.opener.insertOther(insertvalue,title);
                        }
                        else {
                            parent.opener.insertOther(insertvalue,selectedText);
                        }
                        parent.close();
                    }
                    else if (Ektron.Namespace.Exists("parent.window.parent.Ektron.Library.Media.AcceptLink"))
                    {
                        parent.window.parent.Ektron.Library.Media.AcceptLink({"href": insertvalue, "title": title, "type": type});
                    }
                    else if (true == bContentDesigner)
                    {
                        parent.CloseRadDlg(insertvalue, title, type);	// content designer callback function will handle the rest.
                    }
                    else {
                        if ('1' == lDEntryLink) {
                            parent.opener.eWebEditProUseFileLink(textsection, insertvalue, title, true);
                        }
                        else {
                            selectedText = parent.opener.eWebEditPro[textsection].getSelectedText();
                            if (selectedText == "") {
                                selectedText = parent.opener.eWebEditPro[textsection].getSelectedHTML();
                            }
                            if (selectedText == "") {
                                var stuff = '<a href="' + insertvalue + '" title="' + title + '">' + title + '</a>';
                            }
                            else {
                                var stuff = '<a href="' + insertvalue + '" title="' + title + '">' + selectedText + '</a>';
                            }
                            parent.opener.eWebEditPro[textsection].pasteHTML(stuff);
                        }
                        parent.close();
                    }
                }
                else if (type == "quicklinks") { 
                    if (ephox == "true"){	
                        if (selectedText == "") {
                            parent.opener.insertOther(insertvalue,title);
                        }
                        else {
                            parent.opener.insertOther(insertvalue,selectedText);
                        }
                        parent.close();
                    }
                    else if (Ektron.Namespace.Exists("parent.window.parent.Ektron.AdvancedInspector.Link.Parent"))
                    {   //// Check Namespace for AdvancedInspector link plugin to return quicklink to basic/advanced
                        if (parent.window.parent.Ektron.AdvancedInspector.Link.Parent === 'advanced') {
                            if (Ektron.Namespace.Exists("parent.window.parent.Ektron.AdvancedInspector.Link.Advanced.AcceptLink"))
                            {
                                parent.window.parent.Ektron.AdvancedInspector.Link.Advanced.AcceptLink({"href": insertvalue, "title": title, "type": type});
                            }
                        }
                        if (parent.window.parent.Ektron.AdvancedInspector.Link.Parent === 'basic') {
                            if (Ektron.Namespace.Exists("parent.window.parent.Ektron.AdvancedInspector.Link.Basic.AcceptLink"))
                            {
                                parent.window.parent.Ektron.AdvancedInspector.Link.Basic.AcceptLink({"href": insertvalue, "title": title, "type": type});
                            }
                        }
                    }
                    else if (Ektron.Namespace.Exists("parent.window.parent.Ektron.Library.Media.AcceptLink"))
                    {
                        parent.window.parent.Ektron.Library.Media.AcceptLink({"href": insertvalue, "title": title, "type": type});
                    }
                    else if (true == bContentDesigner)
                    {
                        parent.CloseRadDlg(insertvalue, title, type);	// content designer callback function will handle the rest.
                        window.location.href = window.location.href.replace("mediauploader.aspx?action=uploadlibraryitem", "mediainsert.aspx?action=ViewLibraryByCategory");
                    }
                    else 
                    {
                        if ('1' == lDEntryLink) 
                        {
                            parent.opener.eWebEditProUseFileLink(textsection, insertvalue, title, true);
                        }
                        else 
                        {
                            selectedText = parent.opener.eWebEditPro[textsection].getSelectedText();
                            if (selectedText == "") {
                                selectedText = parent.opener.eWebEditPro[textsection].getSelectedHTML();
                            }
                            if (selectedText == "") {
                                var stuff = '<a href="' + insertvalue + '" title="' + title + '">' + title + '</a>';
                            }
                            else {
                                var stuff = '<a href="' + insertvalue + '" title="' + title + '">' + selectedText + '</a>';
                            }					
                            parent.opener.eWebEditPro[textsection].pasteHTML(stuff);
                        }
                        parent.close();
                    }
                }
                else {
                    if (ephox == "true"){	
                        if (selectedText == ""){
                            parent.opener.insertOther(insertvalue,title);
                        }
                        else{
                            parent.opener.insertOther(insertvalue,selectedText);
                        }
                        parent.close();
                    }
                    else if (Ektron.Namespace.Exists("parent.window.parent.Ektron.Library.Media.AcceptLink"))
                    {
                        parent.window.parent.Ektron.Library.Media.AcceptLink({"href": insertvalue, "title": title, "type": type});
                    }
                    else if (true == bContentDesigner)
                    {
                        parent.CloseRadDlg(insertvalue, title, type);	// content designer callback function will handle the rest.
                        window.location.href = window.location.href.replace("mediauploader.aspx?action=uploadlibraryitem", "mediainsert.aspx?action=ViewLibraryByCategory");
                    }
                    else 
                    {	
                        if ('1' == lDEntryLink) 
                        {
                            parent.opener.eWebEditProUseFileLink(textsection, insertvalue, title, true);
                        }
                        else 
                        {
                            selectedText = parent.opener.eWebEditPro[textsection].getSelectedText();
                            if (selectedText == "") {
                                selectedText = parent.opener.eWebEditPro[textsection].getSelectedHTML();
                            }
                            if (selectedText == "") {
                                var stuff = '<a href="' + insertvalue + '" title="' + title + '">' + title + '</a>';
                            }
                            else {
                                var stuff = '<a href="' + insertvalue + '" title="' + title + '">' + selectedText + '</a>';
                            }
                            parent.opener.eWebEditPro[textsection].pasteHTML(stuff);
                        }
                        parent.close();
                    }
                }
            }
            else 
            {
                if ((parent.frames) && (parent.frames[2]) && ("MediaSelect" == parent.frames[2].name))
                {
                    parent.frames[2].metaselect_addMetaSelectRow(insertvalue, title, metadataFormTagId, separator);
                }
                else
                {
                    // used by Enhanced Metadata selection:
                    if ((parent.opener.ek_ma_ReturnMediaUploaderValue != null) && (typeof(parent.opener.ek_ma_ReturnMediaUploaderValue) != 'undefined'))
                    {
                        if ((metadataFormTagId != '') && (metadataFormTagId > 0))
                        {
                            parent.opener.ek_ma_ReturnMediaUploaderValue(insertvalue, title, metadataFormTagId);
                            parent.opener.CloseChildPage();
                        }
                    }
                    parent.close();
                }
            }
        }
        else 
        {
            alert("<asp:literal id="jsEditorClosed" runat="server"/>");
            return false;
        }		
    }
	
    function getQueryVariable(variable) { 
        var query = parent.location.search.substring(1); 
        var vars = query.split("&"); 
        for (var i=0;i<vars.length;i++) { 
            var pair = vars[i].split("="); 
            if (pair[0] == variable) { 
                return pair[1]; 
            } 
        } 
    } 

    function InserValueToField(filename, previewPath, sitePath, retFieldID, optionalEvent)
    {
        var thumbnail = "",
            openedAsDaughterWindow = parent.opener,
	        retField = openedAsDaughterWindow ? parent.opener.document.getElementById(retFieldID) : top.document.getElementById(retFieldID),
            folderId,
	        libraryId;

        if ('undefined' !== typeof(optionalEvent)) {
            folderId = optionalEvent.fid;
            libraryId = optionalEvent.lid;
        }
        else if ('undefined' !== typeof(selectedLibraryId) && 'undefined' !== typeof(selectedFolderId)) {
            folderId = selectedFolderId;
            libraryId = selectedLibraryId;
        }

        if (eval(retField) != null)
        {
            if (filename.indexOf(sitePath) == 0) {
                retField.value = filename.replace(sitePath, '');
            } 
            else
            {          
                retField.value = filename;
            }
            thumbnail = eval(openedAsDaughterWindow ? parent.opener.document.getElementById(retFieldID + "_thumb") : top.document.getElementById(retFieldID + "_thumb"));
            if (thumbnail != null)
            {
                thumbnail.src = previewPath;  
            }
            if (openedAsDaughterWindow) {
                parent.close();
            }
            else {
                if (top.$ektron) {
                    top.$ektron(top.document).trigger('mediaManagerValueInserted', [{
                        filename: filename,
                        previewPath: previewPath,
                        sitePath: sitePath,
                        contentId: parseInt(libraryId, 10),
                        folderId: parseInt(folderId, 10)
                    }]);
                }
            }
        }
        return false;
    }
	
    function CommerceMediaTabAddLibraryImage(newImageObj)
    {
        parent.opener.Ektron.Commerce.MediaTab.Images.addNewImage(newImageObj);
        parent.close();
    }
</script>
 