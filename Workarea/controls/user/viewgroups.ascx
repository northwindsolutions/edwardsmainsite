<%@ Control Language="C#" AutoEventWireup="true" Debug="true" Inherits="viewgroups" CodeFile="viewgroups.ascx.cs" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="../paging/paging.ascx" %>
<asp:Literal ID="PostBackPage" runat="server" />
<script language="javascript" type="text/javascript">
    function resetPostback() {
        document.forms[0].user_isPostData.value = "";
    }
    function searchusergroup() {
        var sSearchTerm = document.forms[0].txtSearch.value;
        var _path = window.location.href;
        if (_path.indexOf('&Searchby') != -1)
                 _path = _path.substr(0,_path.indexOf('&Searchby'));
        window.location.href = _path + "&Searchby=" + document.forms[0].txtSearch.value;
        return true;
    }
    function CheckForReturn(e) {
        var keynum;
        var keychar;

        if (window.event) // IE
        {
            keynum = e.keyCode
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which
        }

        if (keynum == 13) {
            document.getElementById('btnSearch').focus();
        }
    }

</script>
<div id="dhtmltooltip"></div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
    <div class="ektronToolbar" id="htmToolBar" runat="server"></div>
</div>
<div class="ektronPageContainer ektronPageGrid">
    <asp:DataGrid ID="MapCMSGroupToADGrid" 
        runat="server" 
        AutoGenerateColumns="False"
        Width="100%" 
        EnableViewState="False" 
        CssClass="ektronGrid" 
        AllowCustomPaging="True" PagerStyle-Visible="False"
        GridLines="None">
        <HeaderStyle CssClass="title-header" />
    </asp:DataGrid> 
    <uxEktron:Paging ID="uxPaging" runat="server" visible="false" />
    <asp:Literal ID="ltr_message" runat="server" />
    <input type="hidden" id="groupMarker" name="groupMarker" value="true"/>
</div>
<input type="hidden" runat="server" id="isPostData" value="true" name="isPostData" />