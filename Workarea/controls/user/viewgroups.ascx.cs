using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Data;
using System.Web.Caching;
using System.Xml.Linq;
using System.Web.UI;
using System.Diagnostics;
using System.Web.Security;
using System;
using System.Text;
using Microsoft.VisualBasic;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.Profile;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.Common;

public partial class viewgroups : System.Web.UI.UserControl
{


    protected LanguageData[] language_data;
    protected UserGroupData user_data;
    protected PermissionData security_data;
    protected SiteAPI m_refSiteApi = new SiteAPI();
    protected UserAPI m_refUserApi = new UserAPI();
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected StyleHelper m_refStyle = new StyleHelper();
    protected EkMessageHelper m_refMsg;
    protected string AppImgPath = "";
    protected DomainData[] domain_data;
    protected string UserName = "";
    protected int ContentLanguage = -1;
    protected string FirstName = "";
    protected string LastName = "";
    protected string Domain = "";
    protected SettingsData setting_data;
    //protected UserGroupData[] usergroup_data;
    protected EmailHelper m_refEmail = new EmailHelper();
    protected string search = "";
    protected string strFilter = "";
    protected string rp = "";
    protected string e1 = "";
    protected string e2 = "";
    protected string f = "";
    protected int m_intGroupType = 0;
    protected long m_intGroupId = -1;
    private bool _LoadedInIframe = false;
    private string _SubscriptionGroupType;
    private bool _ActiveDirectory = false;
    protected PagingInfo _pagingInfo = new Ektron.Cms.PagingInfo();
    protected int m_intCurrentPage = 1;
    protected int m_intTotalPages = 1;
    Ektron.Cms.User.IUserGroup userGroupManager = null;
    string searchCriteria = "";
    protected string m_strDirection = "asc";
    protected string OrderBy = "";
    //Note: this is used by Commerce >> Items Tab >> Subscriptions >> Membership
    private bool LoadedInIframe
    {
        get
        {
            return _LoadedInIframe;
        }
        set
        {
            _LoadedInIframe = value;
        }
    }

    private string SubscriptionGroupType
    {
        get
        {
            return _SubscriptionGroupType;
        }
        set
        {
            _SubscriptionGroupType = value;
        }
    }

    public bool ActiveDirectory
    {
        get
        {
            return _ActiveDirectory;
        }
        set
        {
            _ActiveDirectory = value;
        }
    }

    #region Load
    private void Page_Init(System.Object sender, System.EventArgs e)
    {
        if ((!(Request.QueryString["grouptype"] == null)) && (Request.QueryString["grouptype"] != ""))
        {           
            if (!string.IsNullOrEmpty(Request.QueryString["RequestedBy"]))
            {
                this.LoadedInIframe = System.Convert.ToBoolean((Request.QueryString["RequestedBy"] == "EktronCommerceItemsSusbscriptionsMembership") ? true : false);
                this._SubscriptionGroupType = (string)((Request.QueryString["grouptype"] == "0") ? "cms" : "membership");
            }
        }
    }

    private void Page_Load(System.Object sender, System.EventArgs e)
    {
        RegisterResources();
        if ((!(Request.QueryString["grouptype"] == null)) && (Request.QueryString["grouptype"] != ""))
        {
            m_intGroupType = Convert.ToInt32(Request.QueryString["grouptype"]);
        }
        if ((!(Request.QueryString["groupid"] == null)) && (Request.QueryString["groupid"] != ""))
        {
            m_intGroupId = Convert.ToInt64(Request.QueryString["groupid"]);
        }
        else if ((!(Request.QueryString["id"] == null)) && (Request.QueryString["id"] != ""))
        {
            m_intGroupId = Convert.ToInt64(Request.QueryString["id"]);
        }
        m_strDirection = Request.QueryString["direction"];

        if (m_strDirection == "asc")
        {
            m_strDirection = "desc";
        }
        else
        {
            m_strDirection = "asc";
        }
        searchCriteria = "";
        if (!string.IsNullOrEmpty(Page.Request.QueryString["Searchby"]))
        {
            searchCriteria = Page.Request.QueryString["Searchby"];
        }
        Utilities.SetLanguage(m_refSiteApi);
        m_refMsg = m_refSiteApi.EkMsgRef;
        AppImgPath = m_refSiteApi.AppImgPath;
        ContentLanguage = m_refSiteApi.ContentLanguage;
        userGroupManager = Ektron.Cms.ObjectFactory.GetUserGroup();
        if (!_ActiveDirectory)
        {
            ViewUserGroups();
        }
    }
    private string Quote(string KeyWords)
    {
        string result = KeyWords;
        if (KeyWords.Length > 0)
        {
            result = KeyWords.Replace("\'", "\'\'");
        }
        return result;
    }
    #endregion

    #region ViewUserGroup
    public bool ViewUserGroups()
    {
        m_intCurrentPage = System.Convert.ToInt32(this.uxPaging.SelectedPage);
        //TOOLBAR
        ViewUserGroupsToolBar();
        if (Request.QueryString["OrderBy"] == "" || Request.QueryString["OrderBy"] == string.Empty || Request.QueryString["OrderBy"] == null)
        {
            OrderBy = "name";
        }
        else
        {
            OrderBy = Request.QueryString["OrderBy"];
        }
        List<UserGroupData> _Userdata = new List<UserGroupData>();
        Ektron.Cms.UserGroupCriteria criteria = new UserGroupCriteria();

        criteria.PagingInfo.RecordsPerPage = m_refContentApi.RequestInformationRef.PagingSize;
        criteria.PagingInfo.CurrentPage = m_intCurrentPage + 1;
        if (m_strDirection == "asc")
            criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
        else
            criteria.OrderByDirection = EkEnumeration.OrderByDirection.Descending;
        switch (OrderBy)
        {
            case "name": 
                        criteria.OrderByField = UserGroupProperty.Name;
                        break;
            case "id":
                        criteria.OrderByField = UserGroupProperty.Id;
                        break;
        }
                
        if (searchCriteria != "")
        {
            criteria.AddFilter(UserGroupProperty.Name, Ektron.Cms.Common.CriteriaFilterOperator.Contains, searchCriteria);
        }
        criteria.AddFilter(UserGroupProperty.GroupType, CriteriaFilterOperator.EqualTo, m_intGroupType);

        _Userdata = userGroupManager.GetList(criteria);
        m_intTotalPages = System.Convert.ToInt32(criteria.PagingInfo.TotalPages);
        
        Populate_ViewUserGroups(_Userdata);

        return true;

    }
    private void Populate_ViewUserGroups(List<UserGroupData> _Userdata)
    {
        string Icon = "users.png";
        if (m_intGroupType == 1)
        {
            Icon = "usersMembership.png";
        }

        string HeaderText = "<a href=\"users.aspx?action=viewallgroups&grouptype=" + m_intGroupType + "&direction=" + m_strDirection + "&OrderBy={0}&LangType=" + ContentLanguage + "\" title=\"" + m_refMsg.GetMessage("click to sort msg") + "\">{1}</a>";

        System.Web.UI.WebControls.BoundColumn colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "GROUPNAME";
        colBound.HeaderText = string.Format(HeaderText, "name", m_refMsg.GetMessage("generic User Group Name")); //m_refMsg.GetMessage("generic User Group Name");
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(65);
        colBound.ItemStyle.Width = Unit.Percentage(65);
        MapCMSGroupToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "GROUPID";
        colBound.HeaderText = string.Format(HeaderText, "id", m_refMsg.GetMessage("lbl Group ID")); //m_refMsg.GetMessage("lbl Group ID"); 
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(5);
        colBound.ItemStyle.Width = Unit.Percentage(5);
        MapCMSGroupToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "COUNT";
        colBound.HeaderText = m_refMsg.GetMessage("generic Number of Users");
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.CssClass = "title-header";
        MapCMSGroupToADGrid.Columns.Add(colBound);
        if (m_intGroupType == 0 && string.IsNullOrEmpty(this.SubscriptionGroupType))
        {
            if (m_refEmail.IsLoggedInUsersEmailValid())
            {
                colBound = new System.Web.UI.WebControls.BoundColumn();
                colBound.DataField = "EMAIL";
                colBound.HeaderText = (string)("<a href=\"#\" onclick=\"ToggleEmailCheckboxes();\" title=\"" + m_refMsg.GetMessage("alt send email to all") + "\"><input type=\"checkbox\"></a>&nbsp;" + m_refMsg.GetMessage("generic all"));
                colBound.ItemStyle.Wrap = false;
                colBound.HeaderStyle.CssClass = "title-header";
                colBound.HeaderStyle.Width = Unit.Percentage(10);
                colBound.ItemStyle.Width = Unit.Percentage(10);
                MapCMSGroupToADGrid.Columns.Add(colBound);
            }
        }
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add(new DataColumn("GROUPNAME", typeof(string)));
        dt.Columns.Add(new DataColumn("GROUPID", typeof(string)));
        dt.Columns.Add(new DataColumn("COUNT", typeof(string)));
        if (m_intGroupType == 0)
        {
            if (m_refEmail.IsLoggedInUsersEmailValid())
            {
                dt.Columns.Add(new DataColumn("EMAIL", typeof(string)));
            }
        }
        if (!(_Userdata == null))
        {
            for (int i = 0; i <= _Userdata.Count - 1; i++)
            {
                dr = dt.NewRow();
                if (this.LoadedInIframe == true)
                {
                    //This is required for Commcerce >> Items Tab >> Subscriptions >> Membership.ascx
                    dr["GROUPNAME"] = "<a href=\"#AddGroup\" onclick=\"parent.Ektron.Commerce.CatalogEntry.Items.Subscriptions.Membership.add(\'" + this.SubscriptionGroupType + "\', \'" + _Userdata[i].Id.ToString() + "\', \'" + _Userdata[i].Name.Replace("\'", "`") + "\');return false;\"><img src=\"" + AppImgPath + "../UI/Icons/" + Icon + "\" align=\"absbottom\" title=\'" + m_refMsg.GetMessage("view user group msg") + " \"" + _Userdata[i].Name.Replace("\'", "`") + "\"\' alt=\'" + m_refMsg.GetMessage("view user group msg") + " \"" + _Userdata[i].Name.Replace("\'", "`") + "\"\'></a>&nbsp;<a href=\"#AddGroup\" onclick=\"parent.Ektron.Commerce.CatalogEntry.Items.Subscriptions.Membership.add(\'" + this.SubscriptionGroupType + "\', \'" + _Userdata[i].Id.ToString() + "\', \'" + _Userdata[i].Name.Replace("\'", "`") + "\');return false;\">" + _Userdata[i].Name.Replace("\'", "`") + "</a>";
                }
                else
                {
                    dr["GROUPNAME"] = "<a href=\"users.aspx?action=viewallusers&grouptype=" + m_intGroupType + "&LangType=" + ContentLanguage + "&groupid=" + _Userdata[i].Id.ToString() + "&id=" + _Userdata[i].Id.ToString() + "\" title=\'" + m_refMsg.GetMessage("view user group msg") + " \"" + _Userdata[i].Name.Replace("\'", "`") + "\"\'><img src=\"" + AppImgPath + "../UI/Icons/" + Icon + "\" align=\"absbottom\" title=\'" + m_refMsg.GetMessage("view user group msg") + " \"" + _Userdata[i].Name.Replace("\'", "`") + "\"\' alt=\'" + m_refMsg.GetMessage("view user group msg") + " \"" + _Userdata[i].Name.Replace("\'", "`") + "\"\'></a>&nbsp;<a href=\"users.aspx?action=viewallusers&grouptype=" + m_intGroupType + "&LangType=" + ContentLanguage + "&groupid=" + _Userdata[i].Id.ToString() + "&id=" + _Userdata[i].Id.ToString() + "\" title=\'" + m_refMsg.GetMessage("view user group msg") + " \"" + _Userdata[i].Name.Replace("\'", "`") + "\"\'>" + _Userdata[i].Name.Replace("\'", "`") + "</a>";
                }

                dr["GROUPID"] = _Userdata[i].Id;
                dr["COUNT"] = _Userdata[i].UserCount;
                if (m_intGroupType == 0 && string.IsNullOrEmpty(this.SubscriptionGroupType))
                {
                    if (m_refEmail.IsLoggedInUsersEmailValid())
                    {

                        dr["EMAIL"] = "<input type=\"checkbox\" name=\"emailcheckbox_" + _Userdata[i].Id.ToString() + "\" ID=\"Checkbox1\">";
                        dr["EMAIL"] += "<a href=\"#\" onclick=\"SelectEmail(\'emailcheckbox_" + _Userdata[i].Id.ToString() + "\');return false\">";
                        dr["EMAIL"] += m_refEmail.MakeEmailGraphic() + "</a>";
                    }
                }
                dt.Rows.Add(dr);
            }
        }
        DataView dv = new DataView(dt);
        MapCMSGroupToADGrid.PageSize = this.m_refContentApi.RequestInformationRef.PagingSize;
        MapCMSGroupToADGrid.DataSource = dv;
        MapCMSGroupToADGrid.CurrentPageIndex = m_intCurrentPage;
        MapCMSGroupToADGrid.DataBind();
        if (m_intTotalPages > 1)
        {
            this.uxPaging.Visible = true;
            this.uxPaging.TotalPages = m_intTotalPages;
            this.uxPaging.CurrentPageIndex = m_intCurrentPage;
        }
        else
        {
            this.uxPaging.Visible = false;
        }
    }
    private void ViewUserGroupsToolBar()
    {
        System.Text.StringBuilder result = new System.Text.StringBuilder();
        txtTitleBar.InnerHtml = m_refStyle.GetTitleBar(m_refMsg.GetMessage("view user groups msg"));
        result.Append("<table><tr>");
        bool addHelpDivider = false;
        if (m_intGroupType == 0)
        {
            bool primaryCssApplied = false;

            if (string.IsNullOrEmpty(this.SubscriptionGroupType))
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/add.png", "users.aspx?action=addusergroup&grouptype=" + m_intGroupType + "&LangType=" + ContentLanguage + "", m_refMsg.GetMessage("alt add button text (user group2)"), m_refMsg.GetMessage("btn add user group"), "", StyleHelper.AddButtonCssClass, !primaryCssApplied));

                primaryCssApplied = true;
                addHelpDivider = true;
            }

            if (m_refEmail.IsLoggedInUsersEmailValid() && string.IsNullOrEmpty(this.SubscriptionGroupType))
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/email.png", "#", m_refMsg.GetMessage("alt send email to selected groups"), m_refMsg.GetMessage("btn email"), "onclick=\"LoadEmailChildPageEx();\"", StyleHelper.EmailButtonCssClass, !primaryCssApplied));

                primaryCssApplied = true;
                addHelpDivider = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(this.SubscriptionGroupType))
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/add.png", "users.aspx?action=addusergroup&grouptype=" + m_intGroupType + "&OrderBy=" + Request.QueryString["OrderBy"] + "&LangType=" + ContentLanguage + "", m_refMsg.GetMessage("alt add button text (user group2)"), m_refMsg.GetMessage("btn add membership usergroup"), "", StyleHelper.AddButtonCssClass, true));

                addHelpDivider = true;
            }
        }
        result.Append(StyleHelper.ActionBarDivider);
        
        result.Append("<td>");
        result.Append("<input type=text class=\"ektronTextMedium\" id=txtSearch name=txtSearch value=\"" + searchCriteria + "\" onkeydown=\"CheckForReturn(event)\" />");
        result.Append(" <input type=button value=" + m_refMsg.GetMessage("btn search") + " id=btnSearch name=btnSearch onclick=\"searchusergroup();\" class=\"ektronWorkareaSearch\" title=\"" + m_refMsg.GetMessage("lbl search groups") + "\" />");
        result.Append("</td>");

        if (addHelpDivider)
        {
            result.Append(StyleHelper.ActionBarDivider);
        }
        
        result.Append("<td>");
        if (string.IsNullOrEmpty(this.SubscriptionGroupType))
        {
            if (m_intGroupType == 0)
            {
                result.Append(m_refStyle.GetHelpButton("ViewUserGroupsToolBar", ""));
            }
            else
            {
                result.Append(m_refStyle.GetHelpButton("ViewMembershipGroups", ""));
            }
        }
        result.Append("</td>");
        result.Append("</tr></table>");
        htmToolBar.InnerHtml = result.ToString();
    }
    #endregion

    #region Grid Events
    
    public void LinkBtn_Click(object sender, System.EventArgs e)
    {

    }
    #endregion

    #region MapCMSUserGroupToAD
    public bool MapCMSUserGroupToAD()
    {
        search = Request.QueryString["search"];
        if (!Page.IsPostBack || (Page.IsPostBack && !string.IsNullOrEmpty(Request.Form["domainname"])))
        {
            Display_MapCMSUserGroupToAD();
            return true;
        }
        else
        {
            Process_MapCMSUserGroupToAD();
            return (true);
        }
    }
    private void Process_MapCMSUserGroupToAD()
    {
        string[] tempArray = Strings.Split(Request.Form["usernameanddomain"], "_@_", -1, 0);
        string strUserName = tempArray[0].ToString();
        string strDomain = tempArray[1].ToString();
        m_refUserApi.MapCMSUserGroupToAD(m_intGroupId, strUserName, strDomain);
        string returnPage = "";
        if (Request.Form["rp"] == "1")
        {
            returnPage = (string)("users.aspx?action=viewallgroups&groupid=" + m_intGroupId + "&grouptype=" + m_intGroupType);
        }
        else
        {
            returnPage = (string)("adreports.aspx?action=SynchUsers&ReportType=" + Request.Form["rt"] + "&groupid=" + m_intGroupId + "&grouptype=" + m_intGroupType);
        }
        Response.Redirect(returnPage, false);
    }
    private void Display_MapCMSUserGroupToAD()
    {
        AppImgPath = m_refSiteApi.AppImgPath;
        rp = Request.QueryString["rp"];
        e1 = Request.QueryString["e1"];
        e2 = Request.QueryString["e2"];
        f = Request.QueryString["f"];
        if (rp == "")
        {
            rp = Request.Form["rp"];
        }

        if (e1 == "")
        {
            e1 = Request.Form["e1"];
        }

        if (e2 == "")
        {
            e2 = Request.Form["e2"];
        }

        if (f == "")
        {
            f = Request.Form["f"];
        }
        language_data = m_refSiteApi.GetAllActiveLanguages();
        user_data = m_refUserApi.GetUserGroupById(m_intGroupId);

        if ((m_intGroupId == 1) && (Convert.ToInt32(rp) == 3))
        {
            domain_data = m_refUserApi.GetDomains(1, 0);
        }
        else
        {
            domain_data = m_refUserApi.GetDomains(0, 0);
        }

        security_data = m_refContentApi.LoadPermissions(0, "content", 0);
        setting_data = m_refSiteApi.GetSiteVariables(m_refSiteApi.UserId);

        if ((search == "1") || (search == "2"))
        {

            if (search == "1")
            {
                strFilter = Request.Form["groupname"];
                Domain = Request.Form["domainname"];
            }
            else
            {
                strFilter = Request.QueryString["groupname"];
                Domain = Request.QueryString["domainname"];
            }
            if (Domain == "All Domains")
            {
                Domain = "";
            }
            GroupData[] adgroup_data;
            adgroup_data = m_refUserApi.GetAvailableADGroups(strFilter, Domain);
            //TOOLBAR
            System.Text.StringBuilder result = new System.Text.StringBuilder();
            txtTitleBar.InnerHtml = m_refStyle.GetTitleBar((string)(m_refMsg.GetMessage("search ad for cms group") + " \"" + user_data.GroupDisplayName + "\""));
            result.Append("<table><tr>");

            //if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().IndexOf("MSIE") > -1) //defect 16045
            //{
            //    result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/back.png", "javascript:window.location.reload(false);", m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
            //}
            //else
            //{
            //}
            result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/back.png", "javascript:history.back(1);", m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));


            if (rp != "1")
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/cancel.png", "#", m_refMsg.GetMessage("generic Cancel"), m_refMsg.GetMessage("close title"), "onclick=\"top.close();\"", StyleHelper.SaveButtonCssClass, true));
            }

            if (!(adgroup_data == null))
            {
                if (rp == "1")
                {
                    result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/save.png", "#", m_refMsg.GetMessage("alt update button text (associate group)"), m_refMsg.GetMessage("btn update"), "onclick=\"return SubmitForm(\'aduserinfo\', \'CheckRadio(1);\');\"", StyleHelper.SaveButtonCssClass, true));
                }
                else
                {
                    result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/save.png", "#", m_refMsg.GetMessage("alt update button text (associate group)"), m_refMsg.GetMessage("btn update"), "onclick=\"return SubmitForm(\'aduserinfo\', \'CheckReturn(1);\');\"", StyleHelper.SaveButtonCssClass, true));
                }
            }
            result.Append(StyleHelper.ActionBarDivider);
            result.Append("<td>");
            result.Append(m_refStyle.GetHelpButton("Display_MapCMSUserGroupToAD", ""));
            result.Append("</td>");
            result.Append("</tr></table>");
            htmToolBar.InnerHtml = result.ToString();
            //Dim i As Integer = 0
            //If (Not (IsNothing(domain_data))) Then
            //    domainname.Items.Add(New ListItem(m_refMsg.GetMessage("all domain select caption"), ""))
            //    domainname.Items(0).Selected = True
            //    For i = 0 To domain_data.Length - 1
            //        domainname.Items.Add(New ListItem(domain_data(i).Name, domain_data(i).Name))
            //    Next
            //End If
            Populate_MapCMSGroupToADGrid(adgroup_data);
        }
        else
        {

            PostBackPage.Text = Utilities.SetPostBackPage((string)("users.aspx?Action=MapCMSUserGroupToAD&Search=1&LangType=" + ContentLanguage + "&rp=" + rp + "&e1=" + e1 + "&e2=" + e2 + "&f=" + f + "&grouptype=" + m_intGroupType + "&groupid=" + m_intGroupId));
            txtTitleBar.InnerHtml = m_refStyle.GetTitleBar((string)(m_refMsg.GetMessage("search ad for cms group") + " \"" + user_data.DisplayUserName + "\""));
            System.Text.StringBuilder result = new System.Text.StringBuilder();
            result.Append("<table><tr>");
            if (rp != "1")
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "../UI/Icons/cancel.png", "#", m_refMsg.GetMessage("generic Cancel"), m_refMsg.GetMessage("btn cancel"), "onclick=\"top.close();\""));
            }
            else
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(AppImgPath + "images/UI/Icons/back.png", "javascript:history.back(1);", m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
            }
            result.Append("<td>");
            result.Append(m_refStyle.GetHelpButton("Display_MapCMSUserGroupToAD", ""));
            result.Append("</td>");
            result.Append("</tr></table>");
            htmToolBar.InnerHtml = result.ToString();
            Populate_MapCMSUserGroupToADGrid_Search(domain_data);
        }
    }
    private void Populate_MapCMSUserGroupToADGrid_Search(DomainData[] data)
    {
        System.Web.UI.WebControls.BoundColumn colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "GROUPTITLE";
        colBound.HeaderText = m_refMsg.GetMessage("active directory group title");
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(40);
        colBound.ItemStyle.Width = Unit.Percentage(40);
        MapCMSGroupToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "DOMAINTITLE";
        colBound.HeaderText = m_refMsg.GetMessage("domain title");
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.HeaderStyle.Width = Unit.Percentage(40);
        colBound.ItemStyle.Width = Unit.Percentage(40);
        MapCMSGroupToADGrid.Columns.Add(colBound);
        System.Text.StringBuilder result = new System.Text.StringBuilder();
        result.Append("<input type=\"hidden\" name=\"id\" value=\"" + user_data.UserId + "\">");
        result.Append("<input type=\"hidden\" name=\"rp\" value=\"" + rp + "\">");
        result.Append("<input type=\"hidden\" name=\"e1\" value=\"" + e1 + "\">");
        result.Append("<input type=\"hidden\" name=\"e2\" value=\"" + e2 + "\">");
        result.Append("<input type=\"hidden\" name=\"f\" value=\"" + f + "\">");
        result.Append("<input type=\"hidden\" name=\"adusername\">");
        result.Append("<input type=\"hidden\" name=\"addomain\">");


        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add(new DataColumn("GROUPTITLE", typeof(string)));
        dt.Columns.Add(new DataColumn("DOMAINTITLE", typeof(string)));
        dr = dt.NewRow();
        dr[0] = "<input type=\"Text\" name=\"groupname\" maxlength=\"255\" size=\"25\" OnKeyPress=\"javascript:return CheckKeyValue(event,\'34\');\">";
        dr[0] += result.ToString();
        int i = 0;
        dr[1] = "<select name=\"domainname\">";
        if ((!(domain_data == null)) && m_refContentApi.RequestInformationRef.ADAdvancedConfig == false)
        {
            dr[1] += "<option selected value=\"All Domains\">" + m_refMsg.GetMessage("all domain select caption") + "</option>";
        }
        for (i = 0; i <= domain_data.Length - 1; i++)
        {
            dr[1] += "<option value=\"" + domain_data[i].Name + "\">" + domain_data[i].Name + "</option>";
        }
        dr[1] += "</select>";

        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr[0] = "<input type=\"submit\" name=\"search\" value=\"" + m_refMsg.GetMessage("generic Search") + "\">";
        dr[1] = "";

        dt.Rows.Add(dr);


        DataView dv = new DataView(dt);
        MapCMSGroupToADGrid.DataSource = dv;
        MapCMSGroupToADGrid.DataBind();
    }
    private void Populate_MapCMSGroupToADGrid(GroupData[] data)
    {
        System.Web.UI.WebControls.BoundColumn colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "TITLE";
        colBound.HeaderText = m_refMsg.GetMessage("add title");
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(5);
        colBound.HeaderStyle.Width = Unit.Percentage(5);
        MapCMSGroupToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "GROUPTITLE";
        colBound.HeaderText = m_refMsg.GetMessage("active directory group title");
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.HeaderStyle.Width = Unit.Percentage(15);
        colBound.HeaderStyle.Width = Unit.Percentage(15);
        MapCMSGroupToADGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "DOMAINTITLE";
        colBound.HeaderStyle.CssClass = "title-header";
        colBound.HeaderText = m_refMsg.GetMessage("domain title");
        colBound.ItemStyle.Wrap = false;
        colBound.HeaderStyle.Width = Unit.Percentage(40);
        colBound.HeaderStyle.Width = Unit.Percentage(40);
        MapCMSGroupToADGrid.Columns.Add(colBound);


        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add(new DataColumn("TITLE", typeof(string)));
        dt.Columns.Add(new DataColumn("GROUPTITLE", typeof(string)));
        dt.Columns.Add(new DataColumn("DOMAINTITLE", typeof(string)));
        int i = 0;
        if (!(data == null))
        {
            for (i = 0; i <= data.Length - 1; i++)
            {
                dr = dt.NewRow();
                dr[0] = "<input type=\"Radio\" name=\"usernameanddomain\" value=\"" + data[i].GroupName + "_@_" + data[i].GroupDomain + "\" onClick=\"SetUp(\'" + data[i].GroupName + "_@_" + data[i].GroupDomain + "\')\">";
                dr[1] = data[i].GroupName;
                dr[2] = data[i].GroupDomain;
                dt.Rows.Add(dr);
            }
        }
        else
        {
            dr = dt.NewRow();
            dr[0] = m_refMsg.GetMessage("no ad groups found");
            dr[1] = "";
            dt.Rows.Add(dr);
        }
        System.Text.StringBuilder result = new System.Text.StringBuilder();
        result.Append("<input type=\"hidden\" name=\"id\" value=\"" + user_data.UserId + "\">");
        result.Append("<input type=\"hidden\" name=\"rp\" value=\"" + rp + "\">");
        result.Append("<input type=\"hidden\" name=\"e1\" value=\"" + e1 + "\">");
        result.Append("<input type=\"hidden\" name=\"e2\" value=\"" + e2 + "\">");
        result.Append("<input type=\"hidden\" name=\"f\" value=\"" + f + "\">");
        result.Append("<input type=\"hidden\" name=\"adusername\">");
        result.Append("<input type=\"hidden\" name=\"addomain\">");
        dr = dt.NewRow();
        dr[0] = result.ToString();
        dr[1] = "";
        dt.Rows.Add(dr);

        DataView dv = new DataView(dt);
        MapCMSGroupToADGrid.DataSource = dv;
        MapCMSGroupToADGrid.DataBind();
    }
    #endregion
    
    private void RegisterResources()
    {
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronWorkareaHelperJS);
    }

}
	
