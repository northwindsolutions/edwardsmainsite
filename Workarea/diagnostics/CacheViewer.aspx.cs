﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Security.Application;
using Ektron.Cms;

public partial class developer_CacheViewer : Ektron.Cms.Workarea.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Ektron.Cms.Framework.User.UserManager umngr = new Ektron.Cms.Framework.User.UserManager();
        Ektron.Cms.User.UserCriteria cri = new Ektron.Cms.User.UserCriteria();
        cri.AddFilter(Ektron.Cms.User.UserProperty.IsMemberShip, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, false);
        List<UserData> udList = umngr.GetList(cri);
        if (umngr.UserId > 0 && udList != null && udList.Exists(x => x.Id == umngr.UserId))
        {
            this.uxLitNotLoggedIn.Visible = false;
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                DisplayCacheItem(AntiXss.HtmlEncode(Request["id"]));
            }
            else
            {

                IEnumerable<DictionaryEntry> list = null;
                if (!string.IsNullOrEmpty(Request["start"]))
                {
                    list = GetStartsWithList(AntiXss.HtmlEncode(Request["start"]));
                }
                else if (!string.IsNullOrEmpty(Request["contains"]))
                {
                    list = GetContainsList(AntiXss.HtmlEncode(Request["contains"]));
                }
                else
                {
                    list = GetCacheList();
                }

                DisplayCacheList(list);

            }
        }
        else
        {
            this.uxPageMultiView.Visible = false;
            this.uxLitNotLoggedIn.Visible = true;
            this.uxLitNotLoggedIn.Text = "You need to be logged in as an admin or a cms user to view this page";
        }
    }

    protected void uxClearCache_Click(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["id"]))
        {
            HttpContext.Current.Cache.Remove(AntiXss.HtmlEncode(Request["id"]));
        }
        else
        {
            IEnumerable<DictionaryEntry> list = GetCacheList();
            foreach (DictionaryEntry item in list)
            {
                HttpContext.Current.Cache.Remove(item.Key.ToString());
            }
        }

        Response.Redirect("CacheViewer.aspx");
    }

    private void DisplayCacheItem(string key)
    {
        object cacheItem = HttpContext.Current.Cache[key];
        uxItemKey.Text = key;

        if (cacheItem != null)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(cacheItem.GetType());
                using (System.IO.StringWriter writer = new System.IO.StringWriter())
                {
                    serializer.Serialize(writer, cacheItem);
                    uxCacheValue.Text = Server.HtmlEncode(writer.ToString());
                }
            }
            catch(Exception e){
                uxCacheValue.Text = "Item could not be deserialized.";
            }
        }
        else
        {
            uxCacheValue.Text = "Item is NULL in value";
        }

        uxPageMultiView.SetActiveView(uxViewItem);
    }

    private void DisplayCacheList(IEnumerable<DictionaryEntry> list)
    {
        uxCacheListView.DataSource = list;
        uxCacheListView.DataBind();

        uxPageMultiView.SetActiveView(uxViewList);

    }

    private IEnumerable<DictionaryEntry> GetCacheList()
    {
        var query = from DictionaryEntry cacheItem in HttpContext.Current.Cache
                    orderby cacheItem.Key.ToString()
                    select cacheItem;

        return query;
    }

    private IEnumerable<DictionaryEntry> GetContainsList(string term)
    {
        var query = from DictionaryEntry cacheItem in HttpContext.Current.Cache
                    where cacheItem.Key.ToString().ToLower().Contains(term.ToLower())
                    orderby cacheItem.Key.ToString()
                    select cacheItem;

        return query;
    }

    private IEnumerable<DictionaryEntry> GetStartsWithList(string term){

        var query = from DictionaryEntry cacheItem in HttpContext.Current.Cache
                    where cacheItem.Key.ToString().ToLower().StartsWith(term.ToLower())
                    orderby cacheItem.Key.ToString()
                    select cacheItem;

        return query;
    }
}