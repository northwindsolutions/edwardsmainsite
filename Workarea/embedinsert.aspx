﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="embedinsert.aspx.cs"
    Inherits="Workarea_embedinsert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<script language="javascript" type="text/javascript">
    function insertHTML() {
        var resultTag = '';
        var text = $ektron('.embed-textval').val();

        text = $ektron.trim(text);
        if (text.indexOf('<') == -1 && text.length > 0) {
            text = '<span>' + text + '</span>';
        }

        if (Ektron.Namespace.Exists("parent.Ektron.Embed.AcceptHTML")) {
            //Get allowed elements from AlohaEktron configurations settings to lower case
            //var aeArray = normalize();
            //var jqueryText = $ektron('<div>' + text + '</div>');
            //var elements = jqueryText.children();
            //if (elements.length > 0) {
            //    $(elements).each(function () {
            //        resultTag = verifyHTML($(this), aeArray, "");
            //        if (resultTag.length > 0) {
            //            return false;
            //        }
            //    });
            //}
            //if (resultTag.length > 0) {
            //    var msg1 = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.errorMessage1;
            //    var msg2 = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.errorMessage2;
            //    msg2 = msg2.replace(/{TagToken}/gi, resultTag);
            //    var msgall = "<p>" + msg1 + "</p><p>" + msg2 + "</p>";
            //    $ektron(".ektron-ui-messageBody p").remove();
            //    $ektron(msgall).appendTo(".ektron-ui-messageBody");
            //    $ektron("#uxErrorMessage").show();
            //}
            //else {
            parent.Ektron.Embed.AcceptHTML({ "html": text });
            //}
        }
    }

    //Check for elements and attributes not allowed before insert
    function verifyHTML(obj, allowedObj, failTag) {
        if (failTag.length > 0) { return failTag; }
        var thisTag = obj.get(0).tagName.toLowerCase();

        if ($.inArray(thisTag, allowedObj.elements) === -1) {
            failTag = thisTag;
            return failTag;
        }
        else {
            //Get attributes for element
            var myval = getAttributes(obj);
            //Check attributes present
            if (myval.length > 0) {
                //Get allowed attributes for this element
                var myList = allowedObj.attributes[thisTag];
                if (myList) {
                    for (var a = 0; a < myval.length; a++) {
                        if ($.inArray(myval[a], myList) === -1) {
                            failTag = 'attribute ' + myval[a] + ' for element ' + thisTag;
                            break;
                        }
                    }
                }
                else {
                    //no attributes configured
                    failTag = 'attributes for ' + thisTag;
                }
            }
        }

        if (failTag.length === 0) {
            obj.children().each(function () {
                var thisTag = $(this).get(0).tagName.toLowerCase();
                if ($.inArray(thisTag, allowedObj.elements) === -1) {
                    failTag = thisTag;
                    return false;
                }
                else {
                    failTag = verifyHTML($(this), allowedObj, failTag);
                }
            });
        }
        return failTag;
    }

    //Get attibutes for current element
    function getAttributes(obj) {
        var queryArr = [];
        var objAttributes = obj[0].attributes;
        var lenAttr = objAttributes.length;
        var objTag = obj.get(0).tagName.toLowerCase();
        for (a = 0; a < lenAttr; a++) {
            queryArr.push(objAttributes[a].name);
        }
        return queryArr;
    }

    //Get allowed elements from AlohaEktron configurations settings to lower case
    function normalize() {
        //Get allowed list from AlohaEktron configurations settings to lower case
        var allowed = parent.Aloha.settings.contentHandler.allows;
        $.each(allowed.elements, function (index, item) {
            allowed.elements[index] = item.toLowerCase();
        });
        var allowedAttributes = allowed.attributes;

        for (var key in allowedAttributes) {
            var temp;
            temp = allowedAttributes[key];
            $.each(temp, function (index, item) {
                temp[index] = item.toLowerCase();
            });
            delete allowedAttributes[key];
            allowedAttributes[key.toLowerCase()] = temp;
        }
        return allowed;
    }



    $(document).ready(function () {
        var inst = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.instructionMessage;
        $ektron("span.ektron-aloha-embed-header").text(inst);

        var canceltext = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.canceltext;
        $ektron(".cancelbuttontext:button").val(canceltext).button();

        var embedtext = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.embedtext;
        $ektron(".embedbuttontext:button").val(embedtext).button();

    });      //end ready


</script>
<body>
    <form id="form1" runat="server">
        <div class="ektron-aloha-embed-header">
            <span class="ektron-aloha-embed-header"></span>
        </div>
        <div>
            <textarea class="embed-textval" cols="20" rows="2" style="width: 427px; height: 120px; resize: none; margin-bottom: 5px;"></textarea>
            <div class="ektron-ui-control ui-corner-all ektron-ui-message ektron-ui-error ektron-ui-clearfix" id="uxErrorMessage" style="display: none; width: 420px;">
                <span class="ui-icon ektron-ui-sprite ui-icon-notice ektron-ui-sprite-exclamation" id="uxErrorMessage_Message_aspIcon"></span>
                <div class="ektron-ui-clearfix ektron-ui-messageBody">
                </div>
            </div>
            <div class="ektron-aloha-dialog">
                <div style="float: right; margin-right: 16px;">
                    <span>
                        <input type="button" class="embedbuttontext" value="Embed" onclick="insertHTML()" />
                    </span>
                    <span>
                        <input type="button" class="cancelbuttontext" value="Cancel" onclick="parent.$ektron('.ektron-aloha-embed-modal').dialog('close');" />
                    </span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
