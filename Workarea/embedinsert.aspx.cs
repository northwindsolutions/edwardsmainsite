﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Common;
using Ektron.Cms;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

public partial class Workarea_embedinsert : Ektron.Cms.Workarea.Page
{
    protected Ektron.Cms.Content.ContentCriteria criteria = new Ektron.Cms.Content.ContentCriteria();
    protected Ektron.Cms.Framework.Content.ContentManager cmanager = new Ektron.Cms.Framework.Content.ContentManager(Ektron.Cms.Framework.ApiAccessMode.LoggedInUser);
    protected List<Ektron.Cms.ContentData> templatesList;

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterResources();
    }

    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();

        // create a package that will register the UI JS and CSS we need
        Package searchResultsControlPackage = new Package()
        {
            Components = new List<Component>()
            {
                // Register JS Files
                Packages.EktronCoreJS,
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/empjsfunc.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/ewebeditpro/eweputil.js"),
                Packages.Ektron.StringObject,
                Packages.Ektron.Namespace,
                Packages.jQuery.jQueryUI.Button,

                JavaScript.Create(cmsContextService.WorkareaPath + "/java/plugins/modal/ektron.modal.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/toolbar_roll.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/workareahelper.js"),

                // Register CSS Files
                //Css.Create(cmsContextService.WorkareaPath + "/csslib/ektron.fixedPositionToolbar.css"),
                Css.Create(cmsContextService.WorkareaPath + "/java/plugins/modal/ektron.modal.css"),
                 Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-message.css")
            }
        };
        searchResultsControlPackage.Register(this);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
    }


}