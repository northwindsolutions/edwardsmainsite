﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="imageinsert.aspx.cs" Inherits="Workarea_imageinsert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        /*style for ektronUI:FolderTree on imageinsert*/
        .folderWrapper {
            overflow: auto;
            white-space: nowrap;
            float: left;
            width: 240px;
            height: 484px;
        }

        .imageWrapper {
            width: 564px;
        }

        .imageGrid {
            overflow-y: auto;
            max-height: 424px;
            width: 308px;
        }

        #images ul {
            width: 280px;
            padding-left: 5px;
            float: left;
        }
        #images ul li {
                float: left;
        }
        .ektron-ui-tree ul {
            margin-left: 5px;
        }
        .aloha-responsive-image-label {
            height: 125px;
            width: 125px;
            display: block;
            margin: 0px 2px;
            border: solid 2px white;
            float: left;
        }

        .ektron-ui-tree ul li > ul {
            padding-left: 8px;
        }

        .aloha-responsive-image-label:hover {
            border: solid 2px gold;
        }

        input:checked + label {
            border: 2px solid #80b5f2;
        }

        .insertimagebutton {
            margin-top: 10px;
        }
    </style>
</head>
<script language="javascript" type="text/javascript">

    $(document).ready(function () {
        $ektron(' .folderWrapper .ektron-ui-tree-root').on('click', '.selectionLocation', function (event) {
            loadChildContent(this);
            return false;
        });
        //$ektron(".insertimagebutton:button").button();
        //Set Root folder name to Library
        $('#uxFolderTree_uxFolderTree_template_TreeRootElement').find('span:contains("Root"):first').html("Library");

    });      //end ready

    function loadChildContent(selectedItem) {
        var selectedFolderID = $ektron(selectedItem).attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().parent().attr('data-ektron-id');

        var currentFolder = $ektron('.templateInsertContent .hdnSourceFolder').val();
        if (selectedFolderID != currentFolder) {
            $ektron('.templateInsertContent .hdnSourceFolder').val(selectedFolderID);
            $ektron('.templateInsertContent .uxTriggerGetContent').click();
        }

    }

    function imageselected() {
        var selval = $('[name="insertImage"]:checked');
        if (selval.length > 0) {
            var image = {
                "path": selval.val(),
                "title": selval.attr('Title')
            }
            //Create breakpoint images
            createImages(selval.val());

            if (Ektron.Namespace.Exists("parent.Ektron.AdvancedInspector.ResponsiveImage.AcceptInsert")) {
                parent.Ektron.AdvancedInspector.ResponsiveImage.AcceptInsert(image);
            }
        }
        else {
            alert('no image selected');
        }
    }
    function createImages(path) {
        $ektron.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "<%=_api.AppPath %>FrameworkUI/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/resources/advancedinspector.ashx",
            data: { "action": "createbreakpointimages", "path": path },
            success: function (msg) {

            }
        });
    }
</script>
<body>
    <form id="form1" runat="server">
        <div class="imageWrapper">
            <div class="folderWrapper">
                <ektronUI:FolderTree ID="uxFolderTree" runat="server" UseInternalAdmin="true" OnPreSerializeData="PreSerializeHandler">
                </ektronUI:FolderTree>
            </div>
            <asp:ScriptManager ID="uxPackageScriptManager" runat="server">
            </asp:ScriptManager>
            <div class="templateInsertContent">
                <asp:UpdatePanel ID="uxUpdatePanelContent" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="hdnFields">
                            <input type="hidden" id="uxOldSourceFolder" runat="server" class="hdnOldSourceFolder" value="0" />
                            <input type="hidden" id="uxSourceFolder" runat="server" class="hdnSourceFolder" />
                            <asp:Button ID="uxTriggerGetContent" runat="server" CssClass="uxTriggerGetContent"
                                Style="display: none;" />
                        </div>
                        <div class="imageGrid">
                            <ektronUI:Message ID="uxContentMessage" runat="server">
                            </ektronUI:Message>
                            <asp:ListView ID="uxLibraryGrid" runat="server">
                                <LayoutTemplate>
                                    <div id="images">
                                        <ul>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                        </ul>
                                    </div>
                                </LayoutTemplate>

                                <ItemTemplate>
                                    <li style="list-style-type: none;">
                                        <input id='Radio_<%# Eval("ID").ToString() %>' type="radio" title="<%# Eval("Title").ToString() %>" value="<%# Eval("FileName").ToString() %>" name="insertImage" style="display: none;" />
                                        <label for="Radio_<%# Eval("ID").ToString() %>" class="aloha-responsive-image-label" style="background-image: url(<%# Eval("fullpath").ToString() %>); background-repeat: no-repeat;"></label>

                                    </li>
                                </ItemTemplate>

                            </asp:ListView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:UpdatePanel ID="uxUpdatePanelButton" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="ektron-aloha-dialog aloha">
                        <div style="float: right; margin-right: 30px;">
                            <span>
                                <ektronUI:Button ID="insertimagebutton" runat="server" DisplayMode="Button" Text="Select Image" OnClientClick="imageselected(); return false;" />
                            </span>

                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
