﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Interfaces.Context;

public partial class Workarea_imageinsert : Ektron.Cms.Workarea.Page
{
    protected Ektron.Cms.Content.LibraryCriteria criteria = new Ektron.Cms.Content.LibraryCriteria();
    protected List<Ektron.Cms.ContentData> templatesList;
    protected LibraryType selectedLibraryType = LibraryType.images;
    protected long folderid = 0;
    protected long breakpointwidth = 0;
    protected CommonApi _api;

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterResources();

        if (Page.IsPostBack)
        {
            // Folder Change Event
            if (!String.IsNullOrEmpty(uxSourceFolder.Value) && uxSourceFolder.Value != uxOldSourceFolder.Value)
            {
                this.uxFolder_Change();
            }
            else
            {
                uxFolderTree.Visible = true;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderid"]))
            {
                long.TryParse(Request.QueryString["folderid"], out folderid);
            }
            uxFolderTree.RootId = folderid.ToString();
            uxSourceFolder.Value = folderid.ToString();
            uxImageListGridView_EktronUIThemeFolderChanged(folderid);
        }
    }
    protected void uxFolder_Change()
    {
        long.TryParse(uxSourceFolder.Value, out folderid);
        uxOldSourceFolder.Value = folderid.ToString();
        uxImageListGridView_EktronUIThemeFolderChanged(folderid);
    }
    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        _api = new Ektron.Cms.CommonApi();

        object localizedText = GetGlobalResourceObject("Ektron_Cms_Framework_UI_Controls_EktronUI", "Aloha_Plugins_ImageInsert_buttonTitle");
        if (null != localizedText)
        {
            insertimagebutton.Text = localizedText.ToString();
        }
        //used to filter on images with width  < breakpointwidth
        if (!string.IsNullOrEmpty(Request.QueryString["breakpointwidth"]))
        {
            long.TryParse(Request.QueryString["breakpointwidth"], out breakpointwidth);
        }
        // create a package that will register the UI JS and CSS we need
        Package searchResultsControlPackage = new Package()
        {
            Components = new List<Component>()
            {
                // Register JS Files
                Packages.EktronCoreJS,
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/empjsfunc.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/ewebeditpro/eweputil.js"),
                Packages.Ektron.StringObject,
                Packages.Ektron.Namespace,

                JavaScript.Create(cmsContextService.WorkareaPath + "/java/plugins/modal/ektron.modal.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/toolbar_roll.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/workareahelper.js"),

                // Register CSS Files
                Css.Create(cmsContextService.WorkareaPath + "/java/plugins/modal/ektron.modal.css"),
                Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-message.css")
            }
        };
        searchResultsControlPackage.Register(this);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
    }

    protected void uxImageListGridView_EktronUIThemeFolderChanged(long folderid)
    {

        this.criteria = this.GetLibraryCriteria();
        this.bindData();
    }

    protected void PreSerializeHandler(object sender, EventArgs e)
    {
        if (uxFolderTree != null && uxFolderTree.Items is Ektron.Cms.Framework.UI.Tree.TreeNodeCollection)
        {
            ModifyFolderTree(uxFolderTree.Items);
        }
    }

    ///// <summary>
    ///// Exclude Calendar and Commerce Folders from TREE, And Also Check the FolderSelected when It is in Package Folder list.
    ///// </summary>
    ///// <param name="nodeList"></param>
    private void ModifyFolderTree(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection nodeList)
    {
        for (int index = nodeList.Count - 1; index >= 0; index--)
        {
            var treeNode = (Ektron.Cms.Framework.UI.Controls.EktronUI.FolderTreeNode)nodeList[index];

            if (treeNode.FolderType == 8 || treeNode.FolderType == 9 || treeNode.FolderType == 3 || treeNode.FolderType == 1)
            {
                // Exclude Ecommerce, Blogs, Forums (Discussion Board) and Calendar Folders from Tree                 
                nodeList.RemoveAt(index);
            }
            else
            {
                if (treeNode.Items.Count > 0)
                {
                    ModifyFolderTree(treeNode.Items);
                }
            }
        }
    }

    private void bindData()
    {
        ILibraryManager libraryManager = ObjectFactory.GetLibraryManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());

        List<LibraryData> libraryList = libraryManager.GetList(criteria);
        uxLibraryGrid.Visible = true;
        if (libraryList != null && libraryList.Count > 0)
        {
            uxContentMessage.Visible = false;
            uxLibraryGrid.DataSource = formatOutput(libraryList);
            uxLibraryGrid.DataBind();
            insertimagebutton.Visible = true;
            uxUpdatePanelButton.Update();
        }
        else
        {
            uxLibraryGrid.DataSource = null;
            uxLibraryGrid.DataBind();
            uxLibraryGrid.Visible = false;
            insertimagebutton.Visible = false;
            uxUpdatePanelButton.Update();
            uxContentMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            uxContentMessage.Text =  GetGlobalResourceObject("Ektron_Cms_Framework_UI_Controls_EktronUI", "Aloha_Plugins_ResponsiveImage_noText").ToString();
            uxContentMessage.Visible = true;
        }
    }

    private LibraryCriteria GetLibraryCriteria()
    {

        LibraryCriteria libraryCriteria = new LibraryCriteria();
        libraryCriteria.OrderByField = LibraryProperty.ContentId;
        libraryCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
        libraryCriteria.AddFilter(LibraryProperty.ParentId, CriteriaFilterOperator.EqualTo, this.folderid);
        libraryCriteria.AddFilter(LibraryProperty.TypeId, CriteriaFilterOperator.EqualTo, this.selectedLibraryType);
        libraryCriteria.AddFilter(LibraryProperty.ContentType, CriteriaFilterOperator.EqualTo, EkEnumeration.CMSContentType.LibraryItem);

        return libraryCriteria;
    }
    private DataTable formatOutput(List<LibraryData> input)
    {
        //create table for formatting image list for databinding
        DataTable dt = new DataTable("Images");
        //DEcalre variables for columns and rows
        DataColumn column;
        DataRow row;

        column = new DataColumn("fullpath", System.Type.GetType("System.String"));
        dt.Columns.Add(column);
        column = new DataColumn("Title", System.Type.GetType("System.String"));
        dt.Columns.Add(column);
        column = new DataColumn("FileName", System.Type.GetType("System.String"));
        dt.Columns.Add(column);
        column = new DataColumn("ID", System.Type.GetType("System.String"));
        dt.Columns.Add(column);

        //prepend path with site api 
        int count = input.Count;
        for (int index = 0; index < count; )
        {

            row = dt.NewRow();
            row["fullpath"] = _api.SitePath + makeThumb(input[index].FileName.Substring(1));
            row["Title"] = input[index].Title;
            row["FileName"] = _api.SitePath + input[index].FileName.Substring(1);
            row["ID"] = input[index].Id;
            dt.Rows.Add(row);
            index++;
        }

        return dt;
    }
    //Get thumbnal image by prepend thumb_ to filename 
    private string makeThumb(string imagepath)
    {
        string retString;
        int index = imagepath.LastIndexOf("/");
        string ext = imagepath.Substring(imagepath.Length - 3);
        // gif extension images create png extension thumbnails
        if (ext.ToLower() == "gif")
        {
            imagepath = imagepath.Substring(0, imagepath.Length - 3) + "png";
        }
        retString = imagepath.Substring(0, index + 1) + "thumb_" + imagepath.Substring(index + 1);

        return HttpUtility.UrlPathEncode(retString);
    }

}