/* depends on Ektron.Namespace.js */
Ektron.Namespace.Register('Ektron.Controls.Editor.Aloha');
$ektron.extend(Ektron.Controls.Editor.Aloha, {
    isLoaded: false,
    triggerCallback: function (command, uniqueId, clientId, callbackData) {
        var params = "",
            postData = "",
            contentBlock = $ektron("#" + clientId);

        if (Ektron.Controls.Editor.Aloha.isLoaded) {
            $ektron.extend(callbackData, { "previouslyLoaded": true });
        }
        for (var property in callbackData) {
            params += property + "=" + callbackData[property];
            if (params.length > 0) {
                params += "&"
            }
        }
        params += "command=" + command;
        postData += "&__CALLBACKID=" + uniqueId; // Must use ASP.NET UniqueID as that's what's expected on server side when control is used with master pages.
        postData += "&__VIEWSTATE="; // + encodeURIComponent($ektron("#__VIEWSTATE").attr("value"));
        postData += "&__CALLBACKPARAM=" + escape(params);
        postData += "&EktronClientManager=" + $ektron("#EktronClientManager").val();

        switch (command) {
            case "save":
            case "cancel":
                Aloha.deactivateEditable();
                break;
            case "edit":
                $ektron(document).trigger('editInContextStarted');
                break;
            default:
                // do nothing
                break;
        }

        var ajaxUrl = window.location.href.replace(window.location.hash, ""); // IE/Windows will block callbacks with a pound/hash symbol in the URL.
        $ektron.ajax({
            async: false,
            type: "POST",
            url: ajaxUrl,
            data: postData,
            dataType: "html",
            error: function (data) { console.log("inData: " + data + ", context: " + context); },
            success: function (data) {
                var EICBody = data.substring(data.indexOf("|") + 1);
                contentBlock.html("");
                contentBlock.append(EICBody);
            }
        });
    }
});