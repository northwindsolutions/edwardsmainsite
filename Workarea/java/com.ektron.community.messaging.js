// Class CommunityMsgClass:

function CommunityMsgClass(idTxt) {
    this.GetId = function () {
        return (this._id);
    },

    this.SetId = function (id) {
        this._id = id;
    },

    this.Initialize = function () {
        this.UserSelectCtl_Initialize();
        this.AddClickableRecipientUI();
    },

    this.AddClickableRecipientUI = function () {
        if (!this.ClickableRecipientUIEnabled) {
            return;
        }
        var msgToElement = this.GetMsgToElement();
        if (msgToElement && this.IsValidObject($ektron)) {
            var userNames = this.GetUserNames();
            var userIds = this.GetUserIds();
            if (userNames != null
                && userNames.length > 0
                && userNames[0] != null
                && userNames[0].length > 0
                && userIds != null
                && userNames.length == userIds.length) {
                var msgTo = $ektron(msgToElement);
                msgTo.parent().find('.clickable_recipients').remove();
                var html = "<div class='clickable_recipients' >";
                for (var x = 0; x < userNames.length; x++) {
                    html += "<span class='clickable_recipient'><span class='clickable_recipient_remove' ";
                    html += " onclick='CommunityMsgClass.GetObject(\"" + this.GetId() + "\").RemoveRecipient(" + userIds[x] + ", \"" + this.SafeTrim(userNames[x]) + "\", this);'";
                    html += " title='click to remove'>&#160;&#160;&#160;</span>" + this.SafeTrim(userNames[x]) + "</span>";
                }
                html += "</div>";
                msgTo.parent().prepend(html);
                msgTo.hide();
            }
        }
    },

    this.RemoveRecipient = function (id, name, trigger) {
        $ektron(trigger).parent().remove()
        var userNames = this.GetUserNames();
        var userIds = this.GetUserIds();
        var userNameString = "";
        var userIdsString = "";

        if (userNames != null && userNames.length > 0) {
            for (var x = 0; x < userNames.length; x++) {
                if (this.SafeTrim(userNames[x]) != this.SafeTrim(name)) {
                    userNameString = this.AppendDelimitedString(userNameString, this.SafeTrim(userNames[x]), ", ");
                }
            }
        }
        this.SetUserNames(userNameString);

        if (userIds != null && userIds.length > 0) {
            for (var x = 0; x < userIds.length; x++) {
                if (this.SafeTrim(userIds[x]) != this.SafeTrim(id)) {
                    userIdsString = this.AppendDelimitedString(userIdsString, this.SafeTrim(userIds[x]), ",");
                }
            }
        }
        this.SetUserIds(userIdsString);

        $ektron("#CommunitySearch_ResultsContainer_" + this.usersel_comsearch_ClientID).html("");
        this.InitSelectedUsers(false);
    },

    this.AppendDelimitedString = function (originalText, newText, delimiter) {
        var result = originalText;
        if (result != null && result.length > 0) {
            result += delimiter;
        }
        return result + newText;
    },

    this.GetMsgToElement = function () {
        return document.getElementById('ekpmsgto' + this.GetId());
    },

    this.GetUserIdElement = function () {
        return document.getElementById('ektouserid' + this.GetId());
    },

    this.GetUserNames = function () {
        var toObj = this.GetMsgToElement();
        if (toObj) {
            return toObj.value.split(',');
        }
        return null;
    },

    this.GetUserIds = function () {
        var hdbObj = this.GetUserIdElement();
        if (hdbObj) {
            return hdbObj.value.split(',');
        }
        return null;
    },

    this.SetUserNames = function (names) {
        var hdbObj = this.GetMsgToElement();
        if (hdbObj) {
            hdbObj.value = names;
        }
    },

    this.SetUserIds = function (ids) {
        var hdbObj = this.GetUserIdElement();
        if (hdbObj) {
            hdbObj.value = ids;
        }
    },

    this.GetUserList = function () {
        return window["UserSelectCtl_UserList" + this.usersel_comsearch_ClientID];
    },

    this.SetUserList = function (list) {
        window["UserSelectCtl_UserList" + this.usersel_comsearch_ClientID] = list;
    },

    this.GetUserNameList = function () {
        return window["UserSelectCtl_UserNameList" + this.usersel_comsearch_ClientID];
    },

    this.SetUserNameList = function (list) {
        window["UserSelectCtl_UserNameList" + this.usersel_comsearch_ClientID] = list;
    },

    this.MsgShowMessageTargetUI = function (hdnId, flag) {
        this.InitSelectedUsers(flag);
    },

	this.MsgSaveMessageTargetUI = function () {
	    var divObj = document.getElementById(this.GetId() + '_EktMsgTargetsBody' + this.GetId());
	    var hdbObj = this.GetUserIdElement();
	    var toObj = this.GetMsgToElement();
	    var idx;
	    var userId;
	    if (toObj) { toObj.value = ''; }
	    if (divObj) {
	        if (hdbObj) {
	            hdbObj.value = '';

	            var users = this.UserSelectCtl_GetSelectUsers();
	            if (this.IsValidObject(users)) {
	                hdbObj.value = users;
	                if (toObj) {
	                    toObj.value = '';
	                    var userArray = users.split(',');
	                    for (idx = 0; idx < userArray.length; idx++) {
	                        if (toObj && toObj.value.length > 0) {
	                            toObj.value += ', ';
	                        }
	                        var userName = this.UserSelectCtl_GetUserName(userArray[idx]);
	                        if (userName)
	                            toObj.value += userName;
	                    }
	                }
	            }

	        }
	    }
	    this.MsgCloseMessageTargetUI();
	    this.AddClickableRecipientUI();
	},

	this.MsgCloseMessageTargetUI = function () {
	    var divObj = document.getElementById('MessageTargetUI' + this.GetId());
	    if (divObj) {
	        divObj.style.display = 'none';
	    }
	    ektb_remove();
	},

	this.MsgCancelMessageTargetUI = function () {
	    //Recover Original State:
	    this.searchHasRun = false;
	    var obj = CommunitySearchClass.GetObject(this.usersel_comsearch_ClientID);
	    if (obj) {
	        obj.SelectTab("basic");
	    }
	    this.MsgCloseMessageTargetUI();
	},

	this.MsgTarg_PrevPage = function () {
	    var pageHdnObj = document.getElementById("RecipientsPage" + this.GetId());
	    if (pageHdnObj && pageHdnObj.value && (pageHdnObj.value.length > 0)) {
	        var page = parseInt(pageHdnObj.value);
	        if (!isNaN(page)) {
	            page -= 1;
	            pageHdnObj.value = page.toString();
	            this.MsgTarg_DoSearch('', '');
	        }
	    }
	},

	this.MsgTarg_NextPage = function () {
	    var pageHdnObj = document.getElementById("RecipientsPage" + this.GetId());
	    if (pageHdnObj && pageHdnObj.value && (pageHdnObj.value.length > 0)) {
	        var page = parseInt(pageHdnObj.value);
	        if (!isNaN(page)) {
	            page += 1;
	            pageHdnObj.value = page.toString();
	            this.MsgTarg_DoSearch('', '');
	        }
	    }
	},

    this.InitSelectedUsers = function (flag) {
        var userIds = null;
        var hdbObj = this.GetUserIdElement();
        if (hdbObj) {
            this.UserSelectCtl_SetSelectUsers(hdbObj.value);
            userIds = hdbObj.value.split(',');
        }

        var toObj = this.GetMsgToElement();
        var userNames = null;
        if (toObj) {
            userNames = toObj.value.split(", ");
        }
        if ((userIds.length > 0) && userNames) {
            for (var idx = 0; idx < userIds.length; idx++) {
                this.UserSelectCtl_SetUserName(this.SafeTrim(userIds[idx]), userNames[idx]);
            }
        }

        // IE6 has a bug, where even though the checkboxes are updated to be set to a checked state, they appear unchecked (only with elements added via Ajax?!?). Workaround, refresh when browser is IE6:
        var isIE6 = (window.navigator.appVersion.indexOf("MSIE 6.") >= 0);
        if (isIE6 || (flag && !this.searchHasRun)) {
            // kick-off a search for all users:
            CommunitySearchClass.DoSearch(this.usersel_comsearch_ClientID, '');
            this.searchHasRun = true;
        }
    },

	this.SendMessage = function(alertNoRecipientMsg){
	    var hdbObj = this.GetUserIdElement();
		var hdbGroupObj = document.getElementById('ektogroupid' + this.GetId());
		if (hdbObj && (hdbObj.value.length > 0)){
			hdbObj = document.getElementById('EkMsg_hdnRecipientsValidated' + this.GetId());
			if (hdbObj){
				hdbObj.value = "1";
				//don't submit here, use submit button instead, as this causes problem in FireFox (the Javascript Editor posts data that causes an ASP.NET validation error): document.form1.submit();
				return (true);
			}
		}
		else if (hdbGroupObj && (hdbGroupObj.value.length > 0)){			
    		hdbObj = document.getElementById('EkMsg_hdnRecipientsValidated' + this.GetId());
			if (hdbObj){
				hdbObj.value = "1";
				//don't submit here, use submit button instead, as this causes problem in FireFox (the Javascript Editor posts data that causes an ASP.NET validation error): document.form1.submit();
				return (true);
			}
		}
		else {
			alert(alertNoRecipientMsg);
			return (false);
		}
	},

    this.SetUserSelectId = function (userSelId){
        this.userSelId = userSelId;
    },
    
    this.SetAjaxCall = function (ptr){
        this._ajaxCall = ptr;
    },
    
    this.GetAjaxCall = function (){
        return (this._ajaxCall);
    },
    

    /////////////////////////////////////////////////
    // methods for user selection - community search:

    this.UserSelectCtl_Initialize = function () {
        if ('undefined' !== typeof(CommunitySearchClass) && CommunitySearchClass.IsReady(this.usersel_comsearch_ClientID)) {            
            CommunityMsgClass.UserSelectCtl_Initialize_Complete(this.GetId());
        }
        else {
            setTimeout('CommunityMsgClass.GetObject("' + this.GetId() + '").UserSelectCtl_Initialize()', 50);
        }
    },

    this.UserSelectCtl_Initialize_Complete = function (){
        CommunitySearchClass.Hook_NotifySearchResultsChanged(this.usersel_comsearch_ClientID, function (containerObj) {
            CommunityMsgClass.UserSelectCtl_NotifySearchResultsChanged(this.GetId(), containerObj);
        });
    },

    this.UserSelectCtl_NotifySearchResultsChanged = function (containerObj){
        if (containerObj && containerObj.innerHTML && containerObj.innerHTML.length > 0){
            var userList = null;
            if (this.IsValidObject(this.GetUserList())) {
                userList = this.GetUserList();
                var els = containerObj.getElementsByTagName('input');
                var chkAllObj = null;
                var allChecked = true;
                var checkedCnt = 0;
                for (var idx = 0; idx < els.length ; idx++){
                    if ("checkbox" == els[idx].type){

                        if (els[idx].name.length > 0){
                            if (this.IsValidObject(userList[els[idx].name]) && userList[els[idx].name]) {
                                els[idx].checked = true;
                                ++checkedCnt;
                            }
                            else{
                                allChecked = false;
                            }
                        }
                        else{
                            chkAllObj = els[idx];
                        }
                    }
                }
                if (chkAllObj && allChecked && (checkedCnt > 0)){
                    chkAllObj.checked = true;
                }
            }
        }
    },
    
    this.UserSelectCtl_CheckAll = function (chkboxObj, ctrlId){
        var containerObj = document.getElementById('CommunitySearch_ResultsContainer_Table_' + ctrlId);
        if (containerObj && containerObj.innerHTML && containerObj.innerHTML.length > 0){
            var els = containerObj.getElementsByTagName('input');
            for (var idx = 0; idx < els.length ; idx++){
                if (("checkbox" == els[idx].type) && (els[idx].name.length > 0)){
                    els[idx].checked = (chkboxObj.checked && !this.usersel_comsearch_SingleSelection);
                    this.UserSelectCtl_CheckboxClicked(els[idx], ctrlId, els[idx].name, els[idx].alt)
                }
            }
        }
    },
    
    this.UserSelectCtl_CheckboxClicked = function (chkboxObj, ctrlId, userId, displayName){
        var userList = null;
        var userNameList = null;
        
        if (chkboxObj.checked && this.usersel_comsearch_SingleSelection){
            this.UserSelectCtl_CheckAll(chkboxObj, ctrlId);
            chkboxObj.checked = true;
        }

        if (this.IsValidObject(this.GetUserList())) {
            userList = this.GetUserList();
        }
        else{
            userList = new Array;
            this.SetUserList(userList);
        }
        userList[userId] = chkboxObj.checked;

        // save usernames for each id:
        if (this.IsValidObject(this.GetUserNameList())) {
            userNameList = this.GetUserNameList();
        }
        else{
            userNameList = new Array;
            this.SetUserNameList(userNameList);
        }
        userNameList[userId] = displayName;
    },
    
    this.UserSelectCtl_GetSelectUsers = function (){
        var userList = null;
        var result = '';
        if (this.IsValidObject(this.GetUserList())) {
            userList = this.GetUserList();
        }
        else{
            userList = new Array;
            this.SetUserList(userList);
        }
        
        for (var userItem in userList){
            if (userList[userItem]){
                if (result.length)
                    result += ",";
                result += userItem.toString();
            }
        }
         //In safari the for is going through an extra loop so removing the ',' if appended to the end. 
        if (result.lastIndexOf(",") == result.length - 1) {
            result = result.substring(0,result.length-1);
        }
        return (result);
    },

    this.UserSelectCtl_GetUserName = function (userId){
        var userNameList = null;
        var result = '';
        if (this.IsValidObject(this.GetUserNameList())) {
            userNameList = this.GetUserNameList();
        }
        else{
            userNameList = new Array;
            this.SetUserNameList(userNameList);
        }
        if (this.IsValidObject(userNameList[userId])) {
            result = userNameList[userId];
        }
        return (result);
    },

    this.UserSelectCtl_SetSelectUsers = function (idList){
        var idArray = idList.split(',');
        var userList = new Array;
        this.SetUserList(userList);
        
        for (var idx = 0; idx < idArray.length; idx++){
            userList[this.SafeTrim(idArray[idx])] = true;
        }
    },

    this.UserSelectCtl_SetUserName = function (userId, displayName){
        var userNameList = null;
        var result = '';
        if (this.IsValidObject(this.GetUserNameList())) {
            userNameList = this.GetUserNameList();
        }
        else{
            userNameList = new Array;
            this.SetUserNameList(userNameList);
        }
        if (this.IsValidObject(userNameList)) {
            userNameList[userId] = displayName;
        }
    },

    this.IsValidObject = function (obj) {
        return ("undefined" != typeof(obj)) && (null != obj);
    },

    this.SafeTrim = function (obj) {
        if (!this.IsValidObject(obj)) {
            return "";
        }

        if ("undefined" != typeof ($ektron)
            && "string" == typeof (obj)) {
            return $ektron.trim(obj)
        }


        if ("string" != typeof (obj)) {
            if (this.IsValidObject(obj.toString)) {
                obj = obj.toString();
            } else {
                obj = new String(obj);
            }
        }

        if (this.IsValidObject(obj.trim)) {
            return obj.trim();
        } else {
            return obj;
        }
    },

	
	///////////////////////////
	// initialize properties:
	this._id = '';
	this.SetId(idTxt);
	this.MsgUsersSelArray = new Object();
	this.SearchInit = false;
	this.userSelId = idTxt + '_ComSearch';

    this.usersel_comsearch_ClientID = this.GetId() + '_ComSearch';
    this.usersel_comsearch_SingleSelection = false;

    this.searchHasRun = false;
    this.ClickableRecipientUIEnabled = false;
}

CommunityMsgClass.GetObject = function (id){
	var fullId = "CommunityMsgObj_" + id;
	if (("undefined" != typeof window[fullId])
		&& (null != window[fullId])){
		return (window[fullId]);
	}
	else {
	    var cmObj = new CommunityMsgClass(id);
	    window[fullId] = cmObj;
	    window[fullId + '_ComSearch'] = cmObj; // save a Sreference for the search control.
	    cmObj.Initialize();
	    return (cmObj);
	}
}

CommunityMsgClass.addLoadEvent = function (func) {
    if ("undefined" != typeof ($ektron)) {
        $ektron(document).ready(func);
        return;
    }
    setTimeout('CommunityMsgClass.addLoadEvent(' + func + ')', 50);
}

CommunityMsgClass.UserSelectCtl_Initialize_Complete = function (id){
    var obj = CommunityMsgClass.GetObject(id);
    if (obj)
        obj.UserSelectCtl_Initialize_Complete();
}

CommunityMsgClass.UserSelectCtl_NotifySearchResultsChanged = function (id, containerObj){
    var obj = CommunityMsgClass.GetObject(id);
    if (obj)
        obj.UserSelectCtl_NotifySearchResultsChanged(containerObj);
}

function StripHtml(stext){
        var re = /<\S[^>]*>/g;
        stext = stext.replace(re,"");
        return (stext); 
}
