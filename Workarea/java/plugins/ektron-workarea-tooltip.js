﻿//tooltip plugin
(function ($)
{
    $.fn.toolbarTip = function (options)
    {

        var defaults = {
            offset: [0, 0],
            classNameBase: "tooltip",
            srcAttrib: "title"
        };

        var options = $.extend(defaults, options);
        var arrow_class = options.classNameBase + "-arrow";

        return this.each(function ()
        {
            var button = $(this);

            if (!(button.attr(options.srcAttrib).length > 0)) { return; }

            //keep browser tooltip from appearing on links
            if (options.srcAttrib == "title")
            {
                button.attr({ "tt": button.attr("title") });
                button.removeAttr("title");
                options.srcAttrib = "tt";
            }

            button.hover(function ()
            {
                $("." + options.classNameBase + ", ." + arrow_class).remove();

                var tooltip = document.createElement("div");
                $("body").prepend(tooltip);
                tooltip = $(tooltip);
                tooltip.html(button.attr(options.srcAttrib)).addClass(options.classNameBase);

                var xpos = button.offset().left - ((tooltip.outerWidth() - button.outerWidth()) / 2) + options.offset[0];
                var ypos = button.offset().top - tooltip.outerHeight() + options.offset[1];
                var xlimit = $(window).width() - tooltip.outerWidth();
                var ylimit = button.offset().top - tooltip.outerHeight();

                if (xpos < 0)
                {
                    xpos = 0;
                } else
                {
                    if (xpos > xlimit)
                    {
                        xpos = xlimit;
                    }
                }
                if (ypos < 0)
                {
                    ypos = 0;
                } else
                {
                    if (ypos > ylimit)
                    {
                        ypos = ylimit;
                    }
                }
                tooltip.css({
                    "left": Math.floor(xpos) + "px",
                    "top": Math.floor(ypos) + "px"
                });

                var arrow = document.createElement("div");
                $("body").prepend(arrow);
                arrow = $(arrow);
                arrow.addClass(arrow_class);

                var xpos_arrow = button.offset().left + ((button.outerWidth() - arrow.outerWidth()) / 2) + options.offset[0];
                var ypos_arrow = ypos + tooltip.outerHeight();

                arrow.css({
                    "left": Math.floor(xpos_arrow) + "px",
                    "top": Math.floor(ypos_arrow) + "px"
                });

            }, function ()
            {
                $("." + options.classNameBase + ", ." + arrow_class).remove();
            });
        });
    };
})($ektron);