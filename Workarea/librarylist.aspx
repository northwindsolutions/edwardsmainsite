﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="librarylist" CodeFile="librarylist.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel='stylesheet' type='text/css' href='Tree/css/com.ektron.ui.contextmenu.css' />
    <link rel='stylesheet' type='text/css' href='Tree/css/com.ektron.ui.tree.css' />
    <link rel="stylesheet" type="text/css" href="csslib/ektron.workarea.css" />
    <script language="javascript" type="text/javascript">
        

        function onFolderNodeClick_New( id, clickedElement )
		{	
		   if (clickedElementPrevious != null) // Only null if running it outside of the frameset while debugging
            {
                clickedElementPrevious.className = "";
            }

            clickedElement.className += " ektronTreeSelectedItem";
            clickedElementPrevious = clickedElement;
			clickedIdPrevious = id;

			var folderName = clickedElement.innerText;
			var folderId   = id;

			document.getElementById( "folderName" ).value = folderName;
			document.getElementById( "selected_folder_id" ).value = id;
            var autonavfolder = getQuerystring("autonavfolder",0,parent["libraryinsert"].location.href);
			parent["libraryinsert"].location.href=ContentUrl+id+"&autonavfolder="+autonavfolder;
            returnValue = new Folder( folderName, folderId );
		}

        function getQuerystring(key, default_,url)
        {
          if (default_==null) default_="";
          key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
          var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
          var qs = regex.exec(url);
          if(qs == null)
            return default_;
          else
            return qs[1];
        }
		function Folder( name, id )
		{
			this.name = name;
			this.id   = id;
		}
        function setnode()
        {     
              var element = document.getElementById("L" + <%=m_autonav%> + "_tv0");
              if (<%=m_autonav%> != 0)
              {
                var rootelement = document.getElementById("L0_tv0");
                rootelement.className="";
                onFolderNodeClick_New( <%=m_autonav%>, element )
              }
        }   
                  
		function displayTree_New()
		{
		    switch (FrameName) {
			    case "SmartDesktop":
			    case "Module":
			    case "Admin":{
				    var html='';
				    TreeOutput.innerHTML=html;
				    break;
			    }
			    default:{
				    toolkit.getRootFolder( function( folderRoot ) {
                    var vFolderName = '<% =MsgHelper.GetMessage("generic library title")%>';
				    document.body.style.cursor = "default";
				    var ExpandIds = [<%=m_AutoNavFolderIDs%>];
				    if( vFolderName != null ) {
					    treeRoot = new Tree( vFolderName, 0, null, folderRoot, 0 );
					    TreeDisplayUtil.showSelf( treeRoot);
					    //TreeDisplayUtil.toggleTree( treeRoot.node.id );
					    TreeDisplayUtil.expandTreeSet( ExpandIds );	//[   ServerSideCall(Path)   ]
                        document.getElementById("selected_folder_id").value = [<%=m_autonav%>] ;
                        var element = document.getElementById("L0_tv0");
                        element.className = "";
                    } else {
					    var element = document.getElementById( "TreeOutput" );
					    element.style["padding"] = "10pt";
					    var debugInfo = "<b>Cannot connect to the CMS server</b> "
					    element.innerHTML = debugInfo;
				    }
                    Explorer.onLoadExplorePanel();
                    }, 0 );
			    }
		    }
		}        
        
    </script>
</head>
<body <% if (m_bAjaxTree) { %> onclick="ContextMenuUtil.hide()" onload="Main.start();displayTree_New();window.setTimeout('setnode()', 3000);"
    <% } %>>
    <% if (m_bAjaxTree)
       { %>

    <script type="text/javascript" src="Tree/js/com.ektron.explorer.init.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.explorer.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.explorer.config.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.explorer.windows.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.cms.types.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.cms.parser.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.cms.toolkit.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.cms.api.js">
    </script>

    <script type="text/javascript" src='Tree/js/com.ektron.ui.contextmenu.js'>
    </script>

    <script type="text/javascript" src='Tree/js/com.ektron.ui.iconlist.js'>
    </script>

    <script type="text/javascript" src='Tree/js/com.ektron.ui.explore.js'>
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.ui.tree.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.net.http.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.lang.exception.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.utils.form.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.utils.log.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.utils.dom.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.utils.debug.js">
    </script>

    <script type="text/javascript" src="Tree/js/com.ektron.utils.string.js">
    </script>
    <script type="text/javascript">
        <!--        //--><![CDATA[//><!--

        //Debug.LEVEL = LogUtil.ALL;
        //LogUtil.logType = LogUtil.LOG_CONSOLE;

        var ContentUrl = 'libraryinsert.aspx?action=ViewLibraryByCategory&scope=<%=Request.QueryString["scope"]%><%=QueryType %><%=RetField%><%=disableLinkManage %><%=enableQDOparam%>&id='
        var FrameName = "Library";
        var rootFolderName = '<%=MsgHelper.GetMessage("generic Library title") %>';
        //--><!]]>
    </script>

    <%}%>
    <% if (m_bAjaxTree)
       { %>
    <div id="TreeOutput" style="width: 100%; height: 100%; overflow: auto;">
    </div>
    <input type="hidden" id="folderName" name="folderName" />
    <input type="hidden" id="selected_folder_id" name="selected_folder_id" value="0" />

    <script language="javascript" src="java/ektron.workareatrees.js" type="text/javascript">
    </script>

    <script type="text/javascript">
		    <!--        //--><![CDATA[//><!--
        // This function overrides the one of the same name in ektron.workareatrees.js
        function loadRightFrame(id, treeViewId, openMainPage) {
            top["libraryinsert"].location.href = ContentUrl + id;
        }
        //--><!]]>
    </script>

    <% }
       else
       { %>

    <script type="text/javascript" src="java/ekfoldercontrol.js">
    </script>

    <script type="text/javascript">
	<!--//--><![CDATA[//><!--
	function ClearFolderInfo() {

		<% if (actionType == "library") { %>
			top.libraryinsert.ClearFolderInfo();
		<% } %>
    }

	ekFolderCreateTextLinks = 1;
	ekFolderFontSize = 2;
	ekFolderMaxDescriptionLength=0;
	ekFolderImagePath = "images/application/folders/";

	<%  
	    SiteObj = AppUI.EkSiteRef;
        cPerms = SiteObj.GetPermissions(0, 0, "folder");
        if (cPerms.ContainsKey("ReadOnlyLib"))
        {
            if ((scope == "all"))
            {
                Response.Write(("var urlInfoArray = new Array(\"frame\", \"javascript:ClearFolderInfo();\", \"medialist\", \"frame\", \"library" +
                    "insert.aspx?action=ViewLibraryByCategory&id=" + (0 + ("&scope="
                                + (scope
                                + (RetField + "\", \"libraryinsert\");"))))));
            }
            else
            {
                Response.Write(("var urlInfoArray = new Array(\"frame\", \"javascript:ClearFolderInfo();\", \"medialist\", \"frame\", \"library" +
                    "insert.aspx?action=ViewLibraryByCategory&id=" + (0 + ("&scope="
                                + (scope
                                + (RetField + "&type=images\", \"libraryinsert\");"))))));
            }
            Response.Write(("TopTreeLevel = CreateFolderInstance(\""
                            + (MsgHelper.GetMessage("generic Library title") + "\", urlInfoArray);")));
        }
        else
        {
            Response.Write(("TopTreeLevel = CreateFolderInstance(\""
                            + (MsgHelper.GetMessage("generic Library title") + "\", \"\");")));
        }
        cDbObj = AppUI.EkContentRef;
        cAllFolders = cDbObj.GetFolderTreeForUserID(0);
        OutputLibraryFolders(0, 0); 
     %>
	//--><!]]>
    </script>

    <script type="text/javascript">
	    <!--        //--><![CDATA[//><!--
        InitializeFolderControl();

        
        //--><!]]>
    </script>
     <% } %>
</body>
</html>
