﻿using System;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Proxies.SearchRequestService;
using Ektron.Cms.Search.Contracts.Solr;
using Ektron.Cms.Search.Contracts;

public partial class Workarea_solrsearch_status : Ektron.Cms.Workarea.Page
{
    private const string ActionParameter = "action";
    private const string IncrementalCrawlAction = "incremental";
    private const string FullCrawlAction = "full";
    private readonly DateTime MinDate = new DateTime(1899, 12, 30, 0, 0, 0);
    private ISearchSettings searchSettingsProvider;
    private SearchSettingsData searchSettingsData;

    private SiteAPI _site;
    private StyleHelper _style;
    private SolrCrawler _crawler;

    public SiteAPI Sites
    {
        get
        {
            if (_site == null) _site = new SiteAPI();
            return _site;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ltrincremental.Text = this.Sites.EkMsgRef.GetMessage("js incremental crawl");
        ltrincrementalRequest.Text = this.Sites.EkMsgRef.GetMessage("js incremental crawl request");
        ltrFullC.Text = this.Sites.EkMsgRef.GetMessage("js full crawl");
        ltrFullRequest.Text = this.Sites.EkMsgRef.GetMessage("js full crawl request");

        if (HasPermission())
        {
            _style = new StyleHelper();
            _crawler = new SolrCrawler();

            JS.RegisterJS(this, JS.ManagedScript.EktronStyleHelperJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronJFunctJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaHelperJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaContextMenusJS);
            Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaCss);
            Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaIeCss, Css.BrowserTarget.LessThanEqualToIE8);

            styleHelper.Text = _style.GetClientScript();

            RenderToolbar();
            RenderTitleBar();

            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString[ActionParameter]) &&
                    (Request.QueryString[ActionParameter].ToString().ToLower() == IncrementalCrawlAction ||
                        Request.QueryString[ActionParameter].ToString().ToLower() == FullCrawlAction))
                {
                    switch (Request.QueryString[ActionParameter].ToLower())
                    {
                        case IncrementalCrawlAction:
                            {
                                _crawler.StartIncrementalCrawl(false, CrawlType.All, false);
                                break;
                            }

                        case FullCrawlAction:
                            {
                                _crawler.StartFullCrawl(CrawlType.All, false);
                                break;
                            }
                    }
                }
                else
                {
                    searchSettingsProvider = ObjectFactory.GetSearchSettings();
                    searchSettingsData = searchSettingsProvider.GetItem();

                    RenderState();
                    RenderSettings();
                }
            }
            catch (NullReferenceException)
            {
                Utilities.ShowError(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));
            }
            catch (Exception ex)
            {
                Utilities.ShowError(ex.Message);
            }

            if (!string.IsNullOrEmpty(Request.QueryString[ActionParameter]) &&
                (Request.QueryString[ActionParameter].ToString().ToLower() == IncrementalCrawlAction ||
                    Request.QueryString[ActionParameter].ToString().ToLower() == FullCrawlAction))
            {
                Response.Redirect("solrstatus.aspx");
            }
        }
        else
        {
            Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
        }
    }

    private string GetErrorMessage(string errorCode)
    {
        string result = String.Empty;
        string resource_prefix = "lbl solr admin svc ex ";
        switch (errorCode)
        {
            case CoreStateResponseStatusCode.ConfigurationException:
            case CoreStateResponseStatusCode.ConfigurationNotFoundException:
            case CoreStateResponseStatusCode.SettingsNotFoundException:
            case CoreStateResponseStatusCode.JobNotFoundException:
            case CoreStateResponseStatusCode.ManifoldResponseException:
            case CoreStateResponseStatusCode.GenericException:
                result = this.Sites.EkMsgRef.GetMessage(resource_prefix + errorCode.ToLower());
                break;
            default:
                result = this.Sites.EkMsgRef.GetMessage("lbl solr admin svc ex unhandled_error");
                break;
        }
        return result;
    }

    private string GetLocalizedString(JobStatus status)
    {
        string resource_prefix = "lbl manifold jobstatus ";
        string result = status.ToString();
        switch (status)
        {
            case JobStatus.Unknown:
            case JobStatus.RunningNoConnector:
            case JobStatus.Restarting:
            case JobStatus.Cleaningup:
            case JobStatus.StartingUp:
            case JobStatus.NotYetRun:
                result = this.Sites.EkMsgRef.GetMessage(resource_prefix + status.ToString().ToLower());
                break;
            default:
                result = status.ToString();
                break;
        }
        return result;
    }

    private string GetLocalizedString(JobAction action)
    {
        string result = action.ToString();
        switch (action)
        {
            case JobAction.IncrementalCrawl:
                result = this.Sites.EkMsgRef.GetMessage("lbl Incremental Crawl");
                break;

            case JobAction.FullCrawl:
                result = this.Sites.EkMsgRef.GetMessage("lbl Full Crawl");
                break;
            default:
                result = action.ToString();
                break;
        }
        return result;
    }

    private bool IsValidCrawlDuration(TimeSpan span)
    {
        TimeSpan none = new TimeSpan();
        return (span > none);
    }

    private void RenderState()
    {
        CoreState state = _crawler.GetState();

        if (state != null && state.CoreStateResponseStatus != null && (!String.IsNullOrWhiteSpace(state.CoreStateResponseStatus.ErrorCode)))
        {
            divErrorMessages.Visible = true;
            divStatusInfo.Visible = false;
            ucErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus error");
            ucErrorDescription.Text = GetErrorMessage(state.CoreStateResponseStatus.ErrorCode);
            foreach (string s in state.CoreStateResponseStatus.Details)
                ucErrorDetails.Text += s + "<br />";
            return;
        }

        divErrorMessages.Visible = false;
        divStatusInfo.Visible = true;
        if (state != null)
        {
            ucCoreName.Text = state.CoreName;

            ucSolrServer.Text = String.Format("http://{0}:{1}/{2}",
                                   state.SolrConfigurationData.SolrHostName,
                                   state.SolrConfigurationData.SolrPort.ToString(),
                                   state.SolrConfigurationData.SolrWebApp);
            anchorSolrServer.HRef = ucSolrServer.Text;

            ucManifoldServer.Text = String.Format("http://{0}:{1}/{2}",
                                               state.ManifoldConfigurationData.ManifoldHostName,
                                               state.ManifoldConfigurationData.ManifoldPort.ToString(),
                                               state.ManifoldConfigurationData.ManifoldWebApp);
            anchorManifoldServer.HRef = ucManifoldServer.Text;
            
            //Content Crawl
            if (state.ContentJobState.CrawlStartTime <= DateTime.MinValue)
                ucCrawlStartTime.Text = "-";
            else
                ucCrawlStartTime.Text = GetTimeString(state.ContentJobState.CrawlStartTime);
            if (state.ContentJobState.CrawlEndTime <= DateTime.MinValue)
                ucCrawlEndTime.Text = "-";
            else
                ucCrawlEndTime.Text = GetTimeString(state.ContentJobState.CrawlEndTime);
            
            if (IsValidCrawlDuration(state.ContentJobState.CrawlDuration))
                ucCrawlDuration.Text = state.ContentJobState.CrawlDuration.ToString("hh\\:mm\\:ss");
            else
                ucCrawlDuration.Text = "-";
            
            ucIsCrawlSchedule.Text = state.ContentJobState.IsCrawlScheduled ? this.Sites.EkMsgRef.GetMessage("generic yes") : this.Sites.EkMsgRef.GetMessage("generic no");
            ucManifoldStatus.Text = GetLocalizedString(state.ContentJobState.JobStatus);
            ucCurrentAction.Text = GetLocalizedString(state.ContentJobState.CurrentAction);
            ucPendingAction.Text = GetLocalizedString(state.ContentJobState.PendingAction);


            //User Crawl
            ucCrawlStartTimeUsers.Text = GetTimeString(state.UserJobState.CrawlStartTime);
            if (state.UserJobState.CrawlStartTime <= DateTime.MinValue)
                ucCrawlStartTimeUsers.Text = "-";
            else
                ucCrawlStartTimeUsers.Text = GetTimeString(state.UserJobState.CrawlStartTime);
            if (state.UserJobState.CrawlEndTime <= DateTime.MinValue)
                ucCrawlEndTimeUsers.Text = "-";
            else
                ucCrawlEndTimeUsers.Text = GetTimeString(state.UserJobState.CrawlEndTime);

            if (IsValidCrawlDuration(state.UserJobState.CrawlDuration))
                ucCrawlDurationUsers.Text = state.UserJobState.CrawlDuration.ToString("hh\\:mm\\:ss");
            else
                ucCrawlDurationUsers.Text = "-";

            ucIsCrawlScheduleUsers.Text = state.UserJobState.IsCrawlScheduled ? this.Sites.EkMsgRef.GetMessage("generic yes") : this.Sites.EkMsgRef.GetMessage("generic no");
            ucManifoldStatusUsers.Text = GetLocalizedString(state.UserJobState.JobStatus);
            ucCurrentActionUsers.Text = GetLocalizedString(state.UserJobState.CurrentAction);
            ucPendingActionUsers.Text = GetLocalizedString(state.UserJobState.PendingAction);


            //Community Groups Crawl
            if (state.CommunityGroupJobState.CrawlStartTime <= DateTime.MinValue)
                ucCrawlStartTimeGroups.Text = "-";
            else
                ucCrawlStartTimeGroups.Text = GetTimeString(state.CommunityGroupJobState.CrawlStartTime);
            if (state.CommunityGroupJobState.CrawlEndTime <= DateTime.MinValue)
                ucCrawlEndTimeGroups.Text = "-";
            else
                ucCrawlEndTimeGroups.Text = GetTimeString(state.CommunityGroupJobState.CrawlEndTime);
            
            if (IsValidCrawlDuration(state.CommunityGroupJobState.CrawlDuration))
                ucCrawlDurationGroups.Text = state.CommunityGroupJobState.CrawlDuration.ToString("hh\\:mm\\:ss");
            else
                ucCrawlDurationGroups.Text = "-";

            ucIsCrawlScheduleGroups.Text = state.CommunityGroupJobState.IsCrawlScheduled ? this.Sites.EkMsgRef.GetMessage("generic yes") : this.Sites.EkMsgRef.GetMessage("generic no");
            ucManifoldStatusGroups.Text = GetLocalizedString(state.CommunityGroupJobState.JobStatus);
            ucCurrentActionGroups.Text = GetLocalizedString(state.CommunityGroupJobState.CurrentAction);
            ucPendingActionGroups.Text = GetLocalizedString(state.CommunityGroupJobState.PendingAction);

            ucSearchProvider.Text = this.Sites.EkMsgRef.GetMessage("lbl provider solr server");
        }
        else
        {
            divErrorMessages.Visible = true;
            divStatusInfo.Visible = false;
            ucErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus error");
            string adminServiceUrl = (searchSettingsData != null ? searchSettingsData.AdminService: String.Empty);
            ucErrorDescription.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null response"), adminServiceUrl);
            ucErrorDetails.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null response details");
        }
    }

    private void RenderSettings()
    {       
        ucCrawlInterval.Text = searchSettingsData.Interval.ToString();
        
        string username = string.Empty;
        if (!string.IsNullOrWhiteSpace(searchSettingsData.Provider.AdminCredentials.Domain))
        {
            ucUsername.Text = searchSettingsData.Provider.AdminCredentials.Domain + "\\";
        }

        ucUsername.Text += searchSettingsData.Provider.AdminCredentials.Username;

        ucIncludeHtmlContent.Text = GetFilterString(searchSettingsData.Filter.IncludeHtmlContent);
        ucIncludeDocumentContent.Text = GetFilterString(searchSettingsData.Filter.IncludeDocuments);
        ucIncludeForumContent.Text = GetFilterString(searchSettingsData.Filter.IncludeForums);
        ucIncludeProductContent.Text = GetFilterString(searchSettingsData.Filter.IncludeProducts);
        ucIncludeCommunityMembers.Text = GetFilterString(searchSettingsData.Filter.IncludeCommunityMembers);
        ucIncludeCommunityContent.Text = GetFilterString(searchSettingsData.Filter.IncludeCommunityContent);
    }

    private void RenderTitleBar()
    {
        txtTitleBar.InnerHtml = _style.GetTitleBar(this.Sites.EkMsgRef.GetMessage("msg view search status"));
    }

    private void RenderToolbar()
    {
        tdIncrementalCrawlButton.InnerHtml = _style.GetButtonEventsWCaption(
            this.Sites.AppImgPath + "../UI/Icons/controlRepeat.png", 
            "javascript:crawlIncremental();",
            this.Sites.EkMsgRef.GetMessage("msg search incremental crawl button"),
            this.Sites.EkMsgRef.GetMessage("msg search incremental crawl button"),
            "",
			StyleHelper.CrawlIncrementalButtonCssClass);

        tdFullCrawlButton.InnerHtml = _style.GetButtonEventsWCaption(
            this.Sites.AppImgPath + "../UI/Icons/controlRepeatBlue.png",
            "javascript:crawlFull();",
            this.Sites.EkMsgRef.GetMessage("msg search full crawl button"),
            this.Sites.EkMsgRef.GetMessage("msg search full crawl button"),
            "",
			StyleHelper.CrawlFullButtonCssClass);

        tdRefreshButton.InnerHtml = _style.GetButtonEventsWCaption(
            this.Sites.AppImgPath + "../UI/Icons/refresh.png",
            "solrstatus.aspx",
            this.Sites.EkMsgRef.GetMessage("generic refresh"),
            this.Sites.EkMsgRef.GetMessage("generic refresh"),
            "",
			StyleHelper.RefreshButtonCssClass);
		
		tdSearchStatusHelpButton.InnerHtml = _style.GetHelpButton("crawl", "");
    }

    private string GetTimeString(DateTime input)
    {
        string timeString;
        if (input.Equals(MinDate))
        {
            timeString = this.Sites.EkMsgRef.GetMessage("generic NA");
        }
        else
        {
            timeString = input.ToString();
        }

        return timeString;
    }

    public string GetFilterString(bool input)
    {
        return input ? this.Sites.EkMsgRef.GetMessage("generic Included") : this.Sites.EkMsgRef.GetMessage("generic Excluded");
    }

    /// <summary>
    /// Returns true if the current user has sufficient permissions to access
    /// the functionality on this page, false otherwise.
    /// </summary>
    /// <returns>True if permissions are sufficient, false otherwise</returns>
    private static bool HasPermission()
    {
        return
            !((Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) ||
            ContentAPI.Current.RequestInformationRef.UserId == 0 ||
            !ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0)) &&
            !(ContentAPI.Current.IsARoleMember((long)EkEnumeration.CmsRoleIds.SearchAdmin, ContentAPI.Current.UserId, false)));
    }
}
