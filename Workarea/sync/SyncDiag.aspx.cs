﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Sync;
using Ektron.Cms.Sync.Client;
using Ektron.Cms.Sync.Presenters;
using Ektron.Cms.Sync.Web.Parameters;
using System.Text;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using Ektron.Diagnostics;
using Ektron.Diagnostics.DataCollection.SaveFile;

public partial class SyncDiag : System.Web.UI.Page
{
    private readonly CommonApi _commonApi;
    private readonly SiteAPI _siteApi;
    private readonly StyleHelper _styleHelper;

    public string HelpLocation { set; get; }

    public SyncDiag()
    {
        _commonApi = new CommonApi();
        _siteApi = new SiteAPI();
        _styleHelper = new StyleHelper();
    }

    public void Page_Init(object sender, EventArgs e)
    {
        HelpLocation = "";
        Utilities.ValidateUserLogin();
        if (!_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
        {
            Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
        }

        if (!string.IsNullOrEmpty(Request["DiagnosticDLL"]))
        {
            try
            {
                string directorypath = Path.GetDirectoryName(Server.MapPath(Request.Path)) + "\\syncdiag\\SL\\";

                List<string> fileList = new List<string>(Directory.EnumerateFiles(directorypath, "*.dll", SearchOption.TopDirectoryOnly));

                for (int i = 0; i < fileList.Count; i++)
                {
                    Response.Write(Path.GetFileName(fileList[i]));

                    if (i < fileList.Count - 1) Response.Write(",");
                }
            }
            catch (Exception) { }
            Response.End();
        }

        RegisterResources();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CommonApi api = new CommonApi();
        HelpLocation = api.fetchhelpLink("DiagnosticUtility");
		if (!HelpLocation.StartsWith("http"))
        {
            HelpLocation = api.FullyQualifyURL(HelpLocation);
        }
        RenderHeader();
    }

    private void RegisterResources()
    {
        ektronClientScript.Text = _styleHelper.GetClientScript();

        JS.RegisterJS(this, JS.ManagedScript.EktronJS);
        JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaJS);
        JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaHelperJS);
        JS.RegisterJS(this, JS.ManagedScript.EktronToolBarRollJS);

        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaCss);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaIeCss);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
    }

    private void RenderHeader()
    {
        divTitleBar.InnerHtml =
            _styleHelper.GetTitleBar("Ektron Diagnostics");

    }

    protected void btnDownloadSave_click(object sender, EventArgs e)
    {
        WorkareaProxy proxy = new WorkareaProxy();
        string output = proxy.GetNetworkExport(true, new Ektron.Diagnostics.NetworkDiagram.NodeData());
        Response.Redirect(output);
    }
}
