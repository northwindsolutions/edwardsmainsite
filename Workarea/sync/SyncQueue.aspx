﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SyncQueue.aspx.cs" Inherits="Workarea_sync_SyncQueue" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
        function ConfirmAliasDelete() {
            confirm('Are you sure you want to remove this job from the queue?');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:Css ID="Css1" Path="css/ektron.workarea.sync.queue.css" runat="server" />
        <div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="divTitleBar" runat="server">
                    <span id="WorkareaTitleBar">
                        <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                    </span><span id="_WorkareaTitleBar" style="display: none;"></span>
                </div>
                <div class="ektronToolbar" id="divToolBar" runat="server">
                    <table>
                        <tr>
                            <td class="column-PrimaryButton">
                                <%-- <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSaveButton"
                                class="primary saveButton" ValidationGroup="createpackage" OnClick="uxSaveButton_Click" />--%>
                            </td>
                            <td>
                                <div class="actionbarDivider">
                                </div>
                            </td>
                            <td>
                                <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="ektronPageContainer">
                <ektronUI:Message ID="uxSyncQueueDisabled" runat="server" DisplayMode="Information" Text="<% $Resources:SyncQueueDisabled %>">
                </ektronUI:Message>
                <ektronUI:Message ID="uxNoJobsMessage" runat="server" DisplayMode="Information" Visible="false">
                </ektronUI:Message>
                 <ektronUI:Message ID="uxCannotDeleteMessage" runat="server" DisplayMode="Error" Visible="false" Text="<% $Resources:JobCannotBeDeleted %>">
                </ektronUI:Message>
                <ektronUI:Message ID="uxCannotReorderMessage" runat="server" DisplayMode="Error" Visible="false" Text="<% $Resources:JobCannotBeReordered %>">
                </ektronUI:Message>
                <ektronUI:GridView ID="uxSyncQueueGridView" runat="Server" AutoGenerateColumns="false"
                    EktronUIEnabled="true" AllowPaging="false" PagerSettings-Visible="true">
                    <Columns>
                
                        <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/delete.png"
                                    ID="uxIbDeleteItem" Visible='<%# Container.DataItemIndex!=0%>' OnClientClick="if(confirm('Are you sure you want to remove this job from the queue?'))return true;else return false;" OnCommand="DeleteJob_Click" CommandArgument='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).JobId + "," + ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).State %>' CommandName="delJob" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lbreOrderUp" Text="" CommandName="reorderUP" CommandArgument='<%#((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).JobId %>' Visible='<%# Container.DataItemIndex > 1 %>' CssClass="ui-state-default ui-corner-all ui-icon ui-icon-triangle-1-n reorderUp" />
                                <asp:LinkButton runat="server" ID="lbreOrderDown" Text="" CommandName="reorderDOWN" CommandArgument='<%#((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).JobId %>' CssClass="ui-state-default ui-corner-all ui-icon ui-icon-triangle-1-s reorderDown"
                                    Visible='<%# Container.DataItemIndex > 0 && Container.DataItemIndex != ((List<Ektron.Cms.Sync.Client.QueueJobData>)uxSyncQueueGridView.DataSource).Count-1%>' Enabled='<%# Container.DataItemIndex!=0%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderId">
                            <ItemTemplate>
                                <asp:Literal ID="uxLitProfileId" runat="server" Text='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).ProfileId %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField meta:resourceKey="columnHeaderName">
                            <ItemTemplate>
                                <asp:Literal ID="uxLitProfileName" runat="server" Text='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).ProfileName %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderDate">
                            <ItemTemplate>
                                <asp:Literal ID="uxLitDateQueued" runat="server" Text='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).DateQueued %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderType">
                            <ItemTemplate>
                                <asp:Literal ID="uxLitType" runat="server" Text='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).Type %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderDestination">
                            <ItemTemplate>
                                <asp:Literal ID="uxLitDestination" runat="server" Text='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).Destination %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderStatus">
                            <ItemTemplate>
                                <asp:Literal ID="uxLitStatus" runat="server" Text='<%# ((Ektron.Cms.Sync.Client.QueueJobData)Container.DataItem).State %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </ektronUI:GridView>
            </div>
        </div>
    </form>
</body>
</html>
