﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SyncSettings.aspx.cs" Inherits="Ektron.Workarea.Sync.SyncSettings"
    Culture="auto" meta:resourcekey="page" UICulture="auto" %>

<%@ Register TagPrefix="ektronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:JavaScriptBlock ID="syncSettings" runat="server" ExecutionMode="OnEktronReady">
            <ScriptTemplate>
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="divTitleBar" runat="server">
                    <span id="WorkareaTitleBar">
                        <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                    </span><span id="_WorkareaTitleBar" style="display: none;"></span>
                </div>
                <div class="ektronToolbar" id="divToolBar" runat="server">
                    <table>
                        <tr>
                            <td class="column-PrimaryButton">
                                <asp:LinkButton runat="server" ID="uxSaveButton" meta:resourcekey="uxSaveButton"
                                    class="primary saveButton" ValidationGroup="createpackage" OnClick="uxSaveButton_Click" />
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="uxRelationshipList" meta:resourcekey="uxRelationshipList"
                                    DataTextField="RemoteSite.Connection.DatabaseName" DataValueField="RemoteRelationshipId" OnSelectedIndexChanged="uxRelationshipList_SelectedIndexChanged" AutoPostBack="true" />
                            </td>
                            <td>
                                <div class="actionbarDivider">
                                </div>
                            </td>
                            <td>
                                <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="ektronPageContainer">
                <ektronUI:Message ID="uxMessage" runat="server" Visible="false" DisplayMode="Success">
                </ektronUI:Message>
                <div class="settingsContainer">
                    <table>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblmemoryLimit" runat="server" meta:resourcekey="lblmemoryLimit"></asp:Label>
                            </td>
                            <td class="value">
                                <ektronUI:TextField ID="tbMemoryLimit" runat="server">
                                <ValidationRules>
                                    <ektronUI:DecimalRule />
                                </ValidationRules>
                                </ektronUI:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblConnectionType" runat="server" meta:resourcekey="lblConnectionType"></asp:Label>
                            </td>
                            <td class="value">
                                <asp:DropDownList ID="ddlConnectionType" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblTranSize" runat="server" meta:resourcekey="lblTranSize"></asp:Label>
                            </td>
                            <td class="value">
                                <ektronUI:TextField ID="tbTranSize" runat="server">
                                
                                </ektronUI:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblScriptEncrypt" runat="server" meta:resourcekey="lblScriptEncrypt"></asp:Label>
                            </td>
                            <td class="value">
                                <asp:CheckBox ID="cbEncrypt" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblBackupAction" runat="server" meta:resourcekey="lblBackupAction"></asp:Label>
                            </td>
                            <td class="value">
                                <asp:DropDownList ID="ddlBackupAction" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblBackupLocation" runat="server" meta:resourcekey="lblBackupLocation"></asp:Label>
                            </td>
                            <td class="value">
                                <asp:TextBox ID="tbBackupLocation" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lblCollisionAction" runat="server" meta:resourcekey="lblCollisionAction"></asp:Label>
                            </td>
                            <td class="value">
                                <asp:CheckBoxList ID="cblCollisionAction" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
