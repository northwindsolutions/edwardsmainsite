﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Sync.Client;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.Context;

namespace Ektron.Workarea.Sync
{
    public partial class SyncSettings : System.Web.UI.Page
    {
        #region Local Variables

        private SiteAPI siteAPI = new SiteAPI();
        private long selectedRelationshipID = 0;

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            if (this.HasViewEditPermission())
            {
                try
                {
                    base.OnInit(e);
                    this.initWorkareaUI();
                }
                catch (Exception exc)
                {
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.bindData();
            }
        }

        protected void uxSaveButton_Click(object sender, EventArgs e)
        {
            long.TryParse(uxRelationshipList.SelectedValue, out selectedRelationshipID);

            DbSyncSettings globalSettings = DbSyncSettings.GetItem(selectedRelationshipID);
            globalSettings.Id = selectedRelationshipID;
            globalSettings.MemoryDataCacheSize = int.Parse(tbMemoryLimit.Text);
            globalSettings.RemoteSqlConnection = (DbSyncSettings.SqlConnection)(Enum.Parse(typeof(DbSyncSettings.SqlConnection), ddlConnectionType.SelectedValue));
            globalSettings.ApplicationTranscationSize = long.Parse(tbTranSize.Text);
            globalSettings.ScriptEncryption = cbEncrypt.Checked;
            globalSettings.BackUpAction = byte.Parse(ddlBackupAction.SelectedValue);
            globalSettings.BackUpDevice = tbBackupLocation.Text;
            globalSettings.CollisionAction = 0; // Reset to 0 and repopulate
            globalSettings.CollisionAction = cblCollisionAction.Items
                                                .Cast<ListItem>()
                                                .Where(item => item.Selected)
                                                .Sum(item => int.Parse(item.Value));
            globalSettings.Save();

            uxMessage.Text = GetLocalResourceObject("SettingsSaved").ToString();
            uxMessage.Visible = true;
            JavaScript.RegisterJavaScriptBlock(this, "$ektron('#uxMessage').delay(3000).hide('200');;");
        }

        #endregion

        #region private methods

        private void bindData()
        {
            DbSyncSettings globalSettings = DbSyncSettings.GetItem(selectedRelationshipID);
            tbMemoryLimit.Text = globalSettings.MemoryDataCacheSize.ToString();
            ddlConnectionType.SelectedValue = globalSettings.RemoteSqlConnection.GetHashCode().ToString();
            tbTranSize.Text = globalSettings.ApplicationTranscationSize.ToString();
            cbEncrypt.Checked = globalSettings.ScriptEncryption;
            ddlBackupAction.SelectedValue = globalSettings.BackUpAction.GetHashCode().ToString();
            tbBackupLocation.Text = globalSettings.BackUpDevice;

            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.Users);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.Folders);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.MetaData);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.EmailMessages);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.UrlAliases);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.TagEntries);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.Statistics);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.TaxonomyCounts);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.PriceEntries);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.MultiSiteStatgingUrls);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.Menus);
            checkboxIfSelected(globalSettings.CollisionAction, DbSyncSettings.DbSyncCollisionAction.ClearCacheonTables);

        }

        /// <summary>
        /// Check the flags enum
        /// </summary>
        /// <param name="selectedActions"></param>
        /// <param name="actionToCheck"></param>
        private void checkboxIfSelected(long selectedActions, DbSyncSettings.DbSyncCollisionAction actionToCheck)
        {
            if ((selectedActions & actionToCheck.GetHashCode()) == actionToCheck.GetHashCode())
            {
                cblCollisionAction.Items.FindByValue(actionToCheck.GetHashCode().ToString()).Selected = true;
            }
            else
            {
                cblCollisionAction.Items.FindByValue(actionToCheck.GetHashCode().ToString()).Selected = false;
            }
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();
            StyleHelper styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetHelpButton("sync_settings", string.Empty);
            uxMessage.Visible = false;

            if (!Page.IsPostBack)
            {
                ddlConnectionType.Items.Add(new ListItem(getLocalSQLConnectionTypeName(DbSyncSettings.SqlConnection.AutoDetect), DbSyncSettings.SqlConnection.AutoDetect.GetHashCode().ToString()));
                ddlConnectionType.Items.Add(new ListItem(getLocalSQLConnectionTypeName(DbSyncSettings.SqlConnection.Direct), DbSyncSettings.SqlConnection.Direct.GetHashCode().ToString()));
                ddlConnectionType.Items.Add(new ListItem(getLocalSQLConnectionTypeName(DbSyncSettings.SqlConnection.Proxy), DbSyncSettings.SqlConnection.Proxy.GetHashCode().ToString()));

                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.Users), DbSyncSettings.DbSyncCollisionAction.Users.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.Folders), DbSyncSettings.DbSyncCollisionAction.Folders.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.MetaData), DbSyncSettings.DbSyncCollisionAction.MetaData.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.EmailMessages), DbSyncSettings.DbSyncCollisionAction.EmailMessages.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.UrlAliases), DbSyncSettings.DbSyncCollisionAction.UrlAliases.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.TagEntries), DbSyncSettings.DbSyncCollisionAction.TagEntries.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.Statistics), DbSyncSettings.DbSyncCollisionAction.Statistics.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.TaxonomyCounts), DbSyncSettings.DbSyncCollisionAction.TaxonomyCounts.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.PriceEntries), DbSyncSettings.DbSyncCollisionAction.PriceEntries.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.MultiSiteStatgingUrls), DbSyncSettings.DbSyncCollisionAction.MultiSiteStatgingUrls.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.Menus), DbSyncSettings.DbSyncCollisionAction.Menus.GetHashCode().ToString()));
                cblCollisionAction.Items.Add(new ListItem(getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction.ClearCacheonTables), DbSyncSettings.DbSyncCollisionAction.ClearCacheonTables.GetHashCode().ToString()));

                ddlBackupAction.Items.Add(new ListItem(getLocalBackupActionName(DbSyncSettings.BackupAction.None), DbSyncSettings.BackupAction.None.GetHashCode().ToString()));
                ddlBackupAction.Items.Add(new ListItem(getLocalBackupActionName(DbSyncSettings.BackupAction.InitialSync), DbSyncSettings.BackupAction.InitialSync.GetHashCode().ToString()));
                ddlBackupAction.Items.Add(new ListItem(getLocalBackupActionName(DbSyncSettings.BackupAction.FullSync), DbSyncSettings.BackupAction.FullSync.GetHashCode().ToString()));
            }

            // Get relationships and bind to dropdown.
            List<Relationship> relationships = Relationship.GetRelationships();
            if (relationships != null && relationships.Count > 0)
            {
                ListItem defaultRelationshipListItem = new ListItem("Global", "0");
                uxRelationshipList.Items.Add(defaultRelationshipListItem);
                foreach (Relationship relationship in relationships)
                {
                    ListItem relationshipListItem = new ListItem(relationship.RemoteSite.Connection.DatabaseName, relationship.Id.ToString());
                    uxRelationshipList.Items.Add(relationshipListItem);
                }
                uxRelationshipList.Visible = true;
            }
            else
            {
                uxRelationshipList.Visible = false;
            }
        }

        private string getLocalBackupActionName(DbSyncSettings.BackupAction action)
        {
            string returnVal;
            try
            {
                returnVal = GetLocalResourceObject("backupAction" + action.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = action.ToDescriptionString();
            }
            return returnVal;
        }

        private string getLocalCollisionActionName(DbSyncSettings.DbSyncCollisionAction action)
        {
            string returnVal;
            try
            {
                returnVal = GetLocalResourceObject("collisionAction" + action.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = action.ToDescriptionString();
            }
            return returnVal;
        }

        private string getLocalSQLConnectionTypeName(DbSyncSettings.SqlConnection connectionType)
        {
            string returnVal;
            try
            {
                returnVal = GetLocalResourceObject("connectionType" + connectionType.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = connectionType.ToDescriptionString();
            }
            return returnVal;
        }

        private bool HasViewEditPermission()
        {
            // IF       CMS USER
            // AND      LOGGED IN
            // AND      IS ADMIN    OR  IS SYNC ADMIN
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin), ContentAPI.Current.UserId, false))
                );
        }

        private void RegisterResources()
        {
            Ektron.Cms.Framework.UI.Packages.EktronCoreJS.Register(this);
            Ektron.Cms.Framework.UI.Packages.Ektron.Namespace.Register(this);
            Ektron.Cms.Framework.UI.Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.Framework.UI.JavaScript.Register(this, CmsContextService.Current.WorkareaPath + "/java/jfunct.js");
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.Css.RegisterCss(this, "css/Ektron.Workarea.Sync.Settings.css", "SyncSettingsCSS");
        }

        #endregion
        protected void uxRelationshipList_SelectedIndexChanged(object sender, EventArgs e)
        {
            long parsedSelectedRelationshipID = 0;
            long.TryParse(uxRelationshipList.SelectedValue, out parsedSelectedRelationshipID);
            this.selectedRelationshipID = parsedSelectedRelationshipID;
            this.bindData();
            uxMessage.Visible = false;
        }
    }

}