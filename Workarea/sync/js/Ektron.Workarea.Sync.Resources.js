// Define Ektron object only if it's not already defined
if (typeof (Ektron) == "undefined") {
    Ektron = {};
}

// Define Ektron.Workarea object only if it's not already defined
if (typeof (Ektron.Workarea) == "undefined") {
    Ektron.Workarea = {};
}

// Define Ektron.Workarea.Sync object only if it's not already defined
if (typeof (Ektron.Workarea.Sync) == "undefined") {
    Ektron.Workarea.Sync = {};
}

// Define Ektron.Workarea.Sync.Resources object only if it's not already defined
if (typeof (Ektron.Workarea.Sync.Resources) == "undefined") {
    Ektron.Workarea.Sync.Resources = {};
}
