<%@ Page Language="C#" AutoEventWireup="true" CodeFile="template_config.aspx.cs"
    Inherits="template_config" %>

<%@ Register Src="pagebuilder/foldertree.ascx" TagName="FolderTree" TagPrefix="CMS" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="Controls/paging/paging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select Template to Add</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="java/plugins/modal/ektron.modal.css" />
    <link type="text/css" rel="stylesheet" href="java/plugins/treeview/ektron.treeview.css" />
    <style type="text/css"  >
        .deviceheader
        {
            color: #2E6E9E;
            font-weight:bold;
            padding-left: 4px;
            padding-right: 4px;
        }
        .devicelabel
        {   
            width: 146px;
        }
        .device-header-wrap
        {   
            width: 140%;
        }
        .breakPointlabel
        {
           width:180px;
           padding-left:4px;
        }
        .widget {}        
        .ektronPageContainer 
        {
            background: none repeat scroll 0 0 transparent;            
        }
        .break-point-config
        {
            width:533px;
        }
        .break-point-config input[type="text"]
        {
            width:300px;
        }
    </style>
    <!-- Scripts -->
    <script language="javascript" type="text/javascript" src="java/plugins/modal/ektron.modal.js"></script>
    <script language="javascript" type="text/javascript" src="java/plugins/treeview/ektron.treeview.js"></script>
   
    <script language="javascript" type="text/javascript">

        function disableEnterKey(event) {
            if (event.keyCode == 13) {
                return false;
            }
            return true;
        }
        function ConfirmUpdate() {
            if (confirm('Are you sure you wish to update the active list of templates to this configuration?')) {
                document.forms[0].submit();
                return true;
            }
            else {
                return false;
            }
        }

        function CancelAddTemplate() {
            var notSupportIFrame = "0";
            if (notSupportIFrame == "1") {
                top.close();
            }
            else {
                if (parent.CloseChildPage == 'undefined' || parent.CloseChildPage == null) {
                    top.close();
                }
                else {
                    parent.CloseChildPage();
                }
            }
            return false;
        }

        function ValidateTemplateNames(fieldName, deviceFieldNames, breakPointIds) {
            if (deviceFieldNames != "") {
                var deviceField;
                var splitResult = deviceFieldNames.split(",");
                for (var i = 0; i < splitResult.length; i++) {
                    deviceField = $ektron("#updateDeviceTemplate_" + splitResult[i]).val();
                    if ((deviceField.indexOf("\\") >= 0) || (deviceField.indexOf(":") >= 0) || (deviceField.indexOf("*") >= 0) || (deviceField.indexOf("\"") >= 0) || (deviceField.indexOf("<") >= 0) || (deviceField.indexOf(">") >= 0) || (deviceField.indexOf("|") >= 0) || (deviceField.indexOf("\'") >= 0)) {
                        alert("Device template name can't include ('\\', ':', '*', ' \" ', '<', '>', '|', '\'').");
                        return;
                    }
                }
            }

            if (breakPointIds != "") {
                var deviceField;
                var splitResult = breakPointIds.split(",");
                for (var i = 0; i < splitResult.length; i++) {
                    deviceField = $ektron("#updateBreakpointTemplate_" + splitResult[i]).val();
                    if ((deviceField.indexOf("\\") >= 0) || (deviceField.indexOf(":") >= 0) || (deviceField.indexOf("*") >= 0) || (deviceField.indexOf("\"") >= 0) || (deviceField.indexOf("<") >= 0) || (deviceField.indexOf(">") >= 0) || (deviceField.indexOf("|") >= 0) || (deviceField.indexOf("\'") >= 0)) {
                        alert("Breakpoint template name can't include ('\\', ':', '*', ' \" ', '<', '>', '|', '\'').");
                        return;
                    }
                }
            }

            var field = $ektron("#" + fieldName);
            if (!field.hasClass("masterlayout")) {
                if (Trim(field.val()) != '' && !field.hasClass("masterlayout")) {
                    field = field.val();
                    if ((field.indexOf("\\") >= 0) || (field.indexOf(":") >= 0) || (field.indexOf("*") >= 0) || (field.indexOf("\"") >= 0) || (field.indexOf("<") >= 0) || (field.indexOf(">") >= 0) || (field.indexOf("|") >= 0) || (field.indexOf("\'") >= 0)) {
                        alert("Template name can't include ('\\', ':', '*', ' \" ', '<', '>', '|', '\'').");
                    }
                    else {
                        document.forms[0].submit();
                    }
                }
                else {
                    alert('<asp:Literal ID="ltrTemplateMessage" runat="server" />');
                }
            } else {
                document.forms[0].submit();
            }
        }

        function AddTemplateEntry(template_id, template_name) {
            var parentdoc = parent.document;
            var list = parentdoc.getElementById('addTemplate');
            if (list == null) {
                // see if it's in opener (older non-modal code)
                list = opener.document.getElementById('addTemplate');
                parentdoc = opener.document;
            }
            if (list != null) {
                var new_entry = parentdoc.createElement('option');

                new_entry.appendChild(parentdoc.createTextNode(template_name));
                new_entry.value = template_id;
                list.appendChild(new_entry);

                // select what was added so user can add it by clicking the add button
                for (var i = 0; i < list.options.length; i++) {
                    if (list.options[i].value == template_id) {
                        list.selectedIndex = i;
                        break;
                    }
                }

                if (parent.CloseChildPage == 'undefined' || parent.CloseChildPage == null) {
                    top.close();
                }
                else {
                    parent.CloseChildPage();
                }
            }
            else {
                parent.ReloadPage();
                top.close();
            }
        }

        function ReloadPage() {
            top.opener.document.location.reload(true);
        }
        function OpenAddDialog() {
            window.open('template_config.aspx?view=add', '', 'width=650,height=600,resizable=0');
        }

        function AddToEditFolderList(template_id, template_name) {
            var t_list = window.opener.document.getElementById('addTemplate');
            var selectList = document.getElementById(element_id);
            var newOption = document.createElement('option');
            newOption.value = xml_id;
            newOption.appendChild(document.createTextNode(xml_name));
            selectList.appendChild(newOption);
        }

        function ShowBrowseDialog() {

        }

        function OnPageBuilderCheckboxChanged(checked) {
            $ektron("#widgetDisplay").css("display", checked ? "block" : "none");
        }

        function SelectAllWidgets() {
            var widgets = $ektron(".widget");
            widgets.each(function (i) {
                var widget = $ektron(widgets[i]);
                var checkbox = widget.find("input");
                if (!checkbox.is(":checked")) {
                    widget.addClass("selected");
                    ToggleCheckbox(checkbox);
                }
            });
        }

        function SelectWidgets(widgetIds) {
            for (var i in widgetIds) {
                var id = widgetIds[i];
                SelectWidget(id);
            }
        }

        function SelectWidget(id) {
            var checkbox = $ektron(".widget input#widget" + id);
            var widget = checkbox.parent(".widget");
            if (!checkbox.is(":checked")) {
                widget.addClass("selected");
                ToggleCheckbox(checkbox);
            }
        }

        function UnselectAllWidgets() {
            var widgets = $ektron(".widget");
            widgets.each(function (i) {
                var widget = $ektron(widgets[i]);
                var checkbox = widget.find("input");
                if (checkbox.is(":checked")) {
                    widget.removeClass("selected");
                    ToggleCheckbox(checkbox);
                }
            });
        }

        function OnBrowseButtonClicked() {
            // show folder selection dialog
        }

        function ToggleCheckbox(checkbox) {
            if (checkbox.is(":checked")) {
                checkbox.removeAttr("checked");
            }
            else {
                checkbox.attr("checked", "checked");
            }
        }

        function PrintObject(obj) {
            var str = "";
            var count = 0;
            for (var i in obj) {
                str += i + ":" + obj[i] + "; ";

                if (++count == 10) {
                    count = 0;
                    str += "\n";
                }
            }
            alert(str);
        }

        function OnFileClicked(path) {
            var idBnt = $ektron("#browsebtnclicked").val();
            var input;
            if (idBnt == "") {
                input = $ektron("input#updateTemplate");
                if (input.length == 0)
                    input = $ektron("input#addTemplate");
                if (input.length == 0)
                    return;
            }
            else {
                input = $ektron("input#" + idBnt);
            }
            $ektron("#browsebtnclicked").val("");
            input.attr("value", path.replace(/\\/g, "/"));
            $ektron("#dlgBrowse").modal().modalHide();
        }

        Ektron.ready(function () {
            // add ektronPageHeader since baseClass doesn't add it for us
            // ...base class adds ektronPageHeader now
            // $ektron("table.baseClassToolbar").wrap("<div class='ektronPageHeader'></div>");

            // Initialize browse dialog
            $ektron("#dlgBrowse").modal({
                modal: true, onShow: function (h) {
                    $ektron("#dlgBrowse").css("margin-top", -1 * Math.round($ektron("#dlgBrowse").outerHeight() / 2)); h.w.show();
                }
            });

            // Initialize PageBuilder checkbox
            var checkbox = $ektron(".pageBuilderCheckbox input");

            OnPageBuilderCheckboxChanged(checkbox.is(':checked'));

            checkbox.click(function () {
                OnPageBuilderCheckboxChanged($ektron(".pageBuilderCheckbox input").is(':checked'));
            });

            // Initialize widgetType display

            var widgets = $ektron(".widget");
            widgets.each(function (i) {
                var widget = $(widgets[i]);
                if (widget.find("input").is(":checked")) {
                    widget.addClass("selected");
                }

                widget.click(function () {

                    var widgetCheckbox = widget.find("input");

                    ToggleCheckbox(widgetCheckbox);
                    if (widgetCheckbox.is(":checked")) {
                        widget.addClass("selected");
                    }
                    else {
                        widget.removeClass("selected");
                    }
                });
            });
        });

        function SetBtnClicked(id) {
            $ektron("#browsebtnclicked").val("updateDeviceTemplate_" + id);
        }
        function SetBtnBreakpointClicked(id) {
            $ektron("#browsebtnclicked").val("updateBreakpointTemplate_" + id);
        }
    </script>

</head>
<body>
    <form id="form1" method="post" enctype="multipart/form-data" class="newTemplateForm"
    runat="server">
    <div id="dlgBrowse" class="ektronWindow ektronModalStandard">
        <div class="ektronModalHeader">
            <h3>
                <asp:Literal ID="selectTemplate" runat="server" />
                <asp:HyperLink ToolTip="Close" ID="closeDialogLink" CssClass="ektronModalClose" runat="server" />
            </h3>
        </div>
        <div class="ektronModalBody">
            <div class="folderTree">
                <CMS:FolderTree ID="folderTree" runat="server" Filter="[.]aspx$" />
            </div>
        </div>
    </div>
    <asp:Literal ID="MainBody" runat="server" />
    <div class="ektronPageContainer">
        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
        <ektronUI:CssBlock runat="server">
            <CssTemplate>
                div.ajaxloadingimage 
                {
	                position:fixed;z-index:99999;background-color:White;background-image:url('<%= this.WorkareaPath %>/images/UI/loading_big.gif');
	                background-position:center center;background-repeat:no-repeat;left:0;top:0;right:0;	bottom:0;opacity:.6;filter:Alpha(Opacity=60);
                }
            </CssTemplate>
        </ektronUI:CssBlock>
        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="uxTempalteViewUpdatePanel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="ajaxloadingimage" class="ajaxloadingimage"></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="uxTempalteViewUpdatePanel" runat="server">
            <ContentTemplate>
                <ektronUI:GridView ID="uxTemplateView" runat="server" AutoGenerateColumns="false" OnEktronUIPageChanged="uxTemplateView_EktronUIPageChanged">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="<%$ Resources:Id %>" />
                        <asp:TemplateField HeaderText="<%$ Resources:TemplateName %>">
                            <ItemTemplate>
                                <%# GetFileName(Container.DataItem) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:FileName %>">
                            <ItemTemplate>
                                <a style="text-decoration:none;" href="<%# this.GetTemplateUpdateLink(Container.DataItem) %>" title="<%# this.GetTemplateUpdateLinkTitle() %>">
                                    <img src="<%# this.GetTemplateIcon(Container.DataItem) %>" title="<%# this.GetTemplateText(Container.DataItem) %>" alt="<%# this.GetTemplateText(Container.DataItem) %>" />
                                    <span style="text-decoration:underline;"><%# DataBinder.Eval(Container.DataItem, "FileName") %></span>
                                    <span style="margin-left:.5em;"><%# this.GetTempalteLabel(Container.DataItem) %></span>
                                </a> 
                            </ItemTemplate>
                        </asp:TemplateField>
                        <ektronUI:CheckBoxField ID="EnableToolbar" OnCheckedChanged="EnableToolbar_CheckedChanged">
                            <CheckBoxFieldHeaderStyle HeaderText="Enable Toolbar" />
                            <CheckBoxFieldItemStyle CheckedBoundField="IsToolbarEnabled" />
                            <KeyFields>
                                <ektronUI:KeyField DataField="Id" />
                            </KeyFields>
                            <ValueFields>
                                <ektronUI:ValueField Key="Id" DataField="Id" />
                                <ektronUI:ValueField Key="FileName" DataField="FileName" />
                                <ektronUI:ValueField Key="Description" DataField="Description" />
                                <ektronUI:ValueField Key="TemplateName" DataField="TemplateName" />
                                <ektronUI:ValueField Key="Type" DataField="Type" />
                                <ektronUI:ValueField Key="Thumbnail" DataField="Thumbnail" />
                                <ektronUI:ValueField Key="SubType" DataField="SubType" />
                                <ektronUI:ValueField Key="MasterLayoutID" DataField="MasterLayoutID" />
                            </ValueFields>
                        </ektronUI:CheckBoxField>
                        <asp:TemplateField HeaderText="<%$ Resources:Description %>">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Description") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:DeleteTemplate %>">
                            <ItemStyle Width="50px" />
                            <ItemTemplate>
                                <div style="text-align:center;cursor:pointer;">
                                    <asp:Image runat="server" onclick="<%# this.GetDeleteTempalteOnClick(Container.DataItem) %>" AlternateText="<%# this.DeleteTemplateText %>" ToolTip="<%# this.DeleteTemplateText %>" ImageUrl="<%# this.DeleteImagePath %>" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </ektronUI:GridView>
                <ektronUI:JavaScriptBlock runat="server" ExecutionMode="OnParse">
                    <ScriptTemplate>
                        if ("undefined" == typeof (Ektron)) { Ektron = {}; }
                        if ("undefined" == typeof (Ektron.Workarea)) { Ektron.Workarea = {}; }
                        if ("undefined" == typeof (Ektron.Workarea.Settings)) { Ektron.Workarea.Settings = {}; }
                        Ektron.Workarea.Settings.TemplateConfig = {
                            confirmDelete: function(id){
                                $ektron("<%# uxDeleteTemplateDialog.Selector %>").data("templateId", id);
                                $ektron("<%# uxDeleteTemplateDialog.Selector %>").dialog("open");
                            },
                            delete: function(){
                                var id = $ektron("<%# uxDeleteTemplateDialog.Selector %>").data("templateId");
                                window.location = "template_config.aspx?view=delete&id=" + id;
                            }
                        } 
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <ektronUI:Dialog ID="uxDeleteTemplateDialog" runat="server" Modal="true" Draggable="true" Resizable="false" AutoOpen="false" Title="<%$ Resources:DeleteTemplate %>">
                    <Buttons>
                        <ektronUI:DialogButton ID="uxDeleteTemplateButton" runat="server" OnClientClick="Ektron.Workarea.Settings.TemplateConfig.delete();return false;" Text="<%$ Resources:Delete %>" />
                        <ektronUI:DialogButton runat="server" Text="<%$ Resources:Cancel %>" CloseDialog="true" />
                    </Buttons>
                    <ContentTemplate>
                        <ektronUI:Message runat="server" DisplayMode="Warning">
                            <ContentTemplate>
                                <asp:Literal runat="server" Text="<%$ Resources:ConfirmDelete %>" />
                            </ContentTemplate>
                        </ektronUI:Message>
                    </ContentTemplate>
                </ektronUI:Dialog>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="formBody">
        <asp:Panel ID="pnlPageBuilder" runat="server" Visible="False">
            <div class="ektronPageContainer ektronPageInfo">
                <table>
                    <tbody>
                        <tr>
                            <td class="label">
                                <label for="addTemplate" title="<%= lblTemplateFile.Text%>: <%= m_refContentApi.SitePath%>">
                                    <asp:Literal ID="lblTemplateFile" runat="server" />:</label>
                            </td>
                            <asp:Literal ID="sitePathValue" runat="server" />
                            <td class="value" id="browsebuttontd" runat="server">
                                <input title="Browse" type="button" value="..." class="ektronModal browseButton"
                                    onclick="OnBrowseButtonClicked()" />
                            </td>
                        </tr>
                        <asp:Literal ID="ltrTemplateDetails" runat="server"></asp:Literal>
                        <tr>
                            <td class="label" colspan="2">
                                <asp:CheckBox ToolTip="Enable Toolbar" ID="uxTemplateDetailsEnableToolbarCheckbox" Text="<%$ Resources:EnableToolbar %>" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" colspan="2">
                                <asp:CheckBox ToolTip="Page Builder Wireframe" ID="cbPageBuilderTemplate" runat="server"
                                    CssClass="pageBuilderCheckbox clearfix" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="pageBuilderInfo" runat="server">
                    <div id="widgetDisplay" class="templateConfig" style="display: none">
                        <fieldset>
                            <legend>
                                <asp:Literal ID="lblSelectWidgets" runat="server" /></legend>
                            <div class="widgetsHeader ui-helper-clearfix">
                                <h4>
                                    <asp:Literal ID="widgetTitle" runat="server" /></h4>
                                <ul id="widgetActions" class="buttonWrapper clearfix">
                                    <li>
                                        <asp:LinkButton ID="btnSelectNone" runat="server" CssClass="redHover button selectNoneButton buttonLeft"
                                            OnClientClick="UnselectAllWidgets();return false;" /></li>
                                    <li>
                                        <asp:LinkButton ID="btnSelectAll" runat="server" CssClass="greenHover button selectAllButton buttonRight"
                                            OnClientClick="SelectAllWidgets();return false;" /></li>
                                </ul>
                            </div>
                            <div id="widgets">
                                <ul id="widgetList">
                                    <asp:Repeater ID="repWidgetTypes" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <div class="widget">
                                                    <input type="checkbox" name="widget<%# ((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).ID %>"
                                                        id="widget<%#((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).ID %>" /><img
                                                            src="<%#m_siteApi.SitePath + "widgets/" + ((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).ControlURL + ".jpg"%>"
                                                            alt="<%#((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).Title%>" title="<%#((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).Title%>" /><div
                                                                class="widgetTitle" title="<%#((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).Title%>">
                                                                <%#((Ektron.Cms.Widget.WidgetTypeData)Container.DataItem).Title%></div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </fieldset>
                    </div>
                    <asp:Label ID="lbStatus" runat="server" />
                </div>
                <div class="ektronTopSpace break-point-config">
                    <fieldset>
                        <legend runat="server" id="aspFieldSet"></legend>
                            <asp:Label runat="server" ID="lblDeviceGroupDesc" />
                            <ektronUI:Infotip ID="uxInfo" runat="server"></ektronUI:Infotip>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Literal ID="ltrDeviceConfigurations" runat="server" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                    </fieldset>
                </div>
            </div>
        </asp:Panel>
    </div>
    <input type="hidden" name="browsebtnclicked" id="browsebtnclicked" value="" />
    </form>
</body>
</html>
