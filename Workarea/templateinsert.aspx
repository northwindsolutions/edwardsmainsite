﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="templateinsert.aspx.cs" Inherits="Workarea_templateinsert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        /*style for ektronUI:FolderTree on templateinsert*/
        .folderWrapper {
            overflow: auto;
            white-space: nowrap;
            float: left;
            width: 200px;
            height: 484px;
        }

        .templateGrid {
            overflow: auto;
            height: 484px;
        }

        .ektron-ui-tree ul {
            margin-left: 5px;
        }

        .ektron-ui-tree ul li > ul {
                padding-left: 8px;
        }

    </style>
</head>
<script language="javascript" type="text/javascript">

    $(document).ready(function () {
        $ektron(' .folderWrapper .ektron-ui-tree-root').on('click', '.selectionLocation', function (event) {
            loadChildContent(this);
            return false;
        });
    });      //end ready

    function insertHTML(value) {
        var id = "#content-" + value;
        var text = $ektron(id)[0].innerHTML;
        text = $ektron.trim(text);
        if (text.indexOf('<') == -1)
            text = '<span>' + text + '</span>';
        if (Ektron.Namespace.Exists("parent.Ektron.Template.AcceptHTML")) {
            parent.Ektron.Template.AcceptHTML({ "html": text });
        }
    }

    function loadChildContent(selectedItem) {
        var selectedFolderID = $ektron(selectedItem).attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().attr('data-ektron-id');
        if (typeof (selectedFolderID) == "undefined")
            selectedFolderID = $ektron(selectedItem).parent().parent().parent().attr('data-ektron-id');

        var currentFolder = $ektron('.templateInsertContent .hdnSourceFolder').val();
        if (selectedFolderID != currentFolder) {
            $ektron('.templateInsertContent .hdnSourceFolder').val(selectedFolderID);
            $ektron('.templateInsertContent .uxTriggerGetContent').click();
        }

    }
</script>
<body>
    <form id="form1" runat="server">
        <div class="folderWrapper">
            <ektronUI:FolderTree ID="uxFolderTree" runat="server" UseInternalAdmin="true" CssClass="showControls" OnPreSerializeData="PreSerializeHandler">
            </ektronUI:FolderTree>
        </div>
        <asp:ScriptManager ID="uxPackageScriptManager" runat="server">
        </asp:ScriptManager>
        <div class="templateInsertContent">
            <asp:UpdatePanel ID="uxUpdatePanelContent" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="hdnFields">
                        <input type="hidden" id="uxOldSourceFolder" runat="server" class="hdnOldSourceFolder" />
                        <input type="hidden" id="uxSourceFolder" runat="server" class="hdnSourceFolder" />
                        <asp:Button ID="uxTriggerGetContent" runat="server" CssClass="uxTriggerGetContent"
                            Style="display: none;" />
                    </div>
                    <div class="templateGrid">
                        <ektronUI:Message ID="uxContentMessage" runat="server">
                        </ektronUI:Message>
                        <ektronUI:GridView runat="server" ID="EkGrid" AutoGenerateColumns="false" EnableEktronUITheme="true"
                            Width="100%" OnEktronUIPageChanged="uxTemplateListGridView_EktronUIThemePageChanged"
                            OnEktronUISortChanged="uxTemplateListGridView_EktronUIThemeSortChanged">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="10%" ControlStyle-CssClass="overflow-visible"
                                    ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <input type="button" value="Insert" onclick="insertHTML(<%# Eval("ID").ToString() %>)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title" ItemStyle-Width="25%" SortExpression="Title"
                                    ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <%# Eval("Title").ToString() %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sample" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <div id="content-<%# Eval("ID").ToString() %>">
                                            <%# Eval("Html").ToString() %>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </ektronUI:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
