﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Interfaces.Context;

public partial class Workarea_templateinsert : Ektron.Cms.Workarea.Page
{
    protected Ektron.Cms.Content.ContentCriteria criteria = new Ektron.Cms.Content.ContentCriteria();
    protected Ektron.Cms.Framework.Content.ContentManager cmanager = new Ektron.Cms.Framework.Content.ContentManager(Ektron.Cms.Framework.ApiAccessMode.LoggedInUser);
    protected List<Ektron.Cms.ContentData> templatesList;
    protected long folderid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterResources();

        if (Page.IsPostBack)
        {
            // Folder Change Event
            if (!String.IsNullOrEmpty(uxSourceFolder.Value) && uxSourceFolder.Value != uxOldSourceFolder.Value)
            {
                this.uxFolder_Change();
            }
            else
            {
                uxFolderTree.Visible = true;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderid"]))
            {
                long.TryParse(Request.QueryString["folderid"], out folderid);
            }
            uxFolderTree.RootId = folderid.ToString();
            uxSourceFolder.Value = folderid.ToString();
            uxTemplateListGridView_EktronUIThemeFolderChanged(folderid);
        }
    }
    protected void uxFolder_Change()
    {
        long.TryParse(uxSourceFolder.Value, out folderid);
        uxOldSourceFolder.Value = folderid.ToString();
        uxTemplateListGridView_EktronUIThemeFolderChanged(folderid);
    }
    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();

        // create a package that will register the UI JS and CSS we need
        Package searchResultsControlPackage = new Package()
        {
            Components = new List<Component>()
            {
                // Register JS Files
                Packages.EktronCoreJS,
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/empjsfunc.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/ewebeditpro/eweputil.js"),
                Packages.Ektron.StringObject,
                Packages.Ektron.Namespace,

                JavaScript.Create(cmsContextService.WorkareaPath + "/java/plugins/modal/ektron.modal.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/toolbar_roll.js"),
                JavaScript.Create(cmsContextService.WorkareaPath + "/java/workareahelper.js"),

                // Register CSS Files
                //Css.Create(cmsContextService.WorkareaPath + "/csslib/ektron.fixedPositionToolbar.css"),
                Css.Create(cmsContextService.WorkareaPath + "/java/plugins/modal/ektron.modal.css"),
                Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-message.css")
            }
        };
        searchResultsControlPackage.Register(this);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
    }

    protected void uxTemplateListGridView_EktronUIThemeFolderChanged(long folderid)
    {

        this.criteria = this.getContentCriteria();
        this.bindData();
    }

    protected void uxTemplateListGridView_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
    {
        try
        {
            long.TryParse(uxSourceFolder.Value, out folderid);
            this.criteria = this.getContentCriteria();
            this.criteria.OrderByField = (ContentProperty)Enum.Parse(typeof(ContentProperty), e.OrderByFieldText);
            this.criteria.OrderByDirection = e.OrderByDirection;
            this.bindData();
        }
        catch (Exception exc)
        {
            //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
            //Utilities.ShowError(exc.Message);
        }
    }

    protected void uxTemplateListGridView_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
    {
        try
        {
            long.TryParse(uxSourceFolder.Value, out folderid);
            this.criteria = this.getContentCriteria();
            this.criteria.PagingInfo = e.PagingInfo;
            this.criteria.OrderByDirection = EkGrid.EktronUIOrderByDirection;
            this.criteria.OrderByField = (ContentProperty)Enum.Parse(typeof(ContentProperty), EkGrid.EktronUIOrderByFieldText);

            this.bindData();
        }
        catch (Exception exc)
        {
            //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
            //Utilities.ShowError(exc.Message);
        }
    }

    protected void PreSerializeHandler(object sender, EventArgs e)
    {
        if (uxFolderTree != null && uxFolderTree.Items is Ektron.Cms.Framework.UI.Tree.TreeNodeCollection)
        {
            ModifyFolderTree(uxFolderTree.Items);
        }
    }

    ///// <summary>
    ///// Exclude Calendar and Commerce Folders from TREE, And Also Check the FolderSelected when It is in Package Folder list.
    ///// </summary>
    ///// <param name="nodeList"></param>
    private void ModifyFolderTree(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection nodeList)
    {
        for (int index = nodeList.Count - 1; index >= 0; index--)
        {
            var treeNode = (Ektron.Cms.Framework.UI.Controls.EktronUI.FolderTreeNode)nodeList[index];

            if (treeNode.FolderType == 8 || treeNode.FolderType == 9 || treeNode.FolderType == 3 || treeNode.FolderType == 1)
            {
                // Exclude Ecommerce, Blogs, Forums (Discussion Board) and Calendar Folders from Tree                 
                nodeList.RemoveAt(index);
            }
            else
            {
                if (treeNode.Items.Count > 0)
                {
                    ModifyFolderTree(treeNode.Items);
                }
            }
        }
    }

    private void bindData()
    {
        templatesList = cmanager.GetList(this.criteria);
        if (templatesList != null && templatesList.Count > 0)
        {
            uxContentMessage.Visible = false;
            EkGrid.DataSource = templatesList;
            EkGrid.EktronUIPagingInfo = this.criteria.PagingInfo;
            EkGrid.EktronUIOrderByFieldText = this.criteria.OrderByField.ToString();
            EkGrid.DataBind();
        }
        else
        {
            EkGrid.DataSource = null;
            EkGrid.DataBind();
            uxContentMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            uxContentMessage.Text = GetGlobalResourceObject("Ektron_Cms_Framework_UI_Controls_EktronUI", "Aloha_Plugins_Template_noText").ToString();
            uxContentMessage.Visible = true;
        }
    }

    private ContentCriteria getContentCriteria()
    {
        ContentCriteria contentCriteria = new ContentCriteria();
        PagingInfo pagingInfo = new PagingInfo();
        pagingInfo.RecordsPerPage = 5;// siteAPI.RequestInformationRef.PagingSize;
        contentCriteria.PagingInfo = pagingInfo;
        contentCriteria.OrderByField = ContentProperty.Title;
        contentCriteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
        contentCriteria.AddFilter(ContentProperty.FolderId, CriteriaFilterOperator.EqualTo, folderid);
        contentCriteria.AddFilter(ContentProperty.Type, CriteriaFilterOperator.EqualTo, 1);
        contentCriteria.AddFilter(ContentProperty.LanguageId, CriteriaFilterOperator.EqualTo, cmanager.ContentLanguage);
        contentCriteria.AddFilter(ContentProperty.SubType, CriteriaFilterOperator.EqualTo, 0);
        // For HTML content, XmlConfigurationId is 0 
        contentCriteria.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, 0);

        // Some preloaded content has XmlConfigurationId null - currently cannot filter on this 
        //CriteriaFilterGroup<ContentProperty> xmlFilter = new CriteriaFilterGroup<ContentProperty>();
        //xmlFilter.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, 0);
        ////xmlFilter.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, null);
        //xmlFilter.Condition = LogicalOperation.Or;
        //contentCriteria.FilterGroups.Add(xmlFilter);

        return contentCriteria;
    }
}