<%@ WebHandler Language="C#" Class="urlaliasdialogHandler" %>

using System;
using System.Web;
using System.Data;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Site;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Net.NetworkInformation;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;

public class urlaliasdialogHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = "";

        if (context.Request.QueryString["action"] != null)
        {
            action = context.Request.QueryString["action"].ToString();
        }
        //else
        //{
        //    action = context.Request.Form["action"].ToString();
        //}
        switch (action.ToLower())
        {
            case "getaliaslist":
                context.Response.Write(GetAliasList(context));
                break;
            case "checkaliasname":
                context.Response.Write(CheckAliasName(context));
                break;
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private string GetAliasList(HttpContext context)
    {
        string ret = "";
        long contentId = 0;
        int i = 0;

        Int64.TryParse(context.Request.QueryString["contId"], out  contentId);

        PagingInfo req = new PagingInfo();
        CommonApi _refCommonApi = new CommonApi();
        ContentAPI _refContentApi = new ContentAPI();
        AliasSettingsManager settingsManager = new AliasSettingsManager();
        AliasSettings aliasSettings = settingsManager.Get();
        AliasManager aliasManager = new AliasManager();
        List<AliasData> aliases;

        // Get all auto alias for this item
        AliasCriteria aliasCriteria = new AliasCriteria();
        aliasCriteria.AddFilter(AliasProperty.TargetId, CriteriaFilterOperator.EqualTo, contentId);
        CriteriaFilterGroup<AliasProperty> fg = new CriteriaFilterGroup<AliasProperty>();
        fg.Condition = LogicalOperation.Or;
        fg.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Folder);
        fg.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Taxonomy);
        fg.AddFilter(AliasProperty.Type, CriteriaFilterOperator.EqualTo, EkEnumeration.AliasRuleType.Manual);
        aliasCriteria.FilterGroups.Add(fg);
        aliasCriteria.Condition = LogicalOperation.And;
        aliases = aliasManager.GetList(aliasCriteria);

        try
        {
            if (aliasSettings.IsAliasingEnabled && (aliasSettings.IsManualAliasingEnabled || aliasSettings.IsFolderAliasingEnabled || aliasSettings.IsTaxonomyAliasingEnabled))
            {
                // Has Aliases
                if (aliases.Count > 0)
                {
                    for (i = 0; i <= aliases.Count - 1; i++)
                    {
                        string message = "";
                        switch (aliases[i].Type)
                        {
                            case EkEnumeration.AliasRuleType.Manual:
                                message = _refCommonApi.EkMsgRef.GetMessage("lbl tree url manual aliasing");
                                break;
                            case EkEnumeration.AliasRuleType.Folder:
                            case EkEnumeration.AliasRuleType.Taxonomy:
                                message = _refCommonApi.EkMsgRef.GetMessage("lbl tree url automatic aliasing");
                                break;
                        }
                        string isSelected = (aliases[i].IsDefault) ? "selected=\"selected\"" : "";

                        ret += "<input type=\"radio\" name=\"aliasSelect\" id=\"aliasSelect\" value=\"" + _refContentApi.SitePath + aliases[i].Alias + "\" " + isSelected + ">" + _refContentApi.SitePath + aliases[i].Alias + "</input>" + " " + "(" + message + " " + _refCommonApi.EkMsgRef.GetMessage("lbl alias") + ")<br/>";
                    }

                    // Add link management link if enabled
                    if (_refContentApi.RequestInformationRef.LinkManagement)
                    {
                        ret += "<input type=\"radio\" name=\"aliasSelect\" id=\"aliasSelect\" value=\"" + _refContentApi.AppPath + "linkit.aspx?LinkIdentifier=id&ItemID=" + contentId + "\">" + _refContentApi.AppPath + "linkit.aspx?LinkIdentifier=id&ItemID=" + contentId + "</input>";
                        ret += "<linkmanage></linkmanage>";
                    }

                    // Has only 1. 
                    if (aliases.Count == 1)
                    {
                        ret += "<aliascount></aliascount>";
                    }
                }
                else
                {
                    ret = "<error></error>";
                }
            }
            else
            {
                ret = "<error></error>";
            }
        }
        catch (Exception ex)
        {
            ret = "<error>" + ex.Message + "</error>";
        }
        return ret;
    }

    private string CheckAliasName(HttpContext context)
    {
        string ret = "";
        AliasSettingsManager settingsManager = new AliasSettingsManager();
        AliasSettings aliasSettings = settingsManager.Get();
        AliasManager aliasManager = new AliasManager();
        List<AliasData> aliasList;
        string result = "";
        string aliasName = "";
        string fileExtension = string.Empty;
        string langType = string.Empty;
        string id = string.Empty;
        long folderId = 0;

        if (aliasSettings.IsAliasingEnabled)
        {
            aliasName = context.Request.QueryString["aliasname"];
            fileExtension = context.Request.QueryString["fileextension"];
            langType = context.Request.QueryString["langtype"];
            id = context.Request.QueryString["folderid"];
            Int64.TryParse(id, out folderId);

            long siteid = 0;
            if (folderId > 0)
            {
              siteid =  GetSiteId(folderId);
            }

            AliasCriteria aliasCriteria = new AliasCriteria();
            aliasCriteria.AddFilter(AliasProperty.Alias, CriteriaFilterOperator.EqualTo, aliasName + fileExtension);

            aliasCriteria.AddFilter(AliasProperty.LanguageId, CriteriaFilterOperator.EqualTo, Convert.ToInt32(langType));
            if (siteid > 0)
                aliasCriteria.AddFilter(AliasProperty.SiteId, CriteriaFilterOperator.EqualTo, siteid);
            else
                aliasCriteria.AddFilter(AliasProperty.SiteId, CriteriaFilterOperator.EqualTo, 0);
            
            aliasList = aliasManager.GetList(aliasCriteria);

            if (aliasList == null || aliasList.Count == 0)
            {
                ret = "<aliasname><aliasname>";
            }
            else
            {
                ret = result;
            }
        }

        return ret;
    }

    /// <summary>
    /// Get the SiteID from FolderId
    /// </summary>
    /// <param name="objectID"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    private long GetSiteId(long folderId)
    {
        long siteId = 0;

        var fm = ObjectFactory.GetFolder(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
        var folderdata = fm.GetItem(folderId, false);
        if (folderdata != null && folderdata.NameWithPath.ToString().ToLower() != "/")
        {
            FolderCriteria criteria = new FolderCriteria();
            criteria.AddFilter(FolderProperty.FolderPath, CriteriaFilterOperator.EqualTo, folderdata.NameWithPath.Split('/')[0].ToString() + "/");

            var list = fm.GetList(criteria);
            if (list != null && list.Any())
            {
                if (list[0].IsDomainFolder)
                    siteId = list[0].Id;
            }
        }

        return siteId;
    }


}