    <div id="footer">
        <div class="containerWrap">
            <ul>
                <li><a href="#">Products</a></li>
                <li><a href="#">Liquid ring pumps</a></li>
                <li><a href="#">Steam Ejectors</a></li>
                <li><a href="#">Rotary Vane Pumps</a></li>
                <li><a href="#">Rotary Piston Pumps</a></li>
                <li><a href="#">Mechanical Boosters</a></li>
                <li><a href="#">Small Dry Pumps</a></li>
                <li><a href="#">Large Dry Pumps &amp; Systems</a></li>
                <li><a href="#">Turbopumps</a></li>
                <li><a href="#">UHV Pumps</a></li>
                <li><a href="#">Oil Vapour Diffusion Pumps</a></li>
                <li><a href="#">Vacuum Instruments</a></li>
                <li><a href="#">Vacuum Hardware</a></li>
                <li><a href="#">Lubricants and Sealants</a></li>
                <li><a href="#">Exhaust Gas Management</a></li>
                <li><a href="#">Monitoring &amp; Diagnostics</a></li>
                <li><a href="#">Spares &amp; Service</a></li>
            </ul>
            <ul>
                <li><a href="#">Support</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Find an Answer</a></li>
                <li><a href="#">Reference</a></li>
                <li><a href="#">Vacuum Pump Service and Repair</a></li>
            </ul>
            <ul>
                <li><a href="#">Markets Served</a></li>
                <li><a href="#">Semiconductor processing</a></li>
                <li><a href="#">Flat Panel Display</a></li>
                <li><a href="#">Photovoltaic</a></li>
                <li><a href="#">LED</a></li>
                <li><a href="#">Industrial vacuum</a></li>
                <li><a href="#">Process industries</a></li>
                <li><a href="#">R&amp;D</a></li>
                <li><a href="#">Scientific instruments</a></li>
            </ul>
            <ul>
                <li><a href="#">Corporate</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Corporate Responsibility</a></li>
                <li><a href="#">Careers</a></li>
            </ul>
            <ul>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Asia-Pacific</a></li>
                <li><a href="#">Europe, Middle East and Africa</a></li>
                <li><a href="#">Americas</a></li>
            </ul>
            <ul class="social">
                <li><a href="#">Follow us</a></li>
                <li>
                    <a href="//www.facebook.com/pages/Edwards-Vacuum-Pumps-and-Abatement-Equipment/158330262919" class="swapImage" target="_blank"><img src="/library/images/common/facebook-0.png" alt="Facebook" title="Facebook"></a>
                    <a href="//twitter.com/EdwardsVacuum" class="swapImage" target="_blank"><img src="/library/images/common/twitter-0.png" alt="Twitter" title="Twitter"></a>
                    <a href="//www.linkedin.com/company/edwards" class="swapImage" target="_blank"><img src="/library/images/common/linkedin-0.png" alt="LinkedIn" title="LinkedIn"></a>
                    <!-- <a href="#" class="swapImage" target="_blank"><img src="/library/images/common/google-0.png" alt="Google+" title="Google+"></a>
                    <a href="#" class="swapImage" target="_blank"><img src="/library/images/common/rss-0.png" alt="RSS" title="RSS"></a> -->
                    <a href="#" class="swapImage" target="_blank"><img src="/library/images/common/vimeo-0.png" alt="Vimeo" title="Vimeo"></a>
                </li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>