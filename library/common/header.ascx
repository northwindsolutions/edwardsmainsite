<script language="C#" runat="server">
		public string pageTitle = "";	
		public string metaDescription = "";
		public string metaKeywords = "";	
		
</script>

<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="keywords" content="<% if (metaKeywords != "") Response.Write(metaKeywords); %>" />
	<meta name="description" content="<% if (metaDescription != "") Response.Write(metaDescription); %>" />
    <meta property="og:title" content="Edwards"/>
    <meta property="og:type" content="company" />
    <meta property="og:image" content="//edwardsvacuum.com.dev4.silvertech.net/library/images/common/logo.jpg" />
    <meta property="og:site_name" content="Edwards" />
    <meta property="og:description" content="A leading manufacturer of vacuum pumps and abatement technology for nearly 100 years. Product range includes turbo pumps, industrial and process pumps and dry pumps. Edwards also offer Vacuum Pump Service, repairs and maintenance, refurbished pumps and spare parts worldwide. Edwards Vacuum support a broad spectrum of applications - from laboratory to the most demanding semiconductor and industrial processes." />
	<title>Edwards - <% if (pageTitle != "") Response.Write(pageTitle); %></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="/library/styles/stylesheets/screen.css" />
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript">
		!window.jQuery && document.write('<script src="/library/javascript/jquery-1.10.2.min.js"><\/script>');
	</script>
    <script type="text/javascript" src="/library/javascript/jquery.jcarousellite.js"></script>
    <!--<script type="text/javascript" src="/library/javascript/text-resize.min.js"></script>-->
	<script type="text/javascript" src="/library/javascript/jquery-hover.js"></script>
	<script type="text/javascript" src="/library/javascript/twitter-fetcher.js"></script>
	<script type="text/javascript" src="/library/javascript/scripts.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "d8569235-eeb9-47ea-9a3f-ce44c220a1da", onhover: false, doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>

<body>

<div id="wrap">
    <div id="header">
        <div class="containerWrap">
            <div id="headerContent">
                <div id="language">
                    <div class="dropdown">
                        <select>
                            <option>English</option>
                            <option>French</option>
                            <option>German</option>
                            <option>Spanish</option>
                        </select>
                        <div class="btn"></div>
                    </div>
                </div>
                <div id="contact">
                    <a href="#"><span class="btn"></span>Contact us</a>
                </div>
                <div id="search">
                    <div class="btn"><input type="button" value="Go"></div>
                    <input type="text" value="Search" onfocus="clearMe(this)" onblur="restoreMe(this)" class="ip">
                </div>
            </div>
            <!-- <div id="topLinks">
                <ul>
                    <li><span id="sizer"></span></li>
                    <li><a href="#">RSS</a></li>
                </ul>
            </div> -->
        	<div id="logo"><a href="/"><img src="/library/images/common/logo.png" alt="Edwards" title="Edwards"></a></div>
            <div id="topNavigation">
                <ul class="nav">
                    <li class="selected"><a href="#">About us</a></li>
                    <li>
                        <a href="#">Markets</a>
                        <ul class="subnav">
                            <li><a href="#">Semiconductor processing</a></li>
                            <li><a href="#">Flat Panel Display</a></li>
                            <li><a href="#">Photovoltaic</a></li>
                            <li><a href="#">LED</a></li>
                            <li><a href="#">Industrial Vacuum</a></li>
                            <li><a href="#">Process Industries</a></li>
                            <li><a href="#">Research &amp; Development</a></li>
                            <li><a href="#">Scientific Instruments</a></li>
                            <li><a href="#">Service</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Media</a></li>
                    <li><a href="#">Careers</a></li>
                    <li class="webshop"><a href="#">Webshop</a></li>
                </ul>
            </div>
        </div>
    </div>