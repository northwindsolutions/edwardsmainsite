﻿<%@ WebHandler Language="C#" Class="Calendar" %>

using System;
using System.Linq;
using System.Web;
using Edwards.Utility;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public class Calendar : IHttpHandler {
    
    public void ProcessRequest (HttpContext context)
    {
        var req = HttpContext.Current.Request;
        context.Response.ContentType = "application/json";

        if (req.HttpMethod != "POST")
        {
            context.Response.Write(JsonConvert.SerializeObject(JObject.FromObject(new
            {
                error = "Only POST requests are allowed."
            })));
            return;
        }

        var events = req.GetEventsByRequestParamsFilterVariance();
        var datesWithEvents = events.Select(f => f.EventStart.Date).Distinct().Select(f => f.ToString("yyyy-M-d", Constants.CurrentCulture.DateTimeFormat));
        context.Response.Write(JsonConvert.SerializeObject(JObject.FromObject(new
        {
            events = events.Select(f => f.ToJObject()),
            datesWithEvents = datesWithEvents
        })));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}