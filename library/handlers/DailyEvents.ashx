﻿<%@ WebHandler Language="C#" Class="DailyEvents" %>

using System;
using System.Web;
using System.Linq;
using Edwards.Utility;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public class DailyEvents : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        var req = HttpContext.Current.Request;
        var selectDate = context.Request.Params["startDate"];

        context.Response.ContentType = "application/json";

        if (req.HttpMethod != "POST")
        {
            context.Response.Write(JsonConvert.SerializeObject(JObject.FromObject(new
            {
                error = "Only POST requests are allowed."
            })));
            return;
        }

        var events = req.GetEventsByRequestParamsFilterVariance();
        context.Response.Write(JsonConvert.SerializeObject(JObject.FromObject(new
        {
            events = events.Select(f => f.ToJObject()),
            dailyEvents = events.Where(e => e.EventStart.ToString("MM/dd/yyyy") == selectDate).Select(f => f.ToJObject())
        })));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}