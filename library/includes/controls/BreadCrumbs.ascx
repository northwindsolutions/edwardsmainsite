﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BreadCrumbs.ascx.cs" Inherits="library_includes_controls_BreadCrumbs" %>
<%@ Register assembly="Ektron.Cms.Controls" namespace="Ektron.Cms.Controls" tagPrefix="CMS" %>

<div id="breadcrumbs">
    <%--<CMS:FolderBreadcrumb runat="server" ID="breadCrumbsCtrl" separator=" | " DynamicParameter="pageid" /> | <CMS:BreadCrumb runat="server" DynamicParameter="pageid"/>--%>
    
    <asp:Repeater runat="server" ID="paths">
        <ItemTemplate>
            <asp:HyperLink runat="server" ID="pathLink"></asp:HyperLink> | 
        </ItemTemplate>
    </asp:Repeater>
    <%--<asp:Literal runat="server" ID="sep"></asp:Literal>--%>
    <asp:Literal runat="server" ID="currentItem"></asp:Literal>

    <%--<a href="#">Markets</a> | <strong>Knowledge and document centre</strong>--%>
</div>

