﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Utility;

public partial class library_includes_controls_BreadCrumbs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        long pageId;
        if (long.TryParse(Request.QueryString["pageid"], out pageId))
        {
            var content = ContentUtility.GetItemByContentId(pageId);
            if (content != null)
            {
                Ektron.Cms.API.Folder f = new Ektron.Cms.API.Folder();
                var folder = f.GetFolder(content.FolderId, false, false);
                if (folder != null)
                {
                    Ektron.Cms.Common.SitemapPath[] path = folder.BreadCrumbPath;
                    paths.DataSource = path;
                    paths.ItemDataBound += (o, args) =>
                    {
                        if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                            return;

                        var item = args.Item.DataItem as Ektron.Cms.Common.SitemapPath;
                        if (item == null)
                            return;

                        var pathLink = args.Item.FindControl("pathLink") as HyperLink;
                        if (pathLink == null)
                            return;

                        pathLink.NavigateUrl = item.Url.StartsWith("/") || item.Url.StartsWith("http://") || item.Url.StartsWith("https://") 
                            ? item.Url
                            : string.Format("/{0}", item.Url);
                        pathLink.Text = item.Title;
                    };
                    paths.DataBind();
                }

                currentItem.Text = content.Title;
            }
        }

    }
}