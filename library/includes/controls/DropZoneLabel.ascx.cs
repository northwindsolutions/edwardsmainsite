﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.PageBuilder;

public partial class library_includes_controls_DropZoneLabel : System.Web.UI.UserControl
{
    public string Label { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        var pb = Page as PageBuilder;
        Visible = pb != null && pb.Status == Mode.Editing;
    }
}