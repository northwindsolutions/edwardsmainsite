﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventsCallout.ascx.cs" Inherits="library_includes_controls_EventsCallout" %>
<%@ Import Namespace="Edwards.Utility" %>
<%@ Import Namespace="Ektron.Cms.Common.Calendar" %>
<div class="callout">
    <h3><asp:Literal runat="server" meta:resourcekey="SeeEvents"></asp:Literal></h3>
    <div class="block">
        <div class="datepicker"></div>
            
        <div id="eventsCalloutController" ng-controller="EventsCallout">
            <div ng-repeat="e in events" class="callout-event">
                <strong ng-bind="e.date"></strong><br>
                <strong ng-bind-html="trustIt(e.title)"></strong><br><br>
                <span ng-show="{{ e.bookNowLink }}"><a ng-href="{{ e.bookNowLink }}" target="_blank"><strong><%= GetLocalResourceObject("VisitWebsite.Text") %></strong></a> |</span> 
                <a ng-href="{{ e.downloadCalFileLink }}"><strong><%= GetLocalResourceObject("DownloadCalendarFile.Text") %></strong></a>
            </div>
        </div>
    </div>
</div>
<script>
    if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
    EdwardsApp.controller("EventsCallout",
        function ($scope, $sce) {
            $scope.trustIt = function(htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            }
            $scope.loading = false;

            if (<%= JSONTodaysEvents %> != null) {
                $scope.events = <%= JSONTodaysEvents %>;
            } else {
                $scope.events = <%= JSONEvents %>;
            }

            $scope.datesWithEvents = <%= JSONDates %>;
        }
    );

    $ektron(function ($) {
        var $listController = $("#ngEventsListController");
        var $controller = $("#eventsCalloutController");

        function ngScope() {
            return angular.element($controller).scope();
        }
        function ngListScope() {
            return angular.element($listController).scope();
        }

        Date.prototype.eventsListFormat = function () {
            return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate();
        };
        Date.prototype.postFormat = function () {
            return (this.getMonth() + 1) + "/" + this.getDate() + "/" + this.getFullYear();
        };

        var $datepicker = $(".datepicker");
        $datepicker.datepicker({
            beforeShowDay: function (date) {
                var now = new Date();
                var datesWithEvents = ngScope().datesWithEvents;
                var f = date.eventsListFormat();
                // If the date is in the list of events with dates, display it.
                if (datesWithEvents.indexOf(f) > -1) {
                    return [true, 'event-date-highlight', ''];
                } 
                // If it's today's date, activate it with the normal styles.
                else if (now.getFullYear() === date.getFullYear() && 
                    now.getMonth() === date.getMonth() && 
                    now.getDate() === date.getDate()) {
                    return [true, '', ''];
                }
                return [false];
            },

            onSelect: function(date) {
                var $scope = ngScope();
                $scope.$apply(function($s) {
                    $s.loading = true;
                });

                var d = new Date(date);
                var month = d.getMonth() + 1;
                var year = d.getFullYear();

                var formData = {
                    startDate: date,
                    month: month + "/1/" + year,
                };

                $.post("/library/handlers/DailyEvents.ashx",
                    formData,
                    function(data, textStatus, jqXHR) {
                        $scope.$apply(function($s) {
                            $s.loading = false;
                            $s.events = data.dailyEvents;
                            $datepicker.datepicker("refresh");
                        });
                        ngListScope().$apply(function($s) {
                            $s.showServerResults = false;
                            $s.events = data.events;
                            $s.loading = false;
                        });
                    },
                    "json"
                );
            },

            onChangeMonthYear: function(year, month, widget) {
                var $scope = ngScope();
                $scope.$apply(function($s) {
                    $s.loading = true;
                });

                var formData = {
                    month: month + "/1/" + year
                    
                };

                $.post("/library/handlers/Calendar.ashx",
                    formData,
                    function(data, textStatus, jqXHR) {
                        $scope.$apply(function($s) {
                            $s.loading = false;
                            $s.events = null;
                            $s.datesWithEvents = data.datesWithEvents;
                            $datepicker.datepicker("refresh");
                        });
                        ngListScope().$apply(function($s) {
                            $s.showServerResults = false;
                            $s.events = data.events;
                            $s.loading = false;
                        });
                    },
                    "json"
                );
            }
        });
        $datepicker.datepicker("setDate", new Date(<%= Date.Year %>, <%= Date.Month - 1 %>, <%= Date.Day %>));
    });
</script>