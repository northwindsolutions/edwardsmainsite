﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Utility;
using Ektron.Cms.API;
using Ektron.Cms.Common.Calendar;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public partial class library_includes_controls_EventsCallout : System.Web.UI.UserControl
{
    public IEnumerable<WebEventData> WebEvents { get; set; }
    public IEnumerable<WebEventData> TodayEvents { get; set; }

    public string JSONEvents
    {
        get
        {
            if (WebEvents == null)
                return null;

            return JsonConvert.SerializeObject(WebEvents.Select(f => f.ToJObject()));
        }
    }

    public string JSONDates
    {
        get
        {
            if (WebEvents == null)
                return null;

            return JsonConvert.SerializeObject(WebEvents.Select(f => f.EventStart.ToString("yyyy-M-d")));
        }
    }

    public string JSONTodaysEvents
    {
        get
        {
            if (WebEvents == null)
                return null;

            return JsonConvert.SerializeObject(WebEvents.Where(e => e.EventStart.ToString("yyyy-M-d") == DateTime.UtcNow.Date.ToString("yyyy-M-d")).Select(f => f.ToJObject()));
        }
    }

    public DateTime Date { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime _month;
        DateTime _startDate;
        if (DateTime.TryParse(Request.Params["month"], out _month))
        {
            Date =  _month;
        }
        else if (DateTime.TryParse(Request.Params["start"], out _startDate))
        {
            Date = _startDate;
        }
        else
        {
            Date = DateTime.UtcNow.Date;
        }

        WebEvents = Request.GetEventsByRequestParamsFilterVariance();
    }
}

