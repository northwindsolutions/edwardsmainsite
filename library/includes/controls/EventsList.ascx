﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventsList.ascx.cs" Inherits="library_includes_controls_EventsList" %>

<asp:PlaceHolder runat="server" ID="featuredEventPlaceholder">
    <div id="featuredEvent">
        <h1><asp:Literal runat="server" meta:resourcekey="NextEvent"></asp:Literal></h1>
        <div class="block">
            <strong><%= FeaturedEventDate %></strong><br>
            <strong><%= FeaturedEventTitle %></strong><br/>
            <%= FeaturedEventSummary %><br />
            <span id="eventSpan" runat="server"><asp:HyperLink runat="server" ID="featuredBookNowLink" Target="_blank"><strong><asp:Literal runat="server" meta:resourcekey="VisitWebsite"></asp:Literal></strong></asp:HyperLink> |</span> 
            <asp:HyperLink runat="server" ID="featuredDownloadCalFileLink"><strong><asp:Literal ID="Literal2" runat="server" meta:resourcekey="DownloadCalendarFile"></asp:Literal></strong></asp:HyperLink>
        </div>
    </div>
</asp:PlaceHolder>
<hr>
<div>
    <div id="ngEventsListController" ng-controller="EventsList">
        <div class="one-half form-item">
            <div class="btn">
                <input type="button" value="<%= GoText %>" />
            </div>
            <input type="text" value="<%= SearchForAnEventText %>" class="ip" onfocus="clearMe(this)" onblur="restoreMe(this)" />
            <p ng-show="loading"><img src="/Workarea/images/application/loading_small.gif" alt="<%= GetGlobalResourceObject("EdwardsGlobal", "Loading") %>"/></p>
        </div>
        <br class="clear" /><br />
        <div id="events">
            <p ng-show="!showServerResults && (!events || events.length == 0)"><asp:Literal runat="server" meta:resourcekey="NoItemsFound"></asp:Literal></p>
            <p ng-show="!showServerResults && (!events || events.length == 0)">
                <strong><a ng-click="reset()"><%= GetGlobalResourceObject("EdwardsGlobal", "Reset") %></a></strong>
            </p>

            <div itemscope itemtype="http://schema.org/Event" ng-repeat="item in events" class="item {{ item.iconClass }}">
                <strong itemprop="startDate" ng-bind="item.date"></strong><br>
                <strong itemprop="name" ng-bind="item.title"></strong><br>
                <span ng-bind-html="trustIt(item.summary)"></span><br>
                <span ng-show="{{ item.bookNowLink }}"><a ng-href="{{ item.bookNowLink }}" target="_blank"><strong><asp:Literal runat="server" meta:resourcekey="VisitWebsite"></asp:Literal></strong></a> | 
                <a ng-href="{{ item.downloadCalFileLink }}"><strong><asp:Literal runat="server" meta:resourcekey="DownloadCalendarFile"></asp:Literal></strong></a>
            </div>

            <div class="events-server" ng-show="showServerResults">
                <asp:Repeater runat="server" ID="eventsList">
                    <ItemTemplate>
                        <div runat="server" id="eventWrapper">
                            <strong itemprop="startDate"><asp:Literal runat="server" ID="date"></asp:Literal></strong><br>
                            <strong itemprop="name"><asp:Literal runat="server" ID="title"></asp:Literal></strong><br>
                            <asp:Literal runat="server" ID="summary"></asp:Literal><br>
                            <span id="eventResultSpan" runat="server"><asp:HyperLink runat="server" ID="bookNowLink" Target="_blank"><strong><asp:Literal ID="Literal3" runat="server" meta:resourcekey="VisitWebsite"></asp:Literal></strong></asp:HyperLink> |</span>
                            <asp:HyperLink runat="server" ID="downloadCalFileLink"><strong><asp:Literal ID="Literal2" runat="server" meta:resourcekey="DownloadCalendarFile"></asp:Literal></strong></asp:HyperLink>
                        </div>  
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
<script>
    var ngEventsListController = document.getElementById('ngEventsListController');
    function ngScope() {
        return angular.element(ngEventsListController).scope();
    }

    var $textbox = null;
    var $button = null;

    if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
    EdwardsApp.controller("EventsList",
        function EventsList($scope, $sce) {
            $scope.trustIt = function (htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            }
            $scope.loading = false;
            $scope.showServerResults = true;
            $scope.events = null;

            $scope.reset = function () {
                $scope.showServerResults = true;
                $scope.events = null;
                if ($textbox) {
                    $textbox.val("");
                    $textbox.blur();
                }
            };
        }
    );

    jQuery(function ($) {

        $textbox = $("#ngEventsListController input[type=text]");
        $button = $("#ngEventsListController .btn input");

        function search(e) {
            var formData = {
                q: $textbox.val()
            };
            var $scope = ngScope();
            $scope.$apply(function ($s) {
                $s.loading = true;
            });
            $.post("/library/handlers/Calendar.ashx",
                formData,
                function (data, textStatus, jqXHR) {
                    $scope.$apply(function ($s) {
                        $s.showServerResults = false;
                        $s.events = data.events;
                        $s.loading = false;
                    });
                },
                "json"
            );
            e.stopPropagation();
            e.preventDefault();
        }

        $button.on('click', search);
        $textbox.on('keypress', function(e) {
            if (e.keyCode == 13) {
                var val = $(this).val();
                val = val.replace(/\s+/gi, '');
                val !== '' && search(e);
                e.preventDefault();
            }
        });
    });
</script>