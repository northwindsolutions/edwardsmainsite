﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Edwards.Utility;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Common.Calendar;
using Ektron.Cms.Framework.Calendar;

public partial class library_includes_controls_EventsList : System.Web.UI.UserControl
{
    public string FeaturedEventTitle { get; set; }
    public string FeaturedEventSummary { get; set; }
    public string FeaturedEventDate { get; set; }

    protected string SearchForAnEventText
    {
        get { return GetLocalResourceObject("SearchForAnEvent.Text") as string; }
    }

    protected string GoText
    {
        get { return GetLocalResourceObject("Go.Text") as string; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GetNextEvent();

        ICollection<WebEventData> events = Request.GetEventsByRequestParamsFilterVariance();
        PopulateData(events);
    }

    /// <summary>
    /// Get the next upcoming event in the calendar based off of today.
    /// </summary>
    protected void GetNextEvent()
    {
        var now = DateTime.UtcNow.Date;
        var events = CalendarUtility.GetEventsFilterVariance(startDate: now);
        if (events == null || !events.Any())
        {
            featuredEventPlaceholder.Visible = false;
            return;
        }

        var eevent = events.OrderBy(f => f.EventStart).FirstOrDefault();
        FeaturedEventDate = eevent.FormatWebEventStartEnd();
        FeaturedEventTitle = eevent.DisplayTitle;
        FeaturedEventSummary = eevent.Description;
        featuredBookNowLink.NavigateUrl = eevent.BookNowLink();
        featuredDownloadCalFileLink.NavigateUrl = eevent.ICalUrl();

        eventSpan.Visible = !string.IsNullOrEmpty(eevent.BookNowLink());
    }

    protected void PopulateData(ICollection<WebEventData> events)
    {
        if (events == null || !events.Any())
            return;

        // bind the events
        eventsList.DataSource = events;
        eventsList.ItemDataBound += (o, args) =>
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                return;

            var item = args.Item.DataItem as WebEventData;
            if (item == null)
                return;

            var eventWrapper = args.Item.FindControl("eventWrapper") as HtmlGenericControl;
            var eventSpan = args.Item.FindControl("eventResultSpan") as HtmlGenericControl;
            var title = args.Item.FindControl("title") as Literal;
            var date = args.Item.FindControl("date") as Literal;
            var summary = args.Item.FindControl("summary") as Literal;
            var bookNowLink = args.Item.FindControl("bookNowLink") as HyperLink;
            var downloadCalFileLink = args.Item.FindControl("downloadCalFileLink") as HyperLink;
            if (eventWrapper == null || title == null || date == null || summary == null || bookNowLink == null || downloadCalFileLink == null)
                return;

            eventWrapper.Attributes.Add("class", string.Format("item {0}", item.IconClass()));

            title.Text = item.DisplayTitle;
            date.Text = item.FormatWebEventStartEnd();
            summary.Text = item.Description;
            bookNowLink.NavigateUrl = item.BookNowLink();
            downloadCalFileLink.NavigateUrl = item.ICalUrl();

            eventSpan.Visible = !string.IsNullOrEmpty(item.BookNowLink());
        };
        eventsList.DataBind();
    }

}