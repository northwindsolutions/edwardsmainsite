﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ektron.Cms;
using Edwards.Utility;
using Edwards.Models;

public partial class library_includes_controls_GeneralAnnouncement : System.Web.UI.UserControl
{
    public long ContentId { get; set; }    
    protected void Page_Load(object sender, EventArgs e)
    {
       ContentData contentData = ContentUtility.GetItemByContentId(ContentId);
       if (contentData != null)
       {
            title.Text = contentData.Title;
			uxContent.Text = contentData.Html;
       }
    }
}