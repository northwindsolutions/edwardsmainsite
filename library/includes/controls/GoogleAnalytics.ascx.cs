﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Utility;

public partial class library_includes_controls_GoogleAnalytics : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // If the site isn't running in production mode, don't show the tracking code.
        Visible = Constants.DeploymentMode == Constants.DeploymentModes.Prod;
    }
}