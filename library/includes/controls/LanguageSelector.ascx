﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LanguageSelector.ascx.cs" Inherits="library_includes_controls_LanguageSelector" %>
<div id="language">
    <a href="#" class="toggle"></a>
    <div class="dropdown">
        <ul>
            <li><asp:HyperLink runat="server" ID="currentLanguageLink"></asp:HyperLink></li>
            <asp:Repeater runat="server" ID="languageRepeater">
                <ItemTemplate>
                    <li><asp:HyperLink runat="server" ID="link"/></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</div>