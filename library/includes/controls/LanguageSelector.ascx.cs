﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Utility;
using Ektron.Cms;
using Ektron.Cms.Controls;

public partial class library_includes_controls_LanguageSelector : System.Web.UI.UserControl
{
    protected class LanguageItem
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IEnumerable<LanguageData> viewLangs = null;
        IEnumerable<LanguageItem> resultItems = null;
        var allLanguages = Backend.GetAllActiveLanguages();
        int currentLanguageId = Constants.CurrentLanguageId;

        // To properly handle this, if this is a content block, load up the other language versions of the content block and show the aliases for those in the drop down.
        long _id;
        long _pageid;
        var api = new Ektron.Cms.ContentAPI();
        if (long.TryParse(Request.QueryString["id"], out _id))
        {
            var langs = api.DisplayAddViewLanguage(_id);
            if (langs != null)
            {
                viewLangs = langs.Where(f => f.Type == "VIEW").ToList();
                resultItems = viewLangs
                    .Where(f => f.Id != currentLanguageId)
                    .Select(f =>
                    {
                        var cb = Backend.GetContentData(_id, f.Id);
                        return new LanguageItem
                        {
                            Text = (f.XmlLang == "en-US") ? "EN" : (f.XmlLang == "es-MX") ? "ES" : (f.XmlLang == "pt-BR") ? "PT" : f.XmlLang.Split('-')[1].ToUpper(),
                            Url = cb.Quicklink
                        };
                    }).ToList();
            }
        }
        // Same thing if this is a page builder page.
        else if (long.TryParse(Request.QueryString["pageid"], out _pageid))
        {
            var langs = api.DisplayAddViewLanguage(_pageid);
            if (langs != null)
            {
                viewLangs = langs.Where(f => f.Type == "VIEW").ToList();
                resultItems = viewLangs
                    .Where(f => f.Id != currentLanguageId)
                    .Select(f =>
                    {
                        var cb = Backend.GetContentData(_pageid, f.Id);
                        return new LanguageItem
                        {
                            Id = f.Id,
                            Text = (f.XmlLang == "en-US") ? "EN" : (f.XmlLang == "es-MX") ? "ES" : (f.XmlLang == "pt-BR") ? "PT" : f.XmlLang.Split('-')[1].ToUpper(),
                            Url = cb.Quicklink
                        };
                    })
                .ToList();
            }
        }
        else
        {
            resultItems = allLanguages
                .Where(f => f.Id != currentLanguageId)
                .Select(f => new LanguageItem { Text = f.BrowserCode.ToUpper(), Url = AppendLangtype(f.Id) });
        }

        var currentLanguage = allLanguages.FirstOrDefault(f => f.Id == currentLanguageId);
        if (currentLanguage == null)
            return;

        currentLanguageLink.Text = (currentLanguage.XmlLang == "en-US") ? "EN" : (currentLanguage.XmlLang == "es-MX") ? "ES" : (currentLanguage.XmlLang == "pt-BR") ? "PT" : currentLanguage.XmlLang.Split('-')[1].ToUpper();

        languageRepeater.DataSource = resultItems.OrderBy(li => li.Text);
        languageRepeater.ItemDataBound += (o, args) =>
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
                return;

            var langItem = args.Item.DataItem as LanguageItem;
            if (langItem == null)
                return;

            var link = args.Item.FindControl("link") as HyperLink;
            if (link == null)
                return;

            if (langItem.Id == 2057 || langItem.Id == 1033)
            {
                link.NavigateUrl = ConfigurationManager.AppSettings["siteURlNew"].ToString();
            }
            else
            {
                link.NavigateUrl = langItem.Url.Contains("?") ? string.Format("{0}&langtype={1}", langItem.Url, langItem.Id)
                                                          : string.Format("{0}?langtype={1}", langItem.Url, langItem.Id);
            }
            link.Text = langItem.Text;
        };
        languageRepeater.DataBind();

    }

    private Regex langTypePattern = new Regex(@"(?:\?|&)langtype=(?<langId>\d{4})", RegexOptions.IgnoreCase);
    private Regex ampersandPattern = new Regex(@"&", RegexOptions.IgnoreCase);

    private string AppendLangtype(int languageId)
    {
        if (languageId == 2057 || languageId == 1033)
        {
            return ConfigurationManager.AppSettings["siteURlNew"].ToString();
        }
        else
        {
            var url = Request.RawUrl;
            url = langTypePattern.Replace(url, string.Empty);

            // Make sure that once we replace the langtype parameter we don't force a &xxx=xxx parameter to be at the beginning of the querystring
            // since that would be a malformed URL.
            if (!url.Contains("?") && url.Contains("&"))
            {
                url = ampersandPattern.Replace(url, "?", 1, 0);
            }
            return url.Contains("?")
                    ? string.Format("{0}&langtype={1}", url, languageId)
                    : string.Format("{0}?langtype={1}", url, languageId);
        }
    }
}