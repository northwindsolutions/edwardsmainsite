﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftNavigation.ascx.cs" Inherits="library_includes_controls_LeftNavigation" %>

<ektron:MenuModelSource ID="uxMenuModelSource" runat="server">
</ektron:MenuModelSource>

<ektron:MenuView ID="menuView" runat="server" ModelSourceID="uxMenuModelSource" >
    <ListTemplate>
        <ul>
            <asp:PlaceHolder ID="listPlaceholder" runat="server" />
        </ul>
    </ListTemplate>
    <ItemTemplate>
        <li class="<%# ((Eval("ItemId").ToString() == selectedId) ? "selected " : string.Empty) + (Eval("Type").ToString() == "SubMenu" ? "subnav " : string.Empty) %>">
            <asp:HyperLink ID="nodeLink" runat="server" Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>' Target='<%# Eval("Target") %>' />
            <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        </li>
    </ItemTemplate>
</ektron:MenuView>