﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Organization;
using Ektron.Cms.Framework.Organization;
using Edwards.Utility;

public partial class library_includes_controls_LeftNavigation : System.Web.UI.UserControl
{
    public static string selectedId { get; set; }
    public static string leftNavParentId { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        long mainMenuId = Constants.MenuIds.MainHeaderMenu;
        long pageId = 0;

        if (long.TryParse(Request.QueryString["pageId"], out pageId))
        {
            //Response.Write("Page ID: " + pageId + "<br>");
            MenuManager menuManager = new MenuManager();
            IMenuData mainBaseData = menuManager.GetTree(mainMenuId);
            //Response.Write("Main Menu ID: " + mainMenuId + "<br>");

            if (mainBaseData != null && mainBaseData.Items != null && mainBaseData.Items.Any())
            {
                foreach (var menu in mainBaseData.Items)
                {
                    if (menu.Items != null && menu.Items.Any())
                    {
                        foreach (var item in menu.Items)
                        {
                            //Response.Write(string.Format("Menu Item ID: {0} - {1}<br>", item.ItemId, item.Text));
                            if (IsMenuItemCurrentPage(item, pageId))
                            {
                                uxMenuModelSource.TreeFilter.Id = item.ParentId;
                                uxMenuModelSource.TreeFilter.Depth = 0;

                                selectedId = item.ItemId.ToString();
                                return;
                            }

                            if (item.Items != null && item.Items.Any())
                            {
                                foreach (var subItem in item.Items)
                                {
                                    //Response.Write(string.Format("Sub Menu Item ID: {0} - {1}<br>", subItem.ItemId, subItem.Text));
                                    if (IsMenuItemCurrentPage(subItem, pageId))
                                    {
                                        uxMenuModelSource.TreeFilter.Id = subItem.ParentId;
                                        uxMenuModelSource.TreeFilter.Depth = 0;

                                        selectedId = subItem.ItemId.ToString();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Determine if the current menu item's parent should be the root of the menu.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="pageId"></param>
    /// <returns></returns>
    /// <remarks>
    /// Check if the item id of the menu item matches the current page builder ID.  
    /// Otherwise, if the raw URL is the same as the item's href attribute then that probably means that
    /// it is a sub menu item and should be the root.
    /// </remarks>
    protected bool IsMenuItemCurrentPage(IMenuItemData item, long pageId)
    {
        if (item.Href == "") {
            return false;
        } else {
            return (item.ItemId == pageId) || Request.RawUrl.Contains(item.Href);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}