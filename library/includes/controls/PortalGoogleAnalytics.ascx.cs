﻿using Edwards.Utility;
using System;

/// <summary>
/// This control is intended to conditionally render the Google Analytics
/// tracking code necessary to produce metrics for the Edwards partner portal.
/// </summary>
public partial class library_includes_controls_PortalGoogleAnalytics : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Render the tracking code if the site is in production mode.

        this.Visible = Constants.DeploymentMode == Constants.DeploymentModes.Prod;
    }
}