﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortalMainMenu.ascx.cs" Inherits="library_includes_controls_PortalMainMenu" %>
<%@ Import Namespace="Ektron.Cms.Common" %>
<asp:ListView ID="menuListView" runat="server" ItemPlaceholderID="aspItemPlaceholder" ItemType="Ektron.Cms.Organization.IMenuItemData">
    <LayoutTemplate>
        <div id="nav">
            <ul class="nav">
                <asp:PlaceHolder ID="aspItemPlaceholder" runat="server" />
            </ul>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <li class="<%# (IsWebshopMenuItem(Item) ? "webshop webshop-right " : string.Empty) + ((Item.Type == EkEnumeration.MenuItemType.SubMenu) ? "subnav" : string.Empty) %>">
            <a href="<%# Item.Href %>" target="<%# IsWebshopMenuItem(Item) ? "_blank" : Item.Target %>"><span class="navIcon"></span><%# Item.Text %></a>
            <asp:ListView runat="server" ID="uxSubMenu" ItemPlaceholderID="subMenuPlaceholder" DataSource='<%# Item.Items %>' ItemType="Ektron.Cms.Organization.IMenuItemData">
                <LayoutTemplate>
                    <ul class="subnav">
                        <asp:PlaceHolder runat="server" ID="subMenuPlaceholder" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li class="<%# (Item.Type == EkEnumeration.MenuItemType.SubMenu ? "subnav " : string.Empty) %>">
                        <asp:HyperLink ID="childLink" runat="server" NavigateUrl='<%# Item.Href %>' Target='<%# Item.Target %>' CssClass='<%# (string.IsNullOrEmpty(Item.Href)) ? "empty" : string.Empty %>'>
                            <%# (Item.Type == EkEnumeration.MenuItemType.SubMenu) ? "<span class=\"navIcon\"></span>" : string.Empty %>
                            <%# Item.Text %>
                        </asp:HyperLink>

                        <asp:ListView runat="server" ID="uxSubMenu2" ItemPlaceholderID="subMenu2Placeholder" DataSource='<%# Item.Items %>' ItemType="Ektron.Cms.Organization.IMenuItemData">
                            <LayoutTemplate>
                                <ul class="subnav">
                                    <asp:PlaceHolder runat="server" ID="subMenu2Placeholder"></asp:PlaceHolder>
                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="sub2ChildLink" runat="server" Text='<%# Item.Text %>' NavigateUrl='<%# Item.Href %>' Target='<%# Item.Target %>'></asp:HyperLink>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </li>
    </ItemTemplate>
</asp:ListView>