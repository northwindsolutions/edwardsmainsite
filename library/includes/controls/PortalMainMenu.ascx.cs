﻿using Edwards.Portal;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Cms.Organization;
using Ektron.Cms;
using System.Collections.Generic;
using System;

public partial class library_includes_controls_PortalMainMenu : System.Web.UI.UserControl
{
    private readonly AuthenticationService _authenticationService;
	public PortalUser _currentUser;

    public library_includes_controls_PortalMainMenu()
    {
        this._authenticationService = new AuthenticationService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IMenuData menuBaseData = this.LoadMenu(Constants.MenuIds.AnonymousPortalHeaderMenu);

        if (menuBaseData != null)
        {
            menuListView.DataSource = menuBaseData.Items;
            menuListView.DataBind();
        }
    }

    private IMenuData LoadMenu(long id)
    {
        
        var currentUser = this._authenticationService.GetCurrentUser();

        IPortalMenuFactory menuFactory;
        
        if (this._authenticationService.IsAuthenticated(currentUser))
        {
            menuFactory = new AuthenticatedPortalMenuFactory();
        }
        else
        {
            menuFactory = new AnonymousPortalMenuFactory();
        }

        return menuFactory.Create();
    }

    protected bool IsWebshopMenuItem(IMenuItemData item)
    {
        return item.Text == "Webshop" || item.Text == "Boutique" || 
            item.Text == "网站" || item.Text == "Loja" || 
            item.Text == "ショップ" || item.Text == "온라인몰" || 
            item.Text == "Tienda" || item.Text == "商店";
    }
}