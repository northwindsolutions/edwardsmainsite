﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Text;
using Ektron.Cms;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Organization;
using Ektron.Cms.Framework.Organization;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Content;
using Ektron.Cms.Common;
using Edwards.Utility;
using Edwards.Portal.Repositories;
using Edwards.Portal;
using Edwards.Portal.Services;


public partial class library_includes_controls_SidebarMenu : System.Web.UI.UserControl
{
    public static string selectedId { get; set; }
    public string strMarket = "";
    public string strProduct = "";
    public string partnerType = "";
    public string PageFolderPath = "";
    public PortalUser _currentUser;
    AuthenticationService _authenticationservices = new AuthenticationService();
    FeaturedPageRepository _featuredPageRepository = new FeaturedPageRepository();
    List<ContentData> lstContentData = new List<ContentData>();
    StringBuilder sbMenuItems = new StringBuilder();

    protected void Page_Init(object sender, EventArgs e)
    {
        long mainMenuId = Constants.MenuIds.PortalSidebarMenu;
        long pageId = 0;
        strMarket = this.GetGlobalResourceObject("EdwardsPortal", "MarketInformation").ToString();
        strProduct = this.GetGlobalResourceObject("EdwardsPortal", "ProductInformation").ToString();
        this._currentUser = _authenticationservices.GetCurrentUser();
        PageFolderPath = PortalPaths.GetPagesFolder(this._currentUser.PartnerType);

        if (long.TryParse(Request.QueryString["pageId"], out pageId))
        {
            string marketInformation = GetFeaturedMenuItems(strMarket);
            string productInformation = GetFeaturedMenuItems(strProduct);
            string url = Request.RawUrl;
            lstContentData = GetMenuItems();

            sbMenuItems.Append("<ul>");
            sbMenuItems.Append(marketInformation != "" ? "<li class=\"" + GetSubNav(marketInformation) + "\"><a href=\"#\">" + strMarket + "</a>" : "");
            if (marketInformation != "")
            {
                sbMenuItems.Append(marketInformation);
                sbMenuItems.Append("</li>");
            }

            sbMenuItems.Append(productInformation != "" ? "<li class=\"" + GetSubNav(productInformation) + "\"><a href=\"#\">" + strProduct + "</a>" : "");
            if (productInformation != "")
            {
                sbMenuItems.Append(productInformation);
                sbMenuItems.Append("</li>");
            }
            
            if (lstContentData != null)
            {
                foreach (ContentData cd in lstContentData)
                {
                    sbMenuItems.Append("<li class=\"" + (url == cd.Quicklink ? "selected" : "") + "\"><a href=\"" + cd.Quicklink + "\">" + cd.Title + "</a></li>");
                }
            }

            sbMenuItems.AppendFormat("<li><a href=\"/Events\">{0}</a></li>", GetGlobalResourceObject("EdwardsPortal", "Events").ToString());
            sbMenuItems.Append("</ul>");
            uxSidebarMenu.Text = sbMenuItems.ToString();
        }
    }

    protected string GetFeaturedMenuItems(string featuredInformation)
    {        
        string _currentuserFeaturedItems="";
        if (featuredInformation == strMarket)
        {
            IEnumerable<FeaturedPage> markets = _featuredPageRepository.GetMarketPages(this._currentUser, Constants.CurrentLanguageId);
            _currentuserFeaturedItems = BuildFeaturedMenuItems(markets);
        }
        if (featuredInformation == strProduct)
        {
            IEnumerable<FeaturedPage> products = _featuredPageRepository.GetProductPages(this._currentUser, Constants.CurrentLanguageId);
            _currentuserFeaturedItems = BuildFeaturedMenuItems(products);
        }
        return _currentuserFeaturedItems;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
         
    }

    protected string BuildFeaturedMenuItems(IEnumerable<FeaturedPage> featured)
    {
        int flag = 0;
        StringBuilder sb = new StringBuilder();
        
        if (featured != null)
        {
            if (featured.Count() > 0)
            {
                
                foreach (FeaturedPage fp in featured)
                {
                    if (fp.IsFavorite)
                    {
                        if (flag == 0)
                        {
                            sb.Append("<ul>");
                            flag = 1;
                        }
                        sb.AppendFormat("<li><a href=\"{0}\" title={1}>{1}</a></li>", fp.Url, fp.Title);
                    }
                }
                if (flag == 1)
                {
                    sb.Append("</ul>");
                }
            }
        }
        return sb.ToString();
    }

    protected string GetSubNav(string featured)
    {
        string subNav = "";
        if (featured != "")
        {
            subNav = !String.IsNullOrEmpty(featured) ? "subnav " : string.Empty;
        }        
        return subNav;
    }

    protected List<ContentData> GetMenuItems()
    {
        partnerType = this._currentUser.PartnerType;
        List<ContentData> listcontentdata = new List<ContentData>();

        listcontentdata = ContentUtility.GetMenuUrl(partnerType);

        string items = this.GetGlobalResourceObject("EdwardsPortal", "ExcludeSideBarMenuItems").ToString();

        if (items.Contains(","))
        {
            string[] excludeMenuItems = items.Split(',');
            foreach (string s in excludeMenuItems)
            {
                ContentData contentData = new ContentData();
                contentData = listcontentdata.Find(c => c.Title == s);
                listcontentdata.Remove(contentData);
            }
        }
        else
        {
            ContentData contentData = new ContentData();
            contentData = listcontentdata.Find(c => c.Title.ToLower() == items.ToLower());
            listcontentdata.Remove(contentData);
        }
        return listcontentdata;

    }
}