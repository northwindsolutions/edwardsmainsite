﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Blue.ascx.cs" Inherits="library_includes_controls_callouts_Blue" %>

<div class="callout blue">
    <h3><asp:Label ID="uxCalloutHeaderlbl" runat="server"></asp:Label></h3>
    <asp:Image ID="uxCalloutImage" runat="server" />
    <div class="block">
        <asp:HyperLink ID="uxCalloutLink" runat="server"></asp:HyperLink>
    </div>
</div>