﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactUs.ascx.cs" Inherits="library_includes_controls_callouts_ContactUs" %>

<div class="callout">
    <h3><asp:Label ID="uxCalloutHeaderlbl" runat="server"></asp:Label></h3>
    <div class="block">
        <asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal><br />
        <asp:HyperLink ID="uxCalloutLink" runat="server"><strong><asp:Label ID="uxCalloutLinklbl" runat="server"></asp:Label></strong></asp:HyperLink>
    </div>
</div>