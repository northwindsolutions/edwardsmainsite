﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomeLeft.ascx.cs" Inherits="library_includes_controls_callouts_HomeLeft" %>
<hr />
<h3 id="uxH3" runat="server" class="home-left-h3"><asp:Label ID="uxCalloutHeader" runat="server"></asp:Label></h3>
<div class="callout left">
    <asp:HyperLink ID="uxCalloutLink" runat="server">
        <asp:Image ID="uxCalloutImage" runat="server" />
        <span class="block">
            <span class="block-indicator">&nbsp;</span>
            <span class="block-text"><asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal></span>
            <!-- <span id="fancybutton" runat="server"></span> -->
        </span>
    </asp:HyperLink>
</div>