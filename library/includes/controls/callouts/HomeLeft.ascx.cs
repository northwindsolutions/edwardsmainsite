﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;

public partial class library_includes_controls_callouts_HomeLeft : CalloutControlBase
{
    private bool bFancyButton = false;
    protected CalloutInfo calloutInfo { get; set; }
    public override void Initialize(CalloutInfo _calloutInfo)
    {
        calloutInfo = _calloutInfo;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (calloutInfo != null)
        {
            if (!String.IsNullOrEmpty(calloutInfo.Icon)) { uxH3.Attributes["class"] = calloutInfo.Icon; }
            uxCalloutHeader.Text = !String.IsNullOrEmpty(calloutInfo.Header) ? calloutInfo.Header : string.Empty;
            uxCalloutLink.NavigateUrl = !String.IsNullOrEmpty(calloutInfo.LinkSrc) ? calloutInfo.LinkSrc : string.Empty;
            uxCalloutLink.Target = !String.IsNullOrEmpty(calloutInfo.LinkTarget) ? calloutInfo.LinkTarget : string.Empty;
            uxCalloutImage.ImageUrl = !String.IsNullOrEmpty(calloutInfo.ImageSrc) ? calloutInfo.ImageSrc : string.Empty;
            uxCalloutImage.AlternateText = !String.IsNullOrEmpty(calloutInfo.ImageAlt) ? calloutInfo.ImageAlt : string.Empty;
            uxCalloutContentLit.Text = !String.IsNullOrEmpty(calloutInfo.LinkText) ? calloutInfo.LinkText : string.Empty;

            if (!String.IsNullOrEmpty(calloutInfo.FancyButton)) { bFancyButton = (calloutInfo.FancyButton == "true") ? true : false; } 
            if (bFancyButton) { fancybutton.Attributes["class"] = "icon"; }
        }
    }
}