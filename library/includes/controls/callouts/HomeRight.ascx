﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomeRight.ascx.cs" Inherits="library_includes_controls_callouts_HomeRight" %>

<h3 id="uxH3" runat="server"><asp:Label ID="uxCalloutHeader" runat="server"></asp:Label></h3>
<div class="callout right">
    <asp:HyperLink ID="uxCalloutLink" runat="server">
        <asp:Image ID="uxCalloutImage" runat="server" />
        <span class="block">
            <asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal>
            <span id="fancybutton" runat="server"></span>
        </span>
    </asp:HyperLink>
</div>