﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Standard.ascx.cs" Inherits="library_includes_controls_callouts_Standard" %>

<div class="callout">
    <asp:Literal ID="uxCalloutHeaderlbl" runat="server"></asp:Literal>
    <asp:Image ID="uxCalloutImage" runat="server" />
    <div id="block" runat="server">
        <p class="margin-top-bottom-for-big-icons"><asp:Literal ID="uxCalloutContentIconSpan" runat="server"></asp:Literal></p>
        <p><asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal></p>
        <p><asp:HyperLink ID="uxCalloutLink" runat="server"></asp:HyperLink><span id="fancybutton" runat="server"></span></p>
        <p><asp:HyperLink ID="uxCalloutLink2" runat="server"></asp:HyperLink><span id="fancybutton2" runat="server"></span></p>
    </div>
</div>