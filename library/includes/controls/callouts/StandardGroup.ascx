﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StandardGroup.ascx.cs" Inherits="library_includes_controls_callouts_StandardGroup" %>
<%@ Import Namespace="Edwards.Models" %>
<div class="callout-group">
    <asp:Repeater runat="server" ID="callouts">
        <ItemTemplate>
            <div>
                <h3><%# ((CalloutInfo)Container.DataItem).Header %></h3>
                <img class="alignnone" src="<%# ((CalloutInfo)Container.DataItem).ImageSrc %>" alt="<%# ((CalloutInfo)Container.DataItem).ImageAlt %>" title="<%# ((CalloutInfo)Container.DataItem).ImageAlt %>">
                <div class="block">
                    <strong><%# ((CalloutInfo)Container.DataItem).Content %></strong>
                    <asp:Literal runat="server" Text="<br/>" Visible="False" ID="breakIt"></asp:Literal>
                    <asp:PlaceHolder runat="server" ID="links"></asp:PlaceHolder>

                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>

</div>
