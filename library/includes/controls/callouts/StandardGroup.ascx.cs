﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Edwards.Models;
using Ektron.Cms;

public partial class library_includes_controls_callouts_StandardGroup : UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize(ContentData cdata)
    {
        var calloutList = cdata.ToCalloutList();
        if (calloutList != null)
        {
            callouts.DataSource = calloutList;
            callouts.ItemDataBound += (sender, args) =>
            {
                if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
                    return;

                var item = args.Item.DataItem as CalloutInfo;
                if (item == null)
                    return;

                var links = args.Item.FindControl("links") as PlaceHolder;
                var breakIt = args.Item.FindControl("breakIt") as Literal;
                if (links == null || breakIt == null)
                    return;

                if (!string.IsNullOrWhiteSpace(item.LinkSrc) && !string.IsNullOrWhiteSpace(item.LinkText))
                {
                    breakIt.Visible = true;

                    var link1 = new HyperLink();
                    link1.NavigateUrl = item.LinkSrc;
                    link1.Text = string.Format("{0}<span class='icon'></span>", item.LinkText);
                    link1.Target = string.IsNullOrWhiteSpace(item.LinkTarget) ? "_self" : item.LinkTarget;
                    
                    links.Controls.Add(link1);
                    links.Controls.Add(new Literal { Text = "<br/>" });
                }
                if (!string.IsNullOrWhiteSpace(item.OptionalLinkSrc) && !string.IsNullOrWhiteSpace(item.OptionalLink))
                {
                    var link2 = new HyperLink();
                    link2.NavigateUrl = item.OptionalLinkSrc;
                    link2.Text = string.Format("{0}<span class='icon'></span>", item.OptionalLink);
                    link2.Target = string.IsNullOrWhiteSpace(item.OptionalLinkTarget) ? "_self" : item.OptionalLinkTarget;

                    links.Controls.Add(link2);
                    links.Controls.Add(new Literal { Text = "<br/>" });
                }
            };
            callouts.DataBind();
        }
    }

}