﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeFile="StandardVideo.ascx.cs" Inherits="Standard_Video" %>

<div class="callout">
    <asp:Literal ID="uxCalloutHeaderlbl" runat="server"></asp:Literal>
    <asp:Image ID="uxCalloutImage" runat="server" />
    <div id="block" runat="server">
        <p><asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal></p>
        <p><asp:HyperLink class="fancybox-media" ID="uxCalloutLink" runat="server"></asp:HyperLink><span id="fancybutton" runat="server"></span></p>
        <p><asp:HyperLink ID="uxCalloutLink2" runat="server"></asp:HyperLink><span id="fancybutton2" runat="server"></span></p>
    </div>
</div>

<script>
    $(document).ready(function() {
	    $('.fancybox-media').fancybox({
		    openEffect  : 'none',
		    closeEffect : 'none',
		    helpers : {
			    media : {}
		    }
	    });
    });
</script>