﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;
using Ektron.Cms.Framework.UI;

public partial class Standard_Video : CalloutControlBase
{
    private bool bFancyButton = false;
    private bool bGrayBG = false;
    protected CalloutInfo calloutInfo { get; set; }
    public override void Initialize(CalloutInfo _calloutInfo)
    {
        calloutInfo = _calloutInfo;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterResourcePackages();

        if (calloutInfo != null)
        {
            uxCalloutHeaderlbl.Text = String.IsNullOrEmpty(calloutInfo.Header) ? 
                                     !String.IsNullOrEmpty(calloutInfo.Icon) ?
                                     "<h3 class=" + calloutInfo.Icon + ">&nbsp;</h3>" :
                                     string.Empty :
                                     !String.IsNullOrEmpty(calloutInfo.Icon) ? 
                                     "<h3 class=" + calloutInfo.Icon + ">" + calloutInfo.Header + "</h3>" :
                                     "<h3>" + calloutInfo.Header + "</h3>";

            uxCalloutContentLit.Text = !String.IsNullOrEmpty(calloutInfo.Content) ? calloutInfo.Content : string.Empty;
            uxCalloutImage.ImageUrl = !String.IsNullOrEmpty(calloutInfo.ImageSrc) ? calloutInfo.ImageSrc : string.Empty;
            uxCalloutImage.AlternateText = !String.IsNullOrEmpty(calloutInfo.ImageAlt) ? calloutInfo.ImageAlt : string.Empty;
            uxCalloutImage.Visible = !String.IsNullOrEmpty(calloutInfo.ImageSrc) ? true : false;

            uxCalloutLink.NavigateUrl = !String.IsNullOrEmpty(calloutInfo.Video) ? calloutInfo.Video : string.Empty;
            uxCalloutLink.Text = !String.IsNullOrEmpty(calloutInfo.LinkText) ? calloutInfo.LinkText : string.Empty;
            uxCalloutLink.Target = !String.IsNullOrEmpty(calloutInfo.LinkTarget) ? calloutInfo.LinkTarget : string.Empty;

            // optional link
            uxCalloutLink2.Text = !String.IsNullOrEmpty(calloutInfo.OptionalLink) ? calloutInfo.OptionalLink : string.Empty;
            uxCalloutLink2.Target = !String.IsNullOrEmpty(calloutInfo.OptionalLinkTarget) ? calloutInfo.OptionalLinkTarget : string.Empty;
            uxCalloutLink2.NavigateUrl = !String.IsNullOrEmpty(calloutInfo.OptionalLinkSrc) ? calloutInfo.OptionalLinkSrc : string.Empty;

            // gray background
            if (!String.IsNullOrEmpty(calloutInfo.GrayBackground)) { bGrayBG = (calloutInfo.GrayBackground == "true") ? true : false; }
            if (bGrayBG) { block.Attributes["class"] = "block"; }

            // fancy button
            if (!String.IsNullOrEmpty(calloutInfo.FancyButton)) { bFancyButton = (calloutInfo.FancyButton == "true") ? true : false; }
            if (bFancyButton) { fancybutton.Attributes["class"] = "icon"; }

            if (!String.IsNullOrEmpty(calloutInfo.OptionalLink) && bFancyButton) { fancybutton2.Attributes["class"] = "icon";} 

        }
    }

    private void RegisterResourcePackages()
    {
        var api = new Ektron.Cms.CommonApi();

        var javascriptPackage = new Package
        {
            Components = new List<Component>
                {
                    Packages.EktronCoreJS,
                    JavaScript.Create(api.SitePath + "library/fancybox/source/jquery.fancybox.pack.js"),
                    JavaScript.Create(api.SitePath + "library/fancybox/source/helpers/jquery.fancybox-media.js")
                }
        };

        javascriptPackage.Register(this, true);

        Css.Register(this, api.SitePath + "library/fancybox/source/jquery.fancybox.css", BrowserTarget.All, true, "screen");
    }
}