﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeFile="VideoOverlay.ascx.cs" Inherits="Video_Overlay" %>

<asp:Literal ID="uxCalloutTitleLit" runat="server" />
<div class="calloutVideo" style="background-image:url(<%= calloutInfo.ImageSrc %>);">
    <div class="block">
        <asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal><br />
        <asp:HyperLink ID="uxCalloutLink" class="fancybox-media" runat="server"></asp:HyperLink><br />
        <asp:HyperLink ID="uxCalloutLink2" class="fancybox-media" runat="server"><img class="playIcon" style="margin: 0 auto;" src="/library/images/common/play-icon.png" alt="Watch the video" title="Watch the video"></asp:HyperLink>
        <span class='st_sharethis_custom' st_url='<%= calloutInfo.Video %>' st_title='World-class solutions'>Share this</span>
    </div>
</div>