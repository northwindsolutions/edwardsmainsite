﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;

public partial class Video_Overlay : CalloutControlBase
{
    private bool bFancyButton = false;
    private bool bGrayBG = false;
    protected CalloutInfo calloutInfo { get; set; }
    public override void Initialize(CalloutInfo _calloutInfo)
    {
        calloutInfo = _calloutInfo;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (calloutInfo != null)
        {
            uxCalloutTitleLit.Text = String.IsNullOrEmpty(calloutInfo.Header) ?
                                       !String.IsNullOrEmpty(calloutInfo.Icon) ?
                                       "<h3 class=" + calloutInfo.Icon + ">&nbsp;</h3>" :
                                       string.Empty :
                                       !String.IsNullOrEmpty(calloutInfo.Icon) ?
                                       "<h3 class=" + calloutInfo.Icon + ">" + calloutInfo.Header + "</h3>" :
                                       "<h3>" + calloutInfo.Header + "</h3>";

            uxCalloutContentLit.Text = !String.IsNullOrEmpty(calloutInfo.Content) ? calloutInfo.Content : string.Empty;
            uxCalloutLink.NavigateUrl = !String.IsNullOrEmpty(calloutInfo.Video) ? calloutInfo.Video : string.Empty;
            uxCalloutLink.Text = !String.IsNullOrEmpty(calloutInfo.LinkText) ? calloutInfo.LinkText : string.Empty;
            uxCalloutLink.Target = !String.IsNullOrEmpty(calloutInfo.LinkTarget) ? calloutInfo.LinkTarget : string.Empty;
            uxCalloutLink2.NavigateUrl = !String.IsNullOrEmpty(calloutInfo.Video) ? calloutInfo.Video : string.Empty;
        }
    }
}