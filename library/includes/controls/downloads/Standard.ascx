﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Standard.ascx.cs" Inherits="Downloads_Standard" %>

<div class="callout callout-red">
    <p class="padding-top-bottom-for-big-icons"><span class="icon-downloads big-icons-light">&#160;</span></p>
    <asp:Label ID="uxDownloadsTitle" runat="server" />
    <div class="block">
        <ul class="downloads-list">
            <asp:Repeater ID="uxDownloadsRepeater" runat="server">
                <ItemTemplate>
                    <li><asp:HyperLink ID="uxDownloadsLink" runat="server"></asp:HyperLink></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</div>