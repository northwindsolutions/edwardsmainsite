﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;

public partial class Downloads_StandardRed : DownloadsControlBase
{
    private List<DownloadsInfo> downloadsLinkList { get; set; }

    public override void Initialize(List<DownloadsInfo> _downloadLinkList)
    {
        downloadsLinkList = _downloadLinkList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (downloadsLinkList != null && downloadsLinkList.Count > 0)
        {
            uxDownloadsTitle.Text = String.IsNullOrEmpty(downloadsLinkList[0].Title) ?
                                     !String.IsNullOrEmpty(downloadsLinkList[0].Icon) ?
                                     "<h3 class=" + downloadsLinkList[0].Icon + ">&nbsp;</h3>" :
                                     string.Empty :
                                     !String.IsNullOrEmpty(downloadsLinkList[0].Icon) ?
                                     "<h3 class=" + downloadsLinkList[0].Icon + ">" + downloadsLinkList[0].Title + "</h3>" :
                                     "<h3>" + downloadsLinkList[0].Title + "</h3>";

            LoadListResults();
        }
    }

    protected void LoadListResults()
    {
        uxDownloadsRepeater.DataSource = downloadsLinkList;
        uxDownloadsRepeater.ItemDataBound += (sender, args) =>
        {
            var linkItem = args.Item.DataItem as DownloadsInfo;

            if (linkItem == null)
                return;

            var linkControl = args.Item.FindControl("uxDownloadsLink") as HyperLink;

            if (linkControl == null)
                return;

            linkControl.Text = linkItem.LinkText;
            linkControl.NavigateUrl = linkItem.LinkSource;
            linkControl.Target = linkItem.LinkTarget;
        };
        uxDownloadsRepeater.DataBind();
    }
}