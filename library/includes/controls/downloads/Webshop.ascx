﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Webshop.ascx.cs" Inherits="downloads_webshop" %>

<div class="callout orange">
    <asp:Label ID="uxTitleLabel" runat="server" />
    <div class="block">
        <ul class="document-list">
            <asp:Repeater ID="uxWebshopViewRepeater" runat="server">
                <ItemTemplate>
                    <li><asp:HyperLink ID="uxLink" runat="server"></asp:HyperLink></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</div>