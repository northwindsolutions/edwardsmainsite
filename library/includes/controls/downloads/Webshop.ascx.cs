﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;

public partial class downloads_webshop : DownloadsControlBase
{
    private List<DownloadsInfo> downloadsInfoList { get; set; }

    public override void Initialize(List<DownloadsInfo> _downloadsInfoList)
    {
        downloadsInfoList = _downloadsInfoList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (downloadsInfoList != null)
        {
            uxTitleLabel.Text = String.IsNullOrEmpty(downloadsInfoList[0].Title) ?
                                     !String.IsNullOrEmpty(downloadsInfoList[0].Icon) ?
                                     "<h3 class=" + downloadsInfoList[0].Icon + ">&nbsp;</h3>" :
                                     string.Empty :
                                     !String.IsNullOrEmpty(downloadsInfoList[0].Icon) ?
                                     "<h3 class=" + downloadsInfoList[0].Icon + ">" + downloadsInfoList[0].Title + "</h3>" :
                                     "<h3>" + downloadsInfoList[0].Title + "</h3>";

            LoadListResults();
        }
    }

    protected void LoadListResults()
    {
        uxWebshopViewRepeater.DataSource = downloadsInfoList;
        uxWebshopViewRepeater.ItemDataBound += (sender, args) =>
        {
            var downloadLink = args.Item.DataItem as DownloadsInfo;

            if (downloadLink == null)
                return;

            var link = args.Item.FindControl("uxLink") as HyperLink;

            if (link == null)
                return;

            link.Text = downloadLink.LinkText;
            link.NavigateUrl = downloadLink.LinkSource;
            link.Target = downloadLink.LinkTarget;
        };
        uxWebshopViewRepeater.DataBind();
    }
}