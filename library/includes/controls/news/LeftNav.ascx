﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftNav.ascx.cs" Inherits="library_includes_controls_news_LeftNav" %>
<ul id="newsLeftNav">
    
    <asp:Repeater runat="server" ID="newsLeftNavRepeater">
        <ItemTemplate>
            <li runat="server" id="li">
                <asp:HyperLink runat="server" ID="a"></asp:HyperLink>
            </li>
        </ItemTemplate>
    </asp:Repeater>

</ul>
