﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Edwards.Models;
using Edwards.Utility;

public partial class library_includes_controls_news_LeftNav : System.Web.UI.UserControl
{
    public library_includes_controls_news_LeftNav()
    {
        this.TaxonomyCategories = new List<long>();
    }

    public List<long> TaxonomyCategories { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (TaxonomyCategories.Count == 0)
        {
            TaxonomyCategories.Add(Constants.TaxonomyIds.NewsCategory);
        }

        var now = DateTime.UtcNow;
        // Find all news items and get what years are available.
        IOrderedEnumerable<int> years = TaxonomyUtility.GetContentDataListByTaxonomyCategories(TaxonomyCategories, Constants.CurrentLanguageId, recursive: true)
            .Select(t => t.Html.GetNewsItemObject())
            .Where(t => t != null && t.Date.HasValue)
            .Select(t => t.Date.Value.Year)
            .Distinct()
            .OrderByDescending(y => y);

        newsLeftNavRepeater.DataSource = years;
        newsLeftNavRepeater.ItemDataBound += (o, args) =>
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                return;

            var year = args.Item.DataItem as int?;
            if (year.HasValue)
            {
                var li = args.Item.FindControl("li") as HtmlGenericControl;
                var a = args.Item.FindControl("a") as HyperLink;
                if (li == null || a == null)
                    return;

                if (year == now.Year)
                    li.Attributes.Add("class", "selected");

                a.NavigateUrl = "#";
                a.Text = year.Value.ToString();
                a.Attributes.Add("data-year", year.Value.ToString());
            }
        };
        newsLeftNavRepeater.DataBind();
    }
}