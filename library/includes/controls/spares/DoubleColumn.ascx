﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DoubleColumn.ascx.cs" Inherits="spares_DoubleColumn" %>

<div class="purple">
    <asp:Label ID="uxTitleLabel" runat="server" />
</div>
<div class="keywordSearch">
    <div class="purple">
        <div class="one-half primary">
            <asp:Repeater ID="uxLeftColumnRepeater" runat="server">
                <ItemTemplate>
                    <asp:HyperLink ID="uxLeftColumnLink" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="one-half primary omega">
            <asp:Repeater ID="uxRightColumnRepeater" runat="server">
                <ItemTemplate>
                    <asp:HyperLink ID="uxRightColumnLink" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <asp:HyperLink ID="uxMainLink" runat="server"></asp:HyperLink><span class="icon-purple"></span>
</div>