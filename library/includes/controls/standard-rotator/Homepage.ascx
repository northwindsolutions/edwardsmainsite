﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Homepage.ascx.cs" Inherits="standardRotator_homepage_view" %>

<div id="showHide">
    <div id="homeSlideshowContainer">
        <div id="homeSlideshow" class="billboard">
            <div id="homeSlideshowWrap">
                <div id="homeSlideshow">
                    <ul>
                        <asp:Repeater ID="uxHomepageRotatorRepeater" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:Image ID="uxHomeSliderImage" CssClass="alignMiddle" runat="server" />
                                    <a id="uxRotatorItemLink" runat="server">
                                        <asp:Image ID="uxRotatorItemImageWithLink" runat="server" />
                                    </a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
          <div id="homeSlideshowNavWrap">
            <div id="homeSlideshowNav">
                  <div class="homeSlideshowNavLiner">
                      <a href="#" class="homePrev"></a>
                      <a href="#" class="homeNext"></a>
                  </div>
              </div>
          </div>
        </div> 
    </div>
</div>

<div id="homeToggle">
    <a href="#" class="homeShow selected"><%= GetGlobalResourceObject("EdwardsGlobal", "Show") %></a> | <a href="#" class="homeHide"><%= GetGlobalResourceObject("EdwardsGlobal", "HideAnimation") %></a>
</div>

<br />

<script>
    (function ($) {
        var interval = <%= RotationInterval.HasValue ? (RotationInterval * 1000).ToString() : "null" %>;
    if (interval) {
        var $next = $("#homeSlideshowNav .homeNext");
        var autoScroll = function () {
            $next.click();
        };
        var timer = setInterval(autoScroll, interval);

        var $clip = $("#homeSlideshowWrap, #homeSlideshowNav");
        $clip.on("mouseenter", function (e) {
            clearInterval(timer);
        });
        $clip.on('mouseleave', function (e) {
            timer = setInterval(autoScroll, interval);
        });
    }
})($);
</script>