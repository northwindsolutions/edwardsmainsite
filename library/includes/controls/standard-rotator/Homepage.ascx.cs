﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;
using System.Web.UI.HtmlControls;

public partial class standardRotator_homepage_view : StandardRotatorControlBase
{
    protected List<StandardRotatorInfo> rotatorList { get; set; }
    protected int? RotationInterval { get; set; }

    public override void Initialize(List<StandardRotatorInfo> _rotatorList)
    {
        rotatorList = _rotatorList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (rotatorList != null && rotatorList.Count > 0)
        {
            RotationInterval = (rotatorList[0].RotationInterval > 0) ? rotatorList[0].RotationInterval : 0;

            LoadListResults();

        }

    }

    protected void LoadListResults()
    {
        uxHomepageRotatorRepeater.DataSource = rotatorList;
        uxHomepageRotatorRepeater.ItemDataBound += (sender, args) =>
        {
            var rotatorItem = args.Item.DataItem as StandardRotatorInfo;

            if (rotatorItem == null)
                return;

            var image = args.Item.FindControl("uxHomeSliderImage") as Image;
            var imageAnchor = args.Item.FindControl("uxRotatorItemLink") as HtmlAnchor;
            var linkedImage = args.Item.FindControl("uxRotatorItemImageWithLink") as Image;

            if (image == null || imageAnchor == null || linkedImage == null)
                return;

            image.Visible = false;
            linkedImage.Visible = false;

            if (rotatorItem.HasLink)
            {
                linkedImage.Visible = true;
                imageAnchor.HRef = rotatorItem.LinkUrl;
                imageAnchor.Target = rotatorItem.LinkTarget;
                linkedImage.ImageUrl = rotatorItem.ImageSrc;
                linkedImage.AlternateText = rotatorItem.ImageAlt;
            }
            else
            {
                image.Visible = true;
                image.ImageUrl = rotatorItem.ImageSrc;
                image.AlternateText = rotatorItem.ImageAlt;
            }
        };

        uxHomepageRotatorRepeater.DataBind();
    }
}