﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Standard.ascx.cs" Inherits="standardRotator_standard_view" %>
<div class="content-container">
<h3><asp:Label ID="uxRotatorHeader" runat="server"></asp:Label></h3>
<div id="slideshowContainer">
    <div id="slideshowNav">
        <a href="#" class="prev"></a>
        <a href="#" class="next"></a>
    </div>
<div class="slideshowGates">
        <a href="#" class="right"></a>
        <a href="#" class="left"></a>
    </div>
    <div id="slideshowWrap">
        <div id="slideshow">
            <ul>
                <asp:Repeater ID="uxStandardRotatorRepeater" runat="server">
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="uxRotatorItemLiteral" runat="server" />    
                            <asp:Image ID="uxRotatorItemImage" runat="server" />
                            <a id="uxRotatorItemLink" runat="server">
                                <asp:Image ID="uxRotatorItemImageWithLink" runat="server" />
                            </a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</div>
</div>

<script>
(function ($) {
    var interval = <%= RotationInterval.HasValue ? (RotationInterval * 1000).ToString() : "null" %>;
        if (interval) {
            var $next = $("#slideshowNav .next");
            var autoScroll = function () {
                $next.click();
            };
            var timer = setInterval(autoScroll, interval);

            var $clip = $("#slideshowWrap, #slideshowNav");
            $clip.on("mouseenter", function (e) {
                clearInterval(timer);
            });
            $clip.on('mouseleave', function (e) {
                timer = setInterval(autoScroll, interval);
            });
        }
    })($);
</script>