﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;
using System.Web.UI.HtmlControls;

public partial class standardRotator_standard_view : StandardRotatorControlBase
{
    protected List<StandardRotatorInfo> rotatorList { get; set; }
    protected int? RotationInterval { get; set; }

    public override void Initialize(List<StandardRotatorInfo> _rotatorList)
    {
        rotatorList = _rotatorList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (rotatorList != null && rotatorList.Count > 0)
        {
            uxRotatorHeader.Text = !string.IsNullOrEmpty(rotatorList[0].Title) ? rotatorList[0].Title : string.Empty;
            RotationInterval = (rotatorList[0].RotationInterval > 0) ? rotatorList[0].RotationInterval : 0;

            LoadListResults(); 
            
        }

    }

    protected void LoadListResults()
    {
        uxStandardRotatorRepeater.DataSource = rotatorList;
        
        uxStandardRotatorRepeater.ItemDataBound += (sender, args) =>
        {
            var rotatorItem = args.Item.DataItem as StandardRotatorInfo;
               
            if (rotatorItem == null)
                return;

            var video = args.Item.FindControl("uxRotatorItemLiteral") as Literal;
            var image = args.Item.FindControl("uxRotatorItemImage") as Image;
            var imageAnchor = args.Item.FindControl("uxRotatorItemLink") as HtmlAnchor;
            var linkedImage = args.Item.FindControl("uxRotatorItemImageWithLink") as Image;

            if (video == null || image == null || linkedImage == null || linkedImage == null)
                return;

            image.Visible = false;
            imageAnchor.Visible = false;
            video.Visible = false;

            if (!string.IsNullOrEmpty(rotatorItem.ImageSrc))
            {
                if (rotatorItem.HasLink)
                {
                    imageAnchor.Visible = true;   
                    imageAnchor.HRef = rotatorItem.LinkUrl;
                    imageAnchor.Target = rotatorItem.LinkTarget;
                    linkedImage.ImageUrl = rotatorItem.ImageSrc;
                    linkedImage.AlternateText = rotatorItem.ImageAlt;
                }
                else
                {
                    image.Visible = true;
                    image.ImageUrl = rotatorItem.ImageSrc;
                    image.AlternateText = rotatorItem.ImageAlt;
                }
            }
            else
            {
                video.Text = rotatorItem.VideoEmbed;
                video.Visible = true;
            }
        };

        uxStandardRotatorRepeater.DataBind();
    }
}