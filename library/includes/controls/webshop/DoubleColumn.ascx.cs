﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;

public partial class webshop_DoubleColumn : WebshopControlBase
{
    private WebshopInfo webshopInfo { get; set; }
    private List<WebshopLink> LeftColumnList { get; set; }
    private List<WebshopLink> RightColumnList { get; set; }

    public override void Initialize(WebshopInfo _webshopInfo)
    {
        webshopInfo = _webshopInfo;

        if (_webshopInfo.LeftColumnList.Count > 0 && _webshopInfo.RightColumnList.Count > 0)
        {
            LeftColumnList = _webshopInfo.LeftColumnList;
            RightColumnList = _webshopInfo.RightColumnList;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (webshopInfo != null)
        {
            uxTitleLabel.Text = String.IsNullOrEmpty(webshopInfo.Title) ?
                                     !String.IsNullOrEmpty(webshopInfo.Icon) ?
                                     "<h3 class=" + webshopInfo.Icon + ">&nbsp;</h3>" :
                                     string.Empty :
                                     !String.IsNullOrEmpty(webshopInfo.Icon) ?
                                     "<h3 class=" + webshopInfo.Icon + ">" + webshopInfo.Title + "</h3>" :
                                     "<h3>" + webshopInfo.Title + "</h3>";

            uxMainLink.Text = !string.IsNullOrEmpty(webshopInfo.LinkSource) ? webshopInfo.LinkText : string.Empty;
            uxMainLink.NavigateUrl = !string.IsNullOrEmpty(webshopInfo.LinkSource) ? webshopInfo.LinkSource : string.Empty;
            uxMainLink.Target = !string.IsNullOrEmpty(webshopInfo.LinkSource) ? webshopInfo.LinkTarget : string.Empty;

            LoadColumnRepeater(uxLeftColumnRepeater, LeftColumnList, "uxLeftColumnLink");
            LoadColumnRepeater(uxRightColumnRepeater, RightColumnList, "uxRightColumnLink");
        }
    }

    protected void LoadColumnRepeater(Repeater repeater, List<WebshopLink> columnList, string linkControlName)
    {
        repeater.DataSource = columnList;
        repeater.ItemDataBound += (sender, args) =>
        {
            var linkList = args.Item.DataItem as WebshopLink;

            if (linkList == null)
                return;

            var link = args.Item.FindControl(linkControlName) as HyperLink;

            if (link == null)
                return;

            link.Text = linkList.LinkText + "<span class='icon-right'></span>";
            link.NavigateUrl = linkList.LinkSource;
            link.Target = linkList.LinkTarget;
        };
        repeater.DataBind();
    }
}