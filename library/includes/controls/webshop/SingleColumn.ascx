﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SingleColumn.ascx.cs" Inherits="webshop_singlecolumn" %>

<div class="callout orange">
    <asp:Label ID="uxTitleLabel" runat="server" />
    <div class="keywordSearch">
        <div class="one-half primary">
            <asp:Repeater ID="uxStandardViewRepeater" runat="server">
                <ItemTemplate>
                    <asp:HyperLink ID="uxLink" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>