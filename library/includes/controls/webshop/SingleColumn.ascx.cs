﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;

public partial class webshop_singlecolumn : WebshopControlBase
{
    private WebshopInfo webshopInfo { get; set; }
    private List<WebshopLink> webshopItemList { get; set; }

    public override void Initialize(WebshopInfo _webshopInfo)
    {
        webshopInfo = _webshopInfo;

        if (_webshopInfo.LeftColumnList.Count > 0)
        {
            webshopItemList = _webshopInfo.LeftColumnList;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (webshopInfo != null)
        {
            uxTitleLabel.Text = String.IsNullOrEmpty(webshopInfo.Title) ?
                                     !String.IsNullOrEmpty(webshopInfo.Icon) ?
                                     "<h3 class=" + webshopInfo.Icon + ">&nbsp;</h3>" :
                                     string.Empty :
                                     !String.IsNullOrEmpty(webshopInfo.Icon) ?
                                     "<h3 class=" + webshopInfo.Icon + ">" + webshopInfo.Title + "</h3>" :
                                     "<h3>" + webshopInfo.Title + "</h3>";

            LoadListResults();
        }
    }

    protected void LoadListResults()
    {
        uxStandardViewRepeater.DataSource = webshopItemList;
        uxStandardViewRepeater.ItemDataBound += (sender, args) =>
        {
            var webshopItem = args.Item.DataItem as WebshopLink;

            if (webshopItem == null)
                return;

            var link = args.Item.FindControl("uxLink") as HyperLink;

            if (link == null)
                return;

            link.Text = "<span class='icon-right'></span><span>" + webshopItem.LinkText + "</span>";
            link.NavigateUrl = webshopItem.LinkSource;
            link.Target = webshopItem.LinkTarget;
        };
        uxStandardViewRepeater.DataBind();
    }
}