﻿$(function () {
    modalPosition();
    $(window).resize(function () {
        modalPosition();
    });
    $('.openModal').click(function (e) {
        $('.modal, .modal-backdrop').fadeIn('fast');
        e.preventDefault();
    });
    $('.close-modal').click(function (e) {
        $('.modal, .modal-backdrop').fadeOut('fast');
    });
    $('.close-modal').click(function (e) {
        $('.modal, .modal-backdrop').fadeOut('fast');
    });
});

function modalPosition() {
    var width = $('.modal').width();
    var pageWidth = $(window).width();
    var x = (pageWidth / 2) - (width / 2);
    $('.modal').css({ left: x + "px" });   
}

$(function () {
    $('#avatarUpload').hide();
    $('#positionFail').hide();
    $('#phoneNofail').hide();
    $('#emailAddfail').hide();
    $('#imageSize').hide();
    $('#fNameFail').hide();
    $('#lNameFail').hide();
    $('#companyFail').hide();
    $('#emailAddFail').hide();
    $('#addLine1Fail').hide();
    $('#cityFail').hide();
    $('#regionFail').hide();
    $('#stateFail').hide();
    $('#postalcodeFail').hide();
    $('#edwardscontactFail').hide();

    $('#tradefNameFail').hide();
    $('#tradelNameFail').hide();
    $('#tradecompanyFail').hide();
    $('#tradeemailAddFail').hide();
    $('#tradeaddLine1Fail').hide();
    $('#tradedateFail').hide();
    $('#tradeedwardscontactFail').hide();
    $('#tradeLanguageFail').hide();

    $('#dashboard > .one-half > #myAccountFavorites > #favoriteMarkets > ul.featuredPages > li').hide().filter(':lt(8)').show();
    $('#dashboard > .one-half > #myAccountFavorites > #favoriteMarkets > ul.featuredPages').append('<li class="one-half"><a>View More</a> <span class="icon"></span></li>').find('li:last').click(function () {
        $(this).siblings(':gt(8)').show();
        $('#dashboard > .one-half > #myAccountFavorites > #favoriteMarkets > ul.featuredPages > li').last().hide();
    });

    $('#dashboard > .one-half > #myAccountFavorites > #favoriteProducts > ul.featuredPages > li').hide().filter(':lt(8)').show();
    $('#dashboard > .one-half > #myAccountFavorites > #favoriteProducts > ul.featuredPages').append('<li class="one-half"><a>View More</a> <span class="icon"></span></li>').find('li:last').click(function () {
        $(this).siblings(':gt(8)').show();
        $('#dashboard > .one-half > #myAccountFavorites > #favoriteProducts > ul.featuredPages > li').last().hide();
    });
   
    if ($('#dashboard > .one-half > #myAccountFavorites > #favoriteMarkets > ul.featuredPages > li').length < 9) {
        $('#dashboard > .one-half > #myAccountFavorites > #favoriteMarkets > ul.featuredPages > li').last().hide();
    }
    if ($('#dashboard > .one-half > #myAccountFavorites > #favoriteProducts > ul.featuredPages > li').length < 9) {
        $('#dashboard > .one-half > #myAccountFavorites > #favoriteProducts > ul.featuredPages > li').last().hide();
    }
    $('#saveButton').on('click', function () {
        var title = document.getElementById("position").value;
        var email = document.getElementById("emailAdd").value;
        var phNo = document.getElementById("phoneNo").value;
        var imgsize = $("#avatarUpload")[0].files[0];
        if(imgsize != undefined)
            var size = parseFloat($("#avatarUpload")[0].files[0].size / 1024).toFixed(2);
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var phfilter =/^\(?\d{1,5}\)?[- ]?\d{1,5}[- ]?\d{1,10}$/;
        var titlefilter = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;

        if ((titlefilter.test(title) == false) || ((document.getElementById("position").value).length == 0)) {
            $('#positionFail').show();
            return false;
        }
        else if ((phfilter.test(phNo) == false) || ((document.getElementById("phoneNo").value).length == 0)) {
            $('#phoneNofail').show();
            return false;
        }
        else if (((document.getElementById("emailAdd").value).length == 0) || (filter.test(email) == false)) {
            $('#emailAddfail').show();
            return false;
        }

        else if (size>1000) {
            $('#imageSize').show();
               return false;
        } else {
            $('#imageSize').hide();
            return true;
        }
        
    });

    $("input[id*='position']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#positionFail').hide();
        }
    });
    $("input[id*='phoneNo']").on('keyup', function () {
        if ($(this).val() != "") {
       
                $('#phoneNofail').hide();
            
        }
    });
    $("input[id*='emailAdd']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#emailAddfail').hide();
        }
    });

    $('#avatarClick').on('click', function () {
        $('#avatarUpload').click();
    });

    $('#avatarUpload').change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

        if ($(this).val()) {
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Please select an profile photo in one of the following formats: '.jpeg','.jpg', '.png', '.gif', '.bmp'.");
            }
            else {
                $('#avatarClick').val($('#avatarUpload').get(0).files[0].name);
            }
        }
    });

    /* Promotional Items */

    $('#sumbitItems').on('click', function () {
        var fName = document.getElementById("fName").value;
        var lName = document.getElementById("lName").value;
        var city = document.getElementById("city").value;
        var emailAdd = document.getElementById("emailAdd").value; 
       var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var titlefilter = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;

        if ((titlefilter.test((document.getElementById("fName").value)) == false) || ((document.getElementById("fName").value).length == 0)) {
            $('#fNameFail').show();
            return false;
        }
        else if ((titlefilter.test(lName) == false) || ((document.getElementById("lName").value).length == 0)) {
            $('#lNameFail').show();
            return false;
        }
        else if (((document.getElementById("company").value).length == 0)) {
            $('#companyFail').show();
            return false;
        }
        else if ((filter.test(emailAdd) == false) || ((document.getElementById("emailAdd").value).length == 0)) {
            $('#emailAddFail').show();
            return false;
        }
        else if (((document.getElementById("addLine1").value).length == 0)) {
            $('#addLine1Fail').show();
            return false;
        }
        else if ((titlefilter.test(city) == false) || ((document.getElementById("city").value).length == 0)) {
            $('#cityFail').show();
            return false;
        }
        else if (((document.getElementById("region").value).length == 0)) {
            $('#regionFail').show();
            return false;
        }
        else if (((document.getElementById("state").value).length == 0)) {
            $('#stateFail').show();
            return false;
        }
        else if (((document.getElementById("postalcode").value).length == 0)) {
            $('#postalcodeFail').show();
            return false;
        }
        else if (((document.getElementById("edwardscontact").value).length == 0)) {
            $('#edwardscontactFail').show();
            return false;
        }
    });
    
    $("input[id*='fName']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#fNameFail').hide();
        }
    });
    $("input[id*='lName']").on('keyup', function () {
        if ($(this).val() != "") {

            $('#lNameFail').hide();

        }
    });
    $("input[id*='company']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#companyFail').hide();
        }
    });
    $("input[id*='emailAdd']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#emailAddFail').hide();
        }
    });
    $("input[id*='addLine1']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#addLine1Fail').hide();
        }
    });
    $("input[id*='city']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#cityFail').hide();
        }
    });
    $("input[id*='region']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#regionFail').hide();
        }
    });
    $("input[id*='state']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#stateFail').hide();
        }
    }); 
    $("input[id*='postalcode']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#postalcodeFail').hide();
        }
    });
    $("input[id*='edwardscontact']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#edwardscontactFail').hide();
        }
    });

    /* TradeShow Items */

    $('#sumbittradeItems').on('click', function () {
        var tradefName = document.getElementById("tradefName").value;
        console.log(tradefName);
        var tradelName = document.getElementById("tradelName").value;
        var tradeemailAdd = document.getElementById("tradeemailAdd").value;
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var titlefilter = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;

        if ((titlefilter.test(tradefName) == false) || ((document.getElementById("tradefName").value).length == 0)) {
            $('#tradefNameFail').show();
            return false;
        }
        else if ((titlefilter.test(tradelName) == false) || ((document.getElementById("tradelName").value).length == 0)) {
            $('#tradelNameFail').show();
            return false;
        }
        else if (((document.getElementById("tradecompany").value).length == 0)) {
            $('#tradecompanyFail').show();
            return false;
        }
        else if ((filter.test(tradeemailAdd) == false) || ((document.getElementById("tradeemailAdd").value).length == 0)) {
            $('#tradeemailAddFail').show();
            return false;
        }
        else if (((document.getElementById("tradeLanguage").value).length == 0)) {
            $('#tradeLanguageFail').show();
            return false;
        }
        else if (((document.getElementById("tradegraphicFormat").value).length == 0)) {
            $('#tradeaddLine1Fail').show();
            return false;
        }
        else if (((document.getElementById("tradeedwardscontact").value).length == 0)) {
            $('#tradeedwardscontactFail').show();
            return false;
        } 
        else if (((document.getElementById("graphicDate").value).length == 0)) {
            $('#tradedateFail').show();
            return false;
        }
    });

    $("input[id*='tradefName']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradefNameFail').hide();
        }
    });
    $("input[id*='tradelName']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradelNameFail').hide();
        }
    });
    $("input[id*='tradecompany']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradecompanyFail').hide();
        }
    });
    $("input[id*='tradeemailAdd']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradeemailAddFail').hide();
        }
    });
    $("input[id*='tradegraphicFormat']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradeaddLine1Fail').hide();
        }
    });
    $("input[id*='tradeedwardscontact']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradeedwardscontactFail').hide();
        }
    });
    $("input[id*='graphicDate']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradedateFail').hide();
        }
    });
    $("input[id*='tradeLanguage']").on('keyup', function () {
        if ($(this).val() != "") {
            $('#tradeLanguageFail').hide();
        }
    });
});


