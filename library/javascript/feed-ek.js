/*
* FeedEk jQuery RSS/ATOM Feed Plugin v1.1.2
* http://jquery-plugins.net/FeedEk/FeedEk.html
* Author : Engin KIZIL 
* http://www.enginkizil.com
*/
(function (e) {
    e.fn.FeedEk = function (t) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        var n = {
            FeedUrl: "http://rss.cnn.com/rss/edition.rss",
            MaxCount: 5,
            ShowDesc: true,
            ShowPubDate: true,
            CharacterLimit: 0,
            TitleLinkTarget: "_blank"
        };
        if (t) {
            e.extend(n, t)
        }
        var r = e(this).attr("id");
        var i;
        e("#" + r).empty().append('<div style="padding:3px;"><img src="/library/images/common/loader.gif" /></div>');
        e.ajax({
            url: "http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=" + n.MaxCount + "&output=json&q=" + encodeURIComponent(n.FeedUrl) + "&hl=en&callback=?",
            dataType: "json",
            success: function (t) {
                e("#" + r).empty();
                var s = "";
                e.each(t.responseData.feed.entries, function (e, t) {
                	if (t.title.length != 0) {
	                    s += '<li><div class="itemTitle">' + t.title + "</div>";
	                    if (n.ShowPubDate) {
	                        i = new Date(t.publishedDate);
	                        s += '<div class="itemDate"><a href="' + t.link + '" target="' + n.TitleLinkTarget + '" >Posted on ' + i.getDate() + ' ' + monthNames[i.getMonth()] + "</a></div>"
	                    }
	                    if (n.ShowDesc) {
	                        if (n.DescCharacterLimit > 0 && t.content.length > n.DescCharacterLimit) {
	                            s += '<div class="itemContent">' + t.content.substr(0, n.DescCharacterLimit) + "...</div>"
	                        } else {
	                            s += '<div class="itemContent">' + t.content + "</div>"
	                        }
	                    }
	                }
                });
                e("#" + r).append(s)
            }
        })
    }
})(jQuery)

$(function() {
	$('#facebookFeed').FeedEk({
	  FeedUrl : 'http://www.facebook.com/feeds/page.php?format=atom10&id=158330262919',
	  MaxCount : 3,
	  ShowDesc : false,
	  ShowPubDate:true
	});
});