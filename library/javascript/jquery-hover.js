$(document).ready(function(){
	$(".swapImage img").hover(
		function() {
            if(!$(this).hasClass("disableSwap")){
    			this.src = this.src.replace("-0","-1");
            }
		},
		function() {
            if(!$(this).hasClass("disableSwap")){
    			this.src = this.src.replace("-1","-0");
            }
		}
	);
});