Rotator = $(window).load(function (){
	
	var Ret = {},
		currentSlide,
		container,
		imageSize,
		rotator_interval,
		rotate_timer = 5500;
		
	function on_window_load(){
		$(".prev").on("click",on_click_prev);
		$(".next").on("click",on_click_next);
		
		container = $("#slideshowWrap");
		display_item(container.find("LI:first-child"));
		$(container).css("display", "block");
		// imageSize = container.find("#slideshow IMG:visible").height();
		// $(container).css('height', imageSize);
	}

	function on_click_prev(e){
		var item = currentSlide.prev("LI");
		if(item.length === 0){
			item = container.find("LI:last-child");
		};
		container.find("LI:first-child").appendTo('#slideshow UL');
		display_item(item);
		e.preventDefault();
	}
	
	function on_click_next(e){
		var item = currentSlide.next("LI");
		if(item.length === 0){
			item = container.find("LI:first-child");
		};
		
		container.find("LI:last-child").prependTo('#slideshow UL');
		display_item(item);
		e.preventDefault();
	}

	function display_item(item){
		//container.find("LI").not(item).fadeOut(500);

		//item.fadeIn(500);
		currentSlide = item;
		
		clearInterval(rotator_interval);
		rotator_interval = setInterval(function(){
			var item = currentSlide.next("LI");
			if(item.length === 0){
				item = container.find("LI:first-child");
			};
			display_item(item);
		}, rotate_timer);
	}
	
	$(on_window_load);
	
	// $(window).resize(function () {
	// 		imageSize = container.find("#slideshow IMG:visible").height();
	// 		$(container).css('height',imageSize);
	// });

	
	Ret.display_item = display_item;
	return Ret;
	
});

//if slideshow contains only one image, remove left & right arrows

	$(document).ready(function(){
		
		if ($('#slideshow').find('img').size() === 1) {

			$('#slideshowNav').hide();
		}
});


