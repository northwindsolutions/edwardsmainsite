var EdwardsApp = null;
function getEdwardsNgApp() {
    if (typeof (angular) !== 'undefined') {
        return angular.module('EdwardsApp', []);
    }
}
EdwardsApp = getEdwardsNgApp();

//clear fields
function clearMe(formfield, defaultValue) {
	if ((defaultValue == null) || (defaultValue == undefined))
		defaultValue = formfield.defaultValue;
	if (formfield.value == defaultValue)
		formfield.value = "";
}
function restoreMe(formfield, defaultValue) {
	if ((defaultValue == null) || (defaultValue == undefined))
		defaultValue = formfield.defaultValue;
	if (formfield.value.replace(/^\s+|\s+$/g, "") == "")
		formfield.value = defaultValue;
}

function maxHeight(elements) {
	if (elements && elements.length > 0) {
		var max = 0;
		elements.each(function () {
			var el = $(this);
			var height = el.height();
			max = height > max ? height : max;
		});
		return max;
	}
}

jQuery(function($) {

  var win = $(window);

    // Correct iframes that are marked as auto-height.
    $("iframe.autoheight").each(function() {
        var iframe = this;
        $(iframe).load(function () {
            iframe.height = iframe.contentWindow.document.body.scrollHeight + 35;
        });
    });

   $('iframe.autowidth').load( function() {
    $(this).contents().find("head")
      .append($("<style type='text/css'>img#backbuffer { height:auto !important; width:100% !important; transform:none !important; margin-top: 100px !important;}</style>"));
   });

    //simulate dropdown menu for language select
    var dropdown = $('#language .dropdown');
    $(dropdown).on('click','LI:first',function(e){
        e.preventDefault();

        if (!$(dropdown).hasClass('active')){
            $(dropdown).addClass('active');
            return false;
        }

        $(this).prependTo(dropdown.find('ul'));
        $(dropdown).removeClass('active');
    });


    //mobile menu toggles + desktop search toggle (disabled/enabled at 768 wide)
    if (document.body.offsetWidth * 1 < 769) {
        $('#mobileWrap .toggle').on('click',function(e){
            e.preventDefault();

            var $toggle = $('#mobileWrap .toggle');

            if (!$(this).hasClass('selected')){
                $toggle.removeClass('selected')
                $toggle.next().hide();
                $toggle.next().children('.nav').hide();

                $(this).addClass('selected'); //.next().slideDown();
                $(this).next().slideDown();
                $(this).next().children('.nav').slideDown();
                return false;
            }
            $toggle.removeClass('selected');
            $toggle.next().slideUp();
            $toggle.next().children('.nav').slideUp();
        });
    } else {
        $('#search .toggle').on('click',function(e){
            $(this).next('.field').toggleClass('active');
            e.preventDefault();
        });
    }

    var search = $("#search");
    function goSearch(q) {
        if (!q || q === 'Search') {
            return;
        }
        window.location = '/search/?q=' + encodeURIComponent(q);
    }
    var searchInput = search.find('input.ip');
    search.on('keypress', 'input.ip', function (e) {
        var val = searchInput.val();
        if (e.keyCode === 13) {
            goSearch(searchInput.val());
            e.preventDefault();
            e.stopPropagation();
        }
    });
    search.on('click', '.btn input', function (e) {
        goSearch(searchInput.val());
        e.preventDefault();
        e.stopPropagation();
    });

    $("#topNavigation .nav > li.subnav").each(function () {
        var items = $(this).find('ul.subnav');
        if (items.length === 0) {
            $(this).removeClass('subnav');
        }
    });

    //mobile nav accordion
    $("#topNavigation .nav .navIcon").on('click', function (e) {
        var thisNavIcon = $(e.currentTarget);
        var currentMenu = thisNavIcon.closest('li.subnav');
        var siblingMenus = currentMenu.siblings('li.subnav');

        // If there's no such thing as sub navs on this menu, then don't
        if (currentMenu.find("> ul.subnav").length === 0) {
            e.stopPropagation();
            e.preventDefault();
            return;
        }

        // close all siblings of this menu and their children.
        siblingMenus.each(function () {
            var $this = $(this);
            var subNavs = $this.find('ul.subnav').filter(":visible");
            subNavs.slideUp();
            $this.removeClass('selected');
        });

        // If this menu's submenu is already visible, then close it
        if (currentMenu.find('ul.subnav').is(":visible")) {
            currentMenu.removeClass('selected');
            currentMenu.find("ul.subnav").slideUp();
        } else {
            // Make this one visible since we clicked on it.
            currentMenu.addClass("selected");
            // only show the first subnav.
            currentMenu.find("> ul.subnav").slideToggle();
        }

        e.stopPropagation();
        e.preventDefault();
    });

    //video resize
    var $allVideos = $("iframe");
    $allVideos.each(function() {
        $(this)
			.data('aspectRatio', this.height / this.width)
			.removeAttr('height')
			.removeAttr('width');
    });
    $(window).resize(function() {
        $allVideos.each(function() {
            var $el = $(this);
            var newWidth = $(this).parent().width();
            $el
				.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));
        });
    }).resize();
    $(window).load(function () {
        $allVideos.each(function () {
            var $el = $(this);
            var newWidth = $(this).parent().width();
            $el
				.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));
        });
    }).resize();

    //accordion
    function init_expandable_item(index, element)
    {
        $(element).prev(".toggle").on('click', on_click_accordion_link);
    }

    function on_click_accordion_link(e) {
        var target = $(e.currentTarget);
        var nextExpandable = $(e.currentTarget).next(".expandable");
        if (!nextExpandable.is(":visible")) {
            //$(".expandable").slideUp();
            //$(".toggle").removeClass("selected");
            nextExpandable.slideToggle();
            target.addClass("selected");
        } else {
            nextExpandable.slideUp();
            target.removeClass('selected');
        }

        $(window).load(function () {
            $allVideos.each(function () {
                var $el = $(this);
                var newWidth = $(this).parent().width();
                $el
					.width(newWidth)
					.height(newWidth * $el.data('aspectRatio'));
            });
        }).resize();

        e.preventDefault();
        return false;
    }

    $(".toggle + .expandable").each(init_expandable_item);
    $(".news-list").on("click", ".toggle", on_click_accordion_link);
    $("#faqController").on("click", ".toggle", on_click_accordion_link);

    // $('.keywordSearch .primary > A').on('click', function(e){
    // 	e.preventDefault();
    // 	$('.keywordSearch .primary > A').removeClass('selected');
    // 	if(!$(this).hasClass('selected')){
    // 		$(this).addClass('selected');
    // 	}
    // 	$('.keywordSearch .secondary').html($(this).next('.section').children().clone());
    // });

    //tabs
    var tabbedBtns = $('ul.tabs li');
    var tabbedContent = $('.section');

    $(tabbedBtns).click( function(){
        $(tabbedBtns).removeClass('selected');
        $(this).addClass('selected');
        $('.section:visible').addClass("hide");

        var currentItem = $(tabbedBtns).index($(this));
        $(tabbedContent[currentItem]).removeClass("hide");
    });

    //market rotator
    var holder = $('#marketSlideshow');
    //speed = 5000

    //setInterval(changePosition, speed);

    $('#marketContent').html($('#marketSlideshow .selected .copy').html());

    function rotateContent() {
        $('#marketContent').html($('#marketSlideshow .selected .copy').html());
    }

    function slideLeft(e) {
        e.preventDefault();
        var slides = $('#marketSlideshow .slide');
        slides.removeClass('selected').eq(3).addClass('selected');

        holder.find('div').first().appendTo(holder);

        rotateContent();
    }

    function slideRight(e) {
        e.preventDefault();
        var slides = $('#marketSlideshow .slide');
        slides.removeClass('selected').eq(1).addClass('selected');

        holder.find('div').last().prependTo(holder);

        rotateContent();
    }

    $('.marketPrev').on('click',slideLeft);
    $('.marketNext').on('click',slideRight);

    //    function resizeSlideshow() {
    //    	var maxHeight = 0;
    //    	$("#slideshow li").each(function () {
    //    		var height = $(this).find('img').height();
    //    		var desc = $(this).find('.desc');
    //    		if (desc.length > 0) {
    //    			height += desc.height();
    //    		}
    //    		if (height > maxHeight) {
    //    			maxHeight = height;
    //    		}
    // 		});
    // 		$("#slideshowWrap").css('height', maxHeight);
    //    }
    // $(window).resize(resizeSlideshow);

    (function () {

        $("#homeToggle .homeShow").on('click', function (e) {
            e.preventDefault();
            $("#homeToggle a").removeClass('selected');
            $(this).addClass('selected');
            $("#showHide").slideDown();
        });

        $("#homeToggle .homeHide").on('click', function (e) {
            e.preventDefault();
            $("#homeToggle a").removeClass('selected');
            $(this).addClass('selected');
            $("#showHide").slideUp();
        });

        var imagesLoadedAvailable = typeof ($("BODY").imagesLoaded) === 'function',
            homeSlideshow = $("#homeSlideshow"),
            slideshow = $("#slideshow"),
            productSlideshow = $("#productSlideshow");

        function homeSlideshowLoad() {
          homeSlideshow.fadeIn().jCarouselLite({
            circular: true,
            autoWidth: true,
            responsive: true,
            visible: 1,
            speed: 300,
            pause: true,
            swipe: false,
            btnPrev: '.homePrev',
            btnNext: '.homeNext',
            init: function () {
              var max = -1;
              var li = $(this).find('li');
              li.each(function () {
                var h = $(this).find('img').height();
                max = h > max ? h : max;
              });
              //$('#homeSlideshow li').css('height', max);
              //$('#homeSlideshow li').css("display", "table");
              $('#homeSlideshow li').css('min-height', max);
            }
          });
        }

        homeSlideshow.hide();
        
        if (imagesLoadedAvailable) {
            homeSlideshow.imagesLoaded(homeSlideshowLoad);
        }
        else {
          win.on('load', homeSlideshowLoad);
        }

        function slideshowLoaded() {
            slideshow.fadeIn().jCarouselLite({
                swipe: false,
                circular: true,
                autoWidth: true,
                responsive: true,
                visible: 1,
                speed: 300,
                btnPrev: '.prev',
                btnNext: '.next',
                init: function () {
                    var li = $(this).find('li');
                    li.css('display', 'block');
                    var max = -1;
                    li.each(function () {
                        var h = $(this).height();
                        max = h > max ? h : max;
                    });
                    $('#slideshow li').css('min-height', max);
                }
            });
        }

        if (imagesLoadedAvailable) {
          slideshow.hide();
          slideshow.imagesLoaded(slideshowLoaded);
        }
        else {
          win.on('load', slideshowLoaded);
        }

        function productSlideshowLoaded() {
            productSlideshow.fadeIn().jCarouselLite({
                swipe: false,
                circular: true,
                autoWidth: true,
                responsive: true,
                visible: 2,
                speed: 300,
                pause: true,
                btnPrev: '.productPrev',
                btnNext: '.productNext',
                init: function () {
                    var li = $(this).find('li');
                    var max = -1;
                    li.each(function () {
                        var h = $(this).height();
                        max = h > max ? h : max;
                    });
                    $('#productSlideshow li').css('min-height', max);
                }
            });
        }
        if (imagesLoadedAvailable) {
          productSlideshow.hide();
          productSlideshow.imagesLoaded(productSlideshowLoaded);
        }
        else {
          win.on('load', productSlideshowLoaded);
        }


    })();

    var iconBarLinks = $("#iconBar a");
    var max = maxHeight(iconBarLinks);
    max > 0 && iconBarLinks.height(max);

    //font resize
    dw_fontSizerDX.setDefaults( 'px', 14, 14, 18, ['#innerContent', '#home'] );
    dw_Event.add( window, 'load', dw_fontSizerDX.init );

    // Fancybox
    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    $(".fancybox-pdf").fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        type: 'iframe',
        iframe: {
            preload: false // fixes issue with iframe and IE
        }
    });

    if (navigator.userAgent.match(/Android/i)
		 || navigator.userAgent.match(/webOS/i)
		 || navigator.userAgent.match(/iPhone/i)
		 || navigator.userAgent.match(/iPod/i)
		 || navigator.userAgent.match(/iPad/i)
		 || navigator.userAgent.match(/BlackBerry/i)
		 || navigator.userAgent.match(/Windows Phone/i)
		 ) {
        $('#zoom_01').hide();
    } else {
        $('#zoom_02').hide();
        $('#zoom_01').elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750
        });
    }


	if (document.getElementById('twitterFeed')) {
	    twitterFetcher.fetch('430793841260511232', 'twitterFeed', 3, true, false, true);
	}

});
