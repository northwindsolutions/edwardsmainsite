﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Organization;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
//using Ektron.Cms.API;
using Ektron.Cms.Framework.Organization;
using Edwards.Utility;
using Ektron.Cms.Framework.UI;
using System.Text.RegularExpressions;

namespace Edwards.MasterPage
{
    public partial class MainMaster : System.Web.UI.MasterPage
    {
        public string HomepageUrl { get; set; }
        public readonly int JapaneseId = 1041;


        private long headerMenuId = Constants.MenuIds.MainHeaderMenu;
        private long footerMenuId = Constants.MenuIds.MainFooterMenu;

        public string YouTubeUrl = Constants.SocialUrl.YouTube;
        public string FacebookUrl = Constants.SocialUrl.Facebook;
        public string TwitterUrl = Constants.SocialUrl.Twitter;
        public string LinkedInUrl = Constants.SocialUrl.Linkedin;
        public string YouKuUrl = Constants.SocialUrl.YouKu;

        public enum languageSubDomains
        {
            cn = 0,
            de = 1,
            fr = 2,
            es = 3
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            LoadMenus();

            // Register the PageBuilder edit mode stylesheet to fix CSS rules that conflict in edit mode.
            var pb = Page as PageBuilder;
            if (pb != null && pb.Status == Mode.Editing)
            {
                Ektron.Cms.API.Css.RegisterCss(Page, @"/library/styles/stylesheets/pagebuilder.css", "PBCorrections");
            }

            analyticsPlaceholder.Visible = Constants.DeploymentMode == Constants.DeploymentModes.Prod;
        }

        public static Int32 lcidFromTSubomain(string subDomain)
        {

            switch (subDomain)
            {
                default:
                case "com":
                    return 1033;
                case "de":
                    return 1031;
                case "es":
                    return 2058;
                case "cn":
                    return 2052;
            }
        }
        
        private string buildURl(int languageId, languageSubDomains domain)
        {
            return string.Format("{0}://{1}/{2}/home/?langtype={3}", Request.Url.Scheme, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url.Host.Split('.')[0], languageId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResourcePackages();
	    
		int pagelang = 1033;
		Ektron.Cms.Framework.Content.ContentManager contentManager = new Ektron.Cms.Framework.Content.ContentManager();

            //if current diff to target
        languageSubDomains domain = (languageSubDomains)Enum.Parse(typeof(languageSubDomains), HttpContext.Current.Request.Url.Host.Split('.')[0]);
        if (!string.IsNullOrEmpty(domain.ToString())) 
        {
                pagelang = lcidFromTSubomain(domain.ToString());
                if (pagelang != Constants.CurrentLanguageId)
                {
                    Response.Redirect(buildURl(pagelang, domain));
                }            
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["langtype"]))
		{
			pagelang = Convert.ToInt32(Request.QueryString["langtype"]);
		}
		if (!String.IsNullOrEmpty(Request.QueryString["pageid"])){
			Ektron.Cms.API.Metadata mapi = new Ektron.Cms.API.Metadata();
			mapi.ContentLanguage = pagelang;
			Ektron.Cms.CustomAttributeList aList = new Ektron.Cms.CustomAttributeList();
			aList = mapi.GetContentMetadataList(Convert.ToInt64(Request.QueryString["pageid"]));
			Page.Title = aList["Title"].Value.ToString();
		}
		else if (!String.IsNullOrEmpty(Request.QueryString["id"])){
			Ektron.Cms.API.Metadata mapi = new Ektron.Cms.API.Metadata();
			mapi.ContentLanguage = pagelang;
			Ektron.Cms.CustomAttributeList aList = new Ektron.Cms.CustomAttributeList();
			aList = mapi.GetContentMetadataList(Convert.ToInt64(Request.QueryString["id"]));
			Page.Title = aList["Title"].Value.ToString();
		}else{ 		
			Page.Title = "Edwards Vacuum";
		}

            // Set the url for the homepage icon with alias and language
            int currentLanguageId = Constants.CurrentLanguageId;
            long homeId = 32;

            AliasManager aliasManager = new AliasManager();
            AliasData aliasData = aliasManager.GetAlias(homeId, currentLanguageId, EkEnumeration.TargetType.Content);

            HomepageUrl = (aliasData != null) ? "/" + aliasData.Alias + ((currentLanguageId != 1033) ? "?langtype=" + currentLanguageId : "") : string.Empty;

            // Custom styling needed for Japanese menu and Contact link
            if (currentLanguageId == JapaneseId)
            {
                contactLink.Attributes.Add("style", "font-size: .8em;");
            }

            // Show YouKu icon in footer if Chinese, else always show YouTube icon
            if (currentLanguageId == 2052)
            {
                YouKuIconLit.Visible = true;
                YouKuIconLit.Text = "<a href=" + YouKuUrl + " class='swapImage' target='_blank'><img src='/library/images/common/youku-0.png' alt='YouKu' title='YouKu'></a>";
            }
            else
            {
                YouTubeIconLit.Visible = true;
                YouTubeIconLit.Text = "<a href='" + YouTubeUrl + "' class='swapImage' target='_blank'><img src='/library/images/common/youtube-0.png' alt='YouTube' title='YouTube'></a>";
            }
        }

        private void RegisterResourcePackages()
        {
            var api = new Ektron.Cms.CommonApi();

            var javascriptPackage = new Package
            {
                Components = new List<Component>
                {
                    Packages.EktronCoreJS,
                    JavaScript.Create(api.SitePath + "library/javascript/imagesloaded.pkgd.min.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/jquery.jcarousellite.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/text-resize.min.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/jquery-hover.js"),
                    JavaScript.Create(api.SitePath + "library/fancybox/source/jquery.fancybox.pack.js"),
                    JavaScript.Create(api.SitePath + "library/fancybox/source/helpers/jquery.fancybox-media.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/twitter-fetcher.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/jquery.elevateZoom-3.0.8.min.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/scripts.js")
                }
            };
            
            javascriptPackage.Register(this, true);

            var cssPackage = new Package
            {
                Components = new List<Component>
                {
                    Css.Create(api.SitePath + "library/styles/stylesheets/screen.css", BrowserTarget.All, "screen, projection"),
                    Css.Create(api.SitePath + "library/styles/stylesheets/print.css", BrowserTarget.All, "print"),
                    Css.Create(api.SitePath + "library/fancybox/source/jquery.fancybox.css", BrowserTarget.All, "screen")
                }
            };

            cssPackage.Register(this, false);
        }

        private void LoadMenus()
        {
            var cacheKey = string.Format("MainMaster-LoadMenus-headerMenuId-{0}", headerMenuId);
            var md = Constants.BackendApiCacheInterval > 0 ? HttpContext.Current.Cache[cacheKey] as string : null;
            if (md != null)
            {
                uxMenuListView.Visible = false;
                uxMenuListViewOutput.Text = md;
            }

            var cacheKeyFooter = string.Format("MainMaster-LoadMenus-footerMenuId-{0}", footerMenuId);
            var fmd = Constants.BackendApiCacheInterval > 0 ? HttpContext.Current.Cache[cacheKeyFooter] as string : null;
            if (fmd != null)
            {
                uxFooterListView.Visible = false;
                uxFooterListViewOutput.Text = fmd;
            }

            if (md == null || fmd == null)
            {
                var menuManager = new MenuManager();

                if (md == null)
                {
                    // Load the top menu navigation...
                    IMenuData menuBaseData = menuManager.GetTree(headerMenuId);
                    if (menuBaseData != null)
                    {
                        uxMenuListView.DataSource = menuBaseData.Items;
                        uxMenuListView.DataBind();
                        var output = DynamicControls.RenderControl(uxMenuListView);
                        if (output != null)
                        {
                            if (Constants.BackendApiCacheInterval > 0)
                            {
                                HttpContext.Current.Cache.Add(cacheKey, output, null, DateTime.MaxValue,
                                    new TimeSpan(0, 0, Constants.BackendApiCacheInterval), CacheItemPriority.Normal, null);
                            }
                        }
                    }
                }
                if (fmd == null)
                {
                    // Load the footer menu navigation...
                    IMenuData footerBaseData = menuManager.GetTree(footerMenuId);
                    if (footerBaseData != null)
                    {
                        uxFooterListView.DataSource = footerBaseData.Items;
                        uxFooterListView.DataBind();
                        var output = DynamicControls.RenderControl(uxFooterListView);
                        if (output != null)
                        {
                            if (Constants.BackendApiCacheInterval > 0)
                            {
                                HttpContext.Current.Cache.Add(cacheKey, output, null, DateTime.MaxValue,
                                    new TimeSpan(0, 0, Constants.BackendApiCacheInterval), CacheItemPriority.Normal, null);
                            }
                        }
                    }
                }
            }
        }
    }
}
