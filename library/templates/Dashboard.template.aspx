﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="dashboard.template.aspx.cs" Inherits="library_templates_dashboard_template" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
     <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
   <div id="dashboard" class="containerWrap">

    <%-- Slideshow --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="topSliderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
         <div class="title_welcome"><h2><strong><asp:Literal ID="litwelcome" runat="server"></asp:Literal></strong></h2></div>
         <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="General Announcement" />
    <DZ:DropZone runat="server" ID="DropZone1" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    <%-- Middle Callouts --%>
    <div class="one-half">
       
                  <div class="full-width" id="myAccountFavorites">
            <h2><strong><asp:Literal ID="litwelocme" runat="server"></asp:Literal></strong></h2>
                <div id="favoriteMarkets">
                    <h3><%= GetGlobalResourceObject("EdwardsPortal", "HompageMarket") %></h3>
                     <ul class="featuredPages">
                         <li id="emptyMarketsMessage" runat="server" visible="false">
                             <asp:Literal ID="litemptyMarket" runat="server" Visible="false"></asp:Literal>
                        </li>
                         <asp:HiddenField ID="prevClassValue" runat="server" />
                    <asp:Repeater ID="marketsRepeater" runat="server" OnItemDataBound="HandleFeaturedItemDataBound" Visible="false">
                        <HeaderTemplate>
                           
                        </HeaderTemplate>
                        <ItemTemplate>
                             <%#((bool)Eval("IsFavorite"))
                            ?BuildMarketHtml(Eval("Url").ToString(),Eval("Title").ToString()):""%>
                        </ItemTemplate>
                         <FooterTemplate>
                            
                        </FooterTemplate>
                    </asp:Repeater>
                        </ul>

                    </div>
              <div class="full-width" id="favoriteProducts">
                    <h3><%= GetGlobalResourceObject("EdwardsPortal", "HompageProducts") %></h3>
                  <ul class="featuredPages">
                      <li id="emptyProductsMessage" runat="server" visible="false">
                          <asp:Literal ID="litemptyProd" runat="server" Visible="false"></asp:Literal>
                      </li>
                      <asp:HiddenField ID="prevClassprod" runat="server" />
                    <asp:Repeater ID="productsRepeater" runat="server" OnItemDataBound="HandleFeaturedItemDataBound" Visible="false">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                              <%#((bool)Eval("IsFavorite"))
                            ?BuildProductHtml(Eval("Url").ToString(),Eval("Title").ToString()):""%>
                        </ItemTemplate>
                        <FooterTemplate>
                            
                        </FooterTemplate>
                    </asp:Repeater>
                       </ul>
                </div>
             </div>
        
        <div id="iconBar">
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Middle Callout" />
            <div class="one-half">
                 <DZ:DropZone runat="server" ID="middleCalloutDropzone1" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
       <div class="one-half omega">
        <DZ:DropZone runat="server" ID="middleCalloutDropzone2" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
           </div>
         </div>
    </div>
    <div class="one-half omega">
        <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Middle Right Callout" />
        <DZ:DropZone runat="server" ID="middlerightCalloutDropzone1" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        <DZ:DropZone runat="server" ID="middlerightCalloutDropzone2" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    </div>
    <br class="clear" />
</div>
    <asp:HiddenField ID="viewmoreTxt" runat="server" ClientIDMode="Static" />
</asp:Content>

