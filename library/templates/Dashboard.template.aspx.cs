﻿using Edwards.Portal;
using Edwards.Portal.Repositories;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Organization;
using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class library_templates_dashboard_template : AuthenticatedPageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly UserService _userService;
    private readonly FeaturedPageRepository _featuredPageRepository;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;
    public library_templates_dashboard_template()
    {
        this._authenticationService = new AuthenticationService();
        this._userService = new UserService();
        this._featuredPageRepository = new FeaturedPageRepository();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }
    protected MyAccountModel Model { get; private set; }
    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAuthentication(true);
        viewmoreTxt.Value = this.GetGlobalResourceObject("EdwardsPortal", "ViewMore").ToString();
        this.BindModelToView(this.InitializeModel());

        Css.Register(this, new Ektron.Cms.CommonApi().SitePath + "library/styles/stylesheets/salesrep.css", BrowserTarget.All, false);        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        PortalUser currentUser = this.CurrentUser;
        litwelcome.Text = this.GetGlobalResourceObject("EdwardsPortal", "HomePageWelcome").ToString()+", "+currentUser.FirstName+"!";
    }


    public override void Error(string message)
    {
        jsAlert(message);
    }

    public override void Notify(string message)
    {
        jsAlert(message);
    }

    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }
    private MyAccountModel InitializeModel()
    {
        return new MyAccountModel
        {
            User = this.PrepareUserModel(),
            FeaturedMarkets = this.PrepareFeaturedMarketsModel(),
            FeaturedProducts = this.PrepareFeaturedProductsModel()
        };
    }
    private void BindModelToView(MyAccountModel model)
    {
        this.Model = model;
        marketsRepeater.DataSource = model.FeaturedMarkets;
        marketsRepeater.DataBind();
        productsRepeater.DataSource = model.FeaturedProducts;
        productsRepeater.DataBind();

        long marketItemCount = TotalItems(model.FeaturedMarkets);
        long productItemCount = TotalItems(model.FeaturedProducts);
        marketsRepeater.Visible = true;
        productsRepeater.Visible = true;
        if (marketItemCount == 0)
        {
            emptyMarketsMessage.Visible = true;
            litemptyMarket.Visible = true;
            litemptyMarket.Text = this.GetGlobalResourceObject("EdwardsPortal", "SelectMarketItems").ToString();
        }
    
        if (productItemCount == 0)
        {
            emptyProductsMessage.Visible = true;
            litemptyProd.Visible = true;
            litemptyProd.Text = this.GetGlobalResourceObject("EdwardsPortal", "SelectproductItems").ToString();
        }
    }

    private long TotalItems(IEnumerable<FeaturedPage> model)
    {
        long itemCount = 0;
        foreach (var item in model)
        {
            if (item.IsFavorite)
            {
                itemCount++;
            }
        }
        return itemCount;
    }
    protected void ToMyAccount(object sender, EventArgs e)
    {
        this.RouteToMyAccount();
    }

    private void RouteToMyAccount()
    {
        var route = this._routeBuilder.MyAccount();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private PortalUser PrepareUserModel()
    {
        return this.CurrentUser;
    }

    private IEnumerable<FeaturedPage> PrepareFeaturedMarketsModel()
    {
        return this._featuredPageRepository.GetMarketPages(this.CurrentUser, Constants.CurrentLanguageId);
    }

    private IEnumerable<FeaturedPage> PrepareFeaturedProductsModel()
    {
        return this._featuredPageRepository.GetProductPages(this.CurrentUser, Constants.CurrentLanguageId);
    }

    protected class MyAccountModel
    {
        public PortalUser User { get; set; }
        public IMenuData Menu { get; set; }
        public IEnumerable<FeaturedPage> FeaturedMarkets { get; set; }
        public IEnumerable<FeaturedPage> FeaturedProducts { get; set; }
    }
    protected void HandleFeaturedItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var listItem = e.Item.FindControl("featuredItem") as HtmlControl;

        if (listItem != null)
        {
            string classValue = "one-fourth";

            if ((e.Item.ItemIndex + 1) % 4 == 0)
            {
                classValue += " omega";
            }

            listItem.Attributes.Add("class", classValue);
        }
    }

    protected string BuildMarketHtml(string url, string title)
    {
        string currentClass = prevClassValue.Value == string.Empty ? "one-half" : (prevClassValue.Value == "one-half" ? "omega omega-left" : "one-half");
        prevClassValue.Value = currentClass;
        return string.Format("<li class=\"{2}\"><a href=\"{0}\">{1}</a></li>", url, title, currentClass);

    }
    protected string BuildProductHtml(string url, string title)
    {
        string currentClass = prevClassprod.Value == string.Empty ? "one-half" : (prevClassprod.Value == "one-half" ? "omega omega-left" : "one-half");
        prevClassprod.Value = currentClass;
        return string.Format("<li class=\"{2}\"><a href=\"{0}\">{1}</a></li>", url, title, currentClass);
    }
}