﻿using Edwards.Portal;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.PageBuilder;
using System;
using System.Collections.Generic;

public partial class library_templates_Portal : System.Web.UI.MasterPage
{
    private readonly AuthenticationService _authenticationService;
    private readonly RouteBuilder _portalRouteBuilder;

    public library_templates_Portal()
    {
        this.YouTubeUrl = Constants.SocialUrl.YouTube;
        this.FacebookUrl = Constants.SocialUrl.Facebook;
        this.TwitterUrl = Constants.SocialUrl.Twitter;
        this.LinkedInUrl = Constants.SocialUrl.Linkedin;
        this.YouKuUrl = Constants.SocialUrl.YouKu;
        this.YammerUrl = Constants.SocialUrl.Yammer;

        this._authenticationService = new AuthenticationService();
        this._portalRouteBuilder = new RouteBuilder();
    }

    protected string YouTubeUrl { get; set; }
    protected string FacebookUrl { get; set; }
    protected string TwitterUrl { get; set; }
    protected string LinkedInUrl { get; set; }
    protected string YouKuUrl { get; set; }
    protected string YammerUrl { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        RegisterResourcePackages();
        
        if (this.NeedsPageBuilderStyleCorrections())
        {
            this.IncludePageBuilderStyleCorrections();
        }

        this.SetHomeLink();

        analyticsPlaceholder.Visible = Constants.DeploymentMode == Constants.DeploymentModes.Prod;
    }

    private void SetHomeLink()
    {
        PortalUser currentUser = this._authenticationService.GetCurrentUser();

        PortalRoute homeRoute;

        if (this._authenticationService.IsAuthenticated(currentUser))
        {
            homeRoute = _portalRouteBuilder.DashBoard();
            RenderYammerIcon();
        }
        else
        {
            homeRoute = _portalRouteBuilder.Login();
        }

        if (homeRoute.SupportsLanguage(Constants.CurrentLanguageId))
        {
            homeLink.HRef = homeRoute.Get(Constants.CurrentLanguageId);
        }
        else if (homeRoute.SupportsLanguage(Constants.DefaultLanguageId))
        {
            homeLink.HRef = homeRoute.Get(Constants.DefaultLanguageId);
        }
        else
        {
            homeLink.HRef = "/";
        }
    }

    private void RegisterResourcePackages()
    {
        var api = new Ektron.Cms.CommonApi();

        var javascriptPackage = new Package
        {
            Components = new List<Component>
            {
                Packages.EktronCoreJS,
                JavaScript.Create(api.SitePath + "library/javascript/jquery.jcarousellite.js"),
                JavaScript.Create(api.SitePath + "library/javascript/text-resize.min.js"),
                JavaScript.Create(api.SitePath + "library/javascript/jquery-hover.js"),
                JavaScript.Create(api.SitePath + "library/fancybox/source/jquery.fancybox.pack.js"),
                JavaScript.Create(api.SitePath + "library/fancybox/source/helpers/jquery.fancybox-media.js"),
                JavaScript.Create(api.SitePath + "library/javascript/twitter-fetcher.js"),
                JavaScript.Create(api.SitePath + "library/javascript/customscripts.js"),
                JavaScript.Create(api.SitePath + "library/javascript/scripts.js"),
                JavaScript.Create(api.SitePath + "library/javascript/jquery.elevateZoom-3.0.8.min.js")               
            }
        };

        javascriptPackage.Register(this, true);

        

        var cssPackage = new Package
        {
            Components = new List<Component>
            {
                Css.Create(api.SitePath + "library/styles/stylesheets/screen.css", BrowserTarget.All, "screen, projection"),
                Css.Create(api.SitePath + "/library/styles/stylesheets/print.css", BrowserTarget.All, "print"),
                Css.Create(api.SitePath + "/library/fancybox/source/jquery.fancybox.css", BrowserTarget.All, "screen"),
                Css.Create(api.SitePath + "/library/styles/stylesheets/customstyles.css", BrowserTarget.All)
            }
        };

        cssPackage.Register(this, false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.RenderSocialIcons();
    }

    private bool NeedsPageBuilderStyleCorrections()
    {
        var pageBuilderPage = this.Page as PageBuilder;
        return pageBuilderPage != null && pageBuilderPage.Status == Mode.Editing;
    }

    private void IncludePageBuilderStyleCorrections()
    {
        Ektron.Cms.API.Css.RegisterCss(this.Page, @"/library/styles/stylesheets/pagebuilder.css", "PBCorrections");
    }

    private void RenderSocialIcons()
    {
        if (this.DisplayYouKuIcon())
        {
            this.RenderYouKuIcon();
        }
        else
        {
            this.RenderYouTubeIcon();
        }
    }

    private void RenderYouTubeIcon()
    {
        YouTubeIconLit.Visible = true;
        YouTubeIconLit.Text = "<a href='" + YouTubeUrl + "' class='swapImage' target='_blank'><img src='/library/images/common/youtube-0.png' alt='YouTube' title='YouTube'></a>";
    }

    private void RenderYouKuIcon()
    {
        YouKuIconLit.Visible = true;
        YouKuIconLit.Text = "<a href='" + YouKuUrl + "' class='swapImage' target='_blank'><img src='/library/images/common/youku-0.png' alt='YouKu' title='YouKu'></a>";
    }

    private void RenderYammerIcon()
    {
        YammerIconLit.Visible = true;
        YammerIconLit.Text = "<a href='" + YammerUrl + "' class='swapImage' target='_blank'><img src='/library/images/common/yammer-0.png' alt='Yammer' title='Yammer'></a>";
    }

    private bool DisplayYouKuIcon()
    {
        return Constants.CurrentLanguageId == 2052;
    }
}
