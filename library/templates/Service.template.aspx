﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="Service.template.aspx.cs" Inherits="Service_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
        <Edwards:BreadCrumbs runat="server" />
        
        <%-- Introduction Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <div class="three-fourth">
            <%-- Middle Left (3/4) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Middle Left (3/4)" />
            <DZ:DropZone runat="server" ID="MiddleDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div id="innerCallouts" class="one-fourth omega">
            <%-- Middle Right (1/4) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Middle Right (1/4)" />
            <DZ:DropZone runat="server" ID="RightCalloutsDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
    </div>
</div>

</asp:Content>

