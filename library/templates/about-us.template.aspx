﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="about-us.template.aspx.cs" Inherits="library_templates_about_us_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
        <Edwards:BreadCrumbs runat="server" ID="breadCrumbs" />
        
        <%-- Introduction Text Dropzone --%>
        <div class="three-fourth">
            <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
            <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        
        <div class="three-fourth">
            <%-- Top Left (3/4) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Top Left (3/4)" />
            <DZ:DropZone runat="server" ID="TopThreeFourthsDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div class="one-fourth omega">
            <%-- Top Right (1/4) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Top Right (1/4)" />
            <DZ:DropZone runat="server" ID="TopOneFourthDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <br class="clear">

        <div class="one-third">
            <%-- Middle Left (1/3) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Middle Left (1/3)" />
            <DZ:DropZone runat="server" ID="MiddleOneThirdDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div class="two-thirds omega">
            <%-- Middle Right (2/3) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel5" runat="server" Label="Middle Right (2/3)" />
            <DZ:DropZone runat="server" ID="MiddleTwoThirdDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <br class="clear">


        <div class="three-fourth">
            <%-- Bottom Right (3/4) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel7" runat="server" Label="Bottom Right (3/4)" />
            <DZ:DropZone runat="server" ID="BottomThreeFourthDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

    <div class="one-fourth omega">
            <%-- Bottom Left (1/4) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Bottom Left (1/4)" />
            <DZ:DropZone runat="server" ID="BottomOneFourthDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <br class="clear">
    </div>
</div>

</asp:Content>