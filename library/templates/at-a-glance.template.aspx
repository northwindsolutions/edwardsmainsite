﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="at-a-glance.template.aspx.cs" Inherits="at_a_glance_templates_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/LeftNavigation.ascx" tagPrefix="Edwards" tagName="LeftNavigation" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
<Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="leftNavigation">
        <Edwards:LeftNavigation ID="LeftNav1" runat="server" />
    </div>

    <div id="innerContent" class="three-col" itemprop="owns" itemscope itemtype="http://schema.org/Product">
        <Edwards:BreadCrumbs runat="server" ID="breadCrumbs" />
        
        <%-- Main Body Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Main Body" />
        <DZ:DropZone runat="server" ID="MainBodyDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <div class="one-half">
            <%-- Bottom Left Callout Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Bottom Left Callout" />
            <DZ:DropZone runat="server" ID="BottomLeftCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div class="one-half omega">
            <%-- Bottom Right Callout Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Bottom Right Callout" />
            <DZ:DropZone runat="server" ID="BottomRightCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

    </div>
    <div id="innerCallouts">
        <%-- Right Column Callout Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel5" runat="server" Label="Right Side Collout" />
        <DZ:DropZone runat="server" ID="RightCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    </div>
</div>

</asp:Content>

