﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="blank-leftnav.template.aspx.cs" Inherits="blank_leftnav_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/LeftNavigation.ascx" tagPrefix="Edwards" tagName="LeftNavigation" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="leftNavigation">
        <Edwards:LeftNavigation ID="LeftNav1" runat="server" />
    </div>

    <div id="innerContent">
        <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />
        
        <%-- Introduction Text Dropzone --%>
        <DZ:DropZone runat="server" ID="DropZone1" AllowAddColumn="True" AllowColumnResize="True"></DZ:DropZone>

        <br class="clear" />
    </div>
</div>

</asp:Content>