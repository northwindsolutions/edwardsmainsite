﻿<%@ Page Title="Blog" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="blog.template.aspx.cs" Inherits="library_templates_blog_template" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register Src="~/library/includes/controls/news/LeftNav.ascx" TagPrefix="Edwards" TagName="NewsLeftNav" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/LeftNavigation.ascx" tagPrefix="Edwards" tagName="LeftNavigation" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

    <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    
    <div id="inner" class="containerWrap">

    <div id="leftNavigation">
        <Edwards:NewsLeftNav runat="server" ID="newsLeftNav" />
        <Edwards:LeftNavigation ID="LeftNav1" runat="server" />
    </div>

    <div id="innerContent" class="three-col">
        
        <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />
        <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Center Column" />
        <DZ:DropZone runat="server" ID="centerColumnDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    </div>
    <div id="innerCallouts" style="margin-top:0">
        <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Inner Callout" />
        <DZ:DropZone runat="server" ID="innerCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    </div>
</div>
</asp:Content>

