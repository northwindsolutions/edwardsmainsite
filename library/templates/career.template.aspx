﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="career.template.aspx.cs" Inherits="career_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
        <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />
        
        <%-- Introduction Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <div class="one-half">
            <%-- Left Half Dropzone --%>
            <DZ:DropZone runat="server" ID="LeftColumnDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Top Left Half" />
        </div>

        <div class="one-half omega">
            <%-- Right Half Dropzone --%>
            <DZ:DropZone runat="server" ID="DropZone3" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Callout Top Left" />
        </div>
        <br class="clear">
    </div>
</div>

</asp:Content>

