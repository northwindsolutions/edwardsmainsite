﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Edwards;
using Ektron.Cms.PageBuilder;

public partial class career_template : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void Error(string message)
    {
        jsAlert(message);
    }

    public override void Notify(string message)
    {
        jsAlert(message);
    }

    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }
}