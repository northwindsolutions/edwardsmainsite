﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="contact-form.template.aspx.cs" Inherits="contact_form_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Banner" />

<div id="inner" class="containerWrap">
    <div id="innerContent" class="one-col">
        
        <%-- Introduction Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <br>

        <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Form" />
        <DZ:DropZone runat="server" ID="FormDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <div class="one-half">
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="BottomLeft" />
            <DZ:DropZone runat="server" ID="BottomLeftDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <div class="one-half omega">
            <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="BottomRight" />
            <DZ:DropZone runat="server" ID="BottomRightDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
		
    </div>
</div>

</asp:Content>

