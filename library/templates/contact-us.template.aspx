﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="contact-us.template.aspx.cs" Inherits="contact_us_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
<Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
        <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />
        
        <%-- Introduction Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Map" />
        <DZ:DropZone runat="server" ID="MapDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

		<br>

        <div class="one-half">
        	<Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Bottom Left Callout (1/2)" />
            <DZ:DropZone runat="server" ID="BottomLeftCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <div class="one-half omega">
            <div class="one-half">
                <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Bottom Right 1st Callout (1/4)" />
                <DZ:DropZone runat="server" ID="BottomRightFirstCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-half omega">
            	<Edwards:DropZoneLabel ID="DropZoneLabel5" runat="server" Label="Bottom Right 2nd Callout (1/4)" />
                <DZ:DropZone runat="server" ID="BottomRightSecondCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>
    </div>
</div>

</asp:Content>

