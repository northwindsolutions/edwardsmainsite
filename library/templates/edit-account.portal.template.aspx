﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="edit-account.portal.template.aspx.cs" Inherits="library_templates_edit_account_portal_template" %>

<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Src="~/library/includes/controls/BreadCrumbs.ascx" TagPrefix="Edwards" TagName="BreadCrumbs" %>
<%@ Register Src="~/library/includes/controls/DropZoneLabel.ascx" TagPrefix="Edwards" TagName="DropZoneLabel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" runat="Server">
    <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    <div id="inner" class="containerWrap innerBefore">
        
        <div id="innerContent">
            <div id="resetForm">
                
                
                
                
                <h2><strong><%= GetGlobalResourceObject("EdwardsPortal", "EditMyAccountHeader") %></strong></h2>
                <div class="one-fourth editAcc">
                    <div class="form-item up_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "EditMyaccountPhotoText") %></strong></p>
                        <asp:Image ID="profilePic" runat="server" />
                        <div class="avatarupload-item">
                            <asp:TextBox id="avatarClick" runat="server" ClientIDMode="Static" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>
                            <span class="avataruploadAllign"><asp:LinkButton ID="avatarSaveChanges" runat="server" OnClick="HandleSaveAvatar" ClientIDMode="Static"></asp:LinkButton> <span class="icon"></span></span>
                        </div>
                        <asp:FileUpload ID="avatarUpload" runat="server" ClientIDMode="Static" AllowMultiple="false" />
                    </div>
                    <div class="form-item pos_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "EditAccTitle") %></strong></p>
                        <asp:TextBox ID="position" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                    </div>
                    <div class="form-item ph_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "EditAccPhoneTitle") %></strong></p>
                        <asp:TextBox ID="phoneNo" runat="server" ClientIDMode="Static" MaxLength="15"></asp:TextBox>
                    </div>
                    <div class="form-item email_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "EditAccEmailTitle") %></strong></p>
                        <asp:TextBox ID="emailAdd" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                    </div>
                    <div class="form-item save_item">
                        <p class="form-validation">
                    <span id="positionFail"><%=GetGlobalResourceObject("EdwardsPortal", "TitleCannotbeblank") %></span>
                    <span id="phoneNofail"><%=GetGlobalResourceObject("EdwardsPortal", "PhoneNoCannotbeblank") %></span>
                    <span id="emailAddfail"><%=GetGlobalResourceObject("EdwardsPortal", "EmailIdCannotbeblank") %></span>
                    <span id="imageSize"><%=GetGlobalResourceObject("EdwardsPortal", "ImageSizeAlert") %></span>
                 </p>
                        <asp:Button CssClass="submitBtn" ID="saveButton" runat="server" OnClick="HandleEditClick" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
            <br class="clear" />
        </div>
    </div>
</asp:Content>

