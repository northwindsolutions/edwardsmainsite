﻿using Edwards.Portal;
using Edwards.Portal.Services;
using Ektron.Cms.Instrumentation;
using System;
using System.IO;
using System.Web;

public partial class library_templates_edit_account_portal_template : AuthenticatedPageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly UserService _userService;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_edit_account_portal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._userService = new UserService();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }
    protected MyAccountModel Model { get; private set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAuthentication(true);
        this.RenderLabels();
        this.BindModelToView(this.InitializeModel());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    private PortalUser PrepareUserModel()
    {
        return this.CurrentUser;
    }

    protected class MyAccountModel
    {
        public PortalUser User { get; set; }
    }

    protected void HandleSaveAvatar(object sender, EventArgs e)
    {
        if (this.avatarUpload.HasFile)
        {
            string avatarFile = this.AddAvatar(this.avatarUpload.PostedFile);

            this._authenticationService.EditAccount(
                this.CurrentUser,
                this.CurrentUser.Title,
                this.CurrentUser.Email,
                this.CurrentUser.Phone,
                avatarFile);

            if (!string.IsNullOrEmpty(avatarFile))
            {
                profilePic.ImageUrl = avatarFile;
            }
        }
    }

    protected void HandleEditClick(object sender, EventArgs e)
    {
            this._authenticationService.EditAccount(
                this.CurrentUser, 
                this.position.Text,
                this.emailAdd.Text,
                this.phoneNo.Text,
                null);

            this.RouteToMyAccount();
    }

    private string AddAvatar(HttpPostedFile postedAvatarFile)
    {
        try
        {
            string avatarPath = string.Empty;
            string avatarFileName = string.Empty;

            avatarFileName = Path.GetFileName(postedAvatarFile.FileName);

            if (!string.IsNullOrEmpty(avatarFileName))
            {
                string uniqueFileId = Guid.NewGuid().ToString();

                avatarPath = string.Format(
                    "/{0}/{1}{2}", 
                    this.GetGlobalResourceObject("EdwardsPortal", "AvatarFolderName").ToString(), 
                    uniqueFileId, 
                    avatarFileName);

                postedAvatarFile.SaveAs(Server.MapPath(avatarPath));
            }

            return avatarPath;
        }
        catch (Exception ex)
        {
            string errorMessage = string.Format(
                "Failed to upload profile photo for user: '{0} {1}' (File: {2}).", 
                this.CurrentUser.FirstName, 
                this.CurrentUser.LastName, 
                postedAvatarFile.FileName ?? "NULL");

            Log.WriteError(errorMessage);
            Log.WriteError(ex);
            
            return "";
        }
    }

    private void RouteToMyAccount()
    {
        var route = this._routeBuilder.MyAccount();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private void RenderLabels()
    {
        PortalUser currentUser = this.CurrentUser;

        if (!string.IsNullOrEmpty(currentUser.Title.ToString()))
        {
            position.Text = currentUser.Title.ToString();
        }

        if (!string.IsNullOrEmpty(currentUser.Email.ToString()))
        {
            emailAdd.Text = currentUser.Email.ToString();
        }

        if (!string.IsNullOrEmpty(currentUser.Phone.ToString()))
        {
            phoneNo.Text = currentUser.Phone.ToString();
        }

        position.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "TitleorPosition").ToString());
        phoneNo.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PhoneNo").ToString());
        emailAdd.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "EmailAdd").ToString());
        avatarUpload.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "Avatar").ToString());
        avatarClick.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "AvatarBrowse").ToString());
        avatarSaveChanges.Text = this.GetGlobalResourceObject("EdwardsPortal", "UploadText").ToString();
        saveButton.Text = this.GetGlobalResourceObject("EdwardsPortal", "SaveChanges").ToString();
    }

    private MyAccountModel InitializeModel()
    {
        return new MyAccountModel
        {
            User = this.PrepareUserModel(),
            
        };
    }

    private void BindModelToView(MyAccountModel model)
    {
        this.Model = model;
        if (!string.IsNullOrEmpty(model.User.ImageUrl))
        {
            this.profilePic.Visible = !string.IsNullOrWhiteSpace(model.User.ImageUrl);
            this.profilePic.ImageUrl = model.User.ImageUrl;
        }
        else
        {
            string savePath = "/library/images/common/portal-user.png"; 
            this.profilePic.Visible = true;
            this.profilePic.ImageUrl = savePath;
        }
    }

    public override void Error(string message)
    {
        
    }

    public override void Notify(string message)
    {
        
    }
}