﻿<%@ Page Title="Media" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="events.template.aspx.cs" Inherits="library_templates_events_template" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/LeftNavigation.ascx" tagPrefix="Edwards" tagName="LeftNavigation" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>
<%@ Register src="~/library/includes/controls/EventsList.ascx" tagPrefix="Edwards" tagName="EventsList" %>
<%@ Register src="~/library/includes/controls/EventsCallout.ascx" tagPrefix="Edwards" tagName="EventsCallout" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

    <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    
    <div id="inner" class="containerWrap">
        <div id="leftNavigation">
            <Edwards:LeftNavigation ID="LeftNav1" runat="server" />
        </div>

        <div id="innerContent" class="three-col">
        
            <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />
            <Edwards:EventsList runat="server" />
            <%-- Events list goes here. --%>

            <%--<Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Center Column" />--%>
            <%--<DZ:DropZone runat="server" ID="centerColumnDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>--%>

        </div>
        <div id="innerCallouts" style="margin-top:0">
        
            <Edwards:EventsCallout runat="server" />

            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Inner Callout" />
            <DZ:DropZone runat="server" ID="innerCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        </div>
    </div>

</asp:Content>

