﻿using Edwards;
using Edwards.Utility;
using Edwards.Portal;
using Edwards.Portal.Services;
using System;

public partial class library_templates_resetpassword_portal_template : PageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_resetpassword_portal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.RenderLabels();
    }

    public override void Error(string message)
    {

    }

    public override void Notify(string message)
    {

    }

    protected void ResetPasswordClick(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(usernameField.Text))
        {
            bool success = this._authenticationService.ResetPassword(usernameField.Text);
                
            PasswordMessage.Visible = success;
            failureMessage.Visible = !success;
        }
        else
        {
            failureMessage.Visible = true;
            failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordUserNameBlank").ToString();
        }
    }

    private void RenderLabels()
    {
        PasswordMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordSuccess").ToString();
        failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordFailure").ToString();
    }
}