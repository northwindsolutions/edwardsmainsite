﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="homepage-alternate.template.aspx.cs" Inherits="library_templates_homepage_alternate_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<%-- Add content controls here --%>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="innerContentPlaceholder">
    <div id="billboard">
        <%-- Full Width --%>
        <div class="hero">
            <Edwards:DropZoneLabel runat="server" Label="Row 1: Full" />
            <DZ:DropZone runat="server" ID="row1FullWidth" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
    </div>
    <div id="home" class="containerWrap">        
        <%-- Half & Half --%>
        <div class="one-half">
            <Edwards:DropZoneLabel runat="server" Label="Row 2: 1/2 - Left" />
            <DZ:DropZone runat="server" ID="row2HalfWidthLeft" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div class="one-half omega">
            <Edwards:DropZoneLabel runat="server" Label="Row 2: 1/2 - Right" />
            <DZ:DropZone runat="server" ID="row2HalfWidthRight" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <%-- Full Width --%>
        <div class="full-width omega">
            <Edwards:DropZoneLabel runat="server" Label="Row 3: Full" />
            <DZ:DropZone runat="server" ID="row3FullWidth" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <%-- Thirds --%>
        <div class="one-third">
            <div>
                <Edwards:DropZoneLabel runat="server" Label="Row 4: 1/3 - Left" />
                <DZ:DropZone runat="server" ID="row4OneThirdWidthLeft" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div id="iconBar">
                <Edwards:DropZoneLabel runat="server" Label="Row 4: 1/3 - Left (Callouts)" />
                <DZ:DropZone runat="server" ID="row4OneThirdWidthLeftCallouts" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>

        <div class="one-third">
            <div>
                <Edwards:DropZoneLabel runat="server" Label="Row 4: 1/3 - Mid" />
                <DZ:DropZone runat="server" ID="row4OneThirdWidthMiddle" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div id="iconBar">
                <Edwards:DropZoneLabel runat="server" Label="Row 4: 1/3 - Mid (Callouts)" />
                <DZ:DropZone runat="server" ID="row4OneThirdWidthMiddleCallouts" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>

        <div class="one-third omega">
            <div>
                <Edwards:DropZoneLabel runat="server" Label="Row 4: 1/3 - Right" />
                <DZ:DropZone runat="server" ID="row4OneThirdWidthRight" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div id="iconBar">
                <Edwards:DropZoneLabel runat="server" Label="Row 4: 1/3 - Right (Callouts)" />
                <DZ:DropZone runat="server" ID="row4OneThirdWidthRightCallouts" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>

        <%-- Full Width --%>
        <div class="full-width omega">
            <Edwards:DropZoneLabel runat="server" Label="Row 5: Full" />
            <DZ:DropZone runat="server" ID="row5FullWidth" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <%-- Half & Half --%>
        <div class="one-half">
            <Edwards:DropZoneLabel runat="server" Label="Row 6: 1/2 - Left" />
            <DZ:DropZone runat="server" ID="row6HalfWidthLeft" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div class="one-half omega">
            <Edwards:DropZoneLabel runat="server" Label="Row 6: 1/2 - Right" />
            <DZ:DropZone runat="server" ID="row6HalfWidthRight" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <%-- Full Width --%>
        <div class="full-width omega">
            <Edwards:DropZoneLabel runat="server" Label="Row 7: Full" />
            <DZ:DropZone runat="server" ID="row7FullWidth" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <%-- Two Thirds & One Third --%>
        <div class="two-thirds">
            <Edwards:DropZoneLabel runat="server" Label="Row 7: 2/3 - Left" />
            <DZ:DropZone runat="server" ID="row7TwoThirdsWidthLeft" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

        <div class="one-third omega">
            <div>
                <Edwards:DropZoneLabel runat="server" Label="Row 7: 1/3 - Right" />
                <DZ:DropZone runat="server" ID="row7OneThirdWidthRight" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div id="iconBar">
                <Edwards:DropZoneLabel runat="server" Label="Row 7: 1/3 - Right (Callouts)" />
                <DZ:DropZone runat="server" ID="row7OneThirdWidthCalloutsRight" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>

        <%-- One Third & Two Thirds --%>
        <div class="one-third">
            <div>
                <Edwards:DropZoneLabel runat="server" Label="Row 8: 1/3 - Left" />
                <DZ:DropZone runat="server" ID="row8OneThirdWidthLeft" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>

            <div id="iconBar">
                <Edwards:DropZoneLabel runat="server" Label="Row 8: 1/3 - Left (Callouts)" />
                <DZ:DropZone runat="server" ID="row8OneThirdWidthCalloutsLeft" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>

        <div class="two-thirds omega">
            <Edwards:DropZoneLabel runat="server" Label="Row 8: 2/3 Width - Right" />
            <DZ:DropZone runat="server" ID="row8TwoThirdsWidthRight" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>

    </div>
</asp:Content>