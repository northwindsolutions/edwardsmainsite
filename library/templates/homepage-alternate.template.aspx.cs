﻿using Edwards;
using System;
using System.Web.UI.WebControls;

public partial class library_templates_homepage_alternate_template : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }


    public override void Error(string message)
    {
        jsAlert(message);
    }

    public override void Notify(string message)
    {
        jsAlert(message);
    }

    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }
}