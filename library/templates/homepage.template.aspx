﻿<%@ Page Title="Homepage" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="homepage.template.aspx.cs" Inherits="library_templates_homepage_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<%-- Add content controls here --%>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="innerContentPlaceholder">


<div id="home" class="containerWrap">

    <%-- Slideshow --%>
    <Edwards:DropZoneLabel runat="server" Label="Homepage Rotator" />
    <DZ:DropZone runat="server" ID="topSliderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <%-- Middle Callouts --%>
    <div class="one-half">
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Middle Callout" />
        <DZ:DropZone runat="server" ID="middleCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    </div>
    <div class="one-half orange omega">
        <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Middle Right Callout" />
        <DZ:DropZone runat="server" ID="middlerightCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    </div>

    <div class="two-thirds">
        <%-- Bottom Callouts --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Bottom Callout" />
        <DZ:DropZone runat="server" ID="bottomCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    </div>
    <div class="one-third omega">
        <%-- Bottom Callouts --%>
        <div id="iconBar">
            <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Bottom Right Callout" />
            <DZ:DropZone runat="server" ID="bottomRightTopCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            <%--<div class="orange">--%>
                <Edwards:DropZoneLabel ID="DropZoneLabel5" runat="server" Label="Bottom Right Bottom Callout" />
                <DZ:DropZone runat="server" ID="bottomRightBotCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            <%--</div>--%>
        </div>
    </div>
</div>
</asp:Content>