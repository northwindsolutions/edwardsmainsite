﻿<%@ Page Title="Knowledge Centre" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="knowledgecentre.template.aspx.cs" Inherits="library_templates_knowledgecenter_template" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register TagPrefix="Edwards" TagName="SocialFeedsView" Src="~/widgets/Social/Views/Feeds.ascx" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/LeftNavigation.ascx" tagPrefix="Edwards" tagName="LeftNavigation" %>

<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
    
    <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    
    <div id="inner" class="containerWrap">

    <div id="leftNavigation">
        <Edwards:LeftNavigation ID="LeftNav1" runat="server" />
    </div>

    <div id="innerContent" class="two-col omega" itemprop="department" itemscope itemtype="http://schema.org/Organization">
        <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />

        <%-- Introduction Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <div class="two-thirds">
            <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Main Text" />
            <DZ:DropZone runat="server" ID="MainTextDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <div class="one-third omega">
            <Edwards:DropZoneLabel ID="DropZoneLabel7" runat="server" Label="Callout Right Column" />
            <DZ:DropZone runat="server" ID="CalloutRightDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
    </div>
</div>

</asp:Content>

