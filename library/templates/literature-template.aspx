﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="literature-template.aspx.cs" Inherits="library_templates_literature_template" %>

<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>
<%@ Register Src="~/library/includes/controls/SidebarMenu.ascx" TagPrefix="Edwards" TagName="SidebarMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
        <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <div id="inner" class="containerWrap">
        <div id="leftNavigation">
            <Edwards:SidebarMenu ID="SidebarMenu1" runat="server" />
        </div>
        <div id="innerContent" class="two-col omega">
            <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />

            <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Main Content" />
            <DZ:DropZone runat="server" ID="MainContentDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
    </div>
</asp:Content>

