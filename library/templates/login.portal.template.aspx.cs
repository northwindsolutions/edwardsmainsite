﻿using Edwards;
using Edwards.Portal;
using Edwards.Portal.Services;
using Edwards.Utility;
using System;

public partial class library_templates_login_portal_template : PageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_login_portal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsurePageAvailableInCurrentLanguage();
        this.RenderLabels();
    }

    public override void Error(string message)
    {
        
    }

    public override void Notify(string message)
    {
        
    }

    protected void HandleLoginClick(object sender, EventArgs e)
    {
        var userContext = this._authenticationService.Login(usernameField.Text, passwordField.Text);
        
        if (this._authenticationService.IsAuthenticated(userContext))
        {
            if (userContext.IsActivated)
            {
                this.RouteToDashboard();
            }
            else
            {
                this.RouteToChangePassword();
            }
        }
        else
        {
            failureMessage.Visible = true;            
        }
    }

    protected void ForgotPasswordClick(object sender, EventArgs e)
    {
        this.RouteToForgotPassword();
    }

    private void RouteToDashboard()
    {
        // To do: Route to dashboard...
        var route = this._routeBuilder.DashBoard();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private void RouteToChangePassword()
    {
        var route = this._routeBuilder.ChangePassword();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private void RouteToForgotPassword()
    {
        var route = this._routeBuilder.ForgotPassword();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private void EnsurePageAvailableInCurrentLanguage()
    {
        if (this.Pagedata != null)
        {
            AuthenticatedPageBase.EnsurePageLanguage(this.Pagedata.languageID, Constants.CurrentLanguageId);
        }
    }

    private void RenderLabels()
    {
        usernameField.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "LoginUsername").ToString());
        passwordField.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "LoginPassword").ToString());

        loginButton.Text = this.GetGlobalResourceObject("EdwardsPortal", "LoginSubmit").ToString();
        failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "LoginFailure").ToString();
    }
}