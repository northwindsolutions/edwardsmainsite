﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="market-sub.template.aspx.cs" Inherits="library_templates_market_sub_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/LeftNavigation.ascx" tagPrefix="Edwards" tagName="LeftNavigation" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="leftNavigation">
        <Edwards:LeftNavigation ID="LeftNav1" runat="server" />
    </div>

    <div id="innerContent" class="two-col omega">
        <Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />

        <%-- Introduction Text Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="MarketSubHeadDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        

        <div class="two-thirds">

            <%-- Middle Left (2/3) Dropzone --%>
            <DZ:DropZone runat="server" ID="MiddleLeftDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Middle Left (2/3)" />

        </div>
        <div class="one-third omega">

            <%-- Middle Right (1/3) Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Middle Right (1/3)" />
            <DZ:DropZone runat="server" ID="InnerCalloutsDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        </div>
        <br class="clear">

        <%-- Knowledge Centre Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Knowledge Centre" />
            <DZ:DropZone runat="server" ID="KnowledgeCentreDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    </div>
</div>

</asp:Content>

