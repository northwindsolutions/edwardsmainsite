﻿<%@ Page Title="Markets" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="market.template.aspx.cs" Inherits="library_templates_market_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<%-- Add content controls here --%>
<%-- <DZ:DropZone runat="server" ID="middleCalloutDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone> --%>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="innerContentPlaceholder">

<%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
		<Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />

        <%-- Landing Intro Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Landing Intro" />
        <DZ:DropZone runat="server" ID="LandingIntroDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <%-- Market Rotator Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Market Rotator" />
        <DZ:DropZone runat="server" ID="MarketRotatorDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <br class="clear" />
        <div class="one-third">
            <%-- Callout Left Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Callout Left" />
        	<DZ:DropZone runat="server" ID="CalloutLeftDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <div class="one-third">
            <%-- Callout Middle Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Callout Middle" />
        	<DZ:DropZone runat="server" ID="CalloutMiddleDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <div class="one-third omega">
            <%-- Callout Right Dropzone --%>
            <Edwards:DropZoneLabel ID="DropZoneLabel5" runat="server" Label="Callout Right" />
        	<DZ:DropZone runat="server" ID="CalloutRightDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
		<br class="clear" />

        <div id="iconBar">
            <%-- Navigation Dropzone Area --%>
            <div class="one-third">
                <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Navigation Callout Left" />
                <DZ:DropZone runat="server" ID="NaviLeftDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-third">
                <Edwards:DropZoneLabel ID="DropZoneLabel7" runat="server" Label="Navigation Callout Middle" />
                <DZ:DropZone runat="server" ID="NaviMiddleDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-third omega">
                <Edwards:DropZoneLabel ID="DropZoneLabel8" runat="server" Label="Navigation Callout Right" />
                <DZ:DropZone runat="server" ID="NavRightDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>
    </div>
</div>

</asp:Content>