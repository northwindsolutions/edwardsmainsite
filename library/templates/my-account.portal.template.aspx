﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="my-account.portal.template.aspx.cs" Inherits="library_templates_my_account_portal_template" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
    <%-- Banner Dropzone --%>

    <Edwards:DropZoneLabel runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <div id="inner" class="containerWrap">
        <div id="innerContent">     
            <h2><strong><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountHeader") %></strong></h2>
            <div id="accountDetails">
                <div class="alignleft">
                    <asp:Image CssClass="my-accimg" ID="profileImage" runat="server" />
                </div>
                <div>
                    <h4><%= Model.User.DisplayName ?? string.Empty %></h4>
                    <div class="one-half">
                        <ul id="myAccountProperties">
                            <li><%= Model.User.CompanyName ?? string.Empty %></li>
                            <li><%= Model.User.Title ?? string.Empty %></li>
                            <li><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountRegion") %>: <%= GetRegionLabel(Model.User.Region) ?? string.Empty %></li>
                            <li><%= GetPartnerTypeLabel(Model.User.PartnerType) %></li>
                            <li><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountPhone") %>: <%= Model.User.Phone ?? string.Empty %></li>
                            <li><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountEmail") %>: <%= Model.User.Email ?? string.Empty%></li>
                        </ul>
                    </div>
                    <div class="one-fourth"></div>
                    <div class="one-fourth omega">
                        <ul id="myAccountMenu" class="myacc_textalign">
                        <asp:Repeater ID="accountMenu" runat="server">
                            <HeaderTemplate>
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <a href='<%# Eval("Href") %>' target='<%# Eval("Target") %>'><%# Eval("Text") %><span class="icon"></span></a>
                                </li>
                                
                            </ItemTemplate>
                            <FooterTemplate>
                                
                            </FooterTemplate>
                        </asp:Repeater>
                        <li>
                            <asp:LinkButton ID="btn_open" runat="server" CssClass="openModal" ClientIDMode="Static" ></asp:LinkButton><span class="icon"></span>
                            </li>
                         </ul>      
                    </div>
                </div>  
                <br class="clear" />              
            </div>
            <div class="full-width">
                <Edwards:DropZoneLabel runat="server" Label="Informational Text" />
                <DZ:DropZone runat="server" ID="InformationDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="full-width" id="myAccountFavorites">
                <div id="favoriteMarkets">
                    <h3><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountSelectFeaturedMarkets") %></h3>
                    <asp:Repeater ID="marketsRepeater" runat="server" OnItemDataBound="HandleFeaturedItemDataBound">
                        <HeaderTemplate>
                            <ul class="featuredPages">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li id="featuredItem" runat="server">
                                <asp:CheckBox ID="featuredItemCheckbox" runat="server" Checked='<%# Eval("IsFavorite") %>' data-page-id='<%# Eval("Id") %>' /> <a href='<%# Eval("Url") %>' title='<%# Eval("Title") %>'><%# Eval("Title") %></a>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="full-width" id="favoriteProducts">
                    <h3><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountSelectFeaturedProducts") %></h3>
                    <asp:Repeater ID="productsRepeater" runat="server" OnItemDataBound="HandleFeaturedItemDataBound">
                        <HeaderTemplate>
                            <ul class="featuredPages">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li id="featuredItem" runat="server">
                                <asp:CheckBox ID="featuredItemCheckBox" runat="server" Checked='<%# Eval("IsFavorite") %>' data-page-id='<%# Eval("Id") %>' /> <a href='<%# Eval("Url") %>' title='<%# Eval("Title") %>'><%# Eval("Title") %></a>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="favoriteActions" class="full-width form-item">
                    <asp:Button CssClass="submitBtn" ID="saveButton" CommandName="Save" runat="server" OnClick="HandleSaveClick" />
                </div>
            </div>
        </div>
    </div>
    

    <div class="modal">
    <div class="modal-header">
        <h3><%=GetGlobalResourceObject("EdwardsPortal", "MyAccountCloseMyAccount") %><a class="close-modal" href="#">&times;</a></h3>
    </div>
    <div class="modal-body">
        <p><%= GetGlobalResourceObject("EdwardsPortal", "MyAccountCloseAreYouSure") %></p>
       
    </div>
    <div class="modal-footer">
        <asp:LinkButton ID="btn_close" runat="server" Text="Proceed" ClientIDMode="Static" OnClick="HandleDeleteClick"></asp:LinkButton>
        <a href="#" class="close-modal"><%= GetGlobalResourceObject("EdwardsPortal", "Cancel") %></a>
    </div>
</div>
<div class="modal-backdrop"></div>
</asp:Content>