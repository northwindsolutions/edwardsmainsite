﻿using Edwards.Portal;
using Edwards.Portal.Repositories;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Cms.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class library_templates_my_account_portal_template : AuthenticatedPageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly IPortalMenuFactory _menuFactory;
    private readonly FeaturedPageRepository _featuredPageRepository;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_my_account_portal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._menuFactory = new CachingPortalMenuFactory(new MyAccountPortalMenuFactory());
        this._featuredPageRepository = new FeaturedPageRepository();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected MyAccountModel Model { get; private set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAuthentication(true);
        
        this.RenderLabels();

        var model = this.InitializeModel();
        this.ApplyDefaultSelections(model);
        this.BindModelToView(model);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void Error(string message)
    {
        
    }

    public override void Notify(string message)
    {
        
    }

    private void RenderLabels()
    {
        this.saveButton.Text = this.GetGlobalResourceObject("EdwardsPortal", "MyAccountSaveChanges").ToString();
        btn_open.Text = this.GetGlobalResourceObject("EdwardsPortal", "CloseAccountText").ToString();
    }

    private MyAccountModel InitializeModel()
    {
        return new MyAccountModel
        {
            User = this.PrepareUserModel(),
            Menu = this.PrepareMenuModel(),
            FeaturedMarkets = this.PrepareFeaturedMarketsModel(),
            FeaturedProducts = this.PrepareFeaturedProductsModel()            
        };
    }

    private void BindModelToView(MyAccountModel model)
    {
        this.Model = model;

        if (model.Menu != null)
        {
            accountMenu.DataSource = model.Menu.Items;
            accountMenu.DataBind();
        }

        marketsRepeater.DataSource = model.FeaturedMarkets;
        marketsRepeater.DataBind();

        productsRepeater.DataSource = model.FeaturedProducts;
        productsRepeater.DataBind();

        this.profileImage.Visible = !string.IsNullOrWhiteSpace(model.User.ImageUrl);
        this.profileImage.ImageUrl = model.User.ImageUrl;
    }

    private PortalUser PrepareUserModel()
    {
        return this.CurrentUser;
    }

    private IMenuData PrepareMenuModel()
    {
        return this._menuFactory.Create();
    }

    private IEnumerable<FeaturedPage> PrepareFeaturedMarketsModel()
    {
        return this._featuredPageRepository.GetMarketPages(this.CurrentUser, Constants.CurrentLanguageId);        
    }

    private IEnumerable<FeaturedPage> PrepareFeaturedProductsModel()
    {
        return this._featuredPageRepository.GetProductPages(this.CurrentUser, Constants.CurrentLanguageId);
    }

    private void ApplyDefaultSelections(MyAccountModel model)
    {
        if (!this.IsPostBack)
        {
            if (model.AllFeaturedPages.All(p => !p.IsFavorite))
            {
                foreach (var page in model.AllFeaturedPages)
                {
                    page.IsFavorite = true;
                }

                this.SaveFeaturedPages(model.AllFeaturedPages.Select(p => p.Id));
            }
        }
    }

    protected class MyAccountModel
    {
        public PortalUser User { get; set; }
        public IMenuData Menu { get; set; }
        public IEnumerable<FeaturedPage> FeaturedMarkets { get; set; }
        public IEnumerable<FeaturedPage> FeaturedProducts { get; set; }
        public IEnumerable<FeaturedPage> AllFeaturedPages
        {
            get
            {
                var allPages = new List<FeaturedPage>();

                if (this.FeaturedMarkets != null)
                {
                    allPages.AddRange(this.FeaturedMarkets);
                }

                if (this.FeaturedProducts != null)
                {
                    allPages.AddRange(this.FeaturedProducts);
                }

                return allPages;
            }
        }
    }
    
    protected void HandleFeaturedItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var listItem = e.Item.FindControl("featuredItem") as HtmlControl;
        
        if (listItem != null)
        {
            string classValue = "one-fourth";

            if ((e.Item.ItemIndex + 1) % 4 == 0)
            {
                classValue += " omega";
            }

            listItem.Attributes.Add("class", classValue);
        }
    }

    protected void HandleSaveClick(object sender, EventArgs e)
    {
        List<long> selectedPages = new List<long>();

        selectedPages.AddRange(this.GetFeaturedItemSelections(marketsRepeater));
        selectedPages.AddRange(this.GetFeaturedItemSelections(productsRepeater));

        this.SaveFeaturedPages(selectedPages);

        this.RouteToDashboard();
    }

    private void SaveFeaturedPages(IEnumerable<long> pages)
    {
        this._featuredPageRepository.SetFavoritePages(this.CurrentUser, pages);
    }

    private void RouteToDashboard()
    {
        var route = this._routeBuilder.DashBoard();
        this._router.RouteTo(route, Constants.CurrentLanguageId);
    }

    private List<long> GetFeaturedItemSelections(Repeater featuredItemsRepeater)
    {
        List<long> selectedPages = new List<long>();

        foreach (RepeaterItem featuredItem in featuredItemsRepeater.Items)
        {
            var itemCheckBox = featuredItem.FindControl("featuredItemCheckBox") as CheckBox;

            if (itemCheckBox != null && itemCheckBox.Checked)
            {
                long pageId;
                if (long.TryParse(itemCheckBox.Attributes["data-page-id"], out pageId))
                {
                    selectedPages.Add(pageId);
                }
            }
        }

        return selectedPages;
    }
    protected void HandleDeleteClick(object sender, EventArgs e)
    {
        PortalUser currentUser = this.CurrentUser;
        this._authenticationService.CloseAccount(currentUser);
        this.RouteToHome();

    }
    private void RouteToHome()
    {
        var route = this._routeBuilder.Login();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    protected string GetPartnerTypeLabel(string partnerType)
    {
        switch (partnerType)
        {
            case "Distributor":
                return GetGlobalResourceObject("EdwardsPortal", "PartnerTypeDistributor").ToString();
            default:
                return partnerType;
        }
    }

    protected string GetRegionLabel(string regionId)
    {
        switch (regionId)
        {
            case "Asia":
                return GetGlobalResourceObject("EdwardsPortal", "RegionAsia").ToString();
            case "Americas":
                return GetGlobalResourceObject("EdwardsPortal", "RegionAmericas").ToString();
            case "EMEA":
                return GetGlobalResourceObject("EdwardsPortal", "RegionEMEA").ToString();
        }

        return "";
    }
}