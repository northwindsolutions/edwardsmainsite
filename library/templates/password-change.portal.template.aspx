﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="password-change.portal.template.aspx.cs" Inherits="library_templates_password_change_portal_template" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
    <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <div id="inner" class="containerWrap">
        <div id="innerContent">     
            <div id="resetForm">
                <h2><strong><%= GetGlobalResourceObject("EdwardsPortal", "ChangePasswordHeader") %></strong></h2>
                <p>
                    <%= GetGlobalResourceObject("EdwardsPortal", "ChangePasswordInstructions") %>
                </p>
                <p class="form-validation">
                    <asp:Literal ID="failureMessage" Visible="false" runat="server"></asp:Literal>
                </p>
                <div class="one-fourth">
                    <div class="form-item">
                        <asp:TextBox ID="passwordField" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="form-item">
                        <asp:TextBox ID="passwordConfirmationField" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="form-item">
                        <asp:Button CssClass="submitBtn" ID="resetButton" runat="server" OnClick="HandleResetClick" />
                    </div>
                </div>
            </div>       
            <br class="clear" />
        </div>
    </div>
</asp:Content>