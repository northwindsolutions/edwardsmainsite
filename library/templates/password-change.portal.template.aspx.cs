﻿using Edwards;
using Edwards.Portal;
using Edwards.Portal.Services;
using System;

public partial class library_templates_password_change_portal_template : AuthenticatedPageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly UserService _userService;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_password_change_portal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._userService = new UserService();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAuthentication(true);
        this.RenderLabels();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public override void Error(string message)
    {

    }

    public override void Notify(string message)
    {

    }

    protected void HandleResetClick(object sender, EventArgs e)
    {        
        if (this.ValidateForm())
        {
            PortalUser currentUser = this.CurrentUser;

            this._authenticationService.ChangePassword(currentUser, this.passwordField.Text);

            if (!currentUser.IsActivated)
            {
                this._userService.Activate(currentUser);
            }

            this.RouteToMyAccount();
        }
    }

    private void RouteToMyAccount()
    {
        var route = this._routeBuilder.MyAccount();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private bool ValidateForm()
    {
        bool isValid = true;
        if (string.IsNullOrWhiteSpace(this.passwordField.Text) || string.IsNullOrWhiteSpace(this.passwordConfirmationField.Text))
        {
            failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ChangePasswordCannotBeBlank").ToString();
            failureMessage.Visible = true;
            isValid = false;
        }

        if (this.passwordField.Text != this.passwordConfirmationField.Text)
        {
            failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ChangePasswordNotIdentical").ToString();
            failureMessage.Visible = true;
            isValid = false;
        }

        return isValid;
    }

    private void RenderLabels()
    {
        passwordField.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "ChangePasswordNewPassword").ToString());
        passwordConfirmationField.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "ChangePasswordRetypePassword").ToString());
        resetButton.Text = this.GetGlobalResourceObject("EdwardsPortal", "ChangePasswordSubmit").ToString();
        failureMessage.Text = string.Empty;
    }
}