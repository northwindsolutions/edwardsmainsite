﻿using Edwards.Portal;
using System;

public partial class library_templates_product_portal_template : AuthenticatedPageBase
{    
    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAccess();
    }

    public override void Error(string message)
    {
        
    }

    public override void Notify(string message)
    {
        
    }
}