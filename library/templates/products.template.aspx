﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="products.template.aspx.cs" Inherits="products_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
<Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
		<Edwards:BreadCrumbs ID="BreadCrumbs1" runat="server" />

        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Introduction Text" />
        <DZ:DropZone runat="server" ID="IntroTextHeaderDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

        <br>

        <div id="productContainer">
            <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Top Product Rotator Zone" />
            <DZ:DropZone runat="server" ID="TopRotatorDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
        </div>
        <hr>
    	    <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Middle Product Rotator Zone" />
            <DZ:DropZone runat="server" ID="MiddleRotatorDropZone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

		<br>

        <div id="iconBar">
            <div class="one-third">
                <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Navigation Callout Left" />
                <DZ:DropZone runat="server" ID="NaviLeftDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-third">
                <Edwards:DropZoneLabel ID="DropZoneLabel7" runat="server" Label="Navigation Callout Middle" />
                <DZ:DropZone runat="server" ID="NaviMiddleDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-third omega">
                <Edwards:DropZoneLabel ID="DropZoneLabel8" runat="server" Label="Navigation Callout Right" />
                <DZ:DropZone runat="server" ID="NavRightDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>
    </div>
</div>

</asp:Content>

