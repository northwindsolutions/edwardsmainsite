﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="promotions.portal.template.aspx.cs" Inherits="library_templates_promotionsportal_template" %>
<%@ Import Namespace="Ektron.Edwards.Models.PortalPromotionalItems" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Src="~/library/includes/controls/BreadCrumbs.ascx" TagPrefix="Edwards" TagName="BreadCrumbs" %>
<%@ Register Src="~/library/includes/controls/DropZoneLabel.ascx" TagPrefix="Edwards" TagName="DropZoneLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" runat="Server">
    <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <div id="inner" class="containerWrap">
        <div id="innerContent">
 
            <br class="clear">
            <br>
            <div>
                <div class="one-third one-third-row promo-onethird-row">
 
                    <p class="txt_lft promoTitle">
                       <strong> <asp:Literal ID="litPromoHeader" runat="server" Text="Promo Items"></asp:Literal> </strong></p>
                    <asp:Repeater ID="RepPromoItems" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="promoItems-outer">
                                <div class="promoImage">
                                    <a class="fancybox-media" href="<%# GetFullImageUrl(GetDataItem() as PromotionalItemsItem) %>"><img id="promoImg" alt="Promo Image" src="<%# Eval("Image.img.src") %>" /></a>
                                </div>
                                <p class="promo_imgTitle">
                                  <strong>  <%# Eval("Title") %></strong>
                                </p>
                         
                                <%# BuildDropdownVal( Eval("AvailableQuantities").ToString(),Eval("Title").ToString(),Eval("Image.img.src").ToString()) %>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="portal-form">
                    <Edwards:DropZoneLabel ID="DropZoneLabel10" runat="server" Label="Navigation Callout Middle" />
                    <DZ:DropZone runat="server" ID="DZpromoform" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
                </div>
             </div>
        </div>
 
    </div>
  

    <div class="modal">
        <div class="modal-header">
            <h3><%= GetGlobalResourceObject("EdwardsPortal", "PromoRequestConfirmationHeader") %></h3>
        </div>
        <div class="modal-body">
            <p><%= GetGlobalResourceObject("EdwardsPortal", "PromoConfirmation") %> </p>
        </div>
        <div class="modal-footer">
            <a href="#" class="close-modal"><%= GetGlobalResourceObject("EdwardsPortal", "Ok") %></a>
        </div>
    </div>
    <div class="modal-backdrop"></div>


    <script>
        $(document).ready(function () {

            $('#sumbitItems').click(function () {
                $('#qunaitemval').val('')
                $('.quanDD option:selected').each(function () {
                    if ($(this).val() != '0') {
                        var quantityValue = ($(this).parent().attr("itemName") + '||' + $(this).val() + '||' + $(this).parent().attr("imgUrl"));
                        if ($('#qunaitemval').val())
                            $('#qunaitemval').val($('#qunaitemval').val() + ',' + quantityValue);
                        else
                            $('#qunaitemval').val(quantityValue);
                       
                    }
                });
            });

        });
      
    </script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = modalPosition();
                $(window).resize(function () {
                    modalPosition();
                });
                $('body').append(modal);
                var loading = $('.modal, .modal-backdrop').fadeIn('fast');//$(".loading");
                loading.show();
            }, 200);
        }

        $('form').live("submit", function () {
            ShowProgress();
        });
    </script>
</asp:Content>

