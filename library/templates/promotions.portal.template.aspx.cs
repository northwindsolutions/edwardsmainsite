﻿using Edwards.Portal;
using Edwards.Portal.Repositories;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Edwards.Models.PortalPromotionalItems;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

public partial class library_templates_promotionsportal_template : AuthenticatedPageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly UserService _userService;
    private readonly FeaturedPageRepository _featuredPageRepository;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_promotionsportal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._userService = new UserService();
        this._featuredPageRepository = new FeaturedPageRepository();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAccess();
        BindModelToView(this.InitializeModel());

    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public override void Error(string message)
    {

    }

    public override void Notify(string message)
    {

    }

    protected string GetFullImageUrl(PromotionalItemsItem item)
    {
        string url = string.Empty;

        if (item != null)
        {
            if (item.FullImage != null && item.FullImage.img != null && !string.IsNullOrWhiteSpace(item.FullImage.img.src))
            {
                url = item.FullImage.img.src;
            }
            else if (item.Image != null && item.Image.img != null && !string.IsNullOrWhiteSpace(item.Image.img.src))
            {
                url = item.Image.img.src;
            }
        }

        return url;
    }

    private PromoModel InitializeModel()
    {
        return new PromoModel
        {
            PromoItemProducts = this.PreparePromoItemsModel()
        };
    }

    private void BindModelToView(PromoModel model)
    {
        litPromoHeader.Text = GetGlobalResourceObject("EdwardsPortal", "PromoHeader").ToString();
        RepPromoItems.DataSource = model.PromoItemProducts;
        RepPromoItems.DataBind();
    }

    private PortalUser PrepareUserModel()
    {
        return this.CurrentUser;
    }
    protected class PromoModel
    {
        public IEnumerable<PromotionalItemsItem> PromoItemProducts { get; set; }
    }
    private IEnumerable<PromotionalItemsItem> PreparePromoItemsModel()
    {
        long promosmartId = 0;
        long.TryParse(ConfigurationManager.AppSettings["PromoSmartFormId"].ToString(), out promosmartId);
        return this._featuredPageRepository.GetPromoItems(this.CurrentUser, Constants.CurrentLanguageId, promosmartId).Where(IsValidItem);
    }

    private bool IsValidItem(PromotionalItemsItem item)
    {
        return item != null &&
               item.Image != null &&
               item.Image.img != null &&
               !string.IsNullOrWhiteSpace(item.Image.img.src) &&
               !string.IsNullOrWhiteSpace(item.Title) &&
               !string.IsNullOrWhiteSpace(item.AvailableQuantities);
    }
    
    protected string BuildDropdownVal(string quantity, string itemName,string imgUrl)
    {
        if (!string.IsNullOrEmpty(quantity))
        {
            string[] quantityItems = quantity.Split(',');

            string options = string.Format("<option value=\"0\">{0}</option>", GetGlobalResourceObject("EdwardsPortal", "PromoSelectQuanity"));
            foreach (string quanVal in quantityItems)
            {
                options += string.Format("<option value=\"{0}\">{0}</option>", quanVal);
            }
            return string.Format("<select class=\"quanDD\" itemName=\"" + itemName + "\" imgUrl=\"" + imgUrl + "\" id=\"drpQunatity\">" + options + " </select>");
        }
        else return "";

    }
}