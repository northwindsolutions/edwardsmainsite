﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Base.master" AutoEventWireup="true" CodeFile="promotions.template.aspx.cs" Inherits="promotions_template" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="pageHostPlaceHolder">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">

<%-- Banner Dropzone --%>
<Edwards:DropZoneLabel runat="server" Label="Banner" />
<DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

<div id="inner" class="containerWrap">
    <div id="innerContent">
        <div class="blue">
            <div class="one-half">
                <Edwards:DropZoneLabel ID="DropZoneLabel3" runat="server" Label="Top Left Content (1/2)" />
                <DZ:DropZone runat="server" ID="TopLeftContentDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-half omega">
                <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Top Right Content (1/2)" />
                <DZ:DropZone runat="server" ID="TopRightContentDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <br class="clear">
            <div class="one-half">
                <div class="one-half">
                    <Edwards:DropZoneLabel ID="DropZoneLabel2" runat="server" Label="Middle Left Content (1/4)" />
                    <DZ:DropZone runat="server" ID="MiddleLeftContentDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
                </div>
                <div class="one-half omega">
                    <Edwards:DropZoneLabel ID="DropZoneLabel4" runat="server" Label="Middle Right Content (1/4)" />
                    <DZ:DropZone runat="server" ID="MiddleRightContentDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
                </div>
            </div>
            <div class="one-half omega">
                <Edwards:DropZoneLabel ID="DropZoneLabel5" runat="server" Label="Middle Right Content (1/2)" />
                <DZ:DropZone runat="server" ID="MiddleRightHalfContentDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>
        <br class="clear"><br>
        <div id="iconBar">
            <div class="one-third">
                <Edwards:DropZoneLabel ID="DropZoneLabel6" runat="server" Label="Navigation Callout Left" />
                <DZ:DropZone runat="server" ID="NavLeftDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-third">
                <Edwards:DropZoneLabel ID="DropZoneLabel7" runat="server" Label="Navigation Callout Middle" />
                <DZ:DropZone runat="server" ID="NavMiddleDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
            <div class="one-third omega">
                <Edwards:DropZoneLabel ID="DropZoneLabel8" runat="server" Label="Navigation Callout Right" />
                <DZ:DropZone runat="server" ID="NavRightDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
            </div>
        </div>
    </div>
</div>

</asp:Content>

