﻿using Edwards;
using Edwards.Portal;
using Edwards.Utility;
using Ektron.Cms.Framework.UI;
using System;
using System.Web.UI.WebControls;

public partial class library_templates_request_access_form : PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsurePageAvailableInCurrentLanguage();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public override void Error(string message)
    {
        jsAlert(message);
    }

    public override void Notify(string message)
    {
        jsAlert(message);
    }

    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }

    private void EnsurePageAvailableInCurrentLanguage()
    {
        if (this.Pagedata != null)
        {
            AuthenticatedPageBase.EnsurePageLanguage(this.Pagedata.languageID, Constants.CurrentLanguageId);
        }

        Css.Register(this, new Ektron.Cms.CommonApi().SitePath + "library/styles/stylesheets/salesrep.css", BrowserTarget.All, false); 
    }
}