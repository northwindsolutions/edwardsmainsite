﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="resetpassword.portal.template.aspx.cs" Inherits="library_templates_resetpassword_portal_template" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register src="~/library/includes/controls/BreadCrumbs.ascx" tagPrefix="Edwards" tagName="BreadCrumbs" %>
<%@ Register src="~/library/includes/controls/DropZoneLabel.ascx" tagPrefix="Edwards" tagName="DropZoneLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
    <%-- Banner Dropzone --%>
        <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <div id="inner" class="containerWrap">
        <div id="innerContent">     
            <div id="loginForm">
                <h2><strong><%= GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordHeader") %></strong></h2>
                <p>
                    <%= GetGlobalResourceObject("EdwardsPortal", "ForgotPassword") %>
                </p>
                <p class="form-validation">
                    <asp:Literal ID="failureMessage" Visible="false" runat="server"></asp:Literal>                    
                </p>
                <p>
                    <asp:Literal ID="PasswordMessage" Visible="false" runat="server"></asp:Literal>
                </p>
                <div class="one-fourth">
                    <div class="form-item">
                        <asp:TextBox ID="usernameField" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-item">
                        <asp:Button CssClass="submitBtn" ID="ResetButton" Text="Reset" runat="server" OnClick="ResetPasswordClick" />
                    </div>
                </div>
            </div>       
            <br class="clear" />
        </div>
        </div>
</asp:Content>

