﻿using Edwards;
using Edwards.Portal;
using Edwards.Portal.Services;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class library_templates_resetpassword_portal_template : PageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_resetpassword_portal_template()
    {
        this._authenticationService = new AuthenticationService();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.RenderLabels();
    }

    public override void Error(string message)
    {

    }

    public override void Notify(string message)
    {

    }

    protected void ResetPasswordClick(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(usernameField.Text))
        {
            string username = usernameField.Text;
            string password = "false"; //this._authenticationService.GetUserByUserName(usernameField.Text);

            if (password != "false")
            {
                string email = password.Remove(0, password.IndexOf(",") + 1);
                string message = this.GetGlobalResourceObject("EdwardsPortal", "ResetPasswordEmailText").ToString().Replace("@appPassword@", password);
                string subject = this.GetGlobalResourceObject("EdwardsPortal", "ResetPasswordEmailSubject").ToString();
                //Backend.SendEmail(email, message, subject);
                PasswordMessage.Visible = true;
                failureMessage.Visible = false;
            }
            else
            {
                failureMessage.Visible = true;
                PasswordMessage.Visible = false;                
            }
        }
        else
        {
            failureMessage.Visible = true;
            failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordUserNameBlank").ToString();
        }
    }

    private void RenderLabels()
    {
        PasswordMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordSuccess").ToString();
        failureMessage.Text = this.GetGlobalResourceObject("EdwardsPortal", "ForgotPasswordFailure").ToString();
    }
}