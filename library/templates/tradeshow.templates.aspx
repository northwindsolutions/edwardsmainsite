﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" AutoEventWireup="true" CodeFile="tradeshow.templates.aspx.cs" Inherits="library_templates_tradeshow_templates" %>
<%@ Import Namespace="Ektron.Edwards.Models.TradshowMaterials" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Src="~/library/includes/controls/BreadCrumbs.ascx" TagPrefix="Edwards" TagName="BreadCrumbs" %>
<%@ Register Src="~/library/includes/controls/DropZoneLabel.ascx" TagPrefix="Edwards" TagName="DropZoneLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
      <%-- Banner Dropzone --%>
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>

    <div id="inner" class="containerWrap">
        <div id="innerContent">
            <br class="clear">
            <br>
            <div>
               <div class="one-third one-third-row promo-onethird-row">
                     <p class="txt_lft promoTitle">
                       <strong> <asp:Literal ID="litPromoHeader" runat="server" Text="Tradeshow Materials"></asp:Literal> </strong></p>
                    <asp:Repeater ID="RepPromoItems" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <div class="promoItems-outer">
                                <div class="promoImage">
                                    <a class="fancybox-media" href="<%# GetFullImageUrl(GetDataItem() as TradeshowMaterialsTradeshowItem) %>"><img id="promoImg" alt="Tradeshow Material Image" src="<%# Eval("Image.img.src") %>" /></a>
                                </div>
                                <p class="promo_imgTitle">
                                  <strong>  <%# Eval("Title") %></strong>
                                </p>
                                <p>
                                    <asp:CheckBox ID="tradeSelect"  runat="server" Text='<%# GetGlobalResourceObject("EdwardsPortal", "TradeshowSelectItem") %>' itemName='<%# Eval("Title") %>' imgUrl='<%# Eval("Image.img.src") %>'/>
                                </p>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="portal-form">
                    <Edwards:DropZoneLabel ID="DropZoneLabel10" runat="server" Label="Navigation Callout Middle" />
                    <DZ:DropZone runat="server" ID="DZpromoform" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
                </div>
 
            </div>
        </div>
    </div>
     <div class="modal">
        <div class="modal-header">
            <h3><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowRequestHeader") %></h3>
        </div>
        <div class="modal-body"> 
            <p><%=GetGlobalResourceObject("EdwardsPortal", "TradeShowThankyouMessage") %></p>

        </div>
         <div class="modal-footer">
            <a href="#" class="close-modal"><%= GetGlobalResourceObject("EdwardsPortal", "Ok") %></a>
        </div>
    </div>
    <div class="modal-backdrop"></div>
   
 
    <script>
        $(document).ready(function () {

            $('#sumbittradeItems').click(function () {
                $('#qunaitemval').val('')
                $('input:checkbox:checked').each(function () {
                   // if ($(this).val() != '0') {
                        var quantityValue = ($(this).parent().attr("itemName") + '||' + $(this).parent().attr("imgUrl"));
                        if ($('#qunaitemval').val())
                            $('#qunaitemval').val($('#qunaitemval').val() + ',' + quantityValue);
                        else
                            $('#qunaitemval').val(quantityValue);

                   // }
                });
            });

        });

    </script>
       <script type="text/javascript">
           function ShowProgress() {
               setTimeout(function () {
                   var modal = modalPosition();
                   $(window).resize(function () {
                       modalPosition();
                   });
                   $('body').append(modal);
                   var loading = $('.modal, .modal-backdrop').fadeIn('fast');//$(".loading");
                   loading.show();
               }, 200);
           }

           $('form').on("submit", function () {
               ShowProgress();
           });
    </script>
</asp:Content>

