﻿using Edwards.Portal;
using Edwards.Portal.Repositories;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Edwards.Models.TradshowMaterials;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;


public partial class library_templates_tradeshow_templates : AuthenticatedPageBase
{
    private readonly AuthenticationService _authenticationService;
    private readonly UserService _userService;
    private readonly FeaturedPageRepository _featuredPageRepository;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;

    public library_templates_tradeshow_templates()
    {
        this._authenticationService = new AuthenticationService();
        this._userService = new UserService();
        this._featuredPageRepository = new FeaturedPageRepository();
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.EnsureAccess();
        
        BindModelToView(this.InitializeModel());

    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public override void Error(string message)
    {

    }

    public override void Notify(string message)
    {

    }

    protected string GetFullImageUrl(TradeshowMaterialsTradeshowItem item)
    {
        string url = string.Empty;

        if (item != null)
        {
            if (item.FullImage != null && item.FullImage.img != null && !string.IsNullOrWhiteSpace(item.FullImage.img.src))
            {
                url = item.FullImage.img.src;
            }
            else if (item.Image != null && item.Image.img != null && !string.IsNullOrWhiteSpace(item.Image.img.src))
            {
                url = item.Image.img.src;
            }
        }

        return url;
    }

    private TradeModel InitializeModel()
    {
        return new TradeModel
        {
            TradeItemProducts = this.PrepareTradeItemsModel()
        };
    }

    private void BindModelToView(TradeModel model)
    {
        //this.Model = model;
        litPromoHeader.Text = GetGlobalResourceObject("EdwardsPortal", "TradeshowHeader").ToString();


        RepPromoItems.DataSource = model.TradeItemProducts;
        RepPromoItems.DataBind();


    }

    private PortalUser PrepareUserModel()
    {
        return this.CurrentUser;
    }

    protected class TradeModel
    {
        public IEnumerable<TradeshowMaterialsTradeshowItem> TradeItemProducts { get; set; }
        
    }

    private IEnumerable<TradeshowMaterialsTradeshowItem> PrepareTradeItemsModel()
    {
        long tradesmartId = 0;
        long.TryParse(ConfigurationManager.AppSettings["TradeSmartFormId"].ToString(), out tradesmartId);
        return this._featuredPageRepository.GetTradeItems(this.CurrentUser, Constants.CurrentLanguageId, tradesmartId).Where(IsValidItem);
    }

    private bool IsValidItem(TradeshowMaterialsTradeshowItem item)
    {
        return item != null &&
            item.Image != null &&
            item.Image.img != null &&
            !string.IsNullOrWhiteSpace(item.Image.img.src) &&
            !string.IsNullOrWhiteSpace(item.Title);
    }
}