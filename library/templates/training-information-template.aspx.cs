﻿using System;
using System.Web.UI.WebControls;

using Edwards.Portal;

public partial class library_templates_training_information_template : AuthenticatedPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        EnsureAccess();
    }

    public override void Error(string message)
    {
        jsAlert(message);
    }

    public override void Notify(string message)
    {
        jsAlert(message);
    }

    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }
}