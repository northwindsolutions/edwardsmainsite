﻿<%@ Page Title="" Language="C#" MasterPageFile="~/library/templates/Portal.master" Debug="true" AutoEventWireup="true" CodeFile="videos-template.aspx.cs" Inherits="library_templates_Edwards_Partner_Portal_videos" %>

<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Src="~/library/includes/controls/BreadCrumbs.ascx" TagPrefix="Edwards" TagName="BreadCrumbs" %>
<%@ Register Src="~/library/includes/controls/DropZoneLabel.ascx" TagPrefix="Edwards" TagName="DropZoneLabel" %>
<%@ Register Src="~/library/includes/controls/SidebarMenu.ascx" TagPrefix="Edwards" TagName="SidebarMenu" %>


<asp:Content ID="Content1" ContentPlaceHolderID="pageHostPlaceHolder" Runat="Server">
    <PH:PageHost runat="server" ID="ph1"></PH:PageHost>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="innerContentPlaceholder" Runat="Server">
    <Edwards:DropZoneLabel ID="DropZoneLabel1" runat="server" Label="Banner" />
    <DZ:DropZone runat="server" ID="BannerDropzone" AllowAddColumn="False" AllowColumnResize="False"></DZ:DropZone>
    <div id ="inner" class="containerWrap">

        <div id="leftNavigation">
            <Edwards:SidebarMenu ID="SidebarMenu1" runat="server" />
        </div>
       <div class="video-content">
           
        <div id="innerContent" class="one-col">
             <h2><strong><%= GetGlobalResourceObject("EdwardsPortal", "VideosHeader") %></strong></h2>
        </div>
        </div>
        </div>
    <input type="hidden" id="hdnPageNumber" value="0" />
     <script type="text/javascript">
         $(document).ready(function () {
             $('#hdnPageNumber').val('1');
             $(window).scroll(function () {
                 if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                     // run our call for pagination  
                     $(window).unbind('scroll');
                     GetPagination();
                    // $.getScript('/WebAssets/JS/custom.js', function () {});
                     setTimeout(function () {
                         $.getScript('/library/javascript/jquery-infinity-scrolling.js', function () { });
                     }, 1000);
                 }
             });
             var pageNumber;
             var pageValue = 6;
             // Initial Data Bind
             $.ajax(
                 {
                     type: "POST",
                     url: "/library/handlers/Videos.asmx/GetPrimaryData",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: "{ id: " + <%=folderId%> + "}",
                     async: false,
                     success: function (msg) {
                         if (msg.d != "") {
                             $("div.one-col").append(msg.d);
                         }
                     }
                 });

                function GetPagination() {
                 pageNumber = $('#hdnPageNumber').val();
                 if (pageNumber != 0) {
                     $.ajax(
                     {
                         type: "POST",
                         url: "/library/handlers/Videos.asmx/GetPaginationData",
                         data: "{ pageNumber: " + pageNumber + ",  id: " + <%=folderId%> + "}",
                             contentType: "application/json; charset=utf-8",
                             dataType: "json",
                             async: true,
                             success: function (msg) {
                                 if (msg.d != "") {
                                     $("div.one-col").append(msg.d);
                                     $('#hdnPageNumber').val(Number(pageNumber) + Number(pageValue));
                                 } else {
                                     $('#hdnPageNumber').val(0);
                                 }
                             }
                         });
                     }
             }
         });
    </script>  
</asp:Content>

