﻿using System;
using System.Web.UI.WebControls;

using Edwards.Portal;
using Edwards.Portal.Repositories;
using Ektron.Cms;
using Ektron.Cms.Framework.UI;


public partial class library_templates_Edwards_Partner_Portal_videos : AuthenticatedPageBase
{
    public long folderId = 0;
    private FeaturedPageRepository featuredPage = new FeaturedPageRepository();

    public override void Error(string message)
    {
        jsAlert(message);
    }

    public override void Notify(string message)
    {
        jsAlert(message);
    }

    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        //Form.Controls.Add(lit);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        EnsureAccess();
        if (this.CurrentUser.Id != 0 && this.CurrentUser.PartnerType != "None")
        {
            folderId = featuredPage.GetVideoContentFolderItem(this.CurrentUser.PartnerType);
        }

        Css.Register(this, new Ektron.Cms.CommonApi().SitePath + "library/styles/stylesheets/salesrep.css", BrowserTarget.All, false); 
    }
  }