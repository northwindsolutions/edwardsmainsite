﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Organization;
using Ektron.Cms.PageBuilder;
//using Ektron.Cms.API;
using Ektron.Cms.Framework.Organization;
using Edwards.Utility;
using Ektron.Cms.Framework.UI;

namespace Edwards.MasterPage
{
    public partial class MainMaster : System.Web.UI.MasterPage
    {
        private long headerMenuId = Constants.MenuIds.MainHeaderMenu;
        private long footerMenuId = Constants.MenuIds.MainFooterMenu;

        public string YouTubeUrl = Constants.SocialUrl.YouTube;
        public string FacebookUrl = Constants.SocialUrl.Facebook;
        public string TwitterUrl = Constants.SocialUrl.Twitter;
        public string LinkedInUrl = Constants.SocialUrl.Linkedin;
        public string YouKuUrl = Constants.SocialUrl.YouKu;

        protected void Page_Init(object sender, EventArgs e)
        {
            LoadMenus();

            // Register the PageBuilder edit mode stylesheet to fix CSS rules that conflict in edit mode.
            var pb = Page as PageBuilder;
            if (pb != null && pb.Status == Mode.Editing)
            {
                Ektron.Cms.API.Css.RegisterCss(Page, @"/library/styles/stylesheets/pagebuilder.css", "PBCorrections");
            }

            analyticsPlaceholder.Visible = Constants.DeploymentMode == Constants.DeploymentModes.Prod;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResourcePackages();

            // Show YouKu icon in footer if Chinese, else always show YouTube icon
            int currentLanguageId = Constants.CurrentLanguageId;
            if (currentLanguageId == 2052)
            {
                YouKuIconLit.Visible = true;
                YouKuIconLit.Text = "<a href=" + YouKuUrl + " class='swapImage' target='_blank'><img src='/library/images/common/youku-0.png' alt='YouKu' title='YouKu'></a>";
            }
            else
            {
                YouTubeIconLit.Visible = true;
                YouTubeIconLit.Text = "<a href='" + YouTubeUrl + "' class='swapImage' target='_blank'><img src='/library/images/common/youtube-0.png' alt='YouTube' title='YouTube'></a>";
            }
        }

        private void RegisterResourcePackages()
        {
            var api = new Ektron.Cms.CommonApi();

            var javascriptPackage = new Package
            {
                Components = new List<Component>
                {
                    Packages.EktronCoreJS,
                    JavaScript.Create(api.SitePath + "library/javascript/imagesloaded.pkgd.min.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/jquery.jcarousellite.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/text-resize.min.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/jquery-hover.js"),
                    JavaScript.Create(api.SitePath + "library/fancybox/source/jquery.fancybox.pack.js"),
                    JavaScript.Create(api.SitePath + "library/fancybox/source/helpers/jquery.fancybox-media.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/twitter-fetcher.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/jquery.elevateZoom-3.0.8.min.js"),
                    JavaScript.Create(api.SitePath + "library/javascript/scripts.js")
                }
            };
            
            javascriptPackage.Register(this, true);

            var cssPackage = new Package
            {
                Components = new List<Component>
                {
                    Css.Create(api.SitePath + "library/styles/stylesheets/screen.css", BrowserTarget.All, "screen, projection"),
                    Css.Create(api.SitePath + "library/styles/stylesheets/print.css", BrowserTarget.All, "print"),
                    Css.Create(api.SitePath + "library/fancybox/source/jquery.fancybox.css", BrowserTarget.All, "screen")
                }
            };

            cssPackage.Register(this, false);
        }

        private void LoadMenus()
        {
            var cacheKey = string.Format("MainMaster-LoadMenus-headerMenuId-{0}", headerMenuId);
            var md = Constants.BackendApiCacheInterval > 0 ? HttpContext.Current.Cache[cacheKey] as string : null;
            if (md != null)
            {
                uxMenuListView.Visible = false;
                uxMenuListViewOutput.Text = md;
            }

            var cacheKeyFooter = string.Format("MainMaster-LoadMenus-footerMenuId-{0}", footerMenuId);
            var fmd = Constants.BackendApiCacheInterval > 0 ? HttpContext.Current.Cache[cacheKeyFooter] as string : null;
            if (fmd != null)
            {
                uxFooterListView.Visible = false;
                uxFooterListViewOutput.Text = fmd;
            }

            if (md == null || fmd == null)
            {
                var menuManager = new MenuManager();

                if (md == null)
                {
                    // Load the top menu navigation...
                    IMenuData menuBaseData = menuManager.GetTree(headerMenuId);
                    if (menuBaseData != null)
                    {
                        uxMenuListView.DataSource = menuBaseData.Items;
                        uxMenuListView.DataBind();
                        var output = DynamicControls.RenderControl(uxMenuListView);
                        if (output != null)
                        {
                            if (Constants.BackendApiCacheInterval > 0)
                            {
                                HttpContext.Current.Cache.Add(cacheKey, output, null, DateTime.MaxValue,
                                    new TimeSpan(0, 0, Constants.BackendApiCacheInterval), CacheItemPriority.Normal, null);
                            }
                        }
                    }
                }
                if (fmd == null)
                {
                    // Load the footer menu navigation...
                    IMenuData footerBaseData = menuManager.GetTree(footerMenuId);
                    if (footerBaseData != null)
                    {
                        uxFooterListView.DataSource = footerBaseData.Items;
                        uxFooterListView.DataBind();
                        var output = DynamicControls.RenderControl(uxFooterListView);
                        if (output != null)
                        {
                            if (Constants.BackendApiCacheInterval > 0)
                            {
                                HttpContext.Current.Cache.Add(cacheKey, output, null, DateTime.MaxValue,
                                    new TimeSpan(0, 0, Constants.BackendApiCacheInterval), CacheItemPriority.Normal, null);
                            }
                        }
                    }
                }
            }
        }
    }
}
