$(function() {

    // Responsive Iframe - not requried if not using responsiveiframe.js
    var ri = responsiveIframe();
    ri.allowResponsiveEmbedding();

    // fastclick.js
    FastClick.attach(document.body);

    // Optionally reload on resize to avoid issues with the responsivness
    // $(window).resize(function(){
        // location.reload();
        // listenWidth();
    // });
    // $(document).load($(window).bind("resize", listenWidth));

    // If on mobile screen, position the stats panel below the timeline (rather than above)
    function listenWidth( e ) {
        if($(window).width()<570)
        {
            $(".stats").remove().insertAfter($(".timeline"));
        } else {
            $(".stats").remove().insertBefore($(".timeline"));
        }
        return false;
    }
    listenWidth();

    // Initialise jcarousel for left and right scrolling
    $('.timeline').jcarousel();

    // Previous (scroll left) carousel action
    $('.timeline-prev')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
            $(this).addClass('active');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
            $(this).removeClass('active');
        })
        .jcarouselControl({
            event: 'click',
            target: '-=2' // Move 2 decades at once
        });

    // Next (scroll right) carousel action
    $('.timeline-next')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
            $(this).addClass('active');
            // $(this).fadeIn('fast');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
            $(this).removeClass('active');
            // $(this).fadeOut();
        })
        .jcarouselControl({
            event: 'click',
            target: '+=2' // Move 2 decades at once
        });

    // Parse the timeline item data from items.json, and create HTML in DOM
    $.getJSON( "js/items.json", function( data ) {
            $.each(data, function (key, cat) { // For each decade:
                var $li = $('<li class="decade' + key + '" />');
                $li.append($('<p class="decade-label">' + key + '</p>'));
                var $items = $('<ul class="items">')
                $items.appendTo($li);

                $.each(cat,function(i,item) { // For each item in the decade:
                    var photo = "";
                    if(item.Photo != "") { // Add popup photo if item has one
                       photo = '<div class="item-photo"><img src="images/items/'+item.Photo+'.jpg" alt="'+item.Title+'"></div>';
                    }
                    $items.append($(
                          '<li>'
                          + photo
                            + '<a href="#" class="dot '+item.Category+'"></a>'
                            
                            + '<div class="content">'
                                + '<img class="cat-icon" src="images/icons/'+item.Category+'.png" alt="'+item.Category+'">'
                                + '<p class="year">'+item.Year+'</p>'
                                + '<div class="title">'
                                    + '<h2>'+item.Title+'</h2>'
                                + '</div>'
                                + '<div class="description">'
                                    + '<p>'+item.Body+'</p>'
                                + '</div>'
                                + photo
                            + '</div>'
                        + '</li>'
                    ));
                });
                $li.appendTo( "ul.decades" );
            });
            $('.timeline').jcarousel('reload');

            // Display Default Timeline Item
            $("ul.items li:eq(3) .content").show();
            $("ul.items li:eq(3) > div.item-photo").show();
            $("ul.items li:eq(3) .content > div.item-photo").hide();
            $("ul.items li:eq(3) .dot").addClass("active");

            // When Legend item is clicked on:
            $(".legend ul li a").on("click touchend",function() {

                // Reset items: Hide all currently displayed
                $(".dot").removeClass("active");
                $(".dot").next(".content").fadeOut('fast');
                $(".dot").prev("div.item-photo").fadeOut('fast');

                if($(".legend ul li a.active")[0] && !$(this).hasClass('active')) {

                    // Active the clicked on legend item
                    $(this).removeClass("deactive");
                    $(this).addClass("active");

                    $(".dot."+$(this).data('cat')).removeClass("deactive");
                    
                } else if($(".legend ul li a.active")[0] && $(this).hasClass('active')) {

                    $(this).removeClass("active");
                    $(this).addClass("deactive");

                    $(".dot."+$(this).data('cat')).addClass("deactive");

                // When first legend item is clicked on:
                } else { 

                    // Deactive all legend items
                    $(".legend ul li a").addClass("deactive");

                    // Active the clicked on legend item
                    $(this).removeClass("deactive");
                    $(this).addClass("active");

                    // Deactivate dots excluding clicked on category
                    $(".dot:not(."+$(this).data('cat')+")").addClass("deactive");

                }

                // Reset if last category is deselected
                if(!$(".legend ul li a.active")[0]) {
                    $(".legend ul li a").removeClass("active");
                    $(".legend ul li a").removeClass("deactive");
                    $(".dot").removeClass("deactive");
                }  

            });
            
            // When a dot is moved over (or tapped)
            $(".dot").on("mouseover touchend",function() {

                // Make all dots inactive
                $(".dot").removeClass("active");
                $(".dot").next(".content").fadeOut('fast');
                $(".dot").prev("div.item-photo").fadeOut('fast');

                // If current dot is active, show it's contents
                if(!$(this).hasClass('deactive')) {
                    $(this).addClass("active");
                    $(this).next(".content").fadeIn('fast');
                    $(this).prev("div.item-photo").fadeIn('fast');
                }
                return false;
            });

            // $(".dot").on("mouseout",function() {
            //     $(this).next(".content").fadeOut('fast');
            //     $(this).prev("div.item-photo").fadeOut('fast');
            //     return false;
            // });   

        });   

});