﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountRepresentative.ascx.cs" Inherits="widgets_AccountRepresentative" %>
    <asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
        <asp:View ID="View" runat="server">
            <!-- You Need To Do ..............................  -->
            <asp:Repeater ID="UxRepeater" runat="server">
                <HeaderTemplate>
                    <%if(lstContentType.Count > 0){ %>
                    <h3>Your Representative</h3>
                    <%} %>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="salesrepImg"><%#DataBinder.Eval(Container.DataItem, "SmartForm.HeadShot.img.src") != null?"<img src=\""+DataBinder.Eval(Container.DataItem, "SmartForm.HeadShot.img.src")+"\" width=\"150\" height=\"170\" alt=\""+DataBinder.Eval(Container.DataItem, "SmartForm.HeadShot.img.alt")+"\"/>":"" %></div>
                    <div class="salesrepdetails">                        
                   <p><h4><%#!String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SmartForm.FirstName").ToString())? DataBinder.Eval(Container.DataItem, "SmartForm.FirstName").ToString()+ " ":""%><%# DataBinder.Eval(Container.DataItem, "SmartForm.LastName").ToString() %><br /></h4></p>
                    <p><%#DataBinder.Eval(Container.DataItem, "SmartForm.Designation") != null?DataBinder.Eval(Container.DataItem, "SmartForm.Designation")+", ":"" %><%#GetRegionLabel(DataBinder.Eval(Container.DataItem, "SmartForm.Region").ToString()) %></p>
                    <%#!String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SmartForm.StreetAddress").ToString())?"<p>"+DataBinder.Eval(Container.DataItem, "SmartForm.StreetAddress").ToString()+"</p>":"" %>
                    <p><%#!String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SmartForm.City").ToString())?DataBinder.Eval(Container.DataItem, "SmartForm.City").ToString() + ", ":"" %> <%#DataBinder.Eval(Container.DataItem, "SmartForm.State").ToString() %> <%#DataBinder.Eval(Container.DataItem, "SmartForm.ZipCode").ToString() %></p>
                    <%#!String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SmartForm.PhoneNumber").ToString())?"<p>Phone: "+DataBinder.Eval(Container.DataItem, "SmartForm.PhoneNumber").ToString()+"</p>":"" %>
                    <%#!String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SmartForm.FaxNumber").ToString())?"<p>Fax: "+DataBinder.Eval(Container.DataItem, "SmartForm.FaxNumber").ToString()+"</p>":"" %>
                    <%#!String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SmartForm.EmailAddress").ToString())?"<p>"+DataBinder.Eval(Container.DataItem, "SmartForm.EmailAddress").ToString()+"</p>":"" %>  
                    </div>                          
                    <br/>                        
                </ItemTemplate>
            </asp:Repeater>
            <!-- End To Do ..............................  -->
        </asp:View>      
    </asp:MultiView>

