using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;

using Edwards.Portal;
using Edwards.Utility;
using Ektron.Edwards;
using Ektron.Edwards.Models.AccountRepresentative;
using Edwards.Portal.Services;

public partial class widgets_AccountRepresentative : System.Web.UI.UserControl, IWidget
{
    #region properties
    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
    public List<ContentType<AccountRepresentative>> lstContentType = new List<ContentType<AccountRepresentative>>();
    public ContentTypeManager<AccountRepresentative> contenttypeManager = new ContentTypeManager<AccountRepresentative>(); 
    protected void Page_Init(object sender, EventArgs e)
    {   
        string sitepath = new CommonApi().SitePath;
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronModalJS);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronModalCss);
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        m_refMsg = m_refContentApi.EkMsgRef;
        _host.Title = m_refMsg.GetMessage("Account Representative");
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        AuthenticationService _authentication = new AuthenticationService();
        string region =  _authentication.GetCurrentUser().Region;               
        lstContentType = contenttypeManager.MakeList(ContentUtility.GetListBySmartformIdAndFolder(Constants.SmartFormIds.AccountRepresentative, Constants.FolderIds.AccountRepresentativeFolderId));
        lstContentType = lstContentType.Where(c => (c.SmartForm.Region.ToString() == region)).ToList();
        UxRepeater.DataSource = lstContentType;
        UxRepeater.DataBind();
    }

    protected string GetRegionLabel(string regionId)
    {
        switch (regionId)
        {
            case "Asia":
                return GetGlobalResourceObject("EdwardsPortal", "RegionAsia").ToString();
            case "Americas":
                return GetGlobalResourceObject("EdwardsPortal", "RegionAmericas").ToString();
            case "EMEA":
                return GetGlobalResourceObject("EdwardsPortal", "RegionEMEA").ToString();
        }

        return "";
    }
}




