﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="ArticleList.ascx.cs" Inherits="widgets_ArticleList" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <h3><asp:Label ID="uxSectionHeader" runat="server"></asp:Label></h3>
        <asp:ListView ID="uxArticleListView" runat="server">
            <EmptyDataTemplate>
                <div>
                    <br />
                    <%= GetLocalResourceObject("NoArticles.Text") %>
                </div>
            </EmptyDataTemplate>
            <LayoutTemplate>
                <div id="articleList">
                    <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="item">
                    <asp:Image ID="uxArticleImage" runat="server" />
                    <strong><asp:Label ID="uxArticleTitle" runat="server"></asp:Label></strong><br>
                    <asp:Literal ID="uxArticleSubHeader" runat="server"></asp:Literal>
                    <br><br>
                    <asp:HyperLink ID="uxArticleLink" runat="server"></asp:HyperLink>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditTitleLabel" runat="server"></asp:Label>
                    <asp:TextBox ID="uxEditTitleTxBox" runat="server" Columns="35"></asp:TextBox>
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditMarketDropDownLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditMarketDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>
