using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Organization;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_ArticleList : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Article List Widget";
    private string _sitepath = string.Empty;
    private long pageId = 0;
    private long marketCategory = Constants.TaxonomyIds.MarketsCategory;
    private long knowledgeCategory = Constants.TaxonomyIds.KnowledgeCentreCategory;
    private long articleTaxId = Constants.TaxonomyIds.Articles;
    private long? smartformId = Constants.SmartFormIds.ArticleItem;

    [WidgetDataMember("Header Title: ")]
    public string HeaderTextBoxLabel { get; set; }

    [WidgetDataMember("Article Type: ")]
    public string MarketDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string ArticleHeader { get; set; }

    [WidgetDataMember("")]
    public string SelectedMarket { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["pageId"])) {
            long.TryParse(Request.QueryString["pageId"], out pageId);
        }

        if (Page.Items["itemTaxonomyId"] == null) {
            TaxonomyUtility.LoadCategoryIdsIntoPageItems(Page, pageId);
        }

        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath; 
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        SharedControls.PopulateMarketTaxonomyDropDown(uxEditMarketDropDown, marketCategory);
        AddItemsToTaxonomyDropDown(uxEditMarketDropDown, knowledgeCategory);

        uxEditTitleLabel.Text = HeaderTextBoxLabel;
        uxEditMarketDropDownLabel.Text = MarketDropDownLabel;
        if (!string.IsNullOrEmpty(HeaderTextBoxLabel)) { uxEditTitleTxBox.Text = ArticleHeader; }
        if (!string.IsNullOrEmpty(SelectedMarket)) { uxEditMarketDropDown.SelectedValue = SelectedMarket; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        ArticleHeader = uxEditTitleTxBox.Text; 
        SelectedMarket = uxEditMarketDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        List<ArticleInfo> outputList = new List<ArticleInfo>();
        long _itemId;

        if (long.TryParse(SelectedMarket, out _itemId))
        {
            var taxCritList = new List<long>(new long[] { articleTaxId, _itemId });
            var articleList = ContentUtility.GetListOfArticles(smartformId, taxCritList);

            int maxItems = (articleList.Count < 5) ? articleList.Count : 5;
            if (articleList != null && articleList.Count > 0)
            {
                for (int i = 0; i < maxItems; i++)
                {
                    outputList.Add(articleList[i]);
                }
            }
        }

        uxSectionHeader.Text = ArticleHeader;
        LoadListResults(outputList);
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void LoadListResults(List<ArticleInfo> articleList)
    {
        uxArticleListView.DataSource = articleList;
        uxArticleListView.ItemDataBound += (sender, args) =>
        {
            var articleItem = args.Item.DataItem as ArticleInfo;

            if (articleItem == null)
                return;

            var thumbnail = args.Item.FindControl("uxArticleImage") as Image;
            var title = args.Item.FindControl("uxArticleTitle") as Label;
            var subHeader = args.Item.FindControl("uxArticleSubHeader") as Literal;
            var link = args.Item.FindControl("uxArticleLink") as HyperLink;

            if (thumbnail == null || title == null
                || subHeader == null || link == null)
                return;

            thumbnail.ImageUrl = articleItem.ThumbSrc;
            thumbnail.AlternateText = articleItem.ThumbAlt;
            title.Text = articleItem.Title;
            subHeader.Text = articleItem.SubHeadline;
            link.Text = articleItem.LinkText;
            link.NavigateUrl = articleItem.LinkSrc;
            link.Target = articleItem.LinkTarget;
        };
        uxArticleListView.DataBind();
    }

    public static void AddItemsToTaxonomyDropDown(DropDownList taxonomyDropDown, long taxonomyId)
    {
        List<TaxonomyData> filterList = TaxonomyUtility.GetTaxonomyChildrenByParentId(taxonomyId);

        if (filterList.Count != null && filterList.Count > 0)
        {
            foreach (var items in filterList)
            {
                taxonomyDropDown.Items.Add(new ListItem(
                    items.Name + " Knowledge Centre (" + items.Id.ToString() + ")", items.Id.ToString()));
            }
        }
    }
}

