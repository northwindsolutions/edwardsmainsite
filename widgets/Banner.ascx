﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="Banner.ascx.cs" Inherits="widgets_Banner" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div id="banner" runat="server">
            <div class="containerWrap">
                <div id="bannerCopy">
                    <asp:Literal ID="uxBannerContentLit" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <strong><asp:Label ID="uxEditBannerDropDownLabel" runat="server"></asp:Label></strong>
            <asp:DropDownList Id="uxEditBannerDropDown" runat="server"></asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
        </div>
    </asp:View>
</asp:MultiView>

