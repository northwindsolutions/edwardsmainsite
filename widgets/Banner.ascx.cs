using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_Banner : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Banner Widget";
    private string _sitepath = string.Empty;
    private long? smartformId = Constants.SmartFormIds.Banner;

    [WidgetDataMember("")]
    public string BannerDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedBanner { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateBannerDropDown();
        uxEditBannerDropDownLabel.Text = BannerDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedBanner)) { uxEditBannerDropDown.SelectedValue = SelectedBanner; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedBanner = uxEditBannerDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;

        if (long.TryParse(SelectedBanner, out contentId) && contentId > 0)
        {
            var bannerInfo = ContentUtility.GetBanner(contentId);

            if (bannerInfo != null)
            {
                banner.Attributes.Add("style", "background-image:url(" + bannerInfo.Image + "); background-position: 50% 100%");
                uxBannerContentLit.Text = bannerInfo.Content;
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateBannerDropDown()
    {
        Dictionary<long, string> bannerDict = new Dictionary<long, string>();
        List<ContentData> bannerList = ContentUtility.GetListBySmartformId(smartformId);

        if (bannerList != null & bannerList.Count > 0)
        {
            bannerDict.Add(-1, "Select a Banner Content..."); //default value
            foreach (ContentData item in bannerList)
            {
                bannerDict.Add(item.Id, item.Title + " (" + item.Id + ")");
            }
        }

        if (!string.IsNullOrEmpty(SelectedBanner)) { uxEditBannerDropDown.SelectedValue = SelectedBanner; }

        uxEditBannerDropDown.DataSource = bannerDict;
        uxEditBannerDropDown.DataTextField = "Value";
        uxEditBannerDropDown.DataValueField = "Key";
        uxEditBannerDropDown.DataBind();
    }
}

