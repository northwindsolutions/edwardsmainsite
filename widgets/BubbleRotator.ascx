﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="BubbleRotator.ascx.cs" Inherits="widgets_BubbleRotator" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">

        <h3><asp:Label ID="uxRotatorHeader" runat="server"></asp:Label></h3>

        <div id="marketContainer" runat="server">
            <h4><asp:Label ID="uxTopText" runat="server" Visible="false"></asp:Label></h4>
            <div id="marketNav">
                <a href="#" class="marketPrev"></a>
                <a href="#" class="marketNext"></a> 
            </div>
            <div id="marketWrap">
                <div id="marketSlideshow" <%= bIsViewFull ? "class='full'" : string.Empty %>>
                    <asp:Repeater runat="server" ID="phRotatorSlide">
                        <ItemTemplate>
                            <div class="slide <%# Container.ItemIndex == 2 ? "selected" : string.Empty %>">
                                <asp:Image runat="server" ID="image"/>
                                <span class="label">
                                    <span runat="server" id="icon"></span>
                                    <asp:Literal runat="server" ID="title"></asp:Literal>
                                </span>
                                <span class="copy">
                                    <asp:Literal runat="server" ID="copyText"></asp:Literal>
                                    <br/>
                                    <asp:HyperLink runat="server" ID="link" />
                                </span>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <asp:HyperLink ID="uxMainLink" runat="server" Visible="false"></asp:HyperLink>
        </div>
        <div id="marketContent">

        </div>
        <script>
            (function ($) {
                var interval = <%= RotationInterval.HasValue ? (RotationInterval * 1000).ToString() : "null" %>;
                if (interval) {
                    var $next = $("#marketNav .marketNext");
                    var autoScroll = function () {
                        $next.click();
                    };
                    var timer = setInterval(autoScroll, interval);

                    var $clip = $("#marketWrap, #marketNav");
                    $clip.on("mouseenter", function (e) {
                        clearInterval(timer);
                    });
                    $clip.on('mouseleave', function (e) {
                        timer = setInterval(autoScroll, interval);
                    });
                }
            })($);
        </script>

    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditRotatorDropDownLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditRotatorDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditViewCheckBoxLabel" runat="server"></asp:Label>
                    <asp:CheckBox ID="uxEditViewCheckBox" runat="server" Checked="true" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

