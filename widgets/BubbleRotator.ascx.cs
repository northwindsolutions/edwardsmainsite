using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_BubbleRotator : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Bubble Rotator Widget";
    private string _sitepath = string.Empty;
    private long? smartformId = Constants.SmartFormIds.BubbleRotator;

    [WidgetDataMember("Bubble Rotator: ")]
    public string RotatorDropDownLabel { get; set; }
    
    [WidgetDataMember("")]
    public string SelectedRotator { get; set; }

    [WidgetDataMember("Full Width: ")]
    public string ViewLabel { get; set; }

    [WidgetDataMember(true)]
    public bool bIsViewFull { get; set; }

    [WidgetDataMember("")]
    public string Result { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    public int? RotationInterval { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateRotatorDropDown();

        uxEditRotatorDropDownLabel.Text = RotatorDropDownLabel;
        uxEditViewCheckBoxLabel.Text = ViewLabel;
        uxEditViewCheckBox.Checked = bIsViewFull;
        if (!string.IsNullOrEmpty(SelectedRotator)) { uxEditRotatorDropDown.SelectedValue = SelectedRotator; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedRotator = uxEditRotatorDropDown.SelectedValue;
        bIsViewFull = uxEditViewCheckBox.Checked;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedRotator, out contentId) && contentId > 0)
        {
            var rotatorList = ContentUtility.GetBubbleRotatorList(contentId);

            if (rotatorList != null && rotatorList.Count > 0)
            {
                uxRotatorHeader.Text = rotatorList[0].Title;
                RotationInterval = rotatorList[0].RotationInterval;

                DisplayTopText(rotatorList[0]);
                DisplayFileDownloadLink(rotatorList[0]);
                DisplayBubbleRotator(rotatorList);

                if (bIsViewFull)
                    marketContainer.Attributes.Add("class", "wide");
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateRotatorDropDown()
    {
        List<ContentData> rotatorList = ContentUtility.GetListBySmartformId(smartformId);

        if (rotatorList != null && rotatorList.Any())
        {
            uxEditRotatorDropDown.Items.Clear();
            uxEditRotatorDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in rotatorList)
            {
                uxEditRotatorDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void DisplayBubbleRotator(List<BubbleRotatorInfo> rotatorList)
    {
        if (rotatorList != null)
        {
            phRotatorSlide.DataSource = rotatorList;
            phRotatorSlide.ItemDataBound += (sender, args) =>
            {
                if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                    return;

                var rotator = args.Item.DataItem as BubbleRotatorInfo;
                if (rotator == null)
                    return;

                var image = args.Item.FindControl("image") as Image;
                var spanIcon = args.Item.FindControl("icon") as HtmlGenericControl;
                var title = args.Item.FindControl("title") as Literal;
                var copyText = args.Item.FindControl("copyText") as Literal;
                var link = args.Item.FindControl("link") as HyperLink;
                if (image == null || spanIcon == null || title == null || copyText == null || link == null)
                    return;

                image.ImageUrl = rotator.ImageSrc;
                image.AlternateText = rotator.ImageAlt;
                image.Attributes.Add("title", rotator.ImageAlt);
                if (!string.IsNullOrEmpty(rotator.Icon))
                {
                    spanIcon.Attributes.Add("class", rotator.Icon);
                }
                else
                {
                    spanIcon.Visible = false;
                }
                title.Text = rotator.Label;
                copyText.Text = rotator.Description;
                link.NavigateUrl = rotator.ItemLinkSrc;
                link.Target = rotator.ItemLinkTarget ?? "_self";
                link.Text = rotator.ItemLinkText;

            };
            phRotatorSlide.DataBind();
        }
    }

    protected void DisplayTopText(BubbleRotatorInfo bubbleInfo)
    {
        if (!string.IsNullOrEmpty(bubbleInfo.TopText))
        {
            uxTopText.Visible = true;
            uxTopText.Text = bubbleInfo.TopText;
        }
    }

    protected void DisplayFileDownloadLink(BubbleRotatorInfo bubbleInfo)
    {
        if (!string.IsNullOrEmpty(bubbleInfo.FileLinkSrc))
        {
            uxMainLink.Visible = true;
            uxMainLink.NavigateUrl = bubbleInfo.FileLinkSrc;
            uxMainLink.Text = bubbleInfo.FileLinkText;
            uxMainLink.Target = bubbleInfo.FileLinkTarget;
        }
    }
}

