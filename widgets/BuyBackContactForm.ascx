﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="BuyBackContactForm.ascx.cs" Inherits="Widget_BuyBackContactForm" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div id="form">
            <h2><b><%= GetLocalResourceObject("Header.Text") %></b></h2>
            <p><%= GetLocalResourceObject("Description.Text") %></p>
            <br />
            <div class="full-width">
                <h4><%= GetLocalResourceObject("ContactInfo.Text") %></h4>
                <div class="form-item required">
                    <label><%= GetLocalResourceObject("Fullname.Text") %> *</label>
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="ip" required />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="reqtxtFirstName" 
                            runat="server" ControlToValidate="txtFirstName" ValidationGroup="ContactFormGroup" 
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item required">
                    <label><%= GetLocalResourceObject("Company.Text") %> *</label>
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="ip" required />
                    
                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="reqtxtCompany" 
                            runat="server" ControlToValidate="txtCompany" ValidationGroup="ContactFormGroup" 
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item required">
                    <label><%= GetLocalResourceObject("Email.Text") %> *</label>
                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="ip" required />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="reqtxtEmailAddress" 
                            runat="server" ControlToValidate="txtEmailAddress" ValidationGroup="ContactFormGroup" 
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>

            <div class="full-width">
                <br />
                <h4><%= GetLocalResourceObject("EquipmentDetails.Text") %></h4>
                <div class="form-item required">
                    <label><%= GetLocalResourceObject("Product.Text") %></label>
                    <asp:TextBox ID="txtProduct" runat="server" CssClass="ip" />

                    <div class="form-validation">
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item required">
                    <label><%= GetLocalResourceObject("ProductSerialNumber.Text") %> *</label>
                    <asp:TextBox ID="txtSerialNumber" runat="server" CssClass="ip" required />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="reqtxtSerialNumber" 
                            runat="server" ControlToValidate="txtSerialNumber" ValidationGroup="ContactFormGroup" 
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item">
                    <label><%= GetLocalResourceObject("AdditionalEquipmentSerialNumbers.Text") %></label>
                    <asp:TextBox ID="txtAdditionalEquipment" runat="server" TextMode="MultiLine" CssClass="ip" />

                    <div class="form-validation">
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item">
                    <label><%= GetLocalResourceObject("OriginalOwner.Text") %></label>
                    <asp:DropDownList  ID="DropDownOrigOwner" runat="server" CssClass="ip">
                    </asp:DropDownList>

                    <div class="form-validation">
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item">
                    <label><%= GetLocalResourceObject("DescribeProcess.Text") %></label>
                    <asp:TextBox ID="txtProcess" runat="server" TextMode="MultiLine" CssClass="ip" />

                    <div class="form-validation">
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item">
                    <label><%= GetLocalResourceObject("DescribeCondition.Text") %> *</label>
                    <asp:DropDownList  ID="DropDownPumpCondition" runat="server" CssClass="ip" required>
                    </asp:DropDownList>

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="reqDropDownPumpCondition" 
                            runat="server" ControlToValidate="DropDownPumpCondition" ValidationGroup="ContactFormGroup" 
                            Display="Dynamic" Visible="false" InitialValue="Choose an option"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item">
                    <label><%= GetLocalResourceObject("PumpRequiresDecontamination.Text") %></label>
                    <asp:DropDownList  ID="DropDownDecontamination" runat="server" CssClass="ip">
                    </asp:DropDownList>

                    <div class="form-validation">
                    </div>
                </div>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item required">
                    <label><%= GetLocalResourceObject("PriceExpenctations.Text") %></label>
                    <asp:TextBox ID="txtPrice" runat="server" CssClass="ip" />

                    <div class="form-validation">
                    </div>
                </div>
            </div>

            <div class="full-width">
                <br />
                <label><%= GetLocalResourceObject("RequiredItems.Text") %></label>
            </div>
            <div class="full-width">
                <br />
                <div class="form-item">
                    <asp:Button ID="uxButton" CssClass="submitBtn" runat="server" ValidationGroup="ContactFormGroup" onclick="btnSubmit_Click" />

                    <br />
                    <br />
                    <div class="form-validation">
                        <asp:Label ID="uxErrorMessage" runat="server" Visible="false" />
                    </div>
                </div>
            </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditToEmailLabel" runat="server" /><br />
                    <asp:TextBox ID="uxEditToEmailTextBox" Columns="35" runat="server" />
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditRedirectLabel" runat="server" /><br />
                    <asp:TextBox ID="uxEditRedirectTextBox" Columns="35" runat="server" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" ValidateRequestMode="Disabled" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>
