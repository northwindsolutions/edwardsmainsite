using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using System.Resources;
using Edwards.Utility;
using Edwards.Models;


public partial class Widget_BuyBackContactForm : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Edwards Buy Back Contact Us Form Widget";
    private string _sitepath = string.Empty;

    [WidgetDataMember("Email To Address:")]
    public string ToEmailLabel { get; set; }

    [WidgetDataMember("")]
    public string EdwardsToEmail { get; set; }

    [WidgetDataMember("Redirect \"Thank you\" Page:")]
    public string RedirectPageLabel { get; set; }

    [WidgetDataMember("")]
    public string RedirectPage { get; set; }

    private string SitePath
    {
        get
        {
            if (string.IsNullOrEmpty(_sitepath))
            {
                _sitepath = new CommonApi().SitePath;
            }
            return _sitepath;
        }
    }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();

    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        uxEditToEmailLabel.Text = ToEmailLabel;
        uxEditRedirectLabel.Text = RedirectPageLabel;

        if (!string.IsNullOrEmpty(EdwardsToEmail)) { uxEditToEmailTextBox.Text = EdwardsToEmail; }
        if (!string.IsNullOrEmpty(RedirectPage)) { uxEditRedirectTextBox.Text = RedirectPage; }

        ViewSet.SetActiveView(Edit);
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        EdwardsToEmail = uxEditToEmailTextBox.Text;
        RedirectPage = uxEditRedirectTextBox.Text;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        var pb = Page as PageBuilder;
        bool bRequired = (pb.Status != Mode.Editing) ? true : false;

        reqtxtFirstName.Text = "Full Name Cannot Be Blank.";
        reqtxtCompany.Text = "Company Cannot Be Blank.";
        reqtxtEmailAddress.Text = "Email Address Cannot Be Blank.";
        reqtxtSerialNumber.Text = "Product Serial Number Cannot Be Blank.";
        reqDropDownPumpCondition.Text = "Please Select a Pump Condition Option";

        if (bRequired)
        {
            txtFirstName.Attributes.Add("required", "");
            txtCompany.Attributes.Add("required", "");
            txtEmailAddress.Attributes.Add("required", "");
            txtSerialNumber.Attributes.Add("required", "");
            DropDownPumpCondition.Attributes.Add("required", "");
        }

        reqtxtFirstName.Visible = (bRequired);
        reqtxtCompany.Visible = (bRequired);
        reqtxtEmailAddress.Visible = (bRequired);
        reqtxtSerialNumber.Visible = (bRequired);
        reqDropDownPumpCondition.Visible = (bRequired);

        //Choose Yes No
        Dictionary<string, string> YesNoDictionary = new Dictionary<string, string>()
        {
            {GetLocalResourceObject("ChooseYesNo.Text").ToString(), GetLocalResourceObject("ChooseYesNo.Text").ToString() },
            {GetLocalResourceObject("Yes.Text").ToString(), GetLocalResourceObject("Yes.Text").ToString()},
            {GetLocalResourceObject("NO.Text").ToString(), GetLocalResourceObject("No.Text").ToString()}
        };

        // Original Owner Drop Down
        DropDownOrigOwner.DataSource = YesNoDictionary;
        DropDownOrigOwner.DataTextField = "Value";
        DropDownOrigOwner.DataValueField = "Key";
        DropDownOrigOwner.DataBind();

        //Decontamination Drop Down
        DropDownDecontamination.DataSource = YesNoDictionary;
        DropDownDecontamination.DataTextField = "Value";
        DropDownDecontamination.DataValueField = "Key";
        DropDownDecontamination.DataBind();

        //Decontamination Drop Down
        Dictionary<string, string> DescribeConditionDictionary = new Dictionary<string, string>()
        {
            {GetLocalResourceObject("ChooseAnOption.Text").ToString(), GetLocalResourceObject("ChooseAnOption.Text").ToString() },
            {GetLocalResourceObject("ProductUsedSinceLastService.Text").ToString(), GetLocalResourceObject("ProductUsedSinceLastService.Text").ToString()},
            {GetLocalResourceObject("ProductFunctioningFullService.Text").ToString(), GetLocalResourceObject("ProductFunctioningFullService.Text").ToString()},
            {GetLocalResourceObject("ProductInoperableFullService.Text").ToString(), GetLocalResourceObject("ProductInoperableFullService.Text").ToString()},
            {GetLocalResourceObject("ProductInoperableNotRepairable.Text").ToString(), GetLocalResourceObject("ProductInoperableNotRepairable.Text").ToString()},
            {GetLocalResourceObject("IDontKnow.Text").ToString(), GetLocalResourceObject("IDontKnow.Text").ToString()}
        };

        DropDownPumpCondition.DataSource = DescribeConditionDictionary;
        DropDownPumpCondition.DataTextField = "Value";
        DropDownPumpCondition.DataValueField = "Key";
        DropDownPumpCondition.DataBind();


        //Bind Submit button text
        uxButton.Text = GetLocalResourceObject("SubmitButton.Text").ToString();
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Exception ex;
        if (sendMail(out ex))
        {
            Response.Redirect(RedirectPage, true);
        }
        else
        {
            uxErrorMessage.Text = string.Format("<strong>{0}</strong><br />", GetLocalResourceObject("Error.Text"));
            uxErrorMessage.Visible = true;
        }
    }

    bool sendMail(out Exception e)
    {
        try
        {
            StringBuilder buff = new StringBuilder();

            buff.Append("Submitted on " + DateTime.Now + "<br />");
            buff.Append("Buy Back and Trade-In Offer Form<br /><br />");
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Full Name", txtFirstName.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Company", txtCompany.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Email", txtEmailAddress.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Please indicate the product you wish to be bought back", txtProduct.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Please enter the product serial number", txtSerialNumber.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Additional equipment serial numbers", txtAdditionalEquipment.Text);
            buff.AppendFormat("<b>{0}</b> {1}<br /><br />", "Are you the Original Owner?", DropDownOrigOwner.SelectedValue);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Process or application where the pump has been used", txtProcess.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Pump Condition", DropDownPumpCondition.SelectedValue);
            buff.AppendFormat("<b>{0}</b> {1}<br /><br />", "Does the pump require decontamination?", DropDownDecontamination.SelectedValue);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", "Price expectiations", txtPrice.Text);

            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(EdwardsToEmail));
            message.Subject = "Buy Back and Trade-In Offer Form";
            message.IsBodyHtml = true;
            message.Body = buff.ToString();

            SmtpClient client = new SmtpClient();
            client.Send(message);
            e = null;
            return true;
        }
        catch (Exception ex)
        {
            e = ex;
            return false;
        }
    }
}

