using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_Callout : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Callout Widget";
    private string _sitepath = string.Empty;
    private string controlPath = Constants.BaseCalloutControlsFolder;
    private long? smartformId = Constants.SmartFormIds.Callout;

    [WidgetDataMember("Callout: ")]
    public string EditDropDownLabel { get; set; }

    [WidgetDataMember("Callout View: ")]
    public string ViewDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedCallout { get; set; }

    [WidgetDataMember("")]
    public string SelectedViewControl { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateCalloutDropDown();
        SharedControls.PopulateViewDropDown(uxCalloutViewDropDown, controlPath);

        uxCalloutDropDownLabel.Text = EditDropDownLabel;
        uxCalloutViewLabel.Text = ViewDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedCallout)) { uxCalloutDropDown.SelectedValue = SelectedCallout; }
        if (!string.IsNullOrEmpty(SelectedViewControl)) { uxCalloutViewDropDown.SelectedValue = SelectedViewControl; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedCallout = uxCalloutDropDown.SelectedValue;
        SelectedViewControl = uxCalloutViewDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedCallout, out contentId) && contentId > 0)
        {
            var cdata = ContentUtility.GetItemByContentId(contentId);
            if (cdata != null)
            {
                if (cdata.XmlConfiguration != null && cdata.XmlConfiguration.Id == Constants.SmartFormIds.CalloutGroup)
                {
                    var ctrl = LoadControl("~/library/includes/controls/callouts/StandardGroup.ascx") as library_includes_controls_callouts_StandardGroup;
                    if (ctrl == null)
                        return;
                    ctrl.Initialize(cdata);
                    phCalloutContent.Controls.Add(ctrl);
                    phCalloutContent.Visible = true;
                }
                else
                {
                    var calloutInfo = ContentUtility.GetCallout(contentId);
                    if (calloutInfo != null)
                    {
                        if (DynamicControls.LoadCalloutView(calloutInfo, Page, phCalloutContent, SelectedViewControl))
                        {
                            phCalloutContent.Visible = true;
                        }
                        else
                        {
                            phCalloutContent.Visible = false;
                        }
                    }
                }
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateCalloutDropDown()
    {
        List<ContentData> calloutList = ContentUtility.GetListBySmartformId(smartformId);
        // Also add in the callout group content as well.
        calloutList.InsertRange(0, ContentUtility.GetListBySmartformId(Constants.SmartFormIds.CalloutGroup));

        if (calloutList != null && calloutList.Any())
        {
            uxCalloutDropDown.Items.Clear();
            uxCalloutDropDown.Items.Add(new ListItem("Select a Callout...", "")); //default value
            foreach (ContentData item in calloutList)
            {
                uxCalloutDropDown.Items.Add(new ListItem(
                    Server.HtmlDecode(item.Title) + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }
}

