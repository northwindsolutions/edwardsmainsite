﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="CaseStudyCallout.ascx.cs" Inherits="widgets_MarketSearch" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div class="callout">
                <h3><%= GetLocalResourceObject("CaseStudies.Text") %></h3>
                <div class="block">
                    <strong><%= GetLocalResourceObject("SearchBySector.Text") %></strong>
                    <div class="form-item">
                        <asp:DropDownList ID="uxSectorDropDown" class="ip" AppendDataBoundItems="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="uxSectorDropDown_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <br>
                    <strong><%= GetLocalResourceObject("SearchByRegion.Text") %></strong>
                    <div class="form-item">
                        <asp:DropDownList ID="uxRegionDropDown" class="ip" AppendDataBoundItems="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="uxRegionDropDown_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <br />
                    <div id="uxFeaturedDiv" runat="server" visible="false">
                    <strong>Featured</strong>
                        <asp:ListView ID="uxFeaturedListView" runat="server">
                            <EmptyDataTemplate>
                                <div>
                                    <br />
                                    <%= GetLocalResourceObject("NoFeaturedItems.Text") %>
                                </div>
                            </EmptyDataTemplate>
                            <LayoutTemplate>
                                <ul class="downloads-list">
                                    <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li><asp:HyperLink ID="uxFeaturedLink" runat="server"></asp:HyperLink></li>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditFeaturedLabel" runat="server" /><br />
                    <asp:CheckBox ID="uxEditFeaturedCheckBox" runat="server" Checked="true" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>