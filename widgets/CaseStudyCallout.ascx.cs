using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_MarketSearch : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Market Search Widget";
    private string _sitepath = string.Empty;
    private long? caseTaxId = Constants.TaxonomyIds.CaseStudies;
    private long featuredCaseTaxId = Constants.TaxonomyIds.FeaturedCaseStudies;
    private long marketCategoryId = Constants.TaxonomyIds.MarketsCategory;
    private long regionCategoryId = Constants.TaxonomyIds.RegionCategory;

    [WidgetDataMember("Included Featured Items:")]
    public string FeaturedLabel { get; set; }

    [WidgetDataMember(true)]
    public bool bIsFeaturedExist { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 

        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        uxEditFeaturedLabel.Text = FeaturedLabel;
        uxEditFeaturedCheckBox.Checked = bIsFeaturedExist;
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        bIsFeaturedExist = uxEditFeaturedCheckBox.Checked;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        SharedControls.PopulateSiteTaxonomyDropDown(uxSectorDropDown, marketCategoryId);
        uxSectorDropDown.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ChooseMarket.Text"), ""));
        SharedControls.PopulateSiteTaxonomyDropDown(uxRegionDropDown, regionCategoryId);
        uxRegionDropDown.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ChooseRegion.Text"), ""));

        var featuredList = ContentUtility.GetListOfCaseStudies(Constants.SmartFormIds.CaseStudy, 
                                                                new List<long>() { featuredCaseTaxId });

        if (bIsFeaturedExist && featuredList != null && featuredList.Count > 0)
        {
            uxFeaturedDiv.Visible = true;
            LoadFeaturedList(featuredList);
        }
        else
        {
            uxFeaturedDiv.Visible = false;
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void uxSectorDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(uxSectorDropDown.SelectedValue))
        {
            Response.Redirect("/case-studies/?sector=" + uxSectorDropDown.SelectedValue);
        }
    }

    protected void uxRegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(uxRegionDropDown.SelectedValue))
        {
            Response.Redirect("/case-studies/?region=" + uxRegionDropDown.SelectedValue);
        }
    }

    protected void LoadFeaturedList(List<CaseStudyInfo> featuredList)
    {
        var firstThree = featuredList.Take(3).ToList();

        uxFeaturedListView.DataSource = firstThree;
        uxFeaturedListView.ItemDataBound += (sender, args) =>
        {
            var caseItem = args.Item.DataItem as CaseStudyInfo;

            if (caseItem == null)
                return;

            var linkControl = args.Item.FindControl("uxFeaturedLink") as HyperLink;

            if (linkControl == null)
                return;

            linkControl.Text = caseItem.LinkText;
            linkControl.NavigateUrl = caseItem.LinkSource;
            linkControl.Target = caseItem.LinkTarget;
        };
        uxFeaturedListView.DataBind();
    }
}

