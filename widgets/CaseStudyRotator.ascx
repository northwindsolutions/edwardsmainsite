﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseStudyRotator.ascx.cs" Inherits="widgets_CaseStudyRotator" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">

        <h3><asp:Label ID="uxRotatorHeader" runat="server"></asp:Label></h3>

        <asp:ListView ID="uxFullCaseStudyView" runat="server">
            <EmptyDataTemplate>
                <br />
                <div>
                    <%= GetLocalResourceObject("NoCaseStudies.Text") %>
                </div>
            </EmptyDataTemplate>
            <LayoutTemplate>
                <div id="slideshowContainer">
                    <div id="slideshowNav">
                        <a href="#" class="prev"></a>
                        <a href="#" class="next"></a>
                    </div>
                    <div id="slideshowWrap">
                        <div id="slideshow">
                            <ul>
                                <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                            </ul>
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li>
                    <asp:Image ID="uxCaseStudyImage" runat="server" />
                    <div class="desc">
                        <strong><asp:Label ID="uxCaseStudyTitle" runat="server"></asp:Label></strong>
                        <br /><br />
                        <asp:Literal ID="uxCaseStudySummary" runat="server"></asp:Literal>
                        <br /><br />
                        <strong><asp:HyperLink ID="uxCaseStudyLink" runat="server"></asp:HyperLink></strong>
                    </div>
                </li>
            </ItemTemplate>
        </asp:ListView>

    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditTitleLabel" runat="server"></asp:Label>
                    <asp:TextBox ID="uxEditTitleTxBox" runat="server"></asp:TextBox>
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditTaxonomyLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditTaxonomyDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

