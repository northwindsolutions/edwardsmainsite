using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_CaseStudyRotator : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Case Study Rotator Widget";
    private string _sitepath = string.Empty;
    private long pageId = 0;
    private long caseTaxId = Constants.TaxonomyIds.CaseStudies;
    private long? smartformId = Constants.SmartFormIds.CaseStudy;

    [WidgetDataMember("Header Title: ")]
    public string HeaderTextBoxLabel { get; set; }

    [WidgetDataMember("Market: ")]
    public string TaxonomyDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedTaxonomy { get; set; }

    [WidgetDataMember("")]
    public string RotatorHeader { get; set; }

    [WidgetDataMember("")]
    public string Result { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }
    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["pageId"])) {
            long.TryParse(Request.QueryString["pageId"], out pageId);
        }

        if (Page.Items["itemTaxonomyId"] == null) {
            TaxonomyUtility.LoadCategoryIdsIntoPageItems(Page, pageId);
        }

        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        SharedControls.PopulateMarketTaxonomyDropDown(uxEditTaxonomyDropDown, (long)Page.Items["marketId"]);

        uxEditTitleLabel.Text = HeaderTextBoxLabel;
        uxEditTaxonomyLabel.Text = TaxonomyDropDownLabel;

        if (!string.IsNullOrEmpty(RotatorHeader)) { uxEditTitleTxBox.Text = RotatorHeader; }
        uxEditTaxonomyDropDown.SelectedValue = !string.IsNullOrEmpty(SelectedTaxonomy) ? SelectedTaxonomy : Page.Items["itemTaxonomyId"].ToString();

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        RotatorHeader = uxEditTitleTxBox.Text;
        SelectedTaxonomy = uxEditTaxonomyDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        List<CaseStudyInfo> caseStudyList = new List<CaseStudyInfo>();
        long _itemId;

        if (long.TryParse(SelectedTaxonomy, out _itemId))
        {
            var taxCritList = new List<long>(new long[] { caseTaxId, _itemId });
            caseStudyList = ContentUtility.GetListOfCaseStudies(smartformId, taxCritList);
        }

        uxRotatorHeader.Text = RotatorHeader;
        LoadListResults(caseStudyList);
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void LoadListResults(List<CaseStudyInfo> caseStudyList)
    {
        uxFullCaseStudyView.DataSource = caseStudyList;
        uxFullCaseStudyView.ItemDataBound += (sender, args) =>
        {
            var caseStudyItem = args.Item.DataItem as CaseStudyInfo;

            if (caseStudyItem == null)
                return;

            var Image = args.Item.FindControl("uxCaseStudyImage") as Image;
            var title = args.Item.FindControl("uxCaseStudyTitle") as Label;
            var summary = args.Item.FindControl("uxCaseStudySummary") as Literal;
            var link = args.Item.FindControl("uxCaseStudyLink") as HyperLink;


            if (Image == null || title == null
                || summary == null || link == null)
                return;


            Image.ImageUrl = caseStudyItem.ImageSrc;
            Image.AlternateText = caseStudyItem.ImageAlt;
            title.Text = caseStudyItem.Title;
            summary.Text = caseStudyItem.Summary;
            link.Text = caseStudyItem.LinkText;
            link.NavigateUrl = caseStudyItem.LinkSource;
            link.Target = caseStudyItem.LinkTarget;
        };
        uxFullCaseStudyView.DataBind();
    }
}

