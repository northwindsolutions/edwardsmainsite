﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseStudySearch.ascx.cs" Inherits="widgets_CaseStudySearch" %>
<%@ Reference Control="~/widgets/NewsList/Views/List.ascx" %>
<%@ Register src="~/widgets/CaseStudySearch/Views/Default.ascx" tagPrefix="EdwardsCaseStudySearch" tagName="DefaultView" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <EdwardsCaseStudySearch:DefaultView runat="server" ID="defaultView" />
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <fieldset>
            </fieldset>
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
