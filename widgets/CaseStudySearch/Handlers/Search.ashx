﻿<%@ WebHandler Language="C#" Class="Search" %>

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Edwards.Utility;
using Ektron.Cms;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public class Search : IHttpHandler
{
    private const int _take = 12;
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        var req = context.Request;
        var res = context.Response;

        if (req.HttpMethod != "POST")
        {
            res.Write(JsonConvert.SerializeObject(JObject.FromObject(new
            {
                error = "Only POST requests are allowed."
            })));
        }

        long? sectorTaxonomyId = null;
        long _sectorTaxonomyId;
        long? regionTaxonomyId = null;
        long _regionTaxonomyId;
        if (long.TryParse(req.Params["region"], out _regionTaxonomyId))
        {
            regionTaxonomyId = _regionTaxonomyId;
        }
        if (long.TryParse(req.Params["sector"], out _sectorTaxonomyId))
        {
            sectorTaxonomyId = _sectorTaxonomyId;
        }

        ICollection<ContentData> results = null;
        results = TaxonomyUtility.GetContentDataListByTaxonomyCategories(new[]
        {
            sectorTaxonomyId.HasValue ? sectorTaxonomyId.Value : Constants.TaxonomyIds.MarketsCategory,
            regionTaxonomyId.HasValue ? regionTaxonomyId.Value : Constants.TaxonomyIds.RegionCategory
        },
        andOperation: true,
        recursive: true, pageNum: 1, take: _take);
        
        if (results != null)
        {
            var smartformObjects = results.Select(cdata => ContentUtility.GetCaseStudyInfo(cdata))
                .Where(s => s != null)
                .ToList();
            
            res.Write(JsonConvert.SerializeObject(smartformObjects.Select(r => JObject.FromObject(new
            {
                title = r.Title,
                summary = r.Summary,
                imgSrc = r.ImageSrc,
                imgAlt = r.ImageAlt,
                quicklink = r.LinkSource,
                linkTarget = r.LinkTarget,
                linkText = r.LinkText
            }))));
        }
        else
        {
            res.Write(JsonConvert.SerializeObject(new
            {
                error = HttpContext.GetGlobalResourceObject("EdwardsGlobal", "NoResultsFound")
            }));
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}