﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Default.ascx.cs" Inherits="widgets_CaseStudySearch_Views_Default" %>
<script>
    var queryForResults = null;

    jQuery(function($) {
        var $controller = $("#caseStudiesSearchController");
        function ngScope() {
            return angular.element($controller[0]).scope();
        }

        queryForResults = function(success) {
            var $scope = ngScope();
            var postData = {region: $scope.region, sector: $scope.market};
            $.post("/widgets/CaseStudySearch/Handlers/Search.ashx",
                postData, 
                function(data, textStatus, jqXHR) {
                    success(data, textStatus, jqXHR);
                },
                "json"
            );
        };
    });

    if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
    EdwardsApp.controller('CaseStudiesSearch',
        function CaseStudiesSearch($scope, $sce) {
            $scope.trustIt = function (htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            }

            $scope.loading = false;
            $scope.error = null;
            $scope.results = <%= JSONResults %>;
            $scope.regions = <%= JSONRegions %>;
            $scope.markets = <%= JSONMarkets %>;

            $scope.region = null;
            $scope.market = null;

            $scope.query = function() {
                $scope.loading = true;
                queryForResults(function(data, textStatus, jqXHR) {
                    $scope.$apply(function($s) {
                        if (data.error) {
                            $s.error = data.error;
                            $s.results = null;
                        } else {
                            $s.results = data;
                        }
                        $s.loading = false;
                    });
                });
            };
        }
    );

</script>
<div id="caseStudiesSearchController" ng-controller="CaseStudiesSearch">
    <h1><%= GetLocalResourceObject("Title.Text") %></h1>
    <div class="one-half">
        <h2><strong><%= GetLocalResourceObject("SearchSector.Text") %></strong></h2>
        <div class="form-item">
            <select class="ip" ng-model="market" ng-options="m.id as m.name for m in markets" ng-change="query()">
                <option value=""><%= GetLocalResourceObject("ChooseSector.Text") %></option>
            </select>
        </div>
    </div>
    <div class="one-half omega">
        <h2><strong><%= GetLocalResourceObject("SearchRegion.Text") %></strong></h2>
        <div class="form-item">
            <select class="ip" ng-model="region" ng-options="r.id as r.name for r in regions" ng-change="query()">
                <option value=""><%= GetLocalResourceObject("ChooseRegion.Text") %></option>
            </select>
        </div>
    </div>
    <br class="clear"><br>
    <p class="clear" ng-show="loading"><img src="/Workarea/images/application/loading_small.gif" alt="<%= GetGlobalResourceObject("EdwardsGlobal", "Loading") %>"/></p>
    <div id="caseStudies">
        <p ng-show="results == null || results.length == 0"><%= GetLocalResourceObject("NoResults.Text") %></p>
        <div ng-repeat="r in results" ng-class="{'one-third': true, 'omega': ($index + 1) % 3 == 0 }">
            <img class="alignnone" ng-src="{{ r.imgSrc }}" alt="{{ r.title }}" title="{{ r.title }}">
            <p><strong>{{ r.title }}</strong><br/>
            <span ng-bind-html="trustIt(r.summary)"></span><br/>
            <a ng-href="{{ r.quicklink }}" target="{{r.linkTarget}}"><strong><%= GetLocalResourceObject("ReadCaseStudy.Text") %></strong></a></p>
        </div>
    </div>
</div>
