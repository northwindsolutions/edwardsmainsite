﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Models;
using Edwards.Utility;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public partial class widgets_CaseStudySearch_Views_Default : System.Web.UI.UserControl
{
    private const int _take = 12;

    public long SectorTaxonomyRoot
    {
        get { return Constants.TaxonomyIds.MarketsCategory; }
    }

    public long RegionTaxonomyRoot
    {
        get { return Constants.TaxonomyIds.RegionCategory; }
    }

    public long SelectedSectorTaxonomyRoot { get; set; }
    public long SelectedRegionTaxonomyRoot { get; set; }

    public string JSONMarkets { get; set; }
    public string JSONRegions { get; set; }
    public string JSONResults { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        JS.RegisterJS(this, "/library/javascript/angular.min.js", "AngularJS");
        //JS.RegisterJS(this, "/library/javascript/underscore-min.js", "UnderscoreJS");

        // get a list of taxonomy children of the root nodes to populate the dropdown lists.
        JSONMarkets = JsonConvert.SerializeObject(TaxonomyUtility.GetTaxonomyChildrenByParentId(SectorTaxonomyRoot)
            .Select(t => JObject.FromObject(new
            {
                id = t.TaxonomyId,
                name = t.Name
            })));
        JSONRegions = JsonConvert.SerializeObject(TaxonomyUtility.GetTaxonomyChildrenByParentId(RegionTaxonomyRoot)
            .Select(t => JObject.FromObject(new
            {
                id = t.TaxonomyId,
                name = t.Name
            })));

        // get the results
        long? sectorTaxonomyId = null;
        long _sectorTaxonomyId;
        long? regionTaxonomyId = null;
        long _regionTaxonomyId;
        if (long.TryParse(Request.Params["region"], out _regionTaxonomyId))
        {
            regionTaxonomyId = _regionTaxonomyId;
        }
        if (long.TryParse(Request.Params["sector"], out _sectorTaxonomyId))
        {
            sectorTaxonomyId = _sectorTaxonomyId;
        }

        ICollection<ContentData> results = null;
        results = TaxonomyUtility.GetContentDataListByTaxonomyCategories(new[]
        {
            sectorTaxonomyId.HasValue ? sectorTaxonomyId.Value : Constants.TaxonomyIds.MarketsCategory,
            regionTaxonomyId.HasValue ? regionTaxonomyId.Value : Constants.TaxonomyIds.RegionCategory
        },
        andOperation: true,
        recursive: true, pageNum: 1, take: _take);

        var smartformObjects = results.Select(cdata => ContentUtility.GetCaseStudyInfo(cdata))
            .Where(s => s != null)
            .ToList();

        JSONResults = JsonConvert.SerializeObject(smartformObjects.Select(r => JObject.FromObject(new
        {
            title = r.Title,
            summary = r.Summary,
            imgSrc = r.ImageSrc,
            imgAlt = r.ImageAlt,
            quicklink = r.LinkSource,
            linkTarget = r.LinkTarget,
            linkText = r.LinkText
        })));
    }
}