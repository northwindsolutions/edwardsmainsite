using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class Widget_ContactUsForm : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Contact Us Form Widget";
    private string _sitepath = string.Empty;
    private long? folderId = Constants.FolderIds.FormsPagesFolderId;
    private long? smartformId = Constants.SmartFormIds.ContactUs;

    [WidgetDataMember("Choose a Contact Us Form:")]
    public string FormDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedFormContent { get; set; }

    [WidgetDataMember("Email To Address:")]
    public string ToEmailLabel { get; set; }

    [WidgetDataMember("")]
    public string EdwardsToEmail { get; set; }

    [WidgetDataMember("Redirect \"Thank you\" Page:")]
    public string RedirectPageLabel { get; set; }

    [WidgetDataMember("")]
    public string RedirectPage { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateFormsDropDown();
        PopulateRedirectQuicklinkDropDown();

        uxEditFormContentlabel.Text = FormDropDownLabel;
        uxEditToEmailLabel.Text = ToEmailLabel;
        uxEditRedirectLabel.Text = RedirectPageLabel;

        if (!string.IsNullOrEmpty(SelectedFormContent)) { uxEditFormDropDown.SelectedValue = SelectedFormContent; }
        if (!string.IsNullOrEmpty(EdwardsToEmail)) { uxEditToEmailTextBox.Text = EdwardsToEmail; }
        if (!string.IsNullOrEmpty(RedirectPage)) { uxEditRedirectDropDown.SelectedValue = RedirectPage; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedFormContent = uxEditFormDropDown.SelectedValue;
        EdwardsToEmail = uxEditToEmailTextBox.Text;
        RedirectPage = uxEditRedirectDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;

        if (long.TryParse(SelectedFormContent, out contentId) && contentId > 0)
        {
            var formItemList = FormUtility.GetContactFormInfo(contentId);

            if (formItemList != null)
            {
                uxContactUsHeader.Text = formItemList.Header;
                uxRequiredText.Text = formItemList.RequiredText;
                uxButton.Text = formItemList.ButtonText;

                InitializeFormControl(formItemList.Question1, uxFormLabel1, RequiredFieldValidator1, uxTextBox1);
                InitializeFormControl(formItemList.Question2, uxFormLabel2, RequiredFieldValidator2, uxTextBox2);
                InitializeFormControl(formItemList.Question3, uxFormLabel3, null, null, uxDropDown3);
                InitializeFormControl(formItemList.Question4, uxFormLabel4, RequiredFieldValidator4, uxTextBox4);
				InitializeFormControl(formItemList.Question5, uxFormLabel5, null, uxTextBox5);

                uxHiddenTB1value.Value = formItemList.Question1.Value;
                uxHiddenTB2value.Value = formItemList.Question2.Value;
                uxHiddenTB3value.Value = formItemList.Question3.Value;
                uxHiddenTB4value.Value = formItemList.Question4.Value;
				uxHiddenTB5value.Value = formItemList.Question5.Value;
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateFormsDropDown()
    {
        var formList = ContentUtility.GetListBySmartformId(smartformId);

        if (formList != null & formList.Count > 0)
        {
            uxEditFormDropDown.Items.Clear();
            uxEditFormDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in formList)
            {
                uxEditFormDropDown.Items.Add(new ListItem(
                item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void PopulateRedirectQuicklinkDropDown()
    {
        var redirectList = ContentUtility.GetListByFolderId(folderId);

        if (redirectList != null & redirectList.Count > 0)
        {
            uxEditRedirectDropDown.Items.Clear();
            uxEditRedirectDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in redirectList)
            {
                uxEditRedirectDropDown.Items.Add(new ListItem(
                item.Quicklink, item.Quicklink.ToString()));
            }
        }
    }

    protected void InitializeFormControl(ContactQuestion questionInfo, Literal formLabel, RequiredFieldValidator fieldValidator, TextBox textBox = null, DropDownList dropDown = null)
    {
        var pb = Page as PageBuilder;

        if (questionInfo != null)
        {
            bool bRequired = (questionInfo.Required == "true" && pb.Status != Mode.Editing) ? true : false;

            if (!string.IsNullOrEmpty(questionInfo.Text)) {
                formLabel.Text = (bRequired) ?
                    "<Label>" + questionInfo.Text + " <span class='req'>*</span></Label>" :
                    "<Label>" + questionInfo.Text + "</Label>";
            }

            if (textBox != null) {
                textBox.Attributes.Add("class", "ip");
                if (bRequired) { textBox.Attributes.Add("Required", ""); }
            }

            if (dropDown != null) {
                dropDown.Attributes.Add("class", "ip");
                if (bRequired) { dropDown.Attributes.Add("Required", ""); }

                foreach (var item in questionInfo.DropDownItems)
                {
                    dropDown.Items.Add(new ListItem(
                        item.Text, item.Value));
                }
            }

            if (fieldValidator != null) {
                fieldValidator.Visible = (bRequired) ? true : false;
                fieldValidator.Text = questionInfo.Value + " "  + (string)GetLocalResourceObject("CannotBeBlank.Text");
            }

        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Exception ex;
        if (sendMail(out ex))
        {
            Response.Redirect(RedirectPage, true);
        }
        else
        {
            uxErrorMessage.Text = string.Format("<strong>{0}</strong><br />", GetLocalResourceObject("Error.Text"));
            uxErrorMessage.Visible = true;
        }
    }

    bool sendMail(out Exception e)
    {
        try
        {
            StringBuilder buff = new StringBuilder();

            buff.Append((string)GetLocalResourceObject("SubmittedOn.Text") + DateTime.Now + "<br />");
            buff.Append(string.Format("{0}<br /><br />", GetLocalResourceObject("InformationSubmittedIntro.Text")));
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", uxHiddenTB1value.Value, uxTextBox1.Text);
			buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", uxHiddenTB5value.Value, uxTextBox5.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", uxHiddenTB2value.Value, uxTextBox2.Text);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", uxHiddenTB3value.Value, uxDropDown3.SelectedValue);
            buff.AppendFormat("<b>{0}</b>: {1}<br /><br />", uxHiddenTB4value.Value, uxTextBox4.Text);

            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(EdwardsToEmail));
            message.Subject = (string) GetLocalResourceObject("EmailSubject.Text");
            message.IsBodyHtml = true;
            message.Body = buff.ToString();

            SmtpClient client = new SmtpClient();
            client.Send(message);
            e = null;
            return true;
        }
        catch (Exception ex)
        {
            e = ex;
            return false;
        }
    }
}

