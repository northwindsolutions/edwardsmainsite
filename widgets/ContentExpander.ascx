﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentExpander.ascx.cs" Inherits="widgets_ContentExpander" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div id="accordion">
            <asp:Repeater ID="uxContentRepeater" runat="server" >
                <ItemTemplate>
                    <div class="<%# (!IsExpandFirstItem) ? "toggle" : (Container.ItemIndex == 0) ? "toggle selected" : "toggle" %>">
                        <span class="icon"></span>
                        <strong><asp:Literal ID="uxTitleLiteral" runat="server" /></strong><br>
                    </div>
                    <div class="expandable" <%# (!IsExpandFirstItem) ? "" : (Container.ItemIndex == 0) ? "style='display: block;'" : "" %>>
                        <asp:Literal ID="uxContentLiteral" runat="server" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditContentLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditContentDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditCheckboxLabel" runat="server"></asp:Label>
                    <asp:CheckBox Id="uxEditFirstItemCheckbox" runat="server" Text="Expand first Item by default"></asp:CheckBox>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

