using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_ContentExpander : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Content Expander Widget";
    private string _sitepath = string.Empty;
    private long? SmartformId = Constants.SmartFormIds.ContentExpander;

    [WidgetDataMember("Content Expander: ")]
    public string ContentDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedContent { get; set; }

    [WidgetDataMember(false)]
    public bool IsExpandFirstItem { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

   public int? RotationInterval { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateRotatorDropDown();

        uxEditContentLabel.Text = ContentDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedContent)) { uxEditContentDropDown.SelectedValue = SelectedContent; }
        uxEditFirstItemCheckbox.Checked = (IsExpandFirstItem) ? true : false;

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedContent = uxEditContentDropDown.SelectedValue;
        IsExpandFirstItem = (uxEditFirstItemCheckbox.Checked) ? true : false;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedContent, out contentId) && contentId > 0)
        {
            var contentList = ContentUtility.GetContentExpanderList(contentId);

            if (contentList != null && contentList.Count > 0)
            {
                LoadListResults(contentList);
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateRotatorDropDown()
    {
        List<ContentData> rotatorList = ContentUtility.GetListBySmartformId(SmartformId);

        if (rotatorList != null & rotatorList.Count > 0)
        {
            uxEditContentDropDown.Items.Clear();
            uxEditContentDropDown.Items.Add(new ListItem("", ""));

            foreach (ContentData item in rotatorList)
            {
                uxEditContentDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void LoadListResults(List<ContentExpanderInfo> contentList)
    {
        uxContentRepeater.DataSource = contentList;
        uxContentRepeater.ItemDataBound += (sender, args) =>
        {
            var contentItem = args.Item.DataItem as ContentExpanderInfo;

            if (contentItem == null)
                return;

            var title = args.Item.FindControl("uxTitleLiteral") as Literal;
            var content = args.Item.FindControl("uxContentLiteral") as Literal;

            if (title == null || content == null)
                return;

            title.Text = contentItem.Title;
            content.Text = contentItem.Content;
        };
        uxContentRepeater.DataBind();
    }
}

