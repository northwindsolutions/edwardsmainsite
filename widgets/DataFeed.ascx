﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="DataFeed.ascx.cs" Inherits="widgets_DataFeed" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div id="expertView">
            <h3><asp:Literal ID="uxHeaderLit" runat="server" /></h3>
            <h4><asp:Literal ID="uxSubHeaderLit" runat="server" /></h4>
            <div id="feedList">
                <asp:Repeater runat="server" ID="uxfeedRepeater">
                    <ItemTemplate>
                        <div class="item" itemprop="event" itemscope itemtype="http://schema.org/NewsArticle">
                            <strong itemprop="headline"><asp:Literal ID="uxTitleLit" runat="server" /></strong><br>
                            <p itemprop="about"><asp:Literal ID="uxDescriptionLit" runat="server" /></p>
                            <span itemprop="datePublished"><asp:Literal ID="uxPubDateLit" runat="server" /></span><br><br>
                            <asp:HyperLink ID="uxArticleLink" runat="server" Target="_blank"></asp:HyperLink><br>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:HyperLink ID="uxShowMoreLink" runat="server" Target="_blank" Visible="false" CssClass="showmore"></asp:HyperLink>
            </div>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditHeaderLabel" runat="server" />
                    <asp:TextBox ID="uxEditHeaderTextBox" runat="server" Style="width: 95%"></asp:TextBox>
                </div>  
                <div class="Item">
                    <asp:Label ID="uxEditSubHeaderLabel" runat="server" />
                    <asp:TextBox ID="uxEditSubHeaderTextBox" runat="server" Style="width: 95%"></asp:TextBox>
                </div>  
                <div class="Item">
                    <asp:Label ID="uxEditFeedDropDownLabel" runat="server" />
                    <br />
                    <asp:DropDownList ID="uxEditFeedDropDown" runat="server"></asp:DropDownList>
                </div>    
                <div class="Item">
                    <asp:Label ID="uxEditFeedSizeLabel" runat="server" />
                    <br />
                    <asp:TextBox ID="uxEditFeedSizeTextBox" runat="server"></asp:TextBox>
                    <br />
                    <label>(leave blank to show all items)</label>
                </div>     
                <div class="Item">
                    <asp:Label ID="uxEditShowMoreLabel" runat="server" />
                    <asp:TextBox ID="uxEditShowMoreTextBox" runat="server" Style="width: 95%"></asp:TextBox>
                    <br />
                    <label>(leave blank for no link)</label>
                </div>   
                <div class="Item">
                    <asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="Button2" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>

