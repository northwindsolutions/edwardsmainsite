using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;

public partial class widgets_DataFeed : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Data Feed Widget";

    private string _sitepath = string.Empty;

    [WidgetDataMember("Header:")]
    public string HeaderLabel { get; set; }

    [WidgetDataMember("")]
    public string Header { get; set; }

    [WidgetDataMember("SubHeader:")]
    public string SubHeaderLabel { get; set; }

    [WidgetDataMember("")]
    public string SubHeader { get; set; }

    [WidgetDataMember("Select a Feed:")]
    public string FeedFileNameLabel { get; set; }

    [WidgetDataMember("")]
    public string FeedFileName { get; set; }

    [WidgetDataMember("Number of Items:")]
    public string FeedSizeLabel { get; set; }

    [WidgetDataMember("3")]
    public string FeedSize { get; set; }

    [WidgetDataMember("Show More URL:")]
    public string ShowMoreLabel { get; set; }

    [WidgetDataMember("")]
    public string ShowMoreLink { get; set; }


    private string SitePath
    {
        get
        {
            if (string.IsNullOrEmpty(_sitepath))
            {
                _sitepath = new CommonApi().SitePath;
            }
            return _sitepath;
        }
    }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        uxEditHeaderLabel.Text = HeaderLabel;
        uxEditSubHeaderLabel.Text = SubHeaderLabel;
        uxEditFeedDropDownLabel.Text = FeedFileNameLabel;
        uxEditFeedSizeLabel.Text = FeedSizeLabel;
        uxEditShowMoreLabel.Text = ShowMoreLabel;

        PopulateFeedTypeDropDown();

        if (!string.IsNullOrEmpty(Header)) { uxEditHeaderTextBox.Text = Header; }
        if (!string.IsNullOrEmpty(SubHeader)) { uxEditSubHeaderTextBox.Text = SubHeader; }
        if (!string.IsNullOrEmpty(FeedFileName)) { uxEditFeedDropDown.SelectedValue = FeedFileName; }
        if (!string.IsNullOrEmpty(FeedSize)) { uxEditFeedSizeTextBox.Text = FeedSize; }
        if (!string.IsNullOrEmpty(ShowMoreLink)) { uxEditShowMoreTextBox.Text = ShowMoreLink; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        Header = uxEditHeaderTextBox.Text;
        SubHeader = uxEditSubHeaderTextBox.Text;
        FeedFileName = uxEditFeedDropDown.SelectedValue;
        FeedSize = uxEditFeedSizeTextBox.Text;
        ShowMoreLink = uxEditShowMoreTextBox.Text;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        uxHeaderLit.Text = Header;
        uxSubHeaderLit.Text = SubHeader;

        if (!string.IsNullOrEmpty(ShowMoreLink))
        {
            uxShowMoreLink.NavigateUrl = ShowMoreLink;
            uxShowMoreLink.Text = GetLocalResourceObject("ShowMore.Text").ToString();
            uxShowMoreLink.Visible = true;
        }

        var items = ParseFile(FeedFileName, FeedSize);
        if (items != null && items.Any())
        {
            LoadListResults(items);
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    /// <summary>
    /// Read the data file and 
    /// </summary>
    /// <param name="filename"></param>
    /// <returns></returns>
    protected IEnumerable<FeedItem> ParseFile(string filename, string feedSize)
    {
        int num;

        if (string.IsNullOrWhiteSpace(filename))
            return null;

        var path = Server.MapPath(filename);
        if (!File.Exists(path))
            return null;

        if (!int.TryParse(feedSize, out num)) {
            num = 100;
        }

            var data = File.ReadAllText(path);
            try
            {
                return XDocument.Parse(data).ToFeedItemList(num);
            }
            catch
            {
                return null;
            }
    }

    protected void PopulateFeedTypeDropDown()
    {
        Dictionary<string, string> FeedDictList = new Dictionary<string, string>()
        {
            {GetLocalResourceObject("VacuumTech.Text").ToString(), Constants.DataFeed.VacuumTechnology},
            {GetLocalResourceObject("SemiconductorTech.Text").ToString(), Constants.DataFeed.SemiconductorTechnology},
        };

        if (FeedDictList != null & FeedDictList.Count > 0)
        {
            uxEditFeedDropDown.Items.Clear();
            uxEditFeedDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (var item in FeedDictList)
            {
                uxEditFeedDropDown.Items.Add(new ListItem(
                item.Key, item.Value));
            }
        }
    }

    protected void LoadListResults(IEnumerable<FeedItem> Items)
    {
        uxfeedRepeater.DataSource = Items;
        uxfeedRepeater.ItemDataBound += (sender, args) =>
        {
            var feedItem = args.Item.DataItem as FeedItem;
            if (feedItem == null)
                return;

            var title = args.Item.FindControl("uxTitleLit") as Literal;
            var description = args.Item.FindControl("uxDescriptionLit") as Literal;
            var link = args.Item.FindControl("uxArticleLink") as HyperLink;
            var pubdate = args.Item.FindControl("uxPubDateLit") as Literal;

            if (title == null || description == null 
                || link == null)
                return;

            title.Text = feedItem.Title;
            description.Text = feedItem.Description;
            link.NavigateUrl = feedItem.LinkUrl;
            link.Text = GetLocalResourceObject("Link.Text").ToString();
            pubdate.Text = feedItem.PubDate;
        };
        uxfeedRepeater.DataBind();
    }
}
