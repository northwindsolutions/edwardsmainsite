using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class downloads_Banner : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Downloads Widget";
    private string _sitepath = string.Empty;
    private string controlPath = Constants.BaseDownloadsControlsFolder;
    private long? smartformId = Constants.SmartFormIds.Downloads;
    private long? folderId = Constants.FolderIds.DownloadsFolderId;

    [WidgetDataMember("Choose Download Link Content:")]
    public string DownloadsDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedDownloadContent { get; set; }

    [WidgetDataMember("Downloads View: ")]
    public string ViewDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedViewControl { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateDownloadsDropDown();
        SharedControls.PopulateViewDropDown(uxEditViewDropDown, controlPath);

        uxEditDownloadLabel.Text = DownloadsDropDownLabel;
        uxEditViewLabel.Text = ViewDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedDownloadContent)) { uxEditDownloadDropDown.SelectedValue = SelectedDownloadContent; }
        if (!string.IsNullOrEmpty(SelectedViewControl)) { uxEditViewDropDown.SelectedValue = SelectedViewControl; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedDownloadContent = uxEditDownloadDropDown.SelectedValue;
        SelectedViewControl = uxEditViewDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedDownloadContent, out contentId) && contentId > 0)
        {
            var downloadsInfo = ContentUtility.GetDownloadLinkList(contentId);
            if (downloadsInfo != null)
            {
                if (DynamicControls.LoadDownloadsView(downloadsInfo, Page, phDownloadsContent, SelectedViewControl))
                {
                    phDownloadsContent.Visible = true;
                }
                else
                {
                    phDownloadsContent.Visible = false;
                }
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateDownloadsDropDown()
    {
        var downloadList = ContentUtility.GetListBySmartformId(smartformId);

        if (downloadList != null & downloadList.Count > 0)
        {
            uxEditDownloadDropDown.Items.Clear();
            uxEditDownloadDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in downloadList)
            {
                uxEditDownloadDropDown.Items.Add(new ListItem(
                item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }
}

