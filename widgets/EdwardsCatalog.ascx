﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EdwardsCatalog.ascx.cs" Inherits="widgets_EdwardsCatalog" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <iframe src="<%= iFrameCatalog %>" width="940" height="<%= iFrameHeight %>"></iframe>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditTextBoxLabel" runat="server"></asp:Label>
                    <asp:TextBox Id="uxEditTextBox" Columns="30" runat="server"></asp:TextBox>
                </div>
                <div class="Item">
                    <asp:Label ID="uxHeightTextBoxLabel" runat="server"></asp:Label>
                    <asp:TextBox Id="uxHeightTextBox" Columns="10" runat="server"></asp:TextBox>px
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>