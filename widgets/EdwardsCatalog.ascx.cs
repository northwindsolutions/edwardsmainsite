using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_EdwardsCatalog : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Edwards Catalog Widget";
    private string _sitepath = string.Empty;
    private string catalogFlipPage = Constants.EdwardsCatalogFlipPage;
    public string iFrameCatalog { get; set; }

    [WidgetDataMember("Edwards Catalog Flipbook Page: ")]
    public string CatalogPageLabel { get; set; }

    [WidgetDataMember("")]
    public string EdwardsCatalogFlip { get; set; }

    [WidgetDataMember("iFrame Height: ")]
    public string iFrameHeightLabel { get; set; }

    [WidgetDataMember("")]
    public string iFrameHeight { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        uxEditTextBoxLabel.Text = CatalogPageLabel;
        uxHeightTextBoxLabel.Text = iFrameHeightLabel;
        uxEditTextBox.Text = !string.IsNullOrEmpty(EdwardsCatalogFlip) ? EdwardsCatalogFlip : catalogFlipPage;
        if (!string.IsNullOrEmpty(iFrameHeight)) { uxHeightTextBox.Text = iFrameHeight; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        EdwardsCatalogFlip = uxEditTextBox.Text;
        iFrameHeight = uxHeightTextBox.Text;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        iFrameCatalog = !string.IsNullOrEmpty(EdwardsCatalogFlip) ? EdwardsCatalogFlip : catalogFlipPage;
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }
}

