﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="EdwardsContentList.ascx.cs" Inherits="widgets_EdwardsContentList" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <asp:Literal ID="uxMainHeader" runat="server" />
        <div id="contentList">
            <asp:Repeater ID="uxContentListRepeater" runat="server">
                <ItemTemplate>
                    <div class="item">
                        <asp:Image ID="uxItemImage" runat="server" />
                        <div class="content">
                            <h2><strong><asp:Literal ID="uxSubHeaderLit" runat="server" /></strong></h2>
                            <asp:Literal ID="uxDescriptionLit" runat="server" />
                            <br>
                            <asp:HyperLink ID="uxHyperLink" runat="server"></asp:HyperLink>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditContentDropDownLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditContentDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>
