using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Organization;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_EdwardsContentList : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Edwards Content List Widget";
    private string _sitepath = string.Empty;
    private long pageId = 0;
    private long? smartformId = Constants.SmartFormIds.ContentList;

    [WidgetDataMember("Content List: ")]
    public string ContentListDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedContentList { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath; 
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateContentListDropDown();

        uxEditContentDropDownLabel.Text = ContentListDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedContentList)) { uxEditContentDropDown.SelectedValue = SelectedContentList; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedContentList = uxEditContentDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;

        if (long.TryParse(SelectedContentList, out contentId))
        {
            var contentList = ContentUtility.GetContentListInfo(contentId);

            if (contentList != null)
            {
                uxMainHeader.Text = String.IsNullOrEmpty(contentList.Header) ?
                                     !String.IsNullOrEmpty(contentList.Icon) ?
                                     "<h3 class=" + contentList.Icon + ">&nbsp;</h3>" :
                                     string.Empty :
                                     !String.IsNullOrEmpty(contentList.Icon) ?
                                     "<h3 class=" + contentList.Icon + ">" + contentList.Header + "</h3>" :
                                     "<h3>" + contentList.Header + "</h3>";

                LoadListResults(contentList.ListItem);
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void LoadListResults(List<ContentListItem> contentItemList)
    {
        uxContentListRepeater.DataSource = contentItemList;
        uxContentListRepeater.ItemDataBound += (sender, args) =>
        {
            var contentItem = args.Item.DataItem as ContentListItem;

            if (contentItem == null)
                return;

            var image = args.Item.FindControl("uxItemImage") as Image;
            var header = args.Item.FindControl("uxSubHeaderLit") as Literal;
            var description = args.Item.FindControl("uxDescriptionLit") as Literal;
            var link = args.Item.FindControl("uxHyperLink") as HyperLink;

            if (image == null || header == null
                || description == null || link == null)
                return;

            image.ImageUrl = contentItem.ImageSrc;
            image.AlternateText = contentItem.ImageAlt;

            header.Text = contentItem.Header;
            description.Text = contentItem.Description;

            link.Text = "<strong>" + contentItem.LinkText + "</strong>";
            link.NavigateUrl = contentItem.LinkSource;
            link.Target = contentItem.LinkTarget;
        };
        uxContentListRepeater.DataBind();
    }

    protected void PopulateContentListDropDown()
    {
        List<ContentData> contentList = ContentUtility.GetListBySmartformId(smartformId);

        if (contentList != null & contentList.Count > 0)
        {
            uxEditContentDropDown.Items.Clear();
            uxEditContentDropDown.Items.Add(new ListItem("", ""));

            foreach (ContentData item in contentList)
            {
                uxEditContentDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }
}

