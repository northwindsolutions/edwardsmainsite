﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="EdwardsNewsletter.ascx.cs" Inherits="widgets_FormCallout" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <h3><asp:Literal ID="uxFormHeader" runat="server" /></h3>
        <div class="block">        
            <input name="thx"  value="http://www.edwardsvacuum.com"  type="hidden" />         
            <input name="err"  value="http://www.edwardsvacuum.com"  type="hidden" />         
            <input name="SubAction"  value="sub"  type="hidden" />         
            <input name="MID"  value="74784"  type="hidden" />         
            <table dropzone="copy"  border="0"  cellpadding="0"  cellspacing="0">                             
                <tbody>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;"><font face="Verdana"  size="2">Name:</font>                </td>                                                                     
                        <td style="cursor: default;">                                                                                         
                            <input name="Full Name"  type="text" />                </td>            </tr>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;"><font face="Verdana"  size="2">Company:</font>                </td>                                                                     
                        <td style="cursor: default;">                                                                                         
                            <input name="User Defined"  type="text" />                </td>            </tr>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;"><font face="Verdana"  size="2">Email:</font>                </td>                                                                     
                        <td style="cursor: default;">                                                                                         
                            <input name="Email Address"  type="text" />                </td>            </tr>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;">                                                                                         
                            <input checked="checked"  name="Email Type"  value="HTML"  type="radio" /><font face="Verdana"  size="2">HTML</font>                </td>                                                                     
                        <td style="cursor: default;">                                                                                         
                            <input name="Email Type"  value="TEXT"  type="radio" /><font face="Verdana"  size="2"> Text</font>                </td>            </tr>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;"  align="right">                                                                                         
                            <input name="lid"  value="887442"  type="checkbox" />                </td>                                                                     
                        <td style="cursor: default;"><font face="Verdana"  size="2">English</font>                </td>            </tr>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;"  align="right">                                                                                         
                            <input name="lid"  value="1101035"  type="checkbox" />                </td>                                                                     
                        <td style="cursor: default;"><font face="Verdana"  size="2">French</font>                </td>            </tr>                                                 
                    <tr>                                                                     
                        <td style="cursor: default;">                                                                                         
                            <input id="submitButton" value="Submit" type="submit" />                </td>                                                                     
                        <td style="cursor: default;"> </td>            </tr>        
                </tbody>    
            </table> 
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxFormLbl" runat="server" /><br />
                    <asp:TextBox ID="uxFormHeaderTxtBx" runat="server" Columns="55" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>

<script type="text/javascript">
    document.getElementById('submitButton').onclick = function () {
        var errorMsg = "";
        var toReturn = true;
        var fMethod = 'Post';
        var fAction = 'http://cl.exct.net/subscribe.aspx?lid=887442';
        var fRedirect = '/Thank_You_Newsletter/';
        var mainForm = document.forms[0];

        if (mainForm != null) {
            var fullName = mainForm["Full Name"].value;
            var company = mainForm["User Defined"].value;
            var email = mainForm["Email Address"].value;

            if (fullName == "") {
                errorMsg += 'Name field cannot be blank!\n';
            }

            if (company == "") {
                errorMsg += 'Company field cannot be blank!\n';
            }

            if (email == "") {
                errorMsg += 'Email field cannot be blank!\n';
            }

            if (errorMsg != "") {
                alert(errorMsg);
                toReturn = false;
            } else {
                mainForm.method = fMethod;
                mainForm.action = fAction;
                mainForm.submit();

                window.location.href = fRedirect;
            }

        } else {
            toReturn = false;
        }
            
        return toReturn;
     }
</script>