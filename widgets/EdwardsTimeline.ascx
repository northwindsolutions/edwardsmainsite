﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EdwardsTimeline.ascx.cs" Inherits="widgets_EdwardsTimeline" %>

<style type="text/css">

	div.site-container{
		max-width: 940px;
		margin:0 auto;

		background-color: gray;
		margin-top: 0;
	}

	.iframe-container {
		max-width:840px;
		margin:0 auto;
	}

	iframe {
		width: 100%;
		border: none;
	}

</style>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div class="site-container">
		    <div class="iframe-container">
			    <iframe id="timeline" src="<%= iFrameTimeline %>" scrolling="no"></iframe>
		    </div>
	    </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditTextBoxLabel" runat="server"></asp:Label>
                    <asp:TextBox Id="uxEditTextBox" Columns="25" runat="server"></asp:TextBox>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

<script>
    ; (function ($) {
        $(function () {
            $('#timeline').responsiveIframe({ xdomain: '*' });
        });
    })(jQuery);
</script>