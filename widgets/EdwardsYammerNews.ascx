﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EdwardsYammerNews.ascx.cs" Inherits="widgets_EdwardsYammerNews" %>
<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div>
            <h3>
                <%= Heading %>
            </h3>            
            <asp:Panel ID="noNewsPanel" runat="server" Visible="false">
                <p>
                    <%= GetGlobalResourceObject("EdwardsPortal", "NoNewsAvailable") %>
                </p>
            </asp:Panel>
            <asp:Repeater ID="regionalNewsItems" runat="server">
                <HeaderTemplate>
                    <ul class="regionalNewsItems">
                </HeaderTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
                <ItemTemplate>
                    <li class="regionalNewsItem">                            
                        <div>
                            <div class="regionalNewsItemTeaser">
                                <%# Eval("Teaser") %>
                                <a runat="server" target="_blank" visible='<%# Eval("ShowLink") %>' href='<%# Eval("Link") %>'><%= GetGlobalResourceObject("EdwardsPortal", "ReadFullArticle") %></a>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
            <p>
                <a href="<%= PrimaryUrl %>" target="_blank"><%= GetGlobalResourceObject("EdwardsPortal", "YammerNewsViewAll") %><span class="icon"></span></a>
            </p>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <p>
                Feed Heading: <asp:TextBox ID="feedHeading" runat="server" placeholder="Enter a heading for the feed"></asp:TextBox>
            </p>
            <p>
                Network URL: <asp:TextBox ID="networkUrl" runat="server" placeholder="Enter a link for accessing the feed"></asp:TextBox>
            </p>
            <table class="regionalFeeds">
                <tr>
                    <th>Region</th>
                    <th>RSS Feed URL</th>
                </tr>
                <asp:Repeater ID="regionalFeeds" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><asp:Literal ID="feedRegion" runat="server" Text='<%# Eval("Region") %>'></asp:Literal></td>
                            <td><asp:TextBox ID="feedUrl" runat="server" placeholder="Enter a URL" Text='<%# Eval("Url") %>'></asp:TextBox></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <div class="regionalFeedActions">
                <asp:Button ID="cancelChanges" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
                <asp:Button ID="saveChanges" runat="server" Text="Save Changes" OnClick="SaveButton_Click" />
            </div>
        </div>
    </asp:View>
</asp:MultiView>