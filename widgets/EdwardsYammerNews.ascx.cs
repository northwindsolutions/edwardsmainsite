﻿using Edwards.Portal;
using Edwards.Portal.Services;
using Ektron.Cms.Common;
using Ektron.Cms.Framework;
using Ektron.Cms.Framework.Settings;
using Ektron.Cms.Instrumentation;
using Ektron.Cms.User;
using Ektron.Cms.Widget;
using Ektron.Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// This class is a widget intended to display regional RSS news feeds
/// to authorized users of the Edwards partner portal. The widget
/// renders news items from a feed associated with the portal user's
/// region.
/// </summary>
public partial class widgets_EdwardsYammerNews : System.Web.UI.UserControl, IWidget
{
    private const string DefaultPrimaryUrl = "https://www.yammer.com/edwardspartnerportal/";

    private readonly CustomPropertyManager _customPropertyManager;
    private readonly AuthenticationService _authenticationService;

    private IWidgetHost _host;    
    private List<FeedUrl> _feedMap;
    private PortalUser _currentUser;

    /// <summary>
    /// Constructor
    /// </summary>
    public widgets_EdwardsYammerNews()
    {
        this._customPropertyManager = new CustomPropertyManager(ApiAccessMode.Admin);
        this._authenticationService = new AuthenticationService();
    }

    [WidgetDataMember]
    public string Feeds { get; set; }

    [WidgetDataMember(DefaultPrimaryUrl)]
    public string PrimaryUrl { get; set; }

    [WidgetDataMember]
    public string Heading { get; set; }

    [WidgetDataMember]
    public bool ShowNetworkUrl { get; set; }


    /// <summary>
    /// Gets or sets the map of RSS feed URLs to region configured
    /// for this widget.
    /// </summary>
    private List<FeedUrl> FeedMap
    {
        get
        {
            return this._feedMap;
        }

        set
        {
            this._feedMap = value;
        }
    }

    /// <summary>
    /// Gets the account details for the current portal user.
    /// </summary>
    private PortalUser CurrentUser
    {
        get
        {
            if (this._currentUser == null)
            {
                this._currentUser = this._authenticationService.GetCurrentUser();
            }

            return this._currentUser;
        }
    }

    /// <summary>
    /// Prepares the widget's event handlers.
    /// </summary>
    protected void Page_Init(object sender, EventArgs e)
    {
        this._host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        this._host.Title = "Yammer Network Feed";
        this._host.Edit += new EditDelegate(EditEvent);
        this._host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        this._host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        this._host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        this._host.ExpandOptions = Expandable.ExpandOnEdit;

        this.PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });

        this.RefreshFeedMap();

        this.ViewSet.SetActiveView(View);
    }

    /// <summary>
    /// Handles the widget's 'Edit' event, displaying the settings view
    /// which allows an administrator to specify the regional RSS
    /// feed URLs.
    /// </summary>
    /// <param name="settings">Widget settings</param>
    private void EditEvent(string settings)
    {        
        this.regionalFeeds.DataSource = this.FeedMap;
        this.regionalFeeds.DataBind();

        this.networkUrl.Text = this.PrimaryUrl;
        this.feedHeading.Text = this.Heading;        

        this.ViewSet.SetActiveView(Edit);
    }

    /// <summary>
    /// Displays the items of the RSS feed associated with the current user's region.
    /// </summary>
    protected void SetOutput()
    {
        this.HideNewsItems();

        var feedUrl = this.GetFeedUrlForCurrentUser();

        if (feedUrl != null && !string.IsNullOrWhiteSpace(feedUrl.Url))
        {
            try
            {
                var feedItems = this.GetFeedItems(feedUrl.Url);

                if (feedItems.Any())
                {
                    this.regionalNewsItems.DataSource = feedItems;
                    this.regionalNewsItems.DataBind();

                    this.ShowNewsItems();
                }
            }
            catch(Exception ex)
            {               
                Log.WriteError("An error occurred while attempting to read portal news feed: " + feedUrl.Url);
                Log.WriteError(ex);
            }
        }
    }

    /// <summary>
    /// Handles the "Click" event of the save button on the widget's settings panel,
    /// persisting the configured regional news feed URLs.
    /// </summary>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in regionalFeeds.Items)
        {
            string region = ((Literal)item.FindControl("feedRegion")).Text;
            string url = ((TextBox)item.FindControl("feedUrl")).Text;

            var feed = this.FeedMap.FirstOrDefault(f => f.Region == region);

            if (feed != null)
            {
                feed.Url = url;
            }
        }

        this.Feeds = JsonConvert.SerializeObject(this.FeedMap);

        this.PrimaryUrl = string.IsNullOrWhiteSpace(networkUrl.Text)
            ? DefaultPrimaryUrl
            : networkUrl.Text;

        this.Heading = string.IsNullOrWhiteSpace(feedHeading.Text)
            ? this.GetGlobalResourceObject("EdwardsPortal", "YammerNewsTitle").ToString()
            : feedHeading.Text;

        this._host.SaveWidgetDataMembers();
        
        this.ViewSet.SetActiveView(View);
    }

    /// <summary>
    /// Handles the "Click" event of the cancel button on the widget's settings panel,
    /// discarding any changes to the widget's regional news feeds.
    /// </summary>
    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    /// <summary>
    /// Directs the widget to display its news items.
    /// </summary>
    private void ShowNewsItems()
    {
        noNewsPanel.Visible = false;
        regionalNewsItems.Visible = true;
    }

    /// <summary>
    /// Directs the widget to hide its new items.
    /// </summary>
    private void HideNewsItems()
    {
        noNewsPanel.Visible = true;
        regionalNewsItems.Visible = false;
    }

    /// <summary>
    /// Gets a collection of all regions to which a portal user may be assigned.
    /// </summary>
    /// <returns>Collection of all available regions</returns>
    private IEnumerable<string> GetRegions()
    {
        List<string> allRegions = new List<string>();

        // Retrieve the Ektron custom property definition for "Region"

        var criteria = new CustomPropertyCriteria();
        criteria.AddFilter(UserCustomProperty.Name, CriteriaFilterOperator.EqualTo, "Region");

        var regionProperty = this._customPropertyManager.GetList(criteria).FirstOrDefault();

        // If "Region" is found, split its delimited list of possible values
        // and add them to the collection to be returned.

        if (regionProperty != null)
        {
            var delimitedRegions = regionProperty.PropertyValidationSelectList ?? string.Empty;
            var filteredRegions = delimitedRegions.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                                                  .Where(r => r.ToLowerInvariant() != "none")
                                                  .OrderBy(r => r);

            allRegions.AddRange(filteredRegions);
        }        

        return allRegions;
    }

    /// <summary>
    /// At any point in time regions may be added to or removed from the system.
    /// This method refreshes this widgets feed map so that it contains any
    /// new regions and eliminates any regions which have been removed.
    /// </summary>
    private void RefreshFeedMap()
    {
        if (this._feedMap == null)
        {
            if (string.IsNullOrWhiteSpace(this.Feeds))
            {
                this._feedMap = new List<FeedUrl>();
            }
            else
            {
                this._feedMap = JsonConvert.DeserializeObject<List<FeedUrl>>(this.Feeds) ?? new List<FeedUrl>();
            }
        }

        var allRegions = this.GetRegions();

        // Remove all regions, which are no longer in the system.

        var obsoleteRegions = this._feedMap.Where(f => !allRegions.Contains(f.Region)).ToList();

        foreach (var region in obsoleteRegions)
        {
            this._feedMap.Remove(region);
        }

        // Add new regions, which have been added to the system.

        var newRegions = allRegions.Where(r => !this._feedMap.Any(f => f.Region == r)).ToList();

        foreach (string region in newRegions)
        {
            this._feedMap.Add(new FeedUrl { Region = region, Url = string.Empty });
        }
    }

    /// <summary>
    /// Retrieves a feed URL from the widget's feed map, which corresponds to the
    /// current user's region.
    /// </summary>
    /// <returns></returns>
    private FeedUrl GetFeedUrlForCurrentUser()
    {
        return this.FeedMap.FirstOrDefault(f => f.Region == this.CurrentUser.Region);
    }

    /// <summary>
    /// Reads the RSS feed represented by the specified URL and returns a
    /// collection of its first 5 news items.
    /// </summary>
    /// <param name="url">RSS feed URL</param>
    /// <returns>Collection of items from the RSS feed</returns>
    private IEnumerable<FeedItem> GetFeedItems(string url)
    {
        IEnumerable<FeedItem> items;

        using (XmlReader feedReader = XmlReader.Create(url))
        {
            SyndicationFeed feed = SyndicationFeed.Load(feedReader);
            items = feed.Items                
                .OrderByDescending(si => si.PublishDate.LocalDateTime)
                .Select(this.ToFeedItem)
                .GroupBy(fi => fi.Link)
                .Select(fi => fi.First())
                .Take(10);
                
        }

        return items;
    }

    /// <summary>
    /// Returns a FeedItem object adapted from the specified SyndicationItem object.
    /// </summary>
    /// <param name="item">SyndicationItem object to be adapted</param>
    /// <returns>FeedItem object adapted from the specified SyndicationItem</returns>
    private FeedItem ToFeedItem(SyndicationItem item)
    {
        // Retrieve the source URL for the RSS feed item's
        // first link field. (Yammer: Web URL)

        string sourceUrl;
        if (item.Links.Any())
        {
            sourceUrl = item.Links.First().Uri.AbsoluteUri;
        }
        else
        {
            sourceUrl = string.Empty;
        }

        // Retrieve the formatted teaser text from the RSS feed
        // item's "Title" field.(Yammer: Content Excerpt)

        string teaserText = this.FormatTeaser(item.Title.Text);

        return new FeedItem
        {
            Title = string.Empty,   // No title available for yammer messages
            Teaser = teaserText,    // RSS Title field
            Link = sourceUrl        // RSS links field?
        };
    }

    /// <summary>
    /// Formats the specified teaser text, stripping it of markup and
    /// truncating to approximately 250 characters.
    /// </summary>
    /// <param name="teaserText">Teaser text to be formatted</param>
    /// <returns>Formatted teaser text</returns>
    private string FormatTeaser(string teaserText)
    {
        var strippedInput = Regex.Replace(teaserText, @"<[^>]+>", " ").Trim();
        var normalizedInput = Regex.Replace(strippedInput, @"\s{2,}", " ");

        return this.TruncateText(normalizedInput, 250);
    }

    /// <summary>
    /// Truncates the specified input to the first word break after
    /// the specified character length.
    /// </summary>
    /// <param name="input">Text to truncate</param>
    /// <param name="characterLength">Target text length</param>
    /// <returns>Text truncated to approximately the specified character length</returns>
    private string TruncateText(string input, int characterLength)
    {
        string truncatedInput = null;

        if (input.Length >= characterLength)
        {
            var indexOfBreak = input.IndexOf(' ', characterLength);

            if (indexOfBreak > 0)
            {
                truncatedInput = input.Substring(0, indexOfBreak).TrimEnd();
            }
        }

        return truncatedInput ?? input;
    }

    /// <summary>
    /// The FeedItem class is a model describing an individual news
    /// item within the Edwards partner portal.
    /// </summary>
    public class FeedItem
    {
        public string Title { get; set; }
        public string Teaser { get; set; }
        public string Link { get; set; }
        
        public bool ShowLink
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Link);
            }
        }
    }

    /// <summary>
    /// The FeedUrl class is a model describing an individual news
    /// feed for an Edwards partner portal region.
    /// </summary>
    [Serializable]
    public class FeedUrl
    {
        public string Region { get; set; }
        public string Url { get; set; }
    }
}