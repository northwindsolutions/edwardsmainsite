﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EktronCustomSearch.ascx.cs" Inherits="widgets_EktronCustomSearch" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
    <script>
        $(document).ready(function() {
            doSomething();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(doSomething);
        })

        function doSomething() {
            $('ul.stabs li').click(function() {
                var tab_id = $(this).attr('data-tab');

                $('ul.stabs li').removeClass('current');
                $('.stab-content').removeClass('current');

                $(this).addClass('current');
                $("#" + tab_id).addClass('current');
            })
        }
</script>
    <style>
    .scontainer{
        width: 100%;
      margin: 0 auto;
    }

    ul.stabs {
    margin: 0px;
    padding: 0px;
    list-style: none !important;
    display: inline-flex !important;
}
    ul.stabs li{
      background: #fff;
      color: #003554;
      display: inline-block;
      padding: 10px 15px;
      cursor: pointer;
    }

    ul.stabs li.current {
    background: #003554;
    color: #fff;
    margin: 0 0 0em 0em !important;
}

    .stab-content{
      display: none;
      background: #fff;
      border-style: solid;
      border-color: #003554;
      border-top-width: 2px;
      padding: 15px;
    }

    .stab-content.current{
      display: inherit;
    }
</style>

        
        <div class="form-item">
        <asp:TextBox ID="txtSearchCriteria" Width="200px" runat="server" CssClass="ip" />
        <asp:Button ID="btnSearch" runat="server" CssClass="submitBtn" OnClick="btnSearch_Click" Text="Search" /></div>
        <br /><br />
      

        <div class="scontainer">                                                      
    <ul class="stabs">                                                                                              
        <li class="stab-link current"  data-tab="stab-1">Corporate Results</li>                                                                                              
        <li class="stab-link"  data-tab="stab-2">Web Shop Results</li>  </ul>                                                    
    <div id="stab-1"  class="stab-content current">    
    
                    <asp:Repeater ID="rptSearchResults" runat="server">
                        <HeaderTemplate>
					<ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                               <strong class="title"><a href='/<%#Eval("QuickLink") %>'><%#Eval("Title") %></a></strong>
                                <%#Eval("Summary") %>
                                	</li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                      </div>                                                                    
    <div id="stab-2"  class="stab-content">                                                                                         
        <iframe id="searchiframe" runat="server"  src="https://shop.edwardsvacuum.com/search/search.aspx"  scrolling="auto"  style="border: currentColor; width: 900px; height: 1600px;"
         allowtransparency="true" ></iframe>  </div>
</div>

					


    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
