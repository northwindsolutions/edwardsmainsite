using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.WebPages.Html;
using Edwards.Utility;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using Edwards.Models;
using Edwards.Utility;
using java.awt;
using Ektron.Cms.Framework;
using Ektron.Cms.Instrumentation;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Expressions;
using Content = Ektron.Cms.API.Content.Content;

public partial class widgets_EktronCustomSearch : System.Web.UI.UserControl, IWidget
{
    #region properties

    public int CurrentLanguage
    {
        get { return new Ektron.Cms.Controls.LanguageAPI().CurrentLanguageID; }
    }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string searchQuery = Request.QueryString["q"];
        if (!String.IsNullOrEmpty(searchQuery) && String.IsNullOrEmpty(txtSearchCriteria.Text))
        {
            PerformEktronSearch(searchQuery);
            setupShopSearch(searchQuery);
            txtSearchCriteria.Text = searchQuery;
        }
    }

     protected void btnSearch_Click(object sender, EventArgs e)
     {
        PerformEktronSearch(txtSearchCriteria.Text);
        setupShopSearch(txtSearchCriteria.Text);
     }

    protected void setupShopSearch(string criteria)
    {
        if (!String.IsNullOrWhiteSpace(criteria))
        {
            searchiframe.Src = string.Format("https://shop.edwardsvacuum.com/search/search.aspx?q={0}&language={1}", criteria, CurrentLanguage);
        }
    }

    protected void PerformEktronSearch(string criteria)
    {
        if (!String.IsNullOrWhiteSpace(criteria))
        {
            var languageId = new Ektron.Cms.Controls.LanguageAPI().CurrentLanguageID;
            Ektron.Cms.Framework.Search.SearchManager searchManager =
                new Ektron.Cms.Framework.Search.SearchManager(ApiAccessMode.Admin);

            var searchCriteria = new KeywordSearchCriteria();
            searchCriteria.ReturnProperties = new HashSet<PropertyExpression>()
            {
                SearchContentProperty.Title,
                SearchContentProperty.Id,
                SearchContentProperty.QuickLink,
                SearchContentProperty.FolderId,
                SearchContentProperty.Language,
                SearchContentProperty.HighlightedSummary,
                SearchContentProperty.ContentType,
                SearchContentProperty.Path,
            };
            searchCriteria.ExpressionTree = SearchContentProperty.Language.EqualTo(searchManager.ContentLanguage);
            searchCriteria.ImplicitAnd = true;
            searchCriteria.QueryText = criteria;
            searchCriteria.PagingInfo = new PagingInfo(50);

            SearchResponseData response = null;
            try
            {
                response = searchManager.Search(searchCriteria);
            }
            catch (ArgumentNullException ex)
            {
                Log.WriteError("Search Error: " + ex.Message);
            }


            if (response != null)
            {
                var searchResults = new SearchResults(response.Results, languageId);

                rptSearchResults.DataSource = searchResults.Entries;
                rptSearchResults.DataBind();
            }

        }
    }




    public class SearchResult
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string QuickLink { get; set; }
        public long EbiContentId { get; set; }
        public int LanguageId { get; set; }
    }

    public class SearchResults
    {
        private ContentData GetContent(Int64 contentId, Int32 languageId)
        {
            ContentData cd = new ContentData();
            if (contentId > 0 && languageId > 0)
            {
                Ektron.Cms.API.Content.Content content = new Ektron.Cms.API.Content.Content();
                content.ContentLanguage = languageId;
                cd = content.GetContent(contentId);
            }
            return cd;
        }
        public List<SearchResult> Entries;

        public SearchResults(List<Ektron.Cms.Search.SearchResultData> ecktronsResults, int languageId)
        {
            List<long> ContentTypeIds = new List<long> { 1, 3333 };
            Entries = new List<SearchResult>();

            ecktronsResults.ForEach(x =>
            {
                var result = new SearchResult()
                {
                    Title = x["title"] != null ? x["title"].ToString() : String.Empty
                };

                // Try to get Ebi Content
                long EbiContentId;
                long EbiContentTypeId;

                long.TryParse(x["ebicontentid"] != null ? x["ebicontentid"].ToString() : "0", out EbiContentId);
                long.TryParse(x["ebicontenttype1"] != null ? x["ebicontenttype1"].ToString() : "0", out EbiContentTypeId);

                // Filter on Content Type
                if (ContentTypeIds.Contains(EbiContentTypeId))
                {
                    // Get the 'Ed' content if availabel
                    if (EbiContentId > 0)
                    {
                        ContentData content = GetContent(EbiContentId, languageId);
                        if (content != null)
                        {
                            result.QuickLink = string.IsNullOrWhiteSpace(content.Quicklink) ? "#" : content.Quicklink;
                            result.Summary = content.Teaser;
                            result.LanguageId = content.LanguageId;

                            if (result.Summary.Length == 0)
                                result.Summary = "";

                            Entries.Add(result);
                        }

                    }
                }
            });
        }
    }
}

