﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FAQ.ascx.cs" Inherits="widgets_Faq" %>
<%@ Register src="~/widgets/FAQ/Views/Default.ascx" tagPrefix="Edwards" tagName="FaqDefaultView" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <fieldset>
                <p>
                    <label>Title</label>
                    <asp:TextBox runat="server" ID="txtTitle"></asp:TextBox>
                </p>
                <p>
                    <label>Summary Text</label>
                    <asp:TextBox runat="server" ID="txtSummary"></asp:TextBox>
                </p>
                <p>
                    <label>Page Size</label>
                    <asp:TextBox runat="server" ID="txtPageSize"></asp:TextBox>
                </p>
                <p>
                    <label>View Control</label>
                    <asp:DropDownList runat="server" ID="viewDropdown">
                        <asp:ListItem Text="(Choose a View Control)" Value=""></asp:ListItem>
                        <asp:ListItem Selected="True" Text="Default (Filter View)" Value="Default.ascx"></asp:ListItem>
                        <asp:ListItem Text="Knowledge Page Callout" Value="KnowledgeCallout.ascx"></asp:ListItem>
                    </asp:DropDownList>
                </p>
            </fieldset>
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
