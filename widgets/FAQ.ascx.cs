using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.WebPages.Html;
using Edwards;
using Edwards.Utility;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using System.Configuration;

public partial class widgets_Faq : System.Web.UI.UserControl, IWidget
{
    #region properties

    protected const string ViewPathFormat = "~/widgets/FAQ/Views/{0}";

    public int CurrentLanguage
    {
        get { return Constants.CurrentLanguageId; }
    }

    [WidgetDataMember("")]
    public string Title { get; set; }

    [WidgetDataMember("")]
    public string Summary { get; set; }

    [WidgetDataMember("")]
    public string ViewControl { get; set; }

    [WidgetDataMember(10)]
    public int PageSize { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        string sitepath = new CommonApi().SitePath;
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronModalJS);
        //JS.RegisterJS(this, "/library/javascript/angular.min.js", "AngularJS");
        //JS.RegisterJS(this, "/library/javascript/underscore-min.js", "UnderscoreJS");
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronModalCss);
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        m_refMsg = m_refContentApi.EkMsgRef;
        _host.Title = m_refMsg.GetMessage("FAQ Widget");
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        CancelButton.Text = m_refMsg.GetMessage("btn cancel");
        SaveButton.Text = m_refMsg.GetMessage("btn save");  
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        string myPath = string.Empty;
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ek_helpDomainPrefix"]))
        {
            string helpDomain = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            if ((helpDomain.IndexOf("[ek_cmsversion]") > 1))
            {
                myPath = helpDomain.Replace("[ek_cmsversion]", new CommonApi().RequestInformationRef.Version);
            }
            else
            {
                myPath = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            }
        }
        else
        {
            myPath = sitepath + "Workarea/help";
        }
        //_host.HelpFile = myPath + "EktronReferenceWeb.html#Widgets/Creating_the_Hello_World_Widget.htm";
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        txtTitle.Text = Title;
        txtSummary.Text = Summary;
        txtPageSize.Text = PageSize.ToString();
        viewDropdown.SelectedValue = ViewControl;

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        Title = txtTitle.Text;
        Summary = txtSummary.Text;
        int _pagesize;
        if (int.TryParse(txtPageSize.Text, out _pagesize))
        {
            PageSize = _pagesize;
        }

        ViewControl = viewDropdown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        if (string.IsNullOrWhiteSpace(ViewControl))
            return;

        var view = LoadControl(string.Format(ViewPathFormat, ViewControl)) as FaqControlBase;
        if (view == null)
            return;

        view.Title = Title;
        view.Summary = Summary;
        view.PageSize = PageSize;
        view.Initialize();

        View.Controls.Add(view);
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

}

