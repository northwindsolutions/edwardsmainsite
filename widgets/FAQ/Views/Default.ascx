﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Default.ascx.cs" Inherits="widgets_FAQ_Views_Default" %>
<%@ Import Namespace="Edwards.Models" %>
<%@ Import Namespace="Edwards.Utility" %>

<div>
    <script>
        if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
        EdwardsApp.controller("Faq", 
            function Faq($scope, $sce) {
                function totalPages(count, size) {
                    return Math.ceil(count / size);
                }
                $scope.trustIt = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                }
                $scope.loading = false;
                $scope.title = '<%= Title.JavascriptStringEncode() %>';
                $scope.summary = '<%= Summary.JavascriptStringEncode() %>';
                $scope.topics = <%= JSONTopics %>;
                $scope.topic = <%= ChosenTopic %>;
                $scope.faqs = <%= JSONFaqs %>;
                $scope.currentList = $scope.faqs[$scope.topic];

                $scope.pageSize = <%= PageSize %>;
                $scope.currentPage = 1;
                $scope.totalPages = totalPages($scope.currentList ? $scope.currentList.length : 0, $scope.pageSize);
                $scope.startIndex = function() {
                    var i = 0;
                    if ($scope.currentPage === 1) {
                        i = 0;
                    } else {
                        i = $scope.currentPage * $scope.pageSize - $scope.pageSize;
                    }
                    console.log("startIndex: " + i);
                    return i;
                };
                $scope.endIndex = function() {
                    var i = 0;
                    if ($scope.currentPage === $scope.totalPages) {
                        i = $scope.currentList.length - 1;
                    } else {
                        i = $scope.currentPage * $scope.pageSize - 1;
                    }
                    console.log("endIndex: " + i);
                    return i;
                };
                $scope.next = function(e) {
                    if ($scope.currentPage !== $scope.totalPages) {
                        $scope.currentPage += 1;
                    }
                    e.preventDefault();
                };
                $scope.prev = function(e) {
                    if ($scope.currentPage > 1) {
                        $scope.currentPage -= 1;
                    }
                    e.preventDefault();
                };
                $scope.changeTopic = function() {
                    $scope.currentList = $scope.faqs[$scope.topic];
                    $scope.currentPage = 1;
                    $scope.totalPages = totalPages($scope.currentList.length || 0, $scope.pageSize);
                };
            }
        );
    </script>
    <div id="faqController" ng-controller="Faq">
        <h1 ng-bind="title"></h1>
        <h2 ng-bind="summary"></h2>
        <br />
        <div class="one-half">
            <div class="form-item">
                <select class="ip" ng-model="topic" ng-options="t.id as t.name for t in topics | filter:{name:'!Featured'}" ng-change="changeTopic()">
                    <option value=""><%= GetLocalResourceObject("SelectTopic.Text") %></option>
                </select>
            </div>
        </div>
        <br class="clear"><br>
        <div id="accordion">
            <div ng-repeat="f in currentList.slice(startIndex(), endIndex() + 1)">
                <div class="toggle">
                    <span class="icon"></span>
                    <strong>Q: {{ f.question }}</strong><br>
                    Posted on {{ f.date }}
                </div>
                <div class="expandable">
                    A: <span ng-bind-html="trustIt(f.answer)"></span>
                </div>
            </div>
            <div id="server-list" ng-show="currentList == null">
                <asp:Repeater runat="server" ID="recentFaqsRepeater">
                    <ItemTemplate>
                        <div class="toggle">
                            <span class="icon"></span>
                            <strong>Q: <%# ((Faq)Container.DataItem).Question %></strong><br>
                            Posted on <%# ((Faq)Container.DataItem).Date.ToString("MM/dd/yyyy", Constants.CurrentCulture.DateTimeFormat) %>
                        </div>
                        <div class="expandable">
                            A: <%# ((Faq)Container.DataItem).Answer %>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div id="pagination" ng-show="totalPages > 1">
            <a href="#" ng-click="prev($event)" ng-class="{ prev: true, inactive: (currentPage === 1) }"></a>
            Page {{ currentPage }} of {{ totalPages }}
            <a href="#" ng-click="next($event)" ng-class="{ next: true, inactive: (currentPage === totalPages) }"></a>
        </div>
    </div>
</div>