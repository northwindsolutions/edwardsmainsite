﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;
using Ektron.Cms.API;
using Ektron.Cms.Commerce.Providers.Shipment.FedEx;
using Ektron.Cms.Common;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public partial class widgets_FAQ_Views_Default : FaqControlBase
{
    public string JSONTopics { get; set; }
    public string JSONFaqs { get; set; }
    public long FeaturedTopicsCategory = Constants.TaxonomyIds.FeaturedTopicsCategory;
    public long ChosenTopic { get; set; }
    public ICollection<Faq> FeaturedFaqs { get; set; }
    public IDictionary<long, ICollection<Faq>> Faqs { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        JS.RegisterJS(this, "/library/javascript/angular.min.js", "AngularJS");
        //JS.RegisterJS(this, "/library/javascript/underscore-min.js", "UnderscoreJS");
    }

    public override void Initialize()
    {
        long _topic;
        if (long.TryParse(Request.QueryString["topic"], out _topic))
        {
            ChosenTopic = _topic;
        }
        else
        {
            ChosenTopic = FeaturedTopicsCategory;
        }

        // Get the topics but exclude the featured one since it doesn't have to be explicitly selected.
        Topics = TaxonomyUtility.GetTaxonomyChildrenByParentId(Constants.TaxonomyIds.TopicsCategory)
            .Where(t => t.Id != FeaturedTopicsCategory)
            .ToList();
        JSONTopics = JsonConvert.SerializeObject(Topics.Select(t => JObject.FromObject(new
        {
            id = t.Id,
            name = t.Name
        })));

        // If the passed in value is not in the list of available topics, default to the featured one.
        if (!Topics.Any(f => f.Id == ChosenTopic))
        {
            ChosenTopic = FeaturedTopicsCategory;
        }

        var ids = new long[] { Constants.TaxonomyIds.FeaturedTopicsCategory };
        FeaturedFaqs = Backend.GetContentDataListByTaxonomyCategories(ids)
            .Select(t => t.ToFaqSmartform())
            .Where(f => f != null)
            .OrderByDescending(f => f.Date)
            .ToList();

        recentFaqsRepeater.DataSource = FeaturedFaqs;
        recentFaqsRepeater.DataBind();

        Faqs = new Dictionary<long, ICollection<Faq>>();
        // Build a mapping of category ID to FAQ
        foreach (var topic in Topics.Where(t => t.Id != Constants.TaxonomyIds.FeaturedTopicsCategory))
        {
            var items = Backend.GetContentDataListByTaxonomyCategories(new long[] { topic.Id })
                .Select(t => t.ToFaqSmartform())
                .Where(f => f != null)
                .ToList();

            Faqs.Add(topic.Id, items);
        }

        var faqsDict = new Dictionary<long, ICollection<JObject>>();
        foreach (var kv in Faqs)
        {
            faqsDict.Add(kv.Key, kv.Value.Select(f => JObject.FromObject(new
            {
                date = f.Date.ToString("MM/dd/yyyy", Constants.CurrentCulture.DateTimeFormat),
                question = f.Question,
                answer = f.Answer
            })).ToList());
        }

        JSONFaqs = JsonConvert.SerializeObject(faqsDict);
    }
}