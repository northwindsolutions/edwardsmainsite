﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KnowledgeCallout.ascx.cs" Inherits="widgets_FAQ_Views_KnowledgeCallout" %>
<%@ Import Namespace="Edwards.Models" %>
<%@ Import Namespace="Edwards.Utility" %>

<h3><%= Title %></h3>
<div class="block">
    <div class="one-half">
        <div class="form-item">
            <%--<div class="btn"><input type="button" value="Go"></div>--%>
            <%--<input type="text" value="Keyword search" onfocus="clearMe(this)" onblur="restoreMe(this)" class="ip">--%>
            <asp:DropDownList runat="server" ID="topicsDropDown">
                <asp:ListItem Selected="true" Text="Select a topic" Value=""></asp:ListItem>
            </asp:DropDownList>
        </div>
        <br>
        <strong><%= GetLocalResourceObject("RecentlyAsked.Text") %></strong>
    </div>
    <div id="accordion">
        <asp:Repeater runat="server" ID="recentFaqsRepeater">
            <ItemTemplate>
                <div class="toggle">
                    <span class="icon"></span>
                    <strong>Q: <%# ((Faq)Container.DataItem).Question %></strong><br>
                    Posted on <%# ((Faq)Container.DataItem).Date.ToString("MM/dd/yyyy", Constants.CurrentCulture.DateTimeFormat) %>
                </div>
                <div class="expandable">
                    A: <%# ((Faq)Container.DataItem).Answer %>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <br>
    <%--<a href="#"><strong>View all FAQs</strong></a>--%>
</div>
<script>
    jQuery(function($) {
        var $dropdown = $("#<%= topicsDropDown.ClientID %>");
        $dropdown.change(function (e) {
            var topic = parseInt($dropdown.val());
            if (!isNaN(topic) && topic > 0) {
                window.location = "/faq/?topic=" + topic;
            } else {
                window.location = "/faq/";
            }
        });
    });
</script>