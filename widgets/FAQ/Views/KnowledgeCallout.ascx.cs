﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards;
using Edwards.Models;
using Edwards.Utility;

public partial class widgets_FAQ_Views_KnowledgeCallout : FaqControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void Initialize()
    {
        // Get the topics but exclude the featured one since it doesn't have to be explicitly selected.
        Topics = TaxonomyUtility.GetTaxonomyChildrenByParentId(Constants.TaxonomyIds.TopicsCategory)
            .Where(t => t.Id != Constants.TaxonomyIds.FeaturedTopicsCategory)
            .ToList();

        var topics = Topics.Select(f => new
        {
            Id = f.Id,
            Name = f.Name
        }).ToList();
        topics.Insert(0, new
        {
            Id = (long)-1,
            Name = GetLocalResourceObject("SelectTopic.Text").ToString()
        });
        topicsDropDown.DataTextField = "Name";
        topicsDropDown.DataValueField = "Id";
        topicsDropDown.DataSource = topics;
        topicsDropDown.DataBind();

        var ids = new long[] { Constants.TaxonomyIds.FeaturedTopicsCategory };
        var featuredFaqs = Backend.GetContentDataListByTaxonomyCategories(ids)
            .Take(PageSize > 0 ? PageSize : 5)
            .Select(t => t.ToFaqSmartform())
            .Where(f => f != null)
            .OrderByDescending(f => f.Date)
            .ToList();

        recentFaqsRepeater.DataSource = featuredFaqs;
        recentFaqsRepeater.DataBind();
    }
}