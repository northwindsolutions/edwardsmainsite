﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FreewayDashboard.ascx.cs"
    Inherits="widgets_FreewayDashboard" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script language="JavaScript" type="text/javascript">
	    function callFreewayDashboard()
	    {
	            var url = "Workarea/Freeway/freewaydashboard.aspx?from=desktop";
                window.open(url ,"workarea","status = yes,resizable = no,top = 30, left = 70,height = 620, width = 870");
                return false;
	    }
</script>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <!-- You Need To Do ..............................  -->
        <table width="100%">
            <tr  style="color: White; background-color: #003399; font-size: 11px;
                font-weight: bold; height: 25px;">
                <td>
                    Draft (<asp:Label ID="lblDraftCount" runat="server"></asp:Label>)
                </td>
                <td>
                    Production (<asp:Label ID="lblInProductionCount" runat="server"></asp:Label>)
                </td>
                <td>
                    Quote (<asp:Label ID="lblInQuoteCount" runat="server"></asp:Label>)
                </td>
                <td>
                    Evalution (<asp:Label ID="lblInEvaluationCount" runat="server"></asp:Label>)
                </td>
            </tr>
            <tr>
                <td colspan="4" >
                    <div style="height: 150px; overflow: auto;">
                        <asp:GridView CssClass="ektronGrid" ID="_dataList" runat="server" AutoGenerateColumns="False"
                           GridLines="None" Width="100%" ShowHeader = "false" >
                           <HeaderStyle CssClass="title-header" Width="30px" />
                            <Columns>
                                <asp:BoundField HeaderText="Id"  DataField="Id">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="title-header"/>
                                    <ItemStyle HorizontalAlign="Center" Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Status"  DataField="StatusText">
                                    <HeaderStyle HorizontalAlign="Center" Width="30%" CssClass="title-header"/>
                                    <ItemStyle HorizontalAlign="Center" Width="30%" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Retrieved When"  DataField="RetrievedWhen">
                                    <HeaderStyle HorizontalAlign="Center" Width="50%" CssClass="title-header"/>
                                    <ItemStyle HorizontalAlign="Center" Width="50%" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle BackColor="White" ForeColor="#003399"  />
                            <SelectedRowStyle BackColor="White" />
                            <HeaderStyle BackColor="#E7F0F7" ForeColor="#003399" Font-Bold="True" />
                            <AlternatingRowStyle BackColor="#E7F0F7" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:View>
</asp:MultiView>
