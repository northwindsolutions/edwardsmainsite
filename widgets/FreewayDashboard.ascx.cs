#region Header Comment
/*
---------------------------------
Created By :    Adwait Churi
Created Date :  24 May 2010
Details : This page will dispaly the Freeway Dashboard in User Profile Dashboard.
---------------------------------
*/
#endregion
#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.DataIO.LicenseManager;
using System.Text;
using System.Data;
#endregion

public partial class widgets_FreewayDashboard : System.Web.UI.UserControl, IWidget
{
   

    #region Private Variables
    IWidgetHost _host;
    private string _FreewayDashboardString;
    private const string UserDisplayFormat = "{0} ({1}, {2})";
    #endregion

    #region Public Properties
    /// <summary>
    /// Manages the sorting of project grid based on the Project ID's
    /// </summary>
    public string SortingField
    {
        get
        {
            if (ViewState["SortingField"] == null)
                return "Id";

            return ViewState["SortingField"] as string;
        }
        set { ViewState["SortingField"] = value; }
    }

    /// <summary>
    /// Manages the sorting of project grid based on the Project ID's
    /// </summary>
    public bool SortingOrder
    {
        get
        {
            if (ViewState["SortingOrder"] == null)
                return false;

            return (bool)ViewState["SortingOrder"];
        }
        set { ViewState["SortingOrder"] = value; }
    }
   
    /// <summary>
    /// Gets and Sets the FreewayDashboard widget name is a property of IWidget class
    /// </summary>
    [WidgetDataMember("FreewayDashboard")]
    public string FreewayDashboardString { get { return _FreewayDashboardString; } set { _FreewayDashboardString = value; } }
    #endregion

    #region Page Events
    /// <summary>
    /// Sets the widget specific information
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {   
        string sitepath = new CommonApi().SitePath;
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronModalJS);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronModalCss);
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = "Freeway Dashboard Widget";
        //_host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        //_host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        _host.HelpFile = sitepath + "WorkArea/help/personalization_new.82.1.html";
        ViewSet.SetActiveView(View);
    }

    /// <summary>
    /// To  bind the gridview with projects
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {       
        //doDataBinding(false);

	EktronProjectCollection.Reset();
	doDataBinding(true);
	return;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="settings"></param>
    void EditEvent(string settings)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        //FreewayDashboardString = FreewayDashboardTextBox.Text;
        //_host.SaveWidgetDataMembers();
        //ViewSet.SetActiveView(View);
    }

    /// <summary>
    /// 
    /// </summary>
    protected void SetOutput()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CancelButton_Click(object sender, EventArgs e)
    {
    }
    #endregion

    #region Public/Private Function
    /// <summary>
    /// Binds the project data to grid
    /// </summary>
    /// <param name="refresh"></param>
    public void doDataBinding(bool refresh)
    {
        try
        {
            EktronProjectCollection DSEpc = null;

            // Retrieves the users from Ektron and fill the user dropdown list

            UserAPI userApi = new UserAPI();
            if (userApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.AdminUsers) || FreewayConfiguration.IsExecutiveUser)
                DSEpc = (EktronProjectCollection.GetProjects(string.Empty, refresh));
            else
            {
                UserData currentUser = userApi.UserObject(userApi.UserId);
                DSEpc = (EktronProjectCollection.GetProjects(currentUser.Username, refresh));
            }
            _dataList.DataSource = DSEpc;
            EktronProjectCollection.CurrentProjects.Sort(new EktronProjectComparer(SortingField, SortingOrder));
            _dataList.DataBind();
            System.Collections.Generic.List<EktronProject> DSEpcCount0 = DSEpc.FindAll(FindInDraftProjectStatus);
            System.Collections.Generic.List<EktronProject> DSEpcCount1 = DSEpc.FindAll(FindInEvaluationProjectStatus);
            System.Collections.Generic.List<EktronProject> DSEpcCount2 = DSEpc.FindAll(FindInProductionProjectStatus);
            System.Collections.Generic.List<EktronProject> DSEpcCount3 = DSEpc.FindAll(FindQuoteProjectStatus);

            lblDraftCount.Text = DSEpcCount0.Count.ToString();
            lblInEvaluationCount.Text = DSEpcCount1.Count.ToString();
            lblInProductionCount.Text = DSEpcCount2.Count.ToString();
            lblInQuoteCount.Text = DSEpcCount3.Count.ToString();
           
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("User account is invalid"))
            {
                StringBuilder sberror = new StringBuilder();
                sberror.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr>");
                sberror.Append(@"<td class=""label"" id=""usessltd1"" runat=""server"" style=""color:Red;"">You do not have the Freeway Credentials. <br> Please update the Freeway Credentials using Freeway Settings screen.</td>");

                sberror.Append("</tr></table>");
                Response.Write(sberror.ToString());
                Response.Flush();
                Response.End();
            }
        }

    }

    // Explicit predicate delegate.
    /// <summary>
    /// To get the count of projects in Draft status
    /// </summary>
    /// <param name="ep"></param>
    /// <returns></returns>
    private static bool FindInDraftProjectStatus(EktronProject ep)
    {

        if (ep.StatusText == "Draft")
        {
            return true;
        }
        {
            return false;
        }

    }

    // Explicit predicate delegate.
    /// <summary>
    /// To get the count of projects in Evolution status
    /// </summary>
    /// <param name="ep"></param>
    /// <returns></returns>
    private static bool FindInEvaluationProjectStatus(EktronProject ep)
    {

        if (ep.StatusText == "In Evaluation")
        {
            return true;
        }
        {
            return false;
        }

    }
    /// <summary>
    /// To get the count of projects in Production status
    /// </summary>
    /// <param name="ep"></param>
    /// <returns></returns>
    private static bool FindInProductionProjectStatus(EktronProject ep)
    {

        if (ep.StatusText == "In Production")
        {
            return true;
        }
        {
            return false;
        }

    }
    /// <summary>
    /// To get the count of projects in Quote status
    /// </summary>
    /// <param name="ep"></param>
    /// <returns></returns>
    private static bool FindQuoteProjectStatus(EktronProject ep)
    {

        if (ep.StatusText == "Quote")
        {
            return true;
        }
        {
            return false;
        }

    }   
   
    #endregion
   

   
}

