﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GeneralAnnouncement.ascx.cs" Inherits="Widgets_General_Announcement" %>
<%@ Register Src="~/widgets/ContentBlock/foldertree.ascx" TagPrefix="UC" TagName="FolderTree" %>
<%@ Register Src="~/library/includes/controls/GeneralAnnouncement.ascx" TagPrefix="Edwards" TagName="GeneralAnnouncement" %>

<asp:MultiView ID="ViewSet" runat="server">
    <asp:View ID="View" runat="server">
       <Edwards:GeneralAnnouncement ID="UxGeneralAnnouncement" runat="server" />
        <asp:Label ID="errorLb" runat="server" />
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>" class="CBWidget">
            <div class="CBEdit">
                <asp:Label ID="editError" runat="server" />
                <div id="CBFilterOptions">
                    <span class="FilterOption"><%=m_refMsg.GetMessage("lbl filter results by")%></span>
                    <asp:DropDownList ID="CBTypeFilter" runat="server" CssClass="CBTypeFilter">
                    </asp:DropDownList>
                </div>
                <div class="CBTabInterface">
                    <ul class="CBTabWrapper clearfix">
                        <li class="CBTab selected"><a href="#ByFolder"><span><%=m_refMsg.GetMessage("lbl folder")%></span></a></li>
                    </ul>
                    <div class="ByFolder CBTabPanel">
                        <UC:FolderTree ID="CBFolderTree" runat="server" />
                    </div>
                    <div class="BySearch CBTabPanel">
                        <%=m_refMsg.GetMessage("lbl enter search terms:")%><input type="text" class="searchtext" /><a href="#" class="searchSubmit" onclick="return Ektron.PFWidgets.ContentBlock.Search.DoSearch(this);" title="Search"><%=m_refMsg.GetMessage("lbl search")%></a>
                    </div>
                    <div class="ByABTesting CBTabPanel" style="display: none;">
                        <div class="google-variate-checkbox">
                            <asp:CheckBox ID="cbMultiVariate" runat="server" Text="Google Multivariate" />
                        </div>
                        <label for="testing"><%=m_refMsg.GetMessage("lbl section name:")%></label>
                        <asp:TextBox ID="tbSectionName" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div id="ResultsToggle"><a href="#" onclick="return Ektron.PFWidgets.ContentBlock.toggleResultsPane();"><%=m_refMsg.GetMessage("lbl view results")%></a></div>
                <div id="CBResults" style="display: none;"></div>
                <div id="CBPaging"></div>
                <asp:TextBox ID="tbData" CssClass="HiddenTBData" runat="server" Style="display: none;"></asp:TextBox>
                <asp:TextBox ID="tbFolderPath" CssClass="HiddenTBFolderPath" runat="server" Style="display: none;"></asp:TextBox>
                <div class="CBEditControls">
                    <asp:Button ID="CancelButton" CssClass="CBCancel" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
                    <asp:Button ID="SaveButton" CssClass="CBSave" runat="server" Text="Save" UseSubmitBehavior="false" OnClick="SaveButton_Click" OnClientClick="Ektron.PFWidgets.ContentBlock.Save();" />
                </div>
            </div>
            <input type="hidden" id="hdnAppPath" name="hdnAppPath" value="<%=appPath%>" />
            <input type="hidden" id="hdnLangType" name="hdnLangType" value="<%=langType%>" />
            <input type="hidden" id="hdnFolderId" name="hdnFolderId" value="0" />
        </div>
    </asp:View>
</asp:MultiView>