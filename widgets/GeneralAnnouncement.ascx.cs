﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;

public partial class Widgets_General_Announcement : UserControl, IWidget
{
	private long _ContentId;
    private bool _IsGoogleMultivariate;
    private string _GoogleSectionName;
    private bool _ShowTestingTab;

    [WidgetDataMember(0)]
    public long ContentId { get { return _ContentId; } set { _ContentId = value; } }

    [WidgetDataMember(false)]
    public bool IsGoogleMultivariate { get { return _IsGoogleMultivariate; } set { _IsGoogleMultivariate = value; } }

    [WidgetDataMember("")]
    public string GoogleSectionName { get { return _GoogleSectionName; } set { _GoogleSectionName = value; } }

    [GlobalWidgetData(false)]
    public bool ShowTestingTab { get { return _ShowTestingTab; } set { _ShowTestingTab = value; } }

	Ektron.Cms.PageBuilder.WidgetHost _widgetHost;
    IWidgetHost _host;

    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;

    protected CommonApi _api;
    protected string appPath;
    protected int langType;
    protected string uniqueId;

    protected void Page_Init(object sender, EventArgs e)
    {
        m_refMsg = m_refContentApi.EkMsgRef;
        CancelButton.Text = m_refMsg.GetMessage("btn cancel");
        SaveButton.Text = m_refMsg.GetMessage("btn save");
        
		_host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = "General Announcement Widget";
        _host.Edit += EditEvent;
        _host.Maximize += () => Visible = true;
        _host.Minimize += () => Visible = false;
        _host.Create += () => EditEvent("");
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        EnableViewState = false;
        Page.ClientScript.GetPostBackEventReference(SaveButton, "");
        
		_api = new CommonApi();
        appPath = _api.AppPath;
        langType = _api.RequestInformationRef.ContentLanguage;
        
		MainView();
        ViewSet.SetActiveView(View);
        BindCBTypeFilter();
    }

    protected void BindCBTypeFilter()
    {
        CBTypeFilter.Items.Add(new ListItem(m_refMsg.GetMessage("lbl content"), "content"));
    }

    protected void MainView()
    {
	    if (ContentId > 0)
            UxGeneralAnnouncement.ContentId = ContentId;
    }

    void EditEvent(string settings)
    {
        try
        {
            var webserviceURL = _api.SitePath + "widgets/contentblock/CBHandler.ashx";
            
			// Register JS
            JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
            JS.RegisterJSInclude(this, JS.ManagedScript.EktronClueTipJS);
            JS.RegisterJSInclude(this, JS.ManagedScript.EktronScrollToJS);
            JS.RegisterJSInclude(this, _api.SitePath + "widgets/contentblock/behavior.js", "ContentBlockWidgetBehaviorJS");

            // Insert CSS Links
            Css.RegisterCss(this, _api.SitePath + "widgets/contentblock/CBStyle.css", "CBWidgetCSS"); //cbstyle will include the other req'd stylesheets
            Ektron.Cms.Framework.UI.Packages.jQuery.jQueryUI.ThemeRoller.Register(this); //cbstyle will include the other req'd stylesheets

            JS.RegisterJSBlock(this, "Ektron.PFWidgets.ContentBlock.webserviceURL = \"" + webserviceURL + "\"; Ektron.PFWidgets.ContentBlock.setupAll('" + ClientID + "');", "EktronPFWidgetsCBInit");

            IsGoogleMultivariate = cbMultiVariate.Checked;
            GoogleSectionName = tbSectionName.Text;

            ViewSet.SetActiveView(Edit);

            if (ContentId > 0)
            {
                tbData.Text = ContentId.ToString();
                
				var capi = new ContentAPI();
                
				var folderid = capi.GetFolderIdForContentId(ContentId);
                tbFolderPath.Text = folderid.ToString();
                
				while (folderid != 0)
                {
                    folderid = capi.GetParentIdByFolderId(folderid);
                    if (folderid > 0) tbFolderPath.Text += "," + folderid;
                }
            }
        }
        catch (Exception e)
        {
            errorLb.Text = e.Message + e.Data + e.StackTrace + e.Source + e;
            _host.Title = _host.Title + " error";
            ViewSet.SetActiveView(View);
        }
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        Int64 cid;
        if (Int64.TryParse(tbData.Text, out cid))
        {
            IsGoogleMultivariate = cbMultiVariate.Checked;
            GoogleSectionName = tbSectionName.Text;
            ContentId = cid;

            var objectData = new ObjectData {ObjectId = cid, ObjectType = EkEnumeration.CMSObjectTypes.Content};

	        if ((Page as PageBuilder) != null)
            {
                _widgetHost = _host as Ektron.Cms.PageBuilder.WidgetHost;
                _widgetHost.PBWidgetInfo.Associations.Clear();
                _widgetHost.PBWidgetInfo.Associations.Add(objectData);
                _widgetHost.SaveWidgetDataMembers();
            }
            else
            {
                _host.SaveWidgetDataMembers();
            }

            MainView();
        }
        else
        {
            tbData.Text = String.Empty;
            editError.Text = m_refMsg.GetMessage("lbl invalid content block id");

        }
        ViewSet.SetActiveView(View);
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        MainView();
        ViewSet.SetActiveView(View);
    }
}