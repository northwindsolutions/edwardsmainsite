﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoogleSearch.ascx.cs" Inherits="widgets_GoogleSearch" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        
        <link rel="stylesheet" href="//www.google.com/cse/style/look/default.css" type="text/css" />
        <div class="googleCSEWrap">
            <div id="cse" style="width: 100%;">Loading</div>
        </div>
        <script src="//www.google.com/jsapi" type="text/javascript"></script>

        <script type="text/javascript">
            function parseQueryFromUrl() {
                var queryParamName = "q";
                var search = window.location.search.substr(1);
                var parts = search.split('&');
                for (var i = 0; i < parts.length; i++) {
                    var keyvaluepair = parts[i].split('=');
                    if (decodeURIComponent(keyvaluepair[0]) == queryParamName) {
                        return decodeURIComponent(keyvaluepair[1].replace(/\+/g, ' '));
                    }
                }
                return '';
            }

            google.load('search', '1', { language: 'en' });
            google.setOnLoadCallback(function () {
                var customSearchControl = new google.search.CustomSearchControl(
              '013991590561747824430:fu_-4m9wqbg');

                customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
                customSearchControl.draw('cse');
                var queryFromUrl = parseQueryFromUrl();
                if (queryFromUrl) {
                    customSearchControl.execute(queryFromUrl);
                }
            }, true);
        </script>

    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
