﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomepageRotator.ascx.cs" Inherits="widgets_HomepageRotator" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div class="content-container"><h3><asp:Label ID="uxRotatorHeader" runat="server"></asp:Label></h3>
        <div id="slideshowContainer" class="wide">
            <div class="slideshowGates">
                <span href="#" class="right"></span>
                <span href="#" class="left"></span>
            </div>
            <div id="slideshowWrap">
                <div id="slideshow">
                    <ul>
                        <asp:Placeholder ID="phRotatorSlide" runat="server"></asp:Placeholder>
                    </ul>
                </div>
            </div>
	    <div id="slideshowNav">
                <a href="#" class="prev"></a>
                <a href="#" class="next"></a>
            </div>
        </div></div>
        <script>
            (function ($) {
                var interval = <%= RotationInterval.HasValue ? (RotationInterval * 1000).ToString() : "null" %>;
                if (interval) {
                    var $next = $("#slideshowNav .next");
                    var autoScroll = function () {
                        $next.click();
                    };
                    var timer = setInterval(autoScroll, interval);

                    var $clip = $("#slideshowWrap, #slideshowNav");
                    $clip.on("mouseenter", function (e) {
                        clearInterval(timer);
                    });
                    $clip.on('mouseleave', function (e) {
                        timer = setInterval(autoScroll, interval);
                    });
                }
            })($);
        </script>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditRotatorDropDownLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditRotatorDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>
