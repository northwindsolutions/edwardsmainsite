using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_HomepageRotator : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Homepage Rotator Widget";
    private string _sitepath = string.Empty;
    private long? SmartformId = Constants.SmartFormIds.HomepageRotator;

    [WidgetDataMember("Homepage Rotator: ")]
    public string RotatorDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedRotator { get; set; }

    [WidgetDataMember("")]
    public string Result { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

   public int? RotationInterval { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateRotatorDropDown();

        uxEditRotatorDropDownLabel.Text = RotatorDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedRotator)) { uxEditRotatorDropDown.SelectedValue = SelectedRotator; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedRotator = uxEditRotatorDropDown.SelectedValue;
        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedRotator, out contentId) && contentId > 0)
        {
            var rotatorList = ContentUtility.GetHomepageRotatorList(contentId);

            if (rotatorList != null && rotatorList.Count > 0)
            {
                uxRotatorHeader.Text = rotatorList[0].Title;
                RotationInterval = rotatorList[0].RotationInterval;
                DisplayHomePageRotator(rotatorList);
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateRotatorDropDown()
    {
        List<ContentData> rotatorList = ContentUtility.GetListBySmartformId(SmartformId);

        if (rotatorList != null & rotatorList.Count > 0)
        {
            uxEditRotatorDropDown.Items.Clear();
            uxEditRotatorDropDown.Items.Add(new ListItem("", ""));

            foreach (ContentData item in rotatorList)
            {
                uxEditRotatorDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void DisplayHomePageRotator(List<HomepageRotatorInfo> rotatorList)
    {
        phRotatorSlide.Controls.Add(new LiteralControl(RegisterHomepageRotators(rotatorList)));
    }

    protected string RegisterHomepageRotators(List<HomepageRotatorInfo> rotatorList)
    {
        StringBuilder buff = new StringBuilder();

        if (rotatorList != null)
        {
            foreach (HomepageRotatorInfo rotator in rotatorList)
            {
                bool bVideo = !string.IsNullOrEmpty(rotator.VideoEmbed) ? true : false;
                bool bShare = (rotator.VideoShare == "true") ? true : false;

                buff.Append("<li><span class=\"caption\">");
                if (bVideo == true)
                {
                    if (bShare == true)
                    {
                        buff.AppendFormat("<span class='st_sharethis_custom' st_url='{0}' st_title='{1}'>{2}</span>", rotator.VideoLink, rotator.Description, GetGlobalResourceObject("EdwardsGlobal", "ShareThis"));
                    }
                    buff.AppendFormat("{0}</span>", rotator.Description);
                    buff.AppendFormat("<div class=\"video-wrapper\">{0}</div>", rotator.VideoEmbed);
                }
                else
                {
                    buff.AppendFormat("{0}</span>", rotator.Description);
                    buff.AppendFormat("<img src='{0}' alt='{1}' title='{1}'></span>", rotator.ImageSrc, rotator.ImageAlt);
                }
                buff.Append("</li>");
            }
        }

        return buff.ToString();
    }
}

