﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomepageSlider.ascx.cs" Inherits="widgets_HomepageSlider" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div id="showHide">
            <div id="homeSlideshowContainer">
                <div id="homeSlideshow" class="containerWrap">
                    <div id="homeSlideshowNav">
                        <a href="#" class="homePrev"></a>
                        <a href="#" class="homeNext"></a>
                    </div>
                    <div id="homeSlideshowWrap">
                        <div id="homeSlideshow">
                            <asp:ListView ID="uxHomepageSliderListView" runat="server">
                            <LayoutTemplate>
                                <ul>
                                    <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li>
                                    <div class="block">
                                        <h1><asp:Literal ID="uxSliderHeader" runat="server" /></h1>
                                        <h2><asp:Literal ID="uxSliderDescription" runat="server" /></h2>
                                    </div>
                                    <asp:Image ID="uxSliderImage" CssClass="alignright" runat="server" />
                                </li>
                            </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <script>
            (function ($) {
                var interval = <%= RotationInterval.HasValue ? (RotationInterval * 1000).ToString() : "null" %>;
                if (interval) {
                    var $next = $("#homeSlideshowNav .homeNext");
                    var autoScroll = function () {
                        $next.click();
                    };
                    var timer = setInterval(autoScroll, interval);

                    var $clip = $("#homeSlideshowWrap, #homeSlideshowNav");
                    $clip.on("mouseenter", function (e) {
                        clearInterval(timer);
                    });
                    $clip.on('mouseleave', function (e) {
                        timer = setInterval(autoScroll, interval);
                    });
                }
            })($);
        </script>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditRotatorDropDownLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditRotatorDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

