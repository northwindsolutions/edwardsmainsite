using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_HomepageSlider : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Homepage Branding Slider Widget";
    private string _sitepath = string.Empty;
    private long? SmartformId = Constants.SmartFormIds.HomepageSlider;

    [WidgetDataMember("Homepage Slider: ")]
    public string SliderDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedSlider { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

   public int? RotationInterval { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateRotatorDropDown();

        uxEditRotatorDropDownLabel.Text = SliderDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedSlider)) { uxEditRotatorDropDown.SelectedValue = SelectedSlider; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedSlider = uxEditRotatorDropDown.SelectedValue;
        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedSlider, out contentId) && contentId > 0)
        {
            var rotatorList = ContentUtility.GetHomepageSliderList(contentId);

            if (rotatorList != null && rotatorList.Count > 0)
            {
                RotationInterval = rotatorList[0].RotationInterval;
                LoadListResults(rotatorList);
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateRotatorDropDown()
    {
        List<ContentData> rotatorList = ContentUtility.GetListBySmartformId(SmartformId);

        if (rotatorList != null & rotatorList.Count > 0)
        {
            uxEditRotatorDropDown.Items.Clear();
            uxEditRotatorDropDown.Items.Add(new ListItem("", ""));

            foreach (ContentData item in rotatorList)
            {
                uxEditRotatorDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void LoadListResults(List<HomepageSliderInfo> sliderList)
    {
        uxHomepageSliderListView.DataSource = sliderList;
        uxHomepageSliderListView.ItemDataBound += (sender, args) =>
        {
            var sliderItem = args.Item.DataItem as HomepageSliderInfo;

            if (sliderItem == null)
                return;

            var header = args.Item.FindControl("uxSliderHeader") as Literal;
            var description = args.Item.FindControl("uxSliderDescription") as Literal;
            var image = args.Item.FindControl("uxSliderImage") as Image;

            if (header == null || description == null || image == null)
                return;

            header.Text = sliderItem.Header;
            description.Text = sliderItem.Description;
            image.ImageUrl = sliderItem.ImageSrc;
            image.AlternateText = sliderItem.ImageAlt;
        };
        uxHomepageSliderListView.DataBind();
    }
}

