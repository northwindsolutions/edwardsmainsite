﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Map.ascx.cs" Inherits="widgets_Map" %>
<%@ Import Namespace="System.Globalization" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
    <script src='https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.css' rel='stylesheet' />
        
    <div>
        <script>
            
            var map = null;
            var markerLayer = null;
            var initPosition = [<%= InitLat.ToString(CultureInfo.CreateSpecificCulture("en-US")) %>, <%= InitLng.ToString(CultureInfo.CreateSpecificCulture("en-US")) %>];
            var initZoom = <%= InitZoom %>;
            var mapFeatures = <%= JSONFeatures %>;


            function _findFeatureByCountry(country) {
                for (var i in mapFeatures) {
                    var feature = mapFeatures[i];
                    if (feature.properties.country == country) {
                        return feature;
                    }
                }
                return null;
            }
            function _findFeatureByCity(city) {
                for (var i in mapFeatures) {
                    var feature = mapFeatures[i];
                    if (feature.properties.city == city) {
                        return feature;
                    }
                }
                return null;
            }

            if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
            EdwardsApp.controller('MapFilter', 
                function MapFilter($scope, $sce) {
                    $scope.types = ["Office", "Distributor"];
                    $scope.type = $scope.types[0];
                    $scope.country = null;
                    $scope.city = null;

                    $scope.countryMapping = <%= CountryMapping %>;

                    $scope.countries = {
                        "Office": <%= JSONOfficeCountries %>,
                        "Distributor": <%= JSONDistributorCountries %>
                    };

                    $scope.countryList = function() {
                        var l = Object.keys($scope.countries[$scope.type]); 
                        if (l) {
                            var newL = l.sort(function (cc1, cc2) {
                                var name1 = $scope.countryMapping[cc1];
                                var name2 = $scope.countryMapping[cc2];
                                if ( name1 < name2 )
                                    return -1;
                                if ( name1 > name2 )
                                    return 1;
                                return 0;
                            });
                            return newL;
                        }
                        return l;
                    };
                    $scope.cityList = function() {
                        var l = $scope.countries[$scope.type][$scope.country];
                        if (l) {
                            var newL = l.sort();
                            return newL;
                        }
                        return l;
                    };
                    $scope.$watch('type', function(value) {
                        if (markerLayer) {
                            markerLayer.setFilter(function(f) {
                                return f.properties['type'] === value;
                            });
                            map.setView(initPosition, initZoom);
                        }
                    });
                    $scope.countryChange = function() {
                        if (markerLayer) {
                            if ($scope.country) {
                                // Zoom in to this country
                                var ctry = _findFeatureByCountry($scope.country);
                                if (ctry && map) {
                                    map.setView([ctry.properties.lat, ctry.properties.lng], 5);
                                }
                                markerLayer.setFilter(function(f) {
                                    return f.properties['country'] === $scope.country;
                                });
                            } else {
                                map.setView(initPosition, initZoom);
                                markerLayer.setFilter(function(f) {
                                    return f.properties['type'] === $scope.type;
                                });
                            }
                        }
                    };
                    $scope.cityChange = function() {
                        if (markerLayer) {
                            if ($scope.city) {
                                markerLayer.setFilter(function(f) {
                                    return f.properties['city'] === $scope.city;
                                });
                                // Make this marker pop up.
                                markerLayer.eachLayer(function(marker) {
                                    if (marker.feature.properties.city === $scope.city) {
                                        marker.openPopup();
                                    }
                                });
                            } else {
                                markerLayer.setFilter(function(f) {
                                    return f.properties['country'] === $scope.country;
                                });
                            }
                        }
                    };
                }
            );

        </script>
        <h3><%= Title %></h3>
        <% if (!string.IsNullOrWhiteSpace(Link) && !string.IsNullOrWhiteSpace(LinkText)) { %>
        <p>
            <a href="<%= Link %>"><%= LinkText %></a>
        </p>
        <% } %>
        <div id="mapFilter" ng-controller="MapFilter"  class="map-filter">
            <div class="types">
                <label class="selected"><input type="radio" name="type" ng-model="type" value="Office" checked/> <%= GetLocalResourceObject("GlobalOffices.Text") %></label>
                <label><input type="radio" name="type" ng-model="type" value="Distributor"/> <%= GetLocalResourceObject("GlobalDistributors.Text") %></label>
            </div>
            <div class="filters">
                <select ng-model="country" ng-options="c as countryMapping[c] for c in countryList()" ng-change="countryChange(value)">
                    <option value=""><%= GetLocalResourceObject("ChooseCountry.Text") %></option>
                </select>
                <select ng-model="city" ng-options="c for c in cityList()" ng-change="cityChange(value)">
                    <option value=""><%= GetLocalResourceObject("ChooseCity.Text") %></option>
                </select>
            </div>
        </div>
        <div id="map"></div>
        <script>
            (function($) {

                function layerAdd(e) {
                    //console.log("layer being added", e);

                    var marker = e.layer;
                    var feature = marker.feature;

                    //console.log('marker, feature', marker, feature);

                    marker.setIcon(L.icon(feature.properties.icon));
                    var typeClass = (feature.properties.type === "Distributor") ? "type-distributor" : "type-location";
                    var content = "<div class='popupContent'>" +
                        "<div class='" + typeClass + "'>" +
                        "<div class='title'>" +
                        "<h4>" + feature.properties.title + "</h4>" +
                        "<div class='address'><pre>" + feature.properties.address + "</pre></div>" +
                        "</div>" +
                        "<div class='summary'>" +
                        feature.properties.summary +
                        "</div>" +
                        "</div>" +
                        "</div>";

                    marker.bindPopup(content, {
                        closeButton: false,
                        minWidth: 460
                    });
                }

                function ready() {
                    //console.log("Document ready");

                    var $map = $("#map");
                    var $mapFilter = $("#mapFilter");
                    var $radioButtons = $mapFilter.find('.types input');
                    $radioButtons.on('click', function(e) {
                        $mapFilter.find('.types label').removeClass('selected');
                        $(this).parents('label').addClass('selected');
                    });

                    var $parent = $map.parent();
                    var width = $parent.width();
                    $map.width(width);
                    $map.height(width * 0.60);

                    var mapId = '<%= MapId %>';

                    map = L.mapbox.map('map', mapId).setView(initPosition, initZoom);
                    map.zoomControl.setPosition('topright');

                    var geoJson = {
                        type: 'FeatureCollection',
                        features: mapFeatures
                    };
                    //console.log("geo json", geoJson);

                    markerLayer = L.mapbox.featureLayer().addTo(map);
                    //console.log("marker layer", markerLayer);

                    markerLayer.on('layeradd', layerAdd);
                    markerLayer.setGeoJSON(geoJson);
                    markerLayer.on('mouseover', function(e) {
                        e.layer.openPopup();
                    });

                    markerLayer.setFilter(function(f) {
                        return f.properties['type'] === 'Office';
                    });
                }

                $(ready);

            })(jQuery);
        </script>
    </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <fieldset>
                <p>
                    <label>Title</label>
                    <asp:TextBox runat="server" ID="txtTitle"></asp:TextBox>
                </p>                
                <p>
                    <label>Link (Optional)</label>
                    <asp:TextBox runat="server" ID="txtLink"></asp:TextBox>
                </p>                
                <p>
                    <label>Link Text (Optional)</label>
                    <asp:TextBox runat="server" ID="txtLinkText"></asp:TextBox>
                </p>                
                <p>
                    <label>MapBox Map ID</label>
                    <asp:TextBox runat="server" ID="txtMapId"></asp:TextBox>
                </p>
                <p>
                    <label>Initial Latitude</label>
                    <asp:TextBox runat="server" ID="txtLat"></asp:TextBox>
                </p>
                <p>
                    <label>Initial Longitude</label>
                    <asp:TextBox runat="server" ID="txtLng"></asp:TextBox>
                </p>
                <p>
                    <label>Initial Zoom Level</label>
                    <asp:TextBox runat="server" ID="txtZoom"></asp:TextBox>
                </p>
            </fieldset>
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
