using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.WebPages.Html;
using Edwards.Utility;
using Ektron.Cms.Controls.CmsWebService;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using Edwards.Models;
using Edwards.Utility;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;
using java.awt;
using Content = Ektron.Cms.API.Content.Content;
using System.Globalization;

public partial class widgets_Map : System.Web.UI.UserControl, IWidget
{
    #region properties

    public int CurrentLanguage
    {
        get { return Constants.CurrentLanguageId; }
    }

    [WidgetDataMember("")]
    public string Title { get; set; }

    [WidgetDataMember("")]
    public string Link { get; set; }

    [WidgetDataMember("")]
    public string LinkText { get; set; }

    [WidgetDataMember("")]
    public string MapId { get; set; }

    [WidgetDataMember(0.0)]
    public double InitLat { get; set; }

    [WidgetDataMember(0.0)]
    public double InitLng { get; set; }

    [WidgetDataMember(1)]
    public int InitZoom { get; set; }

    public string JSONFeatures { get; set; }
    public string JSONDistributorFeatures { get; set; }
    public string JSONOfficeCountries { get; set; }
    public string JSONDistributorCountries { get; set; }
    public string CountryMapping { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        string sitepath = new CommonApi().SitePath;
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronModalJS);
        JS.RegisterJS(this, "/library/javascript/angular.min.js", "AngularJS");
        //JS.RegisterJS(this, "/library/javascript/underscore-min.js", "UnderscoreJS");
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronModalCss);
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        m_refMsg = m_refContentApi.EkMsgRef;
        _host.Title = m_refMsg.GetMessage("Map Widget");
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        CancelButton.Text = m_refMsg.GetMessage("btn cancel");
        SaveButton.Text = m_refMsg.GetMessage("btn save");  
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        string myPath = string.Empty;
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ek_helpDomainPrefix"]))
        {
            string helpDomain = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            if ((helpDomain.IndexOf("[ek_cmsversion]") > 1))
            {
                myPath = helpDomain.Replace("[ek_cmsversion]", new CommonApi().RequestInformationRef.Version);
            }
            else
            {
                myPath = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            }
        }
        else
        {
            myPath = sitepath + "Workarea/help";
        }
        //_host.HelpFile = myPath + "EktronReferenceWeb.html#Widgets/Creating_the_Hello_World_Widget.htm";
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        txtTitle.Text = Title;
        txtMapId.Text = MapId;
        txtLink.Text = Link;
        txtLinkText.Text = LinkText;
        txtLat.Text = InitLat.ToString();
        txtLng.Text = InitLng.ToString();
        txtZoom.Text = InitZoom.ToString();

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        int _zoom;
        double _lat, _lng;
        if (double.TryParse(txtLat.Text, out _lat))
        {
            InitLat = _lat;
        }
        if (double.TryParse(txtLng.Text, out _lng))
        {
            InitLng = _lng;
        }
        if (int.TryParse(txtZoom.Text, out _zoom))
        {
            InitZoom = _zoom;
        }

        MapId = txtMapId.Text;
        Title = txtTitle.Text;
        Link = txtLink.Text;
        LinkText = txtLinkText.Text;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        // get the location data from the locations folder.
        int currentLanguageId = Constants.CurrentLanguageId;

        var locations = Backend.GetContentDataListByFolders(new long[] { Constants.FolderIds.LocationsFolderId }, currentLanguageId, returnMetadata: true, pageSize: 500000)
            .Select(c => c.ToLocationSmartform())
            .Where(l => l != null && l.Lat.HasValue && l.Lng.HasValue)
            .ToList();

        //throw new Exception(string.Format("first content id: {2}, Locations: {0}, current langID: {1}", locations.Count, currentLanguageId, locations[0].Id));

        var features = locations
            .Select(l => JObject.FromObject(new
        {
            type = "Feature",
            properties = JObject.Parse(string.Format("{{ title: '{0}', address: '{1}', summary: '{2}', 'marker-color': '#003554', icon: {3}, country: '{4}', city: '{5}', type: '{6}', lat: {7}, lng: {8}, countryName: '{9}' }}",
                l.Title.JavascriptStringEncode(),
                l.Address.JavascriptStringEncode(),
                l.Summary.JavascriptStringEncode(),
                JObject.FromObject(new
                {
                    iconUrl = string.Format("/library/images/common/{0}",
                                l.Type == "Office" ? "Edwards-map-icon.png" : "Edwards-distributor-map-icon.png"),
                    iconSize = new List<int> { 8, 8 },
                    iconAnchor = new List<int> { 4, 4 },
                    popupAnchor = new List<int> { 0, -5 },
                    className = "dot"
                }).ToString(),
                l.Country.JavascriptStringEncode(), 
                l.City.JavascriptStringEncode(),
                l.Type.JavascriptStringEncode(),
                l.Lat.Value.ToString(CultureInfo.CreateSpecificCulture("en-US")),
                l.Lng.Value.ToString(CultureInfo.CreateSpecificCulture("en-US")),
                Backend.CountryShortISOCodeMapping[l.Country]
                )
            ),
            geometry = new
            {
                type = "Point",
                coordinates = new List<double>
                {
                    l.Lng.Value,//.ToString(CultureInfo.CreateSpecificCulture("en-US")), 
                    l.Lat.Value //.ToString(CultureInfo.CreateSpecificCulture("en-US"))
                }
            }
        }));

        JSONFeatures = JsonConvert.SerializeObject(features);

        JSONOfficeCountries = JsonConvert.SerializeObject(
             locations.Where(l => l.Type == "Office")
             .ToLookup(l => l.Country)
             .ToDictionary(
                 k => k.Key,
                 v => v.Select(c => c.City).ToList()
                 )
             );
        JSONDistributorCountries = JsonConvert.SerializeObject(
            locations.Where(l => l.Type == "Distributor")
            .ToLookup(l => l.Country)
             .ToDictionary(
                 k => k.Key,
                 v => v.Select(c => c.City).ToList()
                 )
            );

        CountryMapping = JsonConvert.SerializeObject(Backend.CountryShortISOCodeMapping);
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

}

