﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsList.ascx.cs" Inherits="widgets_NewsList" %>
<%@ Reference Control="~/widgets/NewsList/Views/List.ascx" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <fieldset>
                <p>
                    <asp:Label ID="TitleLabel" runat="server">Title</asp:Label>
                    <asp:TextBox ID="TitleTextBox" runat="server" Style="width: 95%"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="TaxonomyIdLabel" runat="server">Taxonomy ID</asp:Label>
                    <asp:DropDownList ID="TaxonomyDropdownList" runat="server">
                    </asp:DropDownList>
                </p>
                <p>
                    <asp:Label ID="Label1" runat="server">View</asp:Label>
                    <asp:DropDownList ID="ViewDropDownList" runat="server">
                        <asp:ListItem Selected="true" Text="List" Value="List.ascx"></asp:ListItem>
                        <asp:ListItem Text="Home Page" Value="HomePage.ascx"></asp:ListItem>
                        <asp:ListItem Text="Blog" Value="Blog.ascx"></asp:ListItem>
                    </asp:DropDownList>
                </p>
            </fieldset>
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
