using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.WebPages.Html;
using Edwards.Utility;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using Edwards.Models;
using Edwards.Utility;
using java.awt;
using Content = Ektron.Cms.API.Content.Content;

public partial class widgets_NewsList : System.Web.UI.UserControl, IWidget
{
    #region properties

    public int CurrentLanguage
    {
        get { return Constants.CurrentLanguageId; }
    }

    [WidgetDataMember("")]
    public string Title { get; set; }

    [WidgetDataMember(0)]
    public long TaxonomyId { get; set; }

    [WidgetDataMember("List.ascx")]
    public string ViewControl { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        string sitepath = new CommonApi().SitePath;
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronModalJS);
        JS.RegisterJS(this, "/library/javascript/angular.min.js", "AngularJS");
        JS.RegisterJS(this, "/library/javascript/underscore-min.js", "UnderscoreJS");
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronModalCss);
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        m_refMsg = m_refContentApi.EkMsgRef;
        _host.Title = m_refMsg.GetMessage("News List Widget");
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        CancelButton.Text = m_refMsg.GetMessage("btn cancel");
        SaveButton.Text = m_refMsg.GetMessage("btn save");  
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        string myPath = string.Empty;
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ek_helpDomainPrefix"]))
        {
            string helpDomain = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            if ((helpDomain.IndexOf("[ek_cmsversion]") > 1))
            {
                myPath = helpDomain.Replace("[ek_cmsversion]", new CommonApi().RequestInformationRef.Version);
            }
            else
            {
                myPath = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            }
        }
        else
        {
            myPath = sitepath + "Workarea/help";
        }
        //_host.HelpFile = myPath + "EktronReferenceWeb.html#Widgets/Creating_the_Hello_World_Widget.htm";
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        // get a list of taxonomy nodes in the CMS;
        var items = TaxonomyUtility.GetTaxonomyChildrenByParentId(0);
        TaxonomyDropdownList.DataSource = items;
        TaxonomyDropdownList.DataTextField = "Name";
        TaxonomyDropdownList.DataValueField = "Id";
        TaxonomyDropdownList.DataBind();

        if (items.Any(t => t.Id == TaxonomyId))
            TaxonomyDropdownList.SelectedValue = TaxonomyId.ToString();

        ViewDropDownList.SelectedValue = ViewControl;

        TitleTextBox.Text = Title;

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        long taxId;
        if (long.TryParse(TaxonomyDropdownList.SelectedValue, out taxId))
        {
            TaxonomyId = taxId;
        }

        ViewControl = ViewDropDownList.SelectedValue;

        Title = TitleTextBox.Text;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        var singleItem = false;
        IEnumerable<NewsItem> items = null;

        // If there's a single content block, get that from the querystring.
        long contentId;
        if (long.TryParse(Request.QueryString["id"], out contentId))
        {
            var cb = ContentUtility.GetItemByContentId(contentId);
            if (cb != null)
            {
                var item = cb.GetNewsItemObject();
                if (item != null)
                {
                    singleItem = true;
                    items = new List<NewsItem>
                    {
                        cb.GetNewsItemObject()
                    };
                }
            }
        }
        else if (TaxonomyId > 0)
        {
            //var date = DateTime.UtcNow;
            items = TaxonomyUtility.GetContentDataListByTaxonomyCategories(new[] {TaxonomyId}, CurrentLanguage, recursive: true)
                .Select(cd => cd.GetNewsItemObject())
                // Filter out nulls and find the current month.
                .Where(n => n != null)
                .Where(n => n.Date.HasValue); // && n.Date.Value.Year == date.Year && n.Date.Value.Month == date.Month);

        }

        var controlPath = string.Format("~/widgets/NewsList/Views/{0}", ViewControl);
        var viewCtrl = LoadControl(controlPath) as NewsListViewControlBase;
        if (viewCtrl != null)
        {
            viewCtrl.SingleItem = singleItem;
            viewCtrl.Title = Title;
            viewCtrl.TaxonomyId = TaxonomyId;
            viewCtrl.CurrentLanguage = CurrentLanguage;
            viewCtrl.Initialize(items);
            viewCtrl.Visible = contentId > 0 || TaxonomyId > 0;
            View.Controls.Add(viewCtrl);
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }
}

