﻿<%@ WebHandler Language="C#" Class="Data" %>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using Edwards.Models;
using Edwards.Utility;
using Ektron.Cms.Search.Proxies.MSSearchStatsPublisherService;
using Ektron.Newtonsoft.Json;

public class Data : IHttpHandler {
    
    /// <summary>
    /// Retrieve the news items based on the passed in year/month.
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        if (context.Request.HttpMethod != "POST")
        {
            context.Response.Write(JsonConvert.SerializeObject(new { error = "Only POST requests are allowed." }));
            return;
        }
        
        try
        {
            long categoryId;
            long.TryParse(context.Request.Form["category"], out categoryId);

            int? languageId = null;
            int _languageId;
            if (int.TryParse(context.Request.Form["langtype"], out _languageId))
            {
                languageId = _languageId;
            }

            var date = DateTime.UtcNow;
            int month, year;
            if (int.TryParse(context.Request.Form["month"], out month) &&
                int.TryParse(context.Request.Form["year"], out year))
            {
                try
                {
                    date = new DateTime(year, month, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                }
                catch
                {
                    date = DateTime.UtcNow;
                }
            }
            
            if (categoryId > 0)
            {
                var rslts = TaxonomyUtility.GetContentDataListByTaxonomyCategories(new[] {categoryId}, languageId ?? Constants.CurrentLanguageId, recursive: true)
                    .Select(cb => cb.GetNewsItemObject())
                    .Where(i => i != null)
                    .Where(s => s.Date.HasValue && s.Date.Value.Year == date.Year && s.Date.Value.Month == date.Month)
                    .OrderByDescending(s => s.Date.Value)
                    .Select(s => new
                    {
                        title = s.Title,
                        date = s.Date.HasValue ? s.Date.Value.ToString("d", Constants.CurrentCulture.DateTimeFormat) : string.Empty,
                        subHeadline = s.SubHeadline,
                        imageSrc = s.ImageSrc,
                        imageAlt = s.ImageAlt,
                        author = s.Author,
                        jobTitle = s.JobTitle,
                        content = s.Content
                    });

                context.Response.Write(JsonConvert.SerializeObject(new { results = rslts }));
                return;
            }
            context.Response.Write(JsonConvert.SerializeObject(new { error = HttpContext.GetGlobalResourceObject("EdwardsGlobal", "NoCategoryIdSpecified") }));
        }
        catch (Exception ex)
        {
            context.Response.Write(JsonConvert.SerializeObject(new { error = ex.Message }));
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}