﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomePage.ascx.cs" Inherits="widgets_NewsList_Views_HomePage" %>

<div>
    <h3><%= Title %></h3>
    <div id="news" ng-controller="HomePageNews">
        <div class="sort">
            <strong><asp:Literal runat="server" meta:resourcekey="Featured"></asp:Literal> /</strong> <a href="/media/"><asp:Literal runat="server" meta:resourcekey="ViewAll"></asp:Literal></a>
        </div>
        <ul>
            <asp:Repeater runat="server" ID="newsItemsRepeater">
                <ItemTemplate>
                    <li>
                        <span class="news-indicator">&nbsp;</span>
                        <span class="news-text">
                            <strong><asp:Literal runat="server" ID="title" /></strong><br>
                            <asp:Literal runat="server" ID="summary"></asp:Literal> <asp:HyperLink runat="server" ID="link" meta:resourcekey="ReadFullArticle"></asp:HyperLink>
                        </span>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
<%--            <li>
                <strong>Lendae ut et molestibus net ex este volles es ut et molestibus net</strong><br>
                Lcta ipsa nonsequo et prepudae as eum quam... <a href="#">Read full article</a>
            </li>
            <li>
                <strong>Lendae ut et molestibus net ex este volles es ut et molestibus net</strong><br>
                Lcta ipsa nonsequo et prepudae as eum quam... <a href="#">Read full article</a>
            </li>
            <li>
                <strong>Lendae ut et molestibus net ex este volles es ut et molestibus net</strong><br>
                Lcta ipsa nonsequo et prepudae as eum quam... <a href="#">Read full article</a>
            </li>
            <li>
                <strong>Lendae ut et molestibus net ex este volles es ut et molestibus net</strong><br>
                Lcta ipsa nonsequo et prepudae as eum quam... <a href="#">Read full article</a>
            </li>--%>
        </ul>
    </div>
</div>
<script>
    if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
    EdwardsApp.controller("HomePageNews",
        function ($scope, $sce) {
            $scope.trustIt = function (htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            }

            $scope.mostRecentNews = null;
            $scope.allNews = null;
        }
    );
</script>