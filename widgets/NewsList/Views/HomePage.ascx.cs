﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Models;
using Edwards.Utility;

public partial class widgets_NewsList_Views_HomePage : NewsListViewControlBase
{
    protected IOrderedEnumerable<NewsItem> MostRead { get; set; }
    protected IOrderedEnumerable<NewsItem> MostRecent { get; set; }

    public override void Initialize(IEnumerable<NewsItem> items)
    {
        // TODO: Fix this later if there are multiple featured news categories.  For now, just pull from the \News\Featured taxonomy.
        MostRecent = TaxonomyUtility.GetContentDataListByTaxonomyCategories(new[] { Constants.TaxonomyIds.FeaturedNewsCategory }, CurrentLanguage, recursive: true)
                .Select(cd => cd.GetNewsItemObject())
                // Filter out nulls and find the current month.
                .Where(n => n != null)
                .Where(n => n.Date.HasValue)
                .OrderByDescending(n => n.Date); // && n.Date.Value.Year == date.Year && n.Date.Value.Month == date.Month);
            //items.OrderByDescending(t => t.Date);

        // TODO: Most read should be the items in the news taxonomy that have been read the most according to the analytics.
        MostRead = MostRecent;

        newsItemsRepeater.DataSource = MostRecent;
        newsItemsRepeater.ItemDataBound += (sender, args) =>
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                return;

            var item = args.Item.DataItem as NewsItem;
            if (item != null)
            {
                var title = args.Item.FindControl("title") as Literal;
                var summary = args.Item.FindControl("summary") as Literal;
                var link = args.Item.FindControl("link") as HyperLink;
                if (title == null || summary == null || link == null)
                    return;

                title.Text = item.Title;
                summary.Text = item.SubHeadline;
                link.NavigateUrl = item.Quicklink;
            }
        };
        newsItemsRepeater.DataBind();
    }
}