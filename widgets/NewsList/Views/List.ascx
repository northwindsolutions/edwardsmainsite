﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="List.ascx.cs" Inherits="widgets_NewsList_Views_List" %>
<%@ Import Namespace="Edwards.Models" %>

<div>
    <div id="mediaNewsList" class="news-list" ng-controller="NewsList" ng-cloak>
        <div id="featuredEvent">
            <h1><asp:Literal runat="server" meta:resourcekey="TitleLiteral"></asp:Literal> {{ monthName() }} {{ year }}</h1>
            <p class="error" ng-show="error" ng-bind="error"></p>
                
            <div class="block months">
                <span ng-repeat="month in months"><a ng-click="fetchItemsForMonth()" href="#" data-value="{{ month.id }}">{{ month.name }}</a>{{ $last ? "" : " | " }}</span>
            </div>
            <p ng-show="loading"><img src="/Workarea/images/application/loading_small.gif" alt="<%= GetGlobalResourceObject("EdwardsGlobal", "Loading") %>"/></p>
        </div>
        <hr />
        <div id="accordion">
            <p ng-show="newsItems != null && newsItems.length === 0"><asp:Literal runat="server" meta:resourcekey="NoItemsFound"></asp:Literal></p>
            <span ng-repeat="item in newsItems">
                <div ng-class="{ first: $first, toggle: true }">
                    <span class="icon"></span>
                    <strong>{{ item.title }} </strong> <em>{{ item.date }}</em><br>
                    {{ item.subHeadline }}<br>
                    <em><strong>{{ item.author }}</strong>, {{ item.jobTitle }}</em>
                </div>
                <div class="expandable">
                    <span ng-show="{{ item.imageSrc }}"><img src="{{ item.imageSrc }}" alt="{{ item.imageAlt }}" /></span>
                    <p ng-bind-html="trustIt(item.content)"></p>
                </div>
            </span>
            <div id="server-list" ng-show="newsItems == null">
                <p runat="server" id="noItemsFoundMessage"><asp:Literal ID="Literal1" runat="server" meta:resourcekey="NoItemsFound"></asp:Literal></p>
                <asp:Repeater runat="server" ID="newsItemsRepeater">
                    <ItemTemplate>
                        <div class="toggle <%# Container.ItemIndex == 0 ? "first" : string.Empty %>">
                            <span class="icon"></span>
                            <strong><%# ((NewsItem)Container.DataItem).Title %> </strong> <em><%# ((NewsItem)Container.DataItem).Date.HasValue ? ((NewsItem)Container.DataItem).Date.Value.ToString("d") : string.Empty %></em><br>
                            <%# ((NewsItem)Container.DataItem).SubHeadline %><br>
                            <em><strong><%# ((NewsItem)Container.DataItem).Author %></strong>, <%# ((NewsItem)Container.DataItem).JobTitle %></em>
                        </div>
                        <div class="expandable" style="<%= SingleItem ? "display: block;" : string.Empty %>">
                            <img src="<%# ((NewsItem)Container.DataItem).ImageSrc %>" alt="<%#((NewsItem)Container.DataItem).ImageAlt %>" style="<%# string.IsNullOrEmpty(((NewsItem)Container.DataItem).ImageSrc) ? "display: none;" : string.Empty %>" />
                            <%# ((NewsItem)Container.DataItem).Content %>
                        </div>         
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function($) {
        function ngScope() {
            var sel = $("#mediaNewsList");
            if (sel.length === 0) {
                return null;
            }
            return angular.element(sel[0]).scope();
        }

        $(".months").on("click", "a", function(e) {
            e.preventDefault();

            $(".highlight").removeClass("highlight");
            $(this).addClass("highlight");
        });

        var $newsLeftNav = $("#newsLeftNav");
        $newsLeftNav.on("click", "li > a", function(e) {
            var year = parseInt($(this).data('year'));
            if (!isNaN(year)) {
                var $li = $(this).parent('li');
                $("#newsLeftNav li").removeClass('selected');
                $li.addClass('selected');
                var $scope = ngScope();
                if ($scope != null) {
                    $scope.$apply(function($s) {
                        $s.year = year;
                        $s.month = 1;
                        $s.fetchItemsForMonth();
                    });
                }
            }
            e.preventDefault();
        });

        var $nowYear = $newsLeftNav.find("li > a[data-year='<%= Now.Year %>']");
        if ($nowYear.length > 0) {
            $newsLeftNav.find("li").removeClass("selected");
            $nowYear.parent("li").addClass("selected");
        }
    });
    if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
    EdwardsApp.controller("NewsList", 
        function ($scope, $sce) {
            $scope.trustIt = function(htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            }
            $scope.categoryId = <%= TaxonomyId %>;
            $scope.error = "";
            $scope.loading = false;
            
            $scope.featuredNewsItems = null;
            $scope.newsItems = null;

            $scope.months = <%= months %>;
            $scope.year = <%= Now.Year %>;

            $scope.fetchItemsForMonth = function () {
                
                $scope.monthName = function() {
                    var month = _.findWhere($scope.months, {month: $scope.month});
                    if (month) {
                        return month.name;
                    }
                    return "";
                };

                $scope.month = this.month ? this.month.month || 1 : 1;
                $scope.error = "";
                $scope.loading = true;
                var postData = {
                    year: $scope.year, 
                    month: $scope.month,
                    category: $scope.categoryId
                };
                $.post(
                    "/widgets/NewsList/Data.ashx",
                    postData,
                    function(data, textStatus, jqXHR) {
                        $scope.$apply(function($s) {
                            $s.loading = false;
                            if (data.error) {
                                $s.error = data.error;
                            } else if (data.results) {
                                $s.newsItems = data.results;
                            }
                        });
                    }
                );
            };
        }
    );
</script>