﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Models;
using Edwards.Utility;
using Ektron.Newtonsoft.Json;
using Ektron.Newtonsoft.Json.Linq;

public partial class widgets_NewsList_Views_List : NewsListViewControlBase
{
    protected DateTime Now { get; set; }
    protected IEnumerable<NewsItem> Items { get; set; }
    private int ListItemSize = Constants.NewsListNumberOfItems;

    public string months
    {
        get
        {
            return JsonConvert.SerializeObject(Enumerable.Range(1, 12).Select(i => JObject.FromObject(new
            {
                month = i,
                name = Constants.CurrentCulture.DateTimeFormat.GetMonthName(i)
            })));
        }
    }

    public override void Initialize(IEnumerable<NewsItem> items)
    {
        Items = items;
        if (Items == null || !Items.Any())
        {
            noItemsFoundMessage.Visible = false;
            return;
        }

        if (!SingleItem)
        {
            // Filter by the current year/month
            var now = DateTime.UtcNow;
            Now = now;
            Items = Items.OrderByDescending(f => f.Date).Take(ListItemSize);
        }
        else
        {
            var item = Items.FirstOrDefault();
            if (item != null && item.Date.HasValue)
            {
                Now = item.Date.Value;
            }
        }
        noItemsFoundMessage.Visible = !Items.Any();

        newsItemsRepeater.DataSource = Items;
        newsItemsRepeater.DataBind();
    }
}