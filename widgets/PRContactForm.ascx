﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="PRContactForm.ascx.cs" Inherits="Widget_PRContactForm" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div id="form">
            <asp:Literal ID="uxPRContactHeader" runat="server" />
            <div class="one-half">
                <br />
                <div class="form-item">
                    <asp:Literal ID="uxFormLabel1" runat="server" />
                    <asp:TextBox ID="uxTextBox1" runat="server" />
                    <asp:HiddenField ID="uxHiddenTB1value" runat="server" />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                            runat="server" ControlToValidate="uxTextBox1" ValidationGroup="PRFormGroup" 
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-item">
                    <asp:Literal ID="uxFormLabel2" runat="server" />
                    <asp:TextBox ID="uxTextBox2" runat="server" />
                    <asp:HiddenField ID="uxHiddenTB2value" runat="server" />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                            runat="server" ControlToValidate="uxTextBox2"  ValidationGroup="PRFormGroup"
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-item">
                    <asp:Literal ID="uxFormLabel3" runat="server" />
                    <asp:TextBox ID="uxTextBox3" runat="server" />
                    <asp:HiddenField ID="uxHiddenTB3value" runat="server" />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                            runat="server" ControlToValidate="uxTextBox3"  ValidationGroup="PRFormGroup"
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <br />
            <div class="one-half omega">
                <div class="form-item">
                    <asp:Literal ID="uxFormLabel4" runat="server" />
                    <asp:TextBox ID="uxTextBox4" runat="server" />
                    <asp:HiddenField ID="uxHiddenTB4value" runat="server" />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
                            runat="server" ControlToValidate="uxTextBox4"  ValidationGroup="PRFormGroup"
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-item">
                    <asp:Literal ID="uxFormLabel5" runat="server" />
                    <asp:TextBox ID="uxTextBox5" runat="server" />
                    <asp:HiddenField ID="uxHiddenTB5value" runat="server" />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" 
                            runat="server" ControlToValidate="uxTextBox5"  ValidationGroup="PRFormGroup"
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-item">
                    <asp:Literal ID="uxFormLabel6" runat="server" />
                    <asp:TextBox ID="uxTextBox6" runat="server" />
                    <asp:HiddenField ID="uxHiddenTB6value" runat="server" />

                    <div class="form-validation">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" 
                            runat="server" ControlToValidate="uxTextBox6" ValidationGroup="PRFormGroup"
                            Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-item">
                    <label><asp:Literal ID="uxRequiredText" runat="server" /></label>
                    <asp:Button ID="uxButton" CssClass="submitBtn" runat="server" ValidationGroup="PRFormGroup" onclick="btnSubmit_Click" />
                    <br />
                    <br />
                    <div class="form-validation">
                        <asp:Literal ID="uxErrorMessage" runat="server" Visible="false" />
                    </div>
                </div>
            </div>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditFormContentlabel" runat="server" /><br />
                    <asp:DropDownList ID="uxEditFormDropDown" runat="server" />
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditToEmailLabel" runat="server" /><br />
                    <asp:TextBox ID="uxEditToEmailTextBox" Columns="35" runat="server" />
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditRedirectLabel" runat="server" /><br />
                    <asp:DropDownList ID="uxEditRedirectDropDown" runat="server" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" ValidateRequestMode="Disabled" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>

