﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortalCalloutCenter.ascx.cs" Inherits="widgets_PortalCalloutCenter" %>
<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">

        <h3><asp:Label ID="uxSectionHeader" runat="server"></asp:Label></h3>
                
        <div id="iconBar" class="small" runat="server" ClientIDMode="Static">
            <asp:ListView ID="uxKnowledgeCentreListView" runat="server">
                <EmptyDataTemplate>
                    <div>
                        <br />
                        <%= GetLocalResourceObject("NoCalloutsSelected.Text") %>
                    </div>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                </LayoutTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="uxCalloutLink" runat="server">
                        <asp:Literal ID="uxCalloutContentLit" runat="server"></asp:Literal>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <div class="Item">
                <asp:Label ID="uxEditHeaderLabel" runat="server"></asp:Label>
                <asp:TextBox ID="uxEditHeaderTextBox" runat="server" Columns="35"></asp:TextBox>
            </div>
            <div class="Item">
                <asp:Label ID="uxEditCalloutDropDownLabel1" runat="server"></asp:Label>
                <asp:DropDownList Id="uxEditCalloutDropDown1" runat="server"></asp:DropDownList>
            </div>
            <div class="Item">
                <asp:Label ID="uxEditCalloutDropDownLabel2" runat="server"></asp:Label>
                <asp:DropDownList Id="uxEditCalloutDropDown2" runat="server"></asp:DropDownList>
            </div>
            <div class="Item">
                <asp:Label ID="uxEditCalloutDropDownLabel3" runat="server"></asp:Label>
                <asp:DropDownList Id="uxEditCalloutDropDown3" runat="server"></asp:DropDownList>
            </div>
            <div class="Item">
                <asp:Label ID="uxEditCalloutDropDownLabel4" runat="server"></asp:Label>
                <asp:DropDownList Id="uxEditCalloutDropDown4" runat="server"></asp:DropDownList>
            </div>
            <div class="Item">
                <asp:Label ID="uxEditCalloutDropDownLabel5" runat="server"></asp:Label>
                <asp:DropDownList Id="uxEditCalloutDropDown5" runat="server"></asp:DropDownList>
            </div>
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
        </div>
    </asp:View>
</asp:MultiView>
