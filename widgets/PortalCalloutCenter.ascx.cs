﻿using Edwards.Models;
using Edwards.Utility;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Widget;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;

public partial class widgets_PortalCalloutCenter : System.Web.UI.UserControl
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Portal Callout Center Widget";
    private string _sitepath = string.Empty;
    private long? smartformId = Constants.SmartFormIds.Callout;
    private long portalCalloutFolderId = 0;

    [WidgetDataMember("Section Header:")]
    public string HeaderLabel { get; set; }

    [WidgetDataMember("Downloads and Quicklinks")]
    public string Header { get; set; }

    [WidgetDataMember("Callout 1:")]
    public string CalloutDropDownLabel1 { get; set; }

    [WidgetDataMember("Callout 2:")]
    public string CalloutDropDownLabel2 { get; set; }

    [WidgetDataMember("Callout 3:")]
    public string CalloutDropDownLabel3 { get; set; }

    [WidgetDataMember("Callout 4:")]
    public string CalloutDropDownLabel4 { get; set; }

    [WidgetDataMember("Callout 5:")]
    public string CalloutDropDownLabel5 { get; set; }

    [WidgetDataMember("")]
    public string Callout1 { get; set; }

    [WidgetDataMember("")]
    public string Callout2 { get; set; }

    [WidgetDataMember("")]
    public string Callout3 { get; set; }

    [WidgetDataMember("")]
    public string Callout4 { get; set; }

    [WidgetDataMember("")]
    public string Callout5 { get; set; }

    public List<CalloutInfo> ListOfCallouts = new List<CalloutInfo>();

    private long PortalCalloutsFolderId
    {
        get
        {
            if(portalCalloutFolderId == 0)
            {
                long.TryParse(ConfigurationManager.AppSettings["PortalCalloutsFolderId"], out portalCalloutFolderId);
            }

            return portalCalloutFolderId;
        }
    }

    private string SitePath
    {
        get
        {
            if (string.IsNullOrEmpty(_sitepath))
            {
                _sitepath = new CommonApi().SitePath;
            }
            return _sitepath;
        }
    }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();

    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        uxEditHeaderLabel.Text = HeaderLabel;
        uxEditCalloutDropDownLabel1.Text = CalloutDropDownLabel1;
        uxEditCalloutDropDownLabel2.Text = CalloutDropDownLabel2;
        uxEditCalloutDropDownLabel3.Text = CalloutDropDownLabel3;
        uxEditCalloutDropDownLabel4.Text = CalloutDropDownLabel4;
        uxEditCalloutDropDownLabel5.Text = CalloutDropDownLabel5;

        PopulateCalloutDropDown(uxEditCalloutDropDown1);
        PopulateCalloutDropDown(uxEditCalloutDropDown2);
        PopulateCalloutDropDown(uxEditCalloutDropDown3);
        PopulateCalloutDropDown(uxEditCalloutDropDown4);
        PopulateCalloutDropDown(uxEditCalloutDropDown5);

        if (!string.IsNullOrEmpty(Header)) { uxEditHeaderTextBox.Text = Header; }
        if (!string.IsNullOrEmpty(Callout1)) { uxEditCalloutDropDown1.Text = Callout1; }
        if (!string.IsNullOrEmpty(Callout2)) { uxEditCalloutDropDown2.Text = Callout2; }
        if (!string.IsNullOrEmpty(Callout3)) { uxEditCalloutDropDown3.Text = Callout3; }
        if (!string.IsNullOrEmpty(Callout4)) { uxEditCalloutDropDown4.Text = Callout4; }
        if (!string.IsNullOrEmpty(Callout5)) { uxEditCalloutDropDown5.Text = Callout5; }

        ViewSet.SetActiveView(Edit);
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        Header = uxEditHeaderTextBox.Text;
        Callout1 = uxEditCalloutDropDown1.Text;
        Callout2 = uxEditCalloutDropDown2.Text;
        Callout3 = uxEditCalloutDropDown3.Text;
        Callout4 = uxEditCalloutDropDown4.Text;
        Callout5 = uxEditCalloutDropDown5.Text;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        uxSectionHeader.Text = Header;

        InitializeCalloutList();
        if (ListOfCallouts != null && ListOfCallouts.Count > 0)
        {
            LoadListResults();
            iconBar.Attributes.Add("data-Items", ListOfCallouts.Count.ToString());
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateCalloutDropDown(DropDownList calloutDropDown)
    {
        List<ContentData> calloutList = ContentUtility.GetListBySmartformIdAndFolder(smartformId, PortalCalloutsFolderId);

        if (calloutList != null & calloutList.Count > 0)
        {
            calloutDropDown.Items.Clear();
            calloutDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in calloutList)
            {
                calloutDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void InitializeCalloutList()
    {
        CalloutInfo calloutInfo = new CalloutInfo();
        long _cId1; long _cId2;
        long _cId3; long _cId4;
        long _cId5;

        ListOfCallouts.Clear();
        if (long.TryParse(Callout1, out _cId1))
        {
            calloutInfo = ContentUtility.GetCallout(_cId1);
            ListOfCallouts.Add(calloutInfo);
        }
        if (long.TryParse(Callout2, out _cId2))
        {
            calloutInfo = ContentUtility.GetCallout(_cId2);
            ListOfCallouts.Add(calloutInfo);
        }
        if (long.TryParse(Callout3, out _cId3))
        {
            calloutInfo = ContentUtility.GetCallout(_cId3);
            ListOfCallouts.Add(calloutInfo);
        }
        if (long.TryParse(Callout4, out _cId4))
        {
            calloutInfo = ContentUtility.GetCallout(_cId4);
            ListOfCallouts.Add(calloutInfo);
        }
        if (long.TryParse(Callout5, out _cId5))
        {
            calloutInfo = ContentUtility.GetCallout(_cId5);
            ListOfCallouts.Add(calloutInfo);
        }
    }

    protected void LoadListResults()
    {
        uxKnowledgeCentreListView.DataSource = ListOfCallouts;
        uxKnowledgeCentreListView.ItemDataBound += (sender, args) =>
        {
            var callout = args.Item.DataItem as CalloutInfo;

            if (callout == null)
                return;

            var calloutLink = args.Item.FindControl("uxCalloutLink") as HyperLink;
            var calloutContent = args.Item.FindControl("uxCalloutContentLit") as Literal;

            if (calloutLink == null || calloutContent == null)
                return;

            if (!string.IsNullOrEmpty(callout.Icon)) { calloutLink.Attributes["class"] = callout.Icon; }
            calloutLink.NavigateUrl = callout.LinkSrc;
            calloutContent.Text = callout.LinkText;
        };
        uxKnowledgeCentreListView.DataBind();
    }
}