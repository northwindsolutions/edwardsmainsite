﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductRotator.ascx.cs" Inherits="widgets_ProductRotator" %>
<%@ Import Namespace="Edwards.Models" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <asp:PlaceHolder runat="server" ID="productContainerPL">
            <div id="productContainer">
                <h2><strong><%= ProductRotator != null ? ProductRotator.Title : string.Empty %></strong></h2>
                <div id="productNav">
                    <a href="#" class="productPrev"></a>
                    <a href="#" class="productNext"></a> 
                </div>
                <div id="productWrap">
                    <div id="productSlideshow">
                        <ul>
                            <asp:Repeater runat="server" ID="productRepeater">
                                <ItemTemplate>
                                    <li>
                                        <img class="alignright" src="<%# ((Product)Container.DataItem).ImageUrl %>" alt="<%# ((Product)Container.DataItem).ImageAlt %>" title="<%# ((Product)Container.DataItem).ImageAlt %>" />
                                        <strong><%# ((Product)Container.DataItem).Title %></strong><br>
                                        <%# ((Product)Container.DataItem).Summary %><br>
                                        <a href="<%# ((Product)Container.DataItem).LinkHref %>" target="<%# ((Product)Container.DataItem).LinkTarget %>">Find out more<span class="icon"></span></a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <div id="productWrap-print">
                    <div id="productSlideshow-print">
                        <ul>
                            <asp:Repeater runat="server" ID="productRepeaterPrintMode">
                                <ItemTemplate>
                                    <li>
                                        <img class="alignright" src="<%# ((Product)Container.DataItem).ImageUrl %>" alt="<%# ((Product)Container.DataItem).ImageAlt %>" title="<%# ((Product)Container.DataItem).ImageAlt %>" />
                                        <strong><%# ((Product)Container.DataItem).Title %></strong><br>
                                        <%# ((Product)Container.DataItem).Summary %><br>
                                        <a href="<%# ((Product)Container.DataItem).LinkHref %>" target="<%# ((Product)Container.DataItem).LinkTarget %>">Find out more<span class="icon"></span></a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <fieldset>
                <label>Product Rotator:</label>
                <asp:DropDownList runat="server" ID="productRotatorDropdown"/>
            </fieldset>
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
