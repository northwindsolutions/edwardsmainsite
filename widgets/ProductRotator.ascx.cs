using System;
using System.Linq;
using Edwards.Utility;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using System.Configuration;
using Edwards.Models;

public partial class widgets_ProductRotator : System.Web.UI.UserControl, IWidget
{
    #region properties

    public int CurrentLanguage
    {
        get { return Constants.CurrentLanguageId; }
    }

    [WidgetDataMember(0)]
    public long ProductRotatorId { get; set; }

    public ProductRotator ProductRotator { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        string sitepath = new CommonApi().SitePath;
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronJS);
        JS.RegisterJSInclude(this, JS.ManagedScript.EktronModalJS);
        //JS.RegisterJS(this, "/library/javascript/angular.min.js", "AngularJS");
        //JS.RegisterJS(this, "/library/javascript/underscore-min.js", "UnderscoreJS");
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronModalCss);
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        m_refMsg = m_refContentApi.EkMsgRef;
        _host.Title = m_refMsg.GetMessage("Product Rotator Widget");
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        CancelButton.Text = m_refMsg.GetMessage("btn cancel");
        SaveButton.Text = m_refMsg.GetMessage("btn save");  
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        string myPath = string.Empty;
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ek_helpDomainPrefix"]))
        {
            string helpDomain = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            if ((helpDomain.IndexOf("[ek_cmsversion]") > 1))
            {
                myPath = helpDomain.Replace("[ek_cmsversion]", new CommonApi().RequestInformationRef.Version);
            }
            else
            {
                myPath = ConfigurationManager.AppSettings["ek_helpDomainPrefix"];
            }
        }
        else
        {
            myPath = sitepath + "Workarea/help";
        }
        //_host.HelpFile = myPath + "EktronReferenceWeb.html#Widgets/Creating_the_Hello_World_Widget.htm";
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        productRotatorDropdown.DataSource = ContentUtility.GetListBySmartformId(Constants.SmartFormIds.ProductRotator);
        productRotatorDropdown.DataTextField = "Title";
        productRotatorDropdown.DataValueField = "Id";
        productRotatorDropdown.DataBind();

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        long _rotatorId;
        if (long.TryParse(productRotatorDropdown.SelectedValue, out _rotatorId))
        {
            ProductRotatorId = _rotatorId;
        }

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        ProductRotator = ContentUtility.GetItemByContentId(ProductRotatorId).ToProductRotatorSmartform();

        if (ProductRotator != null && ProductRotator.Products != null && ProductRotator.Products.Any())
        {
            productRepeater.DataSource = 
                productRepeaterPrintMode.DataSource = ProductRotator.Products;
            productRepeater.DataBind();
            productRepeaterPrintMode.DataBind();
            productContainerPL.Visible = true;
        }
        else
        {
            productContainerPL.Visible = false;
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

}

