﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PromotionalItemForm.ascx.cs" Inherits="library_includes_controls_PromotionalItemForm" %>
<asp:MultiView ID="ViewSet" runat="server">
    <asp:View ID="View" runat="server">
        <asp:PlaceHolder ID="phForm" runat="server">
            <div id="resetForm">
                 
                <div class="one-fourth promoAcc">
                <p class="form-validation">
                    <span id="fNameFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoFirstNameFail") %></span>
                    <span id="lNameFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoLastNameFail") %></span>
                    <span id="companyFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoCompanyFail") %></span>
                    <span id="emailAddFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidEmail") %></span>
                    <span id="addLine1Fail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidAdd1") %></span>
                    <span id="cityFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidCity") %></span>
                    <span id="regionFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidRegion") %></span>
                    <span id="stateFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidState") %></span>
                    <span id="postalcodeFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidPostalCode") %></span>
                    <span id="edwardscontactFail"><%= GetGlobalResourceObject("EdwardsPortal", "PromoInvalidEdwarsContact") %></span>
                </p>
               
                    <div class="form-item promo_first_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoFirstName") %></strong></p>
                        <asp:TextBox ID="fName" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                    </div>
                    <div class="form-item promo_last_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoLastName") %></strong></p>
                        <asp:TextBox ID="lName" runat="server" ClientIDMode="Static" MaxLength="15"></asp:TextBox>
                    </div>
                    <div class="form-item promo_company">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoComapny") %></strong></p>
                        <asp:TextBox ID="company" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                    </div>
                    <div class="form-item promo_email_item">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "Promoemail") %></strong></p>
                        <asp:TextBox ID="emailAdd" runat="server" TextMode="Email" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                    </div>
                    <div class="form-item promo_shipping">
                        <div class="promo_shipping_title"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoShipping") %></strong></p></div>
                        <div class="promo_add1"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "Promoadd1") %></strong> </p><asp:TextBox ID="addLine1" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox></div>
                        <div class="promo_add2"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "Promoadd2") %></strong> </p><asp:TextBox ID="addLine2" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox></div>
                        <div class="promo_city"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoCity") %></strong></p> <asp:TextBox ID="city" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox></div>
                        <div class="promo_region"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoRegion") %></strong></p> <asp:TextBox ID="region" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox></div>
                        <div class="promo_state"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoState") %></strong></p> <asp:TextBox ID="state" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox></div>
                        <div class="promo_postcode"><p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoPostalCode") %></strong></p> <asp:TextBox ID="postalcode" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox></div>
                    </div>
                    <div class="form-item promo_contact">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoEdwardsContact") %></strong></p>
                        <asp:TextBox ID="edwardscontact" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                    </div>
                    <div class="form-item promo_des">
                        <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "PromoDecription") %></strong></p>
                        <asp:TextBox ID="descriptionfield" runat="server" ClientIDMode="Static" MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <asp:HiddenField ID="qunaitemval" runat="server" ClientIDMode="Static" />
                    <div class="form-item promo_save_item">
                        <asp:Button CssClass="submitBtn" ID="sumbitItems" runat="server" OnClick="HandleSubmitClick" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
            <br class="clear" />
        </asp:PlaceHolder>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div class="CBWidget">
            <div class="CBEdit">
                Email Id : <asp:TextBox ID="txtEmail" TextMode="Email" runat="server" />
                <asp:Label ID="errorLb" runat="server" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" />
            </div>
        </div>
    </asp:View>
</asp:MultiView>
<asp:HiddenField ID="hdnEmailId" runat="server" />
