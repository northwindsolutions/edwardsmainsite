﻿using Edwards.Portal;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Cms.Widget;
using System;
using System.Text;
using System.Web;

public partial class library_includes_controls_PromotionalItemForm : System.Web.UI.UserControl, IWidget
{

    Ektron.Cms.PageBuilder.WidgetHost _widgetHost;
    IWidgetHost _host;
    protected Ektron.Cms.ContentAPI m_refContentApi = new Ektron.Cms.ContentAPI();
    protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;
    private readonly AuthenticationService _authenticationService;
    private PortalUser _currentUser;
    private string _EmailId;

    [WidgetDataMember("")]
    public string EmailId { get { return _EmailId; } set { _EmailId = value; } }

    public library_includes_controls_PromotionalItemForm()
    {
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
        this._authenticationService = new AuthenticationService();
    }

    protected PortalUser CurrentUser
    {
        get
        {
            if (this._currentUser == null)
            {
                this._currentUser = this._authenticationService.GetCurrentUser();
            }

            return this._currentUser;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
       this.RenderLabels();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = "Promotion Item";//m_refMsg.GetMessage("Promotion Item");
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        this.EnableViewState = false;
        ViewSet.SetActiveView(View);

        this.PrefillUserDetails();
    }

    void EditEvent(string settings)
    {
        txtEmail.Text = EmailId;
        ViewSet.SetActiveView(Edit);
    }

    void PrefillUserDetails()
    {
        if (this.CurrentUser != null && this.CurrentUser != PortalUser.Anonymous)
        {
            fName.Text = this.CurrentUser.FirstName ?? string.Empty;
            lName.Text = this.CurrentUser.LastName ?? string.Empty;
            company.Text = this.CurrentUser.CompanyName ?? string.Empty;
            emailAdd.Text = this.CurrentUser.Email ?? string.Empty;
        }
    }

    protected void HandleSubmitClick(object sender, EventArgs e)
    {
        string script = "$(document).ready(function () { $('[id*=sumbitItems]').click(); });";
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

        StringBuilder sbPromoItem = new StringBuilder();
        if (!string.IsNullOrEmpty(EmailId)) {

            var items = qunaitemval.Value.Split(',');
            sbPromoItem.Append("<h3> Contact Details </h3>");
            sbPromoItem.AppendLine("<table border=\"1\">");
            //sbPromoItem.Append("<th>");
            sbPromoItem.Append("<th>First Name</th>");
            sbPromoItem.Append("<th>Last Name</th>");
            sbPromoItem.Append("<th>Email Address</th>");
            sbPromoItem.Append("<th>Company</th>");
            sbPromoItem.Append("<th>Address Line 1</th>");
            sbPromoItem.Append("<th>Address Line 2</th>");
            sbPromoItem.Append("<th>City</th>");
            sbPromoItem.Append("<th>Region</th>");
            sbPromoItem.Append("<th>State</th>");
            sbPromoItem.Append("<th>Postal Code</th>");
            sbPromoItem.Append("<th>Edwards Contact</th>");
            sbPromoItem.Append("<th>Additional Info</th>");
            
            //sbPromoItem.Append("</th>");
            sbPromoItem.Append("<tr>");
            sbPromoItem.Append("<td>" + fName.Text + "</td>");
            sbPromoItem.Append("<td>" + lName.Text + "</td>");
            sbPromoItem.Append("<td>" + emailAdd.Text + "</td>");
            sbPromoItem.Append("<td>" + company.Text + "</td>");
            sbPromoItem.Append("<td>" + addLine1.Text + "</td>");
            sbPromoItem.Append("<td>" + addLine2.Text + "</td>");
            sbPromoItem.Append("<td>" + city.Text + "</td>");
            sbPromoItem.Append("<td>" + region.Text + "</td>");
            sbPromoItem.Append("<td>" + state.Text + "</td>");
            sbPromoItem.Append("<td>" + postalcode.Text + "</td>");
            sbPromoItem.Append("<td>" + edwardscontact.Text + "</td>");
            sbPromoItem.Append("<td>" + descriptionfield.Text + "</td>");
            sbPromoItem.Append("</tr>");
            sbPromoItem.Append("</table> <br/>");
            if (items != null ? items.Length > 0 : false)
            {                
                sbPromoItem.Append("<h3>Promotional Item Details</h3><br/>");
                sbPromoItem.Append("<table border=\"2\">");                
                sbPromoItem.Append("<th>Item Name</th>");
                sbPromoItem.Append("<th>Quantity</th>");
                sbPromoItem.Append("<th>Image</th>");
                
                for (var i = 0; i < items.Length; i++)
                {
                    if (!string.IsNullOrEmpty(items[i]))
                    {
                        string DomainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        sbPromoItem.Append("<tr>");
                        var itemDetails = items[i].Split("||".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        sbPromoItem.Append("<td>" + itemDetails[0] + "</td>");
                        sbPromoItem.Append("<td>" + itemDetails[1] + "</td>");
                        sbPromoItem.Append("<td>" + DomainName +"/"+ itemDetails[2] + "</td>");                        
                        sbPromoItem.Append("</tr>");
                    }
                }
                
                sbPromoItem.Append("</table>");                                
            }


            
            string val = sbPromoItem.ToString();            
			Backend.SendEmail(EmailId, val, "Promotional Items Request");
            this.RouteToDashboard();
        
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtEmail.Text))
        {
            hdnEmailId.Value = txtEmail.Text;
            EmailId = txtEmail.Text;
            _host.SaveWidgetDataMembers();
            ViewSet.SetActiveView(View);
        }
        else
            errorLb.Text = "Please enter an email address.";
    }
    private void RouteToDashboard()
    {
        // To do: Route to dashboard...
        var route = this._routeBuilder.DashBoard();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private void RenderLabels()
    {
        sumbitItems.Text = GetGlobalResourceObject("EdwardsPortal", "PromoSubmitRequest").ToString();
        fName.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoFirstName").ToString());
        lName.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoLastName").ToString());
        company.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoComapny").ToString());
        emailAdd.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "Promoemail").ToString());
        addLine1.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "Promoadd1").ToString());
        addLine2.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "Promoadd2").ToString());
        addLine2.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "Promoadd2").ToString());
        city.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoCity").ToString());
        region.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoRegion").ToString());
        state.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoState").ToString());
        postalcode.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoPostalCode").ToString());
        edwardscontact.Attributes.Add("placeholder", "Edward's Contact");
    }
}