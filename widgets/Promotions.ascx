﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="Promotions.ascx.cs" Inherits="promotions_Banner" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div class="callout blue">
            <asp:Literal ID="uxPromotionsTitle" runat="server" />
            <asp:ListView ID="uxPromotionsListView" runat="server">
                <EmptyDataTemplate>
                    <div>
                        <br />
                        <%= GetLocalResourceObject("NoPromotions.Text") %>
                    </div>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="block">
                        <div id="uxPromotionsDiv" class="bg" runat="server">
                            <asp:HyperLink ID="uxPromotionsLink" runat="server">
                                <asp:Literal ID="uxPromotionsContentLit" runat="server" />
                            </asp:HyperLink>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditPromotionsLabel" runat="server" /><br />
                    <asp:DropDownList ID="uxEditPromotionsDropDown" runat="server" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>

