using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class promotions_Banner : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Promotions Widget";
    private string _sitepath = string.Empty;
    private long? smartformId = Constants.SmartFormIds.Promotions;
    private long? folderId = Constants.FolderIds.PromotionsFolderId;

    [WidgetDataMember("Choose Promotion Content:")]
    public string PromotionDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedPromotionContent { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulatePromotionsDropDown();
        uxEditPromotionsLabel.Text = PromotionDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedPromotionContent)) { uxEditPromotionsDropDown.SelectedValue = SelectedPromotionContent; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedPromotionContent = uxEditPromotionsDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;

        if (long.TryParse(SelectedPromotionContent, out contentId) && contentId > 0)
        {
            var promotionList = ContentUtility.GetPromotionsList(contentId);

            if (promotionList != null && promotionList.Count > 0)
            {
                uxPromotionsTitle.Text = String.IsNullOrEmpty(promotionList[0].Title) ?
                                       !String.IsNullOrEmpty(promotionList[0].Icon) ?
                                       "<h3 class=" + promotionList[0].Icon + ">&nbsp;</h3>" :
                                       string.Empty :
                                       !String.IsNullOrEmpty(promotionList[0].Icon) ?
                                       "<h3 class=" + promotionList[0].Icon + ">" + promotionList[0].Title + "</h3>" :
                                       "<h3>" + promotionList[0].Title + "</h3>";

                LoadListResults(promotionList);
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulatePromotionsDropDown()
    {
        var promotionList = ContentUtility.GetListBySmartformIdAndFolder(smartformId, folderId);

        if (promotionList != null & promotionList.Count > 0)
        {
            uxEditPromotionsDropDown.Items.Clear();
            uxEditPromotionsDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in promotionList)
            {
                uxEditPromotionsDropDown.Items.Add(new ListItem(
                item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void LoadListResults(List<PromotionsInfo> promotionList)
    {
        uxPromotionsListView.DataSource = promotionList;
        uxPromotionsListView.ItemDataBound += (sender, args) =>
        {
            var promoItem = args.Item.DataItem as PromotionsInfo;

            if (promoItem == null)
                return;

            var promoLink = args.Item.FindControl("uxPromotionsLink") as HyperLink;
            var promoContent = args.Item.FindControl("uxPromotionsContentLit") as Literal;
            var promoClass = args.Item.FindControl("uxPromotionsDiv") as HtmlControl;

            if (promoLink == null || promoContent == null || promoClass == null)
                return;

            promoClass.Attributes["style"] = @"background-image: url(" + promoItem.Image + @")";
            promoContent.Text = promoItem.Content;
            promoLink.NavigateUrl = promoItem.LinkSource;
            promoLink.Target = promoItem.LinkTarget;
        };
        uxPromotionsListView.DataBind();
    }
}

