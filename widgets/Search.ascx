﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="Search.ascx.cs" Inherits="widgets_Search" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <label>View Control</label>
                    <asp:DropDownList runat="server" ID="viewDropdown">
                        <asp:ListItem Selected="True" Value="" Text="-- Select --"></asp:ListItem>
                        <asp:ListItem Value="ManualsCallout.ascx" Text="Manuals Callout"></asp:ListItem>
                        <asp:ListItem Value="Results.ascx" Text="Results"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="Item">
                    <label>Search Configuration Settings</label>
                    <asp:DropDownList runat="server" ID="configsDropdown">
                    </asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>