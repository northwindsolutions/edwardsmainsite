using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_Search : System.Web.UI.UserControl, IWidget
{
    #region properties

    private const string ViewPath = "~/widgets/Search/Views/{0}";
    private const string WidgetTitle = "Search Widget";
    private string _sitepath = string.Empty;

    [WidgetDataMember(0)]
    public long SearchConfigContentId { get; set; }

    [WidgetDataMember("")]
    public string ViewControl { get; set; }

    protected string SitePath
    {
        get
        {
            if (string.IsNullOrEmpty(_sitepath))
            {
                _sitepath = new CommonApi().SitePath;
            }
            return _sitepath;
        }
    }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 

        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        var configs = ContentUtility.GetListBySmartformId(Constants.SmartFormIds.SearchConfig);
        configsDropdown.DataSource = configs;
        configsDropdown.DataTextField = "Title";
        configsDropdown.DataValueField = "Id";
        configsDropdown.DataBind();

        if (configs.Any(c => c.Id == SearchConfigContentId))
        {
            configsDropdown.SelectedValue = SearchConfigContentId.ToString();
        }

        viewDropdown.SelectedValue = ViewControl;
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        long _id;
        if (long.TryParse(configsDropdown.SelectedValue, out _id))
        {
            SearchConfigContentId = _id;
        }

        ViewControl = viewDropdown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        try
        {
            if (SearchConfigContentId <= 0)
                return;

            var config = ContentUtility.GetItemByContentId(SearchConfigContentId).ToSearchConfigSmartform();
            if (config == null || string.IsNullOrWhiteSpace(ViewControl))
                return;

            // Load up the view control from the views folder for search widgets.
            var viewCtrl = LoadControl(string.Format(ViewPath, ViewControl)) as SearchViewControlBase;
            if (viewCtrl == null)
                return;

            viewCtrl.SearchConfig = config;
            viewCtrl.Initialize();
            View.Controls.Add(viewCtrl);
        }
        catch
        {
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }
}

