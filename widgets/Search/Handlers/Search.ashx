﻿<%@ WebHandler Language="C#" Class="Search" %>

using System;
using System.Linq;
using System.Web;
using Edwards.Search;
using Edwards.Utility;
using Ektron.Cms.Search;
using Ektron.Newtonsoft.Json;

public class Search : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        try
        {
            var parameters = new QueryParams(context.Request.QueryString);

            if (!parameters.HasQueryText)
            {
                context.Response.Write(JsonConvert.SerializeObject(new { error = HttpContext.GetGlobalResourceObject("EdwardsGlobal", "NoQueryProvided") }));
                return;
            }

            var criteria = new KeywordSearchCriteria();
            criteria.QueryText = parameters.QueryText;

            if (parameters.FolderId > 0)
            {
                criteria.ExpressionTree = SearchContentProperty.FolderId == parameters.FolderId;
            }

            //if (TaxonomyIds != null && TaxonomyIds.Any())
            //{
            //    criteria.ExpressionTree = SearchProperties.TaxonomyId == TaxonomyIds.First();
            //}

            criteria.ReturnProperties.Add(SearchProperties.Title);
            criteria.ReturnProperties.Add(SearchProperties.QuickLink);
            criteria.ReturnProperties.Add(SearchProperties.Summary);
            criteria.ReturnProperties.Add(SearchProperties.Id);
            criteria.ReturnProperties.Add(SearchProperties.DateModified);

            criteria.PagingInfo = new Ektron.Cms.PagingInfo(parameters.PageSize, parameters.Page);

            if (parameters.LanguageId > 0)
            {
                criteria.Locale = Locale.Create(parameters.LanguageId);
            }

            var searchManager = new SearchManager();

            var response = searchManager.Search(criteria);
            var results = response.Results.Select(f => new
            {
                Title = f[SearchProperties.Title],
                Summary = f[SearchProperties.Summary],
                QuickLink = f[SearchProperties.QuickLink]
            });

            context.Response.Write(JsonConvert.SerializeObject(results));
        }
        catch (Exception ex)
        {
            context.Response.Write(JsonConvert.SerializeObject(new { error = ex.Message }));
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}