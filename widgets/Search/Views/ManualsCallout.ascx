﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManualsCallout.ascx.cs" Inherits="widgets_Search_Views_ManualsCallout" %>
<div class="callout">
    <h3><%= GetLocalResourceObject("Manuals.Text") %></h3>
    <div class="block">
        <div class="form-item">
            <div class="btn"><input id="<%= ClientID %>-button" type="button" value="<%= GetLocalResourceObject("Go.Text") %>"></div>
            <input id="<%= ClientID %>-text" type="text" value="<%= GetLocalResourceObject("Search.Text") %>" onfocus="clearMe(this)" onblur="restoreMe(this)" class="ip">
        </div>
    </div>
</div>
<script>
    jQuery(function ($) {
        var $text = $("#<%= ClientID %>-text");
        var $button = $("#<%= ClientID %>-button");

        function search() {
            var url = "<%= SearchConfig.SearchResultsUrl %>";
            url += (url.indexOf('?') >= 0) ? "&" : "?";
            url += "q=" + encodeURIComponent($text.val());
            <% if (FolderId.HasValue) { %>
            url += "&folder=<%= FolderId %>";
            <% } %>
            window.location = url;
        }

        $button.on('click', function (e) {
            search();
            e.preventDefault();
        });
        $text.on('keypress', function (e) {
            if (e.keyCode === 13) {
                search();
                e.preventDefault();
            }
        });
    });
</script>