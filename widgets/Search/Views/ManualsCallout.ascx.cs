﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class widgets_Search_Views_ManualsCallout : SearchViewControlBase
{
    protected long? FolderId
    {
        get
        {
            if (SearchConfig.FolderIds != null && SearchConfig.FolderIds.Any())
            {
                return SearchConfig.FolderIds.First();
            }
            return null;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void Initialize()
    {
    }
}