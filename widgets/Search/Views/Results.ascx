﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Results.ascx.cs" Inherits="widgets_Search_Views_Results" %>
<%@ Import Namespace="Edwards.Utility" %>
<%@ Import Namespace="Ektron.Newtonsoft.Json" %>

<div>
    <script>
        if (typeof(EdwardsApp) === 'undefined') { EdwardsApp = getEdwardsNgApp(); }
        EdwardsApp.controller('SearchResults',
            function SearchResults($scope, $sce) {
                $scope.trustIt = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                }

                $scope.q = "<%= Request.QueryString["q"].JavascriptStringEncode() %>";
                $scope.folderIds = <%= JsonConvert.SerializeObject(FolderIds) %>;
                $scope.loading = false;
                $scope.results = null;
            }
        );

        jQuery(function ($) {
            function ngScope() {
                return angular.element($searchResultsController[0]).scope();
            }

            function search(q) {
                if (q === "<%= DefaultText %>")
                    return;

                var $scope = ngScope();
                $scope.$apply(function ($s) {
                    $s.loading = true;
                });
                var data = { q: encodeURIComponent(q) };
                if ($scope.folderIds && $scope.folderIds.length > 0) {
                    data.folder = $scope.folderIds[0];
                }

                $.ajax({
                    url: "/widgets/search/handlers/search.ashx",
                    data: data,
                    dataType : "json",
                    success: function (data, textStatus, jqXHR) {
                        $scope.$apply(function ($s) {
                            $s.loading = false;
                            if (data.error) {
                                $s.results = null;
                            } else {
                                $s.results = data;
                            }
                        });
                    }
                });
            }

            var $searchResultsController = $("#searchResultsController");
            var $searchButton = $("#search-query input.submitBtn");
            var $searchBox = $("#search-query input[type='text']");

            $searchButton.on('click', function (e) {
                search($searchBox.val());
                e.preventDefault();
            });

            $searchBox.on('keypress', function(e) {
                if (e.keyCode == 13) {
                    search($searchBox.val());
                    e.preventDefault();
                }
            });

            var $scope = ngScope();
            if ($scope.q !== '') {
                $searchBox.val($scope.q);
                search($scope.q);
            }
        });
    </script>
    <div id="searchResultsController" ng-controller="SearchResults" ng-cloak>
        <div id="search-query" class="form-item">
            <input type="text" class="ip query" value="<%= DefaultText %>" onfocus="clearMe(this)" onblur="restoreMe(this)">
            <input type="button" class="submitBtn" value="<%= GetLocalResourceObject("Search.Text") %>"/>
        </div>

        <div id="search-results">
            <p ng-show="loading"><img src="/Workarea/images/application/loading_small.gif" alt="<%= GetGlobalResourceObject("EdwardsGlobal", "Loading") %>"/></p>
            <p ng-show="!loading && results == null || results.length == 0"><%= GetLocalResourceObject("NoResults.Text") %></p>
            <div ng-repeat="rslt in results">
                <h4>
                    <a ng-href="{{ rslt.QuickLink }}" target="_blank">{{ rslt.Title }}</a>
                </h4>
                <div ng-bind-html="trustIt(rslt.Summary)"></div>
            </div>
        </div>
    </div>
</div>