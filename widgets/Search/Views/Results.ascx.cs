﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edwards.Search;
using Ektron.Cms.API;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Expressions;
using Ektron.Cms.Search.Solr;

public partial class widgets_Search_Views_Results : SearchViewControlBase
{
    protected string DefaultText
    {
        get { return GetLocalResourceObject("DefaultQuery.Text").ToString(); }
    }

    protected ICollection<long> FolderIds { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        JS.RegisterJS(Page, @"/library/javascript/angular.min.js", "AngularJS");

        var folders = Request.QueryString.GetValues("folder");
        if (folders != null && folders.Any())
        {
            FolderIds = folders.Select(v =>
            {
                long _fid;
                if (long.TryParse(v, out _fid))
                {
                    return _fid;
                }
                return 0;
            })
            .Where(fid => fid > 0)
            .ToList();
        }
    }


    public override void Initialize()
    {
    }

}