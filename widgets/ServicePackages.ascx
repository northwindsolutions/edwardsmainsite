﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="ServicePackages.ascx.cs" Inherits="widgets_ServicePackage" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">

        <h3><asp:Label ID="uxSectionHeader" runat="server"></asp:Label></h3>
        <p><asp:Literal ID="uxHeaderContent" runat="server"></asp:Literal></p>

        <asp:ListView ID="uxServicePackagesListView" runat="server">
            <EmptyDataTemplate>
                <div>
                    <br />
                    <%= GetLocalResourceObject("NoPackages.Text") %>
                </div>
            </EmptyDataTemplate>
            <LayoutTemplate>
                <div id="packageList">
                    <asp:Literal ID="itemPlaceholder" runat="server"></asp:Literal>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="callout">
                	<div class="block">
                        <h4><strong><asp:HyperLink ID="uxPackageTitleLink" runat="server" class="titleLink" OnClick="createCookie();"></asp:HyperLink></strong></h4>
                        <br>
                        <asp:Literal ID="uxPackageContent" runat="server"></asp:Literal>
                    </div>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="callout omega">
                	<div class="block">
                        <h4><strong><asp:HyperLink ID="uxPackageTitleLink" runat="server" class="titleLink" OnClick="createCookie();"></asp:HyperLink></strong></h4>
                        <br>
                        <asp:Literal ID="uxPackageContent" runat="server"></asp:Literal>
                    </div>
                </div>
            </AlternatingItemTemplate>
        </asp:ListView>

    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditServicesDropDownLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditServicesDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

<script language="javascript" type="text/javascript">
    $(function () {
        function createCookie(event) {
            var buyerType = $(this).attr('buyerType');

            if (buyerType != null) {
                $.cookie('buyerType', buyerType, { expires: 7, path: '/' });
                return true;
            }
        }

        $('.titleLink').click(createCookie);
    });
</script>

