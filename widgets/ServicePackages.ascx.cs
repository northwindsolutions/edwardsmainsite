using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_ServicePackage : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Service Packages Widget";
    private string _sitepath = string.Empty;
    private long? smartformId = Constants.SmartFormIds.ServicePackages;

    [WidgetDataMember("Service Packages: ")]
    public string ServicesDropDownLabel { get; set; }
   
    [WidgetDataMember("")]
    public string SelectedServicesContent { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }
    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        JS.RegisterJS(Page, "/library/javascript/jquery.cookie.js", "jQueryCookie");

        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateServicePackagesDropDown();

        uxEditServicesDropDownLabel.Text = ServicesDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedServicesContent)) { uxEditServicesDropDown.SelectedValue = SelectedServicesContent; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedServicesContent = uxEditServicesDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedServicesContent, out contentId) && contentId > 0)
        {
            var packageList = ContentUtility.GetServicePackagesList(contentId);

            if (packageList != null && packageList.Count > 0)
            {
                uxSectionHeader.Text = packageList[0].Title;
                uxHeaderContent.Text = packageList[0].Content;

                LoadListResults(packageList);
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateServicePackagesDropDown()
    {
        List<ContentData> packageList = ContentUtility.GetListBySmartformId(smartformId);

        if (packageList != null & packageList.Count > 0)
        {
            uxEditServicesDropDown.Items.Clear();
            uxEditServicesDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in packageList)
            {
                uxEditServicesDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    protected void LoadListResults(List<ServicePackagesInfo> packageInfoList)
    {
        uxServicePackagesListView.DataSource = packageInfoList;
        uxServicePackagesListView.ItemDataBound += (sender, args) =>
        {
            var packageItem = args.Item.DataItem as ServicePackagesInfo;

            if (packageItem == null)
                return;

            var packageTitleLink = args.Item.FindControl("uxPackageTitleLink") as HyperLink;
            var packageContent = args.Item.FindControl("uxPackageContent") as Literal;

            if (packageTitleLink == null || packageContent == null)
                return;

            packageTitleLink.NavigateUrl = packageItem.TitleLinkSrc;
            packageTitleLink.Target = packageItem.TitleLinkTarget;
            packageTitleLink.Text = packageItem.TitleLinkText + "<span class='icon'></span>";
            packageTitleLink.Attributes.Add("buyerType", packageItem.BuyerType);

            packageContent.Text = packageItem.PackageContent;
        };
        uxServicePackagesListView.DataBind();
    }
}

