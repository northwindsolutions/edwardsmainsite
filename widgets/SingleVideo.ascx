﻿<%@ Control Language="C#" ClientIDMode="Static" AutoEventWireup="true" CodeFile="SingleVideo.ascx.cs" Inherits="widgets_SingleVideo" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <h3><asp:Label ID="uxSectionHeader" runat="server" /></h3>
        <asp:Literal ID="uxEmbeddedVideoCode" runat="server" />
        <p><asp:Literal ID="uxVideoText" runat="server" /></p>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditVideoLabel" runat="server" /><br />
                    <asp:DropDownList ID="uxEditVideoDropDown" runat="server" />
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>        
            </div>
        </div>
    </asp:View>
</asp:MultiView>