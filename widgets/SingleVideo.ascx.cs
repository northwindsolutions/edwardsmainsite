using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Organization;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_SingleVideo : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Single Video Widget";
    private string _sitepath = string.Empty;
    private long? smartformId = Constants.SmartFormIds.SingleVideo;

    [WidgetDataMember("Select Video Content:")]
    public string VideoLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedVideo { get; set; }
    
   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); }); 
        ViewSet.SetActiveView(View);
    }


    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateSingleVideoDropDown();

        uxEditVideoLabel.Text = VideoLabel;
        if (!string.IsNullOrEmpty(SelectedVideo)) { uxEditVideoDropDown.SelectedValue = SelectedVideo; }
        
        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedVideo = uxEditVideoDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long _contentId;

        if (long.TryParse(SelectedVideo, out _contentId))
        {
            SingleVideoInfo videoInfo = ContentUtility.GetSingleVideo(_contentId);

            if (videoInfo != null)
            {
                uxSectionHeader.Text = videoInfo.Title;
                uxEmbeddedVideoCode.Text = videoInfo.Video;
                uxVideoText.Text = videoInfo.Text;
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }


    protected void PopulateSingleVideoDropDown()
    {
        var videoList = ContentUtility.GetListBySmartformId(smartformId);

        if (videoList != null & videoList.Count > 0)
        {
            uxEditVideoDropDown.Items.Clear();
            uxEditVideoDropDown.Items.Add(new ListItem("", "")); //default value
            foreach (ContentData item in videoList)
            {
                uxEditVideoDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }
}

