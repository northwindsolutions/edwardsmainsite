﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteMap.ascx.cs" Inherits="widgets_SiteMap" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <asp:Literal ID="uxHeader" runat="server" />
        
        <div id="sitemap">
            <asp:ListView ID="uxMenuListView" runat="server" ItemPlaceholderID="aspItemPlaceholder">
                <LayoutTemplate>
                    <ul class="nav">
                        <asp:PlaceHolder ID="aspItemPlaceholder" runat="server" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <span id="mapBlock">
                    <li class="<%# ((Eval("Text").ToString() == "Webshop") ? "webshop " : string.Empty) + ((Eval("Type").ToString() == "SubMenu") ? "subnav " : string.Empty) %>">
                        <a href="<%# Eval("Href") %>" target="<%# Eval("Target") %>"><span class="navIcon"></span><%# Eval("Text") %></a>
                        <asp:ListView runat="server" ID="uxSubMenu" ItemPlaceholderID="subMenuPlaceholder" DataSource='<%# Eval("Items") %>'>
                            <LayoutTemplate>
                                <ul class="subnav">
                                    <asp:PlaceHolder runat="server" ID="subMenuPlaceholder" />
                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li class="<%# (Eval("Type").ToString() == "SubMenu" ? "subnav " : "item") %>">
                                    <asp:HyperLink ID="childLink" runat="server" Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("Href") %>' Target='<%# Eval("Target") %>'></asp:HyperLink>

                                    <asp:ListView runat="server" ID="uxSubMenu2" ItemPlaceholderID="subMenu2Placeholder" DataSource='<%# Eval("Items") %>'>
                                        <LayoutTemplate>
                                            <ul class="subnav">
                                                <asp:PlaceHolder runat="server" ID="subMenu2Placeholder"></asp:PlaceHolder>
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="item">
                                                <asp:HyperLink ID="sub2ChildLink" runat="server" Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("Href") %>' Target='<%# Eval("Target") %>'></asp:HyperLink>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                    </li>
                    </span>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditHeaderLabel" runat="server"></asp:Label>
                    <asp:TextBox Id="uxEditHeaderTextBox" runat="server" Columns="40"></asp:TextBox>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

