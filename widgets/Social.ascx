﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Social.ascx.cs" Inherits="widgets_Social" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <fieldset>
                <p>
                    <asp:Label ID="HeaderLabel" runat="server">Header</asp:Label>
                    <asp:TextBox ID="HeaderTextBox" runat="server" Style="width: 95%"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="TitleLabel" runat="server">Title</asp:Label>
                    <asp:TextBox ID="TitleTextBox" runat="server" Style="width: 95%"></asp:TextBox>
                </p>
            </fieldset>
            <p>
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            </p>
        </div>
    </asp:View>
        
</asp:MultiView>
