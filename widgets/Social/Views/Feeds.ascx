﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Feeds.ascx.cs" Inherits="widgets_Social_Views_Feeds" %>

<div id="expertView">
    <h3><%= Header %></h3>
    <h4><%= Title %></h4>
    <div id="conversations" class="block">
        <%--<ul class="tabs">
            <li class="selected"><%= GetLocalResourceObject("TwitterFeed.Text") %></li>
        </ul>--%>
        <ul id="twitterFeed" class="section"></ul>
    </div>
</div>
