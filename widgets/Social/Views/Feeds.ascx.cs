﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.API;

public partial class widgets_Social_Views_Feeds : SocialViewControlBase
{
    public override void Initialize()
    {
        JS.RegisterJS(this, "/library/javascript/twitter-fetcher.js", "TwitterFetcher");
        JS.RegisterJS(this, "/library/javascript/feed-ek.js", "FeedEk");
    }

    public void Page_Init(object sender, EventArgs e)
    {
        Initialize();
    }
}

