<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Spares.ascx.cs" Inherits="widgets_Spares" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div class="spare">
            <asp:PlaceHolder ID="phSparesContent" runat="server"></asp:PlaceHolder>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditContentLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditContentDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditViewLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditViewDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

