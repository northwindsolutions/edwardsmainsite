using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_Spares : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Spares Widget";
    private string _sitepath = string.Empty;
    private string controlPath = Constants.BaseSparesControlsFolder;
    private long? smartformId = Constants.SmartFormIds.Webshop;

    [WidgetDataMember("Spares: ")]
    public string SparesDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedSpares { get; set; }

    [WidgetDataMember("Spares View: ")]
    public string ViewDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedViewControl { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateSparesDropDown();
        PopulateViewDropDown();

        uxEditContentLabel.Text = SparesDropDownLabel;
        uxEditViewLabel.Text = ViewDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedSpares)) { uxEditContentDropDown.SelectedValue = SelectedSpares; }
        if (!string.IsNullOrEmpty(SelectedViewControl)) { uxEditViewDropDown.SelectedValue = SelectedViewControl; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedSpares = uxEditContentDropDown.SelectedValue;
        SelectedViewControl = uxEditViewDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedSpares, out contentId) && contentId > 0)
        {
            var webshopInfo = ContentUtility.GetWebshopInfo(contentId);
            if (webshopInfo != null)
            {
                if (DynamicControls.LoadSparesView(webshopInfo, Page, phSparesContent, SelectedViewControl))
                {
                    phSparesContent.Visible = true;
                }
                else
                {
                    phSparesContent.Visible = false;
                }
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateSparesDropDown()
    {
        List<ContentData> sparesList = ContentUtility.GetListBySmartformId(smartformId);

        if (sparesList != null & sparesList.Count > 0)
        {
            uxEditContentDropDown.Items.Clear();
            uxEditContentDropDown.Items.Add(new ListItem("", ""));

            foreach (ContentData item in sparesList)
            {
                uxEditContentDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }

    public void PopulateViewDropDown()
    {
        // Populate a list of available user controls to use for view rendering.
        var path = HttpContext.Current.Server.MapPath(controlPath);
        var controls = Directory.GetFiles(path, "*.ascx").Select(filename => Path.GetFileName(filename)).OrderByDescending(file => Path.GetFileName(file));

        uxEditViewDropDown.Items.Clear();
        uxEditViewDropDown.Items.Add(new ListItem("Select View Type...", string.Empty));
        foreach (var control in controls)
        {
            uxEditViewDropDown.Items.Add(new ListItem(control.Replace(".ascx", ""), control));
        }
    }
}

