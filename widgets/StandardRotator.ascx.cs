using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;
using System.Configuration;
using System.Collections.Generic;
using Edwards.Utility;
using Edwards.Models;


public partial class widgets_StandardRotator : System.Web.UI.UserControl, IWidget
{
    #region properties

    //Local constants
    private const string WidgetTitle = "Standard Rotator Widget";
    private string _sitepath = string.Empty;
    private string controlPath = Constants.BaseStandardRotatorControlsFolder;
    private long? SmartformId = Constants.SmartFormIds.StandardRotator;

    [WidgetDataMember("Standard Rotator: ")]
    public string RotatorDropDownLabel { get; set; }

    [WidgetDataMember("Rotator View: ")]
    public string ViewDropDownLabel { get; set; }

    [WidgetDataMember("")]
    public string SelectedRotator { get; set; }

    [WidgetDataMember("")]
    public string SelectedViewControl { get; set; }

    [WidgetDataMember("")]
    public string Result { get; set; }

   private string SitePath
   {
       get
       {
           if (string.IsNullOrEmpty(_sitepath))
           {
               _sitepath = new CommonApi().SitePath;
           }
           return _sitepath;
       }
   }

    public int? RotationInterval { get; set; }

    #endregion

    IWidgetHost _host;
    protected ContentAPI m_refContentApi = new ContentAPI();
   
    protected void Page_Init(object sender, EventArgs e)
    {   
        //string sitepath = new CommonApi().SitePath;
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = WidgetTitle;
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        string sitepath = new CommonApi().SitePath;
        Css.RegisterCss(this, sitepath + "widgets/edwards/CommonStyles.css", "WidgetCSS");

        PopulateRotatorDropDown();
        SharedControls.PopulateViewDropDown(uxEditViewDropDown, controlPath);

        uxEditRotatorDropDownLabel.Text = RotatorDropDownLabel;
        uxEditViewDropDownLabel.Text = ViewDropDownLabel;
        if (!string.IsNullOrEmpty(SelectedRotator)) { uxEditRotatorDropDown.SelectedValue = SelectedRotator; }
        if (!string.IsNullOrEmpty(SelectedViewControl)) { uxEditViewDropDown.SelectedValue = SelectedViewControl; }

        ViewSet.SetActiveView(Edit);   
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        SelectedRotator = uxEditRotatorDropDown.SelectedValue;
        SelectedViewControl = uxEditViewDropDown.SelectedValue;

        _host.SaveWidgetDataMembers();
        ViewSet.SetActiveView(View);
    }

    protected void SetOutput()
    {
        long contentId;
        if (long.TryParse(SelectedRotator, out contentId) && contentId > 0)
        {
            var rotatorList = ContentUtility.GetStandardRotatorList(contentId);

            if (rotatorList != null && rotatorList.Count > 0)
            {
                if (DynamicControls.LoadStandardRotatorView(rotatorList, Page, phStandardRotatorContent, SelectedViewControl))
                {
                    phStandardRotatorContent.Visible = true;
                }
                else
                {
                    phStandardRotatorContent.Visible = false;
                }
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }

    protected void PopulateRotatorDropDown()
    {
        List<ContentData> rotatorList = ContentUtility.GetListBySmartformId(SmartformId);

        if (rotatorList != null & rotatorList.Count > 0)
        {
            uxEditRotatorDropDown.Items.Clear();
            uxEditRotatorDropDown.Items.Add(new ListItem("", ""));
            foreach (ContentData item in rotatorList)
            {
                uxEditRotatorDropDown.Items.Add(new ListItem(
                    item.Title + " (" + item.Id + ")", item.Id.ToString()));
            }
        }
    }
}

