﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebShop.ascx.cs" Inherits="widgets_Webshop" %>

<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <asp:PlaceHolder ID="phWebshopContent" runat="server"></asp:PlaceHolder>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="Widget">
            <div class="Edit">
                <div class="Item">
                    <asp:Label ID="uxEditContentLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditContentDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Label ID="uxEditViewLabel" runat="server"></asp:Label>
                    <asp:DropDownList Id="uxEditViewDropDown" runat="server"></asp:DropDownList>
                </div>
                <div class="Item">
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" /> &nbsp;&nbsp;
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>

