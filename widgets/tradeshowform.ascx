﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tradeshowform.ascx.cs" Inherits="library_includes_controls_tradeshowform" %>
<asp:MultiView ID="ViewSet" runat="server">
    <asp:View ID="View" runat="server">
        <asp:PlaceHolder ID="phForm" runat="server">
            <div id="resetForm">
                <div class="one-fourth promoAcc">
                    <p><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowFormHeader") %></p>
                    <p><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowFormHeader2") %></p>
                    <p class="form-validation">
                        <span id="tradefNameFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowFirstNameFail") %></span>
                        <span id="tradelNameFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowLastNameFail") %></span>
                        <span id="tradecompanyFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowCompanyFail") %></span>
                        <span id="tradeemailAddFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowInvalidEmail") %></span>
                        <span id="tradeaddLine1Fail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowInvalidFormat") %></span>
                        <span id="tradedateFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowDateFail") %></span>
                        <span id="tradeedwardscontactFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowInvalidEdwarsContact") %></span>
                        <span id="tradeLanguageFail"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowInvalidLanguage") %></span>
                    </p>
                    <!-- 1st row -->
                    <div>
                        <div class="one-half">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowFirstName") %></strong></p>
                                <asp:TextBox ID="tradefName" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                        <div class="one-half omega">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowLastName") %></strong></p>
                                <asp:TextBox ID="tradelName" runat="server" ClientIDMode="Static" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <!-- 2nd row -->
                    <div>
                        <div class="one-half">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowCompany") %></strong></p>
                                <asp:TextBox ID="tradecompany" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                        <div class="one-half omega">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowEmail") %></strong></p>
                                <asp:TextBox ID="tradeemailAdd" runat="server" TextMode="Email" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <!-- 3rd row -->
                    <div>
                        <div class="one-half">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowRequestedLanguage") %></strong></p>
                                <asp:TextBox ID="tradeLanguage" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                        <div class="one-half omega">
                            <div>
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowCustomRequest") %></strong></p>
                                <asp:CheckBox id="isCustomGraphicRequest" runat="server" ClientIDMode="Static" />
                            </div>
                        </div>
                    </div>

                    <!-- 4th row -->
                    <div>
                        <div class="one-half">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowGraphicFormat") %></strong></p>
                                <asp:TextBox ID="tradegraphicFormat" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                        <div class="one-half omega">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowGraphicDate") %></strong></p>
                                <asp:TextBox ID="graphicDate" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    
                    <!-- 5th row -->
                    <div>
                        <div class="one-half">
                            <div class="form-item">
                                <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowMaincontact") %></strong></p>
                                <p class="reqText"><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowMaincontactapprovalmsg") %></p>
                                <asp:TextBox ID="tradeedwardscontact" runat="server" ClientIDMode="Static" MaxLength="40"></asp:TextBox>
                            </div>
                        </div>
                        <div class="one-half omega">

                        </div>
                    </div>                    

                    <!-- 6th row -->
                    <div class="full-width">
                        <div class="form-item">
                            <p><strong><%= GetGlobalResourceObject("EdwardsPortal", "TradeshowDescription") %></strong></p>
                            <asp:TextBox ID="tradedescriptionfield" runat="server" ClientIDMode="Static" MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>

                    <asp:HiddenField ID="qunaitemval" runat="server" ClientIDMode="Static" />
                    <div class="form-item promo_save_item">
                        <asp:Button CssClass="submitBtn" ID="sumbittradeItems"  runat="server" OnClick="HandleSubmitClick" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
            <br class="clear" />
        </asp:PlaceHolder>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div class="CBWidget">
            <div class="CBEdit">
                Email Id : <asp:TextBox ID="txtEmail" TextMode="Email" runat="server" />
                <asp:Label ID="errorLb" runat="server" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" />
            </div>
        </div>
    </asp:View>
</asp:MultiView>
<asp:HiddenField ID="hdnEmailId" runat="server" />
