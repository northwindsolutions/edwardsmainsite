﻿using Edwards.Portal;
using Edwards.Portal.Services;
using Edwards.Utility;
using Ektron.Cms.Widget;
using System;
using System.Text;
using System.Web;

public partial class library_includes_controls_tradeshowform : System.Web.UI.UserControl, IWidget
{

    Ektron.Cms.PageBuilder.WidgetHost _widgetHost;
    IWidgetHost _host;
    protected Ektron.Cms.ContentAPI m_refContentApi = new Ektron.Cms.ContentAPI();
    protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
    private readonly RouteBuilder _routeBuilder;
    private readonly Router _router;
    private readonly AuthenticationService _authenticationService;
    private PortalUser _currentUser;

    private string _EmailId;

    [WidgetDataMember("")]
    public string EmailId { get { return _EmailId; } set { _EmailId = value; } }

    public library_includes_controls_tradeshowform()
    {
        this._routeBuilder = new RouteBuilder();
        this._router = new Router();
        this._authenticationService = new AuthenticationService();
    }

    protected PortalUser CurrentUser
    {
        get
        {
            if (this._currentUser == null)
            {
                this._currentUser = this._authenticationService.GetCurrentUser();
            }

            return this._currentUser;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.RenderLabels();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = "Tradeshow Item";//m_refMsg.GetMessage("Promotion Item");
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        this.EnableViewState = false;
        ViewSet.SetActiveView(View);

        this.PrefillUserDetails();
    }

    void PrefillUserDetails()
    {
        if (this.CurrentUser != null && this.CurrentUser != PortalUser.Anonymous)
        {
            tradefName.Text = this.CurrentUser.FirstName ?? string.Empty;
            tradelName.Text = this.CurrentUser.LastName ?? string.Empty;
            tradecompany.Text = this.CurrentUser.CompanyName ?? string.Empty;
            tradeemailAdd.Text = this.CurrentUser.Email ?? string.Empty;
        }
    }

    void EditEvent(string settings)
    {
        txtEmail.Text = EmailId;
        ViewSet.SetActiveView(Edit);
        
    }

    protected void HandleSubmitClick(object sender, EventArgs e)
    {
        string script = "$(document).ready(function () { $('[id*=sumbitItems]').click(); });";
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

        StringBuilder sbPromoItem = new StringBuilder();
        if (!string.IsNullOrEmpty(EmailId))
        {
            var items = qunaitemval.Value.Split(',');
            sbPromoItem.Append("<h3> Contact Details </h3>");
            sbPromoItem.AppendLine("<table border=\"1\">");
            sbPromoItem.Append("<tr>");
            sbPromoItem.Append("<th>First Name</th>");
            sbPromoItem.Append("<th>Last Name</th>");
            sbPromoItem.Append("<th>Email Address</th>");
            sbPromoItem.Append("<th>Company</th>");
            sbPromoItem.Append("<th>Requested Language</th>");
            sbPromoItem.Append("<th>Custom Graphics Request</td>");
            sbPromoItem.Append("<th>Graphic Format</th>");
            sbPromoItem.Append("<th>Edwards Contact</th>");
            sbPromoItem.Append("<th>Additional Info</th>");
            sbPromoItem.Append("</tr>");

            sbPromoItem.Append("<tr>");
            sbPromoItem.Append("<td>" + tradefName.Text + "</td>");
            sbPromoItem.Append("<td>" + tradelName.Text + "</td>");
            sbPromoItem.Append("<td>" + tradeemailAdd.Text + "</td>");
            sbPromoItem.Append("<td>" + tradecompany.Text + "</td>");
            sbPromoItem.Append("<td>" + tradeLanguage.Text + "</td>");
            sbPromoItem.Append("<td>" + (isCustomGraphicRequest.Checked ? "Yes" : "No") + "</td>");
            sbPromoItem.Append("<td>" + tradegraphicFormat.Text + "</td>");
            sbPromoItem.Append("<td>" + tradeedwardscontact.Text + "</td>");
            sbPromoItem.Append("<td>" + tradedescriptionfield.Text + "</td>");
            sbPromoItem.Append("</tr>");

            sbPromoItem.Append("</table> <br/>");
            if (items != null ? items.Length > 0 : false)
            {
                sbPromoItem.Append("<h3>Trade Show Item Details</h3><br/>");
                sbPromoItem.Append("<table border=\"2\">");
                sbPromoItem.Append("<th>Item</th>");
                sbPromoItem.Append("<th>ImagePath</th>");

                for (var i = 0; i < items.Length; i++)
                {
                    if (!string.IsNullOrEmpty(items[i]))
                    {
                        string DomainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        sbPromoItem.Append("<tr>");
                        var itemDetails = items[i].Split("||".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        sbPromoItem.Append("<td>" + itemDetails[0] + "</td>");
                        sbPromoItem.Append("<td>" + DomainName + "/" + itemDetails[1] + "</td>");
                        sbPromoItem.Append("</tr>");
                    }
                }

                sbPromoItem.Append("</table>");
            }



            string val = sbPromoItem.ToString();

            Backend.SendEmail(EmailId, val, "Tradeshow Graphics Request Form");
            
            this.RouteToDashboard();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtEmail.Text))
        {
            hdnEmailId.Value = txtEmail.Text;
            EmailId = txtEmail.Text;
            _host.SaveWidgetDataMembers();
            ViewSet.SetActiveView(View);
        }
        else
        {
            errorLb.Text = "Please enter an email address.";
        }
    }

    private void RouteToDashboard()
    {
        // To do: Route to dashboard...
        var route = this._routeBuilder.DashBoard();
        this._router.RouteTo(route, Edwards.Utility.Constants.CurrentLanguageId);
    }

    private void RenderLabels()
    {
        tradefName.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoFirstName").ToString());
        tradelName.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoLastName").ToString());
        tradecompany.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "PromoComapny").ToString());
        tradeemailAdd.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "Promoemail").ToString());
		tradegraphicFormat.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "TradeshowGraphicFormat").ToString());
        tradeedwardscontact.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "TradeshowMaincontact").ToString());
        graphicDate.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "TradeshowDate").ToString());
        tradeLanguage.Attributes.Add("placeholder", this.GetGlobalResourceObject("EdwardsPortal", "TradeshowRequestedLanguage").ToString());
        isCustomGraphicRequest.Text = this.GetGlobalResourceObject("EdwardsPortal", "TradeshowCustomGraphicsAccept").ToString();
        sumbittradeItems.Text = this.GetGlobalResourceObject("EdwardsPortal", "TradeshowButtonText").ToString();
        tradegraphicFormat.Attributes.Add("placeholder",  this.GetGlobalResourceObject("EdwardsPortal", "TradeshowGraphicType").ToString());
        tradeedwardscontact.Attributes.Add("placeholder",  this.GetGlobalResourceObject("EdwardsPortal", "TradeshowContact").ToString());
    }
}